#ifndef DROBJECT_H_
#define DROBJECT_H_
#include <string>
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "AbstractClasses/DRParams.h"
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <iostream>

namespace spin
{
  /*! \brief DRObject
   *  This structure is common object used by all Dimensionality reduction algorithms.
   */
  template <class Matrix>
  class DRObject
  {
    public:
      DRObject(){}

      // Copy constructor (during prediction)
      DRObject(DRObject<Matrix>* p)
      {
        zcData            = p->zcData;
        numEigenvals      = p->numEigenvals;
        mean              = p->mean;
        minVal            = p->minVal;
        maxVal            = p->maxVal;
        projectionMatrix  = p->projectionMatrix;
        eigvals           = p->eigvals;
        eigvalsInv        = p->eigvalsInv;
        type              = p->type;
        kernel            = p->kernel;
        algorithm         = p->algorithm;
      }//end of function

      // Copy constructor (during training)
      DRObject(DRParams* p)
      {
        numEigenvals      = p->rank;
        type              = p->type;
        kernel            = p->kernel;
        algorithm         = p->algo;
      }//end of function

      // Copy from external data
      void operator = (const DRObject<Matrix>* p)
      {
        zcData            = p->zcData;
        numEigenvals      = p->numEigenvals;
        mean              = p->mean;
        minVal            = p->minVal;
        maxVal            = p->maxVal;
        projectionMatrix  = p->projectionMatrix;
        eigvals           = p->eigvals;
        eigvalsInv        = p->eigvalsInv;
        type              = p->type;
        kernel            = p->kernel;
        algorithm         = p->algorithm;
      }

      // The Zero-centered (or normalized : used with Kernel based methods) input data
      Matrix      zcData = Matrix::Zero(0,0);
      // The low-rank dimension
      int         numEigenvals = 0;
      // The mean across all rows ( a 1 x N matrix where N is # of columns: used with linear methods)
      Matrix      mean = Matrix::Zero(0,0);
      // The min/max of the data
      double      minVal  = 0;
      double      maxVal  = 0;

      // The projection matrix
      Matrix      projectionMatrix = Matrix::Zero(0,0);
      // The eigenvalues
      Matrix      eigvals = Matrix::Zero(0,0);
      // Inverse of eigenvalues
      Matrix      eigvalsInv = Matrix::Zero(0,0);
      // The projected data (for training data only)
      Matrix      projection = Matrix::Zero(0,0);;
      // Name of kernel (only for Kernel-based methods)
      std::string kernel = "";
      // Name of the algorithm
      std::string algorithm = "";
      // The type of data
      int         type = 1;

      // Overload the operator for printing
      friend std::ostream& operator<<(std::ostream& os, const DRObject<Matrix>* dr)
      {
        os << "Algorithm: "<<dr->algorithm<<std::endl;
        os << "Kernel: "<<dr->kernel<<std::endl;
        os << "Data Type: "<<dr->type<<std::endl;
        os << "Projection matrix: "<<dr->projectionMatrix<<std::endl;
        return os;
      }
    private:
      friend class boost::serialization::access;
      // When the class Archive corresponds to an output archive, the
      // & operator is defined similar to <<.  Likewise, when the class Archive
      // is a type of input archive the & operator is defined similar to >>.
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version)
      {
        ar & type;
        ar & algorithm;
        ar & kernel;
        ar & eigvals;
        ar & eigvalsInv;
        ar & projectionMatrix;
        ar & maxVal;
        ar & minVal;
        ar & mean;
        ar & numEigenvals;
        ar & zcData;
      }//end of class
  };//end of struct
}//end of namespace
#endif
