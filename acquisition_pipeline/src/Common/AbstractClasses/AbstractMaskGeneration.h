#ifndef ABSTRACTMASKGENERATION_H_
#define ABSTRACTMASKGENERATION_H_
#include <string>

namespace spin
{
  /*! \brief AbstractMaskGeneration
  *  Virtual class. Any producer class must inherit from this class so as to use
  *  the MPC design that has been defined in this module
  */
  class AbstractMaskGeneration
  {
    protected:
      virtual ~AbstractMaskGeneration() {}

    public :
      virtual int RunPipeline(void* p1, void* p2, void* p3, void* r, \
                              std::string _imgName, \
                              std::string outFldr, \
                              std::string outExtn, \
                              bool saveMask) = 0;

  };//end of class
}//end of spin namespace
#endif
