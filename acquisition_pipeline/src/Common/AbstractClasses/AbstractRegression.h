#ifndef ABSTRACTREGRESSION_H_
#define ABSTRACTREGRESSION_H_
#include <memory>

namespace spin
{
  // Forward declaration
  template <class T>
  class RegressionObject;

  /*! \brief AbstractRegression
   *  This is a base class that will be used to derive any regression algorithm.
   */
  template <class T>
  class AbstractRegression
  {
    public:
      virtual ~AbstractRegression(){}
      // Initialize
      virtual int Initialize(void* params=nullptr, void* model=nullptr) = 0;
      // Build projection matrix
      virtual int ComputeProjectionMatrix(void* input, void* obs, bool c=false) = 0;
      // Regress for new data
      virtual int ProjectNewData(T* input, T* output) = 0;
    protected:
      std::shared_ptr< RegressionObject<T> > model;
  };//end of class
}//end of namespace
#endif
