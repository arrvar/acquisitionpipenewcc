#ifndef REGRESSIONOBJECT_H_
#define REGRESSIONOBJECT_H_
#include <string>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include <iostream>
#include "AbstractClasses/AbstractMatrixTypes.h"

namespace spin
{
  /*! \brief RegressionObject
   *  This object defines a regression object that would need to be
   *  created (training), or populated (testing) when implementing any
   *  regression framework.
   */
  template <class T>
  class RegressionObject
  {
    public:
      // Default destructor
      ~RegressionObject
      {
        ProjectionMatrix.resize(0,0);
        TrainingData.resize(0,0);
      }

      // Constructors
      RegressionObject(){}
      // Overloaded
      RegressionObject(RegressionObject* obj)
      {
        this->ProjectionMatrix  = obj->ProjectionMatrix;
        this->TrainingData      = obj->TrainingData;
        this->kernel            = obj->kernel;
        this->sigma             = obj->sigma;
        this->q                 = obj->q;
      }
    private:
      // The projection matrix
      T ProjectionMatrix;
      // The training data
      T TrainingData;
      // The name of the kernel (if a kernel-based regression is used)
      std::string kernel;
      // Other values depending on regression algorithms used
      float sigma = 1.0;
      int   q = 1;
    private:
      friend class boost::serialization::access;
      // When the class Archive corresponds to an output archive, the
      // & operator is defined similar to <<.  Likewise, when the class Archive
      // is a type of input archive the & operator is defined similar to >>.
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version)
      {
        ar & q;
        ar & sigma;
        ar & kernel;
        ar & TrainingData;
        ar & ProjectionMatrix;
      }//end of class
  };//end of class
}//end of namespace
#endif
