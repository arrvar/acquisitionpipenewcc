#ifndef ABSTRACTIMAGETYPE_H_
#define ABSTRACTIMAGETYPE_H_

#include "itkImage.h"
#include "itkRGBPixel.h"
#include "itkImageFileWriter.h"
#include "itkImageFileReader.h"
#include "itkImageAdaptor.h"
#include "itkCastImageFilter.h"
//#include "itkOpenCVImageBridge.h"
#include "itkLaplacianSharpeningImageFilter.h"
#include "itkMedianImageFilter.h"

#include "AbstractClasses/AbstractMatrixTypes.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/eigen.hpp"

// Boost includes
#include "boost/filesystem.hpp"
#include <string>

// The following hack is needed in order to employ median filtering on  ITK
// images
namespace itk
{
  template< typename TComponent = unsigned char >
  class myRGBPixel : public RGBPixel<TComponent>
  {
    public:
      typedef myRGBPixel           Self;
      typedef RGBPixel<TComponent> Superclass;

      using RGBPixel<TComponent>::operator=;

      bool operator<=(const Self & r) const
      {
        return (this->GetLuminance() <= r.GetLuminance());
      }
      bool operator<(const Self & r) const
      {
        return (this->GetLuminance() < r.GetLuminance());
      }
  };//end of class
}//end of namespace

namespace spin
{

  /*! \brief A structure to hold information
   *  about individual channels of an RGB image
   */
  struct RGBTriplet
  {
    RMMatrix_Float c1;
    RMMatrix_Float c2;
    RMMatrix_Float c3;
  };//end of struct

  // Scalar Image Types - 2D
  typedef itk::Image<float,2> FloatImageType2D;
  typedef itk::Image<double,2> DoubleImageType2D;
  typedef itk::Image<int,2> IntImageType2D;
  typedef itk::Image<unsigned char,2> UCharImageType2D;
  typedef itk::Image<unsigned short,2> UShortImageType2D;

  // Scalar Image Types - 3D
  typedef itk::Image<float,3> FloatImageType3D;
  typedef itk::Image<double,3> DoubleImageType3D;
  typedef itk::Image<int,3> IntImageType3D;
  typedef itk::Image<unsigned char,3> UCharImageType3D;
  typedef itk::Image<unsigned short,3> UShortImageType3D;

  // RGB Image Types 2D
  typedef itk::myRGBPixel<unsigned char> RGBPixelType;
  typedef itk::Image<RGBPixelType,2> RGBImageType;

  // A structure to determine fidelity of blending
  struct RegionStruct
  {
    RGBImageType::RegionType refOverlapRegion;
    RGBImageType::RegionType tgtOverlapRegion;
    float metricThresh;
    float metricVal;
  };//end of struct

  // Access the green channel in a RGB image
  template <class T>
  class GreenChannelPixelAccessor
  {
    public:
      typedef RGBPixelType   InternalType;
      typedef T              ExternalType;
      static ExternalType Get( const InternalType& input )
      {
        return static_cast<ExternalType>( input.GetGreen() );
      }
  };

  // Access the blue channel in a RGB image
  template <class T>
  class BlueChannelPixelAccessor
  {
    public:
      typedef RGBPixelType   InternalType;
      typedef T              ExternalType;
      static ExternalType Get( const InternalType& input )
      {
        return static_cast<ExternalType>( input.GetBlue() );
      }
  };

  // Access the red channel in a RGB image
  template <class T>
  class RedChannelPixelAccessor
  {
    public:
      typedef RGBPixelType   InternalType;
      typedef T              ExternalType;
      static ExternalType Get( const InternalType& input )
      {
        return static_cast<ExternalType>( input.GetRed() );
      }
  };

  /*! \brief WriteITKImage
   *  Writes an image to disk using ITK.
   */
  template<typename T>
  void WriteITKImage(std::string path, T* itkImage)
  {
    typedef itk::ImageFileWriter< T  > WriterType;
    typename WriterType::Pointer writer = WriterType::New();
    writer->SetFileName(path.c_str());
    writer->SetInput(itkImage);
    try
    {
     writer->Update();
    }
    catch (itk::ExceptionObject & error)
    {
     #ifdef DEBUG
       //std::cout << "Error: " << error << std::endl;
     #endif
     return;
    }
  }//end of function

  /*! \brief ReadITKImage
   *  Reads an image from disk using ITK. As ITK does not
   *  have a PGM reader, we do not select PGM's as input
   */
  template<typename T>
  bool ReadITKImage(std::string path, T* itkImage)
  {
    // First check if the file exists
    if (boost::filesystem::exists(path))
    {
      // The image exists.
      // Get the extension of the file
      std::string file_extension = boost::filesystem::extension(path);
      if (file_extension == ".png"  || file_extension == ".PNG"   ||
          file_extension == ".bmp"  || file_extension == ".BMP"   ||
          file_extension == ".jpg"  || file_extension == ".JPG"   ||
          file_extension == ".jpeg" || file_extension == ".JPEG"  ||
          file_extension == ".tif"  || file_extension == ".TIF"   ||
          file_extension == ".tiff" || file_extension == ".TIFF"
        )
      {
        // The image reader
        typedef itk::ImageFileReader<T> InputReaderType;
        // Instantiate a pointer
        typename InputReaderType::Pointer reader = InputReaderType::New();
        reader->SetFileName(path);
        reader->Update();
        itkImage->Graft(reader->GetOutput());
        return true;
      }
    }
    return false;
  }//end of function

  /*! \brief This function reads an image and converts the data to LAB space
   *  using OpenCV
   */
  static void Channel2LAB(std::string path, RGBTriplet* channels)
  {
    cv::Mat inputImage = cv::imread(path);
    cv::Mat convMat;
    inputImage.convertTo(convMat,CV_32FC3);
    convMat *= 1./255;
    // conversion of BGR image to LAB
    cv::cvtColor(convMat, convMat, cv::COLOR_BGR2Lab);
    // Split the channels
    cv::Mat dummy;
    std::vector<cv::Mat> vec(3,dummy);
    cv::split(convMat,vec);
    // Convert each image to an eigen matrix
    channels->c1 = RMMatrix_Float::Zero(vec[0].rows, vec[0].cols);
    channels->c2 = RMMatrix_Float::Zero(vec[0].rows, vec[0].cols);
    channels->c3 = RMMatrix_Float::Zero(vec[0].rows, vec[0].cols);
    cv::cv2eigen(vec[0],channels->c1); // L
    cv::cv2eigen(vec[1],channels->c2); // a
    cv::cv2eigen(vec[2],channels->c3); // b
    vec.clear();
  }//end of function

 /* static void SharpeningPipeline(std::vector<cv::Mat>* vec)
  {
    typedef itk::MedianImageFilter<FloatImageType2D, FloatImageType2D> MedianFilter;
    typedef itk::LaplacianSharpeningImageFilter<FloatImageType2D, FloatImageType2D> SharpeningFilter;
    MedianFilter::InputSizeType radius;
    radius.Fill(4);

    for (int i = 0; i < vec->size(); ++i)
    {
      // Convert the cv::Mat to an ITK image
      FloatImageType2D::Pointer img = itk::OpenCVImageBridge::CVMatToITKImage< FloatImageType2D >((*vec)[i]);
      MedianFilter::Pointer median = MedianFilter::New();
      SharpeningFilter::Pointer sharpen = SharpeningFilter::New();
      median->SetInput(img);
      median->SetRadius(radius);
      sharpen->SetInput(median->GetOutput());
      sharpen->Update();
      // Convert the itkImage back to opencv
      (*vec)[i] = itk::OpenCVImageBridge::ITKImageToCVMat< FloatImageType2D >(sharpen->GetOutput());
    }
  }//end of function
  */	
  /*! \brief This function converts L,a,b into a fused image and
   *  saves it to disk.
   */
  static void LAB2Channel(RGBTriplet* channels, std::string path)
  {
    // Populate a vector of cv::Mat
    cv::Mat dummy;
    std::vector<cv::Mat> vec(3,dummy);
    vec[0] = cv::Mat::zeros(channels->c1.rows(), channels->c1.cols(), CV_32FC1);
    vec[1] = cv::Mat::zeros(channels->c2.rows(), channels->c2.cols(), CV_32FC1);
    vec[2] = cv::Mat::zeros(channels->c3.rows(), channels->c3.cols(), CV_32FC1);
    cv::eigen2cv(channels->c1, vec[0]);
    cv::eigen2cv(channels->c2, vec[1]);
    cv::eigen2cv(channels->c3, vec[2]);
    // Merge the channels
    cv::Mat convMat;
    cv::merge(vec, convMat);
    // And convert from LAB to BGR
    cv::cvtColor(convMat, convMat, cv::COLOR_Lab2BGR);
    // Scale the data with 255
    convMat *= 255;

    /////////////////////////////////////////////////////
    // At this time, we run the sharpening pipeline
    ////////////////////////////////////////////////////
    /*cv::split(convMat, vec);
    SharpeningPipeline(&vec);
    cv::merge(vec, convMat);
    vec.clear();*/
    // And convert the data
    cv::Mat outputImage;
    convMat.convertTo(outputImage,CV_8UC3);
    ////////////////////////////////////////////////////
    // Finally, save the
    cv::imwrite(path, outputImage);
    outputImage.release();
    convMat.release();
  }//end of function

  /*! \brief Split a RGB image into its respective channels and return it
   *  as a structure
   */
  static int Channel2Matrix(std::string path, RGBTriplet* channels)
  {
    // Read the RGB image
    RGBImageType::Pointer rgb = RGBImageType::New();
    ReadITKImage<RGBImageType>(path, rgb);
    // Split the channels
    typedef itk::ImageAdaptor<RGBImageType, BlueChannelPixelAccessor<unsigned char> > BlueAdaptor;
    typedef itk::ImageAdaptor<RGBImageType, GreenChannelPixelAccessor<unsigned char> > GreenAdaptor;
    typedef itk::ImageAdaptor<RGBImageType, RedChannelPixelAccessor<unsigned char> > RedAdaptor;

    typedef FloatImageType2D OutImage;
    OutImage::Pointer blueImage = OutImage::New();
    OutImage::Pointer greenImage = OutImage::New();
    OutImage::Pointer redImage = OutImage::New();

    // Instantiate an operational image
    BlueAdaptor::Pointer blue = BlueAdaptor::New();
    GreenAdaptor::Pointer green = GreenAdaptor::New();
    RedAdaptor::Pointer red = RedAdaptor::New();

    typedef itk::CastImageFilter<BlueAdaptor,OutImage> CastFilterBlue;
    typedef itk::CastImageFilter<GreenAdaptor,OutImage> CastFilterGreen;
    typedef itk::CastImageFilter<RedAdaptor,OutImage> CastFilterRed;

    // Extract blue channel
    CastFilterBlue::Pointer castBlue = CastFilterBlue::New();
    blue->SetImage(rgb);
    castBlue->SetInput(blue);
    try
    {
      castBlue->Update();
    }
    catch(...)
    {
      return -1;
    }

    // Extract green channel
    CastFilterGreen::Pointer castGreen = CastFilterGreen::New();
    green->SetImage(rgb);
    castGreen->SetInput(green);
    try
    {
      castGreen->Update();
    }
    catch(...)
    {
      return -2;
    }

    // Extract red channel
    CastFilterRed::Pointer castRed = CastFilterRed::New();
    red->SetImage(rgb);
    castRed->SetInput(red);
    try
    {
      castRed->Update();
    }
    catch(...)
    {
      return -3;
    }

    // Get the dimensions of the image
    RGBImageType::SizeType size = rgb->GetLargestPossibleRegion().GetSize();
    // Allocate memory for Eigen Matrices
    channels->c1 = RMMatrix_Float::Zero(size[1],size[0]);
    channels->c2 = RMMatrix_Float::Zero(size[1],size[0]);
    channels->c3 = RMMatrix_Float::Zero(size[1],size[0]);
    // And copy data
    memcpy(channels->c1.data(), castRed->GetOutput()->GetBufferPointer(), size[0]*size[1]*sizeof(float));
    // And copy data
    memcpy(channels->c2.data(), castGreen->GetOutput()->GetBufferPointer(), size[0]*size[1]*sizeof(float));
    // And copy data
    memcpy(channels->c3.data(), castBlue->GetOutput()->GetBufferPointer(), size[0]*size[1]*sizeof(float));
    return 1;
  }//end of function

  /*! \brief Merge 3 Eigen Matrices into a a RGB image and save the
   *  resultant image.
   */
  static int Matrix2Channel(RGBTriplet* channels, std::string path)
  {
    if (!channels) return -1;
    if (channels->c1.size() <=0 || channels->c2.size() <=0 || channels->c3.size() <=0) return -2;

    RGBImageType::RegionType region;
    RGBImageType::SizeType size;
    RGBImageType::IndexType index;
    index[0]=0;index[1]=0;
    size[0]=channels->c1.cols();
    size[1]=channels->c1.rows();
    region.SetSize(size);
    region.SetIndex(index);
    RGBImageType::Pointer image = RGBImageType::New();
    image->SetRegions(region);
    image->Allocate();

    for (int r = 0; r < size[1]; ++r)
    for (int c = 0; c < size[0]; ++c)
    {
      index[0] = c;
      index[1] = r;
      RGBPixelType pix;
      pix.SetRed(static_cast<unsigned char>(channels->c1(r,c)));
      pix.SetGreen(static_cast<unsigned char>(channels->c2(r,c)));
      pix.SetBlue(static_cast<unsigned char>(channels->c3(r,c)));
      image->SetPixel(index,pix);
    }

    // Save the file
    WriteITKImage<RGBImageType>(path, image);
    return 1;
  }//end of function

};//end of namespace

#endif
