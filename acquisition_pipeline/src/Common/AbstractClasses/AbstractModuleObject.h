#ifndef ABSTRACTMODULEOBJECT_H_
#define ABSTRACTMODULEOBJECT_H_
#include <string>
namespace spin
{
  /*! \brief ModuleObject
   *  This structure describes basic components
   *  of a Module object. It contains a path where the module 
   *  is located, the name of the module, and 
   *  a parameter file associated with this module.
   */
  struct ModuleObject
  {
    std::string pPath;
    std::string pModuleName="";
    std::string pParamFile="";
  };//end of struct
}//end of namespace
#endif
