#ifndef ABSTRACTWT2D_H_
#define ABSTRACTWT2D_H_

namespace spin
{
  class AbstractWT2D 
  {
    public:
      ~AbstractWT2D(){}
      
      virtual void SetBands(void* p) = 0;
      virtual int  Compute(void* data, void* output) = 0;
      virtual int  RecomposeBands(void* _output, int t=0) = 0;
      virtual void GetBands(void* p) = 0;
    protected:
      int   CheckLevels(int height, int width);
      void  Initialize(int levels, int height, int width, void* p);
  };//end of class
}//end of namespace
#endif
