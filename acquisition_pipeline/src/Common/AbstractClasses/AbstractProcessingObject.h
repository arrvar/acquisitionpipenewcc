#ifndef ABSTRACTPROCESSINGOBJECT_H_
#define ABSTRACTPROCESSINGOBJECT_H_
#include <cstdlib>
#include <string>
#include <memory>
#include <vector>
#include "AbstractClasses/AbstractDataStructures.h"
#include "AbstractClasses/AbstractModuleObject.h"
#include "Framework/Logger/LoggerInterface.h"
#include "Framework/ImageIO/ImageIO.h"

namespace spin
{
  /*! \brief AbstractProcessingObject
    *  This structure contains basic information pertaining to any
    *  processing that needs to be done within the Vista framework.
    */
  struct AbstractProcessingObject
  {
    // Default destructor
    virtual ~AbstractProcessingObject()
    {
      std::map<int, std::vector<ModuleObject> >::iterator it = mPlugins.begin();
      while (it != mPlugins.end())
      {
        it->second.clear();
        ++it;
      }
      mPlugins.clear();
      mCalImage = NULL;
      mModuleStatus.clear();
    }

    // An image IO object
    std::shared_ptr<spin::RGBImageIO> ImageIO;

    // File extension
    std::string file_extension;
    // Output folder
    std::string outDir;

    // Name of the calibration image
    std::string bgImage;
    // Actual image as an ITK image
    spin::RGBImageType::Pointer mCalImage;
    // Should white-normalization be effected on the images
    bool Normalize;
    // Type of normalization
    int normType = 0;
    // Dimensions of the images being processed
    int imgWidth;
    int imgHeight;
    // Number of images to be processed
    int max_images;

    // A Model file associated with each processing pipeline
    std::string modelFile;

    // A logger module
    std::shared_ptr<LoggerInterface> logger;
    // And if logging is enabled
    bool loggingInitialized;

    // A path to a database file
    std::string dbFile;

    // Flag for calibration
    bool calibration;
    // Magnification number
    int  magnification;

    // All plugins that need to be processed
    std::map<int, std::vector<ModuleObject> > mPlugins;

    // A registry of modules
    std::map<int, bool> mModuleStatus;
	
	// Some information about the grid
	int gridHeight;
	int gridWidth;
	int scanDirection;
	int scanType;
  };//end of struct
}//end of namespace spin

#endif
