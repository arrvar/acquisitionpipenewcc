#ifndef ABSTRACTMATRIXTYPES_H_
#define ABSTRACTMATRIXTYPES_H_

// Temporary, until eigen library is updated
#ifndef EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET
#define EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET
#endif

//Eigen3 includes
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <boost/serialization/array_wrapper.hpp>
#include <boost/serialization/array.hpp>

namespace spin
{
  // Eigen is column major by default. Hence, we explicitly force the matrices to
  // be row-major
  typedef std::complex<float>  Complex;
  typedef Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RMMatrix_UChar;
  typedef Eigen::Matrix<char, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RMMatrix_Char;
  typedef Eigen::Matrix<unsigned short, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RMMatrix_UShort;
  typedef Eigen::Matrix<Complex,       Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RMMatrix_Complex;
  typedef Eigen::Matrix<double,        Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RMMatrix_Double;
  typedef Eigen::Matrix<float,         Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RMMatrix_Float;
  typedef Eigen::Matrix<int,           Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RMMatrix_Int;

  typedef Eigen::SparseMatrix<unsigned char, Eigen::RowMajor>        RMSparseMatrix_UChar;
  typedef Eigen::SparseMatrix<double       , Eigen::RowMajor>        RMSparseMatrix_Double;
  typedef Eigen::SparseMatrix<float        , Eigen::RowMajor>        RMSparseMatrix_Float;
  typedef Eigen::SparseMatrix<int          , Eigen::RowMajor>        RMSparseMatrix_Int;
  typedef Eigen::SparseMatrix<Complex      , Eigen::RowMajor>        RMSparseMatrix_Complex;

  typedef Eigen::DynamicSparseMatrix<unsigned char, Eigen::RowMajor> RMDSparseMatrix_UChar;
  typedef Eigen::DynamicSparseMatrix<double,        Eigen::RowMajor> RMDSparseMatrix_Double;
  typedef Eigen::DynamicSparseMatrix<float ,        Eigen::RowMajor> RMDSparseMatrix_Float;
  typedef Eigen::DynamicSparseMatrix<int   ,        Eigen::RowMajor> RMDSparseMatrix_Int;
  typedef Eigen::DynamicSparseMatrix<Complex,       Eigen::RowMajor> RMDSparseMatrix_Complex;

  // Column major matrices
  typedef Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>  CMMatrix_UChar;
  typedef Eigen::Matrix<char, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>  CMMatrix_Char;
  typedef Eigen::Matrix<unsigned short, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> CMMatrix_UShort;
  typedef Eigen::Matrix<double,        Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>  CMMatrix_Double;
  typedef Eigen::Matrix<Complex,       Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>  CMMatrix_Complex;
  typedef Eigen::Matrix<float,         Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>  CMMatrix_Float;
  typedef Eigen::Matrix<int,           Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>  CMMatrix_Int;

  typedef Eigen::SparseMatrix<unsigned char, Eigen::ColMajor>        CMSparseMatrix_UChar;
  typedef Eigen::SparseMatrix<double       , Eigen::ColMajor>        CMSparseMatrix_Double;
  typedef Eigen::SparseMatrix<float        , Eigen::ColMajor>        CMSparseMatrix_Float;
  typedef Eigen::SparseMatrix<int          , Eigen::ColMajor>        CMSparseMatrix_Int;
  typedef Eigen::SparseMatrix<Complex      , Eigen::ColMajor>        CMSparseMatrix_Complex;

  typedef Eigen::DynamicSparseMatrix<unsigned char, Eigen::ColMajor> CMDSparseMatrix_UChar;
  typedef Eigen::DynamicSparseMatrix<double, Eigen::ColMajor>        CMDSparseMatrix_Double;
  typedef Eigen::DynamicSparseMatrix<float , Eigen::ColMajor>        CMDSparseMatrix_Float;
  typedef Eigen::DynamicSparseMatrix<int   , Eigen::ColMajor>        CMDSparseMatrix_Int;
  typedef Eigen::DynamicSparseMatrix<Complex,Eigen::RowMajor>        CMDSparseMatrix_Complex;
}//end of spin namespace

namespace boost
{
  namespace serialization
  {
    template<class Archive, typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
    inline void serialize(
        Archive & ar,
        Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols> & t,
        const unsigned int file_version
    )
    {
        size_t rows = t.rows(), cols = t.cols();
        ar & rows;
        ar & cols;
        if( rows * cols != t.size() )
        t.resize( rows, cols );
        ar & boost::serialization::make_array(t.data(), rows*cols);
    }
  }
}
#endif
