#ifndef ABSTRACTDATASTRUCTURES_H_
#define ABSTRACTDATASTRUCTURES_H_
#include <vector>
#include <map>
#include <utility>
#include "AbstractClasses/AbstractImageTypes.h"
#include "itkImageDuplicator.h"

namespace spin
{
  namespace vista
  {
    typedef std::pair<double, double> pDouble;
	
    struct ProducerDataType
    {
      ~ProducerDataType()
      {
        Clear();
      }

      void Clear()
      {
      }
      // The FFT of the image
      spin::RMMatrix_Complex fft;
	  
      // Image in ITK format
      spin::RGBImageType::Pointer rgbImage;
      // Image in OpenCV format
      cv::Mat Img_cv;
	  
      // Grid coordinates of the image
      int gXCol;
      int gYRow;
      std::string aoiName;
      bool isForeground = true;

      int Magnification = 1;
    };//end of struct

    struct OverlapMetric
    {
      float displacement_fftcost;
    };//end of struct

    /*! \brief FOVStruct
     *  A structure to hold information about a given FOV
     */
    struct FOVStruct
    {
      virtual ~FOVStruct()
      {
        Clear();
      }

      void Clear()
      {
        Img = NULL;
        Img_g = NULL;
        fft.resize(0,0);
        Img_cv.release();
        tNeighbors.clear();
        tMetrics.clear();
      }
      // Name of the AOI at this node
      std::string                               aoiName;
      // The index location of this AOI within the scanning grid
      std::pair<int, int>                       index;
      // Dimensions of this AOI
      std::pair<int, int>                       actDimensions;
      // The actual image (in ITK format)
      RGBImageType::Pointer                     Img;
      // The actual image (in ITK format)
      UCharImageType2D::Pointer                 Img_g;
      // The actual image (in cv::Mat format)
      cv::Mat                                   Img_cv;
      // A complex matrix to hold the FFT of this AOI
      RMMatrix_Complex                          fft;
      // A 1D node corresponding to a 2D index
      int                                       nodeId;
      // A list of all neighbors (displacement estimation)
      std::vector< pDouble >                    tNeighbors;
      // A list of metrics between all neighbors
      std::map< pDouble, OverlapMetric>         tMetrics;
      // Focus ratio w.r.t a well focused image
      float                                     focusRatio=0;
      // Bg ratio w.r.t a well defined fg/bg image
      float                                     bgRatio=0;
      // Mark this AOi as a FG/BG (default is foreground)
      bool                                      isBackground=false;
      // Mark this AOI has having passed through pre-processing
      bool                                      f_preprocess=false;
      // Mark this AOI has having saved to disk
      bool                                      f_write=false;
    };//end of structure

  }//end of stitching namespace

  namespace hemocalc
  {
    typedef std::pair<int, int> pIndex;

    struct AnalysisDataType
    {
      ~AnalysisDataType()
      {
        if (rgbImagePrimary) rgbImagePrimary = NULL;
        if (rgbImagePrimary_wc) rgbImagePrimary_wc = NULL;
        rgbImageSecondary.clear();
      }

      void Clear()
      {
      }
      // Overload = operator to add two Box objects.
      void operator=(AnalysisDataType& b)
      {
        //AnalysisDataType dType;
        this->gXCol = b.gXCol;
        this->gYRow = b.gYRow;
        this->saveImages = b.saveImages;
        this->saveMasks  = b.saveMasks;

        // Copy the primary image
        this->rgbImagePrimary = spin::RGBImageType::New();
        this->rgbImagePrimary->Graft(b.rgbImagePrimary);

        // Copy the primary image
        this->rgbImagePrimary_wc = spin::RGBImageType::New();
        this->rgbImagePrimary_wc->Graft(b.rgbImagePrimary_wc);

        // Copy all secondary images
        std::map<int, spin::RGBImageType::Pointer>::const_iterator it = b.rgbImageSecondary.begin();
        while (it != b.rgbImageSecondary.end())
        {
          this->rgbImageSecondary[it->first] = spin::RGBImageType::New();
          this->rgbImageSecondary[it->first]->Graft(it->second);
          ++it;
        }
        //return dType;
      }//end of function

      spin::RGBImageType::Pointer rgbImagePrimary;
      spin::RGBImageType::Pointer rgbImagePrimaryCalib;
      spin::RGBImageType::Pointer rgbImagePrimary_wc;
      std::map<int, spin::RGBImageType::Pointer> rgbImageSecondary;
      int gXCol;
      int gYRow;
      bool saveImages=false;
      bool saveMasks =false;
    };
  }//end of hemocalc namespace
}//end of spin namespace

#endif
