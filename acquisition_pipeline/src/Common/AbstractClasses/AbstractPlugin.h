#ifndef ABSTRACTPLUGIN_H_
#define ABSTRACTPLUGIN_H_

namespace spin
{
  class AbstractPlugin
  {
    public:
      // Default destructor
      virtual ~AbstractPlugin(){}

      /*! \brief InstantiatePipeline */
      virtual int InstantiatePipeline(const char* inp, void* meta=nullptr)=0;

      /*! \brief Process Pipeline */
      virtual int ProcessPipeline(void* input1, void* input2, void* output)=0;

      /*! \brief Complete Processing */
      virtual int CompleteProcessing()=0;
  };//end of class
}//end of namespace
#endif
