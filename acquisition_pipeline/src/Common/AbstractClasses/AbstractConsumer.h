#ifndef ABSTRACTCONSUMER_H_
#define ABSTRACTCONSUMER_H_

namespace spin
{
  /*! \brief AbstractConsumer
   *  Virtual class. Any consumer class must inherit from this class so as to use
   *  the MPC design that has been defined in this module
   */
  template<class P>
  class AbstractConsumer
  {
    protected :
      virtual ~AbstractConsumer() {}

    public :
      virtual void ConsumeFromQueue(P dataPoint, void* obj) = 0;
      virtual void RequestQueue(void* obj) = 0;
  };// End of class
}//end of spin namespace
#endif
