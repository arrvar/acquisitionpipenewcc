#ifndef ABSTRACTPRODUCER_H_
#define ABSTRACTPRODUCER_H_

namespace spin
{
  /*! \brief AbstractProducer
   *  Virtual class. Any producer class must inherit from this class so as to use
   *  the MPC design that has been defined in this module
   */
  class AbstractProducer
  {
    protected:
      virtual ~AbstractProducer() {}

    public :
      virtual void AddToQueue(void* dataPoint) = 0;

  };//end of class
}//end of spin namespace
#endif
