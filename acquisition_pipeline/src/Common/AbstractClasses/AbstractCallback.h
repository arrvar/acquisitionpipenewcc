#ifndef ABSTRACTCALLBACK_H_
#define ABSTRACTCALLBACK_H_

#include <boost/signals2.hpp>
#include <string>
#include <sstream>
#include <utility>

namespace spin
{
  typedef std::pair<double, double> pDouble;
  /*! \brief CallbackStruct
   *  This structure describes the callback process for any module in the
   *  pipeline
   */
  struct CallbackStruct
  {
    std::string Module        ; // Some description of the module/function
    int         Result=1      ; // Outcome
    std::string Error="Pass"  ; // If any error has been generated
    std::string Descr=""      ; // More description
    int         Finished=0    ; // By default, process is not finished
    double      Time          ; // Elapsed time
    pDouble     Ref           ;
    pDouble     Tgt           ;
    bool        batch=false   ; // A batch process is needed?
  };

  struct FovCompleted
  {
    int         Module        ; // Some description of the module/function
    int         gXCol         ; // The x-coordinate of the FOV in the grid
    int         gYRow         ; // The y-coordinate of the FOV in the grid
  };

  struct ModuleStatus
  {
    int         Module=-1     ; // Index of module
    bool        status=true   ; // Indicates that it is completed
  };

  /*! \brief Signal to communicate between main modules*/
  typedef boost::signals2::signal<void(bool)> BoolSignal;
  /*! \brief Signal to report progress of an algorithm*/
  typedef boost::signals2::signal<void(CallbackStruct)> CallbackSignal;
  /*! \brief void signal callback */
  typedef boost::signals2::signal<void()> VoidSignal;
  /*! \brief char* signal callback*/
  typedef boost::signals2::signal<void(char*)> CharSignal;
  /*! \brief Signal to indicate a FOV has finished processing*/
  typedef boost::signals2::signal<void(FovCompleted)> FOVSignal;
  /*! \brief Signal to indicate a consumer / producer has finished operation */
  typedef boost::signals2::signal<void(ModuleStatus)> ModuleSignal;
}
#endif
