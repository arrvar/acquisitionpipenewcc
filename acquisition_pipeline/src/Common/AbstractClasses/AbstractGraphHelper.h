#ifndef ABSTRACTGRAPHHELPER_H
#define ABSTRACTGRAPHHELPER_H
//Boost includes
#include <boost/config.hpp>
#include <boost/format.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/labeled_graph.hpp>
#include <boost/property_map/property_map.hpp>
#include <utility>

namespace spin
{
  //boost graph library typedefs
  typedef std::pair<int, int>                                                         Edge;
  typedef boost::adjacency_list < boost::vecS, 
                                  boost::vecS, 
                                  boost::undirectedS, 
                                  boost::no_property,
                                  boost::property < boost::edge_weight_t, double > >  graph_t;
  typedef boost::graph_traits < graph_t >::vertex_descriptor                          vertex_t;
  typedef boost::graph_traits < graph_t >::edge_descriptor                            edge_t;
  typedef boost::property_map<graph_t,boost::edge_weight_t>::type                     edgeWeight_t;
  typedef boost::property_map<graph_t,boost::vertex_index_t>::type                    vertIndex_t;
  
  typedef boost::adjacency_list < boost::vecS, 
                                  boost::vecS, 
                                  boost::directedS, 
                                  boost::no_property,
                                  boost::property < boost::edge_weight_t, double > >  dgraph_t;
  typedef boost::graph_traits < dgraph_t >::vertex_descriptor                         dvertex_t;
  typedef boost::graph_traits < dgraph_t >::edge_descriptor                           dedge_t;
  typedef boost::property_map<dgraph_t,boost::edge_weight_t>::type                    dedgeWeight_t;
  typedef boost::property_map<dgraph_t,boost::vertex_index_t>::type                   dvertIndex_t;
}
#endif
