#ifndef ABSTRACTDIMENSIONALITYREDUCTION_H_
#define ABSTRACTDIMENSIONALITYREDUCTION_H_
#include <memory>

namespace spin
{
  // forward declaration
  template <class T>
  struct DRObject;

  template <class T>
  class RandomizedSVD;

  template <class T>
  class AbstractDimensionalityReduction
  {
    public:
      /*! \brief Default constructor (Training)*/
      AbstractDimensionalityReduction(void* p, bool r=false);

      virtual ~AbstractDimensionalityReduction(){}

      /*! \brief Initialize */
      virtual int InitializeKernel(bool t = true) = 0;

      /*! \brief ComputeProjectionMatrix */
      virtual int ComputeProjectionMatrix(T* m, bool c = false) = 0;

      /*! \brief ProjectNewData */
      virtual int ProjectNewData(T* m, T* n) = 0;

      const DRObject<T>* GetData()
      {
        return drObj.get();
      }//end of function
    protected:
      std::shared_ptr< DRObject<T> > drObj;
      std::shared_ptr<RandomizedSVD<T> > rsvd;
      void ZeroCenterData(T* m_InputData);
      void NormalizeData(T* m_InputData, bool f=true);
  };//end of class
};//end of namespace
#endif
