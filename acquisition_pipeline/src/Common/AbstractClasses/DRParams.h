#ifndef DRPARAMS_H_
#define DRPARAMS_H_
#include <string>

namespace spin
{
  /*! \brief DRParams
   *  This structure holds information about various
   *  parameters that are used in common dimensionality reduction
   *  algorithms. Not all parameters are needed by all algorithms.
   */
  struct DRParams
  {
    /*! \brief Default constructor */
    DRParams()
    {
      // Common defaults
      rank              = 4;
      kernel            = "";
      algo              = "";
      nearestNeighbors  = 10;
      sigma             = 2.0;
      power             = 2;
      type              = 1;
      samples           = 0;

      // Defaults for NSPCA
      nspca_alpha           = 1.0e+5;
      nspca_beta            = 0;
      nspca_iterations      = 500;
      nspca_breakPercent    = 0.00001;
    }//end of function

    /*! \brief Default constructor */
    DRParams(DRParams* p)
    {
      rank                  = p->rank;
      kernel                = p->kernel;
      algo                  = p->algo;
      nearestNeighbors      = p->nearestNeighbors;
      sigma                 = p->sigma;
      power                 = p->power;
      type                  = p->type;
      samples               = p->samples;

      // copy other values
      nspca_alpha           = p->nspca_alpha;
      nspca_beta            = p->nspca_beta;
      nspca_iterations      = p->nspca_iterations;
      nspca_breakPercent    = p->nspca_breakPercent;
    }//end of function

    // Low-rank
    int rank;

    // Number of samples
    int samples;
    // The name of the kernel (name of the plugin to be searched for)
    std::string kernel;
    // The name of the algorithm (name of the plugin to be searched for)
    std::string algo;
    // The # of nearest neighbors for non-linear dimensionality reduction
    int nearestNeighbors;
    // The value of \sigma for RBF type kernels
    float sigma;
    // The power for matrix conditioning
    int power;

    // i=Type of data
    int type;

    // Parameters associated with NSPCA
    double nspca_alpha ;
    double nspca_beta;
    int    nspca_iterations;
    double nspca_breakPercent;
  };//end of struct
}//end of namespace
#endif
