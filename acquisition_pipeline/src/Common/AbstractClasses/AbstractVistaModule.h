#ifndef ABSTRACTVISTAMODULE_H_
#define ABSTRACTVISTAMODULE_H_
#ifdef VISTA_PLUGIN_FRAMEWORK
#include <string>
#include <memory>
#include "AbstractClasses/AbstractProcessingObject.h"
#include "AbstractClasses/AbstractCallback.h"

/*! \brief AbstractVistaModule
 *  This is an asbtract class that defines all routines to be
 *  exposed to an external library. This class should be used
 *  when building Vista Acquisition and Panorama as plugins
 */
namespace spin
{
  class AbstractVistaModule
  {
    public:
      typedef spin::AbstractProcessingObject sVObject;
      // Default destructor
      virtual ~AbstractVistaModule()
      {
        //std::cout<<"coming here"<<std::endl;
      }

      // Initialize the pipeline
      virtual int Initialize(const char* pFile, bool f=true)=0;

      // Computing displacements for every aoi
      virtual void ProcessGridLocations(std::string InputImage, \
					                              float gx, float gy, \
                              		      int background)=0;
      // Compute displacements for every aoi
      virtual void ProcessGridLocations( void* _InputImage, \
					                               float gx, float gy,\
                               		       int background)=0;

      // A thread to finish computing the displacements
      virtual void CompleteProcessGridLocations(bool f=true)=0;

      // Abort displacement estimation
      virtual void AbortProcessGridLocations(bool f=true)=0;

      // Run Panorama Pipeline
      virtual int RunPanorama(const char* pFile) = 0;

      // Abort panorama generation
      virtual void AbortPanorama() = 0;

      // Get a const access to gObject
      const std::shared_ptr<sVObject> GetGObject(){return gObject;}

      // Signal indicating completion of pipeline
      BoolSignal sig_Finished;
    protected:
      // Instantiate a spin vista object
      std::shared_ptr<sVObject>   gObject;
  };//end of class
}//end of namespace
#endif
#endif
