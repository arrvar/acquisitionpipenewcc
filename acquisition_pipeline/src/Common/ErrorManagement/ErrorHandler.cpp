#include <QString>

#include "ErrorManagement/ErrorHandler.h"

namespace spin
{
  //Initializing the static reference to NULL
  ErrorHandler* ErrorHandler::s_instance = 0;

  /**
   * @brief get Static method that gives the singleton instance of the error
   * handler.
   * @return Singleton instance of the error handler.
   */
  ErrorHandler* ErrorHandler::get()
  {
    if(!s_instance)
    {
      s_instance = new ErrorHandler();
    }
    return s_instance;
  }


  /**
   * @brief pushError Pushes the error into error handlers stack for later
   * retrieval.
   * @param message Error message to be stored.
   */
  void ErrorHandler::pushError( QString message )
  {
    m_lock.lockForWrite();
    m_errorStack.push( message );
    m_lock.unlock();
  }

  /**
   * @brief lastError Gives the last error without popping from the stack.
   * @return Error that was pushed to error handler most recently.
   */
  QString ErrorHandler::lastError()
  {
    m_lock.lockForRead();
    QString errorMessage = m_errorStack.top();
    m_lock.unlock();
    return errorMessage;
  }

  /**
   * @brief popError Pops the error at the top of the stack.
   * @return Error at the top of the error handler stack.
   */
  QString ErrorHandler::popError()
  {
    m_lock.lockForWrite();
    QString errorMessage = m_errorStack.pop();
    m_lock.unlock();
    return errorMessage;
  }

  /**
   * @brief popAllErrors Pops all the errors from the error handler stack
   * and returns them as a list. Once this method is called the error handler
   * stack will be empty.
   * @return All the errors that were pushed to error handler, empty list if
   * no errors were pushed to the error handler.
   */
  QStringList ErrorHandler::popAllErrors()
  {
    m_lock.lockForWrite();
    QStringList errorList = m_errorStack.toList();
    m_errorStack.clear();
    m_lock.unlock();
    return errorList;
  }

  /**
   * @brief hasErrors Tells whether there are any errors present in the error
   * handler.
   * @return true if one or more error strings are present in the handler.
   */
  bool ErrorHandler::hasErrors()
  {
    m_lock.lockForRead();
    bool result = ! m_errorStack.isEmpty();
    m_lock.unlock();
    return result;
  }

  /**
   * @brief errorCount Gives the number of errors present in the error handler
   * @return Number of errors present in the error handler.
   */
  quint32 ErrorHandler::errorCount()
  {
    m_lock.lockForRead();
    quint32 count = m_errorStack.size();
    m_lock.unlock();
    return count;
  }
  /**
   * @brief clear Clear all the errors in error handler.
   */
  void ErrorHandler::clear()
  {
    m_lock.lockForWrite();
    m_errorStack.clear();
    m_lock.unlock();
  }
}
