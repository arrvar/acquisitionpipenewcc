#ifndef ERRORHANDLER_H
#define ERRORHANDLER_H

#include <QStack>
#include <QStringList>
#include <QReadWriteLock>

#include "Framework/HardwareManager/Framework/AppContext.h"
#include "Framework/HardwareManager/Utils/Common.h"

namespace spin
{
  /**
   * @brief The ErrorHandler class is created for error handling in a SPIN GUI
   * application.
   */
  class ErrorHandler
  {
  public:
    /**
     * @brief pushError Pushes the error into error handlers stack for later
     * retrieval.
     * @param message Error message to be stored.
     */
    void pushError(QString message);

    /**
     * @brief lastError Gives the last error without popping from the stack.
     * @return Error that was pushed to error handler most recently.
     */
    QString lastError();

    /**
     * @brief popError Pops the error at the top of the stack.
     * @return Error at the top of the error handler stack.
     */
    QString popError();

    /**
     * @brief popAllErrors Pops all the errors from the error handler stack
     * and returns them as a list. Once this method is called the error handler
     * stack will be empty.
     * @return All the errors that were pushed to error handler, empty list if
     * no errors were pushed to the error handler. The most recent error is the
     * last element of the list.
     */
    QStringList popAllErrors();

    /**
     * @brief hasErrors Tells whether there are any errors present in the error
     * handler.
     * @return true if one or more error strings are present in the handler.
     */
    bool hasErrors();

    /**
     * @brief errorCount Gives the number of errors present in the error handler
     * @return Number of errors present in the error handler.
     */
    quint32 errorCount();

    /**
     * @brief clear Clear all the errors in error handler.
     */
    void clear();

    /**
     * @brief get Static method that gives the singleton instance of the error
     * handler. It is recommended that this method is called at least once as
     * soon as the main thread starts execution since this method is not
     * synchronized before the singleton instance is created.
     * @return Singleton instance of the error handler.
     */
    static ErrorHandler* get();

  private:
    /**
     * @brief ErrorHandler Private constructor that prevents external
     * instantiation.
     */
    ErrorHandler()
    {

    }

    DISALLOW_COPY_AND_ASSIGNMENT(ErrorHandler)

    /**
     * @brief m_errorStack A stack that holds the error messages that are pushed
     * to the error handler.
     */
    QStack <QString> m_errorStack;

    /**
     * @brief m_lock A read write lock for synchronizing the error handler
     * operations since handler can be used from multiple threads.
     */
    QReadWriteLock m_lock;

    /**
     * @brief s_instance Static singleton instance of the error handler.
     */
    static ErrorHandler* s_instance;

  };// end of ErrorHandler namespace.
} // end of spin namespace.


#ifdef SPIN_NO_LOGGING
  #define SPIN_ERROR(msg) \
      spin::ErrorHandler::get()->pushError(msg)

  #define SPIN_FATAL(msg) \
      spin::ErrorHandler::get()->pushError(msg)
#else
  /**
   * @macro ERROR pushes the error message to error handler and if logging
   * is enabled a error message is written to log.
   */
  #define SPIN_ERROR(msg) \
      spin::AppContext::get().logger()->error(msg, FUNCTION_NAME); \
      spin::ErrorHandler::get()->pushError(msg)

  /**
   * @macro FATAL pushes the fatal error message to the error handler and
   * if logging is enable writes a fatal log message to the log.
   */
  #define SPIN_FATAL(msg) \
      spin::AppContext::get().logger()->fatal(msg, FUNCTION_NAME); \
      spin::ErrorHandler::get()->pushError msg)
#endif

#endif // ERRORHANDLER_H
