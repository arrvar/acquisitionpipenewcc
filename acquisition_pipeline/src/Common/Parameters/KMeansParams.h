#ifndef KMEANSPARAMS_H_
#define KMEANSPARAMS_H_
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

namespace spin
{
  /*! \brief All parameters associated with kmeans algorithm */
  struct KMeansParams
  {
    ~KMeansParams(){}
    int     iterations=20;
    int     seed = 200;
    int     numclusters = 256;
    int     samplingInterval = -1;

    // When the class Archive corresponds to an output archive, the
    // & operator is defined similar to <<.  Likewise, when the class Archive
    // is a type of input archive the & operator is defined similar to >>.
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
      ar & iterations;
      ar & seed;
      ar & numclusters;
      ar & samplingInterval;
    }//end of function

  };//end of struct
}//end of namespace
#endif
