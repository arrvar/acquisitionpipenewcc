#ifndef EIGENHDF5IO_H_
#define EIGENHDF5IO_H_
#include <string>
#include <memory>

namespace spin
{
  namespace EigenHdf5FS 
  {
    struct FileStructure;    
  }
  
  /*! \brief EigenHdf5IO
   *  This class is a common placeholder for all aspects related
   *  to reading and writing to a HDF5 file.
   */
  class EigenHdf5IO 
  {
    public:
      /*! \brief Default Constructor */
      EigenHdf5IO(std::string& mFile, bool overwrite=true);
      
      /*! \brief Default constructor */
      EigenHdf5IO();
      
      /*! \brief Default Destructor */
      ~EigenHdf5IO(){}
      
      /*! \brief Parse tags in the hdf5 file */
      int ParseTags(bool data=true);
      
      /*! \brief ReadData */
      int ReadData(void* _tTags, int type, void* _data);
      
      /*! \brief Create group */
      int CreateGroup(std::string& group);
      
      /*! \brief Write data */
      int WriteData(std::string& group, int type, void* _data);
      
      /*! \brief Read Model */
      int ReadModel(std::string& model, void* _data);
      
      /*! \brief Save Model */
      int SaveModel(std::string& model, void* _data);
    private:
      std::shared_ptr< EigenHdf5FS::FileStructure > fs;
      int CheckDataSanity();
      int CheckModelSanity();
  };//end of class
}//end of namespace
#endif
