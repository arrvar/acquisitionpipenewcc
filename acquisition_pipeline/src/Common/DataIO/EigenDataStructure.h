#ifndef EIGENDATASTRUCTURE_H_
#define EIGENDATASTRUCTURE_H_
#include <vector>
#include <string>

namespace spin
{
  /*! \brief EigenDataStructure
   *  This structure holds information about data parsed from a HDF5 file
   *  and stored with its class labels (if available) and the size of each label
   */
  template <class T>
  struct EigenDataStructure
  {
    // Default constructor
    EigenDataStructure(){}

    // Copying constructor
    EigenDataStructure(const EigenDataStructure<T>* p)
    {
      Copy(p);
    }//end of function

    ~EigenDataStructure()
    {
      Labels.clear();
      Size.clear();
      Block.clear();
    }

    void Copy(const EigenDataStructure<T>* p)
    {
      Size = p->Size;
      Labels = p->Labels;
      regression = p->regression;
      Input = p->Input;
    }//end of function

    T Input;
    T Prior;
    std::vector<int> Size;
    std::vector<std::string> Labels;
    std::vector<int> Block;
    bool regression=false;//default
  };//end of struct
}//end of namespace
#endif
