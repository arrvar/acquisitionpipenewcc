#ifndef FILESTRUCTURE_H_
#define FILESTRUCTURE_H_
#include <vector>
#include <string>
#include <memory>
#include <map>

namespace spin
{
  namespace EigenHdf5FS
  {
    /*! \brief FileStructure
     *  This structure holds information about tags parsed from a
     *  hdf5 file.
     */
    struct FileStructure 
    {   
      typedef std::map<std::string, std::string> DataPriorMapping;
      
      /*! \brief default destructor*/
      ~FileStructure()
      { 
        tags.clear();
        dmap.clear();
      }
      
      // Name of hdf5 file
      std::string fName;
      // All data related tags in the file
      std::vector<std::string> tags;
      // Check if regression analysis can be done
      bool regression = false;// default
      // Get a mapping 
      DataPriorMapping dmap;
    };//end of structure 
  }//end of namespace
}//end of namespace
#endif
