#include "DataIO/EigenHdf5IO.h"
#include "DataIO/FileStructure.h"
#include "DataIO/EigenDataStructure.h"
#include "DataIO/eigenHdf5.h"
#include "ModelIO/SerializedModel.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <H5Fpublic.h>
#include <H5Cpp.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <vector>

/*! \brief The functions described in this file pertain to parsing information
 *  from a data file for either classification or regerssion.
 *  A typical data file will conform to the following structure:
 *  /Data
 *   |
 *   -- <Label1>
 *   |  |
 *   |  -------< K1 rows (i.e. features) of Label 1
 *   |
 *   |
 *   -- <Label2>
 *      |
 *      -------< K2 rows (i.e. features) of Label 2
 *
 *   /Prior
 *   will follow a similar structure as per Data (i.e. each row of each label will have one prior feature).
 *   If this tag is present, then it imples that regression can be performed on the data. If not present
 *   then it implies that only classification can be performed on the data.
 *
 *   /MetaInfo
 *   will follow a similar structure as per Data; This is purely optional and will not be used for
 *   either classification or regression.
 *   A file is parsed for the tags. If the file does not conform to this data structure then no further
 *   analysis is performed on the file.
 *
 *   In another instance, we can also parse data pertaining to a model. In this the primary tag that
 *   we look out for is:
 *   /Model which will contain 5 key elements
 *   /Type (indicating the type of data) /PP (pre-processing), /DR (dimensionality-reduction), /CL (if using
 *   supervised classification) or /RG (if using regression).
 *   Alternately, if we are using semi-supervised learning, we may only have /TR (indicating a transformation
 *   that will be effected to generate the data), in lieu of /DR. As before, /MetaInfo is purely
 *   optional.
 */

namespace spin
{
  /*! \brief A structure for parsing HDF5 model data */
  struct opdata
  {
    unsigned                    recurs;         /* Recursion level.  0=root */
    struct opdata               *prev;          /* Pointer to previous opdata */
    haddr_t                     addr;           /* Group address */
    int                         mGroup;
    int                         mDataset;
    std::string                 mLabel;
    std::vector<std::string>*   m_Datasets;
  };

  /*! \brief This function recursively searches the linked list of
   *  opdata structures for one whose address matches target_addr.
   *  Returns 1 if a match is found, and 0 otherwise.
   */
  int group_check(struct opdata *od, haddr_t target_addr)
  {
    if (od->addr == target_addr)
      return 1;       /* Addresses match */
    else
    if (!od->recurs)
      return 0;       /* Root group reached with no matches */
    else
      return group_check(od->prev, target_addr);
    /* Recursively examine the next node */
  }//end of function

  /*! \brief Operator function.
   *  This function prints the name and type of the object passed to it.  If the
   *  object is a group, it is first checked against other groups in its path using
   *  the group_check function, then if it is not a duplicate, H5Literate is called
   *  for that group.  This guarantees that the program will not enter infinite
   *  recursion due to a circular path in the file.
   */
  herr_t op_func (hid_t loc_id, const char *name, const H5L_info_t *info, void *operator_data)
  {
    herr_t          return_val = 0;
    H5O_info_t      infobuf;
    opdata* od   = static_cast<opdata*>(operator_data);
    // Can the file be opened?
    herr_t status = H5Oget_info_by_name (loc_id, name, &infobuf, H5P_DEFAULT);
    // Switch based on
    switch (infobuf.type)
    {
      case H5O_TYPE_GROUP:
        od->mGroup    = 1;
        od->mDataset  = -1;
        if (od->prev != NULL)
        {
          // This group is part of a previous group
          // So we append this to the parent group
          od->mLabel    = od->prev->mLabel + "/"+std::string(name);
        }
        else
        {
          // This is a brand new group
          od->mLabel = "/"+std::string(name);
        }
        /*
        * Check group address against linked list of operator
        * data structures.  We will always run the check, as the
        * reference count cannot be relied upon if there are
        * symbolic links, and H5Oget_info_by_name always follows
        * symbolic links.  Alternatively we could use H5Lget_info
        * and never recurse on groups discovered by symbolic
        * links, however it could still fail if an object's
        * reference count was manually manipulated with
        * H5Odecr_refcount.
        */
        if ( !group_check (od, infobuf.addr) )
        {
          /*
          * Initialize new operator data structure and
          * begin recursive iteration on the discovered
          * group.  The new opdata structure is given a
          * pointer to the current one.
          */
          struct opdata nextod;
          nextod.mGroup     = -1;
          nextod.mDataset   = -1;
          nextod.recurs     = od->recurs + 1;
          nextod.prev       = od;
          nextod.m_Datasets = od->m_Datasets;
          nextod.addr       = infobuf.addr;
          return_val        = H5Literate_by_name (loc_id, name, H5_INDEX_NAME,
                                                  H5_ITER_NATIVE, NULL, op_func,
                                                  &nextod, H5P_DEFAULT);
        }
        break;
      case H5O_TYPE_DATASET:
        // Check if this dataset if part of a previously found group
        if (od->prev != NULL)
        {
          // It is indeed part of a group
          od->mLabel = od->prev->mLabel+"/"+std::string(name);
        }
        else
        {
          // This dataset was part of the main group
          od->mLabel = std::string(name);
        }
        od->m_Datasets->push_back(od->mLabel);
        break;
      case H5O_TYPE_NAMED_DATATYPE:
        break;
      default:
        break;
    }
    return return_val;
  }//end of function

  /*! \brief LoadData
   *  This function is used to load data from a HDF5 file after
   *  template instantiation has taken place
   */
  template <class T>
  int LoadData(void* _tTags, EigenHdf5FS::FileStructure* fs, void* _data)
  {
    // b.)Typecast variables
    typedef typename std::vector<std::string> vstring;
    vstring* tTags = static_cast<vstring*>(_tTags);

    // c.) Open the file for reading
    std::shared_ptr<H5::H5File> file = std::make_shared<H5::H5File>(fs->fName.c_str(), H5F_ACC_RDONLY);

    // d.) typecast data (It should have been instantiated prior to calling this function)
    typedef typename spin::EigenDataStructure<T> DS;
    DS* data = static_cast<DS*>(_data);

    // e.) Parse the tags
    if (tTags == NULL)
    {
      int blockStart = 0;
      // This implies that all data in the file need to be parsed. We define data
      // as chunks of information that can be parsed and placed into an Eigen Matrix.
      // As such, we only consider "leaf nodes" of the HDF5 tree to be valid data.
      EigenHdf5FS::FileStructure::DataPriorMapping::iterator it = fs->dmap.begin();
      while (it != fs->dmap.end())
      {
        // split the string present in it->first. Due to our constraint, a valid tag
        // should only contain a single element after splitting
        vstring list;
        boost::split(list, it->first, boost::is_any_of("/"));
        // Read the data from the Input File
        T mDummy;
        int retVal = EigenHdf5::load<T>(*(file.get()), it->first, mDummy);
        // Check if we have read the data correctly
        if (retVal == 1)
        {
          // Keep the label name
          data->Labels.push_back(list[list.size()-1]);
          data->Size.push_back(static_cast<int>(mDummy.rows()));
          data->Block.push_back(blockStart);
          // increment blockStart
          blockStart += mDummy.rows();
          // Concatenate this matrix
          // https://forum.kde.org/viewtopic.php?f=74&t=94726
          if (data->Input.rows()>0)
          {
            T temp = data->Input;
            data->Input.resize(data->Input.rows() + mDummy.rows(), mDummy.cols());
            data->Input << temp, mDummy;
          }
          else
          {
            data->Input = mDummy;
          }

          // At this time, we also check if we need to populate the prior matrix
          if (fs->regression == true && it->second !="")
          {
            // The prior tag is available
            // Read the data from the Input File
            T mDummyP;
            int retValP = EigenHdf5::load<T>(*(file.get()), it->second, mDummyP);
            // make sure the dimensions match. If they dont' we stop parsing the file
            // any further and return to the calling point
            if (mDummyP.rows() != mDummy.rows()) return -1;

            // Check if we have read the data correctly
            if (retValP == 1)
            {
              // Concatenate this matrix
              // https://forum.kde.org/viewtopic.php?f=74&t=94726
              if (data->Prior.rows()>0)
              {
                T tempP = data->Prior;
                data->Prior.resize(data->Prior.rows() + mDummy.rows(), mDummy.cols());
                data->Prior << tempP, mDummyP;
              }
              else
              {
                data->Prior = mDummyP;
              }
            }
          }//finished populating the Prior matrix associated with this label
        }//finished reading data associated with this label

        list.clear();
        ++it;
      }//finished parsing all tags
    }//finished checking if all tags need to be parsed
    else
    {
      int blockStart = 0;
      // This implies that some data in the file need to be parsed. We define data
      // as chunks of information that can be parsed and placed into an Eigen Matrix.
      // As such, we only consider "leaf nodes" of the HDF5 tree to be valid data.
      // This implies that all data in the file need to be parsed. We define data
      // as chunks of information that can be parsed and placed into an Eigen Matrix.
      // As such, we only consider "leaf nodes" of the HDF5 tree to be valid data.
      EigenHdf5FS::FileStructure::DataPriorMapping::iterator it = fs->dmap.begin();
      while (it != fs->dmap.end())
      {
        // split the string present in it->first. Due to our constraint, a valid tag
        // should only contain a single element after splitting
        vstring list;
        boost::split(list, it->first, boost::is_any_of("/"));

        // Now, check if this is the label that the user has sought.
        // Now, check if i is present in rLabels
        vstring::iterator lt = std::find(tTags->begin(), tTags->end(), list[0]);
        if (lt != tTags->end())
        {
          // Read the data from the Input File
          T mDummy;
          int retVal = EigenHdf5::load<T>(*(file.get()), it->first, mDummy);
          // Check if we have read the data correctly
          if (retVal == 1)
          {
            // Keep the label name
            data->Labels.push_back(list[0]);
            data->Size.push_back(static_cast<int>(mDummy.rows()));
            data->Block.push_back(blockStart);
            // increment blockStart
            blockStart += mDummy.rows();
            // Concatenate this matrix
            // https://forum.kde.org/viewtopic.php?f=74&t=94726
            if (data->Input.rows()>0)
            {
              T temp = data->Input;
              data->Input.resize(data->Input.rows() + mDummy.rows(), mDummy.cols());
              data->Input << temp, mDummy;
            }
            else
            {
              data->Input = mDummy;
            }

            // At this time, we also check if we need to populate the prior matrix
            if (fs->regression == true && it->second !="")
            {
              // The prior tag is available
              // Read the data from the Input File
              T mDummyP;
              int retValP = EigenHdf5::load<T>(*(file.get()), it->second, mDummyP);
              // make sure the dimensions match. If they dont' we stop parsing the file
              // any further and return to the calling point
              if (mDummyP.rows() != mDummy.rows()) return -1;

              // Check if we have read the data correctly
              if (retValP == 1)
              {
                // Concatenate this matrix
                // https://forum.kde.org/viewtopic.php?f=74&t=94726
                if (data->Prior.rows()>0)
                {
                  T tempP = data->Prior;
                  data->Prior.resize(data->Prior.rows() + mDummy.rows(), mDummy.cols());
                  data->Prior << tempP, mDummyP;
                }
                else
                {
                  data->Prior = mDummyP;
                }
              }
            }//finished populating the Prior matrix associated with this label
          }//finished reading data associated with this label
        }//finished checking user-defined tags

        list.clear();
        ++it;
      }//finished parsing all tags
    }//finished checking if some tags need to be parsed

    // Note: This is needed else Python will continue to hold the file!
    file.get()->close();
    // Now, check if any meaningful data has been deciphered
    if (data->Labels.size() == 0) return -2;
    // Also check if the data has been properly populated
    if (data->Input.rows() <=0 ) return -3;
    // Finally, check if regression has been sought, the data and prior values
    // are correct
    if (fs->regression && (data->Input.rows() != data->Prior.rows())) return -4;
    // Ensure that the regression flag in data is set
    if (fs->regression) data->regression = true;

    return 1;
  }//end of function

  /*! \brief Default constructor */
  EigenHdf5IO::EigenHdf5IO(std::string& mFile, bool overwrite)
  {
    boost::filesystem::path dfile(mFile.c_str());
    if (!boost::filesystem::exists(dfile) || overwrite)
    {
      // create a new file.
      std::shared_ptr<H5::H5File> filePtr = std::make_shared<H5::H5File>(mFile.c_str(), H5F_ACC_TRUNC);
      // and close it
      filePtr.get()->close();
    }

    // Initialize the FileStructure object
    fs = std::make_shared<EigenHdf5FS::FileStructure>();
    // and set the filename that either needs to be
    // parsed for data or written into
    fs.get()->fName = mFile;
  }//end of function

  /*! \brief CheckDataSanity
   *  This function is used to check the sanity of the file by examining
   *  the parsed tags. If the tags do not contain "Data", then it is
   *  deemed an invalid file. In addition, if it does not contain
   *  equal #of "Prior" and "Data" sub-tags (i.e., all contents)
   *  in these two groups should be equal, then we flag the file as
   *  invalid for "regression/semi-supervised learning".
   */
  int EigenHdf5IO::CheckDataSanity()
  {
    std::string tag0= "/";
    std::string tag1a = "Data";
    std::string tag2a = "Prior";
    std::string tag1 = tag0+tag1a+tag0;
    std::string tag2 = tag0+tag2a+tag0;
    typedef typename std::vector<std::string> vstring;

    // sanity check
    if ( (int)fs.get()->tags.size() < 0 ) return -1;

    // We traverse through all the tags that have been parsed. We check if /Data is present
    // in the tags. If not, then the file is deemed unsuitable. If this check is passed then
    // a second check if made to see if /Prior information is available for the particular
    // tag.
    bool containsTag1 = false;
    bool containsTag2 = false;
    for (auto i: fs.get()->tags)
    {
      bool v1 = boost::algorithm::contains(i, tag1);
      bool v2 = boost::algorithm::contains(i, tag2);
      if (v1)
      {
        containsTag1 = true;
        // In addition, we place it in a map
        fs.get()->dmap[i] = "";
      }
      else
      if (v2)
      {
        containsTag2 = true;
        // This is a prior tag. We need to check if a corresonding data tag exists.
        // If it does, we populate it
        vstring list;
        boost::split(list, i, boost::is_any_of("/"));
        // replace list[0] with tag1a
        list[1] = tag1a;
        // Join the string
        std::string pTag = boost::algorithm::join(list, "/");
        // Now, check if pTag is available in dMap
        EigenHdf5FS::FileStructure::DataPriorMapping::iterator it = fs.get()->dmap.find(pTag);
        if (it != fs.get()->dmap.end())
        {
          // this tag exists. So we can place the prior tag also
          it->second = i;
        }
        list.clear();
      }
    }

    // Check if /Data is present in the file. If not then this file is unsuitable
    if (!containsTag1) return -2;

    // Check if /Prior is present in the file
    if (containsTag2)
    {
      // This file can be used for regression/SSL
      fs.get()->regression = true;
    }

    return 1;
  }//end of function

  /*! \brief ParseTags()
   * This is the driver function for parsing a model file. Based on the paremeters
   * used to generate the model, this function will decipher the relevant modules
   * that are present in the pipeline.
   * @data  : If sanity is being checked for data or for model
   */
  int EigenHdf5IO::ParseTags(bool data)
  {
    // First check if the filestructure has been formed
    if (!fs.get()) return -1;

    H5O_info_t      infobuf;
    struct opdata   od;

    /*
     * Open file and initialize the operator data structure.
     */
    hid_t file      = H5Fopen(fs.get()->fName.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    herr_t statusP  = H5Oget_info (file, &infobuf);

    if (statusP != 0)
    {
      return -2;
    }

    od.recurs     = 0;
    od.mGroup     = -1;
    od.mDataset   = -1;
    od.prev       = NULL;
    od.addr       = infobuf.addr;
    od.m_Datasets = &fs.get()->tags;
    /*
     * Recurse through the file and get all tags in the file
     */
    statusP = H5Literate ( file, H5_INDEX_NAME, H5_ITER_NATIVE, NULL, op_func, &od);
    if (fs.get()->tags.size() == 0)
    {
      return -3;
    }
    // Note: This is needed else Python will continue to hold the file!
    H5Fclose(file);

    // At this time, we check the sanity of the data file.
    // We should find the group "Data". Next we search for the "Prior" group.
    // If we also find that then it implies that his data can be used for
    // regression or semi-supervised learning also. If that is the case, then,
    // we need to check if the tags within each group match. However, if
    // the file does not contain "Data" then it is flagged as an unsuitable
    // file.
    if (data)
    {
      int v = CheckDataSanity();
      if (v != 1) return -4;
    }
    else
    {
      int v = CheckModelSanity();
      if (v < 0) return -5;
    }

    // Tags successfully deciphered
    return 1;
  }//end of function

  /*! \brief ReadData
   *  This function is used to parse a Hdf5 file containing labelled data and
   *  return an eigen matrix. A data structure holding data as well as other
   *  information is also returned. Depending on the user's choice, various
   *  parameters in the data structure are populated.
   *  @_tTags         : A vector of tags that need to be read. This is specified
   *                    by the user. If this is NULL, then all tags are read.
   */
  int EigenHdf5IO::ReadData(void* _tTags, int type, void* _data)
  {
    // a.) We check if the file is well formed by parsing the tags
    int v = ParseTags(true);
    if (v !=1) return v;

    switch (type)
    {
      case 1:
        v = LoadData<RMMatrix_Float>(_tTags, fs.get(), _data);
        break;
      case 2:
        v = LoadData<RMMatrix_Double>(_tTags, fs.get(), _data);
        break;
      case 3:
        v = LoadData<CMMatrix_Float>(_tTags, fs.get(), _data);
        break;
      case 4:
        v = LoadData<CMMatrix_Double>(_tTags, fs.get(), _data);
        break;
      default:
        v = -2;
        break;
    }

    return v;
  }//end of function

  /*! \brief CreateGroup
   *  This function is used to create a group in the HDF5 file if it does not exist already
   *  @group      : The group under which data will be stored in the HDF5 file
   */
  int EigenHdf5IO::CreateGroup(std::string& group)
  {
    // Do a sanity check.
    boost::filesystem::path dfile(fs.get()->fName.c_str());
    if (!boost::filesystem::exists(dfile))
    {
      return -1; // the file does not exis
    }

    if (!fs.get()) return -2;
    if (fs.get()->fName == "") return -3;

    // check if the group name is not empty. If so we add a default group name
    if (group == "") group = "/";

    // Also check if the group previously exists. If not only then create the group
    std::shared_ptr<H5::H5File> filePtr = std::make_shared<H5::H5File>(fs.get()->fName.c_str(), H5F_ACC_RDWR);
    H5::Group grp;
    try
    {
      // Here we try to open the group
      grp=filePtr.get()->openGroup(group.c_str());
    }
    catch(H5::Exception& e)
    {
      // Here we create a group only if the group does not exist!! (Yikes)
      grp = filePtr.get()->createGroup(group.c_str());
    }

    // close the file
    filePtr.get()->close();

    return 1;
  }//end of function

  /*! \brief WriteData
   *  This function is used to write data into a preexisting group. If the group
   *  does not exist, we return an error
   *  @group      : The name of the attribute where the data will be written
   *  @type       : The data type that we are writing
   *  @_data      : The actual data that needs to be typecast.
   */
  int EigenHdf5IO::WriteData(std::string& group, int type, void* _data)
  {
    // Do a sanity check.
    boost::filesystem::path dfile(fs.get()->fName.c_str());
    if (!boost::filesystem::exists(dfile))
    {
      return -1; // the file does not exis
    }

    if (!fs.get()) return -2;
    if (fs.get()->fName == "") return -3;

    // Open the file for writing
    std::shared_ptr<H5::H5File> filePtr = std::make_shared<H5::H5File>(fs.get()->fName.c_str(), H5F_ACC_RDWR);
    // Check if the group exists.
    if(H5Lexists(filePtr.get()->getId(), group.data(), H5P_DEFAULT) == true)
    {
      // As the group exists, we delete it
      int result = H5Ldelete(filePtr.get()->getId(), group.data(), H5P_DEFAULT);
    }

    switch (type)
    {
      case 1:
      {
        // Typecast the data
        RMMatrix_Float* data = static_cast<RMMatrix_Float*>(_data);
        // Save the group in the file
        EigenHdf5::save( *(filePtr.get()), group, *data);
      }
      break;
      case 2:
      {
        // Typecast the data
        RMMatrix_Double* data = static_cast<RMMatrix_Double*>(_data);
        // Save the group in the file
        EigenHdf5::save(*(filePtr.get()), group, *data);
      }
      break;
      case 3:
      {
        // Typecast the data
        CMMatrix_Float* data = static_cast<CMMatrix_Float*>(_data);
        // Save the group in the file
        EigenHdf5::save(*(filePtr.get()), group, *data);
      }
      break;
      case 4:
      {
        // Typecast the data
        CMMatrix_Double* data = static_cast<CMMatrix_Double*>(_data);
        // Save the group in the file
        EigenHdf5::save(*(filePtr.get()), group, *data);
      }
      break;
      default:
      {
        // close the file
        filePtr.get()->close();
        return -4;
      }
      break;
    }

    // close the file
    filePtr.get()->close();

    return 1;
  }//end of function

  /////////////////////////////////////////////////////////////////////////////
  /*! \brief WriteModel
   *  This function is used to save a model that was previously serialized
   */
  int WriteModel(std::string& m_ModelFile, void* _modelOutput)
  {
    // We assume that the file exists and it has been created with a /Model group
    std::shared_ptr<H5::H5File> file = std::make_shared<H5::H5File>(m_ModelFile.c_str(), H5F_ACC_RDWR);

    // Typecast the _modelOutput
    SerializedModel* modelOutput = static_cast<SerializedModel*>(_modelOutput);

    // 1.) First, we need to save the data type tag. If it is invalid, nothing else should
    //     be saved.
    if (modelOutput->type.data() || modelOutput->type.size() ==1)
    {
      // Save the matrix to the file
      int retVal = EigenHdf5::save(*(file.get()), "/Model/Type", modelOutput->type);
      if (retVal != 1) return -2;

      // 2.) Check for pre-processing tag
      if (modelOutput->pp.data())
      {
        int retVal = EigenHdf5::save(*(file.get()), "/Model/PP", modelOutput->pp);
        if (retVal != 1) return -3;
      }

      // 3.) Check for dimensionality reduction tag
      if (modelOutput->dr.data())
      {
        int retVal = EigenHdf5::save(*(file.get()), "/Model/DR", modelOutput->dr);
        if (retVal != 1) return -4;
      }

      // 4.) Check for either classification, regression or SSL
      if (modelOutput->cl.data() && (!modelOutput->rg.data()))
      {
        int retVal = EigenHdf5::save(*(file.get()), "/Model/CL", modelOutput->cl);
        if (retVal != 1) return -5;
      }
      else
      if (!(modelOutput->cl.data()) && modelOutput->rg.data())
      {
        // Add the 1 to account for end of line
        int retVal = EigenHdf5::save(*(file.get()), "/Model/RG", modelOutput->rg);
        if (retVal != 1) return -6;
      }
      // 5.) Check for meta information
      if (modelOutput->meta.data())
      {
        int retVal = EigenHdf5::save(*(file.get()), "/Model/Meta", modelOutput->meta);
        if (retVal != 1) return -8;
      }

      return retVal;
    }

    // should not come here
    return -1;
  }//end of function

  /*! \brief LoadModel
   *  This function is used to load model parameters from a HDF5 file. Essentially,
   *  all model parameters are stored in serialized form as char buffer
   *  arrays.
   */
  int LoadModel(std::string& m_ModelFile, void* _modelOutput,
                std::vector<std::string>* inputTags,
                std::vector<std::string>* targetTags)
  {
    // Check if the file exists
    boost::filesystem::path dfile(m_ModelFile.c_str());
    if (!boost::filesystem::exists(dfile) ) return -1;

    // Open the file for reading
    std::shared_ptr<H5::H5File> file = std::make_shared<H5::H5File>(m_ModelFile.c_str(), H5F_ACC_RDONLY);

    // Typecast the _modelOutput
    SerializedModel* modelOutput = static_cast<SerializedModel*>(_modelOutput);

    //std::cout<<"fine 1"<<std::endl;
    // 1.) First, we need to check the data type tag
    int retVal = EigenHdf5::load<RMMatrix_Char>(*(file.get()), "/Model/Type", modelOutput->type);
    if (retVal != 1) return -1;

    // Parse through other tags and populate relevant entries in the model
    for (auto i: *inputTags)
    {
      for (auto j: *targetTags)
      {
        bool v1 = boost::algorithm::contains(i, j);
        if (v1)
        {
          if (j=="/Model/PP")
          {
            // 2.) Check for pre-processing tag
            retVal = EigenHdf5::load<RMMatrix_Char>(*(file.get()), "/Model/PP", modelOutput->pp);
            if (retVal != 1) return -2;
            break;
          }
          else
          if (j=="/Model/DR")
          {
            // 2.) Check for pre-processing tag
            retVal = EigenHdf5::load<RMMatrix_Char>(*(file.get()), "/Model/DR", modelOutput->dr);
            //std::cout<<modelOutput->dr.rows()<<" "<<modelOutput->dr.cols()<<std::endl;
            if (retVal != 1) return -3;
            break;
          }
          else
          if (j=="/Model/CL")
          {
            // 2.) Check for pre-processing tag
            retVal = EigenHdf5::load<RMMatrix_Char>(*(file.get()), "/Model/CL", modelOutput->cl);
            if (retVal != 1) return -4;
            break;
          }
          else
          if (j=="/Model/RG")
          {
            // 2.) Check for pre-processing tag
            retVal = EigenHdf5::load<RMMatrix_Char>(*(file.get()), "/Model/RG", modelOutput->rg);
            if (retVal != 1) return -5;
            break;
          }
          else
          if (j=="/Model/Meta")
          {
            // 2.) Check for pre-processing tag
            retVal = EigenHdf5::load<RMMatrix_Char>(*(file.get()), "/Model/Meta", modelOutput->meta);
            if (retVal != 1) return -6;
            break;
          }
        }//a valid tag
      }//finished checking all tags
    }//finished comparing with valid tags

    file.get()->close();
    return 1;
  }//end of function

  /*! \brief Default constructor during prediction*/
  EigenHdf5IO::EigenHdf5IO()
  {
  }//end of function

  /*! \brief CheckModelSanity
   *  This function is used to check the sanity of the file by examining
   *  the parsed tags. If the tags do not contain "Model", and associated
   *  tags like Type, DR, PP, CL, or SSL, then , the file is deemed
   *  unsuitable for prediction/learning.
   */
  int EigenHdf5IO::CheckModelSanity()
  {
    std::string tag0= "/Model";
    std::vector<std::string> tags ={"/Type","/PP","/DR","/CL","/RG"};
    std::vector<bool> cTags = {false,false,false,false,false,false};

    // sanity check
    if ( (int)fs.get()->tags.size() < 0 ) return -1;

    // We traverse through all the tags that have been parsed. We check if /Data is present
    // in the tags. If not, then the file is deemed unsuitable. If this check is passed then
    // a second check if made to see if /Prior information is available for the particular
    // tag.
    auto count=0;
    for (auto i: fs.get()->tags)
    {
      for (auto j: tags)
      {
        bool v1 = boost::algorithm::contains(i, j);
        if (v1)
        {
          cTags[count] = true;
          // In addition, we place it in a map
          fs.get()->dmap[i] = "";
          break;
        }
      }
      ++count;
    }

    // If no data type has been specified we return it invalid
    int retVal = -3;

    if (!cTags[0])
    {
      retVal = -2;
    }
    else
    // Depending on /CL or /RG beign returned we deem it either
    // a classification or regression model.
    if (cTags[3] && !cTags[4])
    {
      retVal = 2; // classification model
    }
    else
    if (!cTags[3]&& cTags[4])
    {
      retVal = 3; // Regression model
    }
    else
    if (cTags[1] || cTags[2])
    {
      retVal = 4; // Either PP or DR or both
    }

    // Free memory
    cTags.clear();
    tags.clear();

    // Return the value
    return retVal;
  }//end of function

  /*! \brief ReadModel
   *  This function is used to parse a Hdf5 file containing a model.
   *  @model    : The file containing the model
   *  @_data    : The data object
   */
  int EigenHdf5IO::ReadModel(std::string& model, void* _data)
  {
    // a.) We check if the file is well formed by parsing the tags
    int v = ParseTags(false);
    if (v !=1) return -1;

    // All tags possible in a model
    std::vector<std::string> tags ={"/Model/PP",\
                                    "/Model/DR",\
                                    "/Model/CL",\
                                    "/Model/RG",\
                                    "/Model/SSL"};

    // b.) Load the model
    v = LoadModel(model, _data, &fs.get()->tags, &tags);
    tags.clear();
    if (v !=1) return -2;

    return 1;
  }//end of function

  /*! \brief ReadModel
   *  This function is used to parse a Hdf5 file containing a model.
   *  @model    : The file containing the model
   *  @_data    : The data object
   */
  int EigenHdf5IO::SaveModel(std::string& model, void* _data)
  {
    // Do a sanity check
    boost::filesystem::path dfile(model.c_str());
    if (!boost::filesystem::exists(dfile))
    {
      return -1; // the file does not exis
    }

    // Save the model
    int v = WriteModel(model, _data);
    if (v !=1) return -1;

    return 1;
  }//end of function

}//end of namespace
