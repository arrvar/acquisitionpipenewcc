#ifndef MODELIO_H_
#define MODELIO_H_
#include <string>
#include <memory>

namespace spin
{
  struct SerializedModel;
  class EigenHdf5IO;
  /*! \brief ModelIO
   *  This class is a common placeholder for all aspects related
   *  to saving and loading a model from a HDF5 file
   */
  class ModelIO
  {
    public:
      /*! \brief Default Constructor */
      ModelIO(){}

      /*! \brief Default Destructor */
      ~ModelIO(){}

      /*! \brief Initialize */
      int Initialize(std::string& modelFile, bool f, int type=1);

      /*! \brief Deserialize */
      int Deserialize(const char* tag, void* obj);

      /*! \brief Serialize */
      int Serialize(const char* tag, void* obj);

      /*! \brief Read Model */
      int ReadModel(std::string& model, void* _data);

      /*! \brief Save Model */
      int SaveModel(std::string& model);
    private:
      std::shared_ptr<SerializedModel> modelObj;
      std::shared_ptr<EigenHdf5IO> hdfIO;
  };//end of class
}//end of namespace
#endif
