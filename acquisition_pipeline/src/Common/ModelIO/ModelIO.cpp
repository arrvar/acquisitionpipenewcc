#include "ModelIO/ModelIO.h"
#include "ModelIO/SerializedModel.h"
#include "DataIO/EigenHdf5IO.h"
#include <sstream>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/stream_buffer.hpp>
#include <boost/iostreams/device/back_inserter.hpp>

// All objects that can be serialized into a model
#include "AbstractClasses/DRObject.h"

namespace spin
{
  enum string_code
  {
    PP=0,
    DR=1,
    CL=2,
    RG=3,
    SSL=4,
    META=5
  };//end of enum

  string_code hashit (std::string const& inString)
  {
    if (inString == "PP") return string_code::PP;
    else
    if (inString == "DR") return string_code::DR;
    else
    if (inString == "CL") return string_code::CL;
    else
    if (inString == "RG") return string_code::RG;
    else
    if (inString == "SSL") return string_code::SSL;

    // Default
    return string_code::META;
  }//end of function

  /*! \brief ComponentSerialization
   *  This function is used to serialize a component.
   *  @tag      : What type of component are we serializing
   *  @obj      : the object associated with this component
   *  @model    : The serialized model holding this component
   */
  template <class Matrix>
  int ComponentSerialization(const char* tag, void* obj, void* _model)
  {
    // Typecast the model object
    SerializedModel* model = static_cast<SerializedModel*>(_model);

    //https://stackoverflow.com/questions/3015582/direct-boost-serialization-to-char-array
    std::string pTag = std::string(tag);
    std::string serial_str;
    boost::iostreams::back_insert_device<std::string> inserter(serial_str);
    boost::iostreams::stream<boost::iostreams::back_insert_device<std::string> > s(inserter);
    boost::archive::binary_oarchive oa(s);

    // Based on the tag, decipher the seralization sought
    switch(hashit(pTag))
    {
      case (string_code::DR): // dimensionality reduction
      {
        // Serialize the object
        DRObject<Matrix>* dobj = static_cast<DRObject<Matrix>*>(obj);
        if (!dobj) return -2;
        // write class instance to archive
        oa << (*dobj);
        s.flush();
        // Save the serialized data in model
        model->dr = Eigen::Map<RMMatrix_Char>(const_cast<char*>(serial_str.c_str()),1,serial_str.length());
      }
      break;
      case (string_code::PP): // Pre-processing
      {
        /*
        // Serialize the object
        PPObject<Matrix>* dobj = static_cast<PPObject<Matrix>*>(obj);
        if (!dobj) return -2;
        // write class instance to archive
        oa << (*dobj);
        s.flush()
        // Save the serialized data in model
        model->pp = Eigen::Map<RMMatrix_Char>(const_cast<char*>(serial_str.c_str(),1,serial_str.length());
        */
      }
      break;
      case (string_code::CL): // Classification
      {
        /*
        // Serialize the object
        CLObject<Matrix>* dobj = static_cast<CLObject<Matrix>*>(obj);
        if (!dobj) return -2;
        // write class instance to archive
        oa << (*dobj);
        s.flush();
        // Save the serialized data in model
        model->cl = Eigen::Map<RMMatrix_Char>(const_cast<char*>(serial_str.c_str(),1,serial_str.length());
        */
      }
      break;
      case (string_code::RG): // Regression
      {
        /*
        // Serialize the object
        RGObject<Matrix>* dobj = static_cast<RGObject<Matrix>*>(obj);
        if (!dobj) return -2;
        // write class instance to archive
        oa << (*dobj);
        s.flush();
        // Save the serialized data in model
        model->rg = Eigen::Map<RMMatrix_Char>(const_cast<char*>(serial_str.c_str(),1,serial_str.length());
        */
      }
      break;
      case (string_code::META): // Meta information
      {
        /*
        // Serialize the object
        MetaObject<Matrix>* dobj = static_cast<MetaObject<Matrix>*>(obj);
        if (!dobj) return -2;
        // write class instance to archive
        oa << (*dobj);
        s.flush();
        // Save the serialized data in model
        model->meta = Eigen::Map<RMMatrix_Char>(const_cast<char*>(serial_str.c_str(),1,serial_str.length());
        */
      }
      break;
      default: // Meta information
      {
        /*
        // Serialize the object
        MetaObject<Matrix>* dobj = static_cast<MetaObject<Matrix>*>(obj);
        if (!dobj) return -2;
        // write class instance to archive
        oa << (*dobj);
        s.flush();
        // Save the serialized data in model
        model->meta = Eigen::Map<RMMatrix_Char>(const_cast<char*>(serial_str.c_str(),1,serial_str.length());
        */
      }
    }
    return 1;
  }//end of function

    /*! \brief ComponentDeserialization
   *  This function is used to deserialize a component.
   *  @tag      : What type of component are we serializing
   *  @obj      : the object associated with this component
   *  @model    : The serialized model holding this component
   */
  template <class Matrix>
  int ComponentDeserialization(const char* tag, void* obj, void* _model)
  {
    // Typecast the model object
    SerializedModel* model = static_cast<SerializedModel*>(_model);

    //https://stackoverflow.com/questions/3015582/direct-boost-serialization-to-char-array
    std::string pTag = std::string(tag);

    // Based on the tag, decipher the seralization sought
    switch(hashit(pTag))
    {
      case (string_code::DR): // dimensionality reduction
      {
        // Downcast the object
        DRObject<Matrix>* dobj = static_cast<DRObject<Matrix>*>(obj);
        if (!dobj) return -2;

        boost::iostreams::basic_array_source<char> device(model->dr.data(), model->dr.rows()*model->dr.cols());
        boost::iostreams::stream<boost::iostreams::basic_array_source<char> > s(device);
        boost::archive::binary_iarchive ia(s);


        // And deserialize the string to an object
        ia >> (*dobj);
      }
      break;
      case (string_code::PP): // Pre-processing
      {
        /*
        // Downcast the object
        PPObject<Matrix>* dobj = static_cast<PPObject<Matrix>*>(obj);
        if (!dobj) return -2;

        boost::iostreams::basic_array_source<char> device(model->pp.data(), model->pp.rows() * model->pp.cols());
        boost::iostreams::stream<boost::iostreams::basic_array_source<char> > s(device);
        boost::archive::binary_iarchive ia(s);
        // And deserialize the string to an object
        ia >> (*dobj);
        */
      }
      break;
      case (string_code::CL): // Classification
      {
        /*
        // Downcast the object
        CLObject<Matrix>* dobj = static_cast<CLObject<Matrix>*>(obj);
        if (!dobj) return -2;

        boost::iostreams::basic_array_source<char> device(model->cl.data(), model->cl.rows() * model->cl.cols());
        boost::iostreams::stream<boost::iostreams::basic_array_source<char> > s(device);
        boost::archive::binary_iarchive ia(s);
        // And deserialize the string to an object
        ia >> (*dobj);
        */
      }
      break;
      case (string_code::RG): // Regression
      {
        /*
        // Downcast the object
        RGObject<Matrix>* dobj = static_cast<RGObject<Matrix>*>(obj);
        if (!dobj) return -2;

        boost::iostreams::basic_array_source<char> device(model->rg.data(), model->rg.rows()*model->rg.cols());
        boost::iostreams::stream<boost::iostreams::basic_array_source<char> > s(device);
        boost::archive::binary_iarchive ia(s);
        // And deserialize the string to an object
        ia >> (*dobj);
        */
      }
      break;
      case (string_code::META): // Meta information
      {
        /*
        // Downcast the object
        MetaObject<Matrix>* dobj = static_cast<MetaObject<Matrix>*>(obj);
        if (!dobj) return -2;

        boost::iostreams::basic_array_source<char> device(model->meta.data(), model->meta.rows()*model->meta.cols());
        boost::iostreams::stream<boost::iostreams::basic_array_source<char> > s(device);
        boost::archive::binary_iarchive ia(s);
        // And deserialize the string to an object
        ia >> (*dobj);
        */
      }
      break;
      default:
      {
        /*
        // Downcast the object
        MetaObject<Matrix>* dobj = static_cast<MetaObject<Matrix>*>(obj);
        if (!dobj) return -2;

        boost::iostreams::basic_array_source<char> device(model->meta.data(), model->meta.rows()*model->meta.cols());
        boost::iostreams::stream<boost::iostreams::basic_array_source<char> > s(device);
        boost::archive::binary_iarchive ia(s);
        // And deserialize the string to an object
        ia >> (*dobj);
        */
      }
    }
    return 1;
  }//end of function

  /*! \brief Serialize
   *  This function is used to dump the data from an object into a file.
   *  It is assumed that the file has been opened prior to calling
   *  this function. Furthermore, if any pre-existing tag is present it will be
   *  overwritten.
   *  @tag      : The component object that is beiing serialized
   *  @obj      : The object that is holding the component to be serialized
   */
  int ModelIO::Serialize(const char* tag, void* obj)
  {
    if (!tag) return -1;
    int h = -1;

    // typecast model
    SerializedModel* model = modelObj.get();
    if (!model) return -2;

    // Serialize the component based on the data type
    int type = static_cast<int>(model->type(0,0));
    switch(type)
    {
      case 1:
      {
        h = ComponentSerialization<RMMatrix_Float>(tag, obj, model);
        if (h !=1) return -3;
      }
      break;
      case 2:
      {
        h = ComponentSerialization<RMMatrix_Double>(tag, obj, model);
        if (h !=1) return -3;
      }
      break;
      case 3:
      {
        h = ComponentSerialization<CMMatrix_Float>(tag, obj, model);
        if (h !=1) return -3;
      }
      break;
      case 4:
      {
        h = ComponentSerialization<CMMatrix_Double>(tag, obj, model);
        if (h !=1) return -3;
      }
      break;
      default:
      {
        return -4;
      }
      break;
    }

    return 1;
  }//end of function

  /*! \brief Deserialize
   *  This function is used to deserialize a previously parsed model
   *  into its component object.
   *  @tag      : The component object that is beiing deserialized
   *  @obj      : The object that will the component after deserialization
   *  @_model   : The model object that is holding the serialized data
   */
  int ModelIO::Deserialize(const char* tag, void* obj)
  {
    if (!tag) return -1;
    int h = -1;

    // Get a pointer to the model
    SerializedModel* model = modelObj.get();
    if (!model) return -2;

    // Serialize the component based on the data type
    int type = static_cast<int>(model->type(0,0));
    switch(type)
    {
      case 1:
      {
        h = ComponentDeserialization<RMMatrix_Float>(tag, obj, model);
        if (h !=1) return -3;
      }
      break;
      case 2:
      {
        h = ComponentDeserialization<RMMatrix_Double>(tag, obj, model);
        if (h !=1) return -3;
      }
      break;
      case 3:
      {
        h = ComponentDeserialization<CMMatrix_Float>(tag, obj, model);
        if (h !=1) return -3;
      }
      break;
      case 4:
      {
        h = ComponentDeserialization<CMMatrix_Double>(tag, obj, model);
        if (h !=1) return -3;
      }
      break;
      default:
      {
        return -4;
      }
      break;
    }

    return 1;
  }//end of function

  /*! \brief Initialize
   *  This function initializes a model, either for writing or reading
   *  based on the boolean flag f
   */
  int ModelIO::Initialize(std::string& modelFile, bool f, int type)
  {
    int h = 1;

    // Initialize a model object
    modelObj = std::make_shared<SerializedModel>();

    if (f)
    {
      // This is the training phase wherein we need to save a model
      hdfIO = std::make_shared<EigenHdf5IO>(modelFile, true);
      std::string tGroup = "Model";
      hdfIO.get()->CreateGroup(tGroup);
      modelObj.get()->type = RMMatrix_Char::Zero(1,1);
      modelObj.get()->type(0,0) = (char)type;
    }
    else
    {
      // Instantiate a hdf5IO reader
      hdfIO = std::make_shared<EigenHdf5IO>(modelFile, false);
      // and load the model
      h = hdfIO.get()->ReadModel(modelFile, modelObj.get());
    }

    return h;
  }//end of function

  /*! \brief SaveModel
   *  This function saves a model to a specified file
   */
  int ModelIO::SaveModel(std::string& modelFile)
  {
    int h = hdfIO.get()->SaveModel(modelFile, modelObj.get());
    return h;
  }
}//end of namespace
