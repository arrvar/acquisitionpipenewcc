#ifndef SERIALIZEDMODEL_H_
#define SERIALIZEDMODEL_H_
#include <string>
#include <memory>
#include "AbstractClasses/AbstractMatrixTypes.h"
namespace spin
{
  /*! \brief A structure to hold various components associated with
   *  a model of 1D data. Currently, we support the following:
   *  Pre-processing,
   *  Dimensionality reduction,
   *  Classification,
   *  Regression,
   *  Semi-supervised learning
   *  Not all components are needed for a model. Each object is
   *  essentially a serialized string that is saved to this
   *  structure.
   *  In addition, a model may have some meta information associated with
   *  it. We also provide means to save this meta information.
   */
  struct SerializedModel
  {
    /*! \brief Dimensionality reduction */
    RMMatrix_Char dr;
    /*! \brief Pre-processing */
    RMMatrix_Char pp;
    /*! \brief classification */
    RMMatrix_Char cl;
    /*! \brief regression */
    RMMatrix_Char rg;
    /*! \brief Meta information */
    RMMatrix_Char meta;
    /*! \brief Data type */
    RMMatrix_Char type;

  };//end of struct
}//end of namespace
#endif
