#ifdef USE_OPENCL
#include "Framework/GPU/GPUStruct.h"

namespace spin
{
	/*! \brief GPUObject
	 *  A data structure that is created for a specified device
	 */
  struct GPUObject
  {
    GPUObject(){}
    cl::Context context;
    cl::CommandQueue   queue;
    std::map<std::string, cl::Program> programs;
    std::vector<cl::Platform> platforms;
    cl::Device device;
  };

  /*! \brief BuildProgram
	 *  A stand alone function to build a kernel for a specified
	 *  device and context.
	 *  @k        : The kernel name as a string.
	 *  @p        : The program where the compiled kernel will be saved
	 *  @c        : The device on which the program will run
	 *  @options  : Any pre-processor definitions
	 */
  cl_int BuildProgram(std::string& k, \
											cl::Program* p, \
                      cl::Context c,\
                      std::string& options)
  {
    cl_int err;
    // an std::vector of string
#ifdef CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY
    cl::Program::Sources sources(1, std::make_pair(k.c_str(), strlen(k.c_str())));
#else
    cl::Program::Sources sources(1, k);
#endif
    *p = cl::Program(c, sources, &err);
    if (err != CL_SUCCESS)
    {
      return err;
    }
    err = p->build(options.c_str());
    if (err != CL_SUCCESS)
    {
      return err;
    }
    return CL_SUCCESS;
  }//end of function

  // Default constructor if creating a new object
  GPUStruct::GPUStruct()
  {
    // Initialize a new gpuobject
    this->gpuobject = std::make_shared<GPUObject>();
    // And get all platforms
    cl::Platform::get(&this->gpuobject->platforms);
  }//end of function

  // Default constructor if making a copy of an existing object
  GPUStruct::GPUStruct(GPUStruct* p)
  {
    if (p == nullptr) return;
    // Initialize a new gpuobject
    this->gpuobject = std::make_shared<GPUObject>();

    // Copy information
    this->gpuobject->context   = *(p->GetContext());
    this->gpuobject->queue     = *(p->GetQueue());
    this->gpuobject->platforms = p->GetPlatforms();
    this->gpuobject->device    = p->GetDevice();
    this->gpuobject->programs  = *(p->GetPrograms());
  }//end of function

  // Convenience function (context)
  const cl::Context* GPUStruct::GetContext()
  {
    return &(this->gpuobject->context);
  }//end of function

  // Convenience function ( queue )
  const cl::CommandQueue* GPUStruct::GetQueue()
  {
    return &(this->gpuobject->queue);
  }//end of function

  // Set a queue given a device
  int GPUStruct::SetQueue(cl::Device device)
  {
    // Create a command queue using the selected device on this platform
    cl_int err;
    this->gpuobject->queue = cl::CommandQueue(this->gpuobject->context, device, 0, &err);
    if (err != CL_SUCCESS)
    {
      return err;
    }
    this->gpuobject->device = device;
    return 1;
  }//end of function

  // Convenience function ( program )
  cl::Program* GPUStruct::GetProgram(std::string k)
  {
    std::map<std::string, cl::Program>::iterator it = this->gpuobject->programs.find(k);
    if (it != this->gpuobject->programs.end())
    {
      return &(it->second);
    }
    return NULL;
  }//end of function

  // Convenience function ( programs )
  std::map<std::string, cl::Program>* GPUStruct::GetPrograms()
  {
    return &(this->gpuobject->programs);
  }//end of function

  // Get Platforms
  std::vector<cl::Platform> GPUStruct::GetPlatforms()
  {
    return this->gpuobject->platforms;
  }//end of function

  //Get Device
  cl::Device GPUStruct::GetDevice()
  {
    return this->gpuobject->device;
  }//end of function

  //Get Device
  std::vector<cl::Device> GPUStruct::GetAllDevicesInPlatform(cl::Platform p)
  {
    std::vector<cl::Device> devices;
    p.getDevices(CL_DEVICE_TYPE_ALL, &devices);
    return devices;
  }//end of function

  // SetContext (GPU)
  int GPUStruct::SetContext_GPU(cl::Platform p)
  {
    // Select the platform of choice
    cl_context_properties cps[3] =
    {
      CL_CONTEXT_PLATFORM,
      (cl_context_properties)(p)(),
      0
    };

    // Create a context on this platform
    cl_int err;
    this->gpuobject->context = cl::Context (CL_DEVICE_TYPE_GPU, \
                                            cps, \
                                            NULL, \
                                            NULL, \
                                            &err);
    if (err != CL_SUCCESS)
    {
      return err;
    }
    return 1;
  }//end of function

  // Get all GPU devices in this platform
  std::vector<cl::Device> GPUStruct::GetGPUInPlatform(cl::Platform p)
  {
    std::vector<cl::Device> devices;
    p.getDevices(CL_DEVICE_TYPE_GPU, &devices);
    return devices;
  }//end of function

	/*! \brief BuildProgram
	 *  This function is used to build an opencl kernel.
	 *  @kernelList    : A string containing all kernels that need to be built.
	 */
  int GPUStruct::BuildProgram(std::string& kernelList, \
                              std::string& keyName, \
                              std::string& options, \
                              std::string& errLog)
	{
		cl_int err;

		err = spin::BuildProgram( kernelList, \
                              &this->gpuobject->programs[keyName],\
															this->gpuobject->context,\
															options);
		if (err != CL_SUCCESS)
		{
			//https://stackoverflow.com/questions/34662333/opencl-how-to-check-for-build-errors-using-the-c-wrapper
      errLog = this->gpuobject->programs[keyName].getBuildInfo<CL_PROGRAM_BUILD_LOG>(this->gpuobject->device);
			return -1;
		}
		return 1;
	}//end of function

}//end of namespace
#endif
