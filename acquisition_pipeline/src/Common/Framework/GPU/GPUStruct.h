#ifdef USE_OPENCL
#ifndef GPUSTRUCT_H
#define GPUSTRUCT_H
#include <CL/cl2.hpp>
#include <string>
#include <memory>
#include <vector>
#include <map>

namespace spin
{
  // Forward declaration
  struct GPUObject;

  class GPUStruct
  {
    public:
      //Default constructors
      GPUStruct();

      //Copy constructor
      GPUStruct(GPUStruct* p);

      // Build Context
      int SetContext_GPU(cl::Platform p);

      // Get Context
      const cl::Context* GetContext();

      // Build Queue
      int SetQueue();

      // Get Queue
      const cl::CommandQueue* GetQueue();

      // SetProgram
      int BuildProgram(std::string& k, std::string& l, \
                       std::string& p, std::string& e);

      // GetProgram
      cl::Program* GetProgram(std::string k);

      // GetPrograms
      std::map<std::string,cl::Program>* GetPrograms();

      // Get platforms
      std::vector<cl::Platform> GetPlatforms();

      // Set Queue
      int SetQueue(cl::Device device);

      //Get Device
      cl::Device GetDevice();

      //Get Devices
      std::vector<cl::Device> GetAllDevicesInPlatform(cl::Platform p);

      // Get GPU In platform
      std::vector<cl::Device> GetGPUInPlatform(cl::Platform p);

    private:
      std::shared_ptr<GPUObject> gpuobject;

  };//end of class
}//end of namespace
#endif // GPUSTRUCT_H
#endif
