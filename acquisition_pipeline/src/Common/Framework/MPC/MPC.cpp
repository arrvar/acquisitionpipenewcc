#include "Framework/MPC/MPC.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  /*! \brief AddToQueue
   *  Adds the data structure to the Queue
   *  @QueueDataStruct:   The data structure that is being added to the queue
   */
  template <class P>
  void MPC<P>::AddToQueue(P QueueDataStruct)
  {
    // Push the data to a queue
    queue_data.push(QueueDataStruct);

    CallbackStruct cLogs;
    cLogs.Module = "File: MPC.cpp; Module:  AddToQueue";
    // Emit a signal if the queue has data
    if (generateLogs)
    {
      cLogs.Descr =" MPC emitted signal DataAvailable.\n";
      cLogs.Descr+=" Input Data Point : "+std::to_string(QueueDataStruct.gYRow)+", "+ std::to_string(QueueDataStruct.gXCol);
      //sig_Logs(cLogs);
    }

    QueueDataStruct.Clear();
    // but make the object available
    sig_DataAvailable();

    // Check if the queue has reached its maximum capacity. If it has, send a
    // signal to the producer to pause so that it doesn't send data till the queue
    // has been cleared
    if(queue_data.read_available() >= max_num_dp)
    {
      sig_QueueBurdened(true);
      if (generateLogs)
      {
        cLogs.Descr =" !!! MPC Pause state : true !!!\n";
        //sig_Logs(cLogs);
      }
    }
  }//end of function

  /*! \brief PopFromQueue
   *  Pops the latest object from the queue
   */
  template <class P>
  void MPC<P>::PopFromQueue(void* obj)
  {
    CallbackStruct cLogs;
    cLogs.Module = "File: MPC.cpp; Module:  PopFromQueue";

    if(queue_data.read_available() > 0)
    {
      queue_data.consume_one([this](P qdp)
      {
        // Populate this in queue_data_point
        this->datapoint = qdp;
        qdp.Clear();
      });

      if (generateLogs)
      {
	    cLogs.Descr = " MPC emitted a DataPoint signal.\n";
        cLogs.Descr+= " Size of queue : "+ std::to_string(queue_data.read_available())+"\n";
        cLogs.Descr+= " Input Data Point : "+ std::to_string(datapoint.gYRow)+", "+std::to_string(datapoint.gXCol);
        //sig_Logs(cLogs);
      }

      // Emit a signal to this effect
      sig_DataPoint(this->datapoint, obj);

      // And check if we are populating the queue too fast
      if(queue_data.read_available() < queue_free_num_dp)
      {
        sig_QueueBurdened(false);
        if (generateLogs)
        {
          cLogs.Descr =" !!! MPC Pause state : false !!!\n";
          //sig_Logs(cLogs);
        }
      }
      this->datapoint.Clear();
    }
    else
    {
      if (generateLogs)
      {
        cLogs.Descr =" No Data to Send.\n";
        //sig_Logs(cLogs);
      }
    }
  }//end of function

  /*! \brief Finished
   *  Slot to accept
   */
  template <class P>
  void MPC<P>::Finished()
  {
    ////std::cout<<"reached here?"<<std::endl;
    sig_FinAcq();
  }//end of function

  /*! \brief Finished
  * Abort signal forward
  */
  template <class P>
  void MPC<P>::Abort()
  {
    sig_Abort();
  }//end of function

  // explicit instantiation of template class
  template class MPC<spin::vista::ProducerDataType>;
  template class MPC<spin::hemocalc::AnalysisDataType>;
}//end of namespace
