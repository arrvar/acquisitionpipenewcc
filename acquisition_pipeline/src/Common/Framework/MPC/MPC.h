#ifndef MPC_H_
#define MPC_H_
// Boost includes
#include <boost/lockfree/spsc_queue.hpp>
#include <boost/lockfree/policies.hpp>
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  /*! \brief MPC
   * Multi Process Communication
   * This class essentially enables us to link multiple processes in an sync/async
   * fashion. It can be used to solve some general problems in programming such
   * as the producer consumer problem
   */
  template <class P>
  class MPC
  {
    public :
      // Constructor
      MPC(int m1=400, int m2 = 100, bool f = false)
      {
        max_num_dp = m1;
        queue_free_num_dp = m2;
        generateLogs = f;
      }
      /*! \brief Flag to generate logs*/
      void EnableLogging(){generateLogs = true;}
      /*! \brief AddToQueue*/
      void AddToQueue(P QeueuDataStruct);
      /*! \brief PopFromQueue*/
      void PopFromQueue(void* obj);
      /*! \brief Finished*/
      void Finished();
      /*! \brief Abort*/
      void Abort();
    public:
      // Signals
      spin::VoidSignal      sig_DataAvailable;
      spin::VoidSignal      sig_Abort;
      spin::VoidSignal      sig_FinAcq;
      spin::BoolSignal      sig_QueueBurdened;
      spin::CallbackSignal  sig_Logs;
      boost::signals2::signal<void(P,void*)> sig_DataPoint;
    private:
      // Members for configuring the data buffer storage
      int max_num_dp;
      int queue_free_num_dp;
      // Lock free queue that will hold the image objects
      boost::lockfree::spsc_queue<P, boost::lockfree::capacity<600> > queue_data;
      // Data point to hold a queue data point
      P datapoint;
      bool generateLogs;
  };// End of class
};//end of namespace
#endif
