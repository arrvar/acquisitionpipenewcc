#ifndef SPINAPI_H_
#define SPINAPI_H_

#ifdef _MSC_VER
  #ifdef SPIN_EXPORTS
    #define SPIN_API __declspec(dllexport) 
  #else
    #define SPIN_API __declspec(dllimport) 
  #endif
#else
  #define SPIN_API __attribute__((visibility("default")))
#endif

#endif // SPINAPI_H_