#include "Framework/HardwareManager/Utils/DebrisDetection.h"
#include <opencv2/opencv.hpp>
#include "itkMaximumEntropyThresholdImageFilter.h"
//#include "itkOpenCVImageBridge.h"
#include "itkImageFileWriter.h"
namespace spin
{
void showImage(cv::Mat image, std::string name)
{
  cv::namedWindow(name, cv::WINDOW_NORMAL);
  cv::imshow(name, image);
  cv::waitKey(0);
}

/*void MaxEntropy(cv::Mat image, cv::Mat& threshImg)
{
  typedef itk::Image<unsigned char,2>InputImageType2D;
  typedef itk::MaximumEntropyThresholdImageFilter<InputImageType2D, InputImageType2D>MaximumEntrpoyImageFilter;
  typedef itk::ImageFileWriter<InputImageType2D>WriterType; 
  WriterType::Pointer writer = WriterType::New();

  InputImageType2D::Pointer enhImage = InputImageType2D::New();
  enhImage = itk::OpenCVImageBridge::CVMatToITKImage<InputImageType2D>(image);
  
  
  MaximumEntrpoyImageFilter::Pointer maxEntropyFilter = MaximumEntrpoyImageFilter::New();
  maxEntropyFilter->SetInsideValue(255);
  maxEntropyFilter->SetOutsideValue(0);
  maxEntropyFilter->SetNumberOfHistogramBins(256);
  maxEntropyFilter->SetInput(enhImage);
  maxEntropyFilter->Update(); 
  threshImg = itk::OpenCVImageBridge::ITKImageToCVMat<InputImageType2D>(maxEntropyFilter->GetOutput());
}*/

template<class T>
bool DebrisDetection<T>::ConfirmDebris(cv::Mat image, cv::Mat enhGrayImg)
{
  cv::Mat threshImg, foregroundImg;
  //MaxEntropy(enhGrayImg, threshImg);
  
  std::vector<cv::Mat> fgChannels;
  cv::bitwise_and(image, image, foregroundImg, threshImg);
  cv::split(image, fgChannels);
  int count = cv::countNonZero(threshImg);
  double rMean = cv::sum(foregroundImg)[2]/count;
  double gMean = cv::sum(foregroundImg)[1]/count;
  double bMean = cv::sum(foregroundImg)[0]/count;
  float r2g = rMean/gMean;
  float b2g = bMean/gMean;
  //std::cout<< "r2g, b2g" << r2g <<','<< b2g << std::endl;
  if (std::abs(r2g - b2g) > 0.6)
  {
    // Not a debris.
    return false;
  }
  else
  {
    //debris
    return true;
  }
}

template<class T>
bool DebrisDetection<T>::getDebris(cv::Mat image)
{
  cv::Mat enhImg, grayImg;
  /**
   * @brief 
   * @param image
   * enhancing the image digitally
   */
  std::vector<cv::Mat> channels, enhChannels;
  cv::split(image, channels);
  for (int i=0; i<3; ++i)
  {
    cv::Scalar meanValue = mean(channels[i]);
//	//std::cout<<meanValue<<std::endl;
	float mulFactor = 300/meanValue.val[0];
	channels[i] *= mulFactor;
  }
  cv::merge(channels, enhImg);
  cv::cvtColor(enhImg, grayImg, cv::COLOR_BGRA2GRAY);
  return ConfirmDebris(image, grayImg);

}
template class DebrisDetection<float>;
}
