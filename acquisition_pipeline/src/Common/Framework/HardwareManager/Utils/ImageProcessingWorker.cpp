#include <QThread>
#include <QDir>

#include "Common/Framework/HardwareManager/Utils/ImageProcessingWorker.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/ProcessingInterface.h"
#include "Common/Framework/HardwareManager/Utils/SharedProcessBuffer.h"

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"

namespace spin
{
  /**
   * @brief ImageProcessingWorker Default class constructor, initializes
   * objects owned by the class.
   * @param slideName Name of the slide that is being acquired.
   * @param gridName Name of the grid that is being acquired.
   * @param parent Pointer to a parent QObject class.
   */
  ImageProcessingWorker::ImageProcessingWorker(QString slideName,
                                               QString gridName,
                                               QObject* parent) :
    QObject(parent),
    m_stopWorker(false),
    m_abortWorker(false),
    m_procAoiCount(0),
    m_totalAoiCount(0),
    m_slideName(slideName),
    m_gridName(gridName),
    m_procInterface(Q_NULLPTR)
  {

  }

  /**
   * @brief processImage To process the available image.
   */
  void ImageProcessingWorker::processImage()
  {
    void* _vPtr = SHARED_PROCESS_BUFFER()->get();
    acquisition::AoIInfo* aoiInfo = static_cast<acquisition::AoIInfo* >(_vPtr);
    m_procType = aoiInfo->m_procType;

    if(m_procType == DISP_EST)
    {
      if(m_procInterface)
      {
        m_procInterface->Process(&(aoiInfo->m_img),
                                 float(aoiInfo->m_colIdx),
                                 float(aoiInfo->m_rowIdx));
      }
    }

#if 0
    cv::imwrite(aoiInfo->m_destPath.toStdString(), aoiInfo->m_img);
    if(aoiInfo->m_zStack.isEmpty() == false)
    {
      int stackSize = aoiInfo->m_zStack.size();
      const int Remove_Images_From_Stack =
          CONFIG()->intConfig(ConfigKeys::REMOVE_IMAGES_FROM_STACK);
      // Create a folder with the AoI name in the image_stack folder.
      QDir imageStackDir(aoiInfo->m_stackDestPath);
      imageStackDir.mkdir(aoiInfo->m_name);
      for (int idx = Remove_Images_From_Stack;
           idx < stackSize; idx++)
      {
        QString imgName = "img_" + QString::number(idx);
        QString imageStackPath = aoiInfo->m_stackDestPath + "/" +
          aoiInfo->m_name + "/" + imgName + ".bmp";
        cv::Mat stackImage(cv::Size(Constants::FRAME_WIDTH,
                                    Constants::FRAME_HEIGHT), CV_8UC4,
                           aoiInfo->m_zStack[idx]);
        cv::imwrite(imageStackPath.toStdString(), stackImage);
        delete[] aoiInfo->m_zStack[idx];
      }
    }
#endif
    aoiInfo->m_img.release();
    delete aoiInfo;
    m_procAoiCount++;
  }

  /**
   * @brief onStartWorker Slot called to start the task of the worker.
   */
  void ImageProcessingWorker::onStartWorker()
  {
    Q_FOREVER
    {
      // If the sahred buffer is empty then wait until data is available.
      if(SHARED_PROCESS_BUFFER()->isEmpty())
      {
        SHARED_PROCESS_BUFFER()->wait();
      }

      // Pop out data only if the queue is not empty.
      if(SHARED_PROCESS_BUFFER()->isEmpty() == false)
      {
        processImage();
      }

      m_stopMutex.lock();
      if(m_stopWorker || m_abortWorker)
      {
        m_stopMutex.unlock();
        if(m_procAoiCount >= m_totalAoiCount && m_abortWorker == false)
        {
          break;
        }
      }
      m_stopMutex.unlock();
    }

    if(m_abortWorker == false)
    {
      // Flush out remaining data if any.
      while(SHARED_PROCESS_BUFFER()->isEmpty() == false)
      {
        processImage();
      }
    }

    if(m_procType == DISP_EST)
    {
      if(m_procInterface)
      {
        if(m_abortWorker == false)
        {
          m_procInterface->CompleteProcessing();
        }
        else
        {
          m_procInterface->AbortProcessing();
        }
      }
    }

    if(thread())
    {
      thread()->quit();
    }

    Q_EMIT workerFinished();
  }

  /**
   * @brief onStopWorker Slot called to stop the task of the worker.
   */
  void ImageProcessingWorker::onStopWorker()
  {
    m_stopMutex.lock();
    m_stopWorker = true;
    m_stopMutex.unlock();

    SHARED_PROCESS_BUFFER()->resume();
  }

  /**
   * @brief onAbortWorker Slot called to abort the task of the worker.
   */
  void ImageProcessingWorker::onAbortWorker()
  {
    m_stopMutex.lock();
    m_stopWorker = true;
    m_abortWorker = true;
    m_stopMutex.unlock();

    SHARED_PROCESS_BUFFER()->resume();
  }

  /**
   * @brief init To initialize the worker.
   * @return Returns true on successful initialization of the worker.
   */
  bool ImageProcessingWorker::init(int totalAoiCount)
  {
    m_totalAoiCount = totalAoiCount;
    QString gridPath = m_slideName + "/" + m_gridName;
    QString paramFilePath = FileSystemUtil::
              getPathFromWorkspacePath(gridPath + "/acquisition.xml");

    m_procInterface = new ProcessingInterface();
    return m_procInterface->Initialize(paramFilePath);
  }

  /**
   * @brief ~ImageProcessingWorker Default class destructor, destorys objects
   * owned by the class.
   */
  ImageProcessingWorker::~ImageProcessingWorker()
  {
    if(m_procInterface)
    {
      delete m_procInterface;
    }
  }
}
