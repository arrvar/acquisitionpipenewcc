#include "Common/Framework/HardwareManager/Utils/Constants.h"

namespace spin
{
  /**
   * @brief DATABASE_TYPE Type of the database used for the application.
   */
  const QString Constants::DATABASE_TYPE = QString("QSQLITE");

  /**
   * @brief SLIDE_DATABASE_NAME Name of the slide database file.
   */
  const QString Constants::SLIDE_DATABASE_NAME = QString("slideDb.db");

  /**
   * @brief WORKSPACE_FOLDER_NAME Name of the workspace folder.
   */
  const QString Constants::WORKSPACE_FOLDER_NAME = QString("workspaces");

  /**
   * @brief FRAME_WIDTH Frame width of the MatrixVision camera in pixels.
   */
  const int Constants::FRAME_WIDTH = 1936;

  /**
   * @brief FRAME_HEIGHT Frame height of the MatrixVision camera in pixels.
   */
  const int Constants::FRAME_HEIGHT = 1216;

  /**
   * @brief VIRT_SLIDE_WIDTH Width of the virtual slide in pixels.
   */
  const int Constants::VIRT_SLIDE_WIDTH = 1600;

  /**
   * @brief VIRT_SLIDE_HEIGHT Height of the virtual slide in pixels.
   */
  const int Constants::VIRT_SLIDE_HEIGHT = 4800;

  /**
   * @brief SLIDE_WIDTH Width of a physical slide in microns.
   */
  const int Constants::SLIDE_WIDTH = 25000;

  /**
   * @brief SLIDE_HEIGHT Height of a physical slide in microns.
   */
  const int Constants::SLIDE_HEIGHT = 75000;

  /**
   * @brief ACQ_LIB_NAME Name of the acquisition library.
   */
  const QString Constants::ACQ_LIB_NAME = "libspinvistaAcquisition.so";

}
