#ifndef HMIMAGEBUFFER_H
#define HMIMAGEBUFFER_H

#include <QQueue>
#include <QMutex>

namespace spin
{
  /**
   * @brief The ImageBuffer class is a template class for storing a queue of
   * images. A writer thread will retrieve the images from the queue and write
   * them to the destination.
   */
  template <class T>
  class ImageBuffer
  {
  public:
    /**
     * @brief ImageBuffer Default class constructor, initializes objects owned
     * by the class.
     */
    ImageBuffer()
    {

    }

    /**
     * @brief add To add an image to the queue.
     * @param buff Image buffer.
     */
    void add(const T& buff)
    {
      m_queueMutex.lock();
      m_queue.enqueue(buff);
      m_queueMutex.unlock();
    }

    /**
     * @brief get To retrieve the first image data from the queue.
     * @return Returns the object of the image data.
     */
    T get()
    {
      m_queueMutex.lock();
      T imgBuff = m_queue.dequeue();
      m_queueMutex.unlock();

      return imgBuff;
    }

    /**
     * @brief isEmpty To check if the queue is empty.
     * @return Returns true if the queue is empty else returns false.
     */
    bool isEmpty()
    {
      m_queueMutex.lock();
      bool isQEmpty = m_queue.isEmpty();
      m_queueMutex.unlock();

      return isQEmpty;
    }

    /**
     * @brief size To get the size of the queue.
     * @return Returns the size of the queue.
     */
    int size()
    {
      m_queueMutex.lock();
      int qSize = m_queue.size();
      m_queueMutex.unlock();

      return qSize;
    }

    /**
     * @brief ~ImageBuffer Default class destructor, destroys the objects
     * owned by the class.
     */
    ~ImageBuffer()
    {

    }

  private:
    /**
     * @brief m_queueMutex Mutex to guard the queue.
     */
    QMutex m_queueMutex;

    /**
     * @brief m_queue Queue for storing the image buffer and its corresponding
     * destination path.
     */
    QQueue <T> m_queue;

  };//end of ImageBuffer class.
} // end of spin namespace.

#endif // IMAGEBUFFER_H
