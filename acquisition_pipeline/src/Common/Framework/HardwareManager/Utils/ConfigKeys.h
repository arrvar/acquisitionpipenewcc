#ifndef HMCONFIGKEYS_H
#define HMCONFIGKEYS_H

#include <QString>

#include "Common/Framework/HardwareManager/Utils/Common.h"

namespace spin
{
  /**
   * @brief The ConfigKeys class consists of unique names of configuration
   * parameters.
   */
  class ConfigKeys
  {
  public:
    /**
     * @brief SYS_SERIAL_NUM Serial number of the system.
     */
    static const QString SYS_SERIAL_NUM;

    /**
     * @brief MAX_SLOT_COUNT Maximum number of slots for this configuration.
     */
    static const QString MAX_SLOT_COUNT;

    /**
     * @brief STAGE_LIB_NAME Name of the library for connecting to the translation stage.
     */
    static const QString STAGE_LIB_NAME;

    /**
     * @brief STAGE_STEP_SIZE_X Step size of stage in microns for 1 microstepping.
     */
    static const QString STAGE_STEP_SIZE_X;

    /**
     * @brief STAGE_STEP_SIZE_Y Step size of stage in microns for 1 microstepping.
     */
    static const QString STAGE_STEP_SIZE_Y;

    /**
     * @brief STAGE_STEP_SIZE_Z Step size of stage in microns for 1 microstepping.
     */
    static const QString STAGE_STEP_SIZE_Z;

    /**
     * @brief STAGE_MICROSTEPPING_X X-Stage microtepping.
     */
    static const QString STAGE_MICROSTEPPING_X;

    /**
     * @brief STAGE_MICROSTEPPING_Y Y-Stage microtepping.
     */
    static const QString STAGE_MICROSTEPPING_Y;

    /**
     * @brief STAGE_MICROSTEPPING_Z Z-Stage microtepping.
     */
    static const QString STAGE_MICROSTEPPING_Z;

    /**
     * @brief STAGE_MICROSTEPPING_Turret Turret-Stage microtepping.
     */
    static const QString STAGE_MICROSTEPPING_TURRET;

    /**
     * @brief XY_DRIVER_RUN_CURRENT XY motor driver current config.
     */
    static const QString XY_DRIVER_RUN_CURRENT;

    /**
     * @brief Z_DRIVER_RUN_CURRENT Z motor driver current config.
     */
    static const QString Z_DRIVER_RUN_CURRENT;

    /**
     * @brief TURRET_DRIVER_RUN_CURRENT TURRET motor driver current config.
     */
    static const QString TURRET_DRIVER_RUN_CURRENT;

    /**
     * @brief XY_DRIVER_HOLD_CURRENT XY motor driver current config.
     */
    static const QString XY_DRIVER_HOLD_CURRENT;

    /**
     * @brief Z_DRIVER_HOLD_CURRENT Z motor driver current config.
     */
    static const QString Z_DRIVER_HOLD_CURRENT;

    /**
     * @brief TURRET_DRIVER_HOLD_CURRENT TURRET motor driver current config.
     */
    static const QString TURRET_DRIVER_HOLD_CURRENT;

    /**
     * @brief X_DRIVER_MODE X motor driver mode config.
     */
    static const QString X_DRIVER_MODE;

    /**
     * @brief Y_DRIVER_MODE Y motor driver mode config.
     */
    static const QString Y_DRIVER_MODE;

    /**
     * @brief Z_DRIVER_MODE Z motor driver mode config.
     */
    static const QString Z_DRIVER_MODE;

    /**
     * @brief Turret_DRIVER_MODE Turret motor driver mode config.
     */
    static const QString TURRET_DRIVER_MODE;
    /**
     * @brief FOV_WIDTH_20X_MICRONS Width of the camera field of view in microns
     * at 20x magnification.
     */
    static const QString FOV_WIDTH_20X_MICRONS;

    /**
     * @brief FOV_HEIGHT_20X_MICRONS Width of the camera field of view in microns
     * at 20x magnification.
     */
    static const QString FOV_HEIGHT_20X_MICRONS;

    /**
     * @brief FOV_WIDTH_40X_MICRONS Width of the camera field of view in microns
     * at 40x magnification.
     */
    static const QString FOV_WIDTH_40X_MICRONS;

    /**
     * @brief FOV_HEIGHT_40X_MICRONS Width of the camera field of view in microns
     * at 40x magnification.
     */
    static const QString FOV_HEIGHT_40X_MICRONS;

    /**
     * @brief FOV_WIDTH_100X_MICRONS Width of the camera field of view in microns
     * at 100x magnification.
     */
    static const QString FOV_WIDTH_100X_MICRONS;

    /**
     * @brief FOV_HEIGHT_100X_MICRONS Width of the camera field of view in microns
     * at 100x magnification.
     */
    static const QString FOV_HEIGHT_100X_MICRONS;

    /**
     * @brief X_OFFSET Offset in X direction of the current loaded slot.
     */
    static const QString X_OFFSET;

    /**
     * @brief Y_OFFSET Offset in Y direction of the current loaded slot.
     */
    static const QString Y_OFFSET;

    /**
     * @brief X_UPPER_LIMIT Upper limit of the X-Stage in microns.
     */
    static const QString X_UPPER_LIMIT;

    /**
     * @brief Y_UPPER_LIMIT Upper limit of the Y-Stage in microns.
     */
    static const QString Y_UPPER_LIMIT;

    /**
     * @brief Z_LOWER_LIMIT Lower limit of the Z-Stage.
     */
    static const QString Z_LOWER_LIMIT;

    /**
     * @brief Z_UPPER_LIMIT Upper limit of the Z-Stage.
     */
    static const QString Z_UPPER_LIMIT;

    /**
     * @brief Z_20X_LOWER_LIMIT
     */
    static const QString Z_20X_LOWER_LIMIT;

    /**
     * @brief Z_20X_UPPER_LIMIT
     */
    static const QString Z_20X_UPPER_LIMIT;

    /**
     * @brief Z_20X_STACK_LOWER_LIMIT
     */
    static const QString Z_20X_STACK_LOWER_LIMIT;

    /**
     * @brief Z_20X_STACK_UPPER_LIMIT
     */
    static const QString Z_20X_STACK_UPPER_LIMIT;

    /**
     * @brief Z_40X_STACK_LOWER_LIMIT
     */
    static const QString Z_40X_STACK_LOWER_LIMIT;

    /**
     * @brief Z_40X_STACK_UPPER_LIMIT
     */
    static const QString Z_40X_STACK_UPPER_LIMIT;

    /**
     * @brief Z_100X_STACK_LOWER_LIMIT
     */
    static const QString Z_100X_STACK_LOWER_LIMIT;

    /**
     * @brief Z_100X_STACK_UPPER_LIMIT
     */
    static const QString Z_100X_STACK_UPPER_LIMIT;

    /**
     * @brief Z_20X_TO_40X_OFF
     */
    static const QString Z_20X_TO_40X_OFF;

    /**
     * @brief Z_20X_TO_100X_OFF
     */
    static const QString Z_20X_TO_100X_OFF;

    /**
     * @brief Z_40X_TO_100X_OFF
     */
    static const QString Z_40X_TO_100X_OFF;

    /**
     * @brief CAM_EXP_20X Exposure value of the camera at 20x magnification.
     */
    static const QString CAM_EXP_20X;

    /**
     * @brief CAM_EXP_40X Exposure value of the camera at 40x magnification.
     */
    static const QString CAM_EXP_40X;

    /**
     * @brief CAM_EXP_100X Exposure value of the camera at 100x magnification.
     */
    static const QString CAM_EXP_100X;

    /**
     * @brief CAM_EXP_100X_OIL Exposure value of the camera at 100x oil
     * magnification.
     */
    static const QString CAM_EXP_100X_OIL;

    /**
     * @brief CONT_ACQ_EXP_20X Exposure value of the camera at
     * 20x magnification during continous acquisition.
     */
    static const QString CONT_ACQ_EXP_20X;

    /**
     * @brief CONT_ACQ_EXP_40X Exposure value of the camera at
     * 40x magnification during continous acquisition.
     */
    static const QString CONT_ACQ_EXP_40X;

    /**
     * @brief RED_BALANCE_RATIO Red balance ratio value of the camera.
     */
    static const QString RED_BALANCE_RATIO;

    /**
     * @brief BLUE_BALANCE_RATIO Blue balance ration value of the camera.
     */
    static const QString BLUE_BALANCE_RATIO;

    /**
     * @brief OFFSET_X_20_TO_40
     */
    static const QString OFFSET_X_20_TO_40;

    /**
     * @brief OFFSET_Y_20_TO_40
     */
    static const QString OFFSET_Y_20_TO_40;

    /**
     * @brief OFFSET_X_20_TO_100
     */
    static const QString OFFSET_X_20_TO_100;

    /**
     * @brief OFFSET_Y_20_TO_100
     */
    static const QString OFFSET_Y_20_TO_100;

    /**
     * @brief OFFSET_X_40_TO_100
     */
    static const QString OFFSET_X_40_TO_100;

    /**
     * @brief OFFSET_Y_40_TO_100
     */
    static const QString OFFSET_Y_40_TO_100;

    /**
     * @brief OFFSET_Z_20_TO_40
     */
    static const QString OFFSET_Z_20_TO_40;

    /**
     * @brief OFFSET_Z_20_TO_100
     */
    static const QString OFFSET_Z_20_TO_100;

    /**
     * @brief OFFSET_Z_40_TO_100
     */
    static const QString OFFSET_Z_40_TO_100;

    /**
     * @brief STAGE_Z_STEP_SIZE_20x
     */
    static const QString STAGE_Z_STEP_SIZE_20x;

    /**
     * @brief STAGE_Z_STEP_SIZE_40x
     */
    static const QString STAGE_Z_STEP_SIZE_40x;

    /**
     * @brief STAGE_Z_STEP_SIZE_100x
     */
    static const QString STAGE_Z_STEP_SIZE_100x;

    /**
     * @brief MAX_XY_SPEED To store the maximum speed of the XY-Stage in mm/s.
     */
    static const QString MAX_XY_SPEED;

    /**
     * @brief CONTINUOUS_SPEED_20X To store the speed of the XY-Stage during continuous
     * acquisition at 20x magnification
     */
    static const QString CONTINUOUS_SPEED_20X;

    /**
     * @brief CONTINUOUS_SPEED_40X To store the speed of the XY-Stage during continuous
     * acquisition at 40x magnification
     */
    static const QString CONTINUOUS_SPEED_40X;

    /**
     * @brief COLUMN_ZSTACK_MODE For doing a hardware Z-Stage trigger during
     * continuous mode of acquisition.
     * 1 - indicates trigger will be used.
     * 0 - indicates trigger will not be used.
     */
    static const QString COLUMN_ZSTACK_MODE;

    /**
     * @brief ENABLE_STITCHING For running the stitching pipeline during
     * continuous mode of acqusition.
     * 1 - indicates stitching pipeline will be enabled.
     * 0 - indicates stitching pipeline will be disabled.
     */
    static const QString ENABLE_STITCHING;

    static const QString FOCUS_CHANNEL_20X;
    static const QString FOCUS_CHANNEL_40X;
    static const QString FOCUS_CHANNEL_100X;

    /**
     * @brief BG_THRESH_BLOOD_SMEAR To store the background threshold values
     * for blood smear specimen type.
     */
    static const QString BG_THRESH_BLOOD_SMEAR;

    /**
     * @brief BG_THRESH_TISSUE_BIOPSY To store the background threshold values
     * for tissue biopsy specimen type.
     */
    static const QString BG_THRESH_TISSUE_BIOPSY;

    /**
     * @brief FOCUS_METRIC_THRESHOLD To store the threshold value for
     * focus metric.
     */
    static const QString FOCUS_METRIC_THRESHOLD;

    static const QString WRITE_IMAGES;
    static const QString WRITE_STACK;

    static const QString INSERT_FILTER_TAG;

    static const QString REMOVE_FILTER_TAG;

    static const QString BACKLASH_Z;

    static const QString Z_TRIGGER_CAM_FPS;

    static const QString MAG_HOME_POS;

    static const QString MAG_20X_POS;

    static const QString MAG_40X_POS;

    static const QString MAG_100X_POS;

    static const QString MAG_100X_OIL_POS;

    /**
     * @brief BG_FM_BLOOD_SMEAR Focus metric threshold for background images for
     * the blood smear pipeline.
     */
    static const QString BG_FM_BLOOD_SMEAR;

    /**
     * @brief BG_FM_TISSUE_BIOPSY Focus metric threshold for background images
     * for the tissue biopsy pipeline.
     */
    static const QString BG_FM_TISSUE_BIOPSY;

    /**
     * @brief MAX_Z_STAGE_SPEED Maximum Z-Stage speed value in mm/s.
     */
    static const QString MAX_Z_STAGE_SPEED;

    /**
     * @brief DEF_Z_STAGE_SPEED Default Z-Stage speed value at 20x in mm/s.
     */
    static const QString DEF_Z_STAGE_SPEED;

    /**
     * @brief DEF_Z_STAGE_SPEED_40X Default Z-Stage speed value at 40x in mm/s.
     */
    static const QString DEF_Z_STAGE_SPEED_40X;

    /**
     * @brief XY_PRE_TRIGGER_DELAY To store the pre-trigger delay for the
     * XY-Stage.
     */
    static const QString XY_PRE_TRIGGER_DELAY;

    /**
     * @brief XY_POST_TRIGGER_DELAY To store the post-trigger delay for the
     * XY-Stage.
     */
    static const QString XY_POST_TRIGGER_DELAY;

    /**
     * @brief Z_PRE_TRIGGER_DELAY To store the pre-trigger delay for the
     * Z-Stage.
     */
    static const QString Z_PRE_TRIGGER_DELAY;

    /**
     * @brief Z_POST_TRIGGER_DELAY To store the post-trigger delay for the
     * Z-Stage.
     */
    static const QString Z_POST_TRIGGER_DELAY;

    /**
     * @brief Z_STACK_TRIGGER_DELAY To store the delay for a Z-stack during
     * continuous acquisition.
     */
    static const QString Z_STACK_TRIGGER_DELAY;

    /**
     * @brief X_TRIGGER_PULSE_DURATION To store the pulse duration for a
     * X-stack during continuous acquisition.
     */
    static const QString X_TRIGGER_PULSE_DURATION;

    /**
     * @brief Y_TRIGGER_PULSE_DURATION To store the pulse duration for a
     * Y-stack during continuous acquisition.
     */
    static const QString Y_TRIGGER_PULSE_DURATION;

    /**
     * @brief Z_TRIGGER_PULSE_DURATION To store the pulse duration for a
     * Z-stack during continuous acquisition.
     */
    static const QString Z_TRIGGER_PULSE_DURATION;

    /**
     * @brief Z_OFFSET_STEPS To store the offset in steps for the Z-Stage.
     */
    static const QString Z_OFFSET_STEPS;

    /**
     * @brief XY_Z_TRIGGER_STACK_SIZE To store the stack size for Z-Trigger
     * during continuous acquisition.
     */
    static const QString XY_Z_TRIGGER_STACK_SIZE;

    /**
     * @brief XY_Z_TRIGGER_STEP_SIZE To store the step size for the Z-Trigger
     * during continuous acquisition at 20x in microns.
     */
    static const QString XY_Z_TRIGGER_STEP_SIZE;

    /**
     * @brief XY_Z_TRIGGER_STEP_SIZE_40X To store the step size for the
     * Z-Trigger during continuous acquisition at 40x in microns.
     */
    static const QString XY_Z_TRIGGER_STEP_SIZE_40X;

    /**
     * @brief REMOVE_IMAGES_FROM_STACK To store the no. of images to remove
     * from the stack.
     */
    static const QString REMOVE_IMAGES_FROM_STACK;

    /**
     * @brief INLINE_FM_CALC To indicate whether the focus metric computation
     * should be inline (within thread) or not.
     */
    static const QString INLINE_FM_CALC;

  private:
    /**
     * @brief ConfigKeys Class constructor, declared in private to prevent
     * instantiation from outside.
     */
    ConfigKeys();

    DISALLOW_COPY_AND_ASSIGNMENT(ConfigKeys)

  };// end of ConfigKeys class.
} // end of spin namespace.

#endif // CONFIGKEYS_H
