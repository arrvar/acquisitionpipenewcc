#ifndef ZCAPTURETHREAD_H
#define ZCAPTURETHREAD_H

#include <QThread>

#include <opencv2/opencv.hpp>

namespace spin
{
  /**
   * @brief The ZCaptureThread class handles the task of capturing a Z-stack
   * of images.
   */
  class ZCaptureThread : public QThread
  {
  public:
    /**
     * @brief ZCaptureThread Default class constructor, initializes objects
     * owned by the class.
     * @param parent Pointer instance of the parent object.
     */
    ZCaptureThread(QObject* parent = 0);

    /**
     * @brief ~ZCaptureThread Default class destructor, destroys objects owned
     * by the class.
     */
    ~ZCaptureThread();

    /**
     * @brief m_imageStack Container to store a stack of images.
     */
    std::vector <cv::Mat> m_imageStack;

    /**
     * @brief init Initializes and allocates memory to the stack storage
     * container.
     * @param stackSize Size of the image stack.
     */
    void init(int stackSize);

    /**
     * @brief captureState To get the result of the Z-stack capture.
     * @return Returns true if the capture was successful else returns false.
     */
    bool captureState() const
    {
      return m_captureStatus;
    }

  protected:
    /**
     * @brief run Reimplemented method, where the Z-stack capturing will run
     * in the thread.
     */
    virtual void run();

  private:
    /**
     * @brief clearStack To delete memory allocated to the stack container.
     */
    void clearStack();

  private:
    /**
     * @brief m_captureStatus To store the status of the Z-stack capture.
     */
    bool m_captureStatus;

  };// end of ZCaptureThread class.
} // end of spin namespace.

#endif // ZCAPTURETHREAD_H
