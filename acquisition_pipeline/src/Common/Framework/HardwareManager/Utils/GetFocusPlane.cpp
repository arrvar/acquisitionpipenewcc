#include <unistd.h>
#include <cmath>

#include <QThread>
#include <QElapsedTimer>

#include "Common/Framework/HardwareManager/Acquisition/CaptureWorker.h"
#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"
#include "Common/Framework/HardwareManager/Utils/TriggerBestZ.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"
#include "Common/Framework/HardwareManager/Utils/DebrisDetection.h"
#include "Common/Framework/HardwareManager/Utils/GetFocusPlane.h"
namespace spin
{
  const int GetFocusPlane::MAX_RETRY_ATTEMPTS = 5;
  // Driver function to sort the vector elements by
  bool sortbysecdesc(const std::tuple<double,double,int> &a,
                     const std::tuple<double,double,int> &b)
  {
         return std::get<0>(a) > std::get<0>(b);
  }
  // A linear search based function that returns index of all peak elements
  std::vector<int> findPeakUtil(std::vector<std::tuple<double, double,int>> metricVec, int n)
  {
      std::vector<int> peakVec;
      int maxVal = 0;
      int tempIdx = 0;
      if(n<3)
      {
          for(int i =0;i<n;i++)
          {
              peakVec.push_back(i);
          }

      }
      else
      {
        for(int i =1;i<n-1;i++)
        {
            if(std::get<1>(metricVec[i-1]) < std::get<1>(metricVec[i]) &&
               std::get<1>(metricVec[i]) > std::get<1>(metricVec[i+1]))
            {
               peakVec.push_back(std::get<2>(metricVec[i]));
            }
        }
        double mean = 0;
        for(int i =0;i<n;i++)
        {
           mean+=std::get<1>(metricVec[i]);
        }
        mean/=n;
        double std_dev = 0;
        for(int i =0;i<n;i++)
        {
           std_dev+=pow(std::get<1>(metricVec[i])-mean,2);
        }
        std_dev=pow(std_dev/n,0.5);
        int i=0;
        SPIN_LOG_DEBUG(QObject::tr("Mean: %1 Std Dev %2").arg(mean).arg(std_dev));
        SPIN_LOG_DEBUG(QObject::tr("PeakVecSize: %1").arg(peakVec.size()));
        if(peakVec.size()==0)
        {
          for(int i =0;i<n;i++)
          {
              if(maxVal<std::get<1>(metricVec[i]))
              {
                 maxVal = std::get<1>(metricVec[i]);
                 tempIdx = std::get<2>(metricVec[i]);
              }

          }
        }
        peakVec.push_back(tempIdx);
        while(peakVec.size()>1 && i<peakVec.size())
        {
          if(std::get<1>(metricVec[peakVec[i]])<mean+std_dev)
          {
            SPIN_LOG_DEBUG(QObject::tr("Erased: %1").arg(peakVec[i]));
            peakVec.erase(peakVec.begin()+i);
            i=0;
            continue;
          }
          else
          {
            i++;
          }
        }
      }
      return peakVec;
  }
    /**
    * @brief The GetFocusPlane class is a class for finding Z plane
    * @param parent
    */
    GetFocusPlane::GetFocusPlane(QObject* parent):
    QObject(parent)
    {

    }

    /**
    * @brief init To initialize the worker with startZ stopZ focusStepSize and normalSpeed
    * internally this should handle trigger movement speed and return back with a normalSpeed.
    * @param startZ starting position of Z to capture(um).
    * @param stopZ stop position of Z till when to capture(um).
    * @param focusStepSize distance between two captures(um).
    * @param normalSpeed starting position of Z to capture(mm/s).
    */
    void GetFocusPlane::init(double startZ, double stopZ, double focusStepSize,
                            double normalSpeed, int specType)
    {
        m_startZ = startZ;
        m_stopZ = stopZ;
        m_sign = (m_stopZ - m_startZ)/abs(m_stopZ - m_startZ);
        m_focusStepSize = focusStepSize;
        m_normalSpeed = normalSpeed;
        m_specType = (SpecimenType)specType;
        // Calculate the stack size based on focus Steps and Range
        m_imageStackSize = (abs(m_stopZ - m_startZ)/m_focusStepSize);
        SPIN_LOG_DEBUG(QObject::tr("Stack Size: %1").arg(m_imageStackSize));
        // Resize the thread image Stack Size
        m_szt.m_imageStack.resize(m_imageStackSize);
        // Set the stop Z position
        HW_MANAGER()->setZPosition(m_startZ, true);
        //Initialize the vaiables
        m_szt.init(m_focusStepSize,m_normalSpeed,m_imageStackSize);

        // Enable trigger mode
        double zStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::
                                                         STAGE_STEP_SIZE_Z);
        double zMicrostepping = CONFIG()->realConfig(ConfigKeys::
                                                     STAGE_MICROSTEPPING_Z);
        HW_MANAGER()->enableZTriggerMode(m_focusStepSize / (zStepSizeInMicrons /
                                                        zMicrostepping));

        // Set continous mode
//        HW_MANAGER()->setContinuousMode(Z_TRIGGER);
        // Start the Capture Thread and wait for Trigger
        m_szt.start(QThread::TimeCriticalPriority);
        // Set the time critical property for Thread
        //szt.setPriority();
    }
    double GetFocusPlane::getBestZ()
    {
      //Go to end location to capture all images
      HW_MANAGER()->setZPosition(m_stopZ);
      QElapsedTimer sampleTimer;
      sampleTimer.start();
      m_szt.wait();
      double tt = sampleTimer.elapsed()/1000.0;
      SPIN_LOG_DEBUG(QObject::tr("Stack Actual FPS: %1").
                     arg(m_imageStackSize/tt));
      if(m_szt.m_err == false)
      {
        std::vector<std::tuple<double, double,int>> zMetricVec;
        zMetricVec.resize(m_imageStackSize);

        for(int idx=0;idx<m_imageStackSize;idx++)
        {
          cv::Mat imgPtr = m_szt.m_imageStack[idx];

          cv::Mat splitImage[4];
          // Split the image into four different channels.
          cv::split(imgPtr, splitImage);
          //currently using only green channel
          double focusMetricVal = Utilities::computeFocusMetric(splitImage[1]);
          double zVal = m_startZ + m_sign*(idx + 1) * m_focusStepSize;
          zMetricVec[idx] = std::make_tuple(zVal,focusMetricVal,idx);
          SPIN_LOG_INFO(QObject::tr("Z-Stage Value: %1   Focus Metric Value: %2").
                        arg(zVal, -8, 'f', 3, '0').
                        arg(focusMetricVal, 0, 'f', 10));
        }
        /*QString tempImageName;
        for(auto zMetric:zMetricVec)
        {
          tempImageName.sprintf("%f.bmp", std::get<0>(zMetric));
          cv::imwrite(tempImageName.toStdString(), m_szt.m_imageStack[std::get<2>(zMetric)]);
        }*/
        //find the peak values of metric
        std::vector<int> peakVec = findPeakUtil(zMetricVec,zMetricVec.size());
        int i=0;
        //discard the metricvec for nonpeak  values
        while(zMetricVec.size()>peakVec.size())
        {
          bool present = false;
          for(auto peak:peakVec)
          {
            if(peak==std::get<2>(zMetricVec[i]))
            {
              present = true;
              break;
            }
          }
          if(!present)
          {
            zMetricVec.erase(zMetricVec.begin()+i);
          }
          else
          {
            i++;
          }

        }
        // Check for background peaks.
        bool isBG = false;
        bool fgIdentified = false;
        double x,y;
        HW_MANAGER()->getXYPosition(x,y);
        QString imgName, imagePath;
        std::sort(zMetricVec.begin(), zMetricVec.end(), sortbysecdesc);
        for(auto zMetric:zMetricVec)
        {
          SPIN_LOG_INFO(QObject::tr("%1, %2, %3").arg(std::get<0>(zMetric))
                                                 .arg(std::get<1>(zMetric))
                                                 .arg(std::get<2>(zMetric)));

        }
        std::tuple<double, double,int> zMetric;
        for(int idx=0;idx<zMetricVec.size();idx++)
        {
          zMetric = zMetricVec[idx];
          imgName.sprintf("X%f_Y%f_Z%f.jpg", x, y, std::get<0>(zMetric));
          isBG = Utilities::isBackground(m_szt.m_imageStack[std::
                                                            get<2>(zMetric)],
                                         m_specType);
          if (isBG == true)
          {
            QString debris_detection_path = FileSystemUtil::
                getPathFromInstallationPath("etc/debris_data");
            imgName.prepend("BG");
            imagePath = debris_detection_path+"/"+imgName;
          }
          else
          {
            QString focus_sampling_path = FileSystemUtil::
                getPathFromInstallationPath("etc/sampling_focus");
            imagePath = focus_sampling_path+"/"+imgName;
            fgIdentified = true;
          }
          SPIN_LOG_DEBUG(QObject::tr("Image Path: %1").arg(imagePath));
          cv::imwrite(imagePath.toStdString(),
                      m_szt.m_imageStack[std::get<2>(zMetric)]);
          if(fgIdentified)
          {
            SPIN_LOG_DEBUG(QObject::tr("FG Identified"));
            break;
          }
        }
        // Clear the OpenCV memory.
        for(int idx = 0; idx < m_szt.m_imageStack.size(); idx++)
        {
          m_szt.m_imageStack[idx].release();
        }
        m_szt.m_imageStack.clear();

        zMetric = zMetricVec[0];
        if(fgIdentified)
        {
          return std::get<0>(zMetric);
        }
        else
        {
          return -1;
        }
      }
       else
       {
         return -1;
       }
    }

    /**
    * @brief chooseBestZ Getting correct best Z.
    */
    double GetFocusPlane::chooseBestZ(double bestZ1, double bestZ2)
    {
        double bestZ = -1;
        double zStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::
                                                         STAGE_STEP_SIZE_Z);
        double zMicrostepping = CONFIG()->realConfig(ConfigKeys::
                                                     STAGE_MICROSTEPPING_Z);
        double minZstep = zStepSizeInMicrons/zMicrostepping;

        if ((bestZ1 > 0) && (bestZ2 > 0))
        {
            if (abs(bestZ1 - bestZ2) > 50)
            {
                bestZ = std::max(bestZ1, bestZ2);
            }
            else
            {
                bestZ = (bestZ1+bestZ2)/2;
            }
        }
        else if ((bestZ1 > 0) && (bestZ2 < 0))
        {
            bestZ = bestZ1;
        }
        else if ((bestZ1 < 0) && (bestZ2 > 0))
        {
            bestZ = bestZ2;
        }
        else
        {
            bestZ = -1;
        }
        bestZ = (int)(bestZ/minZstep)*minZstep;
        return bestZ;
    }

    /**
    * @brief onStartCapture Slot called to start capture.
    */
    double GetFocusPlane::onStartCapture()
    {
        double bestZ = -1;
        double bestZ1 = -1;
        double bestZ2 = -1;
        double global_startZ = m_startZ;
        double global_stopZ = m_stopZ;
        double new_startZ = m_startZ;
        double new_stopZ = m_stopZ;
        QElapsedTimer sampleTimer;
        sampleTimer.start();
        usleep(500);
        bestZ1 = getBestZ();
        std::cout << "****** BestZ1 ****** :" << bestZ1 << std::endl;
        new_startZ = global_stopZ - m_focusStepSize/2;
        new_stopZ = global_startZ - m_focusStepSize/2;
        init(new_startZ, new_stopZ, m_focusStepSize,
             m_normalSpeed, m_specType);
        usleep(500);
        bestZ2 = getBestZ();
        std::cout << "****** BestZ2 ****** :" << bestZ2 << std::endl;
        bestZ = chooseBestZ(bestZ1, bestZ2);
        std::cout << "****** BestZ ****** :" << bestZ << std::endl;
        // Disable trigger mode
        HW_MANAGER()->disableTriggerMode();
        // Stop continous mode
//        HW_MANAGER()->stopContinuousMode();
        //2nd attempt in case of snap error
        if(bestZ<0)
        {
          init(global_startZ, global_stopZ, m_focusStepSize,
               m_normalSpeed, m_specType);
          usleep(500);
          bestZ1 = getBestZ();
          new_startZ = global_stopZ - m_focusStepSize/2;
          new_stopZ = global_startZ - m_focusStepSize/2;
          init(new_startZ, new_stopZ, m_focusStepSize,
               m_normalSpeed, m_specType);
          usleep(500);
          bestZ2 = getBestZ();
          bestZ = chooseBestZ(bestZ1, bestZ2);
          // Disable trigger mode
          HW_MANAGER()->disableTriggerMode();
          // Stop continous mode
//          HW_MANAGER()->stopContinuousMode();
        }
        double x,y;
        HW_MANAGER()->getXYPosition(x,y);
        if(bestZ<0)
        {
          //change this based on maginfication?
          HW_MANAGER()->setXYPosition(x+500,y);
          init(global_startZ, global_stopZ, m_focusStepSize,
               m_normalSpeed, m_specType);
                  usleep(500);
          bestZ1 = getBestZ();
          new_startZ = global_stopZ - m_focusStepSize/2;
          new_stopZ = global_startZ - m_focusStepSize/2;
          init(new_startZ, new_stopZ, m_focusStepSize,
               m_normalSpeed, m_specType);
          usleep(500);
          bestZ2 = getBestZ();
          bestZ = chooseBestZ(bestZ1, bestZ2);
          // Disable trigger mode
          HW_MANAGER()->disableTriggerMode();
          // Stop continous mode
//          HW_MANAGER()->stopContinuousMode();

        }
        if(bestZ<0)
        {
          //change this based on maginfication?
          HW_MANAGER()->setXYPosition(x,y-500);
          init(global_startZ, global_stopZ, m_focusStepSize,
               m_normalSpeed, m_specType);
                  usleep(500);
          bestZ1 = getBestZ();
          new_startZ = global_stopZ - m_focusStepSize/2;
          new_stopZ = global_startZ - m_focusStepSize/2;
          init(new_startZ, new_stopZ, m_focusStepSize,
               m_normalSpeed, m_specType);
          usleep(500);
          bestZ2 = getBestZ();
          bestZ = chooseBestZ(bestZ1, bestZ2);
          // Disable trigger mode
          HW_MANAGER()->disableTriggerMode();
          // Stop continous mode
//          HW_MANAGER()->stopContinuousMode();

        }
        if(bestZ<0)
        {
          //change this based on maginfication?
          HW_MANAGER()->setXYPosition(x,y+500);
          init(global_startZ, global_stopZ, m_focusStepSize,
               m_normalSpeed, m_specType);
                  usleep(500);
          bestZ1 = getBestZ();
          new_startZ = global_stopZ - m_focusStepSize/2;
          new_stopZ = global_startZ - m_focusStepSize/2;
          init(new_startZ, new_stopZ, m_focusStepSize,
               m_normalSpeed, m_specType);
          usleep(500);
          bestZ2 = getBestZ();
          bestZ = chooseBestZ(bestZ1, bestZ2);
          // Disable trigger mode
          HW_MANAGER()->disableTriggerMode();
          // Stop continous mode
//          HW_MANAGER()->stopContinuousMode();

        }
        if(bestZ<0)
        {
          //change this based on maginfication?
          HW_MANAGER()->setXYPosition(x-500,y);
          init(global_startZ, global_stopZ, m_focusStepSize,
               m_normalSpeed, m_specType);
                  usleep(500);
          bestZ1 = getBestZ();
          new_startZ = global_stopZ - m_focusStepSize/2;
          new_stopZ = global_startZ - m_focusStepSize/2;
          init(new_startZ, new_stopZ, m_focusStepSize,
               m_normalSpeed, m_specType);
          usleep(500);
          bestZ2 = getBestZ();
          bestZ = chooseBestZ(bestZ1, bestZ2);
          // Disable trigger mode
          HW_MANAGER()->disableTriggerMode();
          // Stop continous mode
//          HW_MANAGER()->stopContinuousMode();

        }
        m_szt.quit();
        while(m_szt.isRunning())
        {
          SPIN_LOG_DEBUG("Waiting for the trigger thread to stop.");
        }
        SPIN_LOG_DEBUG(QObject::tr("Time Taken For Best Z : %1").
                       arg(sampleTimer.elapsed()/1000.0));
        //add provision for start stop mode here
        return bestZ;
    }

    /**
    * @brief ~GetFocusPlane Default class destructor, destroys objects owned
    * by the class.
    */
    GetFocusPlane::~GetFocusPlane()
    {

    }
}
