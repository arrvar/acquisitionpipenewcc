#include <dlibxx.hxx>
#include <opencv2/opencv.hpp>

#include <QString>
#include <QFileInfo>

#include "Common/Framework/HardwareManager/Utils/ProcessingInterface.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Framework/AppContext.h"

#include "Common/AbstractClasses/AbstractVistaModule.h"

namespace spin
{
  /**
   * @brief ProcessingInterface Class constructor, initializes objects owned
   * by the class.
   */
  ProcessingInterface::ProcessingInterface()
  {

  }

  /**
   * @brief Initialize To load and initialize the processing module(s).
   * @param paramFilePath Absolute path to the parameter file.
   * @return Returns true on successful initialization of available processing
   * module(s).
   */
  bool ProcessingInterface::Initialize(QString paramFilePath)
  {
    QString libPath = FileSystemUtil::getPathFromInstallationPath("libs");
    QString libFilePath = libPath + "/" + Constants::ACQ_LIB_NAME;
    QFileInfo fileInfo(libFilePath);
    if(fileInfo.exists())
    {
      SPIN_LOG_INFO("Found library.");
    }
    else
    {
      SPIN_LOG_ERROR("Could found library.");
    }
    SPIN_LOG_DEBUG(QObject::tr("Library file path: %1").arg(libFilePath));

    // And Instantiate the loader
    m_lib = std::make_shared<dlibxx::handle>();
    // Resolve symbols when they are referenced instead of on load.
    m_lib->resolve_policy(dlibxx::resolve::lazy);
    // Make symbols available for resolution in subsequently loaded libraries.
    m_lib->set_options(dlibxx::options::global);
    // Load the library specified.
    m_lib->load(libFilePath.toStdString());
    // Check if the library could be loaded.
    if (!m_lib->loaded())
    {
      SPIN_LOG_ERROR(QObject::tr("Library could not be loaded: %1").
                     arg(m_lib->error().c_str()));
      return false;
    }

    // Now, we try and create the plugin.
    m_pModule = m_lib->template create<AbstractVistaModule>("Vista_Module");
    // Check if the module was loaded.
    if (!m_pModule.get())
    {
      SPIN_LOG_ERROR("Module could not be initialize.");
      return false;
    }

    // Instantiate the pipeline by parsing a parameter file.
    int v = m_pModule.get()->Initialize(paramFilePath.toStdString().c_str(), false);
    if (v !=1)
    {
      SPIN_LOG_ERROR("Parameter file could not be parsed.");
      return false;
    }

    return true;
  }

  /**
   * @brief Process To process the received data.
   * @param _imgData Pointer to the image data that requires to be processed.
   * @param gx_colIdx Column index of the data along the x-direction.
   * @param gy_rowIdx Row index of the data along the y-direction
   */
  void ProcessingInterface::Process(void* _imgData,
                                    float gx_colIdx, float gy_rowIdx)
  {
    // Process it; By default we assume that it is a fg image
    m_pModule.get()->ProcessGridLocations(_imgData,gx_colIdx, gy_rowIdx, 0);
  }

  /**
   * @brief CompleteProcessing To ensure that available modules have completed
   * their processing before exiting.
   */
  void ProcessingInterface::CompleteProcessing()
  {
    // Complete the process
    m_pModule.get()->CompleteProcessGridLocations(false);
  }

  /**
   * @brief AbortProcessing To abort the processing.
   */
  void ProcessingInterface::AbortProcessing()
  {
    m_pModule.get()->AbortProcessGridLocations(false);
  }

  /**
   * @brief ~ProcessingInterface Class destructor, destroys objects owned by
   * the class.
   */
  ProcessingInterface::~ProcessingInterface()
  {

  }
}
