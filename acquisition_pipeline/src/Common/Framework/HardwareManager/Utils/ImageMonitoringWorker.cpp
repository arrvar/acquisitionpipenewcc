#include <QThread>
#include <QDebug>

#include "Common/Framework/HardwareManager/Utils/ImageMonitoringWorker.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"

namespace spin
{
  /**
   * @brief ImageMonitoringWorker Default class constructor, initializes objects
   * owned by the class.
   * @param parent Pointer to a oarent QObject class.
   */
  ImageMonitoringWorker::ImageMonitoringWorker(QObject* parent) :
    QObject(parent)
  {

  }

  /**
   * @brief onStartWorker Slot called to start the task of the worker.
   */
  void ImageMonitoringWorker::onStartWorker()
  {
    const int Remove_Images_From_Stack =
        CONFIG()->intConfig(ConfigKeys::REMOVE_IMAGES_FROM_STACK);

    QList <int> aoiIdList;
    QList <int> bestIdxList;
    QList <double> focusMetricList;
    QList <double> colorMetricList;

    cv::Mat mergedImg;
    // Split the image into four different channels.
    std::vector <cv::Mat> channels;

    for(int idx = 0; idx < m_aoiStack.size(); idx++)
    {
      qDebug() << "Processing AoI: " << m_aoiStack.at(idx).m_name;
      // If a stack has been captured then pick the best image.
      if(m_aoiStack.at(idx).m_zStack.isEmpty() == false)
      {
        int bestIdx = 0;
        double bestFocusMetric = 0.0;

        const int stackSize = m_aoiStack.at(idx).m_zStack.size();
        for(int stackIdx = Remove_Images_From_Stack;
            stackIdx < stackSize;
            stackIdx++)
        {
          if(!CONFIG()->intConfig(ConfigKeys::INLINE_FM_CALC))
          {
            cv::Mat stackImage(cv::Size(Constants::FRAME_WIDTH,
                                        Constants::FRAME_HEIGHT), CV_8UC4,
                               m_aoiStack[idx].m_zStack[stackIdx]);

            // Split the image into four different channels.
            channels.reserve(4);
            cv::split(stackImage, channels);

            // Compute the focus metric of the image using the green channel.
            double focusMetric = Utilities::computeFocusMetric(channels[1]);
            qDebug() << stackIdx << ": Metric value: " << focusMetric;
            if(focusMetric > bestFocusMetric)
            {
              // Remove the last channel.
              channels[3].release();
              channels.pop_back();

              // Merge the three channels.
              mergedImg.release();
              cv::merge(channels, mergedImg);

              bestFocusMetric = focusMetric;
              bestIdx = stackIdx;
            }

            for(int chIdx = 0; chIdx < channels.size(); chIdx++)
            {
              channels[chIdx].release();
            }
            channels.clear();
          }
          else
          {
            if(stackIdx == m_aoiStack.at(idx).m_bestIdx)
            {
              cv::Mat stackImage(cv::Size(Constants::FRAME_WIDTH,
                                          Constants::FRAME_HEIGHT), CV_8UC4,
                                 m_aoiStack[idx].m_zStack[stackIdx]);

              // Split the image into four different channels.
              channels.reserve(4);
              cv::split(stackImage, channels);

              // Remove the last channel.
              channels[3].release();
              channels.pop_back();

              // Merge the three channels.
              mergedImg.release();
              cv::merge(channels, mergedImg);

              for(int chIdx = 0; chIdx < channels.size(); chIdx++)
              {
                channels[chIdx].release();
              }
              channels.clear();
            }
          }

          if(!CONFIG()->intConfig(ConfigKeys::WRITE_STACK))
          {
            delete[] m_aoiStack[idx].m_zStack[stackIdx];
            m_aoiStack[idx].m_zStack[stackIdx] = Q_NULLPTR;
          }
        }

        if(CONFIG()->intConfig(ConfigKeys::INLINE_FM_CALC))
        {
          bestIdx = m_aoiStack.at(idx).m_bestIdx;
        }
        else
        {
          m_aoiStack[idx].m_focusMetric = bestFocusMetric;
        }
        bestIdxList.append(bestIdx);
        qDebug() << "Found best image at idx: " << bestIdx;

        if(!CONFIG()->intConfig(ConfigKeys::WRITE_STACK))
        {
          m_aoiStack[idx].m_zStack.clear();
        }
      }
      else
      {
        cv::Mat capturedImage(cv::Size(Constants::FRAME_WIDTH,
                                       Constants::FRAME_HEIGHT), CV_8UC4,
                              m_aoiStack[idx].m_ptr);
        // Split the image into four different channels.
        channels.reserve(4);
        cv::split(capturedImage, channels);

        // Remove the last channel.
        channels[3].release();
        channels.pop_back();

        // Merge the three channels.
        cv::merge(channels, mergedImg);

        // Compute the focus metric of the image using the green channel.
        m_aoiStack[idx].m_focusMetric =
            Utilities::computeFocusMetric(channels[1]);
        bestIdxList.append(-1);
      }

      delete[] m_aoiStack[idx].m_ptr;
      m_aoiStack[idx].m_ptr = Q_NULLPTR;

      m_aoiStack[idx].m_colorMetric =
          Utilities::computeColorMetric(mergedImg, spin::TISSUE_BIOPSY);
      if(CONFIG()->intConfig(ConfigKeys::WRITE_IMAGES))
      {
        // Create a new buffer.
        acquisition::AoIInfo* aoiInfo = new acquisition::AoIInfo();
        *aoiInfo = m_aoiStack.at(idx);
        aoiInfo->m_img = mergedImg.clone();

        // Add to the shared buffer.
        SHARED_WRITE_BUFFER()->add(aoiInfo);
        SHARED_WRITE_BUFFER()->resume();
      }

      if(CONFIG()->intConfig(ConfigKeys::ENABLE_STITCHING))
      {
        // Create a new buffer.
        acquisition::AoIInfo* aoiInfoProc = new acquisition::AoIInfo();
        *aoiInfoProc = m_aoiStack.at(idx);
        aoiInfoProc->m_img = mergedImg.clone();

        // Add to the shared buffer.
        SHARED_PROCESS_BUFFER()->add(aoiInfoProc);
        SHARED_PROCESS_BUFFER()->resume();
      }

      mergedImg.release();
      for(int idx = 0; idx < channels.size(); idx++)
      {
        channels[idx].release();
      }
      channels.clear();

      aoiIdList.append(m_aoiStack.at(idx).m_id);
      focusMetricList.append(m_aoiStack.at(idx).m_focusMetric);
      colorMetricList.append(m_aoiStack.at(idx).m_colorMetric);

      qDebug() << "Completed processing AoI: " << m_aoiStack.at(idx).m_name;
    }

    m_aoiStack.clear();

    Q_EMIT workerFinished(aoiIdList, bestIdxList, focusMetricList,
                          colorMetricList);

    if(thread())
    {
      thread()->quit();
    }
  }

  /**
   * @brief ~ImageMonitoringWorker Default class destructor, destorys objects
   * owned by the class.
   */
  ImageMonitoringWorker::~ImageMonitoringWorker()
  {

  }
}
