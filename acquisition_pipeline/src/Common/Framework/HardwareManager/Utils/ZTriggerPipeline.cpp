#include <chrono>

#include <QElapsedTimer>

#include "Common/Framework/HardwareManager/Utils/ZTriggerPipeline.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"

#define INCLUDE_TIMER 1

namespace spin
{
  // Maximum stack capture attempts.
  const int ZTriggerPipeline::MAX_RETRY_ATTEMPTS = 5;

  // Focus metric threshold for a background image.
  const double ZTriggerPipeline::BG_FM_THRESHOLD = 4.5;

  /**
   * @brief ZTriggerPipeline Default class constructor, initializes the
   * objects of the class.
   * @param Pointer instance of the parent object.
   */
  ZTriggerPipeline::ZTriggerPipeline(QObject* parent) :
    QObject(parent),
    m_specType(spin::SPECIMEN_UNKNOWN),
    m_startZ(0.0),
    m_stopZ(0.0),
    m_stepSize(0.0),
    m_bestFocusMetric(0.0)
  {

  }

  /**
   * @brief captureStack To capture a Z-stack.
   * @param stackSize Size of the image stack.
   * @param zTriggerStepSize Trigger step size in steps.
   * @param zStageSpeed Z-Stage speed in trigger mode.
   * @return Returns true if the stack capture is successful else returns
   * false.
   */
  bool ZTriggerPipeline::captureStack(int stackSize, double zTriggerStepSize,
                                      double zStageSpeed)
  {
#if INCLUDE_TIMER
    QElapsedTimer captureStackTimer;
    captureStackTimer.start();
#endif

    // Move to start position on the Z-Stage.
    double startZ = m_startZ - m_stepSize;
    // Starting from 1 Z-Stage position back to ensure that the image capture
    // starts from the m_startZ position.
    HW_MANAGER()->setZPosition(startZ, true);

#if INCLUDE_TIMER
    SPIN_LOG_DEBUG(QObject::tr("Set Z start position time (sync): %1").
                   arg(captureStackTimer.elapsed()/1000.0, 0, 'g', 10));

    captureStackTimer.restart();
#endif

    // Enable trigger mode.
    HW_MANAGER()->enableZTriggerMode(zTriggerStepSize);

#if INCLUDE_TIMER
    SPIN_LOG_DEBUG(QObject::tr("Enable Z trigger time: %1").
                   arg(captureStackTimer.elapsed()/1000.0, 0, 'g', 10));

//    captureStackTimer.restart();
#endif

    // Set continous mode.
//    HW_MANAGER()->setContinuousMode(Z_TRIGGER);

//#if INCLUDE_TIMER
//    SPIN_LOG_DEBUG(QObject::tr("Set continuous mode time: %1").
//                   arg(captureStackTimer.elapsed()/1000.0, 0, 'g', 10));
//#endif

    // Set the Z-Stage speed for trigger mode.
    HW_MANAGER()->setZStageSpeed(zStageSpeed);

    m_capThread.init(stackSize);

    m_capThread.start(QThread::TimeCriticalPriority);

#if INCLUDE_TIMER
    captureStackTimer.restart();
#endif

    // Move to stop position on the Z-Stage.
    HW_MANAGER()->setZPosition(m_stopZ);

#if INCLUDE_TIMER
    SPIN_LOG_DEBUG(QObject::tr("Set Z stop position time (async): %1").
                   arg(captureStackTimer.elapsed()/1000.0, 0, 'g', 10));

    captureStackTimer.restart();
#endif

    m_capThread.wait();

#if INCLUDE_TIMER
    double capTime = captureStackTimer.elapsed()/1000.0;
    SPIN_LOG_DEBUG(QObject::tr("Thread wait time: %1").
                   arg(captureStackTimer.elapsed()/1000.0, 0, 'g', 10));
    SPIN_LOG_DEBUG(QObject::tr("Stack Actual FPS: %1").
                   arg(stackSize/capTime, 0, 'g', 10));
#endif

    // Set the Z-Stage to orignial speed.
    HW_MANAGER()->setZStageSpeed(CONFIG()->realConfig(spin::ConfigKeys::
                                                      MAX_Z_STAGE_SPEED));

    // Attempt stack capture in trigger mode.
    bool captureStatus = m_capThread.captureState();

#if INCLUDE_TIMER
    captureStackTimer.restart();
#endif

    // Set the Z-Stage trigger mode to OFF.
    HW_MANAGER()->disableTriggerMode();

#if INCLUDE_TIMER
    SPIN_LOG_DEBUG(QObject::tr("Disabling trigger mode time: %1").
                   arg(captureStackTimer.elapsed()/1000.0, 0, 'g', 10));

//    captureStackTimer.restart();
#endif

    // Stop the continuous mode.
//    HW_MANAGER()->stopContinuousMode();

//#if INCLUDE_TIMER
//    SPIN_LOG_DEBUG(QObject::tr("Stopping continuous mode time: %1").
//                   arg(captureStackTimer.elapsed()/1000.0, 0, 'g', 10));
//#endif

    if(captureStatus == false)
    {
#if INCLUDE_TIMER
    captureStackTimer.restart();
#endif

      // Stop the Z-Stage.
      HW_MANAGER()->stopZStage();

#if INCLUDE_TIMER
    SPIN_LOG_DEBUG(QObject::tr("Stopping Z-stage time: %1").
                   arg(captureStackTimer.elapsed()/1000.0, 0, 'g', 10));
#endif
    }

#if INCLUDE_TIMER
    captureStackTimer.invalidate();
#endif
    return captureStatus;
  }

  /**
   * @brief findBestImage To find the best focused image in the stack.
   * @return Returns the index of the best image in the stack. If no best
   * image is available then -1 is returned.
   */
  int ZTriggerPipeline::findBestImage()
  {
    int bestIdx = -1;
    m_bestFocusMetric = 0.0;
    m_bestImage.release();
    for(int idx = 0 ; idx < m_capThread.m_imageStack.size(); idx++)
    {
      cv::Mat imgPtr = m_capThread.m_imageStack[idx];

      cv::Mat splitImage[4];
      // Split the image into four different channels.
      cv::split(imgPtr, splitImage);
      // Currently using only green channel.
      double focusMetricVal = Utilities::computeFocusMetric(splitImage[1]);
      double zVal = m_startZ + (idx * m_stepSize);
      SPIN_LOG_INFO(QObject::tr("Z-Stage Value: %1   Focus Metric Value: %2").
                    arg(zVal, -8, 'f', 3, '0').
                    arg(focusMetricVal, 0, 'f', 10));
      if(m_bestFocusMetric < focusMetricVal &&
         focusMetricVal <= CONFIG()->
         realConfig(ConfigKeys::FOCUS_METRIC_THRESHOLD))
      {
        m_bestFocusMetric = focusMetricVal;
        bestIdx = idx;
        m_bestImage = m_capThread.m_imageStack[idx];
      }
    }

    return bestIdx;
  }

  /**
   * @brief processOutput To process the output of the stack capture.
   * @param bgState True indicates that it is a background and false indicates
   * that it is a foreground.
   * @param bestIdx Index of the best image in the stack.
   * @return Returns the Z-Stage value, region type (if blood smear),
   * focus metric value and the path where the image is written.
   */
  std::tuple<double, int, double, std::string> ZTriggerPipeline::
    processOutput(bool bgState, int bestIdx)
  {
    int regionType = -1;
    QString imgName, imgPath, imgType;
    double zPos = (bestIdx * m_stepSize) + m_startZ;;
    // If blood smear, offset is substracted.
    if(m_specType == BLOOD_SMEAR)
    {
      // Keeping it a multiple of the microstepping.
      const double OFFSET = 3.125;
      zPos -= OFFSET;
      if(zPos < 0)
      {
        zPos = (bestIdx * m_stepSize) + m_startZ;
      }
    }

    imgName.sprintf("X%f_Y%f_Z%f.jpg", m_xyPos.x(), m_xyPos.y(), zPos);
    if(bgState == true)
    {
      QString debris_detection_path = FileSystemUtil::
          getPathFromInstallationPath("etc/debris_data");
      imgName.prepend("BG");
      imgType = "background";
      imgPath = debris_detection_path + "/" + imgName;
    }
    else
    {
      QString focus_sampling_path = FileSystemUtil::
          getPathFromInstallationPath("etc/sampling_focus");
      imgPath = focus_sampling_path + "/" + imgName;
      imgType = "foreground";
    }

    SPIN_LOG_DEBUG(QObject::tr("Image Path: %1").arg(imgPath));
    cv::imwrite(imgPath.toStdString(), m_bestImage);

    // Image path, x position, y position, z position, focus metric, type.
    QStringList samplingData;
    samplingData.append(imgPath);
    samplingData.append(QString::number(m_xyPos.x()));
    samplingData.append(QString::number(m_xyPos.y()));
    samplingData.append(QString::number(zPos));
    samplingData.append(QString::number(m_bestFocusMetric));
    samplingData.append(imgType);

    QString samplingFilePath = FileSystemUtil::
        getPathFromInstallationPath("etc/focus_sampling.txt");
    Utilities::writeStatus(samplingFilePath, samplingData.join(","));
    if(bgState == true)
    {
      return std::make_tuple(-1, regionType, m_bestFocusMetric,
                             imgPath.toStdString());
    }
    else
    {
      return std::make_tuple(zPos, regionType, m_bestFocusMetric,
                             imgPath.toStdString());
    }
  }

  /**
   * @brief init To initialize the Z-Trigger pipeline.
   * @param specType Specimen type.
   * @param startZ Z-Stage start position in microns.
   * @param stopZ Z-Stage stop position in microns.
   * @param stepSize Z-Stage step size in microns.
   * @param xPos X-Stage position in microns.
   * @param yPos Y-Stage position in microns.
   * @param slideName Name of the slide.
   */
  void ZTriggerPipeline::init(SpecimenType specType, double startZ,
                              double stopZ, double stepSize, double xPos,
                              double yPos, QString slideName)
  {
    m_specType = specType;
    m_startZ = startZ;
    m_stopZ = stopZ;
    m_stepSize = stepSize;
    m_xyPos = QPointF(xPos, yPos);
    m_slideName = slideName;
  }

  /**
   * @brief process To start the Z-Trigger pipeline.
   * @return Returns the Z-Stage value, region type (if blood smear),
   * focus metric value and the path where the image is written.
   */
  std::tuple<double, int, double, std::string> ZTriggerPipeline::process()
  {
#if INCLUDE_TIMER
    QElapsedTimer processTimer;
    processTimer.start();

    QElapsedTimer setXYTimer;
    setXYTimer.start();
#endif

    // Move to sampling location.
    HW_MANAGER()->setXYPosition(m_xyPos.x(), m_xyPos.y());

#if INCLUDE_TIMER
    SPIN_LOG_DEBUG(QObject::tr("Set XY time: %1").
                   arg(setXYTimer.elapsed()/1000.0, 0, 'g', 10));
    setXYTimer.invalidate();
#endif

    // Compute image stack size.
    // +1 to ensure that the stack size is correct.
    // Assuming the start and stop Z-Stage values are multiples of the
    // microstepping.
    int imageStackSize = ((m_stopZ - m_startZ) / m_stepSize) + 1;

    // Compute trigger step size.
    double zStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::
                                                     STAGE_STEP_SIZE_Z);
    double zMicrostepping = CONFIG()->realConfig(ConfigKeys::
                                                 STAGE_MICROSTEPPING_Z);
    double zTriggerStepSize = m_stepSize / (zStepSizeInMicrons /
                                            zMicrostepping);

    // Compute the Z-Stage speed for stack capture.
    double stepSizeInMm = m_stepSize / 1000.0;
    double zStageSpeed = stepSizeInMm * CONFIG()->realConfig(ConfigKeys::
                                                             Z_TRIGGER_CAM_FPS);

    double bgFmThreshold = 0.0;
    if(m_specType == BLOOD_SMEAR)
    {
      bgFmThreshold = CONFIG()->realConfig(ConfigKeys::BG_FM_BLOOD_SMEAR);
      SPIN_LOG_DEBUG(QObject::tr("Focus metric threshold for blood smear "
                                 "background images: %1").arg(bgFmThreshold));
    }
    else if(m_specType == TISSUE_BIOPSY)
    {
      bgFmThreshold = CONFIG()->realConfig(ConfigKeys::
                                           BG_FM_TISSUE_BIOPSY);
      SPIN_LOG_DEBUG(QObject::tr("Focus metric threshold for tissue "
                                 "biopsy background images: %1").
                     arg(bgFmThreshold));
    }
    else
    {
      bgFmThreshold = ZTriggerPipeline::BG_FM_THRESHOLD;
      SPIN_LOG_DEBUG(QObject::tr("Unknow specimen type, using default "
                                 "focus metric threshold for background "
                                 "images: %1").arg(bgFmThreshold));
    }

#if INCLUDE_TIMER
    QElapsedTimer triggerAttemptTimer;
    triggerAttemptTimer.start();
#endif

    // Attempt stack capture maximum of defined value.
    bool isBg = false;
    int bestIdx = -1;
    for(int retryCount = 0; retryCount < ZTriggerPipeline::MAX_RETRY_ATTEMPTS;
        retryCount++)
    {
      SPIN_LOG_INFO(QObject::tr("Trigger attempt %1 of %2.").
                    arg(retryCount + 1).
                    arg(ZTriggerPipeline::MAX_RETRY_ATTEMPTS));

      if(captureStack(imageStackSize, zTriggerStepSize, zStageSpeed))
      {
#if INCLUDE_TIMER
        QElapsedTimer findBestImageTimer;
        findBestImageTimer.start();
#endif

        bestIdx = findBestImage();

#if INCLUDE_TIMER
        SPIN_LOG_DEBUG(QObject::tr("Find best image time: %1").
                       arg(findBestImageTimer.elapsed()/1000.0, 0, 'g', 10));
        findBestImageTimer.invalidate();
#endif

        if(bestIdx > -1)
        {
#if INCLUDE_TIMER
          QElapsedTimer bgCheckTimer;
          bgCheckTimer.start();
#endif

          // Check if it is a foreground.
          bool bgCheck = Utilities::isBackground(m_bestImage, m_specType);

#if INCLUDE_TIMER
          SPIN_LOG_DEBUG(QObject::tr("Bg check time: %1").
                         arg(bgCheckTimer.elapsed()/1000.0, 0, 'g', 10));
          bgCheckTimer.invalidate();
#endif

          if((m_bestFocusMetric > bgFmThreshold) || (bgCheck == false))
          {
            // If best image is at boundaries.
            if((bestIdx < 2) || (bestIdx > (imageStackSize - 3)))
            {
              // Try again with a smaller window.
              // Doing the -1 and /2.0 to ensure that a stack size of the
              // samplingrange is captured. Doing similar computation in python
              // where determine the start and stop Z-Stage positions from a
              // reference Z-Stage position.
              const int samplingRange = imageStackSize;
              double currZ = m_startZ + (bestIdx * m_stepSize);
              m_startZ = currZ - ((samplingRange-1)/2.0 * m_stepSize);
              m_stopZ = currZ + ((samplingRange-1)/2.0 * m_stepSize);
            }
            else
            {
              break;
            }
            // It is a foreground.
            isBg = false;
          }
          else
          {
            // It is a background.
            isBg = true;
            break;
          }
        }
        else
        {
          // Trigger might have failed. Trying again.
        }
      }
    }

#if INCLUDE_TIMER
    SPIN_LOG_DEBUG(QObject::tr("Trigger attempts time: %1").
                   arg(triggerAttemptTimer.elapsed()/1000.0, 0, 'g', 10));
    triggerAttemptTimer.invalidate();
#endif

    std::tuple<double, int, double, std::string> metricInfo;
    if(bestIdx > -1)
    {
#if INCLUDE_TIMER
      QElapsedTimer processOutputTimer;
      processOutputTimer.start();
#endif

      metricInfo = processOutput(isBg, bestIdx);

#if INCLUDE_TIMER
      SPIN_LOG_DEBUG(QObject::tr("Process output time: %1").
                     arg(processOutputTimer.elapsed()/1000.0, 0, 'g', 10));
      processOutputTimer.invalidate();
#endif
    }
    else
    {
      metricInfo = std::make_tuple(-1, -1, 0, QString().toStdString());
    }

#if INCLUDE_TIMER

    SPIN_LOG_DEBUG(QObject::tr("Time taken for Focus at Sampling "
                               "Position for %1 : %2").
                   arg(m_slideName).
                   arg(processTimer.elapsed()/1000.0, 0, 'g', 10));
    processTimer.invalidate();
#endif

    return metricInfo;
  }

  /**
   * @brief ~ZTriggerPipeline Default class destructor, destroys objects
   * owned by the class.
   */
  ZTriggerPipeline::~ZTriggerPipeline()
  {
    m_bestImage.release();
  }
}
