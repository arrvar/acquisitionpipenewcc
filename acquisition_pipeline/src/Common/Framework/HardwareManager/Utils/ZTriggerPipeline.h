#ifndef ZTRIGGERPIPELINE_H
#define ZTRIGGERPIPELINE_H

#include <tuple>

#include <QObject>
#include <QPointer>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"
#include "Common/Framework/HardwareManager/Utils/ZCaptureThread.h"

namespace spin
{

  /**
   * @brief The ZTriggerPipeline class handles the capturing of a Z-stack
   * and finding the best image in the Z-stack.
   */
  class ZTriggerPipeline : public QObject
  {
  public:
    /**
     * @brief ZTriggerPipeline Default class constructor, initializes the
     * objects of the class.
     * @param parent Pointer instance of the parent object.
     */
    ZTriggerPipeline(QObject* parent = 0);

    /**
     * @brief ~ZTriggerPipeline Default class destructor, destroys objects
     * owned by the class.
     */
    ~ZTriggerPipeline();

    /**
     * @brief init To initialize the Z-Trigger pipeline.
     * @param specType Specimen type.
     * @param startZ Z-Stage start position in microns.
     * @param stopZ Z-Stage stop position in microns.
     * @param stepSize Z-Stage step size in microns.
     * @param xPos X-Stage position in microns.
     * @param yPos Y-Stage position in microns.
     * @param slideName Name of the slide.
     */
    void init(SpecimenType specType, double startZ, double stopZ,
              double stepSize, double xPos, double yPos, QString slideName);

    /**
     * @brief process To start the Z-Trigger pipeline.
     * @return Returns the Z-Stage value, region type (if blood smear),
     * focus metric value and the path where the image is written.
     */
    std::tuple<double, int, double, std::string> process();

  private:
    /**
     * @brief captureStack To capture a Z-stack.
     * @param stackSize Size of the image stack.
     * @param zTriggerStepSize Trigger step size in steps.
     * @param zStageSpeed Z-Stage speed in trigger mode.
     * @return Returns true if the stack capture is successful else returns
     * false.
     */
    bool captureStack(int stackSize, double zTriggerStepSize,
                      double zStageSpeed);

    /**
     * @brief findBestImage To find the best focused image in the stack.
     * @return Returns the index of the best image in the stack. If no best
     * image is available then -1 is returned.
     */
    int findBestImage();

    /**
     * @brief processOutput To process the output of the stack capture.
     * @param bgState True indicates that it is a background and false indicates
     * that it is a foreground.
     * @param bestIdx Index of the best image in the stack.
     * @return Returns the Z-Stage value, region type (if blood smear),
     * focus metric value and the path where the image is written.
     */
    std::tuple<double, int, double, std::string> processOutput(bool bgState,
                                                               int bestIdx);

  private:
    /**
     * @brief MAX_RETRY_ATTEMPTS Maximum stack capture attempts.
     */
    static const int MAX_RETRY_ATTEMPTS;

    /**
     * @brief BG_FM_THRESHOLD Focus metric threshold for background images.
     */
    static const double BG_FM_THRESHOLD;

    /**
     * @brief m_specType To store the specimen type being acquired.
     */
    SpecimenType m_specType;

    /**
     * @brief m_startZ Starting position of Z-Stage to capture(um).
     */
    double m_startZ;

    /**
     * @brief m_stopZ Stop position of Z-Stage till when to capture(um).
     */
    double m_stopZ;

    /**
     * @brief m_stepSize Distance between two images in a Z-stack.
     */
    double m_stepSize;

    /**
     * @brief m_bestFocusMetric To store the focus metric value of the best
     * image in a Z-stack.
     */
    double m_bestFocusMetric;

    /**
     * @brief m_xyPos XY-Stage position where the Z-stack will be captured.
     */
    QPointF m_xyPos;

    /**
     * @brief m_slideName Slide for which the Z-stack is being captured.
     */
    QString m_slideName;

    /**
     * @brief m_bestImage To store the best image in a Z-stack.
     */
    cv::Mat m_bestImage;

    /**
     * @brief m_capThread Object of the capture thread class.
     */
    ZCaptureThread m_capThread;

  };// end of ZTriggerPipeline class.
} // end of spin namespace.

#endif // ZTRIGGERPIPELINE_H
