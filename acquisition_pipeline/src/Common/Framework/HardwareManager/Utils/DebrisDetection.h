#ifndef DEBRISDETECTION_H
#define DEBRISDETECTION_H
#include <opencv2/opencv.hpp>

/**
 * @class DebrisDetection
 * @author shilpak
 * @date 28/02/18
 * @file DebrisDetection.h
 * @brief Checks for debris in an image
 * Returns 1 if debris is present and 0 if its not present
 */

namespace spin{
template<class T>
class DebrisDetection
{
  public:
  DebrisDetection(){};
  ~DebrisDetection(){};
  bool getDebris(cv::Mat inputImage);
  bool ConfirmDebris(cv::Mat image, cv::Mat enhGrayImg);
  
};
}
#endif
