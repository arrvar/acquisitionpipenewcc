#include <unistd.h>

#include <QThread>
#include <QElapsedTimer>

#include "Common/Framework/HardwareManager/Acquisition/CaptureWorker.h"
#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"
#include "Common/Framework/HardwareManager/Utils/TriggerBestZ.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"
#include "Common/Framework/HardwareManager/Utils/DebrisDetection.h"


namespace spin
{
  const int TriggerBestZ::MAX_RETRY_ATTEMPTS = 5;

  const double TriggerBestZ::BG_FM_THRESHOLD = 4.5;

    /**
    * @brief SnanpZThread Default class constructor, initializes objects
    * owned by the class.
    * @param parent
    */
    SnapZThread::SnapZThread(QObject* parent) :
    QThread(parent),
    m_err(false)
    {

    }

    void SnapZThread::run()
    {
      SPIN_LOG_METHOD();
        m_err = false;
        //focusStep size is in (um) and speed is in (mm/s)
        double camFps = CONFIG()->realConfig(ConfigKeys::Z_TRIGGER_CAM_FPS);
        double stepSizeInMm = m_focusStepSize / 1000.0;
        double zStageSpeed = stepSizeInMm * camFps;
        SPIN_LOG_DEBUG(QObject::tr("Z-Stage Speed: %1").
                       arg(zStageSpeed, 0, 'g', 10));
        HW_MANAGER()->setZStageSpeed(zStageSpeed);
        for(int idx = 0; idx < m_acqStackSize; idx++)
        {
            void* _imgPtr = HW_MANAGER()->snapImage();

            if(_imgPtr != NULL)
            {
                cv::Mat inputImage(cv::Size(Constants::FRAME_WIDTH,
                                            Constants::FRAME_HEIGHT),
                                            CV_8UC4, _imgPtr);
                //cv::imwrite(std::to_string(idx) + ".bmp",inputImage);
                m_imageStack[idx] = inputImage.clone();
            }
            else
            {
                m_err = true;
                SPIN_LOG_ERROR(QObject::tr("Snap missed at index %1 "
                                           "of total %2").arg(idx)
                                                         .arg(m_acqStackSize)
                               );
                break;
            }
            _imgPtr = NULL;
        }
        HW_MANAGER()->setZStageSpeed(m_normalSpeed);
        this->quit();
    }
    /**
    * @brief ~TriggerBestZ Default class destructor, destroys objects owned
    * by the class.
    */
    SnapZThread::~SnapZThread()
    {

    }

    /**
    * @brief TriggerBestZ Default class constructor, initializes objects
    * owned by the class.
    * @param parent
    */
    TriggerBestZ::TriggerBestZ(double xPos, double yPos, QObject* parent):
    QObject(parent),
      m_xyPos(xPos, yPos),
      m_retryCount(0),
      m_bestFocusMetric(0)
    {

    }

    int TriggerBestZ::getBestIndex()
    {
      int bestIdx = -1;
//      usleep(500);
      QElapsedTimer sampleTimer;
      sampleTimer.start();
      SPIN_LOG_DEBUG(QObject::tr("Stack Size: %1").arg(m_imageStackSize));
      if(m_szt.isRunning())
      {
          // Set the stop position
        SPIN_LOG_DEBUG("Moving to stopZ position.");
        double zStageSpeed_Before = HW_MANAGER()->getZStageSpeed();
        SPIN_LOG_DEBUG(QObject::tr("Z-Stage Speed before settings: %1").
                       arg(zStageSpeed_Before, 0, 'g', 10));
          HW_MANAGER()->setZPosition(m_stopZ);
          double zStageSpeed_After = HW_MANAGER()->getZStageSpeed();
          SPIN_LOG_DEBUG(QObject::tr("Z-Stage Speed before settings: %1").
                         arg(zStageSpeed_After, 0, 'g', 10));
          // Wait till thread is over
          m_szt.wait();
          zStageSpeed_After = HW_MANAGER()->getZStageSpeed();
          SPIN_LOG_DEBUG(QObject::tr("Again Z-Stage Speed before settings: %1").
                         arg(zStageSpeed_After, 0, 'g', 10));

          SPIN_LOG_DEBUG("Thread wait over.");
          m_bestFocusMetric = 0;
          // Error check
          double tt = sampleTimer.elapsed()/1000.0;
          SPIN_LOG_DEBUG(QObject::tr("Stack Capture Time: %1").arg(tt));
          SPIN_LOG_DEBUG(QObject::tr("Stack Actual FPS: %1").
                         arg(m_imageStackSize/tt));
          if(m_szt.m_err == false)
          {
              for(int idx = 0 ; idx < m_imageStackSize; idx++)
              {
                cv::Mat imgPtr = m_szt.m_imageStack[idx];

                cv::Mat splitImage[4];
                // Split the image into four different channels.
                cv::split(imgPtr, splitImage);
                //currently using only green channel
                double focusMetricVal = Utilities::computeFocusMetric(splitImage[1]);
                SPIN_LOG_INFO(QObject::tr("Focus Val %1 %2")
                              .arg(idx*m_focusStepSize+m_startZ, 0, 'g', 10)
                              .arg(focusMetricVal, 0, 'g', 10));
                if(m_bestFocusMetric < focusMetricVal &&
                   focusMetricVal <= CONFIG()->
                   realConfig(ConfigKeys::FOCUS_METRIC_THRESHOLD))
                {
                  m_bestFocusMetric = focusMetricVal;
                  bestIdx = idx;
                }
              }
          }
          else
          {
            SPIN_LOG_DEBUG("Z trigger failed.");
          }
      }

      HW_MANAGER()->disableTriggerMode();
//      HW_MANAGER()->stopContinuousMode();
      return bestIdx;
    }

    /**
    * @brief init To initialize the worker with startZ stopZ focusStepSize
    * and normalSpeed internally this should handle trigger movement speed and
    * return back with a normalSpeed.
    * @param startZ starting position of Z to capture(um).
    * @param stopZ stop position of Z till when to capture(um).
    * @param focusStepSize distance between two captures(um).
    * @param normalSpeed starting position of Z to capture(mm/s).
    */
    void TriggerBestZ::init(double startZ, double stopZ, double focusStepSize,
                            double normalSpeed, int specType)
    {
        m_startZ = startZ;
        m_stopZ = stopZ;
        m_focusStepSize = focusStepSize;
        m_normalSpeed = normalSpeed;
        m_specType = (SpecimenType)specType;

        // Calculate the stack size based on focus Steps and Range
//        m_imageStackSize = ((m_stopZ - m_startZ)/m_focusStepSize);
        m_imageStackSize = ((m_stopZ - m_startZ)/m_focusStepSize) + 1;

        // Release the OpenCV memory, if any.
        for(int idx = 0; idx < m_szt.m_imageStack.size(); idx++)
        {
          m_szt.m_imageStack[idx].release();
        }
        m_szt.m_imageStack.clear();

        // Resize the thread image Stack Size
        m_szt.m_imageStack.resize(m_imageStackSize);
        // Set the stop Z position
        double offsetStartZ = m_startZ - m_focusStepSize;
        HW_MANAGER()->setZPosition(offsetStartZ, true);
        //Initialize the vaiables
        m_szt.init(m_focusStepSize,m_normalSpeed,m_imageStackSize);
        // Enable trigger mode
        double zStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::
                                                         STAGE_STEP_SIZE_Z);
        double zMicrostepping = CONFIG()->realConfig(ConfigKeys::
                                                     STAGE_MICROSTEPPING_Z);
        HW_MANAGER()->enableZTriggerMode(m_focusStepSize / (zStepSizeInMicrons /
                                                            zMicrostepping));
        // Set continous mode
//        HW_MANAGER()->setContinuousMode(Z_TRIGGER);
        // Start the Capture Thread and wait for Trigger
        SPIN_LOG_DEBUG("Starting thread.");
        m_szt.start(QThread::TimeCriticalPriority);
//        QString regionDetectionInitFile = FileSystemUtil::
//            getPathFromInstallationPath("etc/WBCDetectionParams.xml");
//        m_regionDetect.InstantiatePipeline(regionDetectionInitFile.
//                                           toStdString().c_str());
    }

    /**
    * @brief onStartCapture Slot called to start capture.
    */
    std::tuple<double, int, double, std::string> TriggerBestZ::
      onStartCapture(QString slideName)
    {
        SPIN_LOG_INFO(QObject::tr("Trigger attempt %1 of %2.").
                      arg(1).
                      arg(TriggerBestZ::MAX_RETRY_ATTEMPTS));
        // Check if the Thread started
        QElapsedTimer sampleTimer;
        sampleTimer.start();
        int bestIdx = getBestIndex();

        bool isBg = true;
        qreal bgFmThreshold = 0.0;
        if(bestIdx != -1)
        {
          
          if(m_specType == BLOOD_SMEAR)
          {
            bgFmThreshold = CONFIG()->realConfig(ConfigKeys::BG_FM_BLOOD_SMEAR);
            SPIN_LOG_DEBUG(QObject::tr("Focus metric threshold for blood smear background images: %1").arg(bgFmThreshold));
          }
          else if(m_specType == TISSUE_BIOPSY)
          {
            bgFmThreshold = CONFIG()->realConfig(ConfigKeys::
                                                 BG_FM_TISSUE_BIOPSY);
            SPIN_LOG_DEBUG(QObject::tr("Focus metric threshold for tissue biopsy background images: %1").arg(bgFmThreshold));
          }
          else
          {
            bgFmThreshold = TriggerBestZ::BG_FM_THRESHOLD;
            SPIN_LOG_DEBUG(QObject::tr("Unknow specimen type, using default focus metric threshold for background images: %1").arg(bgFmThreshold));
          }

          if(m_bestFocusMetric > bgFmThreshold)
          {
            isBg = Utilities::isBackground(m_szt.m_imageStack[bestIdx],
                                           m_specType);
          }
        }

        // Attempt multiple triggers only if not a background.
        if(isBg == false)
        {
          for(int retryCount = 1; retryCount < TriggerBestZ::MAX_RETRY_ATTEMPTS;
              retryCount++)
          {
            // Check if the Thread started
            //to rerun with a smaller window if best found on edges
            //not doing a recursive call to avoid any unknown infinite loop
            //might change later to recursive
            if(bestIdx<2 || bestIdx>m_imageStackSize-3)
            {
              SPIN_LOG_INFO(QObject::tr("Trigger attempt %1 of %2.").
                            arg(retryCount + 1).
                            arg(TriggerBestZ::MAX_RETRY_ATTEMPTS));
              if(bestIdx==-1)
              {
                init(m_startZ, m_stopZ, m_focusStepSize,
                     m_normalSpeed, m_specType);
              }
              else
              {
                // Update startZ stopZ values with a smaller window
                // and run it again.
                const int samplingRange = 9;
                double currZ = m_startZ + (bestIdx * m_focusStepSize);
                double startZ = currZ - ((samplingRange - 1) / 2.0 * m_focusStepSize);
                double stopZ = currZ + ((samplingRange - 1) / 2.0 * m_focusStepSize);
//                init((bestIdx)*m_focusStepSize+m_startZ-5*m_focusStepSize,
//                     (bestIdx)*m_focusStepSize+m_startZ+5*m_focusStepSize,
//                     m_focusStepSize, m_normalSpeed, m_specType);
                init(startZ, stopZ, m_focusStepSize, m_normalSpeed, m_specType);
              }
              //updated values to be reflected after this including
              //m_imageStackSize, m_startZ and m_stopZ
              bestIdx = getBestIndex();
            }
            else
            {
              break;
            }
          }
        }

//        m_szt.quit();
//        while(m_szt.isRunning())
//        {
//          SPIN_LOG_DEBUG("Waiting for the trigger thread to stop.");
//        }
        int regionType = -1;
        if(bestIdx>=0)
        {
          bool isBG = false;
          bool isDebri = false;
          double x = m_xyPos.x(), y = m_xyPos.y(), z;
//          HW_MANAGER()->getXYPosition(x,y);
          QString imgName;
          z = bestIdx * m_focusStepSize + m_startZ;

          // If bloodSmear offset is substracted
          if(m_specType == BLOOD_SMEAR)
          {
            SPIN_LOG_INFO(QObject::tr("Blood Smear: Z changed with Before "
                                      "OFFSET: %1").arg(z));
            // To come from calibration file
            int OFFSET = 3;
            z = z - OFFSET;
            if(z < 0)
            {
              z = bestIdx * m_focusStepSize + m_startZ;
            }
            SPIN_LOG_INFO(QObject::tr("Blood Smear: Z changed with after "
                                      "OFFSET %1.").arg(z));
          }

          imgName.sprintf("X%f_Y%f_Z%f.jpg", x, y, z);
          if(m_bestFocusMetric > bgFmThreshold)
          {
            isBG = Utilities::isBackground(m_szt.m_imageStack[bestIdx],
                                           m_specType);
          }
          else
          {
            isBG = true;
          }

          if(m_specType==BLOOD_SMEAR && !isBG)
          {
//            std::string s = "";
            cv::Mat BGRImg;
            cv::cvtColor(m_szt.m_imageStack[bestIdx],BGRImg,cv::COLOR_BGRA2BGR);
//            regionType = m_regionDetect.ProcessPipeline(&BGRImg, s);
          }
          QString imagePath;
          QString imgType;
          if (isBG == true)
          {
            QString debris_detection_path = FileSystemUtil::
                getPathFromInstallationPath("etc/debris_data");
            imgName.prepend("BG");
            imgType = "background";
            imagePath = debris_detection_path+"/"+imgName;
          }
          else if(isDebri == true)
          {
            QString debris_detection_path = FileSystemUtil::
                getPathFromInstallationPath("etc/debris_data");
            imgName.prepend("DB");
            imgType = "debris";
            imagePath = debris_detection_path+"/"+imgName;
          }
          else
          {
            QString focus_sampling_path = FileSystemUtil::
                getPathFromInstallationPath("etc/sampling_focus");
            imagePath = focus_sampling_path+"/"+imgName;
            imgType = "foreground";
          }
          SPIN_LOG_DEBUG(QObject::tr("Image Path: %1").arg(imagePath));
          cv::imwrite(imagePath.toStdString(), m_szt.m_imageStack[bestIdx]);

          // image path, x position, y position, z position, focus metric, type.
          QStringList samplingData;
          samplingData.append(imagePath);
          samplingData.append(QString::number(x));
          samplingData.append(QString::number(y));
          samplingData.append(QString::number(z));
          samplingData.append(QString::number(m_bestFocusMetric));
          samplingData.append(imgType);

          QString samplingFilePath = FileSystemUtil::
              getPathFromInstallationPath("etc/focus_sampling.txt");
          Utilities::writeStatus(samplingFilePath, samplingData.join(","));
          SPIN_LOG_DEBUG(QObject::tr("Time taken for Focus at Sampling "
                                     "Position for %1 : %2").
                         arg(slideName).arg(sampleTimer.elapsed()/1000.0));

          // Release the OpenCV memory.
          for(int idx = 0; idx < m_szt.m_imageStack.size(); idx++)
          {
            m_szt.m_imageStack[idx].release();
          }
          m_szt.m_imageStack.clear();

          if (isBG == true || isDebri == true)
          {
            return std::make_tuple(-1,regionType,m_bestFocusMetric,
                                   imagePath.toStdString());
          }
          else
          {
            return std::make_tuple(z,regionType,m_bestFocusMetric,
                                   imagePath.toStdString());
          }
        }
        else
        {
          // Release the OpenCV memory.
          for(int idx = 0; idx < m_szt.m_imageStack.size(); idx++)
          {
            m_szt.m_imageStack[idx].release();
          }
          m_szt.m_imageStack.clear();

          //add provision for start stop mode here
          std::string imgPath;
          return std::make_tuple(-1, regionType,m_bestFocusMetric,imgPath);
        }
    }

    /**
    * @brief ~TriggerBestZ Default class destructor, destroys objects owned
    * by the class.
    */
    TriggerBestZ::~TriggerBestZ()
    {

    }
}
