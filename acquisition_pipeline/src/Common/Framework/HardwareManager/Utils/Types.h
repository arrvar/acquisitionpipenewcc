#ifndef HMTYPES_H
#define HMTYPES_H

namespace spin
{
  /**
   * @brief The TriggerMode enum represents the trigger mode for acquisition.
   */
  enum TriggerMode
  {
    UNKNOWN = -1,

    X_TRIGGER = 0,

    Y_TRIGGER = 1,

    Z_TRIGGER = 2,

    XY_TRIGGER = 3

  };// end of TriggerMode enum.

  /**
  * @brief The Magnification enum represents the magnifications for
  * acquisition.
  */
  enum Magnification
  {
    MAG_UNKNOWN = -1,

    MAG_HOME = 0,

    MAG_20X = 1,

    MAG_40X = 2,

    MAG_100X = 3,

    MAG_100X_OIL = 4

  };// end of Magnification enum.

  /**
   * @brief The ScanDirection enum represents the direction of scanning.
   */
  enum ScanDirection
  {
      SCANDIR_UNKNOWN = -1,

      SCANDIR_HORIZONTAL = 0,

      SCANDIR_VERTICAL = 1,

  };// end of ScanDirection enum.

  /**
   * @brief The ScanPattern enum represents the acquisition scan pattern.
   */
  enum ScanPattern
  {
      SCANPATT_UNKNOWN = -1,

      SCANPATT_SERPENTINE = 0,

      SCANPATT_ZIG_ZAG = 1

  };// end of ScanPattern enum.

  /**
   * @brief The RegionType enum represents the region types in a smear.
   */
  enum RegionType
  {
    REGION_UNKNOWN = -1,

    REGION_BACKGROUND = 0,

    REGION_MONOLAYER = 1,

    REGION_VERYTHIN = 2,

    REGION_THIN = 3,

    REGION_THICK = 4

  };// end of RegionType enum.

  /**
   * @brief The ProcessType enum represents the type of processing.
   */
  enum ProcessType
  {
    NO_PROCESSING = -1,

    DISP_EST = 0,

    RBC_ANALYSIS = 1,

    WBC_ANALYSIS = 2,

    PAP_ANALYSIS = 3,

    SPUTUM_ANALYSIS = 4,

    MICRONUCLEUS_ANALYSIS = 5,

  };

  /**
   * @brief The SpecimenType enum represents the specimen type for processing.
   */
  enum SpecimenType
  {
    SPECIMEN_UNKNOWN = -1,

    TISSUE_BIOPSY = 0,

    BLOOD_SMEAR = 2,

    BODY_FLUID = 3,

    BONE_MARROW = 4,

    PAP_SMEAR = 5,

    SPUTUM_SMEAR = 6,

    MICRONUCLEUS_TEST = 7

  };

} // end of spin namespace.

#endif // TYPES_H
