#include "Common/Framework/HardwareManager/Utils/SharedImageBuffer.h"

namespace spin
{
  /**
   * @brief SharedImageBuffer Default class constructor, initializes the
   * objects owned by the class.
   */
  SharedImageBuffer::SharedImageBuffer()
  {

  }

  /**
   * @brief add To add an image data to the queue.
   * @param data Image data.
   */
  void SharedImageBuffer::add(void* data)
  {
    m_buffer.add(data);
  }

  /**
   * @brief get To get the first item in the queue.
   * @return Returns a pointer to the first item from the queue.
   */
  void* SharedImageBuffer::get()
  {
    return m_buffer.get();
  }

  /**
   * @brief wait To wait until data is available in the queue for
   * consuming.
   */
  void SharedImageBuffer::wait()
  {
    m_mutex.lock();
    m_wc.wait(&m_mutex);
    m_mutex.unlock();
  }

  /**
   * @brief resume Resume the consuming of the data from the queue.
   */
  void SharedImageBuffer::resume()
  {
    m_mutex.lock();
    m_wc.wakeAll();
    m_mutex.unlock();
  }

  /**
   * @brief isEmpty To check if the queue is empty.
   * @return Returns true if the queue is empty else returns false.
   */
  bool SharedImageBuffer::isEmpty()
  {
    return m_buffer.isEmpty();
  }

  /**
   * @brief size To get the current size of the queue.
   * @return Returns the size of the queue.
   */
  int SharedImageBuffer::size()
  {
    return m_buffer.size();
  }

  /**
   * @brief ~SharedImageBuffer Default class destructor, destroys objects
   * owned by the class.
   */
  SharedImageBuffer::~SharedImageBuffer()
  {

  }
}
