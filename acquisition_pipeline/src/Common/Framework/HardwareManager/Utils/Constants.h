#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QString>

namespace spin
{
  /**
   * @brief The Constants class is to define the constants that can be accessed
   * globally.
   */
  class Constants
  {
  public:
    /**
     * @brief DATABASE_TYPE Type of the database used for the application.
     */
    static const QString DATABASE_TYPE;

    /**
     * @brief SLIDE_DATABASE_NAME Name of the slide database file.
     */
    static const QString SLIDE_DATABASE_NAME;

    /**
     * @brief WORKSPACE_FOLDER_NAME Name of the workspace folder.
     */
    static const QString WORKSPACE_FOLDER_NAME;

    /**
     * @brief FRAME_WIDTH Frame width of the MatrixVision camera in pixels.
     */
    static const int FRAME_WIDTH;

    /**
     * @brief FRAME_HEIGHT Frame height of the MatrixVision camera in pixels.
     */
    static const int FRAME_HEIGHT;

    /**
     * @brief VIRT_SLIDE_WIDTH Width of the virtual slide in pixels.
     */
    static const int VIRT_SLIDE_WIDTH;

    /**
     * @brief VIRT_SLIDE_HEIGHT Height of the virtual slide in pixels.
     */
    static const int VIRT_SLIDE_HEIGHT;

    /**
     * @brief SLIDE_WIDTH Width of a physical slide in microns.
     */
    static const int SLIDE_WIDTH;

    /**
     * @brief SLIDE_HEIGHT Height of a physical slide in microns.
     */
    static const int SLIDE_HEIGHT;

    /**
     * @brief ACQ_LIB_NAME Name of the acquisition library.
     */
    static const QString ACQ_LIB_NAME;

  private:
    /**
     * @brief Constants Private constructor, to prevent instantiation.
     */
    Constants()
    {

    }

  };// end of Constants class.
} // end of spin namespace.

#endif // CONSTANTS_H
