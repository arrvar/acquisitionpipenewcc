#ifndef HMSHAREDPROCESSBUFFER_H
#define HMSHAREDPROCESSBUFFER_H

#include <QWaitCondition>

#include "Common/Framework/HardwareManager/Utils/ImageBuffer.h"
#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

namespace spin
{
  /**
   * @brief The SharedProcessBuffer class is wrapper class around the ImageBuffer
   * class. This class manages the access to the ImageBuffer queue for
   * processing.
   */
  class SharedProcessBuffer
  {
    public:
      /**
       * @brief SharedProcessBuffer Default class constructor, initializes the
       * objects owned by the class.
       */
      SharedProcessBuffer();

      /**
       * @brief ~SharedProcessBuffer Default class destructor, destroys objects
       * owned by the class.
       */
      ~SharedProcessBuffer();

      /**
       * @brief add To add an image data to the queue.
       * @param data Image data.
       */
      void add(void* data);

      /**
       * @brief get To get the first item in the queue.
       * @return Returns a pointer to the first item from the queue.
       */
      void* get();

      /**
       * @brief wait To wait until data is available in the queue for
       * consuming.
       */
      void wait();

      /**
       * @brief resume Resume the consuming of the data from the queue.
       */
      void resume();

      /**
       * @brief isEmpty To check if the queue is empty.
       * @return Returns true if the queue is empty else returns false.
       */
      bool isEmpty();

      /**
       * @brief size To get the current size of the queue.
       * @return Returns the size of the queue.
       */
      int size();

    private:
      /**
       * @brief m_buffer Pointer to the ImageBuffer class object.
       */
      ImageBuffer <void*> m_buffer;

      /**
       * @brief m_mutex Mutex to guard the access to the image buffer pointer
       * object.
       */
      QMutex m_mutex;

      /**
       * @brief m_wc To synchronize the access to the image buffer queue.
       */
      QWaitCondition m_wc;

  };// end of SharedProcessBuffer class.
} // end of spin namespace.

#endif // HMSHAREDPROCESSBUFFER_H
