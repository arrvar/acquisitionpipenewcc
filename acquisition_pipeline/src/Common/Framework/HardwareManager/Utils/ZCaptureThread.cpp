#include <QElapsedTimer>

#include "Common/Framework/HardwareManager/Utils/ZCaptureThread.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"

#define INCLUDE_TIMER 0

namespace spin
{
  /**
   * @brief ZCaptureThread Default class constructor, initializes objects
   * owned by the class.
   * @param parent Pointer instance of the parent object.
   */
  ZCaptureThread::ZCaptureThread(QObject* parent) :
    QThread(parent),
    m_captureStatus(true)
  {

  }

  /**
   * @brief clearStack To delete memory allocated to the stack container.
   */
  void ZCaptureThread::clearStack()
  {
    for(int idx = 0; idx < m_imageStack.size(); idx++)
    {
      m_imageStack[idx].release();
    }
    m_imageStack.clear();
  }

  /**
   * @brief run Reimplemented method, where the Z-stack capturing will run
   * in the thread.
   */
  void ZCaptureThread::run()
  {
#if INCLUDE_TIMER
    QElapsedTimer captureThreadTimer;
    captureThreadTimer.start();
#endif
    for(int idx = 0; idx < m_imageStack.size(); idx++)
    {
      void* _imgPtr = HW_MANAGER()->snapImage();

      if(_imgPtr != NULL)
      {
        cv::Mat inputImage(cv::Size(Constants::FRAME_WIDTH,
                                    Constants::FRAME_HEIGHT),
                                    CV_8UC4, _imgPtr);
        m_imageStack[idx] = inputImage.clone();
      }
      else
      {
        m_captureStatus = false;
        SPIN_LOG_ERROR(QObject::tr("Snap missed at index %1 "
                                   "of total %2 in trigger mode").
                       arg(idx).arg(m_imageStack.size()));
        clearStack();
        break;
      }
    }
#if INCLUDE_TIMER
    SPIN_LOG_DEBUG(QObject::tr("Capture thread time: %1").
                   arg(captureThreadTimer.elapsed()/1000.0, 0, 'g', 10));
    captureThreadTimer.invalidate();
#endif
  }

  /**
   * @brief init Initializes and allocates memory to the stack storage
   * container.
   * @param stackSize Size of the image stack.
   */
  void ZCaptureThread::init(int stackSize)
  {
    clearStack();
    m_imageStack.resize(stackSize);
  }

  /**
   * @brief ~ZCaptureThread Default class destructor, destroys objects owned
   * by the class.
   */
  ZCaptureThread::~ZCaptureThread()
  {
    clearStack();
  }
}
