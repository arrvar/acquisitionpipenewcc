#ifndef IMAGEMONITORINGWORKER_H
#define IMAGEMONITORINGWORKER_H

#include <QObject>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

namespace spin
{
  // Forward declaration.
  class AutoFocus;

  /**
   * @brief The ImageMonitoringWorker class si a worker class that will write
   * images to the disk when available serially.
   */
  class ImageMonitoringWorker : public QObject
  {
    Q_OBJECT
  public:
    /**
     * @brief ImageMonitoringWorker Default class constructor, initializes objects
     * owned by the class.
     * @param parent Pointer to a parent QObject class.
     */
    explicit ImageMonitoringWorker(QObject* parent = 0);

    /**
     * @brief ~ImageMonitoringWorker Default class destructor, destorys objects
     * owned by the class.
     */
    ~ImageMonitoringWorker();

    /**
     * @brief initWorker To initialize the worker with the AoI stack.
     * @param aoiStack Reference to the AoI stack.
     */
    void initWorker(QList <acquisition::AoIInfo>& aoiStack)
    {
      m_aoiStack = aoiStack;
    }

  Q_SIGNALS:
    /**
     * @brief workerFinished Signal emitted when the worker has finished
     * its task.
     * @param aoiIdList List of AoI Ids.
     * @param bestIdxList List of best indices.
     * @param focusMetricList List of focus metric values.
     * @param colorMetricList List of color metric values.
     */
    void workerFinished(QList <int> aoiIdList,
                        QList <int> bestIdxList,
                        QList <double> focusMetricList,
                        QList <double> colorMetricList);

  public Q_SLOTS:
    /**
     * @brief onStartWorker Slot called to start the task of the worker.
     */
    void onStartWorker();

  private:
    /**
    * @brief m_autoFocus Instance of the AutoFocus class to compute focus
    * metric values.
    */
    spin::AutoFocus* m_autoFocus;

    /**
     * @brief m_aoiStack Container to store the AoI stack for writing and
     * processing.
     */
    QList <acquisition::AoIInfo> m_aoiStack;

  };// end of ImageMonitoringWorker class.
} // end of spin namespace.

#endif // IMAGEMONITORINGWORKER_H
