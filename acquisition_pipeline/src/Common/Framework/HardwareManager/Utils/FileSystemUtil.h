#ifndef HMFILESYSTEMUTIL_H
#define HMFILESYSTEMUTIL_H

#include <QString>

#include "Framework/HardwareManager/Utils/Common.h"

namespace spin
{
  /**
   * @brief The FileSystemUtil class consists of utilities methods for accessing
   * file system attributes (installation path, file paths etc.).
   */
  class FileSystemUtil
  {
  public:
    /**
      * @brief installationPath To retrieve the absolute installation path of
      * the application.
      * @return Returns the absolute installation path of the application.
      */
    static QString installationPath();

    /**
      * @brief getPathFromInstallationPath To retrieve the absolute path from the
      * given relative path.
      * @param relativePath The given relative path.
      * @return Returns the absolute path of the given relative path.
      */
    static QString getPathFromInstallationPath(QString relativePath);

    /**
     * @brief getPathFromWorkspacePath Takes a relative path of a file/directory
     * relative to workspace path and returns absolute path.
     * @param relativePath Path relative to workspace path.
     * @return Absolute path of the given relative path.
     */
    static QString getPathFromWorkspacePath(QString relativePath);

  private:
    /**
      * @brief FileSystemUtil Class constructor, declared in private to prevent
      * instantiation of the class from outside.
      */
    FileSystemUtil();

    DISALLOW_COPY_AND_ASSIGNMENT(FileSystemUtil)

  };// end of FileSystemUtil class.
} // end of spin namespace.

#endif // FILESYSTEMUTIL_H

