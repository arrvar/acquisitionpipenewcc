#ifndef PROCESSINGINTERFACE_H
#define PROCESSINGINTERFACE_H

#include <memory>

#include <QString>

// Forward declaration.
namespace dlibxx
{
  class handle;
}

namespace spin
{
  // Forward declaration.
  class AbstractVistaModule;

  /**
   * @brief The ProcessingInterface class is an interface between the
   * acquisition module and any processing module. All processing modules
   * should register with class to receive the acquired data for processing.
   */
  class ProcessingInterface
  {
  public:
    /**
     * @brief ProcessingInterface Class constructor, initializes objects owned
     * by the class.
     */
    ProcessingInterface();

    /**
     * @brief ~ProcessingInterface Class destructor, destroys objects owned by
     * the class.
     */
    ~ProcessingInterface();

    /**
     * @brief Initialize To load and initialize the processing module(s).
     * @param paramFilePath Absolute path to the parameter file.
     * @return Returns true on successful initialization of available processing
     * module(s).
     */
    bool Initialize(QString paramFilePath);

    /**
     * @brief Process To process the received data.
     * @param _imgData Pointer to the image data that requires to be processed.
     * @param gx_colIdx Column index of the data along the x-direction.
     * @param gy_rowIdx Row index of the data along the y-direction
     */
    void Process(void* _imgData, float gx_colIdx, float gy_rowIdx);

    /**
     * @brief CompleteProcessing To ensure that available modules have completed
     * their processing before exiting.
     */
    void CompleteProcessing();

    /**
     * @brief AbortProcessing To abort the processing.
     */
    void AbortProcessing();

  private:
    /**
     * @brief m_lib Processing library handle.
     */
    std::shared_ptr <dlibxx::handle> m_lib;

    /**
     * @brief m_pModule Abstract processing library module.
     */
    std::shared_ptr <AbstractVistaModule> m_pModule;

  };// end of AcquisitionProcessingInterface class.
} // end of spin namespace.

#endif // PROCESSINGINTERFACE_H
