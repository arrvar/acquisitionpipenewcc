#ifndef IMAGESTACKPROCESSINGWORKER_H
#define IMAGESTACKPROCESSINGWORKER_H

#include <QObject>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

namespace spin
{
  /**
   * @brief The ImageStackProcessingWorker class is a worker class that will
   * process stacks that need to written to the disk.
   */
  class ImageStackProcessingWorker : public QObject
  {
    Q_OBJECT
  public:
    /**
     * @brief ImageStackProcessingWorker Default class constructor,
     * initializes objects owned by the class.
     * @param parent Pointer to a parent QObject class.
     */
    explicit ImageStackProcessingWorker(QObject* parent = 0);

    /**
     * @brief ~ImageStackProcessingWorker Default class destructor,
     * destorys objects owned by the class.
     */
    ~ImageStackProcessingWorker();

    /**
     * @brief initWorker To initialize the worker with the AoI stack.
     * @param aoiStack Reference to the AoI stack.
     */
    void initWorker(acquisition::AoIInfo& aoiInfo)
    {
      m_aoiInfo = aoiInfo;
    }

  Q_SIGNALS:
    /**
     * @brief workerFinished Signal emitted when the worker has finished
     * its task.
     * @param aoiId Id of the AoI.
     * @param focusMetric Focus metric value of the AoI.
     * @param colorMetric Color metric value of the AoI.
     */
    void workerFinished(int aoiId, double focusMetric, double colorMetric);

  public Q_SLOTS:
    /**
     * @brief onStartWorker Slot called to start the task of the worker.
     */
    void onStartWorker();

  private:
    /**
     * @brief m_aoiStack Container to store the AoI stack for writing and
     * processing.
     */
    acquisition::AoIInfo m_aoiInfo;

  };// end of ImageStackProcessingWorker class.
} // end of spin namespace.

#endif // IMAGESTACKPROCESSINGWORKER_H
