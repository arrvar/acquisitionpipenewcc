#ifndef HMGETFOCUSPLANE_H
#define HMGETFOCUSPLANE_H

#include <QObject>
#include <QThread>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"
#include "Common/Framework/HardwareManager/Utils/TriggerBestZ.h"

namespace spin
{
    /**
    * @brief The GetFocusPlane class is a class for finding Z plane
    */
    class GetFocusPlane : public QObject
    {
        Q_OBJECT
        public:
            /**
            * @brief GetFocusPlane Default class constructor, initializes objects
            * owned by the class.
            * @param parent
            */
            GetFocusPlane(QObject* parent = 0);

            /**
            * @brief ~GetFocusPlane Default class destructor, destroys objects owned
            * by the class.
            */
            ~GetFocusPlane();

            /**
            * @brief init To initialize the worker with startZ stopZ focusStepSize and normalSpeed
            * internally this should handle trigger movement speed and return back with a normalSpeed.
            * @param startZ starting position of Z to capture(um).
            * @param stopZ stop position of Z till when to capture(um).
            * @param focusStepSize distance between two captures(um).
            * @param normalSpeed starting position of Z to capture(mm/s).
            */
            void init(double startZ, double stopZ, double focusStepSize,
                      double normalSpeed, int specType);

            Q_SIGNALS:
                /**
                * @brief captureFinished Signal emitted after capturing all the Z.
                */
                void captureFinished();

                /**
                * @brief captureError Signal emitted if an error occurs while capturing.
                */
                void captureError();

                public Q_SLOTS:
                /**
                * @brief onStartCapture Slot called to start capture.
                */
                double onStartCapture();

        private:
            /**
             * @brief getBestZ
             */
            double getBestZ();

            /**
             * @brief MAX_RETRY_ATTEMPTS
             */
            static const int MAX_RETRY_ATTEMPTS;

            /**
            * @brief m_specType To store the specimen type being acquired.
            */
            SpecimenType m_specType;

            /**
            * @brief m_startZ starting position of Z to capture(um)
            */
            double m_startZ;

            /**
            * @brief m_stopZ stop position of Z till when to capture(um)
            */
            double m_stopZ;

            /**
            * @brief m_focusStepSize distance between two captures(um)
            */
            double m_focusStepSize;

            /**
            * @brief m_imageStackSize how many images to capture
            */
            int m_imageStackSize;

            /**
            * @brief m_normalSpeed normalSpeed starting position of Z to capture(mm/s)
            */
            double m_normalSpeed;

            /**
            * @brief m_szt module to capture images in thread
            */
            SnapZThread m_szt;

            /**
            * @brief m_sign signed integer for direction of stack capture
            */
            int m_sign;

            /**
            * @brief chooseBestZ signed integer for direction of stack capture
            */
            double chooseBestZ(double bestZ1, double bestZ2);

    };// end of CaptureWorker class.
} // end of spin namespace.

#endif // HMGETFOCUSPLANE_H 
