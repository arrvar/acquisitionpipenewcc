#ifndef HMIMAGEWRITERWORKER_H
#define HMIMAGEWRITERWORKER_H

#include <QObject>
#include <QMutex>

#include <opencv2/opencv.hpp>

namespace spin
{
  /**
   * @brief The ImageWriterWorker class si a worker class that will write
   * images to the disk when available serially.
   */
  class ImageWriterWorker : public QObject
  {
    Q_OBJECT
  public:
    int stackCount = 1;
    float totTimeTaken = 0.0;
    float AOItotTimeTaken = 0.0;
    /**
     * @brief ImageWriterWorker Default class constructor, initializes objects
     * owned by the class.
     * @param parent Pointer to a parent QObject class.
     */
    explicit ImageWriterWorker(QObject* parent = 0);

    /**
     * @brief ~ImageWriterWorker Default class destructor, destorys objects
     * owned by the class.
     */
    ~ImageWriterWorker();

    /**
     * @brief init To initialize the worker with default parameters.
     * @param whiteRefPath Absolute path of the white reference image.
     */
    void init(QString whiteRefPath);

    void setTotalAoICount(int totalAoICount)
    {
      m_totalAoiCount = totalAoICount;
    }

  Q_SIGNALS:
    /**
     * @brief workerFinished Signal emitted when the worker has finished
     * its task.
     */
    void workerFinished();

    /**
     * @brief workerError Signal emitted when the worker fails to write.
     * @param errMsg Error message.
     */
    void workerError(QString errMsg);

  public Q_SLOTS:
    /**
     * @brief onStartWorker Slot called to start the task of the worker.
     */
    void onStartWorker();

    /**
     * @brief onStopWorker Slot called to stop the task of the worker.
     */
    void onStopWorker();

  private:
    /**
     * @brief writeImage To write the image to the disk.
     * @return Returns true if write was successful else returns false.
     */
    bool writeImage();

  private:
    /**
     * @brief m_stopWorker Flag to check if the worker has to stop its task.
     * True indicates that the task has to be stopped.
     * False indicates that the task should continue as is.
     */
    bool m_stopWorker;

    int m_writtenAoiCount;

    int m_totalAoiCount;



    /**
     * @brief m_stopMutex Mutex to synchronize access to the stop worker flag.
     */
    QMutex m_stopMutex;

    /**
     * @brief m_whiteImg To store the white reference image.
     */
    cv::Mat m_whiteImg;

  };// end of ImageWriterWorker class.
} // end of spin namespace.

#endif // IMAGEWRITERWORKER_H
