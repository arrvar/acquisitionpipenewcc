#include "Common/Framework/HardwareManager/Utils/SharedProcessBuffer.h"

namespace spin
{
  /**
   * @brief SharedProcessBuffer Default class constructor, initializes the
   * objects owned by the class.
   */
  SharedProcessBuffer::SharedProcessBuffer()
  {

  }

  /**
   * @brief add To add an image data to the queue.
   * @param data Image data.
   */
  void SharedProcessBuffer::add(void* data)
  {
    m_buffer.add(data);
  }

  /**
   * @brief get To get the first item in the queue.
   * @return Returns a pointer to the first item from the queue.
   */
  void* SharedProcessBuffer::get()
  {
    return m_buffer.get();
  }

  /**
   * @brief wait To wait until data is available in the queue for
   * consuming.
   */
  void SharedProcessBuffer::wait()
  {
    m_mutex.lock();
    m_wc.wait(&m_mutex);
    m_mutex.unlock();
  }

  /**
   * @brief resume Resume the consuming of the data from the queue.
   */
  void SharedProcessBuffer::resume()
  {
    m_mutex.lock();
    m_wc.wakeAll();
    m_mutex.unlock();
  }

  /**
   * @brief isEmpty To check if the queue is empty.
   * @return Returns true if the queue is empty else returns false.
   */
  bool SharedProcessBuffer::isEmpty()
  {
    return m_buffer.isEmpty();
  }

  /**
   * @brief size To get the current size of the queue.
   * @return Returns the size of the queue.
   */
  int SharedProcessBuffer::size()
  {
    return m_buffer.size();
  }

  /**
   * @brief ~SharedProcessBuffer Default class destructor, destroys objects
   * owned by the class.
   */
  SharedProcessBuffer::~SharedProcessBuffer()
  {

  }
}
