#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"

namespace spin
{
  /**
   * @brief SYS_SERIAL_NUM Serial number of the system.
   */
  const QString ConfigKeys::SYS_SERIAL_NUM = "SYS_SERIAL_NUM";

  /**
   * @brief MAX_SLOT_COUNT Maximum number of slots for this configuration.
   */
  const QString ConfigKeys::MAX_SLOT_COUNT = "MAX_SLOT_COUNT";

  /**
   * @brief STAGE_LIB_NAME Name of the library for connecting to the translation stage.
   */
  const QString ConfigKeys::STAGE_LIB_NAME = "STAGE_LIB_NAME";

  /**
   * @brief STAGE_STEP_SIZE_X Step size of stage in microns for 1 microstepping.
   */
  const QString ConfigKeys::STAGE_STEP_SIZE_X = "STAGE_STEP_SIZE_X";

  /**
   * @brief STAGE_STEP_SIZE_Y Step size of stage in microns for 1 microstepping.
   */
  const QString ConfigKeys::STAGE_STEP_SIZE_Y = "STAGE_STEP_SIZE_Y";

  /**
   * @brief STAGE_STEP_SIZE_Z Step size of stage in microns for 1 microstepping.
   */
  const QString ConfigKeys::STAGE_STEP_SIZE_Z = "STAGE_STEP_SIZE_Z";

  /**
   * @brief STAGE_MICROSTEPPING_X X-Stage microtepping.
   */
  const QString ConfigKeys::STAGE_MICROSTEPPING_X = "STAGE_MICROSTEPPING_X";

  /**
   * @brief STAGE_MICROSTEPPING_Y Y-Stage microtepping.
   */
  const QString ConfigKeys::STAGE_MICROSTEPPING_Y = "STAGE_MICROSTEPPING_Y";

  /**
   * @brief STAGE_MICROSTEPPING_Z Z-Stage microtepping.
   */
  const QString ConfigKeys::STAGE_MICROSTEPPING_Z = "STAGE_MICROSTEPPING_Z";

  /**
   * @brief STAGE_MICROSTEPPING_TURRET TURRET-Stage microtepping.
   */
  const QString ConfigKeys::STAGE_MICROSTEPPING_TURRET = "STAGE_MICROSTEPPING_TURRET";

  /**
   * @brief XY_DRIVER_HOLD_CURENT XY_Motor Driver Configuration.
   */
  const QString ConfigKeys::XY_DRIVER_HOLD_CURRENT = "XY_DRIVER_HOLD_CURRENT";

  /**
   * @brief Z_DRIVER_HOLD_CURENT Z_Motor Driver Configuration.
   */
  const QString ConfigKeys::Z_DRIVER_HOLD_CURRENT = "Z_DRIVER_HOLD_CURRENT";

  /**
   * @brief TURRET_DRIVER_HOLD_CURENT TURRET_Motor Driver Configuration.
   */
  const QString ConfigKeys::TURRET_DRIVER_HOLD_CURRENT = "TURRET_DRIVER_HOLD_CURRENT";

  /**
   * @brief XY_DRIVER_RUN_CURENT XY_Motor Driver Configuration.
   */
  const QString ConfigKeys::XY_DRIVER_RUN_CURRENT = "XY_DRIVER_RUN_CURRENT";

  /**
   * @brief Z_DRIVER_RUN_CURENT Z_Motor Driver Configuration.
   */
  const QString ConfigKeys::Z_DRIVER_RUN_CURRENT = "Z_DRIVER_RUN_CURRENT";

  /**
   * @brief TURRET_DRIVER_RUN_CURENT TURRET_Motor Driver Configuration.
   */
  const QString ConfigKeys::TURRET_DRIVER_RUN_CURRENT = "TURRET_DRIVER_RUN_CURRENT";

  /**
   * @brief X_DRIVER_MODET X_Motor Driver Configuration.
   */
  const QString ConfigKeys::X_DRIVER_MODE = "X_DRIVER_MODE";

  /**
   * @brief Y_DRIVER_MODET Y_Motor Driver Configuration.
   */
  const QString ConfigKeys::Y_DRIVER_MODE = "Y_DRIVER_MODE";

  /**
   * @brief Z_DRIVER_MODET Z_Motor Driver Configuration.
   */
  const QString ConfigKeys::Z_DRIVER_MODE = "Z_DRIVER_MODE";

  /**
   * @brief TURRET_DRIVER_MODET TURRET_Motor Driver Configuration.
   */
  const QString ConfigKeys::TURRET_DRIVER_MODE = "TURRET_DRIVER_MODE";

  /**
   * @brief FOV_WIDTH_20X_MICRONS Width of the camera field of view in microns
   * at 20x magnification.
   */
  const QString ConfigKeys::FOV_WIDTH_20X_MICRONS = "FOV_WIDTH_20X_MICRONS";

  /**
   * @brief FOV_HEIGHT_20X_MICRONS Width of the camera field of view in microns
   * at 20x magnification.
   */
  const QString ConfigKeys::FOV_HEIGHT_20X_MICRONS = "FOV_HEIGHT_20X_MICRONS";

  /**
   * @brief FOV_WIDTH_40X_MICRONS Width of the camera field of view in microns
   * at 40x magnification.
   */
  const QString ConfigKeys::FOV_WIDTH_40X_MICRONS = "FOV_WIDTH_40X_MICRONS";

  /**
   * @brief FOV_HEIGHT_40X_MICRONS Width of the camera field of view in microns
   * at 40x magnification.
   */
  const QString ConfigKeys::FOV_HEIGHT_40X_MICRONS = "FOV_HEIGHT_40X_MICRONS";

  /**
   * @brief FOV_WIDTH_100X_MICRONS Width of the camera field of view in microns
   * at 100x magnification.
   */
  const QString ConfigKeys::FOV_WIDTH_100X_MICRONS = "FOV_WIDTH_100X_MICRONS";

  /**
   * @brief FOV_HEIGHT_100X_MICRONS Width of the camera field of view in microns
   * at 100x magnification.
   */
  const QString ConfigKeys::FOV_HEIGHT_100X_MICRONS = "FOV_HEIGHT_100X_MICRONS";

  /**
   * @brief X_OFFSET Offset in X direction of the current loaded slot.
   */
  const QString ConfigKeys::X_OFFSET = "X_OFFSET";

  /**
   * @brief Y_OFFSET Offset in Y direction of the current loaded slot.
   */
  const QString ConfigKeys::Y_OFFSET = "Y_OFFSET";

  /**
   * @brief X_UPPER_LIMIT Upper limit of the X-Stage in microns.
   */
  const QString ConfigKeys::X_UPPER_LIMIT = "X_UPPER_LIMIT";

  /**
   * @brief Y_UPPER_LIMIT Upper limit of the Y-Stage in microns.
   */
  const QString ConfigKeys::Y_UPPER_LIMIT = "Y_UPPER_LIMIT";

  /**
   * @brief Z_LOWER_LIMIT Lower limit of the Z-Stage.
   */
  const QString ConfigKeys::Z_LOWER_LIMIT = "Z_LOWER_LIMIT";

  /**
   * @brief Z_UPPER_LIMIT Upper limit of the Z-Stage.
   */
  const QString ConfigKeys::Z_UPPER_LIMIT = "Z_UPPER_LIMIT";

  /**
   * @brief Z_20X_LOWER_LIMIT
   */
  const QString ConfigKeys::Z_20X_LOWER_LIMIT = "Z_20X_LOWER_LIMIT";

  /**
   * @brief Z_20X_UPPER_LIMIT
   */
  const QString ConfigKeys::Z_20X_UPPER_LIMIT = "Z_20X_UPPER_LIMIT";

  /**
   * @brief Z_20X_STACK_LOWER_LIMIT
   */
  const QString ConfigKeys::Z_20X_STACK_LOWER_LIMIT = "Z_20X_STACK_LOWER_LIMIT";

  /**
   * @brief Z_20X_STACK_UPPER_LIMIT
   */
  const QString ConfigKeys::Z_20X_STACK_UPPER_LIMIT = "Z_20X_STACK_UPPER_LIMIT";

  /**
   * @brief Z_40X_STACK_LOWER_LIMIT
   */
  const QString ConfigKeys::Z_40X_STACK_LOWER_LIMIT = "Z_40X_STACK_LOWER_LIMIT";

  /**
   * @brief Z_40X_STACK_UPPER_LIMIT
   */
  const QString ConfigKeys::Z_40X_STACK_UPPER_LIMIT = "Z_40X_STACK_UPPER_LIMIT";

  /**
   * @brief Z_100X_STACK_LOWER_LIMIT
   */
  const QString ConfigKeys::Z_100X_STACK_LOWER_LIMIT = "Z_100X_STACK_LOWER_LIMIT";

  /**
   * @brief Z_100X_STACK_UPPER_LIMIT
   */
  const QString ConfigKeys::Z_100X_STACK_UPPER_LIMIT = "Z_100X_STACK_UPPER_LIMIT";

  /**
   * @brief Z_20X_TO_40X_OFF
   */
  const QString ConfigKeys::Z_20X_TO_40X_OFF = "Z_20X_TO_40X_OFF";

  /**
   * @brief Z_20X_TO_100X_OFF
   */
  const QString ConfigKeys::Z_20X_TO_100X_OFF = "Z_20X_TO_100X_OFF";

  /**
   * @brief Z_40X_TO_100X_OFF
   */
  const QString ConfigKeys::Z_40X_TO_100X_OFF = "Z_40X_TO_100X_OFF";

  /**
   * @brief CAM_EXP_20X Exposure value of the camera at 20x magnification.
   */
  const QString ConfigKeys::CAM_EXP_20X = "CAM_EXP_20X";

  /**
   * @brief CAM_EXP_40X Exposure value of the camera at 40x magnification.
   */
  const QString ConfigKeys::CAM_EXP_40X = "CAM_EXP_40X";

  /**
   * @brief CAM_EXP_100X Exposure value of the camera at 100x magnification.
   */
  const QString ConfigKeys::CAM_EXP_100X = "CAM_EXP_100X";

  /**
   * @brief CAM_EXP_100X_OIL Exposure value of the camera at 100x oil
   * magnification.
   */
  const QString ConfigKeys::CAM_EXP_100X_OIL = "CAM_EXP_100X_OIL";

  /**
   * @brief CONT_ACQ_EXP_20X Exposure value of the camera at
   * 20x magnification during continous acquisition.
   */
  const QString ConfigKeys::CONT_ACQ_EXP_20X = "CONT_ACQ_EXP_20X";

  /**
   * @brief CONT_ACQ_EXP_40X Exposure value of the camera at
   * 40x magnification during continous acquisition.
   */
  const QString ConfigKeys::CONT_ACQ_EXP_40X = "CONT_ACQ_EXP_40X";

  /**
   * @brief RED_BALANCE_RATIO Red balance ratio value of the camera.
   */
  const QString ConfigKeys::RED_BALANCE_RATIO = "RED_BALANCE_RATIO";

  /**
   * @brief BLUE_BALANCE_RATIO Blue balance ration value of the camera.
   */
  const QString ConfigKeys::BLUE_BALANCE_RATIO = "BLUE_BALANCE_RATIO";

  /**
   * @brief OFFSET_X_20_TO_40
   */
  const QString ConfigKeys::OFFSET_X_20_TO_40 = "OFFSET_X_20_TO_40";

  /**
   * @brief OFFSET_Y_20_TO_40
   */
  const QString ConfigKeys::OFFSET_Y_20_TO_40 = "OFFSET_Y_20_TO_40";

  /**
   * @brief OFFSET_X_20_TO_100
   */
  const QString ConfigKeys::OFFSET_X_20_TO_100 = "OFFSET_X_20_TO_100";

  /**
   * @brief OFFSET_Y_20_TO_100
   */
  const QString ConfigKeys::OFFSET_Y_20_TO_100 = "OFFSET_Y_20_TO_100";

  /**
   * @brief OFFSET_X_40_TO_100
   */
  const QString ConfigKeys::OFFSET_X_40_TO_100 = "OFFSET_X_40_TO_100";

  /**
   * @brief OFFSET_Y_40_TO_100
   */
  const QString ConfigKeys::OFFSET_Y_40_TO_100 = "OFFSET_Y_40_TO_100";

  /**
   * @brief OFFSET_Z_20_TO_40
   */
  const QString ConfigKeys::OFFSET_Z_20_TO_40 = "OFFSET_Z_20_TO_40";

  /**
   * @brief OFFSET_Z_20_TO_100
   */
  const QString ConfigKeys::OFFSET_Z_20_TO_100 = "OFFSET_Z_20_TO_100";

  /**
   * @brief OFFSET_Z_40_TO_100
   */
  const QString ConfigKeys::OFFSET_Z_40_TO_100 = "OFFSET_Z_40_TO_100";

  /**
   * @brief STAGE_Z_STEP_SIZE_20x
   */
  const QString ConfigKeys::STAGE_Z_STEP_SIZE_20x = "STAGE_Z_STEP_SIZE_20x";

  /**
   * @brief STAGE_Z_STEP_SIZE_40x
   */
  const QString ConfigKeys::STAGE_Z_STEP_SIZE_40x = "STAGE_Z_STEP_SIZE_40x";

  /**
   * @brief STAGE_Z_STEP_SIZE_100x
   */
  const QString ConfigKeys::STAGE_Z_STEP_SIZE_100x = "STAGE_Z_STEP_SIZE_100x";

  /**
   * @brief MAX_XY_SPEED To store the maximum speed of the XY-Stage in mm/s.
   */
  const QString ConfigKeys::MAX_XY_SPEED = "MAX_XY_SPEED";

  /**
   * @brief CONTINUOUS_SPEED_20X To store the speed of the XY-Stage during
   * continuous acquisition at 20x magnification
   */
  const QString ConfigKeys::CONTINUOUS_SPEED_20X = "CONTINUOUS_SPEED_20X";

  /**
   * @brief CONTINUOUS_SPEED_40X To store the speed of the XY-Stage during
   * continuous acquisition at 40x magnification
   */
  const QString ConfigKeys::CONTINUOUS_SPEED_40X = "CONTINUOUS_SPEED_40X";

  /**
   * @brief COLUMN_ZSTACK_MODE For doing a hardware Z-Stage trigger during
   * continuous mode of acquisition.
   * 1 - indicates trigger will be used.
   * 0 - indicates trigger will not be used.
   */
  const QString ConfigKeys::COLUMN_ZSTACK_MODE = "COLUMN_ZSTACK_MODE";

  /**
   * @brief ENABLE_STITCHING For running the stitching pipeline during
   * continuous mode of acqusition.
   * 1 - indicates stitching pipeline will be enabled.
   * 0 - indicates stitching pipeline will be disabled.
   */
  const QString ConfigKeys::ENABLE_STITCHING = "ENABLE_STITCHING";

  /**
   * @brief FOCUS_CHANNEL To store the channel on which focus check
   * would be performed.
   */
  const QString ConfigKeys::FOCUS_CHANNEL_20X = "FOCUS_CHANNEL_20X";
  const QString ConfigKeys::FOCUS_CHANNEL_40X = "FOCUS_CHANNEL_40X";
  const QString ConfigKeys::FOCUS_CHANNEL_100X = "FOCUS_CHANNEL_100X";

  /**
   * @brief BG_THRESH_BLOOD_SMEAR To store the background threshold values
   * for blood smear specimen type.
   */
  const QString ConfigKeys::BG_THRESH_BLOOD_SMEAR = "BG_THRESH_BLOOD_SMEAR";

  /**
   * @brief BG_THRESH_TISSUE_BIOPSY To store the background threshold values
   * for tissue biopsy specimen type.
   */
  const QString ConfigKeys::BG_THRESH_TISSUE_BIOPSY = "BG_THRESH_TISSUE_BIOPSY";

  /**
   * @brief FOCUS_METRIC_THRESHOLD To store the threshold value for
   * focus metric.
   */
  const QString ConfigKeys::FOCUS_METRIC_THRESHOLD =
      "FOCUS_METRIC_THRESHOLD";

  const QString ConfigKeys::WRITE_IMAGES = "WRITE_IMAGES";
  const QString ConfigKeys::WRITE_STACK = "WRITE_STACK";

  const QString ConfigKeys::INSERT_FILTER_TAG = "INSERT_FILTER";
  const QString ConfigKeys::REMOVE_FILTER_TAG = "REMOVE_FILTER";
  const QString ConfigKeys::BACKLASH_Z = "BACKLASH_Z";
  const QString ConfigKeys::Z_TRIGGER_CAM_FPS = "Z_TRIGGER_CAM_FPS";

  const QString ConfigKeys::MAG_HOME_POS = "MAG_HOME_POS";
  const QString ConfigKeys::MAG_20X_POS = "MAG_20X_POS";
  const QString ConfigKeys::MAG_40X_POS = "MAG_40X_POS";
  const QString ConfigKeys::MAG_100X_POS = "MAG_100X_POS";
  const QString ConfigKeys::MAG_100X_OIL_POS = "MAG_100X_OIL_POS";

  const QString ConfigKeys::MAX_Z_STAGE_SPEED = "MAX_Z_STAGE_SPEED";
  const QString ConfigKeys::DEF_Z_STAGE_SPEED = "DEF_Z_STAGE_SPEED";
  const QString ConfigKeys::DEF_Z_STAGE_SPEED_40X = "DEF_Z_STAGE_SPEED_40X";

  /**
   * @brief BG_FM_BLOOD_SMEAR Focus metric threshold for background images for
   * the blood smear pipeline.
   */
  const QString ConfigKeys::BG_FM_BLOOD_SMEAR = "BG_FM_BLOOD_SMEAR";

  /**
   * @brief BG_FM_TISSUE_BIOPSY Focus metric threshold for background images
   * for the tissue biopsy pipeline.
   */
  const QString ConfigKeys::BG_FM_TISSUE_BIOPSY = "BG_FM_TISSUE_BIOPSY";

  /**
   * @brief XY_PRE_TRIGGER_DELAY To store the pre-trigger delay for the
   * XY-Stage.
   */
  const QString ConfigKeys::XY_PRE_TRIGGER_DELAY = "XY_PRE_TRIGGER_DELAY";

  /**
   * @brief XY_POST_TRIGGER_DELAY To store the post-trigger delay for the
   * XY-Stage.
   */
  const QString ConfigKeys::XY_POST_TRIGGER_DELAY = "XY_POST_TRIGGER_DELAY";

  /**
   * @brief Z_PRE_TRIGGER_DELAY To store the pre-trigger delay for the
   * Z-Stage.
   */
  const QString ConfigKeys::Z_PRE_TRIGGER_DELAY = "Z_PRE_TRIGGER_DELAY";

  /**
   * @brief Z_POST_TRIGGER_DELAY To store the post-trigger delay for the
   * Z-Stage.
   */
  const QString ConfigKeys::Z_POST_TRIGGER_DELAY = "Z_POST_TRIGGER_DELAY";

  /**
   * @brief X_TRIGGER_PULSE_DURATION To store the trigger duration for the
   * X-Stage.
   */
  const QString ConfigKeys::X_TRIGGER_PULSE_DURATION = "X_TRIGGER_PULSE_DURATION";

  /**
   * @brief Y_TRIGGER_PULSE_DURATION To store the trigger duration for the
   * Y-Stage.
   */
  const QString ConfigKeys::Y_TRIGGER_PULSE_DURATION = "Y_TRIGGER_PULSE_DURATION";

  /**
   * @brief Z_TRIGGER_PULSE_DURATION To store the trigger duration for the
   * Z-Stage.
   */
  const QString ConfigKeys::Z_TRIGGER_PULSE_DURATION = "Z_TRIGGER_PULSE_DURATION";

  /**
   * @brief Z_STACK_TRIGGER_DELAY To store the delay for a Z-stack during
   * continuous acquisition.
   */
  const QString ConfigKeys::Z_STACK_TRIGGER_DELAY = "Z_STACK_TRIGGER_DELAY";

  /**
   * @brief Z_OFFSET_STEPS To store the offset in steps for the Z-Stage.
   */
  const QString ConfigKeys::Z_OFFSET_STEPS = "Z_OFFSET_STEPS";

  /**
   * @brief XY_Z_TRIGGER_STACK_SIZE To store the stack size for Z-Trigger
   * during continuous acquisition.
   */
  const QString ConfigKeys::XY_Z_TRIGGER_STACK_SIZE = "XY_Z_TRIGGER_STACK_SIZE";

  /**
   * @brief XY_Z_TRIGGER_STEP_SIZE To store the step size for the Z-Trigger
   * during continuous acquisition at 20x in microns.
   */
  const QString ConfigKeys::XY_Z_TRIGGER_STEP_SIZE = "XY_Z_TRIGGER_STEP_SIZE";

  /**
   * @brief XY_Z_TRIGGER_STEP_SIZE_40X To store the step size for the
   * Z-Trigger during continuous acquisition at 40x in microns.
   */
  const QString ConfigKeys::XY_Z_TRIGGER_STEP_SIZE_40X = "XY_Z_TRIGGER_STEP_SIZE_40X";

  /**
   * @brief REMOVE_IMAGES_FROM_STACK To store the no. of images to remove
   * from the stack.
   */
  const QString ConfigKeys::REMOVE_IMAGES_FROM_STACK = "REMOVE_IMAGES_FROM_STACK";

  /**
   * @brief INLINE_FM_CALC To indicate whether the focus metric computation
   * should be inline (within thread) or not.
   */
  const QString ConfigKeys::INLINE_FM_CALC = "INLINE_FM_CALC";
}
