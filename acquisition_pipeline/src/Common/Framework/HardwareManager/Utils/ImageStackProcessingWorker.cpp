﻿#include <QThread>

#include "Common/Framework/HardwareManager/Utils/ImageStackProcessingWorker.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"

namespace spin
{
  /**
   * @brief ImageStackProcessingWorker Default class constructor,
   * initializes objects owned by the class.
   * @param parent Pointer to a oarent QObject class.
   */
  ImageStackProcessingWorker::ImageStackProcessingWorker(QObject* parent) :
    QObject(parent)
  {

  }

  /**
   * @brief onStartWorker Slot called to start the task of the worker.
   */
  void ImageStackProcessingWorker::onStartWorker()
  {
    int bestIdx = 0;
    double bestFocusMetric = 0.0;

    for(int idx = 0; idx < m_aoiInfo.m_zStack.size(); idx++)
    {
      cv::Mat capturedImage(cv::Size(Constants::FRAME_WIDTH,
                                     Constants::FRAME_HEIGHT), CV_8UC4,
                            m_aoiInfo.m_zStack[idx]);

      // Split the image into four different channels.
      std::vector <cv::Mat> channels;
      channels.reserve(4);
      cv::split(capturedImage, channels);

      // Compute the focus metric of the image using the green channel.
      double focusMetric = Utilities::computeFocusMetric(channels[1]);
      if(focusMetric > bestFocusMetric)
      {
        bestFocusMetric = focusMetric;
        bestIdx = idx;
      }

      for(int idx = 0; idx < channels.size(); idx++)
      {
        channels[idx].release();
      }
      channels.clear();
    }

    m_aoiInfo.m_focusMetric = bestFocusMetric;

    // Get the best image.
    cv::Mat bestImage(cv::Size(Constants::FRAME_WIDTH,
                               Constants::FRAME_HEIGHT), CV_8UC4,
                      m_aoiInfo.m_zStack[bestIdx]);
    // Split the image into four different channels.
    std::vector <cv::Mat> channels;
    channels.reserve(4);
    cv::split(bestImage, channels);

    // Remove the last channel.
    channels[3].release();
    channels.pop_back();

    // Merge the three channels.
    cv::Mat mergedImg;
    cv::merge(channels, mergedImg);

    // Compute the color metric of the image.
    m_aoiInfo.m_colorMetric = Utilities::computeColorMetric(mergedImg,
                                                          spin::TISSUE_BIOPSY);

    if(CONFIG()->intConfig(ConfigKeys::WRITE_IMAGES))
    {
      // Create a new buffer.
      acquisition::AoIInfo* aoiInfo = new acquisition::AoIInfo();
      *aoiInfo = m_aoiInfo;
      aoiInfo->m_img = mergedImg.clone();

      // Add to the shared buffer.
      SHARED_WRITE_BUFFER()->add(aoiInfo);
      SHARED_WRITE_BUFFER()->resume();
    }

    if(CONFIG()->intConfig(ConfigKeys::ENABLE_STITCHING))
    {
      // Create a new buffer.
      acquisition::AoIInfo* aoiInfoProc = new acquisition::AoIInfo();
      *aoiInfoProc = m_aoiInfo;
      aoiInfoProc->m_img = mergedImg.clone();

      // Add to the shared buffer.
      SHARED_PROCESS_BUFFER()->add(aoiInfoProc);
      SHARED_PROCESS_BUFFER()->resume();
    }

    mergedImg.release();
    for(int idx = 0; idx < channels.size(); idx++)
    {
      channels[idx].release();
    }
    channels.clear();

    Q_EMIT workerFinished(m_aoiInfo.m_id,
                          m_aoiInfo.m_focusMetric,
                          m_aoiInfo.m_colorMetric);

    if(thread())
    {
      thread()->quit();
    }
  }

  /**
   * @brief ~ImageStackProcessingWorker Default class destructor, destroys
   * objects owned by the class.
   */
  ImageStackProcessingWorker::~ImageStackProcessingWorker()
  {

  }
}
