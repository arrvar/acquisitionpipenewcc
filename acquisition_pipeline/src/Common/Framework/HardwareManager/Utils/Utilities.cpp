#include <QFile>
#include <QTextStream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

namespace spin
{
  /**
   * @brief micronsToPixels To convert from microns to pixel coordinates
   * on the virtual slide.
   * @param micronPos Coordinates in microns.
   * @return Returns coordinates in pixels.
   */
  QPointF Utilities::micronsToPixels(const QPointF& micronPos)
  {
    qreal pixelPosX = micronPos.x() *
        Constants::VIRT_SLIDE_WIDTH / Constants::SLIDE_WIDTH;
    qreal pixelPosY = micronPos.y() *
        Constants::VIRT_SLIDE_HEIGHT / Constants::SLIDE_HEIGHT;

    return QPointF(pixelPosX, pixelPosY);
  }

  /**
   * @brief pixelsToMicrons To convert from virtual slide pixel coordinates
   * to microns.
   * @param pixelPos Coordinates in pixels.
   * @return Returns coordinates in microns.
   */
  QPointF Utilities::pixelsToMicrons(const QPointF& pixelPos)
  {
    qreal micronPosX = pixelPos.x() *
        Constants::SLIDE_WIDTH / Constants::VIRT_SLIDE_WIDTH;
    qreal micronPosY = pixelPos.y() *
        Constants::SLIDE_HEIGHT / Constants::VIRT_SLIDE_HEIGHT;

    return QPointF(micronPosX, micronPosY);
  }

  /**
   * @brief getFovSizeMicrons To retrieve the size of the FoV for the given
   * magnification.
   * @param magVal Magnification of the FoV.
   * @return Returns the size of the FoV in microns for the given
   * magnification.
   */
  QSizeF Utilities::getFovSizeMicrons(Magnification magVal)
  {
    // Retreive the stored FoV width and height in microns.
    switch(magVal)
    {
    case MAG_20X:
      return QSizeF(CONFIG()->realConfig(ConfigKeys::FOV_WIDTH_20X_MICRONS),
                    CONFIG()->realConfig(ConfigKeys::FOV_HEIGHT_20X_MICRONS));

    case MAG_40X:
      return QSizeF(CONFIG()->realConfig(ConfigKeys::FOV_WIDTH_40X_MICRONS),
                    CONFIG()->realConfig(ConfigKeys::FOV_HEIGHT_40X_MICRONS));

    case MAG_100X:
      return QSizeF(CONFIG()->realConfig(ConfigKeys::FOV_WIDTH_100X_MICRONS),
                    CONFIG()->realConfig(ConfigKeys::FOV_HEIGHT_100X_MICRONS));

    default:
      return QSizeF();
    }
  }

  /**, boost::python::object object
   * @brief getFovSizePixels To retrieve the size of the FoV for the given
   * magnification in pixels.
   * @param magVal Magnification of the FoV.
   * @return Returns the size of the FoV in pixels.
   */
  QSizeF Utilities::getFovSizePixels(Magnification magVal)
  {
    // Retrieve the FoV size available in microns.
    QSizeF fovSizeMicrons = Utilities::getFovSizeMicrons(magVal);

    // Convert to pixel value.
    QPointF pixSize = Utilities::
        micronsToPixels(QPointF(fovSizeMicrons.width(),
                                fovSizeMicrons.height()));

    return QSizeF(pixSize.x(), pixSize.y());
  }

  /**
   * @brief loadSlotParams To load the given slot parameters into the
   * system configuration.
   * @param slotNum Slot number.
   * @return Returns true if the slot parameters are available and have been
   * loaded for the given slot number.
   */
  bool Utilities::loadSlotParams(int slotNum)
  {
    acquisition::SlotParams slotParams;
    if(CONFIG()->getSlotParam(slotNum, slotParams))
    {
      CONFIG()->addConfig(ConfigKeys::X_OFFSET,
                          new QVariant(slotParams.m_offsetX));
      CONFIG()->addConfig(ConfigKeys::Y_OFFSET,
                          new QVariant(slotParams.m_offsetY));

      qreal xOffset = CONFIG()->realConfig(ConfigKeys::X_OFFSET);
      qreal yOffset = CONFIG()->realConfig(ConfigKeys::Y_OFFSET);

      SPIN_LOG_DEBUG(QObject::tr("Slot No.: %1").arg(slotNum));
      SPIN_LOG_DEBUG(QObject::tr("X Offset: %1").arg(xOffset));
      SPIN_LOG_DEBUG(QObject::tr("Y Offset: %1").arg(yOffset));

      return true;
    }
    else
    {
      SPIN_LOG_ERROR(QObject::tr("Slot parameters for slot number %1 not "
                                 "available.").arg(slotNum));
      return false;
    }
  }

  /**
   * @brief writeStatus To write the status of the current acquisition into
   * a file.
   * @param filePath Absolute path of the file.
   * @param statusMsg Current status message of the file.
   * @return Returns true on successfully writing the status into the file
   * else returns false.
   */
  bool Utilities::writeStatus(QString filePath, QString statusMsg)
  {
    bool retVal = true;
    QFile file(filePath);

    if(file.open(QIODevice::WriteOnly))
    {
        QTextStream fileStream(&file);

        fileStream << statusMsg << "\r\n";

        file.close();
    }
    else
    {
        SPIN_LOG_ERROR(QObject::tr("Failed to open file for writing "
                                   "status at: %1").arg(filePath));
        retVal = false;
    }

    return retVal;
  }

  /**
  * @brief getCamExpValue To retrieve the camera exposure value for the
  * given magnification.
  * @param magValue The given magnification value.
  * @return Returns the exposure value of the camera for the given
  * magnification.
  */
  qreal Utilities::getCamExpValue(Magnification magValue)
  {
    switch (magValue)
    {
    case MAG_20X:
      return CONFIG()->realConfig(ConfigKeys::CAM_EXP_20X);

    case MAG_40X:
      return CONFIG()->realConfig(ConfigKeys::CAM_EXP_40X);

    case MAG_100X:
      return CONFIG()->realConfig(ConfigKeys::CAM_EXP_100X);

    case MAG_100X_OIL:
      return CONFIG()->realConfig(ConfigKeys::CAM_EXP_100X_OIL);

    default:
      return 0;
    }
  }

  /**
   * @brief getZOffset To get the offset in Z-Stage value from the a lower
   * to the given magnification value.
   * @param magValue The higher magnification value.
   * @return Returns the corresponding offset value.
   */
  qreal Utilities::getZOffset(Magnification lowMag,
                              Magnification highMag)
  {
    if(lowMag == MAG_20X)
    {
      switch(highMag)
      {
      case MAG_40X:
        return CONFIG()->realConfig(ConfigKeys::OFFSET_Z_20_TO_40);

      case MAG_100X:
        return CONFIG()->realConfig(ConfigKeys::OFFSET_Z_20_TO_100);

      default:
        return 0;

      }

    }
    else if(lowMag == MAG_40X)
    {
      switch(highMag)
      {
      case MAG_100X:
        return CONFIG()->realConfig(ConfigKeys::OFFSET_Z_40_TO_100);

      default:
        return 0;

      }
    }
    else
    {
      return 0;
    }
  }

  /**
   * @brief getZStackLimits
   * @param magValue
   * @param lowerLimit
   * @param upperLimit
   */
  void Utilities::getZStackLimits(Magnification magValue,
                                  qreal& lowerLimit, qreal& upperLimit)
  {
    switch(magValue)
    {
    case MAG_20X:
      lowerLimit = CONFIG()->realConfig(ConfigKeys::Z_20X_STACK_LOWER_LIMIT);
      upperLimit = CONFIG()->realConfig(ConfigKeys::Z_20X_STACK_UPPER_LIMIT);

      break;

    case MAG_40X:
      lowerLimit = CONFIG()->realConfig(ConfigKeys::Z_40X_STACK_LOWER_LIMIT);
      upperLimit = CONFIG()->realConfig(ConfigKeys::Z_40X_STACK_UPPER_LIMIT);

      break;

    case MAG_100X:
      lowerLimit = CONFIG()->realConfig(ConfigKeys::Z_100X_STACK_LOWER_LIMIT);
      upperLimit = CONFIG()->realConfig(ConfigKeys::Z_100X_STACK_UPPER_LIMIT);

      break;

    default:
      lowerLimit = 0;
      upperLimit = 0;

      break;
    }
  }

  /**
   * @brief getZStageStepSize
   * @param magValue
   * @return
   */
  qreal Utilities::getZStageStepSize(Magnification magValue)
  {
    switch(magValue)
    {
    case MAG_20X:
      return CONFIG()->realConfig(ConfigKeys::STAGE_Z_STEP_SIZE_20x);

    case MAG_40X:
      return CONFIG()->realConfig(ConfigKeys::STAGE_Z_STEP_SIZE_40x);

    case MAG_100X:
      return CONFIG()->realConfig(ConfigKeys::STAGE_Z_STEP_SIZE_100x);

    default:
      return 0;
    }
  }

  /**
  * @brief magnificationString To retrieve string equivalent of the
  * magnification value.
  * @param magValue Given magnification value.
  * @return Returns the string equivalent of the given magnification value.
  */
  QString Utilities::magnificationString(Magnification magValue)
  {
    switch (magValue)
    {
    case MAG_20X:
      return QString("20x");

    case MAG_40X:
      return QString("40x");

    case MAG_100X:
      return QString("100x");

    case MAG_HOME:
      return QString("home");

    default:
      return QString("unknown");
    }
  }

  /**
   * @brief getXYZTriggerStepSize To get the step size for the Z-Stage
   * during continuous acquisition.
   * @param magValue Magnification value.
   * @return Returns the step size in microns.
   */
  double Utilities::getXYZTriggerStepSize(Magnification magValue)
  {
    switch(magValue)
    {
      case MAG_20X:
        return CONFIG()->realConfig(ConfigKeys::XY_Z_TRIGGER_STEP_SIZE);

      case MAG_40X:
        return CONFIG()->realConfig(ConfigKeys::XY_Z_TRIGGER_STEP_SIZE_40X);

      default:
        return 0;
    }
  }

  /**
   * @brief getDefaultZStageSpeed To get the default Z-Stage speed for
   * continuous acquisition.
   * @param magValue Magnification value.
   * @return Returns the Z-Stage speed in mm/s.
   */
  double Utilities::getDefaultZStageSpeed(Magnification magValue)
  {
    switch(magValue)
    {
      case MAG_20X:
        return CONFIG()->realConfig(ConfigKeys::DEF_Z_STAGE_SPEED);

      case MAG_40X:
        return CONFIG()->realConfig(ConfigKeys::DEF_Z_STAGE_SPEED_40X);

      default:
        return 0;
    }
  }

  /**
   * @brief isBackground To check if the given image is a background or not.
   * @param inputImg Input image.
   * @param specType Specimen type.
   * @return Returns true if the image is a background else returns false.
   */
  bool Utilities::isBackground(cv::Mat &inputImg, SpecimenType specType)
  {
    if (Utilities::computeColorMetric(inputImg, specType) < 1.0)
    {
      // If BG
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @brief computeFocusMetric To compute the focus metric of the given input
   * image.
   * @param inputImg Input image.
   * @return Returns the computed focus metric value of the image.
   */
  double Utilities::computeFocusMetric(cv::Mat& inputImg)
  {
    // Based on Sobel operator
    cv::GaussianBlur(inputImg, inputImg, cv::Size(3, 3), 0, 0,
                     cv::BORDER_DEFAULT);
    cv::Mat processedImg, absprocessedImg;
    cv::Sobel(inputImg, processedImg, CV_32F, 1, 0);

    absprocessedImg = cv::abs(processedImg);
    cv::Scalar focus_val = cv::mean(absprocessedImg);

    return focus_val[0];
  }

  /**
   * @brief computeColorMetric To compute the color metric of the given input
   * image.
   * @param inputImg Input image.
   * @param specType Specimen type.
   * @return Returns the computed color metric value of the image.
   */
  double Utilities::computeColorMetric(cv::Mat &inputImg,
                                       SpecimenType specType)
  {
    // Get the Threshold value from the config file based on the specimen type
    // Different for bioipsy and blood smear
    cv::Scalar stdThresh;
    if(specType == BLOOD_SMEAR)
    {
      stdThresh = CONFIG()->realConfig(ConfigKeys::BG_THRESH_BLOOD_SMEAR);
    }
    else if(specType == TISSUE_BIOPSY)
    {
      stdThresh = CONFIG()->realConfig(ConfigKeys::BG_THRESH_TISSUE_BIOPSY);
    }
    else
    {
      stdThresh = 10.0;
    }

    std::vector<cv::Mat> channels;
    cv::split(inputImg, channels);
    cv::Scalar meanB, stddevB;
    cv::Scalar meanG, stddevG;
    cv::Scalar meanR, stddevR;
    double mean_min, mean_max;
    double stddev_min, stddev_max;
    double metric_A, metric_B, metric_C;

    // Get the mean and deviation of the RGB channel
    cv::meanStdDev(channels[0], meanB, stddevB);
    cv::meanStdDev(channels[1], meanG, stddevG);
    cv::meanStdDev(channels[2], meanR, stddevR);
    std::vector<double> mean_array;
    std::vector<double> stddev_array;
    mean_array = {meanB.val[0], meanG.val[0], meanR.val[0]};
    stddev_array = {stddevB.val[0],stddevG.val[0],stddevR.val[0]};
    cv::minMaxLoc(mean_array, &mean_min, &mean_max);
    cv::minMaxLoc(stddev_array, &stddev_min, &stddev_max);

    metric_A = stddev_max/stdThresh.val[0];
    metric_B = (mean_max-mean_min)/mean_min;
    metric_C = metric_A+metric_B;
    return metric_C;
  }
}
