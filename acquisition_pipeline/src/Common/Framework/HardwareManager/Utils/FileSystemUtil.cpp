#include <QDir>

#include "Framework/HardwareManager/Utils/FileSystemUtil.h"

#include "Framework/HardwareManager/Framework/AppContext.h"

namespace spin
{
  /**
  * @brief installationPath To retrieve the absolute installation path of
  * the application.
  * @return Returns the absolute installation path of the application.
  */
  QString FileSystemUtil::installationPath()
  {
    QDir dir(QString::fromStdString(AppContext::get().installationPath()));
    QString path = dir.absolutePath();

    return QDir::fromNativeSeparators(path);
  }

  /**
   * @brief getPathFromInstallationPath To retrieve the absolute path from the
   * given relative path.
   * @param relativePath The given relative path.
   * @return Returns the absolute path of the given relative path.
   */
  QString FileSystemUtil::getPathFromInstallationPath(QString relativePath)
  {
    QString path = QDir(installationPath()).absoluteFilePath(relativePath);
    return QDir::fromNativeSeparators(path);
  }

  /**
   * @brief getPathFromWorkspacePath Takes a relative path of a file/directory
   * relative to workspace path and returns absolute path.
   * @param relativePath Path relative to workspace path.
   * @return Absolute path of the given relative path.
   */
  QString FileSystemUtil::getPathFromWorkspacePath(QString relativePath)
  {
      // Get workspace path from system configuration.
    QString path = FileSystemUtil::getPathFromInstallationPath("workspaces");
      // Prepare the absolute path.
      path = path + "/acq_ws/" + relativePath;
      // Return the absolute path.
      return QDir::fromNativeSeparators(path);
  }
  
}
