#ifndef HMTRIGGERBESTZ_H
#define HMTRIGGERBESTZ_H

#include <QObject>
#include <QThread>
#include <tuple>
#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"
#ifdef WBC_CELL_DETECTION
#include "Common/Framework/RegionDetection/WBCDetection.h"
#endif
namespace spin
{
    /**
    * @brief The SnapZThread class is a worker class for capturing with trigger
    */
    class SnapZThread : public QThread
    {
        Q_OBJECT

        public:
            /**
            * @brief SnapZThread Default class constructor, initializes objects
            * owned by the class.
            * @param parent
            */
            SnapZThread(QObject* parent = 0);

            /**
            * @brief ~SnapZThread Default class destructor, destroys objects owned
            * by the class.
            */
            ~SnapZThread();

            /**
            * @brief init To initialize the worker with focusStepSize and normalSpeed
            * internally this should handle trigger capture movement speed and return back with a normalSpeed.
            * @param focusStepSize distance between two captures(um).
            * @param normalSpeed starting position of Z to capture(mm/s).
            * @param acqStackSize for how many images to capture.
            */
            void init(double focusStepSize, double  normalSpeed, int acqStackSize)
            {
                m_focusStepSize = focusStepSize;
                m_normalSpeed = normalSpeed;
                m_acqStackSize = acqStackSize;
            }

            std::vector<cv::Mat>m_imageStack;
            bool m_err;

        protected:
            void run();

        private:
            double m_focusStepSize;
            double m_normalSpeed;
            int m_acqStackSize;


    };
    /**
    * @brief The SnapZThread class is a Thread class for capturing at different Z levels
    */
    class TriggerBestZ : public QObject
    {
        Q_OBJECT
        public:
            /**
            * @brief TriggerBestZ Default class constructor, initializes objects
            * owned by the class.
            * @param parent
            */
            TriggerBestZ(double xPos, double yPos, QObject* parent = 0);

            /**
            * @brief ~TriggerBestZ Default class destructor, destroys objects owned
            * by the class.
            */
            ~TriggerBestZ();

            /**
            * @brief init To initialize the worker with startZ stopZ focusStepSize and normalSpeed
            * internally this should handle trigger movement speed and return back with a normalSpeed.
            * @param startZ starting position of Z to capture(um).
            * @param stopZ stop position of Z till when to capture(um).
            * @param focusStepSize distance between two captures(um).
            * @param normalSpeed starting position of Z to capture(mm/s).
            */
            void init(double startZ, double stopZ, double focusStepSize,
                      double normalSpeed, int specType);

            Q_SIGNALS:
                /**
                * @brief captureFinished Signal emitted after capturing all the Z.
                */
                void captureFinished();

                /**
                * @brief captureError Signal emitted if an error occurs while capturing.
                */
                void captureError();

                public Q_SLOTS:
                /**
                * @brief onStartCapture Slot called to start capture.
                */
                std::tuple<double, int, double, std::string>
                  onStartCapture(QString slideName);

        private:
                /**
            * @brief getBestIndex
            * @return
            */
           int getBestIndex();

        private:
           /**
            * @brief MAX_RETRY_ATTEMPTS
            */
           static const int MAX_RETRY_ATTEMPTS;

           /**
            * @brief BF_FM_THRESHOLD
            */
           static const double BG_FM_THRESHOLD;

           /**
            * @brief m_retryCount
            */
           int m_retryCount;

            /**
            * @brief m_specType To store the specimen type being acquired.
            */
            SpecimenType m_specType;

            /**
            * @brief m_startZ starting position of Z to capture(um)
            */
            double m_startZ;

            /**
            * @brief m_stopZ stop position of Z till when to capture(um)
            */
            double m_stopZ;

            /**
            * @brief m_focusStepSize distance between two captures(um)
            */
            double m_focusStepSize;

            /**
            * @brief m_imageStackSize how many images to capture
            */
            int m_imageStackSize;

            /**
            * @brief m_normalSpeed normalSpeed starting position of Z to capture(mm/s)
            */
            double m_normalSpeed;

            /**
             * @brief m_bestFocusMetric To store the best focus metric value of
             * the current image.
             */
            qreal m_bestFocusMetric;

            /**
            * @brief m_szt module to capture images in thread
            */
            SnapZThread m_szt;

            QPointF m_xyPos;

            /**
            * @brief m_szt module to capture images in thread
            */
            //RegionDetection<cv::Mat> m_regionDetect;


    };// end of CaptureWorker class.
} // end of spin namespace.

#endif // HMTRIGGERBESTZ_H
