#ifndef HMCOMMON_H
#define HMCOMMON_H

#ifdef _MSC_VER // Visual Studio
    /**
     * @macro FUNCTION_NAME gives the name of the function in which the macro
     * is used. It is equal to __FUNCTION__ macro in visual studio compilers.
     */
    #define FUNCTION_NAME __FUNCTION__
#else
    /**
     * @macro FUNCTION_NAME gives the name of the function in which the macro
     * is used. C function macro __func__ is used if the compiler is not visual
     * studio.
     */
    #define FUNCTION_NAME __PRETTY_FUNCTION__
#endif


/**
* @macro SPIN_INTERFACE Interface is a class/struct with all it's methods are
*  public and pure virtual. This is just a marker for a interface and does not
*  do any validations
*/
#define SPIN_INTERFACE struct

/**
 * @macro OUT_PARAM Marker for a out parameter
 */
#define OUT_PARAM

/**
 * @macro IN_OUT_PARAM Marker for in-out parameter
 */
#define IN_OUT_PARAM

/**
 * @macro DISALLOW_COPY_AND_ASSIGNMENT(TypeName) This macro when used in the
 * private part of a class prevents compiler from unnecessarily generating copy
 * constructor and assignment operator for that class or it's children
 */
#define DISALLOW_COPY_AND_ASSIGNMENT(TypeName)    \
   TypeName(const TypeName&);                     \
   void operator=(const TypeName&);

#endif // COMMON_H
