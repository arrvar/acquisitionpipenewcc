#ifndef UTILITIES_H
#define UTILITIES_H

#include <opencv2/opencv.hpp>

#include <QPointF>
#include <QSizeF>

#include "Common/Framework/HardwareManager/Utils/Common.h"
#include "Common/Framework/HardwareManager/Utils/Types.h"

namespace spin
{
  /**
   * @brief The Utilities class consists of static methods for generic purposes.
   */
  class Utilities
  {
  public:
    /**
     * @brief micronsToPixels To convert from microns to pixel coordinates
     * on the virtual slide.
     * @param micronPos Coordinates in microns.
     * @return Returns coordinates in pixels.
     */
    static QPointF micronsToPixels(const QPointF& micronPos);

    /**
     * @brief pixelsToMicrons To convert from virtual slide pixel coordinates
     * to microns.
     * @param pixelPos Coordinates in pixels.
     * @return Returns coordinates in microns.
     */
    static QPointF pixelsToMicrons(const QPointF& pixelPos);

    /**
     * @brief getFovSizeMicrons To retrieve the size of the FoV for the given
     * magnification in microns.
     * @param magVal Magnification of the FoV.
     * @return Returns the size of the FoV in microns.
     */
    static QSizeF getFovSizeMicrons(Magnification magVal);

    /**
     * @brief getFovSizePixels To retrieve the size of the FoV for the given
     * magnification in pixels.
     * @param magVal Magnification of the FoV.
     * @return Returns the size of the FoV in pixels.
     */
    static QSizeF getFovSizePixels(Magnification magVal);

    /**
     * @brief loadSlotParams To load the given slot parameters into the
     * system configuration.
     * @param slotNum Slot number.
     * @return Returns true if the slot parameters are available and have been
     * loaded for the given slot number.
     */
    static bool loadSlotParams(int slotNum);

    /**
     * @brief writeStatus To write the status of the current acquisition into
     * a file.
     * @param filePath Absolute path of the file.
     * @param statusMsg Current status message of the file.
     * @return Returns true on successfully writing the status into the file
     * else returns false.
     */
    static bool writeStatus(QString filePath, QString statusMsg);

    /**
     * @brief getCamExpValue To retrieve the camera exposure value for the
     * given magnification.
     * @param magValue The given magnification value.
     * @return Returns the exposure value of the camera for the given
     * magnification.
     */
    static qreal getCamExpValue(Magnification magValue);

    /**
     * @brief getZOffset To get the offset in Z-Stage value from the a lower
     * to the given magnification value.
     * @param magValue The higher magnification value.
     * @return Returns the corresponding offset value.
     */
    static qreal getZOffset(Magnification lowMag,
                            Magnification highMag);

    /**
     * @brief getZStackLimits
     * @param magValue
     * @param lowerLimit
     * @param upperLimit
     */
    static void getZStackLimits(Magnification magValue,
                                qreal& lowerLimit, qreal& upperLimit);

    /**
     * @brief getZStageStepSize
     * @param magValue
     * @return
     */
    static qreal getZStageStepSize(Magnification magValue);

    /**
     * @brief magnificationString To retrieve string equivalent of the
     * magnification value.
     * @param magValue Given magnification value.
     * @return Returns the string equivalent of the given magnification value.
     */
    static QString magnificationString(Magnification magValue);

    /**
     * @brief getXYZTriggerStepSize To get the step size for the Z-Stage
     * during continuous acquisition.
     * @param magValue Magnification value.
     * @return Returns the step size in microns.
     */
    static double getXYZTriggerStepSize(Magnification magValue);

    /**
     * @brief getDefaultZStageSpeed To get the default Z-Stage speed for
     * continuous acquisition.
     * @param magValue Magnification value.
     * @return Returns the Z-Stage speed in mm/s.
     */
    static double getDefaultZStageSpeed(Magnification magValue);

    /**
     * @brief isBackground To check if the given image is a background or not.
     * @param inputImg Input image.
     * @param specType Specimen type.
     * @return Returns true if the image is a background else returns false.
     */
    static bool isBackground(cv::Mat& inputImg, SpecimenType specType);

    /**
     * @brief computeFocusMetric To compute the focus metric of the given input
     * image.
     * @param inputImg Input image.
     * @return Returns the computed focus metric value of the image.
     */
    static double computeFocusMetric(cv::Mat &inputImg);

    /**
     * @brief computeColorMetric To compute the color metric of the given input
     * image.
     * @param inputImg Input image.
     * @param specType Specimen type.
     * @return Returns the computed color metric value of the image.
     */
    static double computeColorMetric(cv::Mat& inputImg,
                                     SpecimenType specType);

  private:
    /**
     * @brief Utilities Private destructor for preventing unnecessary
     * instantiation.
     */
    Utilities();

    DISALLOW_COPY_AND_ASSIGNMENT(Utilities)

  };// end of Utilities class.
} // end of spin namespace.

#endif // UTILITIES_H
