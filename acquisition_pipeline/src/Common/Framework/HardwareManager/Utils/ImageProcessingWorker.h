#ifndef IMAGEPROCESSINGWORKER_H
#define IMAGEPROCESSINGWORKER_H

#include <QObject>
#include <QMutex>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

namespace spin
{
  // Forward declaration.
  class ProcessingInterface;

  /**
   * @brief The ImageProcessingWorker class is a worker class that will process
   * images when available.
   */
  class ImageProcessingWorker : public QObject
  {
    Q_OBJECT
  public:
    /**
     * @brief ImageProcessingWorker Default class constructor, initializes
     * objects owned by the class.
     * @param slideName Name of the slide that is being acquired.
     * @param gridName Name of the grid that is being acquired.
     * @param parent Pointer to a parent QObject class.
     */
    explicit ImageProcessingWorker(QString slideName,
                                   QString gridName,
                                   QObject* parent = 0);

    /**
     * @brief ~ImageProcessingWorker Default class destructor, destorys objects
     * owned by the class.
     */
    ~ImageProcessingWorker();

    /**
     * @brief init To initialize the worker.
     * @return Returns true on successful initialization of the worker.
     */
    bool init(int totalAoiCount);

  Q_SIGNALS:
    /**
     * @brief workerFinished Signal emitted when the worker has finished
     * its task.
     */
    void workerFinished();

  public Q_SLOTS:
    /**
     * @brief onStartWorker Slot called to start the task of the worker.
     */
    void onStartWorker();

    /**
     * @brief onStopWorker Slot called to stop the task of the worker.
     */
    void onStopWorker();

    /**
     * @brief onAbortWorker Slot called to abort the task of the worker.
     */
    void onAbortWorker();

  private:
    /**
     * @brief processImage To process the available image.
     */
    void processImage();

  private:
    /**
     * @brief m_stopWorker Flag to check if the worker has to stop its task.
     * True indicates that the task has to be stopped.
     * False indicates that the task should continue as is.
     */
    bool m_stopWorker;

    /**
     * @brief m_abortWorker Flag to check if the worker has to abort its task.
     * True indicates that the task has to be aborted.
     * False indicates that the task should continue as is.
     */
    bool m_abortWorker;

    /**
     * @brief m_procAoiCount To store count of the AoIs that have been
     * processed.
     */
    int m_procAoiCount;

    /**
     * @brief m_totalAoiCount To store the total no. of AoIs that need to be
     * processed.
     */
    int m_totalAoiCount;

    /**
     * @brief m_slideName To store the name of the slide that is being acquired.
     */
    QString m_slideName;

    /**
     * @brief m_gridName To store the name of the grid that is being acquired.
     */
    QString m_gridName;

    /**
     * @brief m_procType To store the last processing type.
     * Needs to be re-written.
     */
    ProcessType m_procType;

    /**
     * @brief m_stopMutex Mutex to synchronize access to the stop worker flag.
     */
    QMutex m_stopMutex;

    /**
     * @brief m_procInterface Pointer to the processing interface.
     */
    ProcessingInterface*  m_procInterface;

  };// end of ImageProcessingWorker class.
} // end of spin namespace.

#endif // IMAGEPROCESSINGWORKER_H
