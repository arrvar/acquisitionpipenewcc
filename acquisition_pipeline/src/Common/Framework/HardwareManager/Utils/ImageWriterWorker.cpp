#include <QThread>
#include <QDir>

#include "Common/Framework/HardwareManager/Utils/ImageWriterWorker.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"

namespace spin
{
  /**
   * @brief ImageWriterWorker Default class constructor, initializes objects
   * owned by the class.
   * @param parent Pointer to a oarent QObject class.
   */
  ImageWriterWorker::ImageWriterWorker(QObject* parent) :
    QObject(parent),
    m_stopWorker(false),
    m_writtenAoiCount(0),
    m_totalAoiCount(0)
  {

  }

  /**
   * @brief writeImage To write the image to the disk.
   * @return Returns true if write was successful else returns false.
   */
  bool ImageWriterWorker::writeImage()
  {
    bool retVal = true;
    void* _vPtr = SHARED_WRITE_BUFFER()->get();
    acquisition::AoIInfo* aoiInfo = static_cast<acquisition::AoIInfo* >(_vPtr);
    retVal = cv::imwrite(aoiInfo->m_destPath.toStdString(), aoiInfo->m_img);
    if(aoiInfo->m_whiteCorr == true)
    {
      cv::Mat whiteCorrImg = (aoiInfo->m_img / m_whiteImg) * 230;
      whiteCorrImg.convertTo(whiteCorrImg, aoiInfo->m_img.type());
      retVal = cv::imwrite(aoiInfo->m_whiteCorrDestPath.toStdString(),
                           whiteCorrImg);
      whiteCorrImg.release();
    }

    if(aoiInfo->m_zStack.isEmpty() == false)
    {
      const int Remove_Images_From_Stack =
          CONFIG()->intConfig(ConfigKeys::REMOVE_IMAGES_FROM_STACK);
      const int stackSize = aoiInfo->m_zStack.size();

      // Create a folder with the AoI name in the image_stack folder.
      QDir imageStackDir(aoiInfo->m_stackDestPath);
      imageStackDir.mkdir(aoiInfo->m_name);
      for (int idx = Remove_Images_From_Stack;
           idx < stackSize; idx++)
      {
        QString imgName = "img_" + QString::number(idx);
        QString imageStackPath = aoiInfo->m_stackDestPath + "/" +
          aoiInfo->m_name + "/" + imgName + ".bmp";
        cv::Mat capturedImage(cv::Size(Constants::FRAME_WIDTH,
                                       Constants::FRAME_HEIGHT), CV_8UC4,
                              aoiInfo->m_zStack[idx]);
        cv::imwrite(imageStackPath.toStdString(), capturedImage);
        delete[] aoiInfo->m_zStack[idx];
      }
    }

    aoiInfo->m_img.release();
    delete aoiInfo;
    m_writtenAoiCount++;
    return retVal;
  }

  /**
   * @brief onStartWorker Slot called to start the task of the worker.
   */
  void ImageWriterWorker::onStartWorker()
  {
    bool writeErr = false;
    Q_FOREVER
    {
      // If the sahred buffer is empty then wait until data is available.
      if(SHARED_WRITE_BUFFER()->isEmpty())
      {
        SHARED_WRITE_BUFFER()->wait();
      }

      // Pop out data only if the queue is not empty.
      if(SHARED_WRITE_BUFFER()->isEmpty() == false)
      {
        if(!writeImage())
        {
          writeErr = true;
        }
      }

      m_stopMutex.lock();
      if((m_stopWorker == true) || (writeErr == true))
      {
        m_stopMutex.unlock();
        if(m_writtenAoiCount >= m_totalAoiCount)
        {
          break;
        }
      }
      m_stopMutex.unlock();
    }

    if(writeErr == false)
    {
      // Flush out remaining data if any.
      while(SHARED_WRITE_BUFFER()->isEmpty() == false)
      {
        writeImage();
      }
    }
    else
    {
      // Flush out remaining data if any.
      while(SHARED_WRITE_BUFFER()->isEmpty() == false)
      {
        void* _vPtr = SHARED_WRITE_BUFFER()->get();
        acquisition::AoIInfo* aoiInfo = static_cast<acquisition::AoIInfo* >(_vPtr);
        aoiInfo->m_img.release();
        delete aoiInfo;
      }
    }

    if(thread())
    {
      thread()->quit();
    }

    if(writeErr == false)
    {
      Q_EMIT workerFinished();
    }
    else
    {
      QString errMsg = "Write failed.";
      Q_EMIT workerError(errMsg);
    }
  }

  /**
   * @brief onStopWorker Slot called to stop the task of the worker.
   */
  void ImageWriterWorker::onStopWorker()
  {
    m_stopMutex.lock();
    m_stopWorker = true;
    m_stopMutex.unlock();

    SHARED_WRITE_BUFFER()->resume();
  }

  /**
   * @brief init To initialize the worker with default parameters.
   * @param whiteRefPath Absolute path of the white reference image.
   */
  void ImageWriterWorker::init(QString whiteRefPath)
  {
    m_whiteImg = cv::imread(whiteRefPath.toStdString());
  }

  /**
   * @brief ~ImageWriterWorker Default class destructor, destorys objects
   * owned by the class.
   */
  ImageWriterWorker::~ImageWriterWorker()
  {

  }
}
