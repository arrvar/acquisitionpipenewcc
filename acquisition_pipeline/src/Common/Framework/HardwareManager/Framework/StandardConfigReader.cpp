#include <boost/property_tree/xml_parser.hpp>

#include <QFile>
#include <QProcess>
#include <QStringList>
#include <QDebug>

#include "Common/ErrorManagement/ErrorHandler.h"

#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"
#include "Common/Framework/HardwareManager/Framework/StandardConfigReader.h"

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

namespace spin
{
  StandardConfigReader::StandardConfigReader()
  {

  }

  void StandardConfigReader::populateSlotOffsets(SystemConfig* config,
                                               pt::ptree tree)
  {
    SPIN_LOG_INFO("Slot Offsets");
    int maxSlotCount = config->intConfig(ConfigKeys::MAX_SLOT_COUNT);
    for (int index = 1; index <= maxSlotCount; index++)
    {
      QString slotStr = QString("Slot_") + QString::number(index);
      QString path = QString("Calibration.Acquisition.") + slotStr;

      SPIN_LOG_INFO(slotStr);
      for (auto slotsConfig : tree.get_child(path.toStdString()))
      {
        pt::ptree slotTree = slotsConfig.second;
        acquisition::SlotParams params;

        if (slotsConfig.first == "XY_Stage")
        {
          double xOffset = slotTree.get<double>("X_Offset");
          double yOffset = slotTree.get<double>("Y_Offset");

          params.m_slotNum = index;
          params.m_offsetX = xOffset;
          params.m_offsetY = yOffset;

          config->addSlotParam(index, params);

          SPIN_LOG_INFO(QObject::tr("X Offset: %1").
                        arg(QString::number(xOffset)));
          SPIN_LOG_INFO(QObject::tr("Y Offset: %1").
                        arg(QString::number(yOffset)));
        }
      }
    }
  }


  void StandardConfigReader::populateCameraConfig(SystemConfig *config,
                                                pt::ptree tree)
  {
    // Camera configurations
    SPIN_LOG_INFO("Camera parameters");
    for (auto camConfig : tree.get_child("Calibration.Acquisition.Camera"))
    {
      pt::ptree camTree = camConfig.second;

      if (camConfig.first == "Frame_Size")
      {
        SPIN_LOG_INFO("Frame Size at 20x");
        for (auto frameProps : camTree.get_child("Mag_20x"))
        {
          QString frameSize = QString::fromStdString(frameProps.second.data());
          if (frameProps.first == "Width")
          {
            config->addConfig(ConfigKeys::FOV_WIDTH_20X_MICRONS,
                              new QVariant(frameSize));
            SPIN_LOG_INFO(QObject::tr("Width: %1").arg(frameSize));
          }
          else if (frameProps.first == "Height")
          {
            config->addConfig(ConfigKeys::FOV_HEIGHT_20X_MICRONS,
                              new QVariant(frameSize));
            SPIN_LOG_INFO(QObject::tr("Height: %1").arg(frameSize));
          }
        }

        SPIN_LOG_INFO("Frame Size at 40x");
        for (auto frameProps : camTree.get_child("Mag_40x"))
        {
          QString frameSize = QString::fromStdString(frameProps.second.data());
          if (frameProps.first == "Width")
          {
            config->addConfig(ConfigKeys::FOV_WIDTH_40X_MICRONS,
                              new QVariant(frameSize));
            SPIN_LOG_INFO(QObject::tr("Width: %1").arg(frameSize));
          }
          else if (frameProps.first == "Height")
          {
            config->addConfig(ConfigKeys::FOV_HEIGHT_40X_MICRONS,
                              new QVariant(frameSize));
            SPIN_LOG_INFO(QObject::tr("Height: %1").arg(frameSize));
          }
        }

        SPIN_LOG_INFO("Frame Size at 100x");
        for (auto frameProps : camTree.get_child("Mag_100x"))
        {
          QString frameSize = QString::fromStdString(frameProps.second.data());
          if (frameProps.first == "Width")
          {
            config->addConfig(ConfigKeys::FOV_WIDTH_100X_MICRONS,
                              new QVariant(frameSize));
            SPIN_LOG_INFO(QObject::tr("Width: %1").arg(frameSize));
          }
          else if (frameProps.first == "Height")
          {
            config->addConfig(ConfigKeys::FOV_HEIGHT_100X_MICRONS,
                              new QVariant(frameSize));
            SPIN_LOG_INFO(QObject::tr("Height: %1").arg(frameSize));
          }
        }

      }

      else if (camConfig.first == "Color_Balance")
      {
        SPIN_LOG_INFO(QObject::tr("Color Balance"));
        for (auto ratios : camTree.get_child(""))
        {
          std::string balanceRatio = ratios.second.data();
          if (ratios.first == "Red_Balance_Ratio")
          {
            config->addConfig(ConfigKeys::RED_BALANCE_RATIO,
                              new QVariant(QString::
                                           fromStdString(balanceRatio)));
            SPIN_LOG_INFO(QObject::tr("Red Balance Ratio: %1").
                          arg(balanceRatio.c_str()));
          }
          else if (ratios.first == "Blue_Balance_Ratio")
          {
            config->addConfig(ConfigKeys::BLUE_BALANCE_RATIO,
                              new QVariant(QString::
                                           fromStdString(balanceRatio)));
            SPIN_LOG_INFO(QObject::tr("Blue Balance Ratio: %1").
                          arg(balanceRatio.c_str()));
          }
        }
      }

      if (camConfig.first == "Acquisition_Exposure")
      {
        SPIN_LOG_INFO(QObject::tr("Acquisition Exposure"));
        for (auto exposures : camTree.get_child(""))
        {
          std::string expVal = exposures.second.data();
          if (exposures.first == "Mag_20x")
          {
            config->addConfig(ConfigKeys::CAM_EXP_20X,
                              new QVariant(QString::fromStdString(expVal)));
            SPIN_LOG_INFO(QObject::tr("20x: %1").arg(expVal.c_str()));
          }
          else if (exposures.first == "Mag_40x")
          {
            config->addConfig(ConfigKeys::CAM_EXP_40X,
                              new QVariant(QString::fromStdString(expVal)));
            SPIN_LOG_INFO(QObject::tr("40x: %1").arg(expVal.c_str()));
          }
          else if (exposures.first == "Mag_100x")
          {
            config->addConfig(ConfigKeys::CAM_EXP_100X,
                              new QVariant(QString::fromStdString(expVal)));
            SPIN_LOG_INFO(QObject::tr("100x: %1").arg(expVal.c_str()));
          }
          else if (exposures.first == "Mag_100x_Oil")
          {
            config->addConfig(ConfigKeys::CAM_EXP_100X_OIL,
                              new QVariant(QString::fromStdString(expVal)));
            SPIN_LOG_INFO(QObject::tr("100x Oil: %1").arg(expVal.c_str()));
          }
        }
      }
      else if (camConfig.first == "Continuous_Acquisition_Exposure")
      {
        SPIN_LOG_INFO(QObject::tr("Continuous Acquisition Exposure"));
        for (auto exposures : camTree.get_child(""))
        {
          std::string expVal = exposures.second.data();
          if (exposures.first == "Mag_20x")
          {
            config->addConfig(ConfigKeys::CONT_ACQ_EXP_20X,
                              new QVariant(QString::fromStdString(expVal)));
            SPIN_LOG_INFO(QObject::tr("20x: %1").arg(expVal.c_str()));
          }
          else if (exposures.first == "Mag_40x")
          {
            config->addConfig(ConfigKeys::CONT_ACQ_EXP_40X,
                              new QVariant(QString::fromStdString(expVal)));
            SPIN_LOG_INFO(QObject::tr("40x: %1").arg(expVal.c_str()));
          }
        }
      }
    }
  }


  void StandardConfigReader::populateZStageConfig(SystemConfig *config,
                                                pt::ptree tree)
  {
    SPIN_LOG_INFO("Z-Stage parameters")
    for (auto stageConfig : tree.get_child("Calibration.Acquisition.Z_Stage"))
    {
      pt::ptree stageTree = stageConfig.second;

      if(stageConfig.first == "Backlash")
      {
          std::string backlashZ = stageConfig.second.data();
          config->addConfig(ConfigKeys::BACKLASH_Z,
                            new QVariant(QString::fromStdString(backlashZ)));
          SPIN_LOG_INFO(QObject::tr("Z Backlash: %1").arg(backlashZ.c_str()));
      }

      if (stageConfig.first == "Step_Size")
      {
        SPIN_LOG_INFO("Step Size")
        for (auto mag : stageTree.get_child(""))
        {
          std::string stepSize = mag.second.data();
          if (mag.first == "Mag_20x")
          {
            config->addConfig(ConfigKeys::STAGE_Z_STEP_SIZE_20x,
                              new QVariant(QString::fromStdString(stepSize)));
            SPIN_LOG_INFO(QObject::tr("20x: %1").arg(stepSize.c_str()));
          }
          else if (mag.first == "Mag_40x")
          {
            config->addConfig(ConfigKeys::STAGE_Z_STEP_SIZE_40x,
                              new QVariant(QString::fromStdString(stepSize)));
            SPIN_LOG_INFO(QObject::tr("40x: %1").arg(stepSize.c_str()));
          }
          else if (mag.first == "Mag_100x")
          {
            config->addConfig(ConfigKeys::STAGE_Z_STEP_SIZE_100x,
                              new QVariant(QString::fromStdString(stepSize)));
            SPIN_LOG_INFO(QObject::tr("100x: %1").arg(stepSize.c_str()));
          }
        }
      }
      else if (stageConfig.first == "Limits")
      {
        SPIN_LOG_INFO("Limits");
        for (auto limits : stageTree.get_child(""))
        {
          std::string zLimits = limits.second.data();
          if (limits.first == "Default_Lower")
          {
            config->addConfig(ConfigKeys::Z_LOWER_LIMIT,
                              new QVariant(QString::fromStdString(zLimits)));
            SPIN_LOG_INFO(QObject::tr("Default Lower: %1").
                          arg(zLimits.c_str()));
          }
          else if (limits.first == "Default_Upper")
          {
            config->addConfig(ConfigKeys::Z_UPPER_LIMIT,
                              new QVariant(QString::fromStdString(zLimits)));
            SPIN_LOG_INFO(QObject::tr("Default Upper: %1").
                          arg(zLimits.c_str()));
          }
          else if (limits.first == "Mag_20x_Lower")
          {
            config->addConfig(ConfigKeys::Z_20X_LOWER_LIMIT,
                              new QVariant(QString::fromStdString(zLimits)));
            SPIN_LOG_INFO(QObject::tr("20x Lower: %1").
                          arg(zLimits.c_str()));
          }
          else if (limits.first == "Mag_20x_Upper")
          {
            config->addConfig(ConfigKeys::Z_20X_UPPER_LIMIT,
                              new QVariant(QString::fromStdString(zLimits)));
            SPIN_LOG_INFO(QObject::tr("20x Upper: %1").
                          arg(zLimits.c_str()));
          }
          else if (limits.first == "Mag_20x_Stack_Lower")
          {
            config->addConfig(ConfigKeys::Z_20X_STACK_LOWER_LIMIT,
                              new QVariant(QString::fromStdString(zLimits)));
            SPIN_LOG_INFO(QObject::tr("20x Stack Lower: %1").
                          arg(zLimits.c_str()));
          }
          else if (limits.first == "Mag_20x_Stack_Upper")
          {
            config->addConfig(ConfigKeys::Z_20X_STACK_UPPER_LIMIT,
                              new QVariant(QString::fromStdString(zLimits)));
            SPIN_LOG_INFO(QObject::tr("20x Stack Upper: %1").
                          arg(zLimits.c_str()));
          }
          else if (limits.first == "Mag_40x_Stack_Lower")
          {
            config->addConfig(ConfigKeys::Z_40X_STACK_LOWER_LIMIT,
                              new QVariant(QString::fromStdString(zLimits)));
            SPIN_LOG_INFO(QObject::tr("40x Stack Lower: %1").
                          arg(zLimits.c_str()));
          }
          else if (limits.first == "Mag_40x_Stack_Upper")
          {
            config->addConfig(ConfigKeys::Z_40X_STACK_UPPER_LIMIT,
                              new QVariant(QString::fromStdString(zLimits)));
            SPIN_LOG_INFO(QObject::tr("40x Stack Upper: %1").
                          arg(zLimits.c_str()));
          }
          else if (limits.first == "Mag_100x_Stack_Lower")
          {
            config->addConfig(ConfigKeys::Z_100X_STACK_LOWER_LIMIT,
                              new QVariant(QString::fromStdString(zLimits)));
            SPIN_LOG_INFO(QObject::tr("100x Stack Lower: %1").
                          arg(zLimits.c_str()));
          }
          else if (limits.first == "Mag_100x_Stack_Upper")
          {
            config->addConfig(ConfigKeys::Z_100X_STACK_UPPER_LIMIT,
                              new QVariant(QString::fromStdString(zLimits)));
            SPIN_LOG_INFO(QObject::tr("100x Stack Upper: %1").
                          arg(zLimits.c_str()));
          }
        }
      }
      else if (stageConfig.first == "Computed_Offsets")
      {
        SPIN_LOG_INFO("Computed_Offsets");
        for (auto offsets : stageTree.get_child(""))
        {
          std::string zOffset = offsets.second.data();
          if (offsets.first == "Mag_20x_To_40x")
          {
            config->addConfig(ConfigKeys::OFFSET_Z_20_TO_40,
                              new QVariant(QString::fromStdString(zOffset)));
            SPIN_LOG_INFO(QObject::tr("20x to 40x: %1").arg(zOffset.c_str()));
          }
          else if (offsets.first == "Mag_20x_To_100x")
          {
            config->addConfig(ConfigKeys::OFFSET_Z_20_TO_100,
                              new QVariant(QString::fromStdString(zOffset)));
            SPIN_LOG_INFO(QObject::tr("20x to 100x: %1").arg(zOffset.c_str()));
          }
          else if (offsets.first == "Mag_40x_To_100x")
          {
            config->addConfig(ConfigKeys::OFFSET_Z_40_TO_100,
                              new QVariant(QString::fromStdString(zOffset)));
            SPIN_LOG_INFO(QObject::tr("40x to 100x: %1").arg(zOffset.c_str()));
          }
        }
      }
    }
  }

  void StandardConfigReader::populateFocusChannels(SystemConfig* config,
                                                 pt::ptree tree)
  {
    SPIN_LOG_INFO("Focus Channels");
    std::string mag20xChannel, mag40xChannel, mag100xChannel;
    for (auto focusChannelConfig : tree.get_child("Calibration.Acquisition"))
    {
      pt::ptree focusChannelTree = focusChannelConfig.second;

      if (focusChannelConfig.first == "Focus_Channel")
      {
        mag20xChannel = focusChannelTree.get<std::string>("Mag_20x");
        mag40xChannel = focusChannelTree.get<std::string>("Mag_40x");
        mag100xChannel = focusChannelTree.get<std::string>("Mag_100x");

        config->addConfig(ConfigKeys::FOCUS_CHANNEL_20X,
                          new QVariant(QString::fromStdString(mag20xChannel)));
        SPIN_LOG_INFO(QObject::tr("20x: %1").arg(mag20xChannel.c_str()));

        config->addConfig(ConfigKeys::FOCUS_CHANNEL_40X,
                          new QVariant(QString::fromStdString(mag40xChannel)));
        SPIN_LOG_INFO(QObject::tr("40x: %1").arg(mag40xChannel.c_str()));

        config->addConfig(ConfigKeys::FOCUS_CHANNEL_100X,
                          new QVariant(QString::fromStdString(mag100xChannel)));
        SPIN_LOG_INFO(QObject::tr("100x: %1").arg(mag100xChannel.c_str()));
      }
    }
  }

  void StandardConfigReader::populateMappingsConfig(SystemConfig *config, pt::ptree tree)
  {
    std::string xOffset, yOffset;

    SPIN_LOG_INFO(QObject::tr("Low Power high power mapping"));
    for (auto mappingConfig : tree.get_child("Calibration.Acquisition."
                                             "Mapping_Low_High_Power"))
    {
      pt::ptree mappingTree = mappingConfig.second;

      if (mappingConfig.first == "Mag_20x_To_40x")
      {
        xOffset = mappingTree.get<std::string>("X_Offset");
        yOffset = mappingTree.get<std::string>("Y_Offset");

        SPIN_LOG_INFO(QObject::tr("20x to 40x X offset: %1")
                      .arg(xOffset.c_str()));
        config->addConfig(ConfigKeys::OFFSET_X_20_TO_40,
                          new QVariant(QString::fromStdString(xOffset)));

        SPIN_LOG_INFO(QObject::tr("20x to 40x Y offset: %1")
                      .arg(yOffset.c_str()));
        config->addConfig(ConfigKeys::OFFSET_Y_20_TO_40,
                          new QVariant(QString::fromStdString(yOffset)));

      }
    }
  }


  void StandardConfigReader::generateWorkspaceJson()
  {
    QStringList args;
    QString script = FileSystemUtil::
      getPathFromInstallationPath("scripts/config_xmlToJson.py");
    QString syscalibXml = FileSystemUtil::
        getPathFromInstallationPath("workspaces/config/sys_calib.xml");
    QString defconfigXml = FileSystemUtil::
        getPathFromInstallationPath("workspaces/config/default_config.xml");

    args << syscalibXml << defconfigXml;

    QProcess *converter = new QProcess();
    converter->start(script, args);
  }


  bool StandardConfigReader::populateDefConfig(SystemConfig *config, int type)
  {
    QString defConfXml = FileSystemUtil::
        getPathFromInstallationPath("workspaces/config/default_config.xml");
    QFile *configFile = new QFile(defConfXml);

    if (configFile->exists())
    {
      pt::ptree tree;
      pt::read_xml(defConfXml.toStdString(), tree);

      std::string speed20x   = tree.get<std::string>("Configuration.Continuous_Speed_20X");
      std::string speed40x   = tree.get<std::string>("Configuration.Continuous_Speed_40X");
      std::string xyMaxSpeed = tree.get<std::string>("Configuration.Maximum_XY_Speed");
      std::string colZStackMode = tree.get<std::string>("Configuration.Column_ZStack_Mode");
      std::string stitchingMode = tree.get<std::string>("Configuration.Enable_Stitching");

      config->addConfig(ConfigKeys::CONTINUOUS_SPEED_20X,
                        new QVariant(QString::fromStdString(speed20x)));
      config->addConfig(ConfigKeys::CONTINUOUS_SPEED_40X,
                        new QVariant(QString::fromStdString(speed40x)));
      config->addConfig(ConfigKeys::MAX_XY_SPEED,
                        new QVariant(QString::fromStdString(xyMaxSpeed)));
      config->addConfig(ConfigKeys::COLUMN_ZSTACK_MODE,
                        new QVariant(QString::fromStdString(colZStackMode)));
      config->addConfig(ConfigKeys::ENABLE_STITCHING,
                        new QVariant(QString::fromStdString(stitchingMode)));

      SPIN_LOG_INFO("Background Threshold");
      for (auto bgThreshold : tree.get_child("Configuration.Background_Threshold"))
      {
        std::string bgThreshVal = bgThreshold.second.data();
        if (bgThreshold.first == "Blood_Smear")
        {

          config->addConfig(ConfigKeys::BG_THRESH_BLOOD_SMEAR,
                            new QVariant(QString::fromStdString(bgThreshVal)));
          SPIN_LOG_INFO(QObject::tr("Blood Smear: %1").arg(bgThreshVal.c_str()));
        }
        else if (bgThreshold.first == "Tissue_Biopsy")
        {
          config->addConfig(ConfigKeys::BG_THRESH_TISSUE_BIOPSY,
                            new QVariant(QString::fromStdString(bgThreshVal)));
          SPIN_LOG_INFO(QObject::tr("Tissue Biopsy: %1").arg(bgThreshVal.c_str()));
        }
      }

      // Focus Metric Threshold.
      std::string focusMetricThreshold = tree.get<std::string>
          ("Configuration.Focus_Metric_Threshold");
      config->addConfig(ConfigKeys::FOCUS_METRIC_THRESHOLD,
                        new QVariant(QString::
                                     fromStdString(focusMetricThreshold)));
      SPIN_LOG_INFO(QObject::tr("Focus Metric Threshold: %1").
                    arg(focusMetricThreshold.c_str()));

      SPIN_LOG_INFO("Background Focus Metric Threshold");
      for (auto bgFmThreshold : tree.get_child("Configuration.Background_Focus_Metric_Threshold"))
      {
        std::string bgFmThreshVal = bgFmThreshold.second.data();
        if (bgFmThreshold.first == "Blood_Smear")
        {

          config->addConfig(ConfigKeys::BG_FM_BLOOD_SMEAR,
                            new QVariant(QString::fromStdString(bgFmThreshVal)));
          SPIN_LOG_INFO(QObject::tr("Blood Smear: %1").arg(bgFmThreshVal.c_str()));
        }
        else if (bgFmThreshold.first == "Tissue_Biopsy")
        {
          config->addConfig(ConfigKeys::BG_FM_TISSUE_BIOPSY,
                            new QVariant(QString::fromStdString(bgFmThreshVal)));
          SPIN_LOG_INFO(QObject::tr("Tissue Biopsy: %1").arg(bgFmThreshVal.c_str()));
        }
      }

      std::string writeImages   = tree.get<std::string>("Configuration."
                                                        "Write_Images");
      config->addConfig(ConfigKeys::WRITE_IMAGES,
                        new QVariant(QString::fromStdString(writeImages)));

      std::string inlineFmCalc = "0";
      try
      {
        inlineFmCalc = tree.get<std::string>("Configuration.Inline_Fm_Calc");
      }
      catch(const std::exception& e)
      {
        SPIN_LOG_ERROR(QObject::tr("Exception occured: %1").arg(e.what()));
      }
      config->addConfig(ConfigKeys::INLINE_FM_CALC,
                        new QVariant(QString::fromStdString(inlineFmCalc)));

      std::string writeStack = "1";
      try
      {
        writeStack = tree.get<std::string>("Configuration.Write_Stack");
      }
      catch(const std::exception& e)
      {
        SPIN_LOG_ERROR(QObject::tr("Exception occured: %1").arg(e.what()));
      }
      config->addConfig(ConfigKeys::WRITE_STACK,
                        new QVariant(QString::fromStdString(writeStack)));

      try
      {
        std::string zTriggerCamFps = tree.get<std::string>("Configuration."
                                                           "Z_Trigger_Cam_Fps");
        config->addConfig(ConfigKeys::Z_TRIGGER_CAM_FPS,
                          new QVariant(QString::fromStdString(zTriggerCamFps)));
      }
      catch (const std::exception& e)
      {
        config->addConfig(ConfigKeys::Z_TRIGGER_CAM_FPS,
                          new QVariant(QString::fromStdString("30")));
        SPIN_ERROR(QObject::tr("Exception occured: %1").arg(e.what()));
      }

      SPIN_LOG_INFO("Z-Stage Speed");
      for (auto zStageSpeed : tree.get_child("Configuration.Z_Stage_Speed"))
      {
        std::string zStageSpeedVal = zStageSpeed.second.data();
        if (zStageSpeed.first == "Max_Speed")
        {

          config->addConfig(ConfigKeys::MAX_Z_STAGE_SPEED,
                            new QVariant(QString::
                                         fromStdString(zStageSpeedVal)));
          SPIN_LOG_INFO(QObject::tr("Maximum Speed: %1").
                        arg(zStageSpeedVal.c_str()));
        }
        else if (zStageSpeed.first == "Default_Speed")
        {
          config->addConfig(ConfigKeys::DEF_Z_STAGE_SPEED,
                            new QVariant(QString::
                                         fromStdString(zStageSpeedVal)));
          SPIN_LOG_INFO(QObject::tr("Default Speed at 20x: %1").
                        arg(zStageSpeedVal.c_str()));
        }
        else if(zStageSpeed.first == "Default_Speed_40x")
        {
          config->addConfig(ConfigKeys::DEF_Z_STAGE_SPEED_40X,
                            new QVariant(QString::
                                         fromStdString(zStageSpeedVal)));
          SPIN_LOG_INFO(QObject::tr("Default Speed at 40x: %1").
                        arg(zStageSpeedVal.c_str()));
        }
      }

      SPIN_LOG_INFO("Driver Currents");
      for (auto DriverCurrents : tree.get_child("Configuration.Driver_Current_Setting"))
      {
        std::string DriverCurrentVal = DriverCurrents.second.data();
        if (DriverCurrents.first == "XY_Run_Current")
        {
          config->addConfig(ConfigKeys::XY_DRIVER_RUN_CURRENT,
                            new QVariant(QString::
                                         fromStdString(DriverCurrentVal)));
          SPIN_LOG_INFO(QObject::tr("XY Run Current: %1").
                        arg(DriverCurrentVal.c_str()));
        }
        else if (DriverCurrents.first == "Z_Run_Current")
        {
          config->addConfig(ConfigKeys::Z_DRIVER_RUN_CURRENT,
                            new QVariant(QString::
                                         fromStdString(DriverCurrentVal)));
          SPIN_LOG_INFO(QObject::tr("Z Run Current: %1").
                        arg(DriverCurrentVal.c_str()));
        }
        else if(DriverCurrents.first == "Turret_Run_Current")
        {
          config->addConfig(ConfigKeys::TURRET_DRIVER_RUN_CURRENT,
                            new QVariant(QString::
                                         fromStdString(DriverCurrentVal)));
          SPIN_LOG_INFO(QObject::tr("Turret Run Current: %1").
                        arg(DriverCurrentVal.c_str()));
        }
        else if(DriverCurrents.first == "XY_Hold_Current")
        {
          config->addConfig(ConfigKeys::XY_DRIVER_HOLD_CURRENT,
                            new QVariant(QString::
                                         fromStdString(DriverCurrentVal)));
          SPIN_LOG_INFO(QObject::tr("XY Hold Current: %1").
                        arg(DriverCurrentVal.c_str()));
        }
        else if (DriverCurrents.first == "Z_Hold_Current")
        {
          config->addConfig(ConfigKeys::Z_DRIVER_HOLD_CURRENT,
                            new QVariant(QString::
                                         fromStdString(DriverCurrentVal)));
          SPIN_LOG_INFO(QObject::tr("Z Hold Current: %1").
                        arg(DriverCurrentVal.c_str()));
        }
        else if(DriverCurrents.first == "Turret_Hold_Current")
        {
          config->addConfig(ConfigKeys::TURRET_DRIVER_HOLD_CURRENT,
                            new QVariant(QString::
                                         fromStdString(DriverCurrentVal)));
          SPIN_LOG_INFO(QObject::tr("Turret Hold Current: %1").
                        arg(DriverCurrentVal.c_str()));
        }
      }
      return true;
    }
    else
    {
      SPIN_LOG_FATAL("default_config.xml file not found");
      return false;
    }
  }


  bool StandardConfigReader::populateSysCalib(SystemConfig *config, int type)
  {
    QString syscalibXml = FileSystemUtil::
        getPathFromInstallationPath("workspaces/config/sys_calib.xml");
    QFile *configFile = new QFile(syscalibXml);

    if (configFile->exists())
    {
      pt::ptree tree;
      pt::read_xml(syscalibXml.toStdString(), tree);

      std::string serialNum = tree.get<std::string>("Calibration.Serial_Num");
      config->addConfig(ConfigKeys::SYS_SERIAL_NUM,
                        new QVariant(QString::fromStdString(serialNum)));
      SPIN_LOG_INFO(QObject::tr("Serial Num: %1").arg(serialNum.c_str()));

      std::string slotCount = tree.get<std::string>("Calibration.Slot_Count");
      config->addConfig(ConfigKeys::MAX_SLOT_COUNT,
                        new QVariant(QString::fromStdString(slotCount)));
      SPIN_LOG_INFO(QObject::tr("Slot Count: %1").arg(slotCount.c_str()));

      // Localization requested
      if (type == 0)
      {
        // Nothing for now.
      }
      // Acquisition requested
      else if (type == 1)
      {
        std::string stageLib = tree.get<std::string>("Calibration.Acquisition."
                                                     "Stage_Lib_Name");
        config->addConfig(ConfigKeys::STAGE_LIB_NAME,
                          new QVariant(QString::fromStdString(stageLib)));
        SPIN_LOG_INFO(QObject::tr("Stage Library Name: %1").
                      arg(stageLib.c_str()));

        SPIN_LOG_INFO("Stage Step Size in microns");
        for(auto stageStepSizeTag : tree.get_child("Calibration.Acquisition."
                                                   "Stage_Step_Size"))
        {
          QString stageStepSize = QString::fromStdString(stageStepSizeTag.
                                                         second.data());
          if(stageStepSizeTag.first == "X")
          {
            config->addConfig(ConfigKeys::STAGE_STEP_SIZE_X,
                              new QVariant(stageStepSize));
            SPIN_LOG_INFO(QObject::tr("X Step Size: %1").arg(stageStepSize));
          }
          else if(stageStepSizeTag.first == "Y")
          {
            config->addConfig(ConfigKeys::STAGE_STEP_SIZE_Y,
                              new QVariant(stageStepSize));
            SPIN_LOG_INFO(QObject::tr("Y Step Size: %1").arg(stageStepSize));
          }
          else if(stageStepSizeTag.first == "Z")
          {
            config->addConfig(ConfigKeys::STAGE_STEP_SIZE_Z,
                              new QVariant(stageStepSize));
            SPIN_LOG_INFO(QObject::tr("Z Step Size: %1").arg(stageStepSize));
          }
        }

        SPIN_LOG_INFO("Stage Microstepping");
        for(auto stageMicrosteppingTag : tree.get_child("Calibration."
                                                        "Acquisition."
                                                        "Stage_Microstepping"))
        {
          QString stageMicrostepping = QString::
              fromStdString(stageMicrosteppingTag.second.data());
          if(stageMicrosteppingTag.first == "X")
          {
            config->addConfig(ConfigKeys::STAGE_MICROSTEPPING_X,
                              new QVariant(stageMicrostepping));
            SPIN_LOG_INFO(QObject::tr("X Microstepping: %1").
                          arg(stageMicrostepping));
          }
          else if(stageMicrosteppingTag.first == "Y")
          {
            config->addConfig(ConfigKeys::STAGE_MICROSTEPPING_Y,
                              new QVariant(stageMicrostepping));
            SPIN_LOG_INFO(QObject::tr("Y Microstepping: %1").
                          arg(stageMicrostepping));
          }
          else if(stageMicrosteppingTag.first == "Z")
          {
            config->addConfig(ConfigKeys::STAGE_MICROSTEPPING_Z,
                              new QVariant(stageMicrostepping));
            SPIN_LOG_INFO(QObject::tr("Z Microstepping: %1").
                          arg(stageMicrostepping));
          }
          else if(stageMicrosteppingTag.first == "Turret")
          {
            config->addConfig(ConfigKeys::STAGE_MICROSTEPPING_TURRET,
                              new QVariant(stageMicrostepping));
            SPIN_LOG_INFO(QObject::tr("Turret Microstepping: %1").
                          arg(stageMicrostepping));
          }
        }

        SPIN_LOG_INFO("Driver Configuration");
        for(auto motorDriverModes : tree.get_child("Calibration."
                                                        "Acquisition."
                                                        "Driver_Configuration"))
        {
          QString driverModes = QString::
              fromStdString(motorDriverModes.second.data());
          if(motorDriverModes.first == "X")
          {
            config->addConfig(ConfigKeys::X_DRIVER_MODE,
                              new QVariant(driverModes));
            SPIN_LOG_INFO(QObject::tr("X Diver Mode: %1").
                          arg(driverModes));
          }
          else if(motorDriverModes.first == "Y")
          {
            config->addConfig(ConfigKeys::Y_DRIVER_MODE,
                              new QVariant(driverModes));
            SPIN_LOG_INFO(QObject::tr("Y Diver Mode: %1").
                          arg(driverModes));
          }
          else if(motorDriverModes.first == "Z")
          {
            config->addConfig(ConfigKeys::Z_DRIVER_MODE,
                              new QVariant(driverModes));
            SPIN_LOG_INFO(QObject::tr("Z Diver Mode: %1").
                          arg(driverModes));
          }
          else if(motorDriverModes.first == "Turret")
          {
            config->addConfig(ConfigKeys::TURRET_DRIVER_MODE,
                              new QVariant(driverModes));
            SPIN_LOG_INFO(QObject::tr("Turret Diver Mode: %1").
                          arg(driverModes));
          }
        }

        SPIN_LOG_INFO("Trigger Delay");
        for(auto triggerDelayTag : tree.get_child("Calibration.Acquisition."
                                                  "Trigger_Delay"))
        {
          std::string triggerDelay = triggerDelayTag.second.data();
          if(triggerDelayTag.first == "XY_Pre_Delay")
          {
            config->addConfig(ConfigKeys::XY_PRE_TRIGGER_DELAY,
                              new QVariant(QString::
                                           fromStdString(triggerDelay)));
            SPIN_LOG_INFO(QObject::tr("XY-Stage Pre-Delay: %1").
                          arg(triggerDelay.c_str()));
          }
          else if(triggerDelayTag.first == "XY_Post_Delay")
          {
            config->addConfig(ConfigKeys::XY_POST_TRIGGER_DELAY,
                              new QVariant(QString::
                                           fromStdString(triggerDelay)));
            SPIN_LOG_INFO(QObject::tr("XY-Stage Post-Delay: %1").
                          arg(triggerDelay.c_str()));
          }
          else if(triggerDelayTag.first == "Z_Pre_Delay")
          {
            config->addConfig(ConfigKeys::Z_PRE_TRIGGER_DELAY,
                              new QVariant(QString::
                                           fromStdString(triggerDelay)));
            SPIN_LOG_INFO(QObject::tr("Z-Stage Pre-Delay: %1").
                          arg(triggerDelay.c_str()));
          }
          else if(triggerDelayTag.first == "Z_Post_Delay")
          {
            config->addConfig(ConfigKeys::Z_POST_TRIGGER_DELAY,
                              new QVariant(QString::
                                           fromStdString(triggerDelay)));
            SPIN_LOG_INFO(QObject::tr("Z-Stage Post-Delay: %1").
                          arg(triggerDelay.c_str()));
          }
          else if(triggerDelayTag.first == "Z_Stack_Delay")
          {
            config->addConfig(ConfigKeys::Z_STACK_TRIGGER_DELAY,
                              new QVariant(QString::
                                           fromStdString(triggerDelay)));
            SPIN_LOG_INFO(QObject::tr("Z-Stage Stack Delay: %1").
                          arg(triggerDelay.c_str()));
          }
        }

        SPIN_LOG_INFO("Trigger Delay");
        for(auto triggerDurationTag : tree.get_child("Calibration.Acquisition."
                                                  "Trigger_Pulse_Duration"))
        {
          std::string triggerDuration = triggerDurationTag.second.data();
          if(triggerDurationTag.first == "X_Pulse_Duration")
          {
            config->addConfig(ConfigKeys::X_TRIGGER_PULSE_DURATION,
                              new QVariant(QString::
                                           fromStdString(triggerDuration)));
            SPIN_LOG_INFO(QObject::tr("X_Pulse_Duration : %1").
                          arg(triggerDuration.c_str()));
          }
          else if(triggerDurationTag.first == "Y_Pulse_Duration")
          {
            config->addConfig(ConfigKeys::Y_TRIGGER_PULSE_DURATION,
                              new QVariant(QString::
                                           fromStdString(triggerDuration)));
            SPIN_LOG_INFO(QObject::tr("Y_Pulse_Duration : %1").
                          arg(triggerDuration.c_str()));
          }
          else if(triggerDurationTag.first == "Z_Pulse_Duration")
          {
            config->addConfig(ConfigKeys::Z_TRIGGER_PULSE_DURATION,
                              new QVariant(QString::
                                           fromStdString(triggerDuration)));
            SPIN_LOG_INFO(QObject::tr("Z_Pulse_Duration : %1").
                          arg(triggerDuration.c_str()));
          }
        }

        SPIN_LOG_INFO(QObject::tr("Filter Status"));
        for(auto filter : tree.get_child("Calibration.Acquisition.Filter"))
        {
          std::string filterMode = filter.second.data();
          if(filter.first == "Insert")
          {
            config->addConfig(ConfigKeys::INSERT_FILTER_TAG,
                              new QVariant(QString::fromStdString(filterMode)));
            SPIN_LOG_INFO(QObject::tr("Insert Status: %1").
                          arg(filterMode.c_str()));
          }
          else if(filter.first == "Remove")
          {
            config->addConfig(ConfigKeys::REMOVE_FILTER_TAG,
                              new QVariant(QString::fromStdString(filterMode)));
            SPIN_LOG_INFO(QObject::tr("Remove Status: %1").
                          arg(filterMode.c_str()));
          }
        }

        SPIN_LOG_INFO(QObject::tr("Objective Position"));
        for(auto objectivePosTag :
            tree.get_child("Calibration.Acquisition.Objective"))
        {
          std::string objectivePos = objectivePosTag.second.data();
          if(objectivePosTag.first == "Mag_Home")
          {
            config->addConfig(ConfigKeys::MAG_HOME_POS,
                              new QVariant(QString::
                                           fromStdString(objectivePos)));
            SPIN_LOG_INFO(QObject::tr("Mag Home Position: %1").
                          arg(objectivePos.c_str()));
          }
          else if(objectivePosTag.first == "Mag_20x")
          {
            config->addConfig(ConfigKeys::MAG_20X_POS,
                              new QVariant(QString::
                                           fromStdString(objectivePos)));
            SPIN_LOG_INFO(QObject::tr("Mag 20x Position: %1").
                          arg(objectivePos.c_str()));
          }
          else if(objectivePosTag.first == "Mag_40x")
          {
            config->addConfig(ConfigKeys::MAG_40X_POS,
                              new QVariant(QString::
                                           fromStdString(objectivePos)));
            SPIN_LOG_INFO(QObject::tr("Mag 40x Position: %1").
                          arg(objectivePos.c_str()));
          }
          else if(objectivePosTag.first == "Mag_100x")
          {
            config->addConfig(ConfigKeys::MAG_100X_POS,
                              new QVariant(QString::
                                           fromStdString(objectivePos)));
            SPIN_LOG_INFO(QObject::tr("Mag 100x Position: %1").
                          arg(objectivePos.c_str()));
          }
          else if(objectivePosTag.first == "Mag_100x_Oil")
          {
            config->addConfig(ConfigKeys::MAG_100X_OIL_POS,
                              new QVariant(QString::
                                           fromStdString(objectivePos)));
            SPIN_LOG_INFO(QObject::tr("Mag 100x Position: %1").
                          arg(objectivePos.c_str()));
          }
        }

        std::string zTriggerStackSize = "0";
        try
        {
          zTriggerStackSize = tree.get<std::string>("Calibration.Acquisition."
                                                    "XY_Z_Trigger_Stack_Size");
        }
        catch (const std::exception& e)
        {
          SPIN_LOG_ERROR(QObject::tr("Exception occured: %1").arg(e.what()));
        }
        config->addConfig(ConfigKeys::XY_Z_TRIGGER_STACK_SIZE,
                          new QVariant(QString::
                                       fromStdString(zTriggerStackSize)));
        SPIN_LOG_INFO(QObject::tr("XY Z-Trigger Stack Size: %1").
                      arg(zTriggerStackSize.c_str()));

        std::string removeImagesFromStack = "0";
        try
        {
          removeImagesFromStack = tree.get<std::string>("Calibration.Acquisition."
                                                        "Remove_Images_From_Stack");
        }
        catch (const std::exception& e)
        {
          SPIN_LOG_ERROR(QObject::tr("Exception occured: %1").arg(e.what()));
        }
        config->addConfig(ConfigKeys::REMOVE_IMAGES_FROM_STACK,
                          new QVariant(QString::
                                       fromStdString(removeImagesFromStack)));
        SPIN_LOG_INFO(QObject::tr("Remove images from stack: %1").
                      arg(removeImagesFromStack.c_str()));

        std::string zTriggerStepSize = "0";
        try
        {
          zTriggerStepSize = tree.get<std::string>("Calibration.Acquisition."
                                                   "XY_Z_Trigger_Step_Size");
        }
        catch (const std::exception& e)
        {
          SPIN_LOG_ERROR(QObject::tr("Exception occured: %1").arg(e.what()));
        }
        config->addConfig(ConfigKeys::XY_Z_TRIGGER_STEP_SIZE,
                          new QVariant(QString::
                                       fromStdString(zTriggerStepSize)));

        std::string zTriggerStepSize_40x = "0";
        try
        {
          zTriggerStepSize_40x = tree.get<std::string>("Calibration.Acquisition."
                                                       "XY_Z_Trigger_Step_Size_40x");
        }
        catch (const std::exception& e)
        {
          SPIN_LOG_ERROR(QObject::tr("Exception occured: %1").arg(e.what()));
        }
        config->addConfig(ConfigKeys::XY_Z_TRIGGER_STEP_SIZE_40X,
                          new QVariant(QString::
                                       fromStdString(zTriggerStepSize_40x)));

        std::string zOffsetSteps = "0";
        try
        {
          zOffsetSteps = tree.get<std::string>("Calibration.Acquisition."
                                               "Z_Offset_Steps");
        }
        catch (const std::exception& e)
        {
          SPIN_LOG_ERROR(QObject::tr("Exception occured: %1").arg(e.what()));
        }
        config->addConfig(ConfigKeys::Z_OFFSET_STEPS,
                          new QVariant(QString::fromStdString(zOffsetSteps)));
        SPIN_LOG_INFO(QObject::tr("Z-Offset Steps: %1").
                      arg(zOffsetSteps.c_str()));

        std::string xUpperLimit = tree.get<std::string>("Calibration."
                                                        "Acquisition."
                                                        "X_Upper_Limit");
        config->addConfig(ConfigKeys::X_UPPER_LIMIT,
                          new QVariant(QString::fromStdString(xUpperLimit)));
        SPIN_LOG_INFO(QObject::tr("X Upper Limit in microns: %1").
                      arg(xUpperLimit.c_str()));

        std::string yUpperLimit = tree.get<std::string>("Calibration.Acquisition."
                                                        "Y_Upper_Limit");
        config->addConfig(ConfigKeys::Y_UPPER_LIMIT,
                          new QVariant(QString::fromStdString(yUpperLimit)));
        SPIN_LOG_INFO(QObject::tr("Y Upper Limit in microns: %1").
                      arg(yUpperLimit.c_str()));

        populateCameraConfig(config, tree);
        populateMappingsConfig(config, tree);
        populateFocusChannels(config, tree);
        populateZStageConfig(config, tree);
        populateSlotOffsets(config, tree);
      }

      return true;
    }
    else
    {
      SPIN_LOG_FATAL("sys_calib.xml file not found");
      return false;
    }
  }

  bool StandardConfigReader::readConfig(SystemConfig* config)
  {
    return populateSysCalib(config) &&
        populateDefConfig(config);
  }

  StandardConfigReader::~StandardConfigReader()
  {

  }
}
