#ifndef HMSTANDARDDATABASEMANAGER_H
#define HMSTANDARDDATABASEMANAGER_H

#include <QSqlDatabase>

#include "Common/Framework/HardwareManager/Framework/IDatabaseManager.h"

namespace spin
{
  /**
   * @brief The StandardDatabaseManager class is a database manager
   * implementation specific to SQLite.
   */
  class StandardDatabaseManager : public IDatabaseManager
  {
  public:
    /**
     * @brief StandardDatabaseManager Default class constructor.
     */
    StandardDatabaseManager();

    /**
     * @brief ~StandardDatabaseManager Default class destructor.
     */
    virtual ~StandardDatabaseManager();

    /**
     * @brief connect Connects to the slide database.
     * @return Returns true if the connection to the database is successful.
     */
    bool connect(QString slideName);

    /**
     * @brief disconnect Disconnect from a database.
     * @return Returns true if the disconnetion is successful.
     */
    bool disconnect();

    /**
     * @brief getAoIZValue To retrieve focus value of the given AoI.
     * @param aoiId Id of the given AoI.
     * @return Returns the focus value of the given AoI. If not available,
     * it will return -1.
     */
    qreal getAoIZValue(int aoiId);

    /**
     * @brief updateAoIZValue To update the focus value of the given AoI.
     * @param aoiId Id of the AoI.
     * @return Returns true on successfully executing the query else returns
     * false.
     */
    bool updateAoIZValue(int aoiId, qreal zValue, qreal focusMetric);

    /**
     * @brief getAoIInfoList To retrieve the list of all the AoIs for the given
     * grid.
     * @param IN_OUT_PARAM gridInfo Container to store the list of AoIs.
     * @return Returns true on successfully executing the query else returns
     * false.
     */
    bool getAoIInfoList(IN_OUT_PARAM acquisition::GridInfo& gridInfo);

    /**
     * @brief getGridInfo To retrieve the structure of the given grid.
     * @param OUT_PARAM gridInfo To store the structure of the give grid.
     * @param gridName Name of the grid.
     * @return Returns on successfully executing the query else returns false.
     */
    bool getGridInfo(OUT_PARAM acquisition::GridInfo& gridInfo,
                     QString gridName);

    /**
     * @brief getLowPowerMag To retrieve the magnification value of the given
     * low power AoI id.
     * @param lowPowerId Id of the low power AoI.
     * @return Returns the magnification value of the low power AoI.
     */
    Magnification getLowPowerMag(int lowPowerId);

    /**
     * @brief updateAoIFocusMetric To update the focus metric values for the
     * given list of AoIs
     * @param aoiIdList List of AoI Ids.
     * @param focusMetricList List of focus metric values.
     * @param colorMetricList List of color metric values.
     * @param stackPresentList List of status of presence of stack.
     * @param bestIdxList List of best indices of AoI in the stack.
     * @param bestZList List of best z values of AOI in the stack
     * @return Returns true on successfully executing the query else returns
     * false.
     */
    bool updateAoIFocusMetric(const QVariantList& aoiIdList,
                              const QVariantList& focusMetricList,
                              const QVariantList& colorMetricList,
                              const QVariantList& stackPresentList,
                              const QVariantList& bestIdxList,
                              const QVariantList& bestZList);

    /**
     * @brief updateAoIFocusInfo To update the Z values and the focus metric
     * values of all the AoIs in batch mode.
     * @param aoiIdList List of AoI Ids.
     * @param aoiZValueList List of Z values.
     * @param aoiFocusMetricList List of focus metric values.
     * @return Returns true on successfully executing the query else returns
     * false.
     */
    bool updateAoIFocusInfo(const QVariantList& aoiIdList,
                            const QVariantList& aoiZValueList,
                            const QVariantList& aoiFocusMetricList);

  private:
    /**
     * @brief m_slideDb Handle for slide database.
     */
    QSqlDatabase m_slideDb;

  };// end of StandardDatabaseManager class.
} // end of spin namespace.

#endif // STANDARDDATABASEMANAGER_H
