#ifndef HMISYSTEMCONFIGREADER_H
#define HMISYSTEMCONFIGREADER_H

#include "Common/Framework/HardwareManager/Utils/Common.h"

namespace spin
{

  //Forward declaration
  class SystemConfig;

  /**
   * @brief ISystemConfigReader Defines an interface for a configuration reader
   * that populates the system config object.
   */
  SPIN_INTERFACE ISystemConfigReader
  {
    /**
     * @brief readConfig Reads from different system configuration sources and
     * updates the given system configuration objects.
     * @param config System configuration object to be updated.
     * @return true if SystemConfig is successfully populated.
     */
    virtual bool readConfig(OUT_PARAM SystemConfig* config) = 0;

    /**
     * @brief ~ISystemConfigReader Virtual destructor as per convention.
     */
    virtual ~ISystemConfigReader()
    {

    }

  };// end of ISystemConfigReader interface.
} // end of spin namespace.

#endif // ISYSTEMCONFIGREADER_H
