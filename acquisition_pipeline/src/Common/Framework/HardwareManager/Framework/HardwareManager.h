#ifndef HMHARDWAREMANAGER_H
#define HMHARDWAREMANAGER_H

#include <memory>
#include <vector>
#include <map>

#include <QList>

#include "Common/Framework/HardwareManager/Utils/Common.h"
#include "Common/Framework/HardwareManager/Utils/Types.h"

// Forward declaration.
class CMMCore;

namespace spin
{
  // Forward declaration.
  SPIN_INTERFACE IHardwareEventListener;

  /**
   * @brief The DeviceProperty struct to store the name and the value of a
   * device's property.
   */
  struct DeviceProperty
  {
    /**
     * @brief DeviceProperty Contructor, initialize the structure with property
     * values.
     * @param propName Name of the property.
     * @param propValue Value of the property.
     */
    DeviceProperty(std::string propName, std::string propValue) :
      m_propName(propName),
      m_propValue(propValue)
    {

    }

    /**
     * @brief m_propName Name of the property.
     */
    std::string m_propName;

    /**
     * @brief m_propValue Name of the value.
     */
    std::string m_propValue;

  };// end of DeviceProperty structure.

  /**
   * @brief The HardwareManager class manages all the communication between
   * the hardware and the GUI. This class will enabled only when built in CMAKE
   * mode.
   */
  class HardwareManager
  {
  public:
    /**
     * @brief HardwareManager Default class destructor.
     */
    HardwareManager();

    /**
     * @brief ~HardwareManager Default class destructor.
     */
    ~HardwareManager();

    /**
     * @brief addListener Adds a listener which will be notified about the
     * hardware related events that occur in this class's context.
     * @param listener Listener which is to be registered for observation of
     * hardware events.
     */
    void addListener(spin::IHardwareEventListener *listener);

    /**
     * @brief removeListener Unregisters the given listener from observer list.
     * @param listener Instance of listener to be unregistered.
     */
    void removeListener(spin::IHardwareEventListener* listener);

    // Camera operations.
    /**
     * @brief snapImage To capture an image from the camera.
     * @return Returns a raw pointer instance of the snapped image.
     */
    void* snapImage();

    /**
     * @brief setCameraExposure To set the exposure value of the camera.
     * @param exposureValue Exposure value of the camera in milliseconds.
     */
    void setCameraExposure(double exposureValue);

    /**
     * @brief getCameraExposure To get the current exposure value of the camera.
     * @return Returns the current exposure value of the camera in milliseconds.
     */
    double getCameraExposure();

    /**
     * @brief colorBalanceOff To turn off the color balance settings of the
     * camera.
     */
    void colorBalanceOff();

    /**
     * @brief setContinuousMode To set the camera in continuous mode.
     * @param mode Mode of trigger (XY- or Z-Stage).
     */
    void setContinuousMode(TriggerMode mode);

    /**
     * @brief stopContinuousMode To disable the continous mode of the camera.
     */
    void stopContinuousMode();

    /**
     * @brief getCameraTimeLapse
     * @return time delay of the camera
     */
    double getCameraTimeLapse();

    /**
     * @brief openEventCameraTimeLapse
     */
    void openEventCameraTimeLapse();

    /**
     * @brief closeEventCameraTimeLapse
     */
    void closeEventCameraTimeLapse();

    // Z-Stage operations.
    /**
     * @brief setZPosition To set the position of the Z-Stage.
     * @param zVal Position in microns.
     * @param waitTillSet True indicates wait till Z-Stage reaches its position
     * and false indicates don't wait.
     * @return Returns true if Z-Stage position is set successfully else returns
     * false.
     */
    bool setZPosition(double zVal, bool waitTillSet = false);

    /**
     * @brief stopZStage To stop the movement of the Z-Stage.
     */
    void stopZStage();

    /**
     * @brief getZPosition To get the current position of the Z-Stage.
     * @return Returns the current position of the Z-Stage in microns.
     */
    double getZPosition();

    /**
     * @brief setRelativeZPosition To set the relative position of the Z-Stage.
     * @param zVal Relative position in microns.
     * @param waitTillSet True indicates wait till Z-Stage reaches its position
     * and false indicates don't wait.
     */
    void setRelativeZPosition(double zVal, bool waitTillSet = true);

    /**
     * @brief setZStageSpeed To set the speed of the Z-Stage.
     * @param zSpeed Speed in mm/s.
     */
    void setZStageSpeed(double zSpeed);

    /**
     * @brief getZStageSpeed To get the speed of the Z-Stage.
     * @return Returns the speed of the Z-Stage in mm/s.
     */
    double getZStageSpeed();

    /**
     * @brief enableZTriggerMode To enable the trigger mode for the Z-Stage.
     * @param stepSize Step size for the trigger mode.
     */
    void enableZTriggerMode(double stepSize);

    /**
     * @brief setZBacklash To set the backlash value for the Z-Stage.
     * @param backlashValue Backlash value in microns.
     */
    void setZBacklash(double backlashValue);

    // XY-Stage operations.
    /**
     * @brief setXYPosition To set the position of the XY-Stage.
     * @param xPos X position in microns.
     * @param yPos Y position in microns.
     * @param waitTillSet True indicates wait till XY-Stage reaches its position
     * and false indicates don't wait.
     */
    void setXYPosition(double xPos, double yPos, bool waitTillSet = true);

    /**
     * @brief getXYPosition To get the current position of the XY-Stage.
     * @param xPos Reference to the X position in microns.
     * @param yPos Refernce to the Y position in microns.
     */
    void getXYPosition(OUT_PARAM double& xPos,
                       OUT_PARAM double& yPos);

    /**
     * @brief setAbsoluteXYPosition To set the absolute position of the
     * XY-Stage.
     * @param xPos Absolute X position in microns.
     * @param yPos Absolute Y position in microns.
     * @param waitTillSet True indicates wait till XY-Stage reaches its position
     * and false indicates don't wait.
     */
    void setAbsoluteXYPosition(double xPos, double yPos,
                               bool waitTillSet = true);

    /**
     * @brief getAbsoluteXYPosition To get the current absolute position of the
     * XY-Stage.
     * @param xPos Reference to the absolute X position in microns.
     * @param yPos Reference to the absolute Y position in microns.
     */
    void getAbsoluteXYPosition(OUT_PARAM double& xPos,
                               OUT_PARAM double& yPos);

    /**
     * @brief setXYRelativePosition To set the relative position of the
     * XY-Stage.
     * @param xPos X position in microns.
     * @param yPos Y position in microns.
     */
    void setXYRelativePosition(double xPos, double yPos);

    /**
     * @brief stopXYStage To stop the movement of the XY-Stage.
     */
    void stopXYStage();

    /**
     * @brief setXYStageSpeed To set the speed of the XY-Stage.
     * @param xySpeed Speed in mm/s.
     */
    void setXYStageSpeed(double xySpeed);

    /**
     * @brief getXYStageSpeed To get the current movement speed of the
     * XY-Stage.
     * @return Speed of the XY-Stage in mm/s.
     */
    double getXYStageSpeed();

    /**
     * @brief getXYStageStepSize To get the step size of the XY-Stage for
     * capture in continuous mode.
     * @return Returns the step size in microns.
     */
    double getXYStageStepSize();

    /**
     * @brief enableXYTriggerMode To set the XY-Stage into trigger mode for a
     * given step size.
     * @param stepSize Step size in microns for capturing in continuouse mode.
     * @param mode Mode of trigger (X- or Y-).
     */
    void enableXYTriggerMode(double stepSize, TriggerMode mode);

    /**
     * @brief disableTriggerMode To disable the trigger mode set for the
     * XY-Stage.
     */
    void disableTriggerMode();

    /**
     * @brief resetController To reset the controller.
     */
    void resetController();

    /**
     * @brief setZStack To set the Z-stack for continuous mode of acquisition.
     * @param zStack Stack of ';' seperated Z values.
     * @param zStackTable String of ',' separated values for Z-Trigger capture.
     */
    void sendDirectCommand(std::string command);

    /**
     * @brief setZStack To set the Z-stack for continuous mode of acquisition.
     * @param zStack Stack of ';' seperated Z values.
     * @param zStackTable String of ',' separated values for Z-Trigger capture.
     */
    void setZStack(std::string zStack, std::string zStackTable, std::string zFgBgMap);

    /**
     * @brief connect To connect to the available devices.
     * @param magValue Current magnification settings.
     * @return Returns true if the connection to all devices was successful
     * else returns false.
     */
    bool connect(Magnification magValue);

    /**
     * @brief disconnect To disconnect from all connected devices.
     * @return Returns true if the disconnection was successful else returns
     * false.
     */
    bool disconnect();

    /**
     * @brief resetConfiguration To reset the current loaded hardware
     * configuration.
     * @return Returns true on successfully resetting the current loaded
     * configuration else returns false.
     */
    bool resetConfiguration();

    /**
     * @brief isConnected To check if the application is connected or not
     * to the configured hardware devices.
     * @return Returns true if the application is connected to the configured
     * hardware devices else returns false.
     */
    bool isConnected() const
    {
      return m_isConnected;
    }

    /**
     * @brief setObjective To set the objective to the given magnification
     * type.
     * @param magVal Magnification value.
     */
    void setObjective(Magnification magVal);

    /**
     * @brief insertFilter To insert the FLD filter.
     */
    void insertFilter();

    /**
     * @brief removeFilter To remove the FLD filter.
     */
    void removeFilter();

    /**
     * @brief setLedOff To turn OFF the LED.
     */
    void setLedOff();

    /**
     * @brief setLedOn To turn ON the LED.
     */
    void setLedOn();

    /**
     * @brief home To set the translation stage to its home position.
     */
    void home();

    /**
     * @brief eject To eject the translation stage to its home position
     * (Different from home command. Home command moves the translation stage
     * to the home sensor. This function moves the translation stage to its
     * stored 0 position.)
     */
    void eject();

    /**
     * @brief checkPosition To check if the XY-Stage is at the correct offset
     * position.
     */
    void checkPosition();

    /**
     * @brief disableZBacklash To set the backlash of the Z stage.
     * @return Returns true if the properties of the Z stage have
     * been set successfully else returns false.
     */
    bool disableZBacklash();

    /**
     * @brief enableZBacklash To set the backlash of the Z stage.
     * @return Returns true if the properties of the Z stage have
     * been set successfully else returns false.
     */
    bool enableZBacklash();
    /**
     * @brief initCamera To initialize the camera device.
     * @return Returns true if the camera device has been successfully
     * initialized else returns false.
     */

    /**
     * @brief setLedBrightness To set the brightness value of the LED used for
     * acquisition.
     * @param brightnessValue Brightness value.
     */
    void setLedBrightness(double brightnessValue);

    /**
     * @brief setOneXLedBrightness To set the brightness value of the LED
     * @param brightnessValue
     */
    void setOneXLedBrightness(double brightnessValue);

    /**
     * @brief setPropertyStr To set a device property with string value.
     * @param devName Name of the device.
     * @param propName Name of the property.
     * @param propValue Value of the property.
     */
    void setPropertyStr(std::string devName, std::string propName,
                        std::string propValue);

    /**
     * @brief setPropertyInt To set a device property with integer value.
     * @param devName Name of the device.
     * @param propName Name of the property.
     * @param propValue Value of the property.
     */
    void setPropertyInt(std::string devName, std::string propName,
                        long propValue);

    /**
     * @brief setPropertyReal To set a device property with float value.
     * @param devName Name of the device.
     * @param propName Name of the property.
     * @param propValue Value of the property.
     */
    void setPropertyReal(std::string devName, std::string propName,
                         double propValue);

  private:
    /**
     * @brief initAdapterList To initialize the list of available
     * device adapters.
     */
    void initAdapterList();

    /**
     * @brief initComPortList To initialize the list of available COM ports.
     */
    void initComPortList();

    /**
     * @brief initDevice To initialize a device with its properties
     * @param devName Name of the device.
     * @param adapterName Name of the adapter of the device.
     * @param propList List of properties of the device
     * the need to be initialized.
     * @return Returns true if the device has been successfully initialzed
     * else returns false.
     */
    bool initDevice(std::string devName,
                    std::string adapterName,
                    std::vector<DeviceProperty> propList);

    /**
     * @brief initStage To initiaize the translation stage devices.
     * @return Returns true if all the translation devices have been
     * successfully initialize else returns false.
     */
    bool initStage();

    /**
     * @brief setStageProps To set the properties of the translation stage.
     * @return Returns true if the properties of the translation stage have
     * been set successfully else returns false.
     */
    bool setStageProps();

    /**
     * @brief initCamera To initialize the camera device.
     * @return Returns true if the camera device has been successfully
     * initialized else returns false.
     */
    bool initCamera();

    /**
     * @brief setCamProps To set the properties of the camera.
     * @param magVal Current magnification settings.
     * @return Returns true if the properties of the camera have been
     * successfully set else returns false.
     */
    bool setCamProps(Magnification magVal);

  private:
    /**
     * @brief m_isConnected True indicates that the application is connected
     * to the configured hardware devices and false indicates that it is not.
     */
    bool m_isConnected;

    /**
     * @brief m_xyTrigger True indicates trigger mode is ON for XY-Stage.
     * False indicates trigger mode is OFF.
     */
    bool m_xyTrigger;

    /**
     * @brief m_zTrigger True indicates trigger mode is ON for Z-Stage.
     * False indicates trigger mode if OFF.
     */
    bool m_zTrigger;

    /**
     * @brief m_adapterPath To store the path to the available adapters.
     */
    std::string m_adapterPath;

    /**
     * @brief m_camDev To store the name of the connected camera device.
     */
    std::string m_camDev;

    /**
     * @brief m_xyStageDev To store the name of the connected XY-Stage device.
     */
    std::string m_xyStageDev;

    /**
     * @brief m_zStageDev To store the name of the connected Z-Stage device.
     */
    std::string m_zStageDev;

    /**
     * @brief m_devAdapterList To store the list of available device adapters.
     */
    std::vector <std::string> m_devAdapterList;

    /**
     * @brief m_comPortList To store the list of available COM ports.
     */
    std::vector <std::string> m_comPortList;

    /**
     * @brief m_mmcore To store an instance of the micro-manager library.
     */
    std::shared_ptr <CMMCore> m_mmcore;

    /**
     * @brief m_listeners List of objects listening to hardware events.
     */
    QList <IHardwareEventListener* > m_listeners;

    /**
     * @brief m_libDevMap To store the list of available adapters for the
     * given adpater name.
     */
    std::map <std::string, std::vector<std::string>> m_libDevMap;

  };//end of HardwareManager class.
} // end of spin namespace.

#endif // HARDWAREMANAGER_H
