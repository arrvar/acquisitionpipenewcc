#include "Common/Framework/HardwareManager/Framework/AppContext.h"

namespace spin
{
  bool AppContext::s_isInitialized = false;

  /**
   * @brief init To initialise the global resources of the application.
   * @param dbMgr Instance of the datbase manager.
   * @param wsMgr Instance of the workspace manager.
   * @param hwMgr Instance of the hardware manager.
   * @param config Instance of the system configuration.
   * @return Returns true on successful initialisation.
   */
  bool AppContext::init(IDatabaseManager* dbMgr,
                        HardwareManager* hwMgr,
                        SystemConfig* config)
  {
    if(!s_isInitialized)
    {
      AppContext::get().m_databaseManager.reset(dbMgr);
      AppContext::get().m_hardwareManager.reset(hwMgr);
      AppContext::get().m_systemConfig.reset(config);

      s_isInitialized = true;
    }

    return s_isInitialized;
  }

  /**
   * @brief get To retrieve the singleton instance of the class.
   * @return Instance of the class.
   */
  AppContext& AppContext::get()
  {
    static AppContext s_instance;

    return s_instance;
  }

  /**
   * @brief ~AppContext Clas destructor, destorys objects owned by the
   * class.
   */
  AppContext::~AppContext()
  {

  }
}
