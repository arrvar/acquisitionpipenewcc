#ifndef HMIDATABASEMANAGER_H
#define HMIDATABASEMANAGER_H

#include <QString>
#include <QRectF>
#include <QVariantList>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"
#include "Common/Framework/HardwareManager/Utils/Common.h"

namespace spin
{
  /**
   * @brief IDatabaseManager manages database creation, modification and
   * retrieval.
   */
  SPIN_INTERFACE IDatabaseManager
  {
    /**
     * @brief connect Connects to the slide database.
     * @return Returns true if the connection to the database is successful.
     */
    virtual bool connect(QString slideName) = 0;

    /**
     * @brief disconnect Disconnect from a database.
     * @return Returns true if the disconnetion is successful.
     */
    virtual bool disconnect() = 0;

    /**
     * @brief getAoIZValue To retrieve focus value of the given AoI.
     * @param aoiId Id of the given AoI.
     * @return Returns the focus value of the given AoI. If not available,
     * it will return -1.
     */
    virtual qreal getAoIZValue(int aoiId) = 0;

    /**
     * @brief updateAoIZValue To update the focus value of the given AoI.
     * @param aoiId Id of the AoI.
     * @return Returns true on successfully executing the query else returns
     * false.
     */
    virtual bool updateAoIZValue(int aoiId,
                                 qreal zValue, qreal focusMetric) = 0;

    /**
     * @brief getAoIInfoList To retrieve the list of all the AoIs for the given
     * grid.
     * @param IN_OUT_PARAM gridInfo Container to store the list of AoIs.
     * @return Returns true on successfully executing the query else returns
     * false.
     */
    virtual bool getAoIInfoList(IN_OUT_PARAM acquisition::
                                GridInfo& gridInfo) = 0;

    /**
     * @brief getGridInfo To retrieve the structure of the given grid.
     * @param OUT_PARAM gridInfo To store the structure of the give grid.
     * @param gridName Name of the grid.
     * @return Returns on successfully executing the query else returns false.
     */
    virtual bool getGridInfo(OUT_PARAM acquisition::GridInfo& gridInfo,
                             QString gridName) = 0;

    /**
     * @brief getLowPowerMag To retrieve the magnification value of the given
     * low power AoI id.
     * @param lowPowerId Id of the low power AoI.
     * @return Returns the magnification value of the low power AoI.
     */
    virtual Magnification getLowPowerMag(int lowPowerId) = 0;

    /**
     * @brief updateAoIFocusMetric To update the focus metric values for the
     * given list of AoIs
     * @param aoiIdList List of AoI Ids.
     * @param focusMetricList List of focus metric values.
     * @param colorMetricList List of color metric values.
     * @param stackPresentList List of status of presence of stack.
     * @param bestIdxList List of best indices of AoI in the stack.
     * @param bestZList List of best z values of AOI in the stack.
     * @return Returns true on successfully executing the query else returns
     * false.
     */
    virtual bool updateAoIFocusMetric(const QVariantList& aoiIdList,
                                      const QVariantList& focusMetricList,
                                      const QVariantList& colorMetricList,
                                      const QVariantList& stackPresentList,
                                      const QVariantList& bestIdxList,
                                      const QVariantList& bestZList) = 0;

    /**
     * @brief updateAoIFocusInfo To update the Z values and the focus metric
     * values of all the AoIs in batch mode.
     * @param aoiIdList List of AoI Ids.
     * @param aoiZValueList List of Z values.
     * @param aoiFocusMetricList List of focus metric values.
     * @return Returns true on successfully executing the query else returns
     * false.
     */
    virtual bool updateAoIFocusInfo(const QVariantList& aoiIdList,
                                    const QVariantList& aoiZValueList,
                                    const QVariantList& aoiFocusMetricList) = 0;

    /**
     * @brief ~IDatabaseManager A virtual destructor for database manager
     */
    virtual ~IDatabaseManager()
    {

    }

  };// end of IDatabaseManager class.
} // end of spin namespace.

#endif // IDATABASEMANAGER_H
