#ifndef HMIHARDWAREEVENTLISTENER_H
#define HMIHARDWAREEVENTLISTENER_H

#include "Framework/HardwareManager/Utils/Common.h"

namespace spin
{
  /**
   * @brief The IHardwareEventListener defines an interface which needs to be
   * implemented by the class whose objects intend to observe the hardware
   * related events/
   */
  SPIN_INTERFACE IHardwareEventListener
  {
  public:
    /**
     * @brief isEqual Checks the equality of two listeners.
     * @param other Instance of the listener to which this listener has to be
     * compared.
     * @return Returns true if the two instances are equal.
     */
    virtual bool isEqual(IHardwareEventListener* other) = 0;

    /**
     * @brief notifyHardwareConnection This method is called to notify all the
     * listeners of a connection to a hardware device(s).
     */
    virtual void notifyHardwareConnection() = 0;

    /**
     * @brief notifyHardwareDisconnection This method is called to notify all
     * the listeners of a hardware disconnection.
     */
    virtual void notifyHardwareDisconnection() = 0;

    /**
     * @brief ~IHardwareEventListener Virtual destructor as per convention.
     */
    virtual ~IHardwareEventListener()
    {

    }

  };// end of IHardwareEventListener class.
}// end of spin namespace.

#endif // IHARDWAREEVENTLISTENER_H
