#ifndef HMIACQUISITIONEVENTLISTENER_H
#define HMIACQUISITIONEVENTLISTENER_H

#include "Framework/HardwareManager/Utils/Common.h"

namespace spin
{
  SPIN_INTERFACE IAcquisitionEventListener
  {
  public:
    /**
     * @brief isEqual Checks the equality of two listeners.
     * @param other Instance of the listener to which this listener has to be
     * compared.
     * @return Returns true if the two instances are equal.
     */
    virtual bool isEqual(IAcquisitionEventListener* other) = 0;

    /**
     * @brief notifyAcquisitionProgress This method is called to notify all the
     * listeners of the current acquisition progress.
     */
    virtual void notifyAcquisitionProgress() = 0;

    /**
     * @brief notifyHardwareDisconnection This method is called to notify all
     * the listeners of acquisition completion.
     */
    virtual void notifyAcquisitionCompletion() = 0;

    /**
     * @brief ~IAcquisitionEventListener Virtual destructor as per convention.
     */
    virtual ~IAcquisitionEventListener()
    {

    }

  };// end of IAcquisitionEventListener class.
} // end of spin namespace.

#endif // IACQUISITIONEVENTLISTENER_H
