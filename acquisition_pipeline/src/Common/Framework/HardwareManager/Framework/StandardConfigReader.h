#ifndef STANDARDCONFIGREADER_H
#define STANDARDCONFIGREADER_H

#include <boost/property_tree/ptree.hpp>

#include "Common/Framework/HardwareManager/Framework/ISystemConfigReader.h"

namespace pt = boost::property_tree;

namespace spin
{
  class StandardConfigReader : public ISystemConfigReader
  {
  public:
    StandardConfigReader();

    ~StandardConfigReader();

    bool readConfig(SystemConfig* config);
  private:

    bool populateDefConfig(SystemConfig*, int type=1);

    bool populateSysCalib(SystemConfig*, int type=1);

    void populateSlotOffsets(SystemConfig*, pt::ptree );

    void populateCameraConfig(SystemConfig*, pt::ptree );

    void populateZStageConfig(SystemConfig*, pt::ptree );

    void populateFocusChannels(SystemConfig*, pt::ptree );

    void populateMappingsConfig(SystemConfig*, pt::ptree );

    void generateWorkspaceJson();

  };
}

#endif // STANDARDCONFIGREADER_H
