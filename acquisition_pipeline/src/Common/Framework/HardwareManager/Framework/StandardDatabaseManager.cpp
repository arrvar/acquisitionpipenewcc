#include <QObject>
#include <QSqlQuery>
#include <QSqlError>
#include <QDir>
#include <QVariant>

#include "Common/Framework/HardwareManager/Framework/StandardDatabaseManager.h"

#include "Common/ErrorManagement/ErrorHandler.h"

#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"

namespace spin
{
  /**
   * @brief StandardDatabaseManager Default class constructor.
   */
  StandardDatabaseManager::StandardDatabaseManager()
  {

  }

  /**
   * @brief connect Connects to the slide database.
   * @return Returns true if the connection to the database is successful.
   */
  bool StandardDatabaseManager::connect(QString slideName)
  {
    // Check if database is already open.
    if (m_slideDb.isOpen() == false)
    {
      m_slideDb = QSqlDatabase::addDatabase(Constants::DATABASE_TYPE,
        "SlideDbConn");

      QString slideDbPath = FileSystemUtil::
        getPathFromWorkspacePath(slideName + "/" +
          Constants::SLIDE_DATABASE_NAME);
      m_slideDb.setDatabaseName(slideDbPath);

      // Open the database.
      if (!m_slideDb.open())
      {
        SPIN_ERROR(QObject::tr("Could not open slide database due to "
          "the following error: %1")
          .arg(m_slideDb.lastError().text()));
        return false;
      }

      // Before checking, set pragma.
      QSqlQuery pragmaQuery(m_slideDb);
      if (!pragmaQuery.exec("PRAGMA foreign_keys = ON;"))
      {
        SPIN_ERROR(QObject::tr("Error occurred while setting pragma "
          "database due to the following error: %1")
          .arg(pragmaQuery.lastError().text()));
        return false;
      }

      // Check if database already exists.
      QSqlQuery query(m_slideDb);
      if (query.exec("SELECT name FROM sqlite_master "
        "WHERE type='table' AND name='aoi'") && query.next())
      {
        // If table already exists then no need to create
        // other tables, so return
        SPIN_LOG_INFO("Slide database opened.");
        return true;
      }
      else
      {

        SPIN_LOG_FATAL(QObject::tr("Failed to open slide database: %1").
                       arg(query.lastError().text()));
        return false;
      }
    }
    return true;
  }

  /**
   * @brief disconnect Disconnect from a database.
   * @return Returns true if the disconnetion is successful.
   */
  bool StandardDatabaseManager::disconnect()
  {
    if(m_slideDb.isOpen())
    {
      QString connectionName = m_slideDb.connectionName();
      m_slideDb.close();
      m_slideDb = QSqlDatabase();
      QSqlDatabase::removeDatabase(connectionName);
    }
    return true;
  }

  /**
   * @brief getAoIZValue To retrieve focus value of the given AoI.
   * @param aoiId Id of the given AoI.
   * @return Returns the focus value of the given AoI. If not available,
   * it will return -1.
   */
  qreal StandardDatabaseManager::getAoIZValue(int aoiId)
  {
    qreal zValue = -1;
    QString selectQueryStr;
    selectQueryStr.sprintf("SELECT z_value "
                           "FROM aoi "
                           "WHERE aoi_id = %d",
                           aoiId);

    QSqlQuery selectQuery(m_slideDb);
    if (selectQuery.exec(selectQueryStr) && selectQuery.next())
    {
      zValue = selectQuery.value(0).toReal();
    }
    else
    {
      SPIN_LOG_ERROR(QObject::tr("Failed to select from aoi table due "
        "to the following error: ").
        arg(selectQuery.lastError().text()));
    }

    return zValue;
  }

  /**
   * @brief updateAoIZValue To update the focus value of the given AoI.
   * @param aoiId Id of the AoI.
   * @return Returns true on successfully executing the query else returns
   * false.
   */
  bool StandardDatabaseManager::updateAoIZValue(int aoiId, qreal zValue,
                                                qreal focusMetric)
  {
    bool retVal = true;
    QString updateQueryStr;
    updateQueryStr.sprintf("UPDATE aoi "
                           "SET z_value = %f, focus_metric = %f "
                           "WHERE aoi_id = %d",
                           zValue, focusMetric, aoiId);

    QSqlQuery updateQuery(m_slideDb);
    if(!updateQuery.exec(updateQueryStr))
    {
      SPIN_LOG_ERROR(QObject::tr("Failed to update aoi table due "
                                 "to the following error: ").
                     arg(updateQuery.lastError().text()));
      retVal = false;
    }

    return retVal;
  }

  /**
   * @brief getAoIInfoList To retrieve the list of all the AoIs for the given
   * grid.
   * @param IN_OUT_PARAM gridInfo Container to store the list of AoIs.
   * @return Returns true on successfully executing the query else returns
   * false.
   */
  bool StandardDatabaseManager::getAoIInfoList(IN_OUT_PARAM acquisition::
                                               GridInfo& gridInfo)
  {
    bool retVal = true;
    QString selectQueryStr;
    selectQueryStr.sprintf("SELECT aoi_id, aoi_name, aoi_x, aoi_y, sampling_z, "
                           "z_value, aoi_row_idx, aoi_col_idx, focus_metric, "
                           "low_powered_aoi_id, region_type, wbc_present, "
                           "background "
                           "FROM aoi "
                           "WHERE grid_id = %d "
                           "AND magnification = %d",
                           gridInfo.m_id, (int)gridInfo.m_mag);

    QSqlQuery selectQuery(m_slideDb);
    if(selectQuery.exec(selectQueryStr))
    {
      while(selectQuery.next())
      {
        acquisition::AoIInfo aoiInfo;
        aoiInfo.m_id = selectQuery.value(0).toInt();

        aoiInfo.m_name = selectQuery.value(1).toString();

        aoiInfo.m_pos.setX(selectQuery.value(2).toReal());
        aoiInfo.m_pos.setY(selectQuery.value(3).toReal());

        aoiInfo.m_samplingZ = selectQuery.value(4).toDouble();
        aoiInfo.m_zVal = selectQuery.value(5).toDouble();

        aoiInfo.m_rowIdx = selectQuery.value(6).toInt();
        aoiInfo.m_colIdx = selectQuery.value(7).toInt();

        aoiInfo.m_focusMetric = selectQuery.value(8).toReal();

        aoiInfo.m_lowPowerId = selectQuery.value(9).toInt();

        aoiInfo.m_regType = static_cast <RegionType> (selectQuery.
                                                      value(10).toInt());

        aoiInfo.m_wbcPresent = false;
        int wbcPresent = selectQuery.value(11).toInt();
        if (wbcPresent == 1)
        {
          aoiInfo.m_wbcPresent = true;
        }

        int bgState =  selectQuery.value(12).toInt();
        if(bgState == 1)
        {
          aoiInfo.m_bgState = true;
        }

        if(aoiInfo.m_lowPowerId != 0)
        {
          if(aoiInfo.m_zVal == 0)
          {
            aoiInfo.m_typicalZ = getAoIZValue(aoiInfo.m_lowPowerId);
          }
          else
          {
            aoiInfo.m_typicalZ = aoiInfo.m_zVal;
          }
        }

        aoiInfo.m_mag = gridInfo.m_mag;

        gridInfo.m_aoiList.append(aoiInfo);
      }
    }
    else
    {
      SPIN_LOG_ERROR(QObject::tr("Faield to select from aoi table due "
                                 "to the following error: ").
                     arg(selectQuery.lastError().text()));
      retVal = false;
    }

    return retVal;
  }


  /**
   * @brief getGridInfo To retrieve the structure of the given grid.
   * @param OUT_PARAM gridInfo To store the structure of the give grid.
   * @param gridName Name of the grid.
   * @return Returns on successfully executing the query else returns false.
   */
  bool StandardDatabaseManager::getGridInfo(OUT_PARAM acquisition::
                                            GridInfo& gridInfo,
                                            QString gridName)
  {
    bool retVal = true;
    QString selectQueryStr;
    selectQueryStr.sprintf("SELECT grid_id, row_count, column_count, "
                           "width_overlap, height_overlap, "
                           "start_x, start_y, end_x, end_y, "
                           "magnification, pattern, direction, best_focus_val "
                           "FROM grid_info "
                           "WHERE grid_name = '%s'",
                           gridName.toStdString().c_str());

    QSqlQuery selectQuery(m_slideDb);
    if(selectQuery.exec(selectQueryStr))
    {
      while(selectQuery.next())
      {
        gridInfo.m_id = selectQuery.value(0).toInt();

        gridInfo.m_name = gridName;

        gridInfo.m_rowCount = selectQuery.value(1).toInt();

        gridInfo.m_colCount = selectQuery.value(2).toInt();

        gridInfo.m_widthOverlap = selectQuery.value(3).toReal();

        gridInfo.m_heightOverlap = selectQuery.value(4).toReal();

        gridInfo.m_coord.setTopLeft(QPointF(selectQuery.value(5).toReal(),
                                            selectQuery.value(6).toReal()));

        gridInfo.m_coord.setBottomRight(QPointF(selectQuery.value(7).toReal(),
                                                selectQuery.value(8).toReal()));

        gridInfo.m_mag = static_cast<Magnification>(selectQuery.
                                                    value(9).toInt());

        gridInfo.m_pattern = static_cast<ScanPattern>(selectQuery.
                                                      value(10).toInt());

        gridInfo.m_direction = static_cast<ScanDirection>(selectQuery.
                                                          value(11).toInt());

        gridInfo.m_bestFocus = selectQuery.value(12).toReal();

        if(!getAoIInfoList(gridInfo))
        {
          retVal = false;
          break;
        }
      }
    }
    else
    {
      SPIN_ERROR(QObject::tr("Failed to select from grid_info table due "
                             "to the following error: %1").
                 arg(selectQuery.lastError().text()));
      retVal = false;
    }

    return retVal;
  }

  /**
   * @brief getLowPowerMag To retrieve the magnification value of the given
   * low power AoI id.
   * @param lowPowerId Id of the low power AoI.
   * @return Returns the magnification value of the low power AoI.
   */
  Magnification StandardDatabaseManager::getLowPowerMag(int lowPowerId)
  {
    Magnification lowPowerMag;
    lowPowerMag = MAG_UNKNOWN;

    QString selectQueryStr;
    selectQueryStr.sprintf("SELECT magnification "
                           "FROM aoi "
                           "WHERE aoi_id = %d",
                           lowPowerId);

    QSqlQuery selectQuery(m_slideDb);
    if(selectQuery.exec(selectQueryStr))
    {
      while(selectQuery.next())
      {
        lowPowerMag = static_cast<Magnification>(selectQuery.value(0).toInt());
      }
    }
    else
    {
      SPIN_ERROR(QObject::tr("Failed to select from aoi table due "
                             "to the following error: %1").
                 arg(selectQuery.lastError().text()));
    }

    return lowPowerMag;
  }

  /**
   * @brief updateAoIFocusMetric To update the focus metric values for the
   * given list of AoIs
   * @param aoiIdList List of AoI Ids.
   * @param focusMetricList List of focus metric values.
   * @param colorMetricList List of color metric values.
   * @param stackPresentList List of status of presence of stack.
   * @param bestIdxList List of best indices of AoI in the stack.
   * @param bestZList List of best z values of AOI in the stack
   * @return Returns true on successfully executing the query else returns
   * false.
   */
  bool StandardDatabaseManager::updateAoIFocusMetric(const QVariantList
                                                     &aoiIdList,
                                                     const QVariantList
                                                     &focusMetricList,
                                                     const QVariantList
                                                     &colorMetricList,
                                                     const QVariantList
                                                     &stackPresentList,
                                                     const QVariantList
                                                     &bestIdxList,
                                                     const QVariantList
                                                     &bestZList)
  {
    bool retVal = true;
    QString updateQueryStr;
    updateQueryStr.sprintf("UPDATE aoi "
                           "SET focus_metric = ?, color_metric = ?, "
                           "stack_present = ?, best_idx = ?, best_z = ? "
                           "WHERE aoi_id = ?");

    QSqlQuery updateQuery(m_slideDb);
    updateQuery.exec("begin exclusive transaction;");

    updateQuery.prepare(updateQueryStr);
    updateQuery.addBindValue(focusMetricList);
    updateQuery.addBindValue(colorMetricList);
    updateQuery.addBindValue(stackPresentList);
    updateQuery.addBindValue(bestIdxList);
    updateQuery.addBindValue(bestZList);
    updateQuery.addBindValue(aoiIdList);
    if(!updateQuery.execBatch())
    {
      SPIN_LOG_ERROR(QObject::tr("Failed to update aoi table due "
                                 "to the following error: ").
                     arg(updateQuery.lastError().text()));
      retVal = false;
    }

    updateQuery.exec("commit;");

    return retVal;
  }

  /**
   * @brief updateAoIFocusInfo To update the Z values and the focus metric
   * values of all the AoIs in batch mode.
   * @param aoiIdList List of AoI Ids.
   * @param aoiZValueList List of Z values.
   * @param aoiFocusMetricList List of focus metric values.
   * @return Returns true on successfully executing the query else returns
   * false.
   */
  bool StandardDatabaseManager::
    updateAoIFocusInfo(const QVariantList& aoiIdList,
                       const QVariantList& aoiZValueList,
                       const QVariantList& aoiFocusMetricList)
  {
    bool retVal = true;
    QString updateQueryStr;
    updateQueryStr.sprintf("UPDATE aoi "
                           "SET z_value = ?, focus_metric = ? "
                           "WHERE aoi_id = ?");

    QSqlQuery updateQuery(m_slideDb);
    updateQuery.exec("begin exclusive transaction;");

    updateQuery.prepare(updateQueryStr);
    updateQuery.addBindValue(aoiZValueList);
    updateQuery.addBindValue(aoiFocusMetricList);
    updateQuery.addBindValue(aoiIdList);
    if (!updateQuery.execBatch())
    {
      SPIN_LOG_ERROR(QObject::tr("Failed to update aoi table due "
                                 "to the following error: ").
                     arg(updateQuery.lastError().text()));
      retVal = false;
    }

    updateQuery.exec("commit;");

    return retVal;
  }

  /**
   * @brief ~StandardDatabaseManager Default class destructor.
   */
  StandardDatabaseManager::~StandardDatabaseManager()
  {
    disconnect();
  }
}
