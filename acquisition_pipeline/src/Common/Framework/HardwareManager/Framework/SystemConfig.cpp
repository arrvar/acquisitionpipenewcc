#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"

namespace spin
{
  SystemConfig* SystemConfig::s_instance = 0;

  /**
   * @brief SystemConfig Default class constructor.
   */
  SystemConfig::SystemConfig()
  {

  }

  /**
   * @brief addConfig Adds configuration key and value pair. The value is
   * defined in one of the configuration files and the key is defined as a
   * unique string.
   * @param key A unique string that identifies the attribute value.
   * @param value Attribute value retrieved from the system configuration.
   */
  void SystemConfig::addConfig(QString key, QVariant* value)
  {
    //prepare the configuration map
    if(value)
    {
      if(m_configMap.contains(key))
      {
        // Remove an old value if present.
        QVariant* oldValue = m_configMap.take(key);
        if(oldValue)
        {
          delete oldValue;
        }
      }
      m_configMap.insert(key, value);
    }
  }

  /**
   * @brief config Gives the configuration value identified by the given
   * unique key.
   * @param key Key identifying the configuration value.
   * @return The configuration value associated with given key
   */
  const QVariant* SystemConfig::config(QString key)
  {
      QVariant* value = m_configMap.value(key, 0);
      return value;
  }

  /**
   * @brief intConfig Gives the Integer configuration value identified by the
   * given unique key.
   * @param key Key identifying the configuration value.
   * @return Integer configuration value associated with given key
   */
  qint32 SystemConfig::intConfig(QString key)
  {
      const QVariant* value = this->config(key);
      qint32 intVal = 0;
      if(value)
      {
          bool ok = false;
          intVal = value->toInt(&ok);
      }
      return intVal;
  }

  /**
   * @brief realConfig Gives the real number configuration value identified by
   * the given unique key.
   * @param key Key identifying the configuration value.
   * @return The real number configuration value associated with given key
   */
  qreal SystemConfig::realConfig(QString key)
  {
      const QVariant* value = this->config( key );
      qreal realVal = 0.0f;
      if(value)
      {
          bool ok = false;
          realVal = value->toReal(&ok);
      }
      return realVal;
  }

  /**
   * @brief stringConfig Gives string configuration value identified by the
   * given unique key.
   * @param key Key identifying the configuration value.
   * @return The string value associated with given key
   */
  QString SystemConfig::stringConfig(QString key)
  {
      const QVariant* value = this->config(key);
      if(value)
      {
          return value->toString();
      }
      return QString();
  }

  /**
   * @brief addSlotParam To add the parameters of a slot to the system
   * configuration.
   * @param slotNum Slot number.
   * @param slotParam Parameters of the slot.
   */
  void SystemConfig::addSlotParam(int slotNum,
                                  const acquisition::SlotParams& slotParam)
  {
    m_slotParamHash.insert(slotNum, slotParam);
  }

  /**
   * @brief getSlotParam To retreive the parameters of a given slot.
   * @param slotNum Slot number.
   * @param OUT_PARAM slotParam Container to store the parameters of the
   * slot.
   * @return Returns true if the slot parameters are available for the given
   * slot number else returns false.
   */
  bool SystemConfig::getSlotParam(int slotNum,
                                  OUT_PARAM acquisition::
                                  SlotParams& slotParam)
  {
    if(m_slotParamHash.contains(slotNum))
    {
      slotParam = m_slotParamHash.value(slotNum);
      return true;
    }
    return false;
  }

  /**
   * @brief get Get the static instance (which is assumed to be singleton).
   * @return instance of system config.
   */
  SystemConfig* SystemConfig::get()
  {
    //create main instance of application
    if(!s_instance)
    {
      s_instance = new SystemConfig();
    }
    //return singleton instance
    return s_instance;
  }

  /**
   * @brief ~SystemConfig Destructor
   */
  SystemConfig::~SystemConfig()
  {
    if(m_configMap.isEmpty() == false)
    {
      qDeleteAll(m_configMap);
    }
  }
}
