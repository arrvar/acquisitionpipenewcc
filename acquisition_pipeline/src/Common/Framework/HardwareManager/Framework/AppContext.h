#ifndef HMAPPCONTEXT_H
#define HMAPPCONTEXT_H

#include <memory>
#include <string>

#include "Common/Framework/HardwareManager/Framework/HardwareManager.h"
#include "Common/Framework/HardwareManager/Framework/IDatabaseManager.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"
#include "Common/Framework/Logger/AbstractLogger.h"
#include "Common/Framework/HardwareManager/Utils/Common.h"
#include "Common/Framework/HardwareManager/Utils/SharedImageBuffer.h"
#include "Common/Framework/HardwareManager/Utils/SharedProcessBuffer.h"

namespace spin
{
  /**
  * @brief The AppContext class represents the application context for
  * a SPIN GUI application using which global resource managers, application
  * phases and global attributes can be accessed from anywhere in the
  * application.
  */
  class AppContext
  {
    public:
      /**
       * @brief ~AppContext Class destructor, destorys objects owned by the
       * class.
       */
      ~AppContext();

    /**
     * @brief init To initialise the global resources of the application.
     * @param dbMgr Instance of the datbase manager.
     * @param wsMgr Instance of the workspace manager.
     * @param config Instance of the system configuration.
     * @return Returns true on successful initialisation.
     */
    static bool init(IDatabaseManager* dbMgr,
                     HardwareManager* hwMgr,
                     SystemConfig* config);

      /**
       * @brief get To retrieve the singleton instance of the class.
       * @return Instance of the class.
       */
      static AppContext& get();

      /**
       * @brief setLogger To set the application logger.
       * @param logger Instance of the logger.
       */
      void setLogger(spin::AbstractLogger* logger)
      {
        m_logger.reset(logger);
      }

      /**
       * @brief setSharedWriteBuffer To set the application's shared write
       * buffer.
       * @param sharedWriteBuffer Instance of the shared write buffer.
       */
      void setSharedWriteBuffer(spin::SharedImageBuffer* sharedWriteBuffer)
      {
        m_sharedWriteBuffer.reset(sharedWriteBuffer);
      }

      /**
       * @brief setSharedProcessBuffer To set the application's shared process
       * buffer.
       * @param sharedProcessBuffer Instance of the shared write buffer.
       */
      void setSharedProcessBuffer(spin::SharedProcessBuffer*
                                  sharedProcessBuffer)
      {
        m_sharedProcessBuffer.reset(sharedProcessBuffer);
      }

      /**
       * @brief logger To retrieve the application logger.
       * @return Returns an instance of the application logger.
       */
      std::shared_ptr<spin::AbstractLogger> logger()
      {
        return m_logger;
      }

      /**
       * @brief databaseManager To retrieve the application database
       * manager.
       * @return Returns an instance of the application database manager.
       */
      std::shared_ptr<spin::IDatabaseManager> databaseManager()
      {
        return m_databaseManager;
      }

      /**
       * @brief hardwareManager To retrieve the application's hardware
       * manager.
       * @return Returns an instance of the application hardware manager.
       */
      std::shared_ptr<spin::HardwareManager> hardwareManager()
      {
        return m_hardwareManager;
      }

      /**
       * @brief config To retrieve the system level configuration.
       * @return Returns an instance of the application's configuration.
       */
      std::shared_ptr<spin::SystemConfig> config()
      {
        return m_systemConfig;
      }

      /**
       * @brief sharedWriteBuffer To retrieve the application's shared image
       * buffer for writing.
       * @return Returns an instance of the shared write buffer.
       */
      std::shared_ptr<spin::SharedImageBuffer> sharedWriteBuffer()
      {
        return m_sharedWriteBuffer;
      }

      /**
       * @brief sharedProcessBuffer To retrieve the application's shared image
       * buffer for processing.
       * @return Returns an instance of the shared process buffer.
       */
      std::shared_ptr<spin::SharedProcessBuffer> sharedProcessBuffer()
      {
        return m_sharedProcessBuffer;
      }

      /**
       * @brief setInstallationPath To store the application installation
       * path.
       * @param installPath Absolute path of the application.
       */
      void setInstallationPath(std::string installPath)
      {
          m_installationPath = installPath;
      }

      /**
       * @brief installationPath To retrieve the application installation
       * path.
       * @return Absolute path of the folder where the application is
       * installed.
       */
      std::string installationPath() const
      {
          return m_installationPath;
      }

    private:
      /**
       * @brief s_isInitialized Flag to store the intialization status
       * of the singleton class.
       */
      static bool s_isInitialized;

      /**
       * @brief m_logger To store an instance of the application logger.
       */
      std::shared_ptr<spin::AbstractLogger> m_logger;

      /**
       * @brief m_databaseManager To store an instance of the application's
       * database manager.
       */
      std::shared_ptr<spin::IDatabaseManager> m_databaseManager;

      /**
       * @brief m_hardwareManager To store an instance of the application's
       * hardware manager.
       */
      std::shared_ptr<spin::HardwareManager> m_hardwareManager;

      /**
       * @brief m_systemConfig To store an instance of the system's
       * configuration.
       */
      std::shared_ptr<spin::SystemConfig> m_systemConfig;

      /**
       * @brief m_sharedWriteBuffer To store an instance of the application's
       * shared image buffer for writing.
       */
      std::shared_ptr<spin::SharedImageBuffer> m_sharedWriteBuffer;

      /**
       * @brief m_sharedProcessBuffer To store an instance of the application's
       * shared image buffer for processing.
       */
      std::shared_ptr<spin::SharedProcessBuffer> m_sharedProcessBuffer;

      /**
       * @brief m_installationPath To store the absolute path of the folder
       * where the application is installed.
       */
      std::string m_installationPath;

    private:
      /**
       * @brief AppContext Default class constructor, declared private to
       * prevent unauthorized instantiation.
       */
      AppContext()
      {

      }

      DISALLOW_COPY_AND_ASSIGNMENT(AppContext)

  };// end of AppContext class.
} // end of spin namespace.

/**
* @macro LOGGER Convenience macro for accessing application logger.
*/
#define SPIN_LOGGER()  \
    spin::AppContext::get().logger()

/**
 * @macro DB_MANAGER macro to access database manager
 */
#define DB_MANAGER()  \
    spin::AppContext::get().databaseManager()

/**
 * @macro CONFIG macro to access configuration object
 */
#define CONFIG()  \
    spin::AppContext::get().config()

/**
 * @macro HW_MANAGER macro to access hardware manager
 */
#define HW_MANAGER()  \
    spin::AppContext::get().hardwareManager()

/**
 * @macro SHARED_WRITE_BUFFER macro to access the shared write buffer
 */
#define SHARED_WRITE_BUFFER()  \
    spin::AppContext::get().sharedWriteBuffer()

/**
 * @macro SHARED_PROCESS_BUFFER macro to access the shared process buffer
 */
#define SHARED_PROCESS_BUFFER()  \
    spin::AppContext::get().sharedProcessBuffer()


/**
* @macro LOG Convenience macros for logging messages.
*/
#define SPIN_LOG_DEBUG(msg)  \
    SPIN_LOGGER()->debug(msg, FUNCTION_NAME)

#define SPIN_LOG_INFO(msg)  \
    SPIN_LOGGER()->info(msg, FUNCTION_NAME);

#define SPIN_LOG_WARNING(msg)  \
    SPIN_LOGGER()->warning(msg, FUNCTION_NAME);

#define SPIN_LOG_ERROR(msg)  \
    SPIN_LOGGER()->error(msg, FUNCTION_NAME);

#define SPIN_LOG_FATAL(msg)  \
    SPIN_LOGGER()->fatal(msg, FUNCTION_NAME);

namespace spin
{
  /**
  * @brief The MethodEntryExitLog class is responsible for logging the entry and
  * exit in method.
  */
  class MethodEntryExitLog
  {
    public:
      /**
      * @brief MethodEntryExitLog Class constructor, intializes objects owned by
      * the class.
      * @param context Context in which the message arises.
      */
      MethodEntryExitLog(QString context) :
        m_context(context)
      {
        SPIN_LOGGER()->debug("Entered", context);
      }

      /**
      * @brief ~MethodEntryExitLog Class destructor, destroys objects owned by
      * the class.
      */
      ~MethodEntryExitLog()
      {
        SPIN_LOGGER()->debug("Exited", m_context);
      }

    private:
      /**
      * @brief m_context Stores the context in which the message arises.
      */
      QString m_context;

  };// end of MethodEntryExitLog class.
} // end of spin namespace.

  /**
  * @brief Convenience macro for method logging.
  */
#define SPIN_LOG_METHOD()  \
    spin::MethodEntryExitLog obj(FUNCTION_NAME)

#endif // APPCONTEXT_H
