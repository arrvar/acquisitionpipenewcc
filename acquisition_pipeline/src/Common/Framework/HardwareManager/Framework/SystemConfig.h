#ifndef HMSYSTEMCONFIG_H
#define HMSYSTEMCONFIG_H

#include <QHash>
#include <QString>
#include <QVariant>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"
#include "Common/Framework/HardwareManager/Utils/Common.h"

namespace spin
{
  /**
   * @brief The SystemConfig class is a runtime representation of the
   * system configuration values.
   */
  class SystemConfig
  {
  public:
    /**
     * @brief addConfig Adds configuration key and value pair. The value is
     * defined in one of the configuration files and the key is defined as a
     * unique string.
     * @param key A unique string that identifies the attribute value.
     * @param value Attribute value retrieved from the system configuration.
     */
    void addConfig(QString key, QVariant* value);

    /**
     * @brief config Gives the configuration value identified by the given
     * unique key.
     * @param key Key identifying the configuration value.
     * @return The configuration value associated with given key
     */
    const QVariant* config(QString key);

    /**
     * @brief intConfig Gives the Integer configuration value identified by the
     * given unique key.
     * @param key Key identifying the configuration value.
     * @return Integer configuration value associated with given key
     */
    qint32 intConfig(QString key);

    /**
     * @brief realConfig Gives the real number configuration value identified by
     * the given unique key.
     * @param key Key identifying the configuration value.
     * @return The real number configuration value associated with given key
     */
    qreal realConfig(QString key);

    /**
     * @brief stringConfig Gives string configuration value identified by the
     * given unique key.
     * @param key Key identifying the configuration value.
     * @return The string value associated with given key
     */
    QString stringConfig(QString key);

    /**
     * @brief addSlotParam To add the parameters of a slot to the system
     * configuration.
     * @param slotNum Slot number.
     * @param slotParam Parameters of the slot.
     */
    void addSlotParam(int slotNum, const acquisition::SlotParams& slotParam);

    /**
     * @brief getSlotParam To retreive the parameters of a given slot.
     * @param slotNum Slot number.
     * @param OUT_PARAM slotParam Container to store the parameters of the
     * slot.
     * @return Returns true if the slot parameters are available for the given
     * slot number else returns false.
     */
    bool getSlotParam(int slotNum,
                      OUT_PARAM acquisition::SlotParams& slotParam);

    /**
     * @brief ~SystemConfig Destructor
     */
    ~SystemConfig();

    /**
     * @brief get Get the static instance (which is assumed to be singleton).
     * @return instance of system config.
     */
    static SystemConfig* get();

  protected:
    /**
     * @brief SystemConfig protected constructor to prevent direct
     * instantiation
     */
    SystemConfig();

  private:
    /**
     * @brief s_instance Static instance to make singleton object
     */
    static SystemConfig* s_instance;

    /**
     * @brief m_configMap Stores attribute value against a unique key.
     */
    QHash <QString, QVariant* > m_configMap;

    /**
     * @brief m_slotParamHash To store the parameters of a given slot no.
     */
    QHash <int, acquisition::SlotParams> m_slotParamHash;

    /**
     * @brief m_workspacePath Stores the path of the directory where all the
     * workspaces are present.
     */
    QString m_workspacePath;

    /**
     * @brief m_appVersion Application version
     */
    QString m_appVersion;

    // No copying this object!
    DISALLOW_COPY_AND_ASSIGNMENT(SystemConfig)

  };// end of SystemConfig class.
} // end of spin namespace.

#endif // SYSTEMCONFIG_H
