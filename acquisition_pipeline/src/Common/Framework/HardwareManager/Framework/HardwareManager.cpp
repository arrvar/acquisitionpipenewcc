#include <QDebug>
#include <QElapsedTimer>
#include <QString>

#include <MMCore/MMCore.h>

#include "Common/Framework/HardwareManager/Framework/HardwareManager.h"

#include "Common/ErrorManagement/ErrorHandler.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Framework/IHardwareEventListener.h"

#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"

namespace spin
{
  /**
   * @brief HardwareManager Default class destructor.
   */
  HardwareManager::HardwareManager() :
    m_isConnected(false),
    m_xyTrigger(false),
    m_zTrigger(false)
  {
      m_mmcore.reset(new CMMCore);

      m_adapterPath = FileSystemUtil::
          getPathFromInstallationPath("libs").toStdString();

      initAdapterList();

      initComPortList();
  }

  /**
   * @brief initAdapterList To initialize the adapter list.
   */
  void HardwareManager::initAdapterList()
  {
    try
    {
      std::vector <std::string> adapterPaths;
      adapterPaths.push_back(m_adapterPath);
      m_mmcore->setDeviceAdapterSearchPaths(adapterPaths);

      m_devAdapterList = m_mmcore->getDeviceAdapterNames();
      Q_FOREACH(std::string adapterName, m_devAdapterList)
      {
        std::vector <std::string> devList = m_mmcore->
            getAvailableDevices(adapterName.c_str());
        m_libDevMap[adapterName] = devList;
          SPIN_LOG_INFO(QObject::tr("Adapter Name: %1").
                        arg(adapterName.c_str()));
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief initComPortList To initialize the list of available COM ports.
   */
  void HardwareManager::initComPortList()
  {
    try
    {
      m_comPortList = m_libDevMap["SerialManager"];
      Q_FOREACH(std::string comPortname, m_comPortList)
      {
        SPIN_LOG_INFO(QObject::tr("COM Port Name: %1").
                      arg(comPortname.c_str()));
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief initDevice To initialize a device with its properties
   * @param devName Name of the device.
   * @param adapterName Name of the adapter of the device.
   * @param propList List of properties of the device
   * the need to be initialized.
   * @return Returns true if the device has been successfully initialzed
   * else returns false.
   */
  bool HardwareManager::initDevice(std::string devName,
                                   std::string adapterName,
                                   std::vector<DeviceProperty> propList)
  {
    try
    {
      // First load the device.
      m_mmcore->loadDevice(devName.c_str(), adapterName.c_str(),
                           devName.c_str());

      // Set all the available properties of the device.
      for(int idx = 0; idx < propList.size(); idx++)
      {
        DeviceProperty devProp = propList.at(idx);
        m_mmcore->setProperty(devName.c_str(), devProp.m_propName.c_str(),
                              devProp.m_propValue.c_str());
      }

      // Initialize the device.
      m_mmcore->initializeDevice(devName.c_str());

      return true;
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
      return false;
    }
  }

  /**
   * @brief initStage To initiaize the translation stage devices.
   * @return Returns true if all the translation devices have been
   * successfully initialize else returns false.
   */
  bool HardwareManager::initStage()
  {
    bool retVal = false;
    try
    {
      std::string stageLibName =
          CONFIG()->stringConfig(ConfigKeys::STAGE_LIB_NAME).toStdString();

      std::vector <DeviceProperty> propList;
      propList.push_back(DeviceProperty("AnswerTimeout", "1000"));
      propList.push_back(DeviceProperty("BaudRate", "230400"));
      propList.push_back(DeviceProperty("DelayBetweenCharsMs", "0"));
      propList.push_back(DeviceProperty("Handshaking", "Off"));
      propList.push_back(DeviceProperty("Parity", "None"));
      propList.push_back(DeviceProperty("StopBits", "1"));

      for(int idx = 0; idx < m_comPortList.size(); idx++)
      {
        std::string comPortName = m_comPortList.at(idx);
        SPIN_LOG_INFO(QObject::tr("Trying COM Port: %1").
                      arg(comPortName.c_str()));
        if(initDevice(comPortName, "SerialManager", propList))
        {
          // Clear and set property for the next device.
          propList.clear();
          propList.push_back(DeviceProperty("Port", comPortName));
          initDevice("Scope", stageLibName, propList);

          // No properties required to be set for the next devices.
          propList.clear();
          initDevice("XYStage", stageLibName, propList);
          initDevice("ZStage", stageLibName, propList);
          initDevice("XYZCom", stageLibName, propList);
          initDevice("WhiteLamp", stageLibName, propList);
          initDevice("JoyStick", stageLibName, propList);
          initDevice("ObjectiveLens", stageLibName, propList);

          m_mmcore->setXYStageDevice("XYStage");
          m_mmcore->setFocusDevice("ZStage");

          retVal = true;
          break;
        }
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
      retVal = false;
    }

    return retVal;
  }

  /**
   * @brief setStageProps To set the properties of the translation stage.
   * @return Returns true if the properties of the translation stage have
   * been set successfully else returns false.
   */
  bool HardwareManager::setStageProps()
  {
    try
    {
      m_xyStageDev = m_mmcore->getXYStageDevice();
      m_zStageDev = m_mmcore->getFocusDevice();

      if(m_mmcore->getPosition(m_zStageDev.c_str()) == 0)
      {
        home();
      }

      std::string devName = "XYZCom";
      std::string propName = "EnableXYZ";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), "ON");

      double xStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::STAGE_STEP_SIZE_X);
      double yStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::STAGE_STEP_SIZE_Y);
      double zStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::STAGE_STEP_SIZE_Z);

      double xMicrostepping = CONFIG()->realConfig(ConfigKeys::STAGE_MICROSTEPPING_X);
      double yMicrostepping = CONFIG()->realConfig(ConfigKeys::STAGE_MICROSTEPPING_Y);
      double zMicrostepping = CONFIG()->realConfig(ConfigKeys::STAGE_MICROSTEPPING_Z);
      double turretMicrostepping = CONFIG()->realConfig(ConfigKeys::STAGE_MICROSTEPPING_TURRET);

      double xDriverMode = CONFIG()->realConfig(ConfigKeys::X_DRIVER_MODE);
      double yDriverMode = CONFIG()->realConfig(ConfigKeys::Y_DRIVER_MODE);
      double zDriverMode = CONFIG()->realConfig(ConfigKeys::Z_DRIVER_MODE);
      double turretDriverMode = CONFIG()->realConfig(ConfigKeys::TURRET_DRIVER_MODE);

      double xUpperLimitInMicrons = CONFIG()->realConfig(ConfigKeys::X_UPPER_LIMIT);
      double yUpperLimitInMicrons = CONFIG()->realConfig(ConfigKeys::Y_UPPER_LIMIT);
      double zUpperLimitInMicrons = CONFIG()->realConfig(ConfigKeys::Z_UPPER_LIMIT);

      double xyHoldCurrent = CONFIG()->realConfig(ConfigKeys::XY_DRIVER_HOLD_CURRENT);
      double zHoldCurrent = CONFIG()->realConfig(ConfigKeys::Z_DRIVER_HOLD_CURRENT);
      double turretHoldCurrent = CONFIG()->realConfig(ConfigKeys::TURRET_DRIVER_HOLD_CURRENT);
      double xyRunCurrent = CONFIG()->realConfig(ConfigKeys::XY_DRIVER_RUN_CURRENT);
      double zRunCurrent = CONFIG()->realConfig(ConfigKeys::Z_DRIVER_RUN_CURRENT);
      double turretRunCurrent = CONFIG()->realConfig(ConfigKeys::TURRET_DRIVER_RUN_CURRENT);

      double xUpperLimitInSteps = xUpperLimitInMicrons / (xStepSizeInMicrons /
                                                          xMicrostepping);
      double yUpperLimitInSteps = yUpperLimitInMicrons / (yStepSizeInMicrons /
                                                          yMicrostepping);
      double zUpperLimitInSteps = zUpperLimitInMicrons / (zStepSizeInMicrons /
                                                          zMicrostepping);

//      SPIN_LOG_INFO(QObject::tr("X Step Size in microns: %1").
//                    arg(xStepSizeInMicrons));
//      SPIN_LOG_INFO(QObject::tr("Y Step Size in microns: %1").
//                    arg(yStepSizeInMicrons));
//      SPIN_LOG_INFO(QObject::tr("Z Step Size in microns: %1").
//                    arg(zStepSizeInMicrons));

//      SPIN_LOG_INFO(QObject::tr("X Microstepping: %1").arg(xMicrostepping));
//      SPIN_LOG_INFO(QObject::tr("Y Microstepping: %1").arg(yMicrostepping));
//      SPIN_LOG_INFO(QObject::tr("Z Microstepping: %1").arg(zMicrostepping));

//      SPIN_LOG_INFO(QObject::tr("X Upper Limit in microns: %1").
//                    arg(xUpperLimitInMicrons));
//      SPIN_LOG_INFO(QObject::tr("Y Upper Limit in microns: %1").
//                    arg(yUpperLimitInMicrons));
//      SPIN_LOG_INFO(QObject::tr("Z Upper Limit in microns: %1").
//                    arg(zUpperLimitInMicrons));

//      SPIN_LOG_INFO("Calculated limits:-----");
//      SPIN_LOG_INFO(QObject::tr("X Upper Limit in steps: %1").
//                    arg(xUpperLimitInSteps));
//      SPIN_LOG_INFO(QObject::tr("Y Upper Limit in steps: %1").
//                    arg(yUpperLimitInSteps));
//      SPIN_LOG_INFO(QObject::tr("Z Upper Limit in steps: %1").
//                    arg(zUpperLimitInSteps));

//      SPIN_LOG_INFO("Set limits:-----");
      QString propVal;
      propName = "SetLimitX";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(),
                            xUpperLimitInSteps);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("X Upper Limit in steps: %1").arg(propVal));

      propName = "SetLimitY";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(),
                            yUpperLimitInSteps);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("Y Upper Limit in steps: %1").arg(propVal));

      propName = "SetLimitZ";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(),
                            zUpperLimitInSteps);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("Z Upper Limit in steps: %1").arg(propVal));

      propName = "EnableSetLimitXYZ";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), "ON");

      propName = "MicroStepX";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), xMicrostepping);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("X Upper Microstepping: %1").arg(propVal));

      propName = "MicroStepY";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), yMicrostepping);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("Y Upper Microstepping: %1").arg(propVal));

      propName = "MicroStepZ";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), zMicrostepping);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("Z Upper Microstepping: %1").arg(propVal));

      propName = "MicroStepTurret";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), turretMicrostepping);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("Turret Upper Microstepping: %1").arg(propVal));

      propName = "XYRunCurrent";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), xyRunCurrent);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
      //      SPIN_LOG_INFO(QObject::tr("XY Run Curent: %1").arg(propVal)

      propName = "ZRunCurrent";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), zRunCurrent);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
      //      SPIN_LOG_INFO(QObject::tr("Z Run Curent: %1").arg(propVal)

      propName = "TurretRunCurrent";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), turretRunCurrent);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
      //      SPIN_LOG_INFO(QObject::tr("Turret Run Curent: %1").arg(propVal)

      propName = "XYHoldCurrent";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), xyHoldCurrent);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
      //      SPIN_LOG_INFO(QObject::tr("XY Hold Curent: %1").arg(propVal)

      propName = "ZHoldCurrent";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), zHoldCurrent);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
      //      SPIN_LOG_INFO(QObject::tr("Z Hold Curent: %1").arg(propVal)

      propName = "TurretHoldCurrent";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), turretHoldCurrent);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
      //      SPIN_LOG_INFO(QObject::tr("Turret Hold Curent: %1").arg(propVal)

      propName = "XStageMode";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), xDriverMode);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("Turret Upper Microstepping: %1").arg(propVal));

      propName = "YStageMode";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), yDriverMode);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("Turret Upper Microstepping: %1").arg(propVal));

      propName = "ZStageMode";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), zDriverMode);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("Turret Upper Microstepping: %1").arg(propVal));

      propName = "TurretMode";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), turretDriverMode);
      propVal = QString::fromStdString(m_mmcore->getProperty(devName.c_str(),
                                                             propName.c_str()));
//      SPIN_LOG_INFO(QObject::tr("Turret Upper Microstepping: %1").arg(propVal));

      propName = "EnableCurrentSettingXY";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), "ON");

      propName = "EnableCurrentSettingZ";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), "ON");

      propName = "EnableCurrentSettingTurret";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), "ON");

      propName = "EnableMicroStepXYZ";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), "ON");

      propName = "BacklashZ";
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(), 0.0);

      m_mmcore->setProperty(m_xyStageDev.c_str(), "SpeedXY",
                            CONFIG()->realConfig(ConfigKeys::
                                                 MAX_XY_SPEED));
      double zStageSpeed = CONFIG()->realConfig(spin::ConfigKeys::
                                                MAX_Z_STAGE_SPEED);
      m_mmcore->setProperty(m_zStageDev.c_str(), "SpeedZ", zStageSpeed);
      m_mmcore->setProperty("WhiteLamp", "SetLedBrightness", 0.0);
      m_mmcore->setProperty(m_xyStageDev.c_str(), "EnableTrigger", "OFF");
      m_mmcore->setProperty(m_zStageDev.c_str(), "EnableTrigger", "OFF");

      removeFilter();

      return true;
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
      return false;
    }
  }

  /**
   * @brief enableZBacklash To set the backlash of the Z stage.
   * @return Returns true if the properties of the Z stage have
   * been set successfully else returns false.
   */
  bool HardwareManager::enableZBacklash()
  {
    try
    {
      m_zStageDev = m_mmcore->getFocusDevice();
      std::string propName = "BacklashZ";
      qreal backlash = CONFIG()->realConfig(ConfigKeys::BACKLASH_Z);
      if(backlash != 0)
      {
        m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(), backlash);
      }
      return true;
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
      return false;
    }

  }
  /**
   * @brief disableZBacklash To set the backlash of the Z stage.
   * @return Returns true if the properties of the Z stage have
   * been set successfully else returns false.
   */
  bool HardwareManager::disableZBacklash()
  {
    try
    {
      m_zStageDev = m_mmcore->getFocusDevice();
      std::string propName = "BacklashZ";
      qreal b = CONFIG()->realConfig(ConfigKeys::BACKLASH_Z);
      if(b!=0)
        m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(), 0.0);
      return true;
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
      return false;
    }
  }

  /**
   * @brief initCamera To initialize the camera device.
   * @return Returns true if the camera device has been successfully
   * initialized else returns false.
   */
  bool HardwareManager::initCamera()
  {
    bool retVal = false;
    try
    {
      std::string adapterName = "MatrixVision";
      // Empty property list.
      std::vector <DeviceProperty> propList;

      std::vector <std::string> devNameList = m_libDevMap[adapterName.c_str()];
      for(int idx = 0; idx < devNameList.size(); idx++)
      {
        std::string devName = devNameList.at(idx);
        if(QString::fromStdString(devName).contains("FF"))
        {
          SPIN_LOG_INFO(QObject::tr("Loading device: %1").arg(devName.c_str()));

          if(initDevice(devName, adapterName, propList))
          {
            m_mmcore->setCameraDevice(devName.c_str());
            retVal = true;
            break;
          }
        }
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
      retVal = false;
    }

    return retVal;
  }

  /**
   * @brief setCamProps To set the properties of the camera.
   * @param magVal Current magnification settings.
   * @return Returns true if the properties of the camera have been
   * successfully set else returns false.
   */
  bool HardwareManager::setCamProps(Magnification magVal)
  {
    try
    {
      m_camDev = m_mmcore->getCameraDevice();

      stopContinuousMode();

      std::string propName = "Camera/GenICam/ImageFormatControl/PixelFormat";
//      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "BGRA8Packed");
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "BayerRG8");

      qreal expVal = Utilities::getCamExpValue(magVal);
      m_mmcore->setExposure(expVal);

      colorBalanceOff();

      return true;
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
      return false;
    }
  }

  /**
   * @brief addListener Adds a listener which will be notified about the
   * hardware related events that occur in this class's context.
   * @param listener Listener which is to be registered for observation of
   * hardware events.
   */
  void HardwareManager::addListener(spin::IHardwareEventListener* listener)
  {
    // If the listener reference is valid and it is not a duplicate then
    // add it to the hardware listeners list.
    if(listener)
    {
      bool duplicate = false;
      for(int i = 0; i < m_listeners.size(); i++)
      {
        if(listener->isEqual(m_listeners.at(i)))
        {
          duplicate = true;
          break;
        }
      }

      if(!duplicate)
      {
        m_listeners.append(listener);
      }
    }
  }

  /**
   * @brief removeListener Unregisters the given listener from observer list.
   * @param listener Instance of listener to be unregistered.
   */
  void HardwareManager::removeListener(spin::IHardwareEventListener* listener)
  {
    // Remove the listener from the hardware listeners list.
    for(int i = 0; i < m_listeners.size(); i++)
    {
      if(listener->isEqual(m_listeners.at(i)))
      {
        m_listeners.removeAt(i);
        break;
      }
    }
  }

  /**
   * @brief snapImage To capture an image from the camera.
   * @return Returns a raw pointer instance of the snapped image.
   */
  void* HardwareManager::snapImage()
  {
    void* imgPtr = NULL;

    try
    {
      m_mmcore->snapImage();
      imgPtr = m_mmcore->getImage();
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }

    return imgPtr;
  }

  /**
   * @brief setCameraExposure To set the exposure value of the camera.
   * @param exposureValue Exposure value of the camera in milliseconds.
   */
  void HardwareManager::setCameraExposure(double exposureValue)
  {
    try
    {
      m_mmcore->setExposure(exposureValue);
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief getCameraExposure To get the current exposure value of the camera.
   * @return Returns the current exposure value of the camera in milliseconds.
   */
  double HardwareManager::getCameraExposure()
  {
    double exposureValue = 0;

    try
    {
      exposureValue = m_mmcore->getExposure();
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }

    return exposureValue;
  }

  /**
   * @brief colorBalanceOff To turn off the color balance settings of the
   * camera.
   */
  void HardwareManager::colorBalanceOff()
  {
    try
    {
      std::string propName = "Camera/GenICam/AnalogControl/BalanceWhiteAuto";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Off");

      propName = "ImageProcessing/ColorTwist/ColorTwistEnable";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Off");

      propName = "ImageProcessing/ColorTwist/"
                 "ColorTwistInputCorrectionMatrixEnable";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Off");

      propName = "ImageProcessing/ColorTwist/"
                 "ColorTwistOutputCorrectionMatrixEnable";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Off");

      propName = "Camera/GenICam/AnalogControl/BalanceRatioSelector";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Red");

      propName = "Camera/GenICam/AnalogControl/BalanceRatio";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(),
                            CONFIG()->realConfig(ConfigKeys::
                                                 RED_BALANCE_RATIO));

      propName = "Camera/GenICam/AnalogControl/"
                 "BalanceRatioSelector";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Blue");

      propName = "Camera/GenICam/AnalogControl/BalanceRatio";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(),
                            CONFIG()->realConfig(ConfigKeys::
                                                 BLUE_BALANCE_RATIO));

      propName = "ImageProcessing/LUTOperations/LUTEnable";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Off");
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setContinuousMode To set the camera in continuous mode.
   * @param mode Mode of trigger (XY- or Z-Stage).
   */
  void HardwareManager::setContinuousMode(TriggerMode mode)
  {
    try
    {
      std::string propName = "Camera/GenICam/"
          "AcquisitionControl/AcquisitionMode";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Continuous");
      // Doing a dummy snap.
      //snapImage();

      propName = "Camera/GenICam/AcquisitionControl/TriggerMode";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "On");

      propName = "Camera/GenICam/AcquisitionControl/TriggerSelector";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "FrameStart");

      if(mode == Z_TRIGGER)
      {
        propName = "Camera/GenICam/AcquisitionControl/TriggerSource";
        m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Line5");
        m_zTrigger = true;
      }
      else if(mode == XY_TRIGGER)
      {
        propName = "Camera/GenICam/AcquisitionControl/TriggerSource";
        m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Line5");
        m_xyTrigger = true;
      }

      propName = "Camera/GenICam/AcquisitionControl/TriggerActivation";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "RisingEdge");
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief stopContinuousMode To disable the continous mode of the camera.
   */
  void HardwareManager::stopContinuousMode()
  {
    try
    {
      std::string propName = "Camera/GenICam/AcquisitionControl/TriggerMode";
      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Off");

      // If the XY-Stage is still moving then wait till it stops.
      while(m_mmcore->deviceBusy(m_xyStageDev.c_str()))
      {
        // Wait till device reaches its position.
        // This will block.
      }

//      propName = "Camera/GenICam/AcquisitionControl/AcquisitionMode";
//      m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "SingleFrame");
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }
  /**
   * @brief getCameraTimeLapse
   * @return time delay of the camera
   */
  double HardwareManager::getCameraTimeLapse()
  {
    std::string propName = "Camera/GenICam/EventControl/EventFrameEndData/EventFrameEndTimestamp";
    std::string frameEndTS = m_mmcore->getProperty(m_camDev.c_str(), propName.c_str());
    propName = "Camera/GenICam/EventControl/EventLine4RisingEdgeData/EventLine4RisingEdgeTimestamp";
    std::string l4RETS = m_mmcore->getProperty(m_camDev.c_str(), propName.c_str());
    return double(std::stof(frameEndTS) - std::stof(l4RETS));
  }

  /**
   * @brief closeEventCameraTimeLapse
   */
  void HardwareManager::closeEventCameraTimeLapse()
  {
    std::string propName = "Camera/GenICam/EventControl/EventSelector";
    m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "FrameEnd");
    propName = "Camera/GenICam/EventControl/EventNotification";
    m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Off");
    propName = "Camera/GenICam/EventControl/EventSelector";
    m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Line4RisingEdge");
    propName = "Camera/GenICam/EventControl/EventNotification";
    m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Off");
  }

  /**
   * @brief openEventCameraTimeLapse
   */
  void HardwareManager::openEventCameraTimeLapse()
  {
    std::string propName = "Camera/GenICam/EventControl/EventSelector";
    m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "FrameEnd");
    propName = "Camera/GenICam/EventControl/EventNotification";
    m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "On");
    propName = "Camera/GenICam/EventControl/EventSelector";
    m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "Line4RisingEdge");
    propName = "Camera/GenICam/EventControl/EventNotification";
    m_mmcore->setProperty(m_camDev.c_str(), propName.c_str(), "On");
  }

  /**
   * @brief setZPosition To set the position of the Z-Stage.
   * @param zVal Position in microns.
   * @param waitTillSet True indicates wait till Z-Stage reaches its position
   * and false indicates don't wait.
   * @return Returns true if Z-Stage position is set successfully else returns
   * false.
   */
  bool HardwareManager::setZPosition(double zVal, bool waitTillSet)
  {
    bool retVal = true;
    try
    {
      // Z-Stage upper limit check.
      if(zVal <= CONFIG()->realConfig(ConfigKeys::Z_UPPER_LIMIT))
      {
        double xPos = m_mmcore->getXPosition(m_xyStageDev.c_str());
        m_mmcore->setPosition(m_zStageDev.c_str(), zVal);
        if (waitTillSet == true)
        {
          while (m_mmcore->deviceBusy(m_zStageDev.c_str()))
          {
            // Wait till device reaches its position.
            // This will block.
          }
        }
      }
      else
      {
        SPIN_LOG_WARNING("Can't move Z-Stage. Reached the "
                         "upper limit of Z-Stage.");
        retVal = false;
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
      retVal = false;
    }

    return retVal;
  }

  /**
   * @brief stopZStage To stop the movement of the Z-Stage.
   */
  void HardwareManager::stopZStage()
  {
    setRelativeZPosition(0);

    resetController();
  }


  /**
   * @brief getZPosition To get the current position of the Z-Stage.
   * @return Returns the current position of the Z-Stage in microns.
   */
  double HardwareManager::getZPosition()
  {
    double zVal = -1;
    try
    {
      zVal = m_mmcore->getPosition(m_zStageDev.c_str());
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }

    return zVal;
  }

  /**
   * @brief setRelativeZPosition To set the relative position of the Z-Stage.
   * @param zVal Relative position in microns.
   * @param waitTillSet True indicates wait till Z-Stage reaches its position
   * and false indicates don't wait.
   */
  void HardwareManager::setRelativeZPosition(double zVal, bool waitTillSet)
  {
    try
    {
      // Z-Stage upper limit check.
      double currZPos = getZPosition() + zVal;
      if(currZPos <= CONFIG()->realConfig(ConfigKeys::Z_UPPER_LIMIT))
      {
        m_mmcore->setRelativePosition(m_zStageDev.c_str(), zVal);
        if (waitTillSet == true)
        {
          while (m_mmcore->deviceBusy(m_zStageDev.c_str()))
          {
            // Wait till device reaches its position.
            // This will block.
          }
        }
      }
      else
      {
        SPIN_LOG_WARNING("Can't move Z-Stage. Reached the "
                         "upper limit of Z-Stage.");
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setZStageSpeed To set the speed of the Z-Stage.
   * @param zSpeed Speed in mm/s.
   */
  void HardwareManager::setZStageSpeed(double zSpeed)
  {
    try
    {
      std::string propName = "SpeedZ";
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(), zSpeed);
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief getZStageSpeed To get the speed of the Z-Stage.
   * @return Returns the speed of the Z-Stage in mm/s.
   */
  double HardwareManager::getZStageSpeed()
  {
    double zSpeed = 0;
    try
    {
      std::string propName = "SpeedZ";
      std::string zSpeedStr = m_mmcore->getProperty(m_zStageDev.c_str(),
                                                    propName.c_str());
      zSpeed = atof(zSpeedStr.c_str());
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }

    return zSpeed;
  }

  /**
   * @brief enableZTriggerMode To enable the trigger mode for the Z-Stage.
   * @param stepSize Step size for the trigger mode.
   */
  void HardwareManager::enableZTriggerMode(double stepSize)
  {
    try
    {

      std::string propName = "XYTriggerPreDelayZ";
      double xyPreTriggerDelay = 0;
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(),
                            xyPreTriggerDelay);

      propName = "ZTriggerPreDelayZ";
      double zPreTriggerDelay = CONFIG()->realConfig(ConfigKeys::
                                                     Z_PRE_TRIGGER_DELAY);
      m_mmcore->setProperty(m_zStageDev.c_str(),
                            propName.c_str(), zPreTriggerDelay);

      propName = "XYTriggerPostDelayZ";
      double xyPostTriggerDelay = 0;
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(),
                            xyPostTriggerDelay);

      propName = "ZTriggerPostDelayZ";
      double zPostTriggerDelay = CONFIG()->realConfig(ConfigKeys::
                                                      Z_POST_TRIGGER_DELAY);
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(),
                            zPostTriggerDelay);

      propName = "SetPulseDurationX";
      double xPulseDuration = CONFIG()->realConfig(ConfigKeys::
                                                      X_TRIGGER_PULSE_DURATION);
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(),
                            xPulseDuration);

      propName = "SetPulseDurationY";
      double yPulseDuration = CONFIG()->realConfig(ConfigKeys::
                                                      Y_TRIGGER_PULSE_DURATION);
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(),
                            yPulseDuration);

      propName = "SetPulseDurationZ";
      double zPulseDuration = CONFIG()->realConfig(ConfigKeys::
                                                      Z_TRIGGER_PULSE_DURATION);
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(),
                            zPulseDuration);

      propName = "ZTrigger";
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(), stepSize);

      propName = "EnableTrigger";
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(), "ON");
      m_zTrigger = true;
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setZBacklash To set the backlash value for the Z-Stage.
   * @param backlashValue Backlash value in microns.
   */
  void HardwareManager::setZBacklash(double backlashValue)
  {
    try
    {
      std::string propName = "BacklashZ";
      m_mmcore->setProperty(m_zStageDev.c_str(),
                            propName.c_str(), backlashValue);
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setXYPosition To set the position of the XY-Stage.
   * @param xPos X position in microns.
   * @param yPos Y position in microns.
   * @param waitTillSet True indicates wait till XY-Stage reaches its position
   * and false indicates don't wait.
   */
  void HardwareManager::setXYPosition(double xPos, double yPos,
                                      bool waitTillSet)
  {
    try
    {
      xPos += CONFIG()->realConfig(ConfigKeys::X_OFFSET);
      yPos += CONFIG()->realConfig(ConfigKeys::Y_OFFSET);

      m_mmcore->setXYPosition(xPos, yPos);
      SPIN_LOG_INFO(QObject::tr("After offset, X Pos: %1,Y Pos: %2").
                    arg(xPos).arg(yPos));

      if(waitTillSet)
      {
          while(m_mmcore->deviceBusy(m_xyStageDev.c_str()))
          {
            // Wait till device reaches its position.
            // This will block.
          }
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief getXYPosition To get the current position of the XY-Stage.
   * @param xPos Reference to the X position in microns.
   * @param yPos Refernce to the Y position in microns.
   */
  void HardwareManager::getXYPosition(OUT_PARAM double& xPos,
                                      OUT_PARAM double& yPos)
  {
    try
    {
      xPos = m_mmcore->getXPosition(m_xyStageDev.c_str());
      yPos = m_mmcore->getYPosition(m_xyStageDev.c_str());

      xPos -= CONFIG()->realConfig(ConfigKeys::X_OFFSET);
      if(xPos < 0)
      {
        xPos = 0;
      }

      yPos -= CONFIG()->realConfig(ConfigKeys::Y_OFFSET);
      if(yPos < 0)
      {
        yPos = 0;
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setAbsoluteXYPosition To set the absolute position of the
   * XY-Stage.
   * @param xPos Absolute X position in microns.
   * @param yPos Absolute Y position in microns.
   * @param waitTillSet True indicates wait till XY-Stage reaches its position
   * and false indicates don't wait.
   */
  void HardwareManager::setAbsoluteXYPosition(double xPos, double yPos,
                                              bool waitTillSet)
  {
    try
    {
      m_mmcore->setXYPosition(xPos, yPos);
      if(waitTillSet)
      {
          while(m_mmcore->deviceBusy(m_xyStageDev.c_str()))
          {
            // Wait till device reaches its position.
            // This will block.
          }
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief getAbsoluteXYPosition To get the current absolute position of the
   * XY-Stage.
   * @param xPos Reference to the absolute X position in microns.
   * @param yPos Reference to the absolute Y position in microns.
   */
  void HardwareManager::getAbsoluteXYPosition(OUT_PARAM double& xPos,
                                              OUT_PARAM double& yPos)
  {
    try
    {
      xPos = m_mmcore->getXPosition(m_xyStageDev.c_str());
      yPos = m_mmcore->getYPosition(m_xyStageDev.c_str());
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setXYRelativePosition To set the relative position of the
   * XY-Stage.
   * @param xPos X position in microns.
   * @param yPos Y position in microns.
   */
  void HardwareManager::setXYRelativePosition(double xPos, double yPos)
  {
    try
    {
      m_mmcore->setRelativeXYPosition(m_xyStageDev.c_str(), xPos, yPos);
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief stopXYStage To stop the movement of the XY-Stage.
   */
  void HardwareManager::stopXYStage()
  {
    try
    {
      setXYRelativePosition(0, 0);
      m_mmcore->setProperty(m_xyStageDev.c_str(), "EnableJoyStick", "OFF");
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setXYStageSpeed To set the speed of the XY-Stage.
   * @param xySpeed Speed in mm/s.
   */
  void HardwareManager::setXYStageSpeed(double xySpeed)
  {
    try
    {
      // Specifically for Vulcan system.
      std::string propName = "SpeedXY";
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(), xySpeed);
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief getXYStageSpeed To get the current movement speed of the
   * XY-Stage.
   * @return Speed of the XY-Stage in mm/s.
   */
  double HardwareManager::getXYStageSpeed()
  {
    double xySpeed = -1;
    try
    {
      std::string propName = "SpeedXY";
      std::string xySpeedStr = m_mmcore->getProperty(m_xyStageDev.c_str(),
                                                     propName.c_str());
      xySpeed = atof(xySpeedStr.c_str());
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }

    return xySpeed;
  }

  /**
   * @brief getXYStageStepSize To get the step size of the XY-Stage for
   * capture in continuous mode.
   * @return Returns the step size in microns.
   */
  double HardwareManager::getXYStageStepSize()
  {
    double xyStepSize = 0;
    try
    {
      std::string propName = "GetStepSizeXYUm";
      std::string xyStepSizeStr = m_mmcore->getProperty(m_xyStageDev.c_str(),
                                                        propName.c_str());
      xyStepSize = atof(xyStepSizeStr.c_str());
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }

    return xyStepSize;
  }

  /**
   * @brief enableTriggerMode To set the XY-Stage into trigger mode for a
   * given step size.
   * @param stepSize Step size in microns for capturing in continuouse mode.
   * @param mode Mode of trigger (X- or Y-).
   */
  void HardwareManager::enableXYTriggerMode(double stepSize, TriggerMode mode)
  {
    try
    {
      std::string propName = "XYTriggerPreDelay";
      double xyPreTriggerDelay =
          CONFIG()->realConfig(ConfigKeys::XY_PRE_TRIGGER_DELAY);
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(),
                            xyPreTriggerDelay);

      propName = "XYTriggerPostDelay";
      double xyPostTriggerDelay =
          CONFIG()->realConfig(ConfigKeys::XY_POST_TRIGGER_DELAY);
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(),
                            xyPostTriggerDelay);

      propName = "ZTriggerPreDelay";
      double zPreTriggerDelay = 0;
      m_mmcore->setProperty(m_xyStageDev.c_str(),
                            propName.c_str(), zPreTriggerDelay);

      propName = "ZTriggerPostDelay";
      double zPostTriggerDelay = 0;
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(),
                            zPostTriggerDelay);

      propName = "SetPulseDurationX";
      double xPulseDuration = CONFIG()->realConfig(ConfigKeys::
                                                      X_TRIGGER_PULSE_DURATION);
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(),
                            xPulseDuration);

      propName = "SetPulseDurationY";
      double yPulseDuration = CONFIG()->realConfig(ConfigKeys::
                                                      Y_TRIGGER_PULSE_DURATION);
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(),
                            yPulseDuration);

      propName = "SetPulseDurationZ";
      double zPulseDuration = CONFIG()->realConfig(ConfigKeys::
                                                      Z_TRIGGER_PULSE_DURATION);
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(),
                            zPulseDuration);

      switch(mode)
      {
        case X_TRIGGER:
        {
          std::string propName = "XTrigger";
          m_mmcore->setProperty(m_xyStageDev.c_str(),
                                propName.c_str(), stepSize);

          if(CONFIG()->intConfig(ConfigKeys::COLUMN_ZSTACK_MODE))
          {
            propName = "ZStackXSteps";
            m_mmcore->setProperty(m_zStageDev.c_str(),
                                  propName.c_str(), stepSize);
          }

          propName = "EnableTrigger";
          m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(), "ON");
          m_xyTrigger = true;
          break;
        }

        case Y_TRIGGER:
        {
          std::string propName = "YTrigger";
          m_mmcore->setProperty(m_xyStageDev.c_str(),
                                propName.c_str(), stepSize);

          if(CONFIG()->intConfig(ConfigKeys::COLUMN_ZSTACK_MODE))
          {
            propName = "ZStackYSteps";
            m_mmcore->setProperty(m_zStageDev.c_str(),
                                  propName.c_str(), stepSize);
          }

          propName = "EnableTrigger";
          m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(), "ON");
          m_xyTrigger = true;
          break;
        }

        case XY_TRIGGER:
        {
          // Do nothing.
          break;
        }

        case Z_TRIGGER:
        {
          // Do nothing.
          break;
        }

        case UNKNOWN:
        {
          // Do nothin.
          break;
        }
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief disableTriggerMode To disable the trigger mode set for the
   * XY-Stage.
   */
  void HardwareManager::disableTriggerMode()
  {
    try
    {
      std::string propName = "EnableTrigger";
      if(m_xyTrigger)
      {
        m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(), "OFF");
        m_xyTrigger = false;
      }

      if(m_zTrigger)
      {
        m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(), "OFF");
        m_zTrigger = false;
      }
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief resetController To reset the controller.
   */
  void HardwareManager::resetController()
  {
    try
    {
      std::string devName = "JoyStick";
      std::string propName = "EnableJoyStick";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), "ON");

      m_mmcore->setProperty(devName.c_str(), propName.c_str(), "OFF");
    }
    catch(const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief sendDirectCommand send any command string to the board.
   * @param command ';' seperated command strings.
   */
  void HardwareManager::sendDirectCommand(std::string command)
  {
    try
    {
      std::string propName = "SendDesiredCommand";
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(),
                            command.c_str());

      propName = "EnableCommandLine";
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(),
                            "ON");
    }
    catch(const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setZStack To set the Z-stack for continuous mode of acquisition.
   * @param zStack Stack of ';' seperated Z values.
   * @param zStackTable String of ',' separated values for Z-Trigger capture.
   */
  void HardwareManager::setZStack(std::string zStack,
                                  std::string zStackTable,
                                  std::string zFgBgMap)
  {
    try
    {
      long zStackSize = CONFIG()->intConfig(ConfigKeys::XY_Z_TRIGGER_STACK_SIZE);
      double zStackStepSize = CONFIG()->realConfig(ConfigKeys::
                                                   XY_Z_TRIGGER_STEP_SIZE);
      std::string propName = "SetZStackSize";
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(), zStackSize);

      if(zStackSize == 0)
      {
        zStackStepSize = 0.0;
      }
      propName = "SetZStackStepSize";
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(),
                            zStackStepSize);

      propName = "SetZFgBgMap";
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(),
                            zFgBgMap.c_str());

      // Always set the stack size, step size and Fg/Bg map
      // before calling "SetZStack".
      propName = "SetZStack";
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(),
                            zStack.c_str());

      propName = "ZStackSetInstance";
      double zStackTriggerDelay = CONFIG()->realConfig(ConfigKeys::
                                                       Z_STACK_TRIGGER_DELAY);
      m_mmcore->setProperty(m_zStageDev.c_str(), propName.c_str(),
                            zStackTriggerDelay);

      propName = "SetZStackTable";
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(),
                            zStackTable.c_str());

      propName = "EnableNewAcqTrigger";
      m_mmcore->setProperty(m_xyStageDev.c_str(), propName.c_str(),
                            "ON");

    }
    catch(const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief connect To connect to the available devices.
   * @param magValue Current magnification settings.
   * @return Returns true if the connection to all devices was successful
   * else returns false.
   */
  bool HardwareManager::connect(Magnification magValue)
  {
    bool retVal = true;

    QElapsedTimer connectTimer;
    connectTimer.start();

    // Disconnect.
    disconnect();

    // Initialize the translation devices.
    if(!initStage())
    {
      SPIN_LOG_ERROR("Failed to initialize stage devices.");
      retVal = false;
    }

    // Set the translation stage properties.
    if(!setStageProps())
    {
      SPIN_LOG_ERROR("Failed to initialize stage device properties.");
      retVal = false;
    }

    // Initialize the camera.
    if(!initCamera())
    {
      SPIN_LOG_ERROR("Failed to initialize camera device.");
      retVal = false;
    }

    // Set the camera properties.
    if(!setCamProps(magValue))
    {
      SPIN_LOG_ERROR("Failed to initialize camera device properties.");
      retVal = false;
    }

    if(retVal)
    {
      checkPosition();
    }

    qreal connectTime = connectTimer.elapsed() / 1000.0;
    SPIN_LOG_INFO(QObject::tr("Time to connect to hardware (s): %1").
                  arg(connectTime))
    m_isConnected = retVal;

    return m_isConnected;
  }

  /**
   * @brief disconnect To disconnect from all connected devices.
   * @return Returns true if the disconnection was successful else returns
   * false.
   */
  bool HardwareManager::disconnect()
  {
    bool retVal = true;

    if(isConnected())
    {
      retVal = resetConfiguration();
    }

    return retVal;
  }

  /**
   * @brief resetConfiguration To reset the current loaded hardware
   * configuration.
   * @return Returns true on successfully resetting the current loaded
   * configuration else returns false.
   */
  bool HardwareManager::resetConfiguration()
  {
    try
    {
      Q_FOREACH(IHardwareEventListener* listener, m_listeners)
      {
        listener->notifyHardwareDisconnection();
      }

      m_mmcore->reset();
      m_isConnected = false;

      return true;
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
      return false;
    }
  }

  /**
   * @brief setObjective To set the objective to the given magnification
   * type.
   * @param magVal Magnification value.
   */
  void HardwareManager::setObjective(Magnification magVal)
  {
    std::string magStr;
    switch(magVal)
    {
    case MAG_20X:
      magStr = CONFIG()->stringConfig(ConfigKeys::MAG_20X_POS).toStdString();
      break;

    case MAG_40X:
      magStr = CONFIG()->stringConfig(ConfigKeys::MAG_40X_POS).toStdString();
      break;

    case MAG_100X:
      magStr = CONFIG()->stringConfig(ConfigKeys::MAG_100X_POS).toStdString();
      break;

    case MAG_100X_OIL:
      magStr = CONFIG()->stringConfig(ConfigKeys::
                                      MAG_100X_OIL_POS).toStdString();
      break;

    default:
      magStr = CONFIG()->stringConfig(ConfigKeys::MAG_HOME_POS).toStdString();
      break;
    }

    try
    {
      m_mmcore->setProperty("ObjectiveLens", "SetObjectiveLens",
                            magStr.c_str());
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief insertFilter To insert the FLD filter.
   */
  void HardwareManager::insertFilter()
  {
    try
    {
      m_mmcore->setProperty("XYZCom", "SetFilter",
                            CONFIG()->stringConfig(
                              ConfigKeys::INSERT_FILTER_TAG).toStdString().c_str());
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief removeFilter To remove the FLD filter.
   */
  void HardwareManager::removeFilter()
  {
    try
    {
      m_mmcore->setProperty("XYZCom", "SetFilter",
                            CONFIG()->stringConfig(
                              ConfigKeys::REMOVE_FILTER_TAG).toStdString().c_str());
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setLedOff To turn OFF the LED.
   */
  void HardwareManager::setLedOff()
  {
    try
    {
      m_mmcore->setProperty("WhiteLamp", "SetLedPower", "OFF");
    }
    catch(const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occured: %1").arg(e.what()));
    }
  }

  /**
   * @brief setLedOn To turn ON the LED.
   */
  void HardwareManager::setLedOn()
  {
    try
    {
      m_mmcore->setProperty("WhiteLamp", "SetLedPower", "ON");
    }
    catch(const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occured: %1").arg(e.what()));
    }
  }

  /**
   * @brief home To set the translation stage to its home position.
   */
  void HardwareManager::home()
  {
    try
    {
      std::string objHomePos =
          CONFIG()->stringConfig(ConfigKeys::MAG_HOME_POS).toStdString();
      m_mmcore->home(m_zStageDev.c_str());
      m_mmcore->setProperty("ObjectiveLens", "SetObjectiveLens",
                            objHomePos.c_str());
      m_mmcore->home(m_xyStageDev.c_str());

      removeFilter();

      setLedOff();
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief eject To eject the translation stage to its home position
   * (Different from home command. Home command moves the translation stage
   * to the home sensor. This function moves the translation stage to its
   * stored 0 position.)
   */
  void HardwareManager::eject()
  {
    // Set Z to 0 position.
    double zPos = 0;
    m_mmcore->setPosition(m_zStageDev.c_str(), zPos);
    while(m_mmcore->deviceBusy(m_zStageDev.c_str()))
    {
      // Wait till device reaches its position.
    }

    // Set objective to home position.
    setObjective(MAG_HOME);

    // Set Y to 0 position.
    double xPos = m_mmcore->getXPosition(m_xyStageDev.c_str());
    double yPos = 0;
    m_mmcore->setXYPosition(xPos, yPos);
    while(m_mmcore->deviceBusy(m_xyStageDev.c_str()))
    {
      // Wait till device reaches its position.
    }

    // Set X to 0 position.
    xPos = 0;
    yPos = m_mmcore->getYPosition(m_xyStageDev.c_str());
    m_mmcore->setXYPosition(xPos, yPos);
    while(m_mmcore->deviceBusy(m_xyStageDev.c_str()))
    {
      // Wait till device reaches its position.
    }

    // Remove the filter.
    removeFilter();
  }

  /**
   * @brief checkPosition To check if the XY-Stage is at the correct offset
   * position.
   */
  void HardwareManager::checkPosition()
  {
    // Important check!!!
    double xPos = m_mmcore->getXPosition(m_xyStageDev.c_str());
    double yPos = m_mmcore->getYPosition(m_xyStageDev.c_str());
    SPIN_LOG_DEBUG(QObject::tr("Current Start Pos, "
                               "X Pos: %1\tY Pos: %2").arg(xPos).arg(yPos));
    if(xPos < CONFIG()->realConfig(ConfigKeys::X_OFFSET))
    {
      // First move X position.
      xPos = CONFIG()->realConfig(ConfigKeys::X_OFFSET);
      if(xPos < 0)
      {
        xPos = 0;
      }
      m_mmcore->setXYPosition(xPos, yPos);
      // Wait till X position is reached.
      while(m_mmcore->deviceBusy(m_xyStageDev.c_str()))
      {

      }

      xPos = m_mmcore->getXPosition(m_xyStageDev.c_str());
      yPos = m_mmcore->getYPosition(m_xyStageDev.c_str());

      // Now move Y position.
      yPos = CONFIG()->realConfig(ConfigKeys::Y_OFFSET);
      if(yPos < 0)
      {
        yPos = 0;
      }
      m_mmcore->setXYPosition(xPos, yPos);
      // Wait till Y position is reached.
      while(m_mmcore->deviceBusy(m_xyStageDev.c_str()))
      {

      }
    }

    xPos = m_mmcore->getXPosition(m_xyStageDev.c_str());
    yPos = m_mmcore->getYPosition(m_xyStageDev.c_str());
    SPIN_LOG_DEBUG(QObject::tr("After Offset Check Current Start Pos, "
                               "X Pos: %1\tY Pos: %2").arg(xPos).arg(yPos));
  }

  /**
   * @brief setLedBrightness To set the brightness value of the LED used for
   * acquisition.
   * @param brightnessValue Brightness value.
   */
  void HardwareManager::setLedBrightness(double brightnessValue)
  {
    try
    {
      std::string devName = "WhiteLamp";
      std::string propName = "SetLedBrightness";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), brightnessValue);
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setOneXLedBrightness To set the brightness value of the LED
   * @param brightnessValue Brightness value.
   */
  void HardwareManager::setOneXLedBrightness(double brightnessValue)
  {
    try
    {
      std::string devName = "WhiteLamp";
      std::string propName = "SetOneXLedBrightness";
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), brightnessValue);
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setPropertyStr To set a device property with string value.
   * @param devName Name of the device.
   * @param propName Name of the property.
   * @param propValue Value of the property.
   */
  void HardwareManager::setPropertyStr(std::string devName,
                                       std::string propName,
                                       std::string propValue)
  {
    try
    {
      m_mmcore->setProperty(devName.c_str(), propName.c_str(),
                            propValue.c_str());
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setPropertyInt To set a device property with integer value.
   * @param devName Name of the device.
   * @param propName Name of the property.
   * @param propValue Value of the property.
   */
  void HardwareManager::setPropertyInt(std::string devName,
                                       std::string propName, long propValue)
  {
    try
    {
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), propValue);
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief setPropertyReal To set a device property with float value.
   * @param devName Name of the device.
   * @param propName Name of the property.
   * @param propValue Value of the property.
   */
  void HardwareManager::setPropertyReal(std::string devName,
                                        std::string propName, double propValue)
  {
    try
    {
      m_mmcore->setProperty(devName.c_str(), propName.c_str(), propValue);
    }
    catch (const std::exception& e)
    {
      SPIN_ERROR(QObject::tr("Exception occurred: %1").arg(e.what()));
    }
  }

  /**
   * @brief ~HardwareManager Default class destructor.
   */
  HardwareManager::~HardwareManager()
  {
    resetConfiguration();
  }
}
