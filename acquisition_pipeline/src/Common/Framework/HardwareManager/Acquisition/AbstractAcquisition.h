#ifndef ABSTRACTACQUISITION_H
#define ABSTRACTACQUISITION_H

#include <QObject>
#include <QString>
#include <QElapsedTimer>
#include <QPointer>
#include <QVariantList>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"
#include "Common/Framework/HardwareManager/Utils/Types.h"

namespace spin
{
  // Forward declaration;
  class ImageWriterWorker;

  namespace acquisition
  {
    /**
     * @brief The AbstractAcquisition class is the base for controlling the
     * acquisition of images.
     */
    class AbstractAcquisition : public QObject
    {
      Q_OBJECT
    public:
      /**
       * @brief ~AbstractAcquisition Default class destructor,
       * destroys objects owned by the class.
       */
      virtual ~AbstractAcquisition();

      /**
       * @brief init To initialize the acquisition for a grid of a slide.
       * @return Returns true if the initialization is successful else returns
       * false.
       */
      virtual bool init(SpecimenType specType) = 0;

      /**
       * @brief process To start the acquisition of a grid of a slide.
       */
      virtual void process() = 0;

    Q_SIGNALS:
      /**
       * @brief acquisitionFinished Signal emitted when an acquisition of a grid
       * has finished.
       */
      void acquisitionFinished();

      /**
       * @brief processFinished Signal emitted to indicate that the current
       * acquisition pipeline has completed its processing.
       * @param slideName Name of the slide.
       * @param gridName Name of the grid.
       * @param writeError True indicates write error else no error.
       */
      void processFinished(QString slideName, QString gridName,
                           bool writeError);

    protected:
      /**
       * @brief AbstractAcquisition Class constructor, to initialize the
       * object for the given slide and grid.
       * @param slideName Name of the slide that requires to be acquired.
       * @param gridName Name of the grid that required to tbe acquired.
       * @param parent Instance of the parent QObject.
       */
      AbstractAcquisition(QString slideName,
                          QString gridName,
                          QObject* parent = 0);

      /**
       * @brief setupWriterWorker To setup the worker class for writing
       * captured images to the disk.
       */
      void setupWriterWorker();

    protected Q_SLOTS:
      /**
       * @brief onAcquisitionFinished Slot called when acquisition finishes.
       */
      virtual void onAcquisitionFinished();

      /**
       * @brief onWriteError Slot called when a write error occurs.
       * @param errMsg Error message.
       */
      virtual void onWriteError(QString errMsg);

    protected:
      /**
       * @brief m_writeError To store the state of the write error.
       */
      bool m_writeError;

      /**
       * @brief m_normalSpeed To store the default speed of the XY-Stage.
       */
      double m_normalSpeed;

      /**
       * @brief m_maxSpeed
       */
      double m_maxSpeed;

      /**
       * @brief m_defSpeed
       */
      double m_defSpeed;

      /**
       * @brief m_slideName Current slide being acquired.
       */
      QString m_slideName;

      /**
       * @brief m_gridName Current grid being acquired.
       */
      QString m_gridName;

      /**
       * @brief m_specType To store the specimen type.
       */
      SpecimenType m_specType;

      /**
       * @brief m_gridInfo To store the structure of the current grid being
       * acquired.
       */
      GridInfo m_gridInfo;

      /**
       * @brief m_acqTimer Timer to keep a track of the time taken to acquire
       * a grid.
       */
      QElapsedTimer m_acqTimer;

      /**
       * @brief m_capThread Pointer to the Z-Stage capture thread object.
       */
      QPointer <QThread> m_capThread;

      /**
       * @brief m_writerWorker Pointer to the writer worker.
       */
      QPointer <spin::ImageWriterWorker> m_writerWorker;

      /**
       * @brief m_writerThread Pointer to the writer thread;
       */
      QPointer <QThread> m_writerThread;

      /**
       * @brief m_aoiIdList To store the list of AoI Ids for batch commit to the
       * database.
       */
      QVariantList m_aoiIdList;

      /**
       * @brief m_bestIdxList To store the list of best indices for each AoI
       * for batch commit to the database.
       */
      QVariantList m_bestIdxList;

      /**
       * @brief m_bestZList To store the list of best indices for each AoI
       * for batch commit to the database.
       */
      QVariantList m_bestZList;

      /**
       * @brief m_stackPresentList To store the list of status for each AoI
       * indicating whether the stack is present or not.
       */
      QVariantList m_stackPresentList;

      /**
       * @brief m_aoiZValueList To store the list of Z values for all the AoIs
       * for batch commit to the database.
       */
      QVariantList m_aoiZValueList;

      /**
       * @brief m_aoiFocusMetricList To store the list of focus metric values
       * for all the AoIs for batch commit to the database.
       */
      QVariantList m_aoiFocusMetricList;

      /**
       * @brief m_aoiColorMetricList To store the list of color metric values
       * for all the AoIs for batch commit to the database.
       */
      QVariantList m_aoiColorMetricList;

    };// end of AbstractAcquisition class.
  } // end of acquisition namespace.
} // end of spin namespace.

#endif // ABSTRACTACQUISITION_H
