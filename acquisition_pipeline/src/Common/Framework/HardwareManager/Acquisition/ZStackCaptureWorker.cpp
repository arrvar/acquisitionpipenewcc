#include <QThread>
#include <QFileInfo>
#include <QDebug>

#include "Common/Framework/HardwareManager/Acquisition/ZStackCaptureWorker.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"

#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"

namespace spin
{
  namespace acquisition
  {
    /**
     * @brief The ZStackCaptureWorker class is a worker class for capturing
     * a z-stack of images with trigger.
     */
    ZStackCaptureWorker::ZStackCaptureWorker(QObject* parent) :
      QObject(parent),
      m_zStepSize(0),
      m_startZ(0),
      m_stopZ(0),
      m_focusChannel(1),
      m_mag(0)
    {

    }

    /**
     * @brief clearStack Release stack memory.
     */
    void ZStackCaptureWorker::clearStack()
    {
      // Flush out remaining data if any.
      for(int idx = 0; idx < m_aoiInfo.m_zStack.size(); idx++)
      {
        if(m_aoiInfo.m_zStack[idx] != Q_NULLPTR)
        {
          delete[] m_aoiInfo.m_zStack[idx];
        }
      }
    }

    /**
     * @brief onStartCapture Slot called to start capture.
     */
    void ZStackCaptureWorker::onStartCapture()
    {
      bool err = false;
      const unsigned buffSize = Constants::FRAME_WIDTH *
          Constants::FRAME_HEIGHT * 4;
      // Create an image stack and reserve the size.
      QString errMsg;
      for(int idx = 0; idx < m_aoiInfo.m_zStack.size(); idx++)
      {
        // Capture the image and check for NULL.
        void* _imgPtr = HW_MANAGER()->snapImage();
        if(_imgPtr != Q_NULLPTR)
        {
          unsigned char* imgBuff = new unsigned char[buffSize];
          memcpy(imgBuff, _imgPtr, buffSize);

          m_aoiInfo.m_zStack[idx] = imgBuff;
        }
        else
        {
          err = true;
          errMsg = QObject::tr("Snap missed at index %1 of total %2").
              arg(idx).arg(m_aoiInfo.m_zStack.size());
          break;
        }
      }

      // Stop the thread.
      if(thread())
      {
        thread()->quit();
      }

      if(err == true)
      {
        clearStack();
        Q_EMIT captureError(errMsg);
      }
      else
      {
        int bestIdx = -1;
        double bestFocusMetric = 0;
        for(int idx = 0 ; idx < m_aoiInfo.m_zStack.size(); idx++)
        {
          cv::Mat inputImage(cv::Size(Constants::FRAME_WIDTH,
                                      Constants::FRAME_HEIGHT), CV_8UC4,
                             m_aoiInfo.m_zStack[idx]);

          // Split the image into four different channels.
          std::vector <cv::Mat> channels;
          channels.reserve(4);
          cv::split(inputImage, channels);

          double focusMetricVal =
              Utilities::computeFocusMetric(channels[m_focusChannel]);
          if(bestFocusMetric < focusMetricVal &&
             focusMetricVal <= CONFIG()->
             realConfig(ConfigKeys::FOCUS_METRIC_THRESHOLD))
          {
            bestFocusMetric = focusMetricVal;
            bestIdx = idx;
          }
        }

        if(bestIdx > -1)
        {
          m_aoiInfo.m_ptr = new unsigned char[buffSize];
          memcpy(m_aoiInfo.m_ptr, m_aoiInfo.m_zStack[bestIdx], buffSize);

          clearStack();
          Q_EMIT captureFinished();
        }
        else
        {
          clearStack();
          errMsg = "Failed to find best focus.";
          Q_EMIT captureError(errMsg);
        }
      }
    }

    /**
     * @brief ~ZStackCaptureWorker Default class destructor,
     * destroys objects owned by the class.
     */
    ZStackCaptureWorker::~ZStackCaptureWorker()
    {

    }
  }
}
