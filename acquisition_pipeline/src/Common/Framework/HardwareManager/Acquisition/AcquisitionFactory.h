#ifndef ACQUISITIONFACTORY_H
#define ACQUISITIONFACTORY_H

#include <memory>

#include "Common/Framework/HardwareManager/Utils/Common.h"
#include "Common/Framework/HardwareManager/Utils/Types.h"

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionPipeline.h"
#include "Common/Framework/HardwareManager/Acquisition/AcqStartStop.h"
#include "Common/Framework/HardwareManager/Acquisition/AcqZStack.h"

namespace spin
{
  namespace acquisition
  {
    class AcquisitionFactory
    {
    public:
      static AbstractAcquisition* getPipeline(QString slideName,
                                              QString gridName,
                                              SpecimenType specType)
      {
        switch(specType)
        {
        case TISSUE_BIOPSY:
          return new AcquisitionPipeline(slideName, gridName);

        case BLOOD_SMEAR:
          return new AcqStartStop(slideName, gridName);

        case MICRONUCLEUS_TEST:
          return new AcqZStack(slideName, gridName);

        default:
          return Q_NULLPTR;
        }
      }

    private:
      AcquisitionFactory();

      DISALLOW_COPY_AND_ASSIGNMENT(AcquisitionFactory)
    };
  }
}

#endif // ACQUISITIONFACTORY_H
