#ifndef ZCAPTUREWORKER_H
#define ZCAPTUREWORKER_H

#include <QObject>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

namespace spin
{
  namespace acquisition
  {
    /**
     * @brief The SnapZThread class is a worker class for capturing with trigger
     */
    class ZCaptureWorker : public QObject
    {
      Q_OBJECT

    public:    
      /**
       * @brief ZCaptureWorker Default class constructor, initializes objects
       * owned by the class.
       * @param slideName
       * @param parent
       */
      ZCaptureWorker(QString slideName, QObject* parent = 0);

      /**
       * @brief ~SnapZThread Default class destructor, destroys objects owned
       * by the class.
       */
      ~ZCaptureWorker();

      /**
       * @brief init To initialize the worker with focusStepSize, startZ and stopZ
       * internally this should handle only capture movement and
       * speed should be set by parent.
       * @param focusStepSize distance between two captures(um).
       * @param startZ starting position of Z to capture(um).
       * @param stopZ stopping position of Z to capture(um).
       */
      void init(int zStackSize, qreal zStepSize,
                qreal startZ, qreal stopZ, const AoIInfo& aoiInfo,
                int focusChannel, int mag)
      {
        m_acqStackSize = zStackSize;
        m_zStepSize = zStepSize;
        m_startZ = startZ;
        m_stopZ = stopZ;
        m_aoiInfo = aoiInfo;
        m_focusChannel = focusChannel;
        m_mag = mag;
      }

    Q_SIGNALS:
      /**
       * @brief captureFinished Signal emitted after capturing all the Z.
       */
      void captureFinished(int bestIdx, qreal zValue, qreal focusMetric,
                           QList <double> focusMetricList);

      /**
       * @brief captureError Signal emitted if an error occurs while capturing.
       */
      void captureError(QString errMsg);

    public Q_SLOTS:
      /**
       * @brief onStartCapture Slot called to start capture.
       */
      void onStartCapture();

    private:
      /**
       * @brief m_acqStackSize
       */
      int m_acqStackSize;

      /**
       * @brief m_zStepSize
       */
      qreal m_zStepSize;

      /**
       * @brief m_startZ
       */
      qreal m_startZ;

      /**
       * @brief m_stopZ
       */
      qreal m_stopZ;

      /**
       * @brief m_focusChannel
       */
      int m_focusChannel;

      /**
       * @brief m_mag
       */
      int m_mag;

      /**
       * @brief m_slideName To store the name of the slide of the AoIs.
       */
      QString m_slideName;

      /**
       * @brief AoIInfo for which Z stack to capture(um)
       */
      AoIInfo m_aoiInfo;

    };// end of ZCaptureWorker class.
  } // end of acquisition namespace.
} // end of spin namespace.

#endif // ZCAPTUREWORKER_H
