#include <QThread>

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Acquisition/AbstractAcquisition.h"
#include "Common/Framework/HardwareManager/Utils/ImageWriterWorker.h"

namespace spin
{
  namespace acquisition
  {
    /**
     * @brief AbstractAcquisition Class constructor, to initialize the
     * object for the given slide and grid.
     * @param slideName Name of the slide that requires to be acquired.
     * @param gridName Name of the grid that required to tbe acquired.
     * @param parent Instance of the parent QObject.
     */
    AbstractAcquisition::AbstractAcquisition(QString slideName,
                                             QString gridName,
                                             QObject* parent) :
      QObject(parent),
      m_writeError(false),
      m_normalSpeed(0),
      m_maxSpeed(0),
      m_defSpeed(0),
      m_slideName(slideName),
      m_gridName(gridName)
    {

    }

    /**
     * @brief onAcquisitionFinished Slot called when acquisition finishes.
     */
    void AbstractAcquisition::onAcquisitionFinished()
    {
      if(m_writerThread)
      {
        // Stop the writer thread and wait till it exits.
        m_writerWorker->onStopWorker();
        while(m_writerThread->isRunning())
        {

        }
      }

      Q_EMIT processFinished(m_slideName, m_gridName, m_writeError);
    }

    /**
     * @brief onWriteError Slot called when a write error occurs.
     * @param errMsg Error message.
     */
    void AbstractAcquisition::onWriteError(QString errMsg)
    {
      SPIN_LOG_ERROR(errMsg);

      m_writeError = true;
    }

    /**
     * @brief setupWriterWorker To setup the worker class for writing
     * captured images to the disk.
     */
    void AbstractAcquisition::setupWriterWorker()
    {
      m_writerThread = new QThread();

      m_writerWorker = new ImageWriterWorker();
      m_writerWorker->moveToThread(m_writerThread);

      connect(m_writerThread, &QThread::started,
              m_writerWorker, &ImageWriterWorker::onStartWorker);

      connect(m_writerWorker, &ImageWriterWorker::workerError,
              this, &AbstractAcquisition::onWriteError);
    }

    /**
     * @brief ~AbstractAcquisition Default class destructor,
     * destroys objects owned by the class.
     */
    AbstractAcquisition::~AbstractAcquisition()
    {

    }
  }
}
