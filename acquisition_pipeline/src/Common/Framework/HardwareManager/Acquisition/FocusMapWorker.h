#ifndef FOCUSMAPWORKER_H
#define FOCUSMAPWORKER_H

#include <QObject>
#include <QString>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

namespace spin
{
  namespace acquisition
  {
    /**
    * @brief The FocusMapWorker class will calculate
    * focus map of each next row using current rows best z values.
    */
    class FocusMapWorker : public QObject
    {
      Q_OBJECT
    public:
      /**
       * @brief FocusMapWorker Default class constructor, initializes objects
       * owned by the class.
       * @param parent
       */
      FocusMapWorker(QObject* parent = 0);

      /**
       * @brief ~FocusMapWorker Default class destructor, destorys objects
       * owned by the class.
       */
      ~FocusMapWorker();

    public:
      /**
      * @brief init Initialize parameters.
      */
      void init(double offset, QStringList preRefZList, QList <AoIInfo> aoiInfoList);

      /**
      * @brief getFocusMap To calculate focus map for current row using bestZ
      * values of previous row.
      */
      QStringList getFocusMap(QList <AoIInfo> aoiInfoList);

    private:
      /**
      * @brief m_preBestZList List of best z of each AOI in previous row.
      */
      QStringList m_preBestZList;

      /**
      * @brief m_preFGBGList List of bg status of each AOI in previous row.
      */
      QStringList m_preFGBGList;

      /**
      * @brief m_preFocusMetricList List of focus metric of best image of
      * each AOI in previous row.
      */
      QStringList m_preFocusMetricList;

    };// end of FocusMapWorker class.
  } // end of acquisition namespace.
} // end of spin namespace.

#endif // IMAGEMONITORINGWORKER_H
