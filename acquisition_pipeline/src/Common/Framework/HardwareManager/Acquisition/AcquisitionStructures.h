#ifndef HMACQUISITIONSTRUCTURES_H
#define HMACQUISITIONSTRUCTURES_H

#include <QString>
#include <QRectF>
#include <QList>
#include <QVector>

#include <opencv2/opencv.hpp>

#include "Common/Framework/HardwareManager/Utils/Types.h"

namespace spin
{
  namespace acquisition
  {
    /**
     * @brief The AoIInfo struct is used to store information of a single AoI
     * of a grid.
     */
    struct AoIInfo
    {
      AoIInfo() :
        m_ptr(Q_NULLPTR),
        m_bgState(false),
        m_bestIdx(0)
      {

      }

      /**
       * @brief m_whiteCorr To indicate whether white correction is required
       * or not.
       */
      bool m_whiteCorr;

      /**
       * @brief m_stackPresent To indicate whether a stack is available for
       * the AoI.
       */
      bool m_stackPresent;

      /**
       * @brief m_id Unique ID of the AoI.
       */
      int m_id;

      /**
       * @brief m_name Name of the AoI.
       */
      QString m_name;

      /**
       * @brief m_pos Position of the AoI on the slide in pixels.
       */
      QPointF m_pos;

      /**
       * @brief m_samplingZ BestZ value from focus sampling in microns.
       */
      double m_samplingZ;

      /**
       * @brief m_zVal Position of the AoI on the Z-Stage in microns.
       */
      double m_zVal;

      /**
       * @brief m_rowIdx Row index position of the AoI on the grid.
       */
      int m_rowIdx;

      /**
       * @brief m_colIdx Column index position of the AoI on the grid.
       */
      int m_colIdx;

      /**
       * @brief m_focusMetric Focus metric value of the AoI.
       */
      qreal m_focusMetric;

      /**
       * @brief m_ptr Raw pointer to store the reference of the image when
       * captured.
       */
      unsigned char* m_ptr;

      /**
       * @brief m_destPath Absolute path of the AoI where it would have to be
       * written.
       */
      QString m_destPath;

      /**
       * @brief m_whiteCorrDestPath Absolute path of the AoI where the white
       * corrected image would have to be written.
       */
      QString m_whiteCorrDestPath;

      /**
       * @brief m_stackDestPath Absolute path of the AoI where the Z-Stack
       * would have to be written.
       */
      QString m_stackDestPath;

      /**
       * @brief m_whiteCorrStackDestPath Absolute path of the AoI where the
       * white corrected Z-Stack would have to be written.
       */
      QString m_whiteCorrStackDestPath;

      /**
       * @brief m_img To store the image as a OpenCV matrix.
       */
      cv::Mat m_img;

      /**
       * @brief m_lowPowerId Id of the mapped low power AoI.
       */
      int m_lowPowerId;

      /**
      * @brief m_regType The region type of this AoI.
      */
      RegionType m_regType;

      /**
       * @brief m_wbcPresent To indicate whether a WBC is present in
       * this AoI or not.
       */
      bool m_wbcPresent;

      /**
       * @brief m_typicalZ Reference focus value of this AoI.
       */
      qreal m_typicalZ;

      /**
       * @brief m_procType Indicates what type of processing needs to be
       * performed in the AoI.
       */
      ProcessType m_procType;

      /**
       * @brief m_mag Magnification at which the AoI will be acquired.
       */
      Magnification m_mag;

      /**
       * @brief m_zStack Container to store z-stack of images.
       */
      QVector <unsigned char*> m_zStack;

      /**
       * @brief m_colorMetric To store the color metric of the image.
       */
      double m_colorMetric;

      /**
       * @brief m_bgState To store the background state of the AoI.
       * True indicates that it is a background and false indicates that it is
       * a foreground.
       */
      bool m_bgState;

      /**
       * @brief m_bestIdx To store the index of the best focused image in the
       * stack.
       */
      int m_bestIdx;

    };// end of AoIInfo structure.

    /**
     * @brief The GridInfo struct is used to store information of a grid
     * that is available for acquisition.
     */
    struct GridInfo
    {
      /**
       * @brief m_id Unique ID of the grid.
       */
      int m_id;

      /**
       * @brief m_name Name of the grid.
       */
      QString m_name;

      /**
       * @brief m_rowCount No. of rows in the grid.
       */
      int m_rowCount;

      /**
       * @brief m_colCount No. of columns in the grid.
       */
      int m_colCount;

      /**
       * @brief m_bestFocus Stores the best focus of the grid for
       * 20x magnification.
       */
      qreal m_bestFocus;

      /**
       * @brief m_widthOverlap Overlap in width between AoIs in the grid in
       * percentage pixels.
       */
      qreal m_widthOverlap;

      /**
       * @brief m_heightOverlap Overlap in height between AoIs in the grid in
       * percentage pixels.
       */
      qreal m_heightOverlap;

      /**
       * @brief m_coord Coordinates of the grid in pixels.
       */
      QRectF m_coord;

      /**
       * @brief m_mag Magnification at which the grid will be acquired.
       */
      Magnification m_mag;

      /**
       * @brief m_pattern To store the acquisition scan pattern of the grid.
       */
      ScanPattern m_pattern;

      /**
       * @brief m_direction To store the primary acquisition scan direction of
       * the grid.
       */
      ScanDirection m_direction;

      /**
       * @brief m_aoiList List of AoIs in the grid.
       */
      QList <AoIInfo> m_aoiList;

    };// end of GridInfo structure.

    /**
     * @brief The DispEstParams struct is used to store the parameters used by
     * the stitching pipeline for displacement estimation.
     */
    struct DispEstParams
    {
      /**
       * @brief m_channel To store the channel no.
       */
      int m_channel;

      /**
       * @brief m_subsample To store the subsampling value.
       */
      int m_subsample;

      /**
       * @brief m_estDisp1X To store the estimated displacement value in
       * x-direction.
       */
      qreal m_estDisp1X;

      /**
       * @brief m_estDisp1Y To store the estimated displacement value in
       * y-direction.
       */
      qreal m_estDisp1Y;

      /**
       * @brief m_estDisp1X To store the estimated displacement value in
       * x-direction.
       */
      qreal m_estDisp2X;

      /**
       * @brief m_estDisp1Y To store the estimated displacement value in
       * y-direction.
       */
      qreal m_estDisp2Y;

      /**
       * @brief m_estDisp1Y To store the tolerance in the displacement value in
       * x-direction.
       */
      qreal m_tolDisp1X;

      /**
       * @brief m_estDisp1Y To store the tolerance in the displacement value in
       * y-direction.
       */
      qreal m_tolDisp1Y;

      /**
       * @brief m_estDisp1Y To store the tolerance in the displacement value in
       * x-direction.
       */
      qreal m_tolDisp2X;

      /**
       * @brief m_estDisp1Y To store the tolerance in the displacement value in
       * y-direction.
       */
      qreal m_tolDisp2Y;

      /**
       * @brief m_minCC To store the minimum connected components value.
       */
      int m_minCC;

    };// end of DispEstParams structure.

    /**
     * @brief The SlotParams struct To store the parameters of a slot.
     */
    struct SlotParams
    {
      /**
       * @brief m_slotNum To store the slot no.
       */
      int m_slotNum;

      /**
       * @brief m_region_1x To store the region required for cropping for
       * 1x localization.
       */
      QRectF m_region_1x;

      /**
       * @brief m_offsetX To store the offset in the x-direction.
       */
      qreal m_offsetX;

      /**
       * @brief m_offsetY To store the offset in the y-direction.
       */
      qreal m_offsetY;

      /**
       * @brief m_lowerLimitZ To store the lower limit value of the Z-stage.
       */
      qreal m_lowerLimitZ;

      /**
       * @brief m_upperLimitZ To store the upper limit value of the Z-Stage.
       */
      qreal m_upperLimitZ;

      /**
       * @brief m_lowerLimitZ_20x To store the lower limit value of the Z-stage
       * at 20x magnification.
       */
      qreal m_lowerLimitZ_20x;

      /**
       * @brief m_upperLimitZ_20x To store the upper limit value of the Z-stage
       * at 20x magnification.
       */
      qreal m_upperLimitZ_20x;

      /**
       * @brief m_stackLowerLimitZ_20x
       */
      qreal m_stackLowerLimitZ_20x;

      /**
       * @brief m_stackUpperLimitZ_20x
       */
      qreal m_stackUpperLimitZ_20x;

      /**
       * @brief m_stackLowerLimitZ_40x
       */
      qreal m_stackLowerLimitZ_40x;

      /**
       * @brief m_stackUpperLimitZ_40x
       */
      qreal m_stackUpperLimitZ_40x;

      /**
       * @brief m_stackLowerLimitZ_100x To store the lower Z-stack limit at
       * 100x magnification.
       */
      qreal m_stackLowerLimitZ_100x;

      /**
       * @brief m_stackUpperLimitZ_100x To store the upper Z-stack limit at
       * 100x magnification.
       */
      qreal m_stackUpperLimitZ_100x;

      /**
       * @brief m_offsetZ_20x_40x To store the Z-stage offset mapping from 20x
       * to 40x magnification.
       */
      qreal m_offsetZ_20x_40x;

      /**
       * @brief m_offsetZ_20x_100x To store the Z-stage offset mapping from 20x
       * to 100x magnification.
       */
      qreal m_offsetZ_20x_100x;

      /**
       * @brief m_offsetZ_40x_100x To storethe Z-stage offset mapping from 40x
       * to 100x mapping.
       */
      qreal m_offsetZ_40x_100x;

      /**
       * @brief m_dispEstParams To store the displacement estimation parameters
       * for the slot.
       */
      DispEstParams m_dispEstParams;
    };// end of SlotParams structure.

  } // end of acquisition namespace.
} // end of spin namespace.

#endif // ACQUISITIONSTRUCTURES_H
