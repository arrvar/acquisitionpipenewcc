#include <QRectF>
#include <QThread>
#include <QElapsedTimer>
#include <QDebug>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionPipeline.h"
#include "Common/Framework/HardwareManager/Acquisition/CaptureWorker.h"
#include "Common/Framework/HardwareManager/Acquisition/FocusMapWorker.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"

#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/ImageProcessingWorker.h"
#include "Common/Framework/HardwareManager/Utils/ImageMonitoringWorker.h"
#include "Common/Framework/HardwareManager/Utils/ImageWriterWorker.h"
#include "Common/Framework/HardwareManager/Utils/Types.h"

namespace spin
{
  namespace acquisition
  {
    /**
     * @brief AcquisitionPipeline Default class constructor, initializes
     * objects owned by the class.
     * @param slideName Name of the slide for acquiring.
     * @param gridName Name of the grid for acquiring.
     * @param parent Reference to a parent QObject class.
     */
    AcquisitionPipeline::AcquisitionPipeline(QString slideName,
                                             QString gridName,
                                             QObject* parent) :
      AbstractAcquisition(slideName, gridName, parent),
      m_captureHeightinMic(0),
      m_captureWidthinMic(0),
      m_captureHeightinSteps(0),
      m_captureWidthinSteps(0),
      m_rowWidthinMic(0),
      m_colHeightinMic(0),
      m_acqIdx(0),
      m_acqCount(0),
      m_imgCount(0),
      m_offset(0)
    {
      setupConnections();

      setupCaptureWorker();

      setupFocusMapWorker();

      if(CONFIG()->intConfig(ConfigKeys::WRITE_IMAGES))
      {
        setupWriterWorker();
      }

      if(CONFIG()->intConfig(ConfigKeys::ENABLE_STITCHING))
      {
        setupProcessWorker();
      }
    }

    /**
     * @brief setupConnections To setup signal and slot connections for this
     * class.
     */
    void AcquisitionPipeline::setupConnections()
    {
      connect(this, &AcquisitionPipeline::acquire,
              this, &AcquisitionPipeline::onAcquire);

      connect(this, &AcquisitionPipeline::acquisitionFinished,
              this, &AcquisitionPipeline::onAcquisitionFinished);
    }

    /**
     * @brief setupCaptureWorker To setup the worker class for capturing
     * AoIs.
     */
    void AcquisitionPipeline::setupCaptureWorker()
    {
      m_capThread = new QThread();

      m_capWorker = new CaptureWorker();
      m_capWorker->moveToThread(m_capThread);

      connect(m_capThread, &QThread::started,
              m_capWorker, &CaptureWorker::onStartCapture);

      connect(m_capWorker, &CaptureWorker::captureFinished,
          this, &AcquisitionPipeline::onAcquired);

      connect(m_capWorker, &CaptureWorker::captureError,
              this, &AcquisitionPipeline::onCaptureError);
    }

    /**
     * @brief setupProcessWorker To setup the worker class for processing
     * captured images.
     */
    void AcquisitionPipeline::setupProcessWorker()
    {
      m_imgProcThread = new QThread();

      m_imgProcWorker = new ImageProcessingWorker(m_slideName, m_gridName);
      m_imgProcWorker->moveToThread(m_imgProcThread);

      connect(m_imgProcThread, &QThread::started,
              m_imgProcWorker, &ImageProcessingWorker::onStartWorker);

      connect(this, &AcquisitionPipeline::destroyed,
              m_imgProcThread, &QThread::deleteLater);

      connect(this, &AcquisitionPipeline::destroyed,
              m_imgProcWorker, &QThread::deleteLater);
    }

    /**
     * @brief setupFocusMapWorker To setup the worker class for creating
     * focus map
     */
    void AcquisitionPipeline::setupFocusMapWorker()
    {
      m_FocusMapWorker = new FocusMapWorker();
    }

    /**
     * @brief onAcquire Slot that will be called for acquiring a row/column
     * in a grid.
     */
    void AcquisitionPipeline::onAcquire()
    {
      if((m_acqIdx < m_acqCount) && (m_writeError == false))
      {
       // SPIN_LOG_DEBUG(QObject::tr("Acquiring %1 %2 of %3").
       //                arg(m_acqStr).arg(m_acqIdx + 1).arg(m_acqCount));

        // Retrieve the XY-Stage and Z-Stage positions for the first AoI of the
        // current row/column.
        int startIdx = m_acqIdx * m_imgCount;
        int stopIdx = m_imgCount;
        QList <AoIInfo> aoiInfoList = m_gridInfo.m_aoiList.mid(startIdx,
                                                               stopIdx);

        AoIInfo firstAoI = aoiInfoList.first();
        AoIInfo lastAoI = aoiInfoList.last();

        // Convert from pixels to microns.
        QPointF firstXYPosInMic = Utilities::pixelsToMicrons(firstAoI.m_pos);
        QPointF lastXYPosInMic = Utilities::pixelsToMicrons(lastAoI.m_pos);

        // Move the XY-Stage to the next start position.
        double xStartPos = 0.0;
        double yStartPos = 0.0;

        //SPIN_LOG_DEBUG(QObject::tr("X firstXYPosInMic %1 , Y firstXYPosInMic %2, X lastXYPosInMic %3, Y lastXYPosInMic %4").
        //               arg(firstXYPosInMic.x()).
        //               arg(firstXYPosInMic.y()).
        //               arg(lastXYPosInMic.x()).
        //               arg(lastXYPosInMic.y()));

        // Scanning in the Horizontal direction
        if(m_gridInfo.m_direction == SCANDIR_HORIZONTAL)
        {
          // Scanning in ZIGZAG mode
          if(m_gridInfo.m_pattern == SCANPATT_ZIG_ZAG)
          {
            // Starting location of each row
            xStartPos = firstXYPosInMic.x() - m_captureWidthinMic;
          }
          // Scanning in Serpentine mode
          else if(m_gridInfo.m_pattern == SCANPATT_SERPENTINE)
          {
            // If row Idx is ODD
            if(m_acqIdx % 2 == 0)
            {
              xStartPos = firstXYPosInMic.x() - m_captureWidthinMic;
            }
            // If row IDX is even
            else
            {
              xStartPos = lastXYPosInMic.x() + m_captureWidthinMic;
            }
          }
          yStartPos = firstXYPosInMic.y();
        }
        else
        {
            xStartPos = firstXYPosInMic.x();
          if(m_gridInfo.m_pattern == SCANPATT_ZIG_ZAG)
          {
            yStartPos = firstXYPosInMic.y() - m_captureHeightinMic;
          }
          else if(m_gridInfo.m_pattern == SCANPATT_SERPENTINE)
          {
            if(m_acqIdx % 2 == 0)
            {
              yStartPos = firstXYPosInMic.y() - m_captureHeightinMic;
            }
            else
            {
              yStartPos = lastXYPosInMic.y() + m_captureHeightinMic;
            }
          }
        }

        double zStartPos = firstAoI.m_zVal;

//        SPIN_LOG_DEBUG(QObject::tr("Before X Start %1 , Y Start %2").
//                       arg(xStartPos).
//                       arg(yStartPos));
        // Set the stage to the original speed.
        HW_MANAGER()->setXYStageSpeed(CONFIG()->realConfig(ConfigKeys::
                                                           MAX_XY_SPEED));

        // Set the Z-Stage for the first location in the column.
        // And wait till it reaches its position.
        HW_MANAGER()->setZPosition(zStartPos, true);

        // Move the XY-Stage to the start position.
        HW_MANAGER()->setXYPosition(xStartPos, yStartPos, true);

        // Enable the trigger mode.
        if(m_gridInfo.m_direction == SCANDIR_HORIZONTAL)
        {
          HW_MANAGER()->enableXYTriggerMode(m_captureWidthinSteps, X_TRIGGER);
        }
        else
        {
          HW_MANAGER()->enableXYTriggerMode(m_captureHeightinSteps, Y_TRIGGER);
        }

        if(CONFIG()->intConfig(ConfigKeys::COLUMN_ZSTACK_MODE))
        {
          // Retrieve the focus values for the current column.
          QString zStackStr;
          QString zStackTable;
          QString zFgBgMap;

          QStringList zStackStrList;
          QStringList zStackTableList;

          m_finalZList = m_FocusMapWorker->getFocusMap(aoiInfoList);
          int idx = 0;
          m_usePreZ = CONFIG()->intConfig(ConfigKeys::INLINE_FM_CALC);
          double stepJump =
              CONFIG()->realConfig(ConfigKeys::XY_Z_TRIGGER_STEP_SIZE);
          int stackSize =
              CONFIG()->intConfig(ConfigKeys::XY_Z_TRIGGER_STACK_SIZE);

          Q_FOREACH(AoIInfo aoiInfo, aoiInfoList)
          {
            // Define best z value
            double bestZ;
            if(m_usePreZ == 1)
            {
              bestZ = m_finalZList[idx].toDouble();
            }
            else
            {
              bestZ = aoiInfo.m_zVal;
            }
            SPIN_LOG_DEBUG(QObject::tr("Final Z value %1: ").
                           arg(bestZ));
            // It is a background.
            if(aoiInfo.m_bgState == true)
            {
              // To disable on the Controller side.
              zStackTableList.append("1");
              zStackStrList.append(QString::number(bestZ - m_offset,
                                                   'g', 8));
            }
            else
            {
               zStackTableList.append("0");
               zStackStrList.append(QString::number(bestZ,
                                                    'g', 8));
               //zStackStrList.append(QString::number(bestZ - 2*stepJump,
               //                                     'g', 8));
               aoiInfoList[idx].m_zStack.resize(stackSize);
               aoiInfoList[idx].m_zStack.fill(Q_NULLPTR);
            }
            idx++;
          }

          // dilate fg by some aois
          int dilateBy = 1;
          if(m_gridInfo.m_mag == 1)
          {
            dilateBy = 1;
          }
          else
          {
            dilateBy = 2;
          }
          // SPIN_LOG_DEBUG(QObject::tr("Dilate: %1").arg(dilateBy));
          for(int idx = 1; idx < zStackTableList.size()-1; idx++)
          {
            if(zStackTableList.at(idx).toInt() == 0)
            {
              if(zStackTableList.at(idx-1).toInt() == 1)
              {
                int startIDX = std::max(0, idx-dilateBy);
                int endIDX = std::min(startIDX+dilateBy,zStackTableList.size()-1);
                for(int IDX = startIDX; IDX < endIDX; IDX++)
                {
                  if(zStackTableList[IDX].toInt()==1)
                  {
                    double bestZ = zStackStrList[IDX].toDouble();
                    zStackTableList[IDX] = QString::number(0, 'g', 8);
                    zStackStrList[IDX] = (QString::number(bestZ + m_offset,'g', 8));
                    //zStackStrList[IDX] = (QString::number(bestZ + m_offset - 2*stepJump,
                    //                                            'g', 8));
                    aoiInfoList[IDX].m_zStack.resize(stackSize);
                    aoiInfoList[IDX].m_zStack.fill(Q_NULLPTR);
                  }
                  else
                  {
                    break;
                  }
                }
              }
              if(zStackTableList.at(idx+1).toInt() == 1)
              {
                int startIDX = idx+1;
                int endIDX = std::min(startIDX+dilateBy, zStackTableList.size()-1);
                for(int IDX = startIDX; IDX < endIDX; IDX++)
                {
                  idx = idx+1;
                  if(zStackTableList[IDX].toInt()==1)
                  {
                    double bestZ = zStackStrList[IDX].toDouble();
                    zStackTableList[IDX] = QString::number(0, 'g', 8);
                    zStackStrList[IDX] = (QString::number(bestZ + m_offset,'g', 8));
                    //zStackStrList[IDX] = (QString::number(bestZ + m_offset - 2*stepJump,
                    //                                      'g', 8));
                    aoiInfoList[IDX].m_zStack.resize(stackSize);
                    aoiInfoList[IDX].m_zStack.fill(Q_NULLPTR);
                  }
                  else
                  {
                    break;
                  }
                }
              }
            }
          }

          zStackStr = zStackStrList.join(";");
          zStackTable = zStackTableList.join(";");
          zFgBgMap = zStackTableList.join(";");

          zStackTable = m_stackCountStr + ";" +
              QString::number(aoiInfoList.size()) + ";" +
              zStackTable;

          HW_MANAGER()->setZStack(zStackStr.toStdString(),
                                  zStackTable.toStdString(),
                                  zFgBgMap.toStdString());
        }

        m_capWorker->init(m_slideName, m_gridName, aoiInfoList);

        // Set the XY-Stage speed to the defined continuous speed.
        if(m_gridInfo.m_mag == MAG_20X)
        {
          HW_MANAGER()->setXYStageSpeed(CONFIG()->
                                        realConfig(ConfigKeys::
                                                   CONTINUOUS_SPEED_20X));
        }
        else if(m_gridInfo.m_mag == MAG_40X)
        {
          HW_MANAGER()->setXYStageSpeed(CONFIG()->
                                        realConfig(ConfigKeys::
                                                   CONTINUOUS_SPEED_40X));
        }

        // Start the snap thread.
        m_capThread->start(QThread::TimeCriticalPriority);

        // Move to the end of the row.
        // If it is Zig-Zag movement then it would be left to right.
        // If it is Serpentine movement then it would alternate from
        // left to right and right to left.
        double xStopPos = 0.0;
        double yStopPos = 0.0;

        if(m_gridInfo.m_direction == SCANDIR_HORIZONTAL)
        {
          if(m_gridInfo.m_pattern == SCANPATT_ZIG_ZAG)
          {
            xStopPos = lastXYPosInMic.x();
          }
          else if(m_gridInfo.m_pattern == SCANPATT_SERPENTINE)
          {
            if(m_acqIdx % 2 == 0)
            {
              xStopPos = lastXYPosInMic.x();
            }
            else
            {
              xStopPos = firstXYPosInMic.x();
            }
          }
          yStopPos = lastXYPosInMic.y();
        }
        else
        {
          xStopPos = lastXYPosInMic.x();
          if(m_gridInfo.m_pattern == SCANPATT_ZIG_ZAG)
          {
            yStopPos = lastXYPosInMic.y();
          }
          else if(m_gridInfo.m_pattern == SCANPATT_SERPENTINE)
          {
            if(m_acqIdx % 2 == 0)
            {
              yStopPos = lastXYPosInMic.y();
            }
            else
            {
              yStopPos = firstXYPosInMic.y();
            }
          }
          }

//        SPIN_LOG_DEBUG(QObject::tr(" After X Stop %3, Y Stop %4").
//                       arg(xStopPos).
//                       arg(yStopPos));

        // Should not check for device busy state. For the stage to
        // move continuously.
        HW_MANAGER()->setXYPosition(xStopPos, yStopPos, false);
      }
      else
      {
        SPIN_LOG_DEBUG(QObject::tr("Total acquisition time (s) for %1 "
                                   "images: %2").
                       arg(m_gridInfo.m_rowCount * m_gridInfo.m_colCount).
                       arg(m_acqTimer.elapsed() / 1000.0));
        // Once all the columns have been acquired, set the speed back to
        // normal and stop camera continuous mode.
        HW_MANAGER()->setXYStageSpeed(m_normalSpeed);
        HW_MANAGER()->stopContinuousMode();
        HW_MANAGER()->disableZBacklash();

        Q_EMIT acquisitionFinished();
      }
    }

    /**
     * @brief onAcquired Slot called when a row/column has been acquired.
     */
    void AcquisitionPipeline::onAcquired()
    {
      SPIN_LOG_DEBUG(QObject::tr("Acquired %1 %2 of %3").
                     arg(m_acqStr).
                     arg(m_acqIdx + 1).
                     arg(m_acqCount));

      // Set the XY-Stage trigger mode to OFF.
      HW_MANAGER()->disableTriggerMode();

      // Update the row/column index.
      m_acqIdx++;

      QElapsedTimer capThreadTimer;
      capThreadTimer.start();
      // Wait till the capture thread comes to a halt.
      while (m_capThread && m_capThread->isRunning())
      {
        // Do nothing.
      }
      qreal capThreadTime = capThreadTimer.elapsed() / 1000.0;
      capThreadTimer.invalidate();
      SPIN_LOG_DEBUG(QObject::tr("Capture thread wait time (ms): %1").
                     arg(QString::number(capThreadTime)));
#if 1
      QList <AoIInfo> aoiInfoList;
      m_capWorker->getAoIInfoList(aoiInfoList);

      // Get best z for previous row
      m_FocusMapWorker->init(m_offset, m_finalZList, aoiInfoList);

      QThread* imgMonThread = new QThread();

      ImageMonitoringWorker* imgMonWorker = new ImageMonitoringWorker();
      imgMonWorker->initWorker(aoiInfoList);
      aoiInfoList.clear();

      imgMonWorker->moveToThread(imgMonThread);

      connect(imgMonThread, &QThread::started,
              imgMonWorker, &ImageMonitoringWorker::onStartWorker);
      connect(imgMonWorker, &ImageMonitoringWorker::workerFinished,
              this, &AcquisitionPipeline::onMonitorFinished);
      connect(imgMonThread, &QThread::finished,
              imgMonWorker, &QObject::deleteLater);
      connect(imgMonThread, &QThread::finished,
              imgMonThread, &QObject::deleteLater);

      imgMonThread->start(QThread::HighestPriority);
#endif
      // Acquire next row/column.
      Q_EMIT acquire();
    }

    /**
     * @brief onCaptureError Slot called when an error occurs during
     * capturing.
     * @param errMsg Error message.
     */
    void AcquisitionPipeline::onCaptureError(QString errMsg)
    {
      SPIN_LOG_ERROR(errMsg);

      // Set the XY-Stage trigger mode to OFF.
      HW_MANAGER()->disableTriggerMode();

      // Stop the XY-Stage.
      HW_MANAGER()->stopXYStage();

      QElapsedTimer capThreadTimer;
      capThreadTimer.start();
      // Wait till the capture thread comes to a halt.
      while (m_capThread && m_capThread->isRunning())
      {
        // Do nothing.
      }
      qreal capThreadTime = capThreadTimer.elapsed() / 1000.0;
      capThreadTimer.invalidate();
      SPIN_LOG_DEBUG(QObject::tr("Capture thread wait time (ms): %1").
                     arg(QString::number(capThreadTime)));

      // Start row/column acquisition.
      Q_EMIT acquire();
    }

    /**
     * @brief onMonitorFinished To write the focus metric values of the
     * processed AoIs to the database.
     * @param aoiIdList List of AoI Ids.
     * @param bestIdxList List of best indices for each AoI.
     * @param focusMetricList List of focus metric values.
     * @param colorMetricList List of color metric values.
     */
    void AcquisitionPipeline::onMonitorFinished(QList <int> aoiIdList,
                                                QList <int> bestIdxList,
                                                QList <double> focusMetricList,
                                                QList <double> colorMetricList)
    {
      int atIdx = m_aoiIdList.size();
      int stackSize = CONFIG()->intConfig(ConfigKeys::XY_Z_TRIGGER_STACK_SIZE);
      double stepSize = CONFIG()->realConfig(ConfigKeys::XY_Z_TRIGGER_STEP_SIZE);

      for(int idx = 0; idx < aoiIdList.size(); idx++)
      {
        m_aoiIdList.append(aoiIdList.at(idx));
        m_bestIdxList.append(bestIdxList.at(idx));
        if(bestIdxList.at(idx) == -1)
        {
          m_stackPresentList.append(0);
          double stackBestZ;
          if(m_usePreZ == 1)
          {
            stackBestZ = m_finalZList[idx].toDouble();
          }
          else
          {
            stackBestZ = m_gridInfo.m_aoiList[atIdx+idx].m_zVal - m_offset;
          }
          m_bestZList.append(stackBestZ);
        }
        else
        {
          m_stackPresentList.append(1);
          // Assuming that the stack size is always odd number
          int steps = bestIdxList[idx] - (stackSize - 1)/2;
          //int steps = bestIdxList[idx] - (stackSize - 1)/2 - 2;
          double stackBestZ;
          if(m_usePreZ == 1)
          {
            stackBestZ = m_finalZList[idx].toDouble();
          }
          else
          {
            stackBestZ = m_gridInfo.m_aoiList[atIdx+idx].m_zVal + steps*stepSize;
          }
          m_bestZList.append(stackBestZ);
        }
        m_aoiFocusMetricList.append(focusMetricList.at(idx));
        m_aoiColorMetricList.append(colorMetricList.at(idx));
      }
    }

    /**
     * @brief onAcquisitionFinished Slot called when acquisition finishes.
     */
    void AcquisitionPipeline::onAcquisitionFinished()
    {
      if(m_writerThread)
      {
        // Stop the writer thread and wait till it exits.
        m_writerWorker->onStopWorker();
        QElapsedTimer writerThreadTimer;
        writerThreadTimer.start();
        // Wait till the writer thread comes to a halt.
        while (m_writerThread->isRunning())
        {
          // Do nothing.
        }
        qreal writerThreadTime = writerThreadTimer.elapsed() / 1000.0;
        writerThreadTimer.invalidate();
        SPIN_LOG_DEBUG(QObject::tr("Writer thread wait time (ms): %1").
                       arg(QString::number(writerThreadTime)));

        if(m_writeError == true)
        {
          // Flush out remaining data if any.
          while(SHARED_WRITE_BUFFER()->isEmpty() == false)
          {
            void* _vPtr = SHARED_WRITE_BUFFER()->get();
            acquisition::AoIInfo* aoiInfo =
                static_cast<acquisition::AoIInfo* >(_vPtr);
            aoiInfo->m_img.release();
            delete aoiInfo;
          }
        }
      }

      if(m_imgProcThread)
      {
        // Stop the image processing thread and wait till it exits.
        if(m_writeError == true)
        {
          m_imgProcWorker->onAbortWorker();
        }
        else
        {
          m_imgProcWorker->onStopWorker();
        }

        QElapsedTimer procThreadTimer;
        procThreadTimer.start();
        // Wait till the writer thread comes to a halt.
        while (m_imgProcThread->isRunning())
        {
          // Do nothing.
        }
        qreal procThreadTime = procThreadTimer.elapsed() / 1000.0;
        procThreadTimer.invalidate();
        SPIN_LOG_DEBUG(QObject::tr("Process thread wait time (ms): %1").
                       arg(QString::number(procThreadTime)));
      }

      if(m_aoiIdList.isEmpty() == false)
      {
        // Update the focus metric of the image in the database.
        QElapsedTimer dbTimer;
        dbTimer.start();
        DB_MANAGER()->updateAoIFocusMetric(m_aoiIdList, m_aoiFocusMetricList,
                                           m_aoiColorMetricList, m_stackPresentList,
                                           m_bestIdxList, m_bestZList);
        qreal dbUpdateTime = dbTimer.elapsed() / 1000.0;
        dbTimer.invalidate();
        SPIN_LOG_DEBUG(QObject::tr("Database update time (ms): %1").
                       arg(QString::number(dbUpdateTime)));
      }

      Q_EMIT processFinished(m_slideName, m_gridName, m_writeError);
    }

    /**
     * @brief init To initialize the acquisition for a grid of a slide.
     * @return Returns true if the initialization is successful else returns
     * false.
     */
    bool AcquisitionPipeline::init(SpecimenType specType)
    {
      // Register the argument as a meta type for signals to work.
      qRegisterMetaType< QList<AoIInfo> >( "QList<AoIInfo>" );

      // Retrieve and store the grid information from the database.
      if(!DB_MANAGER()->getGridInfo(m_gridInfo, m_gridName))
      {
        return false;
      }

      m_specType = specType;

      double zStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::
                                                       STAGE_STEP_SIZE_Z);
      double zMicrostepping = CONFIG()->realConfig(ConfigKeys::
                                                   STAGE_MICROSTEPPING_Z);

      int zOffsetSteps = CONFIG()->intConfig(ConfigKeys::Z_OFFSET_STEPS);
      double zOffsetInMicrons = (zOffsetSteps * zStepSizeInMicrons /
                                 zMicrostepping);

      // Set the processing type for all the AoIs to displacement esitmation
      // mode. This is specific to continuous acquisition.
      QString magStr = Utilities::magnificationString(m_gridInfo.m_mag);
      QString path = m_slideName + "/" + m_gridName + "/1/" + magStr +"/";
//      QString stackPath = m_slideName + "/" + m_gridName + "/image_stack";
      QString stackPath = "/home/adminspin/stack_images/" + m_slideName + "/" +
          m_gridName + "/image_stack";

      int stackSize = CONFIG()->intConfig(ConfigKeys::XY_Z_TRIGGER_STACK_SIZE);
      for(int idx = 0; idx < m_gridInfo.m_aoiList.size(); idx++)
      {
        m_gridInfo.m_aoiList[idx].m_procType = DISP_EST;
        QString aoiPath = FileSystemUtil::
                getPathFromWorkspacePath(path + m_gridInfo.m_aoiList.
                                         at(idx).m_name + ".bmp");
        m_gridInfo.m_aoiList[idx].m_destPath = aoiPath;
//        m_gridInfo.m_aoiList[idx].m_stackDestPath = FileSystemUtil::
//            getPathFromWorkspacePath(stackPath);
        m_gridInfo.m_aoiList[idx].m_stackDestPath = stackPath;
        m_gridInfo.m_aoiList[idx].m_whiteCorr = false;
        m_gridInfo.m_aoiList[idx].m_zVal += zOffsetInMicrons;
        /*if(stackSize > 0 && m_gridInfo.m_aoiList[idx].m_bgState == false)
        {
          m_gridInfo.m_aoiList[idx].m_zStack.resize(stackSize);
          m_gridInfo.m_aoiList[idx].m_zStack.fill(Q_NULLPTR);
        }*/
      }

      // Compute and store the stack size and the step size for Z-Trigger
      // capture.
      double stackStepSizeInMicrons =
          CONFIG()->realConfig(ConfigKeys::XY_Z_TRIGGER_STEP_SIZE);
      int stackStepSizeInSteps = (stackStepSizeInMicrons * zMicrostepping /
                                  zStepSizeInMicrons);

      QStringList stackCountStrList;
      stackCountStrList.append(QString::number(stackSize));
      for(int stackIdx = 0; stackIdx < stackSize; stackIdx++)
      {
        stackCountStrList.append(QString::number(stackStepSizeInSteps));
      }
      m_stackCountStr = stackCountStrList.join(";");

      // Get the FoV size in microns.
      QSizeF fovSize = Utilities::getFovSizeMicrons(m_gridInfo.m_mag);
      qreal micronPerSteps = HW_MANAGER()->getXYStageStepSize();

      //Compute the offsets for stage movements.
      int tempX = (fovSize.width() *(1 - m_gridInfo.m_widthOverlap))/micronPerSteps;
      qreal triggerXinMic = (double)micronPerSteps * (double)tempX;
      int tempY = (fovSize.height() *(1 - m_gridInfo.m_heightOverlap))/micronPerSteps;
      qreal triggerYinMic = (double)micronPerSteps * (double)tempY;
      SPIN_LOG_INFO(QObject::tr("triggerXinMic: %1 triggerYinMic: %2").
                    arg(triggerXinMic).arg(triggerYinMic));
      
      // Get the trigger interval in microns.

      m_captureHeightinMic = triggerYinMic;
      m_captureWidthinMic = triggerXinMic;

      m_captureHeightinSteps = m_captureHeightinMic/micronPerSteps;
      m_captureWidthinSteps = m_captureWidthinMic/micronPerSteps;
      if(m_gridInfo.m_direction == SCANDIR_HORIZONTAL)
      {
        SPIN_LOG_DEBUG(QObject::tr("Capture width in steps: %1").
               arg(m_captureWidthinSteps));
      }
      else
      {
        SPIN_LOG_DEBUG(QObject::tr("Capture height in steps: %1").
               arg(m_captureHeightinSteps));

      }

      int totalAoiCount = m_gridInfo.m_rowCount * m_gridInfo.m_colCount;
      if(CONFIG()->intConfig(ConfigKeys::ENABLE_STITCHING))
      {
        if(m_imgProcWorker->init(totalAoiCount))
        {
          // Start the image processing thread.
          m_imgProcThread->start();
        }
        else
        {
          SPIN_LOG_ERROR("Unable to start the process thread.");
          return false;
        }
      }

      if(CONFIG()->intConfig(ConfigKeys::WRITE_IMAGES))
      {
        m_writerWorker->setTotalAoICount(totalAoiCount);
        if(m_writerThread)
        {
          // Start the writer thread.
          m_writerThread->start();
        }
        else
        {
          SPIN_LOG_ERROR("Unable to start the writer thread.");
          return false;
        }
      }

      return true;
    }

    /**
     * @brief process To start the acquisition of a grid of a slide.
     */
    void AcquisitionPipeline::process()
    {
      // Start the acquisition timer.
      m_acqTimer.start();

      HW_MANAGER()->enableZBacklash();

      m_colHeightinMic = m_captureHeightinMic * m_gridInfo.m_rowCount;
      m_rowWidthinMic = m_captureWidthinMic * m_gridInfo.m_colCount;

      // Store the current XY_Stage speed because the stage will be
      // moved at a different speed for continuous acquisition.
      m_normalSpeed = HW_MANAGER()->getXYStageSpeed();
      m_maxSpeed = CONFIG()->realConfig(spin::ConfigKeys::MAX_Z_STAGE_SPEED);
      // This speed will be used to set the Z-Stage during Z-Trigger.
      m_defSpeed = Utilities::getDefaultZStageSpeed(m_gridInfo.m_mag);
      HW_MANAGER()->setZStageSpeed(m_defSpeed);

      // Setup camera continuous capture.
      HW_MANAGER()->setContinuousMode(XY_TRIGGER);

      // Set the camera exposure for continouse mode acquisition.
      double camExpVal = Utilities::getCamExpValue(m_gridInfo.m_mag);
      HW_MANAGER()->setCameraExposure(camExpVal);

      if(m_gridInfo.m_direction == SCANDIR_HORIZONTAL)
      {
        m_acqStr = "row";
        m_acqCount = m_gridInfo.m_rowCount;
        m_imgCount = m_gridInfo.m_colCount;
      }
      else
      {
        m_acqStr = "column";
        m_acqCount = m_gridInfo.m_colCount;
        m_imgCount = m_gridInfo.m_rowCount;
      }

      Q_EMIT acquire();
    }

    /**
     * @brief ~AcquisitionPipeline Default class destructor, destroys objects
     * owned by the class.
     */
    AcquisitionPipeline::~AcquisitionPipeline()
    {
      if(m_capThread)
      {
        delete m_capThread;
      }

      if(m_capWorker)
      {
        delete m_capWorker;
      }

      if(m_writerThread)
      {
        delete m_writerThread;
      }

      if(m_writerWorker)
      {
        delete m_writerWorker;
      }

      if(m_FocusMapWorker)
      {
        delete m_FocusMapWorker;
      }
    }
  }
}
