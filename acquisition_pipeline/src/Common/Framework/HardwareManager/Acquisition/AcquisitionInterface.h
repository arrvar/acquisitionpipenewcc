#ifndef ACQUISITIONINTERFACE_H
#define ACQUISITIONINTERFACE_H

#include <QObject>

namespace spin
{
  namespace acquisition
  {
    // Forward declaration.
    class AbstractAcquisition;

    /**
     * @brief The AcquisitionInterface class is an interface to the acquisition
     * pipeline. This class will manage the instantiation of an acquisition
     * pipeline based on the specimen type and will also manage its lifetime.
     */
    class AcquisitionInterface : public QObject
    {
      Q_OBJECT
    public:
      /**
       * @brief AcquisitionInterface Initializes objects owned by the class.
       * @param parent Instance of the QObject parent.
       */
      explicit AcquisitionInterface(QObject* parent = 0);

      /**
       * @brief ~AcquisitionInterface Class destructor, destroys objects
       * owned by the class.
       */
      ~AcquisitionInterface();

      /**
       * @brief Initialize To initialize the acquisition interface class.
       * @param slotNum Slot number of the slide.
       * @param magType Magnification of selected for acquisition.
       * @param specimenType Type of the specimen being acquired.
       * @param slideName Name of the slide being acquired.
       * @param gridName Name of the grid being acquired.
       * @param py_interface To check if the acquisition is being as an
       * executable or as a python interface.
       * @return Returns true on successful initialization else returns false.
       */
      bool Initialize(int slotNum, int magType, int specimenType,
                      QString slideName, QString gridName,
                      bool py_interface = false);

      /**
       * @brief Process To start the corresponding acquisition process.
       */
      void Process();

    Q_SIGNALS:
      void quitProcess();

    private Q_SLOTS:
      /**
       * @brief onProcessFinished Slot called when the acquisition process is
       * finished.
       * @param slideName Name of the slide.
       * @param gridName Name of the grid.
       * @param writeError True indicates write error else no error.
       */
      void onProcessFinished(QString slideName, QString gridName,
                             bool writeError);

    private:
      /**
       * @brief m_py_interface True indicates that the acquisition is being
       * called from a python interface and false indicates that it is being
       * called as an executable.
       */
      bool m_py_interface;

      /**
       * @brief m_acqPipeline Instance of the acquisition pipeline.
       */
      AbstractAcquisition* m_acqPipeline;

    }; // end of AcquisitionInterface class.
  } // end of acquisition namespace.
} // end of spin namespace.

#endif // ACQUISITIONINTERFACE_H
