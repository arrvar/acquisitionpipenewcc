#ifndef ACQZSTACK_H
#define ACQZSTACK_H

#include "Common/Framework/HardwareManager/Acquisition/AbstractAcquisition.h"

namespace spin
{
  // Forward declaration.
  class ImageProcessingWorker;

  namespace acquisition
  {
    // Forward declaration.
    class ZStackCaptureWorker;

    /**
     * @brief The AcqZStack class is reponsible for the Z-stack acquisition
     * of AoIs.
     */
    class AcqZStack : public AbstractAcquisition
    {
      Q_OBJECT

    public:
      /**
       * @brief AcqZStack Class constructor, initializes objects owned by the
       * class.
       * @param slideName Name of the slide being acquired.
       * @param gridName Name of the grid being acquired.
       * @param parent Instance of the parent QObject.
       */
      AcqZStack(QString slideName,
                QString gridName,
                QObject* parent = 0);

      /**
       * @brief ~AcqZStack Default class destructor, destroys objects owned
       * by the class.
       */
      virtual ~AcqZStack();

      /**
       * @brief init To initialize the acquisition process.
       * @param specType Specimen type being acquired.
       * @return Returns true on successful initialization.
       */
      bool init(SpecimenType specType);

      /**
       * @brief process To start the acquisition process.
       */
      void process();

    Q_SIGNALS:
      /**
       * @brief Signal emitted to acquire an AoI.
       */
      void acquireAoI();

    private Q_SLOTS:
      /**
       * @brief onAcquireAoI Slot called to acquire an AoI.
       */
      void onAcquireAoI();

      /**
       * @brief onAoIAcquired Slot called when an AoI has been acquired.
       * @param zValue Focus value of the acquired AoI.
       */
      void onAoIAcquired();

      /**
       * @brief onCaptureError Slot called when an error occurs during
       * capturing.
       */
      void onCaptureError(QString errMsg);

      /**
       * @brief onMonitorFinished To write the focus metric values of the
       * processed AoIs to the database.
       * @param aoiIdList List of AoI Ids.
       * @param bestIdxList List of best indices for each AoI.
       * @param focusMetricList List of focus metric values.
       * @param colorMetricList List of color metric values.
       */
      void onMonitorFinished(QList <int> aoiIdList,
                             QList <int> bestIdxList,
                             QList <double> focusMetricList,
                             QList <double> colorMetricList);

      /**
       * @brief onAcquisitionFinished Slot called when the acquisition for the
       * slide finishes.
       */
      void onAcquisitionFinished();

    private:
      /**
       * @brief setupConnections To setup signal and slot connections for this
       * class.
       */
      void setupConnections();

      /**
       * @brief setupCaptureWorker To setup the worker class for capturing
       * AoIs.
       */
      void setupCaptureWorker();

      /**
       * @brief setupStackProcessWorker To setup the stack processing worker.
       */
      void setupProcessWorker();

    private:
      /**
       * @brief m_aoiIdx To store the index of the AoI that is currently being
       * captured.
       */
      int m_aoiIdx;

      /**
       * @brief m_refFocus To store the reference Z-Stage position.
       */
      double m_refFocus;

      /**
       * @brief m_zStepSize To store the step size for capturing a Z-stack.
       */
      double m_zStepSize;

      /**
       * @brief m_zStackLowerLimit To store the lower limit position of the
       * Z-Stage for captureing a stack.
       */
      double m_zStackLowerLimit;

      /**
       * @brief m_zStackUpperLimit To store the upper limit position of the
       * Z-Stage for capturing a stack.
       */
      double m_zStackUpperLimit;

      /**
       * @brief m_zCaptureSpeed Z-Stage speed during stack capture in mm/s.
       */
      double m_zCaptureSpeed;

      /**
       * @brief m_capWorker Pointer to the Z-Stage capture worker object.
       */
      QPointer <ZStackCaptureWorker> m_capWorker;

      /**
       * @brief m_imgProcWorker Pointer to the stack processing worker object.
       */
      QPointer <ImageProcessingWorker> m_imgProcWorker;

      /**
       * @brief m_imgProcThread Pointer to the image processing thread object.
       */
      QPointer <QThread> m_imgProcThread;

    };// end of AcqZStack class.
  } // end of acquisition namespace.
} // end of spin namespace.

#endif // ACQZSTACK_H
