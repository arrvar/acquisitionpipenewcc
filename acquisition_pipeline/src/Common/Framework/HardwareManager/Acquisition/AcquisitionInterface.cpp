#include <QCoreApplication>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionInterface.h"
#include "Common/Framework/HardwareManager/Acquisition/AcquisitionFactory.h"
#include "Common/Framework/HardwareManager/Acquisition/AbstractAcquisition.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"

#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"

namespace spin
{
  namespace acquisition
  {
    /**
     * @brief AcquisitionInterface Initializes objects owned by the class.
     * @param parent Instance of the QObject parent.
     */
    AcquisitionInterface::AcquisitionInterface(QObject* parent) :
      QObject(parent),
      m_py_interface(false),
      m_acqPipeline(Q_NULLPTR)
    {

    }

    /**
     * @brief onProcessFinished Slot called when the acquisition process is
     * finished.
     */
    void AcquisitionInterface::onProcessFinished(QString slideName,
                                                 QString gridName,
                                                 bool writeError)
    {
      // Disconnect from the slide database.
      DB_MANAGER()->disconnect();

      if(m_py_interface == false)
      {
        // Disconnect from the hardware.
        HW_MANAGER()->disconnect();
      }

      delete m_acqPipeline;

      // Write the status of the acquisition to a text file.
      QString path = slideName + "/" + gridName + "/";
      QString statusFilePath = FileSystemUtil::
              getPathFromWorkspacePath(path + "acq_post_proc.txt");
      if(writeError == false)
      {
        Utilities::writeStatus(statusFilePath, "completed:acquistion");
      }
      else
      {
        Utilities::writeStatus(statusFilePath, "error:write_error");
      }

      Q_EMIT quitProcess();
    }

    /**
     * @brief Process To start the corresponding acquisition process.
     */
    void AcquisitionInterface::Process()
    {
      m_acqPipeline->process();
    }

    /**
     * @brief Initialize To initialize the acquisition interface class.
     * @param slotNum Slot number of the slide.
     * @param magType Magnification of selected for acquisition.
     * @param specimenType Type of the specimen being acquired.
     * @param slideName Name of the slide being acquired.
     * @param gridName Name of the grid being acquired.
     * @param py_interface To check if the acquisition is being as an
     * executable or as a python interface.
     * @return Returns true on successful initialization else returns false.
     */
    bool AcquisitionInterface::Initialize(int slotNum, int magType,
                                          int specimenType,
                                          QString slideName,
                                          QString gridName,
                                          bool py_interface)
    {
      QString errMsg = "";
      m_py_interface = py_interface;
      if(m_py_interface == true)
      {
        // Connect to the slide database.
        if(DB_MANAGER()->connect(slideName))
        {
          m_acqPipeline = spin::acquisition::AcquisitionFactory::
                  getPipeline(slideName, gridName,
                              (spin::SpecimenType)specimenType);
          connect(m_acqPipeline, &AbstractAcquisition::processFinished,
                  this, &AcquisitionInterface::onProcessFinished);

          // Initialize the acquisition pipeline.
          bool ret = m_acqPipeline->init((spin::SpecimenType)specimenType);
          if(ret)
          {
            return true;
          }
          else
          {
            errMsg = "failed to initialize acquisition pipeline";
          }
        }
        else
        {
          errMsg = "failed to connect to slideDb.db";
        }
      }
      else
      {
        // Load the slot parameters.
        if(Utilities::loadSlotParams(slotNum))
        {
          // Connect to the slide database.
          if(DB_MANAGER()->connect(slideName))
          {
            if(HW_MANAGER()->connect((spin::Magnification)magType))
            {
                m_acqPipeline = spin::acquisition::AcquisitionFactory::
                        getPipeline(slideName, gridName,
                                    (spin::SpecimenType)specimenType);
              connect(m_acqPipeline, &AbstractAcquisition::processFinished,
                      this, &AcquisitionInterface::onProcessFinished);

              // Initialize the acquisition pipeline.
              bool ret = m_acqPipeline->init((spin::SpecimenType)specimenType);
              if(ret)
              {
                return true;
              }
              else
              {
                errMsg = "failed to initialize acquisition pipeline";
              }
            }
            else
            {
              errMsg = "failed to connect to hardware";
            }
          }
          else
          {
            errMsg = "failed to connect to slideDb.db";
          }
        }
        else
        {
          errMsg = "failed to configure slot parameters";
        }
      }

      // Write the status of the acquisition to a text file.
      QString path =slideName +"/" + gridName + "/";
      QString statusFilePath = spin::FileSystemUtil::
              getPathFromWorkspacePath(path + "acq_post_proc.txt");
      QString statusMsg = "error:" + errMsg;
      Utilities::writeStatus(statusFilePath, statusMsg);

      return false;
    }

    /**
     * @brief ~AcquisitionInterface Class destructor, destroys objects
     * owned by the class.
     */
    AcquisitionInterface::~AcquisitionInterface()
    {

    }
  }
}

