#include <QThread>
#include <QDebug>

#include "Common/Framework/HardwareManager/Acquisition/CaptureWorker.h"
#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"

#define COMPUTE_FM 1

namespace spin
{
  namespace acquisition
  {
    /**
     * @brief CaptureWorker Default class constructor, initializes objects
     * owned by the class.
     * @param parent
     */
    CaptureWorker::CaptureWorker(QObject* parent) :
      QObject(parent)
    {

    }

    /**
     * @brief onStartCapture Slot called to start capture.
     */
    void CaptureWorker::onStartCapture()
    {
      bool err = false;
      const unsigned buffSize = Constants::FRAME_WIDTH *
          Constants::FRAME_HEIGHT * 4;
      const int Remove_Images_From_Stack =
          CONFIG()->intConfig(ConfigKeys::REMOVE_IMAGES_FROM_STACK);

      QString errMsg;

      int bestIdx = 0;
      double focusMetric = 0.0;
      double bestFocusMetric = 0.0;

      for(int idx = 0; idx < m_aoiInfoList.size(); idx++)
      {
        // Capture the image and check for NULL.
        void* _imgPtr = HW_MANAGER()->snapImage();
        if(_imgPtr != Q_NULLPTR)
        {
          unsigned char* imgBuff = new unsigned char[buffSize];
          memcpy(imgBuff, _imgPtr, buffSize);

          m_aoiInfoList[idx].m_ptr = imgBuff;

          bestIdx = 0;
          bestFocusMetric = 0.0;

          const int stackSize = m_aoiInfoList.at(idx).m_zStack.size();
          for(int stackIdx = 0; stackIdx < stackSize;
              stackIdx++)
          {
            void* _stackImgPtr = HW_MANAGER()->snapImage();
            if(_stackImgPtr != Q_NULLPTR)
            {
              if((stackIdx >= Remove_Images_From_Stack) &&
                 (stackIdx < stackSize))
              {

                if(CONFIG()->intConfig(ConfigKeys::INLINE_FM_CALC))
                {
                  cv::Mat stackImage(cv::Size(Constants::FRAME_WIDTH,
                                              Constants::FRAME_HEIGHT), CV_8UC4,
                                     _stackImgPtr);

                  // Split the image into four different channels.
                  std::vector <cv::Mat> channels;
                  channels.reserve(4);
                  cv::split(stackImage, channels);
                  focusMetric = Utilities::computeFocusMetric(channels[1]);

                  /*
                  // Extract a single image in uchar format
                  cv::Mat1b green;
                  cv::extractChannel(stackImage, green, 1);
                  focusMetric = Utilities::computeFocusMetric(green);
                  */

                  if(focusMetric > bestFocusMetric)
                  {
                    bestFocusMetric = focusMetric;
                    bestIdx = stackIdx;
                  }
                }

                unsigned char* stackImgBuff = new unsigned char[buffSize];
                memcpy(stackImgBuff, _stackImgPtr, buffSize);

                m_aoiInfoList[idx].m_zStack[stackIdx] = stackImgBuff;
              }
            }
            else
            {
              err = true;
              errMsg = QObject::tr("Snap missed at stack trigger %1 of "
                                   "total %2 of AoI index %3").
                  arg(stackIdx + 1).
                  arg(m_aoiInfoList.at(idx).m_zStack.size()).
                  arg(idx);
              break;
            }
          }

          if(err == true)
          {
            break;
          }
          else
          {
            m_aoiInfoList[idx].m_bestIdx = bestIdx;
            m_aoiInfoList[idx].m_focusMetric = bestFocusMetric;
          }
        }
        else
        {
          err = true;
          errMsg = QObject::tr("Snap missed at index %1 of total %2").
              arg(idx).arg(m_aoiInfoList.size());
          break;
        }

        // Set the Z-Stage for the next AoI.
        if(!CONFIG()->intConfig(ConfigKeys::COLUMN_ZSTACK_MODE) &&
                idx + 1 < m_aoiInfoList.size())
        {
          HW_MANAGER()->setZPosition(m_aoiInfoList.at(idx + 1).m_zVal, false);
        }
      }

      // Stop the thread.
      if(thread())
      {
        thread()->quit();
      }

      if(err == true)
      {
        // Flush out remaining data if any.
        for(int idx = 0; idx < m_aoiInfoList.size(); idx++)
        {
          if(m_aoiInfoList[idx].m_ptr != Q_NULLPTR)
          {
            delete[] m_aoiInfoList[idx].m_ptr;
          }
          for(int stackIdx = 0; stackIdx < m_aoiInfoList.at(idx).m_zStack.size();
              stackIdx++)
          {
            if(m_aoiInfoList.at(idx).m_zStack.at(stackIdx) != Q_NULLPTR)
            {
              delete[] m_aoiInfoList[idx].m_zStack[stackIdx];
            }
          }
        }
        Q_EMIT captureError(errMsg);
      }
      else
      {
        Q_EMIT captureFinished();
      }
    }

    /**
     * @brief ~CaptureWorker Default class destructor, destroys objects owned
     * by the class.
     */
    CaptureWorker::~CaptureWorker()
    {

    }
  }
}

