#include <QDebug>

#include "Common/Framework/HardwareManager/Acquisition/FocusMapWorker.h"

#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"

namespace spin
{
  namespace acquisition {
    /**
    * @brief FocusMapWorker Class for calculating focus map
    * @param parent
    */
    FocusMapWorker::FocusMapWorker(QObject* parent):
      QObject (parent)
    {
    }

    /**
    * @brief init Initialize variables.
    */
    void FocusMapWorker::init(double offset, QStringList preRefZList, QList <AoIInfo> aoiInfoList)
    {
      int idx = 0;
      Q_FOREACH(AoIInfo aoiInfo, aoiInfoList)
      {
        if(aoiInfo.m_zStack.isEmpty() == true)
        {
          double stackBestZ = preRefZList[idx].toDouble() - offset;
          m_preBestZList.append(QString::number(stackBestZ, 'g', 8));
        }
        else
        {
          // Assuming that the stack size is always odd number
          int stackSize = CONFIG()->intConfig(ConfigKeys::XY_Z_TRIGGER_STACK_SIZE);
          double stepSize = CONFIG()->realConfig(ConfigKeys::XY_Z_TRIGGER_STEP_SIZE);
          int dumpImgs = CONFIG()->intConfig(ConfigKeys::REMOVE_IMAGES_FROM_STACK);
          int steps;
          if(aoiInfo.m_bestIdx == dumpImgs)
          {
            steps = (aoiInfo.m_bestIdx - 1) - (stackSize - 1)/2;
            //steps = (aoiInfo.m_bestIdx - 1) - (stackSize - 1)/2 - 2;
          }
          else if(aoiInfo.m_bestIdx == stackSize-1)
          {
            steps = (aoiInfo.m_bestIdx + 1) - (stackSize - 1)/2;
            //steps = (aoiInfo.m_bestIdx + 1) - (stackSize - 1)/2 - 2;
          }
          else
          {
            steps = aoiInfo.m_bestIdx - (stackSize - 1)/2;
            //steps = aoiInfo.m_bestIdx - (stackSize - 1)/2 - 2;
          }
          double stackBestZ = preRefZList[idx].toDouble() + steps*stepSize;
          m_preBestZList.append(QString::number(stackBestZ, 'g', 8));
        }
        m_preFGBGList.append(QString::number(aoiInfo.m_bgState, 'g', 8));
        m_preFocusMetricList.append(QString::number(aoiInfo.m_focusMetric, 'g', 8));
        idx++;
      }
    }

    /**
    * @brief getFocusMap To calculate focus map for current row using bestZ
    * values of previous row.
    */
    QStringList FocusMapWorker::getFocusMap(QList <AoIInfo> aoiInfoList)
    {
      // Populating fgbg list for focus map
      QStringList rowFGBGList;
      QStringList samplingZList;
      QStringList itrpltdZList;
      Q_FOREACH(AoIInfo aoiInfo, aoiInfoList)
      {
        rowFGBGList.append(QString::number(aoiInfo.m_bgState, 'g', 8));
        samplingZList.append(QString::number(aoiInfo.m_samplingZ, 'g', 8));
        itrpltdZList.append(QString::number(aoiInfo.m_zVal, 'g', 8));
      }

      QStringList rowFocusMapList;
      bool fillBG = false;
      int startFill = 0;
      double fgThreshold = CONFIG()->realConfig(ConfigKeys::BG_FM_TISSUE_BIOPSY);
      for(int idx=0; idx < rowFGBGList.size(); idx++)
      {
        if(rowFGBGList.at(idx).toInt() == 0)
        {
          double bestZ;
          if(samplingZList.at(idx).toDouble() > 0)
          {
            bestZ = samplingZList[idx].toDouble();
            SPIN_LOG_DEBUG(QObject::tr("Sampling z %1").arg(bestZ));
            fillBG = true;
          }
          else
          {
            if(m_preBestZList.size() > 0)
            {
              if(m_preFGBGList.at(idx).toInt() == 0)
              {
                if(m_preFocusMetricList.at(idx).toDouble() > fgThreshold)
                {
                  bestZ = m_preBestZList.at(idx).toDouble();
                  SPIN_LOG_DEBUG(QObject::tr("Previous z %1").arg(bestZ));
                  fillBG = true;
                }
                else
                {
                  // Fill or use interpolated z value at end
                  bestZ = -1;
                  SPIN_LOG_DEBUG(QObject::tr("FM fail z %1").arg(bestZ));
                  fillBG = false;
                }
              }
              else
              {
                // Fill or use interpolated z value at end
                bestZ = -1;
                SPIN_LOG_DEBUG(QObject::tr("FG fail z %1").arg(bestZ));
                fillBG = false;
              }
            }
            else
            {
              // Fill or use interpolated z value at end
              bestZ = -1;
              SPIN_LOG_DEBUG(QObject::tr("No z %1").arg(bestZ));
              fillBG = false;
            }
          }
          rowFocusMapList.append(QString::number(bestZ, 'g', 8));
          if (fillBG == true)
          {
            if (startFill == 0)
            {
              // Fill start
              // SPIN_LOG_DEBUG(QObject::tr("Fill start"));
              for(int fillIdx=startFill; fillIdx < idx; fillIdx++)
              {
                double bestZ = rowFocusMapList.at(idx).toDouble();
                rowFocusMapList[fillIdx] = QString::number(bestZ, 'g', 8);
              }
            }
            else
            {
              // Fill intermediate
              // SPIN_LOG_DEBUG(QObject::tr("Fill intermediate"));
              double startZ = rowFocusMapList.at(startFill-1).toDouble();
              double stopZ = rowFocusMapList.at(idx).toDouble();
              double zStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::
                                                               STAGE_STEP_SIZE_Z);
              double zMicrostepping = CONFIG()->realConfig(ConfigKeys::
                                                           STAGE_MICROSTEPPING_Z);
              double minZstep = zStepSizeInMicrons/zMicrostepping;
              for(int fillIdx=startFill; fillIdx < idx; fillIdx++)
              {
                int deltaZ = (stopZ-startZ)*(fillIdx-startFill+1)/(idx-fillIdx+1)/minZstep;
                double bestZ = startZ + deltaZ*minZstep;
                rowFocusMapList[fillIdx] = QString::number(bestZ, 'g', 8);
              }
            }
            fillBG = false;
            startFill = idx + 1;
          }
        }
        else
        {
          if(idx == rowFGBGList.size() - 1)
          {
            // Fill end
            // SPIN_LOG_DEBUG(QObject::tr("Fill end"));
            rowFocusMapList.append(QString::number(-1, 'g', 8));
            SPIN_LOG_DEBUG(QObject::tr("No z %1").arg(-1));
            if(startFill > 0)
            {
              double bestZ = rowFocusMapList.at(startFill-1).toDouble();
              for(int fillIdx=startFill; fillIdx < idx+1; fillIdx++)
              {
                rowFocusMapList[fillIdx] = QString::number(bestZ, 'g', 8);
              }
            }
          }
          else
          {
            rowFocusMapList.append(QString::number(-1, 'g', 8));
            SPIN_LOG_DEBUG(QObject::tr("No z %1").arg(-1));
            fillBG = false;
          }
        }
      }
      // Replace -1 values by interpolated z values
      for(int IDX=0; IDX < rowFocusMapList.size(); IDX++)
      {
        if(rowFocusMapList[IDX].toDouble() < 0)
        {
          rowFocusMapList[IDX] = QString::number(itrpltdZList[IDX].toDouble(), 'g', 8);
        }
      }
      m_preFGBGList.clear();
      m_preBestZList.clear();
      m_preFocusMetricList.clear();
      return rowFocusMapList;
    }

    /**
    * @brief ~FocusMapWorker Default class destructor, destorys objects
    * owned by the class.
    */
    FocusMapWorker::~FocusMapWorker()
    {

    }
  }
}
