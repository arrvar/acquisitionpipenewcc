#ifndef ZSTACKCAPTUREWORKER_H
#define ZSTACKCAPTUREWORKER_H

#include <QObject>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

namespace spin
{
  namespace acquisition
  {
    /**
     * @brief The ZStackCaptureWorker class is a worker class for capturing
     * a z-stack of images with trigger.
     */
    class ZStackCaptureWorker : public QObject
    {
      Q_OBJECT

    public:
      /**
       * @brief ZStackCaptureWorker Default class constructor,
       * initializes objects owned by the class.
       * @param parent Pointer to the parent object.
       */
      ZStackCaptureWorker(QObject* parent = 0);

      /**
       * @brief ~ZStackCaptureWorker Default class destructor,
       * destroys objects owned by the class.
       */
      ~ZStackCaptureWorker();

      /**
       * @brief init To initialize the worker.
       * @param zStepSize Step size for capturing the z-stack.
       * @param startZ Start z-position.
       * @param stopZ Stop z-position.
       * @param aoiInfo To store the AoI info which ahs to be captured.
       * @param focusChannel Channel which will be used for computing the focus
       * metric of the capture image.
       * @param mag Magnification of the AoI at which it is being captured.
       */
      void init(qreal zStepSize, qreal startZ, qreal stopZ,
                const AoIInfo& aoiInfo, int focusChannel, int mag,
                qreal zStageSpeed)
      {
        m_zStepSize = zStepSize;
        m_startZ = startZ;
        m_stopZ = stopZ;
        m_aoiInfo = aoiInfo;
        m_focusChannel = focusChannel;
        m_mag = mag;
        m_zStageSpeed = zStageSpeed;
      }

      /**
       * @brief getAoIInfo To retrieve the captured AoI information.
       * @param aoiInfo Container to store the AoI information.
       */
      void getAoIInfo(AoIInfo& aoiInfo)
      {
        aoiInfo = m_aoiInfo;
      }

    Q_SIGNALS:
      /**
       * @brief captureFinished Signal emitted after capturing all the Z.
       */
      void captureFinished();

      /**
       * @brief captureError Signal emitted if an error occurs while capturing.
       * @param errMsg Error message.
       */
      void captureError(QString errMsg);

      /**
       * @brief deviceError Signal emitted if a device error occurs while
       * capturing.
       * @param errMsg Error message.
       */
      void deviceError(QString errMsg);

    public Q_SLOTS:
      /**
       * @brief onStartCapture Slot called to start capture.
       */
      void onStartCapture();

    private:
      /**
       * @brief clearStack Release stack memory.
       */
      void clearStack();

    private:
      /**
       * @brief m_zStepSize To store the step size for capturing the z-stack.
       */
      qreal m_zStepSize;

      /**
       * @brief m_startZ To store the start z-position.
       */
      qreal m_startZ;

      /**
       * @brief m_stopZ To store the stop z-position.
       */
      qreal m_stopZ;

      /**
       * @brief m_focusChannel To store the channel number to use for computing
       * the focus metric.
       */
      int m_focusChannel;

      /**
       * @brief m_mag To store the magnification value at which the AoI was
       * has to be acquired.
       */
      int m_mag;

      /**
       * @brief m_zStageSpeed Z-Stage speed in mm/s.
       */
      qreal m_zStageSpeed;

      /**
       * @brief m_aoiInfo Container to store the AoI info which has to be
       * captured.
       */
      AoIInfo m_aoiInfo;

    };// end of ZStackCaptureWorker class.
  } // end of acquisition namespace.
} // end of spin namespace.

#endif // ZSTACKCAPTUREWORKER_H
