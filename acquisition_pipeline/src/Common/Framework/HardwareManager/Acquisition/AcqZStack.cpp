﻿#include <QThread>
#include <QElapsedTimer>
#include <QDebug>

#include "Common/Framework/HardwareManager/Acquisition/AcqZStack.h"
#include "Common/Framework/HardwareManager/Acquisition/ZStackCaptureWorker.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"

#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/ImageProcessingWorker.h"
#include "Common/Framework/HardwareManager/Utils/ImageMonitoringWorker.h"
#include "Common/Framework/HardwareManager/Utils/ImageWriterWorker.h"
#include "Common/Framework/HardwareManager/Utils/Types.h"

namespace spin
{
  namespace acquisition
  {
  /**
   * @brief AcqZStack Class constructor, initializes objects owned by the
   * class.
   * @param slideId Unique id of the slide.
   * @param slideName Name of the slide being acquired.
   * @param gridName Name of the grid being acquired.
   * @param parent Instance of the parent QObject.
   */
    AcqZStack::AcqZStack(QString slideName,
                         QString gridName,
                         QObject* parent) :
      AbstractAcquisition(slideName, gridName, parent),
      m_aoiIdx(0),
      m_refFocus(0),
      m_zStepSize(0),
      m_zStackLowerLimit(0),
      m_zStackUpperLimit(0),
      m_zCaptureSpeed(0)
    {
      setupConnections();

      setupCaptureWorker();

      if(CONFIG()->intConfig(ConfigKeys::WRITE_IMAGES))
      {
        setupWriterWorker();
      }

      if(CONFIG()->intConfig(ConfigKeys::ENABLE_STITCHING))
      {
        setupProcessWorker();
      }
    }

    /**
     * @brief setupConnections To setup signal and slot connections for this
     * class.
     */
    void AcqZStack::setupConnections()
    {
      connect(this, &AcqZStack::acquireAoI,
        this, &AcqZStack::onAcquireAoI);

      connect(this, &AcqZStack::acquisitionFinished,
        this, &AcqZStack::onAcquisitionFinished);
    }

    /**
     * @brief setupCaptureWorker To setup the worker class for capturing
     * AoIs.
     */
    void AcqZStack::setupCaptureWorker()
    {
      m_capThread = new QThread();

      m_capWorker = new ZStackCaptureWorker();
      m_capWorker->moveToThread(m_capThread);

      connect(m_capThread, &QThread::started,
        m_capWorker, &ZStackCaptureWorker::onStartCapture);

      connect(m_capWorker, &ZStackCaptureWorker::captureError,
        this, &AcqZStack::onCaptureError);

      connect(m_capWorker, &ZStackCaptureWorker::captureFinished,
        this, &AcqZStack::onAoIAcquired);
    }

    /**
     * @brief setupProcessWorker To setup the image processing worker.
     */
    void AcqZStack::setupProcessWorker()
    {
      m_imgProcThread = new QThread();

      m_imgProcWorker = new ImageProcessingWorker(m_slideName, m_gridName);
      m_imgProcWorker->moveToThread(m_imgProcThread);

      connect(m_imgProcThread, &QThread::started,
              m_imgProcWorker, &ImageProcessingWorker::onStartWorker);

      connect(this, &AcqZStack::destroyed,
              m_imgProcThread, &QThread::deleteLater);

      connect(this, &AcqZStack::destroyed,
              m_imgProcWorker, &QThread::deleteLater);
    }

    /**
     * @brief onAcquireAoI Slot called to acquire an AoI.
     */
    void AcqZStack::onAcquireAoI()
    {
      SPIN_LOG_METHOD();

      if((m_aoiIdx < m_gridInfo.m_aoiList.size()) && (m_writeError == false))
      {
        qreal startZ = m_refFocus - m_zStackLowerLimit;
        qreal stopZ = m_refFocus + m_zStackUpperLimit;
        int zStackSize = ((stopZ - startZ) / m_zStepSize) - 1;
        m_gridInfo.m_aoiList[m_aoiIdx].m_zStack.resize(zStackSize);

        // Ensure the filter is out.
        HW_MANAGER()->removeFilter();

        // Move to its position.
        QPointF startPos = Utilities::pixelsToMicrons(m_gridInfo.m_aoiList.
                                                      at(m_aoiIdx).m_pos);
        HW_MANAGER()->setXYPosition(startPos.x(), startPos.y());

        // Set the Z-Stage to the start position.
        HW_MANAGER()->setZPosition(startZ, true);
        // Initialize the capture worker with required values.
        int focusChannel;
        switch(m_gridInfo.m_mag)
        {
          case MAG_40X:
            focusChannel = CONFIG()->intConfig(ConfigKeys::FOCUS_CHANNEL_40X);
            break;
          case MAG_100X:
            focusChannel = CONFIG()->intConfig(ConfigKeys::FOCUS_CHANNEL_100X);
            break;
          default:
            focusChannel = CONFIG()->intConfig(ConfigKeys::FOCUS_CHANNEL_20X);
        }
        m_capWorker->init(m_zStepSize, startZ, stopZ,
                          m_gridInfo.m_aoiList.at(m_aoiIdx),
                          focusChannel, m_gridInfo.m_mag, m_normalSpeed);

        // Enable trigger mode.
        double zStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::
                                                         STAGE_STEP_SIZE_Z);
        double zMicrostepping = CONFIG()->realConfig(ConfigKeys::
                                                     STAGE_MICROSTEPPING_Z);
        HW_MANAGER()->enableZTriggerMode(m_zStepSize / (zStepSizeInMicrons /
                                                        zMicrostepping));

        // Start the Capture Thread and wait for Trigger
        m_capThread->start(QThread::TimeCriticalPriority);

        HW_MANAGER()->setZStageSpeed(m_zCaptureSpeed);

        // Set the stop position.
        HW_MANAGER()->setZPosition(stopZ);
      }
      else
      {
        SPIN_LOG_DEBUG(QObject::tr("Total acquisition time (s) for %1 "
                                   "images: %2").
                       arg(m_gridInfo.m_aoiList.size()).
                       arg(m_acqTimer.elapsed() / 1000.0));

        // Once all the AoIs have been acquired, set the speed back to
        // normal.
        HW_MANAGER()->setZStageSpeed(m_normalSpeed);
        HW_MANAGER()->stopContinuousMode();
        HW_MANAGER()->disableZBacklash();

        Q_EMIT acquisitionFinished();
      }
    }

    /**
     * @brief onAoIAcquired Slot called when an AoI has been acquired.
     * @param zValue Focus value of the acquired AoI.
     */
    void AcqZStack::onAoIAcquired()
    {
      // Set the XY-Stage trigger mode to OFF.
      HW_MANAGER()->disableTriggerMode();

      SPIN_LOG_DEBUG(QObject::tr("Acquired row %1 of %2").
                     arg(m_aoiIdx + 1).
                     arg(m_gridInfo.m_rowCount));

      // Update the row index.
      m_aoiIdx++;

      QElapsedTimer capThreadTimer;
      capThreadTimer.start();
      // Wait till the capture thread comes to a halt.
      while (m_capThread && m_capThread->isRunning())
      {
        // Do nothing.
      }
      qreal capThreadTime = capThreadTimer.elapsed() / 1000.0;
      capThreadTimer.invalidate();
      SPIN_LOG_DEBUG(QObject::tr("Capture thread wait time (ms): %1").
                     arg(QString::number(capThreadTime)));

      QList <AoIInfo> aoiInfoList;
      AoIInfo aoiInfo;
      m_capWorker->getAoIInfo(aoiInfo);
      aoiInfoList.append(aoiInfo);

      QThread* imgMonThread = new QThread();

      ImageMonitoringWorker* imgMonWorker = new ImageMonitoringWorker();
      imgMonWorker->initWorker(aoiInfoList);
      aoiInfoList.clear();

      imgMonWorker->moveToThread(imgMonThread);

      connect(imgMonThread, &QThread::started,
              imgMonWorker, &ImageMonitoringWorker::onStartWorker);
      connect(imgMonWorker, &ImageMonitoringWorker::workerFinished,
              this, &AcqZStack::onMonitorFinished);
      connect(imgMonThread, &QThread::finished,
              imgMonWorker, &QObject::deleteLater);
      connect(imgMonThread, &QThread::finished,
              imgMonThread, &QObject::deleteLater);

      imgMonThread->start(QThread::HighestPriority);

      Q_EMIT acquireAoI();
    }

    /**
     * @brief onCaptureError Slot called when an error occurs during
     * capturing.
     */
    void AcqZStack::onCaptureError(QString errMsg)
    {
      SPIN_LOG_ERROR(errMsg);

      // Set the XY-Stage trigger mode to OFF.
      HW_MANAGER()->disableTriggerMode();

      // Stop the Z-Stage.
      HW_MANAGER()->stopZStage();

      QElapsedTimer capThreadTimer;
      capThreadTimer.start();
      // Wait till the capture thread comes to a halt.
      while (m_capThread && m_capThread->isRunning())
      {
        // Do nothing.
      }
      qreal capThreadTime = capThreadTimer.elapsed() / 1000.0;
      capThreadTimer.invalidate();
      SPIN_LOG_DEBUG(QObject::tr("Capture thread wait time (ms): %1").
                     arg(QString::number(capThreadTime)));

      // Re-acquire the AoI.
      Q_EMIT acquireAoI();
    }

    /**
     * @brief onMonitorFinished To write the focus metric values of the
     * processed AoIs to the database.
     * @param aoiIdList List of AoI Ids.
     * @param bestIdxList List of best indices for each AoI.
     * @param focusMetricList List of focus metric values.
     * @param colorMetricList List of color metric values.
     */
    void AcqZStack::onMonitorFinished(QList <int> aoiIdList,
                                      QList <int> bestIdxList,
                                      QList <double> focusMetricList,
                                      QList <double> colorMetricList)
    {
      for(int idx = 0; idx < aoiIdList.size(); idx++)
      {
        m_aoiIdList.append(aoiIdList.at(idx));
        m_bestIdxList.append(bestIdxList.at(idx));
        if(bestIdxList.at(idx) == -1)
        {
          m_stackPresentList.append(0);
        }
        else
        {
          m_stackPresentList.append(1);
        }
        m_aoiFocusMetricList.append(focusMetricList.at(idx));
        m_aoiColorMetricList.append(colorMetricList.at(idx));
      }
    }

    /**
     * @brief onAcquisitionFinished Slot called when the acquisition for the
     * slide finishes.
     */
    void AcqZStack::onAcquisitionFinished()
    {
      if(m_writerThread)
      {
        // Stop the writer thread and wait till it exits.
        m_writerWorker->onStopWorker();
        QElapsedTimer writerThreadTimer;
        writerThreadTimer.start();
        // Wait till the writer thread comes to a halt.
        while (m_writerThread->isRunning())
        {
          // Do nothing.
        }
        qreal writerThreadTime = writerThreadTimer.elapsed() / 1000.0;
        writerThreadTimer.invalidate();
        SPIN_LOG_DEBUG(QObject::tr("Writer thread wait time (ms): %1").
                       arg(QString::number(writerThreadTime)));

        if(m_writeError == true)
        {
          // Flush out remaining data if any.
          while(SHARED_WRITE_BUFFER()->isEmpty() == false)
          {
            void* _vPtr = SHARED_WRITE_BUFFER()->get();
            acquisition::AoIInfo* aoiInfo =
                static_cast<acquisition::AoIInfo* >(_vPtr);
            aoiInfo->m_img.release();
            for(int idx = 0; idx < aoiInfo->m_zStack.size(); idx++)
            {
              if(aoiInfo->m_zStack[idx] != Q_NULLPTR)
              {
                delete[] aoiInfo->m_zStack[idx];
              }
            }
            delete aoiInfo;
          }
        }
      }

      if(m_imgProcThread)
      {
        // Stop the image processing thread and wait till it exits.
        if(m_writeError == true)
        {
          m_imgProcWorker->onAbortWorker();
        }
        else
        {
          m_imgProcWorker->onStopWorker();
        }

        QElapsedTimer procThreadTimer;
        procThreadTimer.start();
        // Wait till the writer thread comes to a halt.
        while (m_imgProcThread->isRunning())
        {
          // Do nothing.
        }
        qreal procThreadTime = procThreadTimer.elapsed() / 1000.0;
        procThreadTimer.invalidate();
        SPIN_LOG_DEBUG(QObject::tr("Process thread wait time (ms): %1").
                       arg(QString::number(procThreadTime)));
      }

      if(m_aoiIdList.isEmpty() == false)
      {
        // Update the focus metric of the image in the database.
        QElapsedTimer dbTimer;
        dbTimer.start();
        DB_MANAGER()->updateAoIFocusMetric(m_aoiIdList, m_aoiFocusMetricList,
                                           m_aoiColorMetricList, m_bestIdxList,
                                           m_stackPresentList, m_bestIdxList);
        qreal dbUpdateTime = dbTimer.elapsed() / 1000.0;
        dbTimer.invalidate();
        SPIN_LOG_DEBUG(QObject::tr("Database update time (ms): %1").
                       arg(QString::number(dbUpdateTime)));
      }

      Q_EMIT processFinished(m_slideName, m_gridName, m_writeError);
    }

    /**
     * @brief init To initialize the acquisition process.
     * @param specType Specimen type being acquired.
     * @return Returns true on successful initialization.
     */
    bool AcqZStack::init(SpecimenType specType)
    {
      SPIN_LOG_METHOD();

      // Retrieve and store the grid information from the database.
      if (!DB_MANAGER()->getGridInfo(m_gridInfo, m_gridName))
      {
        return false;
      }

      m_specType = specType;

      // Set the processing type for all the AoIs to displacement esitmation
      // mode. This is specific to continuous acquisition.
      QString magStr = Utilities::magnificationString(m_gridInfo.m_mag);
      QString path = m_slideName + "/" + m_gridName + "/1/" + magStr +"/";
      QString stackPath = m_slideName + "/" + m_gridName + "/image_stack/";

      for(int idx = 0; idx < m_gridInfo.m_aoiList.size(); idx++)
      {
        m_gridInfo.m_aoiList[idx].m_procType = DISP_EST;
        QString aoiPath = FileSystemUtil::
                getPathFromWorkspacePath(path + m_gridInfo.m_aoiList.
                                         at(idx).m_name + ".bmp");
        m_gridInfo.m_aoiList[idx].m_destPath = aoiPath;
        m_gridInfo.m_aoiList[idx].m_stackDestPath = FileSystemUtil::
            getPathFromWorkspacePath(stackPath);
        m_gridInfo.m_aoiList[idx].m_whiteCorr = false;
      }

      m_refFocus = m_gridInfo.m_bestFocus;
      m_zStepSize = Utilities::getZStageStepSize(m_gridInfo.m_mag);
      Utilities::getZStackLimits(m_gridInfo.m_mag, m_zStackLowerLimit,
                                 m_zStackUpperLimit);

      int totalAoiCount = m_gridInfo.m_rowCount * m_gridInfo.m_colCount;
      if(CONFIG()->intConfig(ConfigKeys::ENABLE_STITCHING))
      {
        if(m_imgProcWorker->init(totalAoiCount))
        {
          // Start the image processing thread.
          m_imgProcThread->start();
        }
        else
        {
          SPIN_LOG_ERROR("Unable to start the process thread.");
          return false;
        }
      }

      if(CONFIG()->intConfig(ConfigKeys::WRITE_IMAGES))
      {
        m_writerWorker->setTotalAoICount(totalAoiCount);
        if(m_writerThread)
        {
          // Start the writer thread.
          m_writerThread->start();
        }
        else
        {
          SPIN_LOG_ERROR("Unable to start the writer thread.");
          return false;
        }
      }

      return true;
    }

    /**
     * @brief process To start the acquisition process.
     */
    void AcqZStack::process()
    {
      SPIN_LOG_METHOD();

      // Start the acquisition timer.
      m_acqTimer.start();

      // Set the objective of the stage.
      HW_MANAGER()->setObjective(m_gridInfo.m_mag);

      // Set the exposure value of the camera for the given magnification.
      qreal camExpVal = Utilities::getCamExpValue(m_gridInfo.m_mag);
      HW_MANAGER()->setCameraExposure(camExpVal);

      // Ensure the filter is out.
      HW_MANAGER()->removeFilter();

      // Store the current speed of the Z-Stage.
      m_normalSpeed = HW_MANAGER()->getZStageSpeed();

      // Set the speed of the Z-Stage for trigger acquisition.
      double camFps = CONFIG()->realConfig(ConfigKeys::Z_TRIGGER_CAM_FPS);
      // Convert from microns to mm.
      double stepSizeInMm = m_zStepSize / 1000.0;
      m_zCaptureSpeed = stepSizeInMm * camFps;

      // Start the acquisition.
      Q_EMIT acquireAoI();
    }

    /**
     * @brief ~AcqZStack Default class destructor, destroys objects owned
     * by the class.
     */
    AcqZStack::~AcqZStack()
    {

    }
  }
}
