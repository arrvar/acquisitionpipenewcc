#ifndef ACQUISITIONPIPELINE_H
#define ACQUISITIONPIPELINE_H

#include "Common/Framework/HardwareManager/Acquisition/AbstractAcquisition.h"

namespace spin
{
  // Forward declaration.
  class ImageProcessingWorker;

  namespace acquisition
  {
    // Forward declaration.
    class CaptureWorker;
    class FocusMapWorker;

    /**
     * @brief The AcquisitionPipeline class is the base for controlling the
     * acquisition of images.
     */
    class AcquisitionPipeline : public AbstractAcquisition
    {
      Q_OBJECT

    public:
      /**
       * @brief AcquisitionPipeline Default class constructor, initializes
       * objects owned by the class.
       * @param slideName Name of the slide for acquiring.
       * @param gridName Name of the grid for acquiring.
       * @param parent Reference to a parent QObject class.
       */
      AcquisitionPipeline(QString slideName,
                          QString gridName,
                          QObject* parent = 0);

      /**
       * @brief ~AcquisitionPipeline Default class destructor, destroys objects
       * owned by the class.
       */
      ~AcquisitionPipeline();

      /**
       * @brief init To initialize the acquisition for a grid of a slide.
       * @return Returns true if the initialization is successful else returns
       * false.
       */
      bool init(SpecimenType specType);

      /**
       * @brief process To start the acquisition of a grid of a slide.
       */
      void process();

    Q_SIGNALS:
      /**
       * @brief acquire Signal to start acquisition of a row/column in a grid.
       */
      void acquire();

    private Q_SLOTS:
      /**
       * @brief onAcquire Slot that will be called for acquiring a row/column
       * in a grid.
       */
      void onAcquire();

      /**
       * @brief onAcquired Slot called when a row/column has been acquired.
       */
      void onAcquired();

      /**
       * @brief onCaptureError Slot called when an error occurs during
       * capturing.
       * @param errMsg Error message.
       */
      void onCaptureError(QString errMsg);

      /**
       * @brief onMonitorFinished To write the focus metric values of the
       * processed AoIs to the database.
       * @param aoiIdList List of AoI Ids.
       * @param bestIdxList List of best indices for each AoI.
       * @param focusMetricList List of focus metric values.
       * @param colorMetricList List of color metric values.
       */
      void onMonitorFinished(QList <int> aoiIdList,
                             QList <int> bestIdxList,
                             QList <double> focusMetricList,
                             QList <double> colorMetricList);

      /**
       * @brief onAcquisitionFinished Slot called when acquisition finishes.
       */
      void onAcquisitionFinished();

    private:
      /**
       * @brief setupConnections To setup signal and slot connections for this
       * class.
       */
      void setupConnections();

      /**
       * @brief setupCaptureWorker To setup the worker class for capturing
       * AoIs.
       */
      void setupCaptureWorker();

      /**
       * @brief setupProcessWorker To setup the worker class for processing
       * captured images.
       */
      void setupProcessWorker();

      /**
       * @brief setupFocusMapWorker To setup the worker class for creating
       * focus map
       */
      void setupFocusMapWorker();

    private:
      /**
       * @brief m_captureHeighttinMic To store the height of a column that
       * needs to be captured in microns.
       */
      qreal m_captureHeightinMic;
	   /**
       * @brief m_captureWidthinMic To store the width of a row that
       * needs to be captured in microns.
       */
      qreal m_captureWidthinMic;

      /**
       * @brief m_captureHeightinSteps To store the height of a column that
       * needs to be captured in steps.
       */
      qreal m_captureHeightinSteps;

      /**
       * @brief m_captureWidthinSteps To store the width of a row that
       * needs to be captured in steps.
       */
      qreal m_captureWidthinSteps;

      /**
       * @brief m_rowWidthinMic To store the row width in microns.
       */
      qreal m_rowWidthinMic;

      /**
       * @brief m_colHeightinMic To store the column height in microns.
       */
      qreal m_colHeightinMic;

      /**
       * @brief m_acqIdx Counter to store the current index of the row/column
       * that is being acquired.
       */
      int m_acqIdx;

      /**
       * @brief m_acqCount To store the total no. of row/columns that have to
       * be acquired.
       */
      int m_acqCount;

      /**
       * @brief m_imgCount To store the total no. of images that have to be
       * acquired in a row/column.
       */
      int m_imgCount;

      /**
       * @brief m_acqStr String to store the acquisition is along row or column.
       */
      QString m_acqStr;

      /**
       * @brief m_stackCountStr String to store the stack count and the step
       * sizes for Z-Trigger capture during continuous acquisition mode.
       */
      QString m_stackCountStr;

      /**
       * @brief m_capWorker Pointer to the capture worker object.
       */
      QPointer <CaptureWorker> m_capWorker;

      /**
       * @brief m_imgProcWorker Pointer to the image processing worker object.
       */
      QPointer <ImageProcessingWorker> m_imgProcWorker;

      /**
       * @brief m_imgProcThread Pointer to the image processing thread object.
       */
      QPointer <QThread> m_imgProcThread;

      /**
       * @brief m_imgProcWorker Pointer to the focus map worker object.
       */
      QPointer <FocusMapWorker> m_FocusMapWorker;

      /**
       * @brief m_offset Offset value for background images
       */
      double m_offset;
      
       /**
       * @brief m_finalZList List to store reference z values of
       * current row
       */
      QStringList m_finalZList;
      
      /**
      * @brief m_usePreZ Bool to use previous z or interpolated z
      */
      int m_usePreZ;

    };// end of AcquisitionPipeline class.
  } // end of acquisition namespace.
} // end of spin namespace.

#endif // ACQUISITIONPIPELINE_H
