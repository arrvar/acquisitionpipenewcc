#include <QThread>
#include <QFileInfo>
#include <QDebug>

#include "Common/Framework/HardwareManager/Acquisition/ZCaptureWorker.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"

#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"

namespace spin
{
namespace acquisition
{
  /**
  * @brief ZCaptureWorker Default class constructor, initializes objects
  * owned by the class.
  * @param parent
  */
  ZCaptureWorker::ZCaptureWorker(QString slideName, QObject* parent) :
    QObject(parent),
    m_slideName(slideName),
    m_acqStackSize(0),
    m_zStepSize(0),
    m_startZ(0),
    m_stopZ(0),
    m_focusChannel(1),
    m_mag(0)
  {

  }

  void ZCaptureWorker::onStartCapture()
  {
    bool err = false;
    QVector <cv::Mat> imageStack(m_acqStackSize);
    QString errMsg;
    for(int idx = 0; idx < m_acqStackSize; idx++)
    {

      void* _imgPtr = HW_MANAGER()->snapImage();
      if(_imgPtr != NULL)
      {
        cv::Mat inputImage(cv::Size(Constants::FRAME_WIDTH,
                                    Constants::FRAME_HEIGHT),
                           CV_8UC4, _imgPtr);

        imageStack[idx] = inputImage.clone();
      }
      else
      {
        err = true;
        qDebug() << QObject::tr("Snap missed at index %1 of total %2").
                    arg(idx).arg(m_acqStackSize);
        break;
      }
      _imgPtr = NULL;
    }
    if(err == true)
    {
      // Set the Z-Stage trigger mode to OFF.
      HW_MANAGER()->disableTriggerMode();

      // Stop the continuous mode.
      HW_MANAGER()->stopContinuousMode();

      // Stop the Z-Stage.
      HW_MANAGER()->stopZStage();

      int idx = 0;
      imageStack.clear();
      imageStack.resize(m_acqStackSize);
      qreal currZ = m_startZ;
      while(currZ <= m_stopZ)
      {
        HW_MANAGER()->setZPosition(currZ, true);

        void* _imgPtr = HW_MANAGER()->snapImage();

        if(_imgPtr != NULL)
        {
          cv::Mat inputImage(cv::Size(Constants::FRAME_WIDTH,
                                      Constants::FRAME_HEIGHT),
                             CV_8UC4, _imgPtr);
          imageStack[idx++] = inputImage.clone();
        }
        else
        {
          imageStack[idx++] = cv::Mat::zeros(0, 0, CV_8UC4);
        }
        currZ += m_zStepSize;
      }
    }

    double bestFocusVal = 0;
    int bestIdx = 0;
    QList <double> focusMetricList;
    for(int idx = 0; idx < imageStack.size(); idx++)
    {
      std::vector <cv::Mat> channels;
      channels.reserve(4);

      // Split the image into four different channels.
      cv::split(imageStack[idx], channels);

      //find the center of the image
      int img_cen_x = channels[m_focusChannel].cols/2;
      int img_cen_y = channels[m_focusChannel].rows/2;
      //find starting x,y,width and height
      int start_x = img_cen_x - channels[m_focusChannel].cols/4;
      int start_y = img_cen_y - channels[m_focusChannel].rows/4;
      int crop_width = channels[m_focusChannel].cols/2;
      int crop_height = channels[m_focusChannel].rows/2;
      //create opencv Rect
      cv::Rect bbox(start_x,start_y,crop_width, crop_height);
      //crop image...if this is working, smit owes me a kitkat
      cv::Mat centerCroppedImage = (channels[m_focusChannel])(bbox);

      //find the  best focused image
      double focusMetricVal = Utilities::computeFocusMetric(centerCroppedImage);
      focusMetricList.append(focusMetricVal);

      if(bestFocusVal<focusMetricVal)
      {
        bestFocusVal = focusMetricVal;
        bestIdx = idx;
      }

      centerCroppedImage.release();
      // Clear all OpenCV buffers.
      for(int idx = 0; idx < channels.size(); idx++)
      {
        channels[idx].release();
      }
    }

    // Stop the thread.
    if(thread())
    {
      thread()->quit();
    }

    if(bestFocusVal != 0)
    {
      qreal bestZ = m_startZ + ((bestIdx + 1) * m_zStepSize);

      // Update the Z Value and the best focus metric.
      // Create an AoI buffer.
      AoIInfo* aoiBuff = new AoIInfo();
      *aoiBuff = m_aoiInfo;

      aoiBuff->m_zVal = bestZ;
      aoiBuff->m_focusMetric = bestFocusVal;

      std::vector <cv::Mat> channels;
      channels.reserve(4);
      // Split the image into 4 channels.
      cv::split(imageStack[bestIdx], channels);

      // Remove the last channel.
      channels.pop_back();

      // Merge the three channels.
      cv::merge(channels, aoiBuff->m_img);

      // Add to the shared buffer
      SHARED_WRITE_BUFFER()->add(aoiBuff);
      SHARED_WRITE_BUFFER()->resume();

      // Clear all OpenCV buffers.
      for(int idx = 0; idx < channels.size(); idx++)
      {
        channels[idx].release();
      }
      channels.clear();

      for(int idx = 0; idx < imageStack.size(); idx++)
      {
        imageStack[idx].release();
      }
      imageStack.clear();

      Q_EMIT captureFinished(bestIdx, bestZ, bestFocusVal, focusMetricList);
    }
    else
    {
      // Clear all OpenCV buffers.
      for(int idx = 0; idx < imageStack.size(); idx++)
      {
        imageStack[idx].release();
      }
      imageStack.clear();

      errMsg = "Could not find best focus.";
      Q_EMIT captureError(errMsg);
    }
  }

  /**
   * @brief ~ZCaptureWorker Default class destructor, destroys objects owned
   * by the class.
   */
  ZCaptureWorker::~ZCaptureWorker()
  {

  }

  }
}
