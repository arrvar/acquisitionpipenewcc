#ifndef HMCAPTUREWORKER_H
#define HMCAPTUREWORKER_H

#include <QObject>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionStructures.h"

namespace spin
{
  namespace acquisition
  {
    /**
     * @brief The CaptureWorker class is a worker class for capturing a given
     * list of AoI data in continuous mode.
     */
    class CaptureWorker : public QObject
    {
      Q_OBJECT

    public:
      /**
       * @brief CaptureWorker Default class constructor, initializes objects
       * owned by the class.
       * @param parent
       */
      CaptureWorker(QObject* parent = 0);

      /**
       * @brief ~CaptureWorker Default class destructor, destroys objects owned
       * by the class.
       */
      ~CaptureWorker();

      /**
       * @brief init To initialize the worker with the list of AoIs for
       * capturing.
       * @param aoiInfoList List of AoIs for capturing.
       */
      void init(QString slideName, QString gridName,
                const QList <AoIInfo>& aoiInfoList)
      {
        m_aoiInfoList.clear();
        m_slideName = slideName;
        m_gridName = gridName;
        m_aoiInfoList = aoiInfoList;
      }

      /**
       * @brief getAoIInfoList To retrieve the AoI info list after acquisition.
       * @param aoiInfoList Container to store the AoI info list.
       */
      void getAoIInfoList(QList <AoIInfo>& aoiInfoList)
      {
        aoiInfoList = m_aoiInfoList;
      }

    Q_SIGNALS:
      /**
       * @brief captureFinished Signal emitted after capturing all the AoIs.
       */
      void captureFinished();

      /**
       * @brief captureError Signal emitted if an error occurs while capturing.
       * @param errMsg Error message.
       */
      void captureError(QString errMsg);

    public Q_SLOTS:
      /**
       * @brief onStartCapture Slot called to start capture.
       */
      void onStartCapture();

    private:
      /**
       * @brief m_slideName To store the name of the slide that
       * is being acquired.
       */
      QString m_slideName;

      /**
       * @brief m_gridName To store the name of the grid that is being acquired.
       */
      QString m_gridName;

      /**
       * @brief m_aoiInfoList To store list of AoIs.
       */
      QList <AoIInfo> m_aoiInfoList;

    };// end of CaptureWorker class.
  } // end of acquisition namespace.
} // end of spin namespace.

#endif // CAPTUREWORKER_H
