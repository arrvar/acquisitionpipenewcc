#include <QThread>
#include <QDebug>
#include <QtMath>

#include "Common/Framework/HardwareManager/Acquisition/AcqStartStop.h"
#include "Common/Framework/HardwareManager/Acquisition/ZCaptureWorker.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"

#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/ImageWriterWorker.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"


namespace spin
{
  namespace acquisition
  {
    bool variantLessThan(AoIInfo &v1, AoIInfo &v2)
    {
      return v1.m_pos.x() < v2.m_pos.x();
    }


    QElapsedTimer sampleTimer;
    /**
     * @brief AcqStartStop Class constructor for initializing the
     * acquisition for the given slide and grid.
     * @param slideName Name of the slide.
     * @param gridName Name of the grid.
     * @param parent Pointer to the parent object.
     */
    AcqStartStop::AcqStartStop(QString slideName,
                               QString gridName,
                               QObject *parent) :
      AbstractAcquisition(slideName, gridName, parent),
      m_aoiIdx(0),
      m_zOffset(0),
      m_zStepSize(0),
      m_zStackLowerLimit(0),
      m_zStackUpperLimit(0),
      m_prezStackSize(0),
      m_startZ(0)
    {
      setupConnections();

      setupCaptureWorker();

      if(CONFIG()->intConfig(ConfigKeys::WRITE_IMAGES))
      {
        setupWriterWorker();
      }
    }

    /**
    * @brief setupConnections To setup signal and slot connections for this
    * class.
    */
    void AcqStartStop::setupConnections()
    {
      connect(this, &AcqStartStop::acquireAoI,
        this, &AcqStartStop::onAcquireAoI);

      connect(this, &AcqStartStop::acquisitionFinished,
        this, &AcqStartStop::onAcquisitionFinished);
    }

    /**
    * @brief setupCaptureWorker To setup the worker class for capturing
    * AoIs.
    */
    void AcqStartStop::setupCaptureWorker()
    {
      m_capThread = new QThread();

      m_capWorker = new ZCaptureWorker(m_slideName);
      m_capWorker->moveToThread(m_capThread);

      connect(m_capThread, &QThread::started,
        m_capWorker, &ZCaptureWorker::onStartCapture);

      connect(m_capWorker, &ZCaptureWorker::captureError,
        this, &AcqStartStop::onCaptureError);

      connect(m_capWorker, &ZCaptureWorker::captureFinished,
        this, &AcqStartStop::onAoIAcquired);
    }

    void AcqStartStop::onAcquireAoI()
    {
      if((m_aoiIdx < m_gridInfo.m_aoiList.size()) && (m_writeError == false))
      {
        // Get the current AoI.
        AoIInfo& aoiInfo = m_gridInfo.m_aoiList[m_aoiIdx];
        if(aoiInfo.m_typicalZ > 0)
        {
          // Set the capture path of the AoI.
          QString path = m_slideName + "/" + m_gridName + "/1/" + Utilities::
            magnificationString(m_gridInfo.m_mag);
          if(m_specType == SPUTUM_SMEAR)
          {
              aoiInfo.m_procType = SPUTUM_ANALYSIS;
              path += "/";
          }
          else
          {
              if (aoiInfo.m_wbcPresent == true)
              {
                aoiInfo.m_procType = WBC_ANALYSIS;
                path += "/wbc_region/";
              }
              else
              {
                aoiInfo.m_procType = RBC_ANALYSIS;
                path += "/mono_region/";
              }
          }
          QString aoiPath = FileSystemUtil::
                  getPathFromWorkspacePath(path + aoiInfo.m_name + ".bmp");
          aoiInfo.m_destPath = aoiPath;

          if(m_whiteRefPath.isEmpty() == false)
          {
            QString whiteCorrPath = m_slideName + "/" + m_gridName +
              "/white_corrected/" +
              Utilities::magnificationString(m_gridInfo.m_mag);
            QString aoiWhiteCorrPath = FileSystemUtil::
                getPathFromWorkspacePath(whiteCorrPath + "/" +
                                         aoiInfo.m_name + ".jpeg");
            aoiInfo.m_whiteCorrDestPath = aoiWhiteCorrPath;
            aoiInfo.m_whiteCorr = true;
          }

          qreal bestFocusVal;
          if(aoiInfo.m_zVal == 0)
          {
            bestFocusVal = aoiInfo.m_typicalZ + m_zOffset;
          }
          else
          {
            bestFocusVal = aoiInfo.m_typicalZ;// + m_zOffset;
          }

          //qreal bestFocusVal = aoiInfo.m_typicalZ;// + m_zOffset;
          m_startZ = bestFocusVal - m_zStackLowerLimit;
          qreal stopZ = bestFocusVal + m_zStackUpperLimit;
          int zStackSize = ((stopZ - m_startZ) / m_zStepSize) - 1;
          /* To reduce the stack size in 100x Acquisition
           * Checking if the previous image is focused
           * Checking the distance of the current AOI to the previous AOI
           * If the distance is less than 1 single 20X AOI, the stack range will
           * be reduced to new Z range.
           */
          if (m_aoiIdx > 2)
          {
              // Get the new range from the configuration file
              qreal newZRange = 4*m_zStepSize;
              int preIdx;
              int listSize;
              listSize = m_bestIdxList.size();
              preIdx = m_bestIdxList[listSize - 1];
              // Check if we are hitting the stack boundary
              if ((preIdx > 0) && (preIdx < m_prezStackSize))
              {
                  // Store the details for the previous AOI & calculate distance
                  AoIInfo& preaoiInfo = m_gridInfo.m_aoiList[m_aoiIdx - 1];
                  QPointF prePos = Utilities::pixelsToMicrons(preaoiInfo.m_pos);
                  QPointF curPos = Utilities::pixelsToMicrons(aoiInfo.m_pos);
                  qreal dist, dist1, dist2;
                  dist1 = qPow(prePos.x() - curPos.x(), 2);
                  dist2 = qPow(prePos.y() - curPos.y(), 2);
                  dist = qPow((dist1 + dist2), 0.5);
                  SPIN_LOG_DEBUG(QObject::tr("Distance from previous AOI %1").
                                 arg(dist));
                  QSizeF aoiSize;
                  aoiSize = Utilities::getFovSizeMicrons(MAG_20X);
                  if (dist < aoiSize.width())
                  {
                      // Redefine the stack size and typical z value
                      bestFocusVal = preaoiInfo.m_zVal;// Use previous bestZ
                      // Making bestz multiple of step size
                      // bestFocusVal = int(bestFocusVal/m_zStepSize)*m_zStepSize;
                      m_startZ = bestFocusVal - newZRange;
                      stopZ = bestFocusVal + newZRange;
                      zStackSize = ((stopZ - m_startZ) / m_zStepSize) - 1;
                  }
              }
          }
          m_prezStackSize = zStackSize;



          //Profile removeFilter time
          sampleTimer.start();
//          // Ensure the filter is out.
//          HW_MANAGER()->removeFilter();
//          SPIN_LOG_DEBUG(QObject::tr("Time Taken to remove filter for %1 : %2").
//                         arg(m_slideName).arg(sampleTimer.elapsed()/1000.0));

          //Profile time to move to next position
//          sampleTimer.restart();
          // Move to its position.
          QPointF startPos = Utilities::pixelsToMicrons(aoiInfo.m_pos);
          SPIN_LOG_DEBUG(QObject::tr("XY Pos for %1, x : %2, y : %3").
                         arg(m_slideName).arg(startPos.x()).arg(startPos.y()));
          HW_MANAGER()->setXYPosition(startPos.x(), startPos.y());
          SPIN_LOG_DEBUG(QObject::tr("Time Taken to set XY for %1 : %2").
                         arg(m_slideName).arg(sampleTimer.elapsed()/1000.0));
          //Profile time to move to z start position
          sampleTimer.restart();
          // Set the Z-Stage to the start position.
          HW_MANAGER()->setZPosition(m_startZ, true);
          SPIN_LOG_DEBUG(QObject::tr("Time Taken to set start Z for %1 : %2").
                         arg(m_slideName).arg(sampleTimer.elapsed()/1000.0));
          SPIN_LOG_DEBUG(QObject::tr("AoI Name: %1").arg(aoiInfo.m_name));
          // Initialize the capture worker with required values.
          int focusChannel;
          switch(m_gridInfo.m_mag)
          {
            case MAG_40X:
              focusChannel = CONFIG()->intConfig(ConfigKeys::FOCUS_CHANNEL_40X);
              break;
            case MAG_100X:
              focusChannel = CONFIG()->intConfig(ConfigKeys::FOCUS_CHANNEL_100X);
              break;
            default:
              focusChannel = CONFIG()->intConfig(ConfigKeys::FOCUS_CHANNEL_20X);
          }

          //Profile time taken to capture zstack
          sampleTimer.restart();
          m_capWorker->init(zStackSize, m_zStepSize, m_startZ,
                            stopZ, aoiInfo, focusChannel, m_gridInfo.m_mag);
          //Profile time to enable trigger
          sampleTimer.restart();

          // Enable trigger mode.
          double zStepSizeInMicrons = CONFIG()->realConfig(ConfigKeys::
                                                           STAGE_STEP_SIZE_Z);
          double zMicrostepping = CONFIG()->realConfig(ConfigKeys::
                                                       STAGE_MICROSTEPPING_Z);
          HW_MANAGER()->enableZTriggerMode(m_zStepSize / (zStepSizeInMicrons /
                                                          zMicrostepping));

          // Set continous mode.
          HW_MANAGER()->setContinuousMode(Z_TRIGGER);
          SPIN_LOG_DEBUG(QObject::tr("Time Taken to enable trigger for %1 : %2").
                         arg(m_slideName).arg(sampleTimer.elapsed()/1000.0));

          // Start the Capture Thread and wait for Trigger
          sampleTimer.restart();
          m_capThread->start(QThread::TimeCriticalPriority);

          // Set the speed of the Z-Stage for trigger acquisition.
          qreal camFps = CONFIG()->realConfig(ConfigKeys::Z_TRIGGER_CAM_FPS);
          qreal stepSizeInMm = m_zStepSize / 1000.0;
          qreal zSpeed = stepSizeInMm * camFps;
          if(zSpeed > CONFIG()->realConfig(spin::ConfigKeys::DEF_Z_STAGE_SPEED))
          {
            zSpeed = CONFIG()->realConfig(spin::ConfigKeys::DEF_Z_STAGE_SPEED);
          }
          HW_MANAGER()->setZStageSpeed(zSpeed);

          // Set the stop position.
          HW_MANAGER()->setZPosition(stopZ);
        }
        else
        {
          // Error check.
          // This AoI does not have any low power field associated with it.
          // So it will be skipped.
          // Update the AoI index.
          m_aoiIdx++;

          // Acquire next AoI.
          Q_EMIT acquireAoI();
        }
      }
      else
      {
        SPIN_LOG_DEBUG(QObject::tr("Total acquisition time (s) for %1 "
                                   "images: %2").
                       arg(m_gridInfo.m_aoiList.size()).
                       arg(m_acqTimer.elapsed() / 1000.0));

        // Once all the AoIs have been acquired, set the speed back to
        // normal.
        HW_MANAGER()->setZStageSpeed(m_normalSpeed);
        Q_EMIT acquisitionFinished();
      }
    }

    /**
     * @brief onAoIAcquired Slot called when an AoI has been acquired.
     * @param zValue Focus value of the acquired AoI.
     */
    void AcqStartStop::onAoIAcquired(int bestIdx, qreal zValue,
                                     qreal focusMetric,
                                     QList <double> focusMetricList)
    {
      SPIN_LOG_DEBUG(QObject::tr("Time take to capture stack for %1 : %2").
                     arg(m_slideName).arg(sampleTimer.elapsed()/1000.0));
      //Profile time to disable trigger
      sampleTimer.restart();
      // Enable trigger mode.

      // Set the XY-Stage trigger mode to OFF.
      HW_MANAGER()->disableTriggerMode();

      // Stop the continuous mode.
      HW_MANAGER()->stopContinuousMode();
      SPIN_LOG_DEBUG(QObject::tr("Time Taken to disbale trigger for %1 : %2").
                     arg(m_slideName).arg(sampleTimer.elapsed()/1000.0));
      // Set to normal speed.
      sampleTimer.restart();
      HW_MANAGER()->setZStageSpeed(m_normalSpeed);
      SPIN_LOG_DEBUG(QObject::tr("Time Taken to set z speed for %1 : %2").
                     arg(m_slideName).arg(sampleTimer.elapsed()/1000.0));

      SPIN_LOG_DEBUG(QObject::tr("Focus Metric values for best focus image at %1 : %2").
                     arg(zValue).arg(focusMetric));

      QString aoiName = m_gridInfo.m_aoiList[m_aoiIdx].m_name;
      qreal bestFocusVal;
      if(m_gridInfo.m_aoiList[m_aoiIdx].m_zVal == 0)
      {
        bestFocusVal = m_gridInfo.m_aoiList[m_aoiIdx].m_typicalZ + m_zOffset;
      }
      else
      {
        bestFocusVal = m_gridInfo.m_aoiList[m_aoiIdx].m_typicalZ;
        m_startZ = bestFocusVal - m_zStackLowerLimit;
      }
      
      for(int idx = 0; idx < focusMetricList.size(); idx++)
      {
        qreal zValue = m_startZ + ((idx + 1) * m_zStepSize);
        SPIN_LOG_DEBUG(QObject::tr("AoI: %1, Z-Stage Position: %2,"
                                   "Focus Metric Value: %3").
                       arg(aoiName).arg(zValue).arg(focusMetricList.at(idx)));
      }

      m_bestIdxList.push_back(bestIdx);
      if (m_bestIdxList.size()==3)
      {
        int idxAv = 0;
        for(auto idx:m_bestIdxList)
        {
          idxAv+=idx;
        }
        idxAv/=3;
        m_zStackLowerLimit = m_zStackLowerLimit - idxAv * m_zStepSize + 5;
        m_zStackUpperLimit = idxAv * m_zStepSize - m_zStackUpperLimit + 5;
      }
      // Wait till the capture thread comes to a halt.
      while (m_capThread && m_capThread->isRunning())
      {
        // Do nothing.
      }

      // Check if the acquired 100X image is in FOCUS
      // 1. Checking the focus metric values

      if(focusMetric <= CONFIG()->realConfig(ConfigKeys::
                                             FOCUS_METRIC_THRESHOLD))
      {
          // Update the focus and metric value of the AoI.
          m_gridInfo.m_aoiList[m_aoiIdx].m_zVal = zValue;
          m_gridInfo.m_aoiList[m_aoiIdx].m_focusMetric = focusMetric;
          m_aoiIdList.append(m_gridInfo.m_aoiList[m_aoiIdx].m_id);
          m_aoiZValueList.append(m_gridInfo.m_aoiList[m_aoiIdx].m_zVal);
          m_aoiFocusMetricList.append(m_gridInfo.m_aoiList[m_aoiIdx].
                                      m_focusMetric);

          // Update the database every 10th AoI.
          if(m_aoiIdx % 10 == 0)
          {
            DB_MANAGER()->updateAoIFocusInfo(m_aoiIdList, m_aoiZValueList,
                                             m_aoiFocusMetricList);
            // Clear the lists so that we start fresh again.
            m_aoiIdList.clear();
            m_aoiZValueList.clear();
            m_aoiFocusMetricList.clear();
          }

          SPIN_LOG_INFO(QObject::tr("Captured %1 of %2 images.").
                        arg(m_aoiIdx + 1).arg(m_gridInfo.m_aoiList.size()));
          // Update the AoI index.
          m_aoiIdx++;
      }
      else
      {
        SPIN_LOG_WARNING(QObject::tr("The focus metric ((%1) is above "
                                     "the threshold of 90.0 for image %2. "
                                     "Re-acquiring the image.").
                        arg(focusMetric).
                        arg(m_gridInfo.m_aoiList.at(m_aoiIdx).m_name));
      }

      // Acquire next AoI.
      Q_EMIT acquireAoI();
    }

    /**
     * @brief onCaptureError Slot called when an error occurs during AoI
     * capture.
     */
    void AcqStartStop::onCaptureError(QString errMsg)
    {
      SPIN_LOG_ERROR(errMsg);

      // Set the XY-Stage trigger mode to OFF.
      HW_MANAGER()->disableTriggerMode();

      // Stop the continuous mode.
      HW_MANAGER()->stopContinuousMode();

      // Stop the Z-Stage.
      HW_MANAGER()->stopZStage();

      // Wait till the capture thread comes to a halt.
      while (m_capThread && m_capThread->isRunning())
      {
        // Do nothing.
      }

      // Update the AoI index.
      m_aoiIdx++;

      // Acquire next AoI.
      Q_EMIT acquireAoI();
    }

    /**
     * @brief onAcquisitionFinished Slot called when acquisition finishes.
     */
    void AcqStartStop::onAcquisitionFinished()
    {
      if(m_writerThread)
      {
        // Stop the writer thread and wait till it exits.
        m_writerWorker->onStopWorker();
        QElapsedTimer writerThreadTimer;
        writerThreadTimer.start();
        // Wait till the writer thread comes to a halt.
        while (m_writerThread->isRunning())
        {
          // Do nothing.
        }
        qreal writerThreadTime = writerThreadTimer.elapsed() / 1000.0;
        writerThreadTimer.invalidate();
        SPIN_LOG_DEBUG(QObject::tr("Writer thread wait time (ms): %1").
                       arg(QString::number(writerThreadTime)));

        if(m_writeError == true)
        {
          // Flush out remaining data if any.
          while(SHARED_WRITE_BUFFER()->isEmpty() == false)
          {
            void* _vPtr = SHARED_WRITE_BUFFER()->get();
            acquisition::AoIInfo* aoiInfo =
                static_cast<acquisition::AoIInfo* >(_vPtr);
            aoiInfo->m_img.release();
            delete aoiInfo;
          }
        }
      }

      if(m_aoiIdList.isEmpty() == false)
      {
        // Update the focus info for all the images in the database.
        QElapsedTimer dbTimer;
        dbTimer.start();
        DB_MANAGER()->updateAoIFocusInfo(m_aoiIdList, m_aoiZValueList,
                                         m_aoiFocusMetricList);
        qreal dbUpdateTime = dbTimer.elapsed() / 1000.0;
        SPIN_LOG_DEBUG(QObject::tr("Database update time (ms): %1").
                       arg(QString::number(dbUpdateTime)));
      }

      Q_EMIT processFinished(m_slideName, m_gridName, m_writeError);
    }

    /**
     * @brief init To initialize the acquisition for a grid of a slide.
     * @return Returns true if the initialization is successful else returns
     * false.
     */
    bool AcqStartStop::init(SpecimenType specType)
    {
      m_bestIdxList.clear();

      // Retrieve and store the grid information from the database.
      if (!DB_MANAGER()->getGridInfo(m_gridInfo, m_gridName))
      {
        return false;
      }

      m_specType = specType;

      Magnification lowPowerMag;
      lowPowerMag = MAG_UNKNOWN;
      Q_FOREACH(AoIInfo aoiInfo, m_gridInfo.m_aoiList)
      {
        lowPowerMag = DB_MANAGER()->getLowPowerMag(aoiInfo.m_lowPowerId);
        if(lowPowerMag != MAG_UNKNOWN)
        {
          break;
        }
      }

      m_zOffset = Utilities::getZOffset(lowPowerMag, m_gridInfo.m_mag);
      m_zStepSize = Utilities::getZStageStepSize(m_gridInfo.m_mag);

      //add sorting here
      qSort(m_gridInfo.m_aoiList.begin(), m_gridInfo.m_aoiList.end(), variantLessThan);


      Utilities::getZStackLimits(m_gridInfo.m_mag, m_zStackLowerLimit,
                                 m_zStackUpperLimit);

      int totalAoiCount = m_gridInfo.m_aoiList.size();
      if(CONFIG()->intConfig(ConfigKeys::WRITE_IMAGES))
      {
        // Start the writer thread.
        QString whiteRefPath;
        if(m_gridInfo.m_mag == MAG_40X)
        {
          whiteRefPath = m_slideName + "/" + "white.bmp";
        }
        else if(m_gridInfo.m_mag == MAG_100X)
        {
          whiteRefPath = m_slideName + "/" + "white1.bmp";
        }
        if(whiteRefPath.isEmpty() == false)
        {
          m_whiteRefPath = FileSystemUtil::getPathFromWorkspacePath(whiteRefPath);
          if(m_writerWorker)
          {
            m_writerWorker->init(m_whiteRefPath);
            m_writerWorker->setTotalAoICount(totalAoiCount);
          }
          else
          {
            SPIN_LOG_ERROR("Unable to initialize the writer worker.");
            return false;
          }
        }

        if(m_writerThread)
        {
          m_writerThread->start();
        }
        else
        {
          SPIN_LOG_ERROR("Unable to start the writer thread.");
          return false;
        }
      }

      return true;
    }

    /**
     * @brief process To start the acquisition of a grid of a slide.
     */
    void AcqStartStop::process()
    {
      // Start the acquisition timer.
      m_acqTimer.start();

      // Set the objective of the stage.
      HW_MANAGER()->setObjective(m_gridInfo.m_mag);

      // Set the exposure value of the camera for the given magnification.
      qreal camExpVal = Utilities::getCamExpValue(m_gridInfo.m_mag);
      HW_MANAGER()->setCameraExposure(camExpVal);

      // Ensure the filter is out.
      HW_MANAGER()->removeFilter();

      // Store the current speed of the Z-Stage.
      m_normalSpeed = HW_MANAGER()->getZStageSpeed();

      // Start the acquisition.
      Q_EMIT acquireAoI();
    }

    /**
     * @brief ~AcqStartStop Class destructor, destroys objects owned
     * by the class.
     */
    AcqStartStop::~AcqStartStop()
    {
      if(m_capThread)
      {
        delete m_capThread;
      }

      if(m_capWorker)
      {
        delete m_capWorker;
      }

      if(m_writerThread)
      {
        delete m_writerThread;
      }

      if(m_writerWorker)
      {
        delete m_writerWorker;
      }
    }
  }
}
