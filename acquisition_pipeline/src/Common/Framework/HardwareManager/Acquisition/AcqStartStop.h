#ifndef ACQSTARTSTOP_H
#define ACQSTARTSTOP_H

#include <QElapsedTimer>
#include <QPointer>

#include "Common/Framework/HardwareManager/Acquisition/AbstractAcquisition.h"

namespace spin
{
  namespace acquisition
  {
    // Forward declaration.
    class ZCaptureWorker;

    /**
     * @brief The AcqStartStop class is handles acquisition in a
     * start stop mode. The XY-Stage would move to a given location,
     * the Z-Stage would adjust the focus and the camera will snap an image.
     */
    class AcqStartStop : public AbstractAcquisition
    {
      Q_OBJECT
    public:
      /**
       * @brief AcqStartStop Class constructor for initializing the
       * acquisition for the given slide and grid.
       * @param slideName Name of the slide.
       * @param gridName Name of the grid.
       * @param parent Pointer to the parent object.
       */
      AcqStartStop(QString slideName,
                   QString gridName,
                   QObject* parent = 0);

      /**
       * @brief ~AcqStartStop Class destructor, destroys objects owned
       * by the class.
       */
      virtual ~AcqStartStop();

      /**
       * @brief init To initialize the acquisition for a grid of a slide.
       * @return Returns true if the initialization is successful else returns
       * false.
       */
      bool init(SpecimenType specType);

      /**
       * @brief process To start the acquisition of a grid of a slide.
       */
      void process();

    Q_SIGNALS:
      /**
       * @brief Signal emitted to acquire an AoI.
       */
      void acquireAoI();

    private Q_SLOTS:
      /**
       * @brief onAcquireAoI Slot called to acquire an AoI.
       */
      void onAcquireAoI();

      /**
       * @brief onAoIAcquired Slot called when an AoI has been acquired.
       * @param zValue Focus value of the acquired AoI.
       */
      void onAoIAcquired(int bestIdx, qreal zValue, qreal focusMetric,
                         QList <double> focusMetricList);

      /**
       * @brief onCaptureError Slot called when an error occurs during
       * capturing.
       */
      void onCaptureError(QString errMsg);

      /**
       * @brief onAcquisitionFinished Slot called when acquisition finishes.
       */
      void onAcquisitionFinished();

    private:
      /**
       * @brief setupConnections To setup signal and slot connections for this
       * class.
       */
      void setupConnections();

      /**
       * @brief setupCaptureWorker To setup the worker class for capturing
       * AoIs.
       */
      void setupCaptureWorker();

    private:
      /**
       * @brief m_aoiIdx To store the index of the current AoI being acquired.
       */
      int m_aoiIdx;

      /**
       * @brief m_normalSpeed
       */
      qreal m_normalSpeed;

      /**
       * @brief m_zOffset
       */
      qreal m_zOffset;

      /**
       * @brief m_zStepSize
       */
      qreal m_zStepSize;

      /**
       * @brief m_zStackLowerLimit
       */
      qreal m_zStackLowerLimit;

      /**
       * @brief m_zStackUpperLimit
       */
      qreal m_zStackUpperLimit;
      
      /**
       * @brief m_whiteRefPath
       */
      QString m_whiteRefPath;

      /**
       * @brief m_acqTimer Timer to keep a track of the time taken to acquire
       * a grid.
       */
      QElapsedTimer m_acqTimer;

      /**
       * @brief m_capWorker Pointer to the Z-Stage capture worker object.
       */
      QPointer <ZCaptureWorker> m_capWorker;

      /**
       * @brief
       */
      std::vector<int> m_bestIdxList;
      /**
       * @brief m_prezStackSize
       */
      int m_prezStackSize;
      qreal m_startZ;


    };// end of AcqStartStop class.
  } // end of acquisition namespace.
} // end of spin namespace.

#endif // ACQSTARTSTOP_H
