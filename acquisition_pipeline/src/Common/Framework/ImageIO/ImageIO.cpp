#include "Framework/ImageIO/ImageIO.h"
#include "Framework/ImageIO/lodepng.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/eigen.hpp"
#include "boost/filesystem.hpp"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include "itkTriangleThresholdImageFilter.h"
#include <iostream>

namespace spin
{
  struct myImage
  {
    typedef spin::RGBImageType  ImageType;
    ImageType::RegionType       Region;
    ImageType::SizeType         Size;
  };

  /*! \brief ComputeMask
   *  This function computes a binary mask using Triangle Thresholding
   *  and returns it as an Eigen Matrix
   */
  void CreateMask(void* channel, void* _mask)
  {
    // typecast the data
    RMMatrix_Float* L = static_cast<RMMatrix_Float*>(channel);
    // We will use ITK's triangle thresholding algorithm to generate a mask
    FloatImageType2D::Pointer mask = FloatImageType2D::New();
    UCharImageType2D::SizeType size;
    size[0] = L->cols();
    size[1] = L->rows();
    FloatImageType2D::IndexType index;
    index[0] = 0;
    index[1] = 0;
    FloatImageType2D::RegionType region;
    region.SetSize(size);
    region.SetIndex(index);
    mask->SetRegions(region);
    mask->Allocate();
    mask->FillBuffer(0);
    // Copy the L-channel to mask
    memcpy(mask->GetBufferPointer(),L->data(),size[0]*size[1]*sizeof(float));
    // Run the triangle thresholding algorithm
    typedef itk::TriangleThresholdImageFilter<FloatImageType2D, FloatImageType2D> ThresholdFilterType;
    ThresholdFilterType::Pointer threshold = ThresholdFilterType::New();
    threshold->SetInput(mask);
    threshold->SetInsideValue(1);
    threshold->SetOutsideValue( 0 );
    threshold->SetNumberOfHistogramBins( 256 );
    threshold->Update();
    // Allocate memory to the Eigen matrix
    RMMatrix_Float* maskV = static_cast<RMMatrix_Float*>(_mask);
    *maskV = RMMatrix_Float::Zero(size[1]*size[0],1);
    memcpy(maskV->data(),threshold->GetOutput()->GetBufferPointer(),size[1]*size[0]*sizeof(float));
  }//end of function

  /*! \brief Default constructor
   *  The constructor of this class is used to read meta information
   *  associated with RGB images
   */
  RGBImageIO::RGBImageIO(std::string& path)
  {
    // First check if the file exists
    if (boost::filesystem::exists(path))
    {
      // Instantiate ImagePtrs
      ImagePtr = std::make_shared<spin::myImage>();
      typedef itk::ImageFileReader<RGBImageType> Reader;
      Reader::Pointer reader = Reader::New();
      reader->SetFileName(path.c_str());
      reader->Update();
      ImagePtr.get()->Region     = reader->GetOutput()->GetLargestPossibleRegion();
      ImagePtr.get()->Size       = ImagePtr.get()->Region.GetSize();
    }
  }//end of function

  /*! \brief WriteImage
   *  This function is used to save a RGB Image
   */
  int RGBImageIO::WriteImage(std::string& path, void* _img)
  {
    //Check if a valid extension is being used to write the file
    std::string file_extension = boost::filesystem::extension(path);
    if (file_extension == ".png"  || file_extension == ".PNG"   ||
        file_extension == ".bmp"  || file_extension == ".BMP"   ||
        file_extension == ".jpg"  || file_extension == ".JPG"   ||
        file_extension == ".jpeg" || file_extension == ".JPEG"  ||
        file_extension == ".tif"  || file_extension == ".TIF"   ||
        file_extension == ".tiff" || file_extension == ".TIFF" ||
        file_extension == ".webp" || file_extension == ".WEBP"
      )
    {
      // typecast the image to an RGB image
      RGBImageType::Pointer* itkImage = static_cast<RGBImageType::Pointer*>(_img);
      RGBImageType::SizeType size = (*itkImage)->GetLargestPossibleRegion().GetSize();

      //we will use lodepng for saving PNG's
      if(file_extension == ".png"  || file_extension == ".PNG")
      {
        /*std::vector<int> compression_params;
        compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
        compression_params.push_back(9);
        cv::imwrite(path, inputImage, compression_params);*/
        RMMatrix_Float data;
        Image2Matrix<float>(itkImage, &data);
        RMMatrix_UChar temp = data.template cast<unsigned char>();
        RMMatrix_UChar temp1= RMMatrix_UChar::Ones(size[0]*size[1],1) * 255;
        RMMatrix_UChar temp2 = RMMatrix_UChar(size[0]*size[1],4);
        temp2<<temp,temp1;
        std::vector<unsigned char> png;

        lodepng::State state;
        state.encoder.filter_palette_zero = 0; //We try several filter types, including zero, allow trying them all on palette images too.
        state.encoder.add_id = false; //Don't add LodePNG version chunk to save more bytes
        state.encoder.text_compression = 1; //Not needed because we don't add text chunks, but this demonstrates another optimization setting
        state.encoder.zlibsettings.nicematch = 258; //Set this to the max possible, otherwise it can hurt compression
        state.encoder.zlibsettings.lazymatching = 1; //Definitely use lazy matching for better compression
        state.encoder.zlibsettings.windowsize = 512;
        state.encoder.filter_strategy = LodePNGFilterStrategy::LFS_ENTROPY;//LFS_MINSUM;
        state.encoder.zlibsettings.minmatch = 16;
        state.encoder.zlibsettings.btype = 2;
        state.encoder.auto_convert = 1;

        unsigned error = lodepng::encode(png, temp2.data(), size[0], size[1],state);
        if(error)
        {
          return -2;
        }

        lodepng::save_file(png, path.c_str());
        png.clear();
      }
      else
      {
        cv::Mat inputImage(size[1], size[0], CV_8UC3);
        // Copy the data to this buffer
        memcpy(inputImage.data, (*itkImage)->GetBufferPointer(),3*inputImage.rows*inputImage.cols);

        cv::Mat inputImage1;
        cv::cvtColor(inputImage, inputImage1, cv::COLOR_RGB2BGR);

        if(file_extension == ".webp"  || file_extension == ".WEBP")
        {
          std::vector<int> compression_params;
          compression_params.push_back(cv::IMWRITE_WEBP_QUALITY);
          compression_params.push_back(99);
          cv::imwrite(path, inputImage1, compression_params);
        }
        else
        if(file_extension == ".jpeg"  || file_extension == ".JPEG" || file_extension == ".jpg" || file_extension == ".JPG")
        {
          std::vector<int> compression_params;
          compression_params.push_back(cv::IMWRITE_JPEG_QUALITY);
          compression_params.push_back(99);
          cv::imwrite(path, inputImage1, compression_params);
        }
        else
          cv::imwrite(path, inputImage1);
        inputImage.release();
        inputImage1.release();
      }

      return 1;
    }
    return -1; //should not come here
  }//end of function

  /*! \brief ReadImage
   *  This function is used to read a RGB image
   */
  int RGBImageIO::ReadImage2(std::string& path, void* _img)
  {
    // First check if the file exists
    if (boost::filesystem::exists(path))
    {
      // The image exists.
      // Get the extension of the file
      std::string file_extension = boost::filesystem::extension(path);
      if (file_extension == ".png"  || file_extension == ".PNG"   ||
          file_extension == ".bmp"  || file_extension == ".BMP"   ||
          file_extension == ".jpg"  || file_extension == ".JPG"   ||
          file_extension == ".jpeg" || file_extension == ".JPEG"  ||
          file_extension == ".tif"  || file_extension == ".TIF"   ||
          file_extension == ".tiff" || file_extension == ".TIFF"
        )
      {
        // typecast the image to an RGB image
        cv::Mat* inputImage = static_cast<cv::Mat*>(_img);
        (*inputImage) = cv::imread(path);
        int r = inputImage->rows;
        return r;
      }
    }
    return -1; //should not come here
  }//end of function

  /*! \brief ReadImage
   *  This function is used to read a RGB image
   */
  int RGBImageIO::ReadImage(std::string& path, void* _img, bool allocate)
  {
    // First check if the file exists
    if (boost::filesystem::exists(path))
    {
      // The image exists.
      // Get the extension of the file
      std::string file_extension = boost::filesystem::extension(path);
      if (file_extension == ".png"  || file_extension == ".PNG"   ||
          file_extension == ".bmp"  || file_extension == ".BMP"   ||
          file_extension == ".jpg"  || file_extension == ".JPG"   ||
          file_extension == ".jpeg" || file_extension == ".JPEG"  ||
          file_extension == ".tif"  || file_extension == ".TIF"   ||
          file_extension == ".tiff" || file_extension == ".TIFF"
        )
      {
        // typecast the image to an RGB image
        RGBImageType::Pointer* itkImage = static_cast<RGBImageType::Pointer*>(_img);
        cv::Mat tempImg = cv::imread(path);
        cv::Mat inputImage;
        cv::cvtColor(tempImg, inputImage, cv::COLOR_BGR2RGB);
        if (!allocate)
        {
          (*itkImage)->SetRegions(ImagePtr.get()->Region);
          (*itkImage)->Allocate();
        }
        memcpy((*itkImage)->GetBufferPointer(),inputImage.data,3*tempImg.rows*tempImg.cols);
        tempImg.release();
        return 1;
      }
    }
    return -1; //should not come here
  }//end of function

  /*! \brief ReadImage
   *  This function is used to read a RGB image
   */
  int RGBImageIO::ReadImage_g(std::string& path, void* _img, bool allocate)
  {
    // First check if the file exists
    if (boost::filesystem::exists(path))
    {
      // The image exists.
      // Get the extension of the file
      std::string file_extension = boost::filesystem::extension(path);
      if (file_extension == ".png"  || file_extension == ".PNG"   ||
          file_extension == ".bmp"  || file_extension == ".BMP"   ||
          file_extension == ".jpg"  || file_extension == ".JPG"   ||
          file_extension == ".jpeg" || file_extension == ".JPEG"  ||
          file_extension == ".tif"  || file_extension == ".TIF"   ||
          file_extension == ".tiff" || file_extension == ".TIFF"
        )
      {
        // typecast the image to an RGB image
        UCharImageType2D::Pointer* itkImage = static_cast<UCharImageType2D::Pointer*>(_img);
        cv::Mat inputImage = cv::imread(path);
        if (!allocate)
        {
          (*itkImage)->SetRegions(ImagePtr.get()->Region);
          (*itkImage)->Allocate();
        }
        memcpy((*itkImage)->GetBufferPointer(),inputImage.data,inputImage.rows*inputImage.cols);
        inputImage.release();
        return 1;
      }
    }
    return -1; //should not come here
  }//end of function

  /*! \brief ReadImage
   *  This function is used to read a RGB image
   */
  int RGBImageIO::ReadImage3(std::string& path, void* _img, void* _img2, bool allocate)
  {
    // First check if the file exists
    if (boost::filesystem::exists(path))
    {
      // The image exists.
      // Get the extension of the file
      std::string file_extension = boost::filesystem::extension(path);
      if (file_extension == ".png"  || file_extension == ".PNG"   ||
          file_extension == ".bmp"  || file_extension == ".BMP"   ||
          file_extension == ".jpg"  || file_extension == ".JPG"   ||
          file_extension == ".jpeg" || file_extension == ".JPEG"  ||
          file_extension == ".tif"  || file_extension == ".TIF"   ||
          file_extension == ".tiff" || file_extension == ".TIFF"
        )
      {
        // typecast the image to an RGB image
        RGBImageType::Pointer* itkImage = static_cast<RGBImageType::Pointer*>(_img);
        cv::Mat* imgCV = static_cast<cv::Mat*>(_img2);
        (*imgCV) = cv::imread(path);
        cv::Mat inputImage;
        cv::cvtColor((*imgCV), inputImage, cv::COLOR_BGR2RGB);
        if (!allocate)
        {
          (*itkImage)->SetRegions(ImagePtr.get()->Region);
          (*itkImage)->Allocate();
        }
        memcpy((*itkImage)->GetBufferPointer(),inputImage.data,3*inputImage.rows*inputImage.cols);
        int r = inputImage.rows;
        inputImage.release();
        return 1;
      }
    }
    return -1; //should not come here
  }//end of function

  /*! \brief ExtractChannel
   *  This function is used to extract a channel from a 3-channel LAB image
   *  and also returns the LAB image. This is useful if we have read the
   *  image somewhere else.
   */
  int RGBImageIO::ExtractChannel(void* _img, int cNum, void* _channel, void* _mat)
  {
    if (cNum <0 || cNum >= 3) return -1;
    cv::Mat* img = static_cast<cv::Mat*>(_img);
    if (!img->data) return -2;

    // Convert the data to floating point
    cv::Mat convMat;
    img->convertTo(convMat,CV_32FC3);
    convMat *= 1./255;
    // conversion of BGR image to LAB
    cv::cvtColor(convMat, convMat, cv::COLOR_BGR2Lab);

    cv::Mat channels[3];
    cv::split(convMat, channels);
    RMMatrix_Float* channel = static_cast<RMMatrix_Float*>(_channel);
    (*channel) = RMMatrix_Float::Zero(channels[cNum].rows,channels[cNum].cols);
    cv::cv2eigen(channels[cNum],*channel);

    // Reshape the LAB data into a 3-channel data
    convMat = convMat.reshape(1,convMat.rows*convMat.cols);
    RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(_mat);
    *mat = RMMatrix_Float::Zero(img->rows*img->cols,3);
    memcpy(mat->data(),convMat.data,convMat.rows*convMat.cols*sizeof(float));
    convMat.release();
    return 1;
  }//end of function

  /*! \brief ReadImage
   *  This function is used to read a RGB image and returns the output
   *  as an Eigen matrix. The image is implicitly converted to LAB space
   */
  int RGBImageIO::ReadLABImage(std::string& path, void* _mat, void* _mask)
  {
    // First check if the file exists
    if (boost::filesystem::exists(path))
    {
      // The image exists.
      // Get the extension of the file
      std::string file_extension = boost::filesystem::extension(path);
      if (file_extension == ".png"  || file_extension == ".PNG"   ||
          file_extension == ".bmp"  || file_extension == ".BMP"   ||
          file_extension == ".jpg"  || file_extension == ".JPG"   ||
          file_extension == ".jpeg" || file_extension == ".JPEG"  ||
          file_extension == ".tif"  || file_extension == ".TIF"   ||
          file_extension == ".tiff" || file_extension == ".TIFF"
        )
      {
        // typecast the matrix
        RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(_mat);
        cv::Mat inputImage = cv::imread(path);
        cv::Mat convMat;
        inputImage.convertTo(convMat,CV_32FC3);
        convMat *= 1./255;
        // conversion of BGR image to LAB
        cv::cvtColor(convMat, convMat, cv::COLOR_BGR2Lab);
        //convMat *= 1./255;
        // And reshape it into a 3-column matrix
        convMat = convMat.reshape(1,inputImage.rows*inputImage.cols);
        // Copy the data to en eigen matrix
        *mat = RMMatrix_Float::Zero(inputImage.rows*inputImage.cols,3);
        memcpy(mat->data(),convMat.data,convMat.rows*convMat.cols*sizeof(float));
        // In addition, we need to generate a mask from the L-channel
        //RMMatrix_Float temp = mat->block(0,0,mat->rows(),1);
        //RMMatrix_Float L = Eigen::Map<RMMatrix_Float>(temp.data(),inputImage.rows, inputImage.cols);
        imgRows = inputImage.rows;
        inputImage.release();
        convMat.release();
        return imgRows;
      }
    }
    return -1; //should not come here
  }//end of function

  /*! \brief ReturnRGBImage
   *  This function returns a RGB image from a LAB matrix
   */
  int RGBImageIO::ReturnLAB2RGBImage(void* inMat, void* outMat, int _imgRows)
  {
    // Sanity checks
    RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(inMat);
    // Check if the data has been passed correctly
    if (!mat->data()) return -1;
    // Allocate memory for a 3-channel cv::Mat float image
    cv::Mat convMat(mat->rows(),3,CV_32FC1);
    // Copy the data
    memcpy(convMat.data, mat->data(), 3*mat->rows()*sizeof(float));
    // Reshape the data (but check if _imgRows is non negative)
    if (_imgRows > 0)
      convMat = convMat.reshape(3,_imgRows);
    else
      convMat = convMat.reshape(3, imgRows);

    // And convert from LAB to BGR
    cv::cvtColor(convMat, convMat, cv::COLOR_Lab2BGR);
    // Scale the data by 255
    convMat *= 255;
    // And convert the data
    cv::Mat* outputImage = static_cast<cv::Mat*>(outMat);
    convMat.convertTo(*outputImage,CV_8UC3);
    return 1;
  }//end of function

  /*! \brief WriteImage
   *  This function is used to save a LAB image as a an RGB image
   */
  int RGBImageIO::WriteLABImage(std::string& path, void* _mat, int _imgRows)
  {
    //Check if a valid extension is being used to write the file
    std::string file_extension = boost::filesystem::extension(path);
    if (file_extension == ".png"  || file_extension == ".PNG"   ||
        file_extension == ".bmp"  || file_extension == ".BMP"   ||
        file_extension == ".jpg"  || file_extension == ".JPG"   ||
        file_extension == ".jpeg" || file_extension == ".JPEG"  ||
        file_extension == ".tif"  || file_extension == ".TIF"   ||
        file_extension == ".tiff" || file_extension == ".TIFF"
      )
    {
      // Typecast the eigen matrix
      RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(_mat);
      // Check if the data has been passed correctly
      if (!mat->data()) return -1;
      // Allocate memory for a 3-channel cv::Mat float image
      cv::Mat convMat(mat->rows(),3,CV_32FC1);
      // Copy the data
      memcpy(convMat.data, mat->data(), 3*mat->rows()*sizeof(float));
      // Reshape the data (but check if _imgRows is non negative)
      if (_imgRows > 0)
        convMat = convMat.reshape(3,_imgRows);
      else
        convMat = convMat.reshape(3, imgRows);
      //convMat *= 255;
      // And convert from LAB to BGR
      cv::cvtColor(convMat, convMat, cv::COLOR_Lab2BGR);
      // Scale the data by 255
      convMat *= 255;
      // And convert the data
      cv::Mat outputImage;
      convMat.convertTo(outputImage,CV_8UC3);
      // Finally, save the
      cv::imwrite(path, outputImage);
      outputImage.release();
      convMat.release();
      return 1;
    }
    return -2; //should not come here
  }//end of function

}//end of namespace
