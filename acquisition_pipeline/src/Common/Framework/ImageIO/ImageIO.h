#ifndef IMAGEIO_H_
#define IMAGEIO_H_
#include <string>
#include <memory>

namespace spin
{
  // Forward declaration
  struct myImage;

  // Class for reading RGB images
  class RGBImageIO
  {
    public:
      // Constructors
      RGBImageIO(std::string& v);
      RGBImageIO(){}
      // Default destructor
      ~RGBImageIO(){}
      // Reading a RGB image
      int ReadImage2(std::string& path, void* _img);
      // Reading a RGB image in different formats
      int ReadImage3(std::string& path, void* _img, void* _img2, bool a=false);
      // Reading a RGB image
      int ReadImage(std::string& v, void* p, bool a=false);
      // Reading a grayscale image
      int ReadImage_g(std::string& v, void* p, bool a=false);
      // Reading a RGB Image and returning a LAB image
      int ReadLABImage(std::string& path, void* _mat, void* _mask=NULL);
      // Saving a RGB image
      int WriteImage(std::string& v, void* p);
      // Saving a RGB Image from a LAB image
      int WriteLABImage(std::string& path, void* _mat, int _imgR=-1);
      // Return a RGB image from a LAB matrix
      int ReturnLAB2RGBImage(void* inMat, void* outMat, int _imgRows);
      // Extract channel
      int ExtractChannel(void* _img, int cNum, void* _channel, void* _mat);
    private:
      std::shared_ptr<myImage> ImagePtr;
      int imgRows;
  };//end of class
}//end of namespace
#endif
