#ifndef ABSTRACTLOGGER_H
#define ABSTRACTLOGGER_H

#include <QString>
#include <QHash>
#include <QDateTime>

#include "Framework/Logger/Common.h"

namespace spin
{

/**
 * @brief The LogLevel enum represents the severity level of log messages
 */
enum LogLevel
{
    Log_Debug,      /** Message severity debug */
    Log_Info,       /** Message severity info */
    Log_Warning,    /** Message severity warning */
    Log_Error,      /** Message severity error */
    Log_Fatal       /** Message severity fatal */
};


/**
 * @brief The LogMessage struct represents a log message with other context
 * details.
 */
struct LogMessage
{
    /**
     * @brief message The log message
     */
    QString   message;

    /**
     * @brief context Context in which the log message is generated.
     */
    QString   context;

    /**
     * @brief level The severity level of the message.
     */
    LogLevel  level;

    /**
     * @brief time Time at which the log message has been generated.
     */
    QDateTime time;
};



/**
 * @brief ILogTarget represents the interface which shall be implemented by the
 * classes of objects that receive the log message and writes it to a output
 * device, file, console, network or any other target.
 */
SPIN_INTERFACE ILogTarget
{
public:
    /**
     * @brief write Writes the log message to a log target.
     * @param message Message to be written to target.
     */
    virtual void write( LogMessage *message ) = 0;

    /**
     * @brief uniqueId An id that helps the logger identify this target when
     * it is registered with the logger.
     * @return
     */
    virtual QString uniqueId() = 0;

    /**
     * @brief close Closes the log target.
     */
    virtual void close() = 0;

    /**
     * @brief ~ILogTarget Virtual destructor as per convention.
     */
    virtual ~ILogTarget()
    {

    }
};



/**
 * @brief AbstractLogger class represents the base class of loggers. Loggers
 * can contain 0 or more log targets. A logger is responsible for receiving the
 * log message from anywhere in the application and write those messages to
 * all the registered targets. The sub-classes of this class can decide how
 * and when, when and in what form the log messages can be written to the
 * registered targets. The logger owns the registered targets.
 */
class AbstractLogger
{
public:
    /**
     * @brief debug Write debugging message to all the registered targets.
     * @param message Message to be written.
     * @param context Context at which the given message is generated.
     */
    void debug( QString message, QString context )
    {
        //Here the messages are filtered based on the log level
        if( m_level <= Log_Debug )
        {
            LogMessage *logMessage = new LogMessage;
            logMessage->message = message;
            logMessage->context = context;
            logMessage->level   = Log_Debug;
            logMessage->time    = QDateTime::currentDateTime();
            write( logMessage );
            delete logMessage;
        }
    }


    /**
     * @brief info Write information message to all the registered targets.
     * @param message Message to be written.
     * @param context Context at which the given message is generated.
     */
    void info( QString message, QString context )
    {
        if( m_level <= Log_Info )
        {
            LogMessage *logMessage = new LogMessage;
            logMessage->message = message;
            logMessage->context = context;
            logMessage->level   = Log_Info;
            logMessage->time    = QDateTime::currentDateTime();
            write( logMessage );
            delete logMessage;
        }
    }


    /**
     * @brief warning Write warning message to all the registered targets.
     * @param message Message to be written.
     * @param context Context at which the given message is generated.
     */
    void warning( QString message, QString context )
    {
        if( m_level <= Log_Warning )
        {
            LogMessage *logMessage = new LogMessage;
            logMessage->message = message;
            logMessage->context = context;
            logMessage->level   = Log_Warning;
            logMessage->time    = QDateTime::currentDateTime();
            write( logMessage );
            delete logMessage;
        }
    }


    /**
     * @brief error Write error message to all the registered targets.
     * @param message Message to be written.
     * @param context Context at which the given message is generated.
     */
    void error( QString message, QString context )
    {
        if( m_level <= Log_Error )
        {
            LogMessage *logMessage = new LogMessage;
            logMessage->message = message;
            logMessage->context = context;
            logMessage->level   = Log_Error;
            logMessage->time    = QDateTime::currentDateTime();
            write( logMessage );
            delete logMessage;
        }
    }


    /**
     * @brief fatal Write fatal error message to all the registered targets.
     * @param message Message to be written.
     * @param context Context at which the given message is generated.
     */
    void fatal( QString message, QString context )
    {
        if( m_level <= Log_Fatal )
        {
            LogMessage *logMessage = new LogMessage;
            logMessage->message = message;
            logMessage->context = context;
            logMessage->level   = Log_Fatal;
            logMessage->time    = QDateTime::currentDateTime();
            write( logMessage );
            delete logMessage;
        }
    }


    /**
     * @brief setLogLevel Sets the log level which is filter out the messages
     * based on the severity of the log messages.
     * @param level New log level.
     */
    void setLogLevel( LogLevel level )
    {
        m_level = level;
    }


    /**
     * @brief addTarget Adds a log target to the logger. The logger assumes the
     * ownership of the targets.
     * @param target A log target that has to be added to the logger.
     */
    void addTarget( ILogTarget *target )
    {
        if( (target) && ! m_targets.contains( target->uniqueId() ))
        {
            m_targets.insert( target->uniqueId(), target );
        }
    }


    /**
     * @brief removeTarget Removes a log target from the logger.
     * @param uniqueId Unique id of the logger that is to be removed from the
     * logger.
     * @return Returns true if the log target is found and is successfully
     * removed.
     */
    bool removeTarget( QString uniqueId )
    {
        if( m_targets.contains( uniqueId ))
        {
            /* The hash returns the number of targets removed which is casted to
             * to convert that result to bool.
             */
            return static_cast< bool >(
                        m_targets.remove( uniqueId ));
        }
        return false;
    }


    /**
     * @brief logTarget Gives a log target with given unique id.
     * @param id Unique id of the log target that is to be retrieved.
     * @return A registered log target with give id, if it exists otherwise
     * NULL.
     */
    ILogTarget* logTarget( QString id )
    {
        return m_targets.value( id, 0 );
    }


    /**
     * @brief close Closes all the log targets and deletes them.
     */
    void close()
    {
        // Iterate, close and delete the registered log targets.
        QHash< QString, ILogTarget *>::iterator it = m_targets.begin();
        for( ; it != m_targets.end(); ++ it )
        {
            ILogTarget *target = it.value();
            target->close();
            delete target;
        }
    }

    /**
     * @brief ~AbstractLogger Virtual destructor.
     */
    virtual ~AbstractLogger()
    {
        close();
    }

protected:
    /**
     * @brief AbstractLogger A protected default constructor that initializes
     * log level to Warning.
     */
    AbstractLogger()
        : m_level( Log_Warning )
    {
        //Nothing here
    }

    /**
     * @brief write Writes the log messages to registered log targets.
     * @param message Log message to be written to targets.
     */
    virtual void write( LogMessage *message ) = 0;

    /**
     * @brief m_level Log severity based on which the log messages will be
     * filtered.
     */
    LogLevel m_level;

    /**
     * @brief m_targets Mapping of log targets from their unique ids.
     */
    QHash< QString, ILogTarget *> m_targets;
};
}


#endif // SPIN_ABSTRACTLOGGER_H
