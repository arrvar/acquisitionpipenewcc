#ifndef LOGGER_INTERFACE_H_
#define LOGGER_INTERFACE_H_
#include "AbstractClasses/AbstractCallback.h"
// Boost includes
#include <boost/thread.hpp>
#include <memory>

namespace spin
{
  // Forward declaration of an Abstract Logger 
  class AbstractLogger;
  
  /*! \brief LoggerInterface
   *  This class is a placeholder for interfacing with a generic logging mechanism
   *  and storing the results in a stream. The stream in this case will be an
   *  ASCII file that will be saved to disk. By default, this file will be saved
   *  in the same folder from where the executable will be called. 
   */
  class LoggerInterface
  {
    public :
      // Constructor
      LoggerInterface():logFile("./spinVista_log.txt"){};
      // Destructor
      ~LoggerInterface(){}
      /*! \brief Set the filename for logging */
      void SetLoggingFile(std::string& s){logFile = s;}
      /*! \brief Initialize the logging process*/
      void Initialize();
      /*! \brief Catch Signal*/
      void ReceiveMessage(CallbackStruct p);
    private:
      // Members for configuring the data buffer storage
      std::string logFile;
      // A logger object
      std::shared_ptr<AbstractLogger> logger;
      // A mutex to help in writing operations
      boost::mutex  write_mutex;
  };// End of class
};//end of namespace
#endif
