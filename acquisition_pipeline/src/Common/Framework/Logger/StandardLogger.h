#ifndef STANDARDLOGGER_H
#define STANDARDLOGGER_H

#include <QTextStream>
#include <QFile>
#include <QDebug>

#include "Framework/Logger/AbstractLogger.h"

namespace spin
{
  /**
   * @brief The StandardLogger class A simple logger which writes the messages
   * to all the targets without delay. Also pushes the error messages to error
   * handler for GUI to display it wherever it gets the control.
   */
  class StandardLogger : public AbstractLogger
  {
  public:
    StandardLogger()
    {

    }

  protected:
    /**
     * @brief write Writes the message to all the targets.
     * @param message Message to be written.
     */
    void write( LogMessage *message )
    {
      if( message->level >= Log_Error )
      {
        /**
         * @todo push the error to error handler for later display.
         */
      }
      QHash< QString, ILogTarget *>::iterator it = m_targets.begin();
      for( ; it != m_targets.end(); ++ it )
      {
        ILogTarget *target = it.value();
        target->write( message );
      }
    }
  };



  /**
   * @brief The ConsoleTarget class Writes the log to console.
   */
  class ConsoleTarget : public ILogTarget
  {
  public:
      /**
       * @brief write Writes the log message to a log target.
       * @param message Message to be written to target.
       */
      void write( LogMessage *message )
      {
          QString msg;
          switch( message->level ) {
          case Log_Debug:
              msg = "[DEBUG] ";
              break;
          case Log_Info:
              msg = "[INFO] ";
              break;
          case Log_Warning:
              msg = "[WARN] ";
              break;
          case Log_Error:
              msg = "[ERROR] ";
              break;
          case Log_Fatal:
              msg = "[FATAL] ";
              break;
          default:
              msg = "[UNKNOWN]";
              break;
          }
          msg.append( message->time.toString( "dd-MM-yyyy HH:mm:ss" ))
             .append( " { ")
             .append( message->context.isEmpty() ? "" : message->context )
             .append( " } ")
                  .append( message->message );
          qDebug() << msg;
      }


      /**
       * @brief uniqueId An id that helps the logger identify this target when
       * it is registered with the logger.
       * @return Returns the unique id of the console log target
       */
      QString uniqueId()
      {
          return "Console_Log_Target";
      }


      /**
       * @brief close Closes the log target.
       */
      void close()
      {
          //Nothing to do here...
      }
  };


  /**
   * @brief The FileTarget class Writes the log to a file.
   */
  class FileTarget : public ILogTarget
  {
  public:
      explicit FileTarget( QString fileName )
      {
          m_logFile = new QFile( fileName );
          //m_logFile->remove();
          m_logFile->open( QIODevice::Append);
          m_logStream.setDevice( m_logFile );
      }


      /**
       * @brief ~FileTarget Virtual destructor
       */
      virtual ~FileTarget()
      {
          delete m_logFile;
      }


      /**
       * @brief write Writes the log message to a log target.
       * @param message Message to be written to target.
       */
      void write( LogMessage *message )
      {
          QString msg;
          switch( message->level )
          {
          case Log_Debug:
              msg = "[DEBUG] ";
              break;
          case Log_Info:
              msg = "[ INFO] ";
              break;
          case Log_Warning:
              msg = "[ WARN] ";
              break;
          case Log_Error:
              msg = "[ERROR] ";
              break;
          case Log_Fatal:
              msg = "[FATAL] ";
              break;
          default:
              msg = "[UNKNOWN]";
          }
          msg.append( message->time.toString( "dd-MM-yyyy HH:mm:ss" ))
             .append( " { ")
             .append( message->context.isEmpty() ? "" : message->context )
             .append( " } ")
             .append( message->message );
          m_logStream << msg << endl;
      }


      /**
       * @brief uniqueId An id that helps the logger identify this target when
       * it is registered with the logger.
       * @return
       */
      QString uniqueId()
      {
          return "File_Log_Target";
      }


      /**
       * @brief close Closes the log target.
       */
      void close()
      {
          m_logStream.flush();
          m_logFile->close();
      }

  private:
      /**
       * @brief m_logStream Text stream where the message is written
       */
      QTextStream m_logStream;

      /**
       * @brief m_logFile Log file.
       */
      QFile *m_logFile;
  };
} // end of spin namespace.

#endif // STANDARDLOGGER_H
