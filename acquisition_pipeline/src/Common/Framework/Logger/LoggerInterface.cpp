#include "Framework/Logger/LoggerInterface.h"
#include "Framework/Logger/StandardLogger.h"
#include <iostream>
namespace spin
{
  /*! \brief Initialize()
   *  This function initializes a logger object and keeps it ready for
   *  accepting logging calls
   */
  void LoggerInterface::Initialize()
  {
    logger = std::make_shared<StandardLogger>();
    // Convert the file name to a QString
    QString qs = QString::fromUtf8(logFile.c_str());
    // And instantiate a new target in the logger
    logger.get()->addTarget(new spin::FileTarget(qs));
  }//end of function

  /*! \brief ReceiveMessage
   *  This function is the slot that will be used to catch all
   *  logging messages that emanate from various parts of the
   *  program
   */
  void LoggerInterface::ReceiveMessage(CallbackStruct p)
  {
    // This is a standard information
    std::string pLog = p.Descr +"\n";
    pLog += std::to_string(p.Result)+"\n";
    pLog += p.Error;

    QString module = QString::fromUtf8(p.Module.c_str());
    QString msg = QString::fromUtf8(pLog.c_str());

    if (p.Result ==1)
    {
      // instantiate a lock so that others do not call this function
      write_mutex.lock();
      // Code break (new API)
      //logger.get()->logMessage(msg, module, LogLevel_Info);
      write_mutex.unlock();
    }
    else
    {
      // instantiate a lock so that others do not call this function
      write_mutex.lock();
      // Code break (new API)
      //logger.get()->logMessage(msg, module, LogLevel_Error);
      write_mutex.unlock();
    }
  }//end of function
}//end of namespace
