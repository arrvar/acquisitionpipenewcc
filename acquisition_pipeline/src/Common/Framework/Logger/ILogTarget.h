#ifndef SPIN_ILOGTARGET_H
#define SPIN_ILOGTARGET_H

#include <QDateTime>
#include <QString>

#include "Framework/Logger/Common.h"

namespace spin 
{
  /**
   * @brief The LogLevel enum represents the severity level of log messages.
   */
  enum LogLevel
  {
    /**
      * @brief LogLevel_Debug Denotes a debug message.
      */
    LogLevel_Debug,

    /**
      * @brief LogLevel_Info Denotes an information message.
      */
    LogLevel_Info,

    /**
      * @brief LogLevel_Warning Denotes a warning message.
      */
    LogLevel_Warning,

    /**
      * @brief LogLevel_Error Denotes an error message.
      */
    LogLevel_Error,

    /**
      * @brief LogLevel_Fatal Denotes a fatal message.
      */
    LogLevel_Fatal

  };// end of LogLevel enum.

  /**
   * @brief The LogMessage struct represents a log message with other context
   * details.
   */
  struct LogMessage
  {
    /**
      * @brief m_message The log message.
      */
    QString m_message;

    /**
      * @brief m_context Context in which the log message is generated.
      */
    QString m_context;

    /**
      * @brief m_level The severity level of the message.
      */
    LogLevel m_level;

    /**
      * @brief m_time Time at which the log message has been generated.
      */
    QDateTime m_time;

  };// end of LogMessage struct.

  /**
   * @brief ILogTarget represents the interface which shall be implemented by the
   * classes of objects that receive the log message and writes it to a output
   * device, file, console, network or any other target.
   */
  SPIN_INTERFACE ILogTarget
  {
  public:
    /**
      * @brief ~ILogTarget Virtual destructor.
      */
    virtual ~ILogTarget()
    {

    }

    /**
      * @brief write Writes the log message to a log target.
      * @param message Message to be written to target.
      */
    virtual void write(const LogMessage& message) = 0;

    /**
      * @brief uniqueId An id that helps the logger identify this target when
      * it is registered with the logger.
      * @return Returns the unique ID of the target.
      */
    virtual QString uniqueId() = 0;

    /**
      * @brief close Closes the log target.
      */
    virtual void close() = 0;

  };// end of ILogTarget class.
}   // end of spin namespace.

#endif // SPIN_ILOGTARGET_H
