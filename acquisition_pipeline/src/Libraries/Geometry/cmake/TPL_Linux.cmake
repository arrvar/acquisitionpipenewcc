#-----------------------------------------------------------------------------------#
#Boost
#-----------------------------------------------------------------------------------#
SET(Boost_LIBS
    filesystem
    thread
    date_time
    chrono
    system
    timer
    serialization
)
FIND_PACKAGE(Boost REQUIRED COMPONENTS "${Boost_LIBS}")
#Here, we add a global definition indicating that only dynamic linking is possible
#Also make sure that /MD flags are set for all your CMake projects
#https://svn.boost.org/trac/boost/ticket/6644
ADD_DEFINITIONS(-DBOOST_ALL_DYN_LINK )
#Include the path of the boost include directory
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
#and the library directories
LINK_DIRECTORIES(${Boost_LIBRARY_DIR})
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${Boost_LIBRARIES})

#-----------------------------------------------------------------------------------#
#ITK
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(ITK REQUIRED)
INCLUDE(${ITK_USE_FILE})
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${ITK_LIBRARIES})

#-----------------------------------------------------------------------------------#
#Eigen
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(Eigen3 REQUIRED)
INCLUDE_DIRECTORIES(${EIGEN3_INCLUDE_DIR})

#-----------------------------------------------------------------------------------#
#Qt5
#-----------------------------------------------------------------------------------#
SET(QT5_LIBRARIES
    Qt5Core
    Qt5Sql
)
FOREACH(Package ${QT5_LIBRARIES})
  FIND_PACKAGE(${Package})
  #Though this is not required, we do it so as to preserve the structure of the
  #third party library folder. All these can be substituted by the qt5_use_modules 
  #macro
  #http://qt-project.org/doc/qt-5.0/qtdoc/cmake-manual.html
  INCLUDE_DIRECTORIES(${${Package}_INCLUDE_DIRS})
  ADD_DEFINITIONS(${${Package}_DEFINITIONS})
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${${Package}_EXECUTABLE_COMPILE_FLAGS}")
  SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${${Package}_LIBRARIES})
ENDFOREACH()

#-----------------------------------------------------------------------------------#
#OpenCV
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(OpenCV REQUIRED)
INCLUDE_DIRECTORIES(${OpenCV_INCLUDE_DIRS})
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${OpenCV_LIBS} ${OpenCV_LIBRARIES})

#-----------------------------------------------------------------------------------#
#DLIBXX
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(DLIBXX REQUIRED)
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${DLIBXX_LIBRARIES})

IF(ENABLE_OPENCL)
  #----------------------------------------------------------------------------#
  #OpenCL
  #----------------------------------------------------------------------------#
  FIND_PACKAGE( OpenCL REQUIRED)
  SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${OpenCL_LIBRARY})
ENDIF()
