#include "AbstractClasses/AbstractMatrixTypes.h"
#include "Utilities/Geometry/PolygonUtils.h"
#include <boost/polygon/polygon.hpp>
#include <fstream>
#include <sstream>
#include <string>


using namespace spin;

//https://stackoverflow.com/questions/7868936/read-file-line-by-line-using-ifstream-in-c
void PopulateMatrixFromFile( std::string& file, RMMatrix_Float& data)
{
  std::ifstream infile(file.c_str());
  std::string line;
  auto count = 0;
  while (std::getline(infile, line))
  {
    //std::cout<<line<<std::endl;
    std::istringstream iss(line);
    float a, b;
    if (!(iss >> a >> b ))
    { 
      return; 
    }
    //std::cout<<"parsed: "<<a<<" "<<b<<std::endl;

    //add the values to the matrix
    if (count == 0)
    {
      data = RMMatrix_Float(1,2);
      data(0,0) = a;
      data(0,1) = b;
    }
    else
    {
      // Append the line to the existing matrix
      RMMatrix_Float temp = data;
      data = RMMatrix_Float::Zero(temp.rows()+1,temp.cols());
      RMMatrix_Float temp1 = RMMatrix_Float(1,2);
      temp1(0,0) = a;
      temp1(0,1) = b;
      data<<temp,temp1;
    }
    ++count;
  }
}//end of function

int main(int argc, char* argv[])
{
  if (argc < 4)
  {
    std::cout<<argv[0]<<" <model_file> <file_to_be_check.txt> <0: across_rows, 1: across_cols> <file_out.txt>"<<std::endl;
    return -1;
  }
  
  // Loads a pre-existing model file
  PolygonUtils utils;
  std::string modelFile = std::string(argv[1]);
  int h = utils.LoadModel(modelFile);
  if (h !=1)
  {
    std::cout<<"Model file could not be loaded"<<std::endl;
    return -2;
  }
  
  // Parse the input file and populate 
  std::string fFile = std::string(argv[2]);
  RMMatrix_Float testData;
  PopulateMatrixFromFile(fFile, testData);
  if (testData.rows() <=0)
  {
    std::cout<<"Test data could not be parsed"<<std::endl;
    return -3;
  }
  
  std::vector< std::string > dump;
  RMMatrix_Float pExp;
  int dir = atoi(argv[3]);
  if (dir == 1)
    h = utils.GetPolygon((int)PolygonSOZ::ACROSS_COLS, &pExp);
  else
    h = utils.GetPolygon((int)PolygonSOZ::ACROSS_ROWS, &pExp);

  std::cout<<pExp<<std::endl;
  std::string t1="1";
  std::string t2="0";
  // Now for each data point, check if it is within or outside the 
  // model polygon
  for (int j = 0; j < testData.rows(); ++j)
  {
    std::pair<double,double> k = std::make_pair(testData(j,0),testData(j,1));
    bool b;
    if (dir == 0)
      utils.CheckPointInsidePolygon((int)PolygonSOZ::ACROSS_ROWS, &k, &b);
    else
      utils.CheckPointInsidePolygon((int)PolygonSOZ::ACROSS_COLS, &k, &b);


    if (b == true)
    {
      std::string k1=std::to_string(testData(j,0))+"\t"+std::to_string(testData(j,1))+"\t"+t1;
      dump.push_back(k1);
    }
    else
    {
      std::string k2=std::to_string(testData(j,0))+"\t"+std::to_string(testData(j,1))+"\t"+t2;
      dump.push_back(k2);
    }
  }
  
  // Save the result to the output file 
  std::ofstream output_file(argv[4]);
  std::ostream_iterator<std::string> output_iterator(output_file, "\n");
  std::copy(dump.begin(), dump.end(), output_iterator);
  return 1;
}//end of function
