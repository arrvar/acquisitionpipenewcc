#include "AbstractClasses/AbstractMatrixTypes.h"
#include "Utilities/Geometry/PolygonUtils.h"
#include <boost/polygon/polygon.hpp>
#include <fstream>
#include <sstream>
#include <string>
using namespace spin;

//https://stackoverflow.com/questions/7868936/read-file-line-by-line-using-ifstream-in-c
void PopulateMatrixFromFile( std::string& file, RMMatrix_Float& data)
{
  std::ifstream infile(file.c_str());
  std::string line;
  auto count = 0;
  while (std::getline(infile, line))
  {
    //std::cout<<line<<std::endl;
    std::istringstream iss(line);
    float a, b;
    if (!(iss >> a >> b ))
    {
      return;
    }
    //std::cout<<"parsed: "<<a<<" "<<b<<std::endl;

    //add the values to the matrix
    if (count == 0)
    {
      data = RMMatrix_Float(1,2);
      data(0,0) = a;
      data(0,1) = b;
    }
    else
    {
      // Append the line to the existing matrix
      RMMatrix_Float temp = data;
      data = RMMatrix_Float::Zero(temp.rows()+1,temp.cols());
      RMMatrix_Float temp1 = RMMatrix_Float(1,2);
      temp1(0,0) = a;
      temp1(0,1) = b;
      data<<temp,temp1;
    }
    ++count;
  }
}//end of function

int main(int argc, char* argv[])
{
  if (argc < 6)
  {
    std::cout<<argv[0]<<" <0:Across_Rows, 1:Across_Cols> <primary.txt> <0:Across_Rows, 1:Across_Cols> <secondary.txt> <model_name> <expansion_factor>"<<std::endl;
    return -1;
  }
  std::string pName = std::string(argv[2]);
  int pDir = atoi(argv[1]);
  std::string sName = std::string(argv[4]);
  int sDir = atoi(argv[3]);
  std::string modelName = std::string(argv[5]);
  //float f = 0.4;
  //if (argc > 6)
  //  f = atof(argv[6]);

  // Create two sets of indices representing convex hull's of primray and 
  // secondary directions
  /*std::vector< std::pair<double,double> > primary=\
  {
    std::make_pair(-1552, -120),\
    std::make_pair(-1512, -112),\
    std::make_pair(-1556, -108),\
    std::make_pair(-1568, -108),\
    std::make_pair(-1572, -116),\
    std::make_pair(-1572, -112),\
    std::make_pair(-1560, -120),\
    std::make_pair(-1552, -120)
  };
  
  std::vector< std::pair<double,double> > secondary=\
  {
    std::make_pair(80, -1024),\
    std::make_pair(80, -1008),\
    std::make_pair(76, -996),\
    std::make_pair(52, -984),\
    std::make_pair(72, -1024),\
    std::make_pair(80, -1024)
  };*/
  
  /*RMMatrix_Float primary = RMMatrix_Float::Zero(7,2);
  RMMatrix_Float secondary = RMMatrix_Float::Zero(6,2);
  primary(0,0) = -1512;primary(0,1) = -112;
  primary(1,0) = -1548;primary(1,1) = -84;
  primary(2,0) = -1588;primary(2,1) = -112;
  primary(3,0) = -1592;primary(3,1) = -116;
  primary(4,0) = -1564;primary(4,1) = -120;
  primary(5,0) = -1552;primary(5,1) = -120;
  primary(6,0) = -1512;primary(6,1) = -112;

  secondary(0,0) = 80;secondary(0,1) = -1024;
  secondary(1,0) = 80;secondary(1,1) = -1008;
  secondary(2,0) = 76;secondary(2,1) = -996;
  secondary(3,0) = 52;secondary(3,1) = -984;
  secondary(4,0) = 72;secondary(4,1) = -1024;
  secondary(5,0) = 80;secondary(5,1) = -1024;
  */

  RMMatrix_Float primary;
  PopulateMatrixFromFile(pName, primary);
  RMMatrix_Float secondary;
  PopulateMatrixFromFile(sName, secondary);
  
  // Instantiate an object
  PolygonUtils utils;
  utils.Initialize();
  if (pDir == 1 && sDir == 0)
  {
    utils.AddPolygon((int)PolygonSOZ::ACROSS_COLS, &primary);
    utils.AddPolygon((int)PolygonSOZ::ACROSS_ROWS, &secondary);
  }
  else
  {
    utils.AddPolygon((int)PolygonSOZ::ACROSS_ROWS, &primary);
    utils.AddPolygon((int)PolygonSOZ::ACROSS_COLS, &secondary);
  }

  //float f = 0.4;
  int h = 0;
  //h = utils.ResizePolygon((int)PolygonSOZ::ACROSS_COLS,f);
  //h = utils.ResizePolygon((int)PolygonSOZ::ACROSS_ROWS,f);
  
  RMMatrix_Float pExp;
  h = utils.GetPolygon((int)PolygonSOZ::ACROSS_COLS, &pExp);
  std::cout<<pExp<<std::endl;
  std::cout<<"================="<<std::endl;
  h = utils.GetPolygon((int)PolygonSOZ::ACROSS_ROWS, &pExp);
  std::cout<<pExp<<std::endl;
  std::cout<<"================="<<std::endl;


 // std::string modelFile = std::string(argv[1]);
  h = utils.SaveModel(modelName);

  /*PolygonUtils utils2;
  h = utils2.LoadModel(modelFile);

  /*for (int count = 0; count < primary.rows(); ++count)
  {
    std::pair<double,double> k = std::make_pair(primary(count,0),primary(count,1));
    bool b;
    utils2.CheckPointInsidePolygon((int)PolygonSOZ::ACROSS_COLS, &k, &b);
    std::cout<<"count1: "<<b<<std::endl;
  }

  std::cout<<"================="<<std::endl;
  for (int count = 0; count < secondary.rows(); ++count)
  {
    std::pair<double,double> k = std::make_pair(secondary(count,0),secondary(count,1));
    bool b;
    utils2.CheckPointInsidePolygon((int)PolygonSOZ::ACROSS_ROWS, &k, &b);
    std::cout<<"count2: "<<b<<std::endl;
  }
  */

   bool gh;
   std::pair<double,double> test = std::make_pair(primary(100,0),primary(100,1));
   utils.CheckPointInsidePolygon((int)PolygonSOZ::ACROSS_COLS, &test, &gh);
   std::cout<<gh<<std::endl;

  if (h !=1)
  {
    std::cout<<h<<std::endl;
    return -1;
  }

  
  return 1;
}
