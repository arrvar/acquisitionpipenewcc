#ifndef IMAGEWRITERPIPELINE_H_
#define IMAGEWRITERPIPELINE_H_

// pipeline includes
#include "AbstractClasses/AbstractConsumer.h"
#include "AbstractClasses/AbstractCallback.h"
// Boost includes
#include <boost/thread.hpp>

namespace spin
{
  namespace vista
  {
    /*! \brief PreprocessingPipeline
     *  This class acts as the interface with the stitching pipeline and
     *  in essence is the interface for the library of the
     *  Spin Stitching Pipeline
     */
    template <class P>
    class ImageWriterPipeline : public spin::AbstractConsumer<P>
    {
      public:
      // Signals
        spin::VoidSignal sig_GetData;

        // Default destructor
        ~ImageWriterPipeline();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* dataPoint, void* obj);

        /*! \brief ProcessData*/
        void ProcessData(P dataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        void CompleteProcessing(void* obj);

        /*! \brief AbortPipeline*/
        void AbortPipeline();

        /*! \brief Reimplemented functions */
        void ConsumeFromQueue(P dataPoint, void* obj);
        void RequestQueue(void* obj);

        // Signals for metrics and logging
        spin::CallbackSignal sig_Logs;
        spin::CallbackSignal sig_Metrics;
        spin::ModuleSignal   sig_Finished;
        spin::FOVSignal      sig_FOV;
      private :
        // Thread to process the data
        boost::thread t_Process;
        boost::thread t_ImageWriter;
        boost::mutex  abort_mutex;
        bool          abort;

        /*! \brief Special functions*/
        void AbortOperations();
        void BeginProcessingQueue(void* obj);
        void RelayLogs(spin::CallbackStruct p);
        void RelayMetrics(spin::CallbackStruct p);
        void RelaySignal(spin::ModuleStatus p);

        spin::ModuleStatus mStatus;
    };//end of class
  }//end of namespace stitching
}//end of namespace spin

#endif
