// Pipeline includes
#include "Libraries/vista/Pipeline/Acquisition/ImageWriter/ImageWriterPipeline.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/spinVistaAcquisitionObject.h"

namespace spin
{
  namespace vista
  {
    /*! \brief Destructor */
    template<class P>
    ImageWriterPipeline<P>::~ImageWriterPipeline()
    {
      // Call the destructor in the sub routines as well
      AbortPipeline();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageWriterPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelaySignalFinished
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageWriterPipeline<P>::RelaySignal(spin::ModuleStatus p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageWriterPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline.
     */
    template<class P>
    int ImageWriterPipeline<P>::InstantiatePipeline(void* obj)
    {
      abort = false;
      // TODO: Some other initializations may be needed for acquisition from the camera
      return 1;
    }//end of function

    /*! \brief ProcessPipeline
     * Processes the data that is input from the source. Internally it pushes
     * the data into an appropriate data structure and then to a queue for
     * any consumer to pick it up.
     * parameters :
     * @producerDataPoint : The pointer to a data point from the producer.
     * @obj               : The spinVista object
     */
    template<class P>
    int ImageWriterPipeline<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      if (gObject == NULL) return -1;
      // Get information
      GridMapGeneration* gmap = gObject->gMap.get();
      GridInfo* gInfo         = &(gmap->GetGridHeader());
      std::map<pDouble, FOVStruct>* dFov = gmap->GetGridMapping();
      pDouble it(producerDataPoint->gYRow, producerDataPoint->gXCol);
      // Create the image filename
      std::string imgNameRaw = gObject->slideDir + "/"+(*dFov)[it].aoiName+gObject->file_extension;
      // Save the image
      gObject->ImageIO.get()->WriteImage(imgNameRaw, &((*dFov)[it].Img));
      // Send a signal to this effect
      (*dFov)[it].f_write = true;
      spin::FovCompleted p;
      p.Module = 3;//indicates image writer
      p.gYRow  = producerDataPoint->gYRow;
      p.gXCol  = producerDataPoint->gXCol;
      sig_FOV(p);
      return 1;
    }//end of function

    /*! \brief ProcessData
     * Chains up the process pipelines that need to be sequenced for the data
     * point
    */
    template <class P>
    void ImageWriterPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // Process the data point
      ProcessPipeline(&dataPoint, obj);
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     * This function ensures that processing is finished
     */
    template <class P>
    void ImageWriterPipeline<P>::CompleteProcessing(void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);

      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();

        // Indicate that this module has completed by marking the
        // approrpiate location in the module registry
        gObject->mModuleStatus[mStatus.Module] = true;
        // And Send a completion signal to the calling thread
        sig_Finished(mStatus);
      }
      return;
    }//end of function

    /*! \brief BeginProcessingQueue [SLOT]
     * This function Sends a request to the queue in the broker for data
     */
    template <class P>
    void ImageWriterPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      GridMapGeneration* gmap = gObject->gMap.get();

      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetGridHeader().gridWidth * gmap->GetGridHeader().gridHeight;

      while(n_images < max_images)
      {
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_ImageWriter.joinable())
        {
          t_ImageWriter.join();
          ++n_images;
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
      }
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     * This function receives a signal from the MPC with a new data point
     */
    template <class P>
    void ImageWriterPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      // Process the data point in a separate thread
      t_ImageWriter  = boost::thread(&ImageWriterPipeline<P>::ProcessData,this,dataPoint,obj);
    }//end of function

    /*! \brief RequestQueue [SLOT]
    * This function receives a signal from the MPC that a new datapoint is
    * available for consumption
    */
    template <class P>
    void ImageWriterPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&ImageWriterPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts the pipelines and cleanly exits
     */
    template <class P>
    void ImageWriterPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
      AbortOperations();
    }//end of function

    /*! \brief AbortOperations
     *	This function forwards the abort signal to other consumers and
     *	deallocates any memory objects that are part of this class alone.
     *	This however does not destroy the configuration for which this
     *  pipeline was primed with. Thereby enabling reacquisition instantly.
     */
    template <class P>
    void ImageWriterPipeline<P>::AbortOperations()
    {
    }//end of function

    // explicit instantiation of template class
    template class ImageWriterPipeline<ProducerDataType>;
  }//end of namespace vista
}//end of namespace spin
