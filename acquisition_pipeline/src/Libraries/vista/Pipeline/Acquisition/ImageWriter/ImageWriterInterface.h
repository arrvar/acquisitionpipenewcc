#ifndef IMAGWRITERINTERFACE_H_
#define IMAGWRITERINTERFACE_H_
// Pipeline includes
#include "AbstractClasses/AbstractProducer.h"
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  namespace vista
  {
    /*! \brief ImageWriterInterface
     *  This class is the interface between the image writer source and the
     *  actual writing pipeline framework
     */
    template <class P>
    class ImageWriterInterface : public spin::AbstractProducer
    {
      public:
        // Signals
        // Add data point to queue in broker
        boost::signals2::signal<void(P)> sig_AddQ;
        // Finished acquisition
        spin::VoidSignal sig_FinAcq;
        // Save image
        spin::CharSignal sig_SaveImageToDisk;
        // Abort Operations
        spin::VoidSignal sig_Abort;

        // Reimplemented function
        void AddToQueue(void* dataPoint);

    };//end of class
  }//end of namespace vista
}//end of namespace spin
#endif
