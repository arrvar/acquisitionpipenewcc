#include "Libraries/vista/Pipeline/Acquisition/ImageWriter/ImageWriterInterface.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  namespace vista
  {
    /*! \brief AddToQueue
     *  This function adds an object to the Pre-processing queue
     */
    template<class P>
    void ImageWriterInterface<P>::AddToQueue(void* dataPoint)
    {
      // typecasts
      ProducerDataType* dp = static_cast<ProducerDataType*>(dataPoint);
      //emit the signal
      sig_AddQ(*dp);
    }//end of function

    // explicit instantiation of template class
    template class ImageWriterInterface<ProducerDataType>;
  }//end of namespace vista
}//end of spin namespace
