#ifndef TRANSLATIONESTIMATIONPIPELINE_H_
#define TRANSLATIONESTIMATIONPIPELINE_H_

// pipeline includes
#include "AbstractClasses/AbstractConsumer.h"
#include "AbstractClasses/AbstractDataStructures.h"
#include "AbstractClasses/AbstractCallback.h"
// Boost includes
#include <boost/thread.hpp>

namespace spin
{
  // Forward declaration
  class TranslationEstimation;
  namespace vista
  {
    /*! \brief TranslationEstimationPipeline
     *  This class acts as the interface with the translation estimation
     *  module.
     */
    template <class P>
    class TranslationEstimationPipeline : public AbstractConsumer<P>
    {
      public:
        // Signals
        spin::VoidSignal      sig_GetData;
        spin::CallbackSignal  sig_Logs;
        spin::CallbackSignal  sig_Metrics;
        spin::ModuleSignal    sig_Finished;
        spin::FOVSignal       sig_FOV;

        // Default destructor
        ~TranslationEstimationPipeline();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* dataPoint, void* obj);

        /*! \brief ProcessData*/
        void ProcessData(P dataPoint, void* obj);

        /*! \brief AbortPipeline*/
        void AbortPipeline();

        /*! \brief Optimize */
        int Optimize(void* obj);

        /*! \brief Reimplemented functions */
        void ConsumeFromQueue(P dataPoint, void* obj);
        void RequestQueue(void* obj);
        void CompleteProcessing(void* obj);

      private :
        // Thread to process the data
        boost::thread t_Process;
        boost::thread t_TranslationEstimationProcess;
        boost::mutex  abort_mutex;
        bool          abort;
        std::shared_ptr<TranslationEstimation> ts;

        /*! \brief Special functions*/
        void AbortOperations();
        void BeginProcessingQueue(void* obj);
        void RelayLogs(spin::CallbackStruct p);
        void RelayMetrics(spin::CallbackStruct p);
        void RelayFOVCompletion(spin::FovCompleted p);
        spin::ModuleStatus mStatus;
    };//end of class
  }//end of namespace vista
}//end of namespace spin

#endif
