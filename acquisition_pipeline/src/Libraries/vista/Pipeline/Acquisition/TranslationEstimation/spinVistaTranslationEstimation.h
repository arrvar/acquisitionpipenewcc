#ifndef SPINTRANSLATIONESTIMATION_H_
#define SPINTRANSLATIONESTIMATION_H_

// Pipeline includes
#include <memory>
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  // Forward declaration of a producer/consumer framework
  template <class P>
  class MPC;

  namespace vista
  {
    // Forward declaration of a database class
    class DatabaseIO;
    template <class P> class TranslationEstimationInterface;
    template <class P> class TranslationEstimationPipeline;

    /*! \brief
     * This class acts as the interface with the stitching pipeline and in essence
     * is the interface for the library of the Spin Stitching Pipeline
     */
    template <class P>
    class SpinTranslationEstimation
    {
      public:
        /*! \brief Destructor*/
        ~SpinTranslationEstimation();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* producerDataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        int CompleteProcessing();

        /*! \brief AbortPipeline*/
        int AbortPipeline();

        /*! \brief A relay signal*/
        spin::ModuleSignal   sig_Finished;
        spin::FOVSignal      sig_FOV;
      private:
        bool pause;
        // Create objects for the producer level, broker and consumer level
        std::shared_ptr< TranslationEstimationInterface<P> > mTranslationEstimationInterface;
        std::shared_ptr< TranslationEstimationPipeline<P> > mTranslationEstimationPipeline;
        std::shared_ptr< spin::MPC<P> > mMPC;
        std::shared_ptr< DatabaseIO > db;

        /*! \brief ConnectSignals*/
        int ConnectSignals(void* obj);
        /*! \brief Pause producer */
        void PauseProducer(bool state);
        /*! \brief RelaySignal*/
        void RelaySignal(spin::ModuleStatus p);
        /*! \brief RelayFOVCompleted*/
        void RelayFOVCompleted(spin::FovCompleted p);
      };
  }//end of namespace vista
}//end of namespace spin
#endif
