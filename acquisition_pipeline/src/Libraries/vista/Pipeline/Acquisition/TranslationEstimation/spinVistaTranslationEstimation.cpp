#include "Libraries/vista/Pipeline/Acquisition/TranslationEstimation/spinVistaTranslationEstimation.h"
#include "Libraries/vista/Pipeline/Acquisition/TranslationEstimation/TranslationEstimationInterface.h"
#include "Libraries/vista/Pipeline/Acquisition/TranslationEstimation/TranslationEstimationPipeline.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/spinVistaAcquisitionObject.h"
#include "Libraries/vista/Pipeline/Acquisition/DatabaseIO/DatabaseIO.h"
#include "Framework/MPC/MPC.h"
#include "Framework/Logger/LoggerInterface.h"

namespace spin
{
  namespace vista
  {
    /*! \brief PauseProducer
     *  enables a boolean to either pause or resume the producer
     */
    template <class P>
    void SpinTranslationEstimation<P>::PauseProducer(bool state)
    {
      pause = state;
    }//end of function

    /*! \brief ConnectSignals
     *  Connects signals and slots for all the members of this class here
     */
    template<class P>
    int SpinTranslationEstimation<P>::ConnectSignals(void* obj)
    {
      // Connect the corresponding signals and slots
      mMPC.get()->sig_DataAvailable.connect(boost::bind(&TranslationEstimationPipeline<P>::RequestQueue,mTranslationEstimationPipeline,obj));
      mMPC.get()->sig_DataPoint.connect(boost::bind(&TranslationEstimationPipeline<P>::ConsumeFromQueue,mTranslationEstimationPipeline, _1, _2));
      mMPC.get()->sig_FinAcq.connect(boost::bind(&TranslationEstimationPipeline<P>::CompleteProcessing,mTranslationEstimationPipeline,obj));
      mMPC.get()->sig_Abort.connect(boost::bind(&TranslationEstimationPipeline<P>::AbortPipeline,mTranslationEstimationPipeline));
      mMPC.get()->sig_QueueBurdened.connect(boost::bind(&SpinTranslationEstimation<P>::PauseProducer,this, _1));

      mTranslationEstimationInterface.get()->sig_AddQ.connect(boost::bind(&MPC<P>::AddToQueue, mMPC, _1));
      mTranslationEstimationInterface.get()->sig_FinAcq.connect(boost::bind(&MPC<P>::Finished, mMPC));
      mTranslationEstimationInterface.get()->sig_Abort.connect(boost::bind(&MPC<P>::Abort, mMPC));
      mTranslationEstimationPipeline.get()->sig_GetData.connect(boost::bind(&MPC<P>::PopFromQueue, mMPC, obj));
      mTranslationEstimationPipeline.get()->sig_Finished.connect(boost::bind(&SpinTranslationEstimation<P>::RelaySignal, this, _1));
      mTranslationEstimationPipeline.get()->sig_FOV.connect(boost::bind(&SpinTranslationEstimation<P>::RelayFOVCompleted, this, _1));

      // Typecast the object
      // Here, we downcast a default object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);

      // Connect the logging signal
      if (gObject->loggingInitialized)
      {
        mMPC.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
        mTranslationEstimationPipeline.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
        //mTranslationEstimationPipeline.get()->sig_Finished.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
      }

      //Connect the metric signal
      if (gObject->metrics_translationestimation)
      {
        // connect the metric signal
        //mTranslationEstimationPipeline.get()->sig_Metrics.connect(boost::bind(&spin::vista::DatabaseIO::WriteToDatabase, gObject->db.get(), \
        //                                                                      gObject->dbFile,gObject->gridId, gObject->gMap.get(), _1));
      }

      return 0;
    }//end of function

    /*! \brief Destructor
     *  Clears all objects, joins all threads and exits
     */
    template<class P>
    SpinTranslationEstimation<P>::~SpinTranslationEstimation()
    {
      CompleteProcessing();
    }//end of function

    /*! \brief RelaySignal
     *  A slot to relay an incoming signal
     */
    template <class P>
    void SpinTranslationEstimation<P>::RelaySignal(spin::ModuleStatus p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief RelaySignal
     *  A slot to relay an incoming signal
     */
    template <class P>
    void SpinTranslationEstimation<P>::RelayFOVCompleted(spin::FovCompleted p)
    {
      sig_FOV(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline. Creates the objects and makes all the
     *  connections if necessary. It does not configure any data structure or
     *  set any constants as they would be dependent on the grid that is being
     *  @obj      : The spinVistaAcquisitionObject that has been initialized prior
     *              to calling this function
     */
    template<class P>
    int SpinTranslationEstimation<P>::InstantiatePipeline(void* obj)
    {
      // The producer can be either a camera or a File I/O interface
      mTranslationEstimationInterface = std::make_shared<TranslationEstimationInterface<P> >();

      // The consumer can be any process that can receive this data structure
      mTranslationEstimationPipeline = std::make_shared<TranslationEstimationPipeline<P> >();

      // Both the producer and consumer communicate with each other via a Broker
      mMPC = std::make_shared< spin::MPC<P> >();

      // Typecast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);

      // Connect the logging signal
      if (gObject->loggingInitialized)
      {
        mMPC.get()->EnableLogging();
      }

      // Instantiate the pipelines (lightweight in nature)
      mTranslationEstimationPipeline.get()->InstantiatePipeline(obj);

      // Connect all the signals and slots
      ConnectSignals(obj);

      return 0;
    }//end of function

    /*! \brief CompleteProcessing
     *  Completes the pipeline till the end and cleanly exits
     */
    template<class P>
    int SpinTranslationEstimation<P>::CompleteProcessing()
    {
      // Send a signal to indicate end of acquisition
      mTranslationEstimationInterface.get()->sig_FinAcq();

      return 0;
    }//end of function

    /*! \brief ProcessPipeline
     *  Processes the data that is input from the source. Internally it pushes
     *  the data into an appropriate data structure and then to a queue for
     *  any consumer to pick it up.
     */
    template<class P>
    int SpinTranslationEstimation<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // Push this data point into a queue, but check if this is in a paused state
      while(pause)
      {
        boost::this_thread::sleep(boost::posix_time::milliseconds(5));
      }

      // Push this data point into a queue
      mTranslationEstimationInterface.get()->AddToQueue(producerDataPoint);

      return 1;
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts all operations and cleanly exits
     */
    template<class P>
    int SpinTranslationEstimation<P>::AbortPipeline()
    {
      // Send a signal to indicate end of acquisition
      mTranslationEstimationInterface.get()->sig_Abort();

      return 0;
    }//end of function

    // explicit instantiation of template class
    template class SpinTranslationEstimation<ProducerDataType>;
  }//end of stitching namespace
}//end of spin namespace
