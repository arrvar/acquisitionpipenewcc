// Pipeline includes
#include "AbstractClasses/AbstractImageTypes.h"
#include "Libraries/vista/Pipeline/Acquisition/TranslationEstimation/TranslationEstimationPipeline.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/spinVistaAcquisitionObject.h"
#include "Utilities/Stitching/Acquisition/TranslationEstimation.h"
#include <chrono>
#include <fstream>
#include <iterator>

namespace spin
{
  namespace vista
  {
    /*! \brief Destructor
     * Finishes processing, deletes all objects and cleanly exits
     */
    template <class P>
    TranslationEstimationPipeline<P>::~TranslationEstimationPipeline()
    {
      AbortPipeline();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void TranslationEstimationPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelayMetrics
     *  A slot to relay an incoming signal
     */
    template <class P>
    void TranslationEstimationPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief RelayFOVCompletion
     *  A slot to relay that a given FOV has been completed and its FFT can be cleared
     */
    template <class P>
    void TranslationEstimationPipeline<P>::RelayFOVCompletion(spin::FovCompleted p)
    {
      // Push the information about FOV completion
      sig_FOV(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline.
     */
    template <class P>
    int TranslationEstimationPipeline<P>::InstantiatePipeline(void* obj)
    {
      typedef typename spin::TranslationEstimation TranslationEstimation;
      ts = std::make_shared<TranslationEstimation>();
      // Connect callback relay and FOV completion signals
      ts.get()->sig_Logs.connect(boost::bind(&TranslationEstimationPipeline<P>::RelayLogs,this,_1));
      ts.get()->sig_Metrics.connect(boost::bind(&TranslationEstimationPipeline<P>::RelayMetrics,this,_1));
      ts.get()->sig_FOV.connect(boost::bind(&TranslationEstimationPipeline<P>::RelayFOVCompletion,this,_1));

      abort = false;
      return 0;
    }//end of function

    /*! \brief ProcessPipeline
     *  This function estimates a displacement between the current image
     *  and its 4-connected neighbors (if the current image is deemed a
     *  foreground).
     */
    template <class P>
    int TranslationEstimationPipeline<P>::ProcessPipeline(P* dataPoint, void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      if (gObject == NULL) return -6;

      GridMapGeneration* gmap = gObject->gMap.get();
      std::map<pDouble, FOVStruct>* dFov = gmap->GetGridMapping();

      // Get the index of the FOV from the grid map and populate
      // elements in gmap
      pDouble it(dataPoint->gYRow, dataPoint->gXCol);
      (*dFov)[it].actDimensions = std::make_pair(gObject->imgWidth, gObject->imgHeight);

      // Compute a sub-pixel level displacement
      int val = ts.get()->ComputeDisplacement(&it, \
                                              gObject->fftUtils1.get(),\
                                              gObject->fftUtils2.get(),\
                                              gObject->fft2d.get(),\
                                              gObject->unbiasedCost,\
                                              gmap,\
                                              gObject->disp1_X,\
                                              gObject->disp1_Y,\
                                              gObject->disp2_X,\
                                              gObject->disp2_Y,\
                                              gObject->default_horizontal, \
                                              gObject->default_vertical, \
                                              gObject->ignoreBG,\
                                              gObject->ignoreBacklash,\
                                              &gObject->polygon,\
                                              gObject->log_translationestimation,\
                                              gObject->calibration,\
                                              &gObject->primary_disp,\
                                              &gObject->secondary_disp,\
                                              &gObject->primary_disp_meta,\
                                              &gObject->secondary_disp_meta);
      if (val !=1) return -1;

      return 1;
    }//end of function

    /*! \brief Optimize
     *  This function implements a global optimization of the
     *  displacements using a MST algorithm or any bundle adjustment
     *  pipeline.
     */
    template <class P>
    int TranslationEstimationPipeline<P>::Optimize(void* obj)
    {
      // downcast the object
      // Here, we downcast a default object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      if (gObject == NULL) return -6;

      // Don't do anything here if calibration is turned on
      if (!gObject->calibration)
      {
        GridMapGeneration* gmap  = gObject->gMap.get();

        // 1) Use the MST algorithm to get the right context between image pairs
        int val = -1;
        if (gObject->graphType==0)
        {
          val = ts->GlobalOptimizationMST( gmap,gObject->slideDir,gObject->modelFile);
        }
        else
        if (gObject->graphType==1)
        {
          val = ts->GlobalOptimizationSSP( gmap,gObject->slideDir,gObject->modelFile);
        }
        if (val !=1) return val;
      }
      else
      {
        /*// save the data to a calibration file
        std::ofstream output_file1("./calibration_H_x.txt");
        std::ostream_iterator<float> output_iterator1(output_file1, "\n");
        std::copy(gObject->H_x.begin(), gObject->H_x.end(), output_iterator1);
        std::ofstream output_file2("./calibration_H_y.txt");
        std::ostream_iterator<float> output_iterator2(output_file2, "\n");
        std::copy(gObject->H_y.begin(), gObject->H_y.end(), output_iterator2);
        std::ofstream output_file3("./calibration_V_x.txt");
        std::ostream_iterator<float> output_iterator3(output_file3, "\n");
        std::copy(gObject->V_x.begin(), gObject->V_x.end(), output_iterator3);
        std::ofstream output_file4("./calibration_V_y.txt");
        std::ostream_iterator<float> output_iterator4(output_file4, "\n");
        std::copy(gObject->V_y.begin(), gObject->V_y.end(), output_iterator4);*/

        // save the data to calibration _files
        std::string primary_direction = gObject->calibration_dir+"/primary_dir.txt";
        std::string primary_direction_meta = gObject->calibration_dir+"/primary_dir_meta.txt";
        std::string secondary_direction = gObject->calibration_dir+"/secondary_dir.txt";
        std::string secondary_direction_meta = gObject->calibration_dir+"/secondary_dir_meta.txt";

        std::ofstream output_file1(primary_direction.c_str());
        std::ostream_iterator<std::string> output_iterator1(output_file1, "\n");
        std::copy(gObject->primary_disp.begin(), gObject->primary_disp.end(), output_iterator1);

        std::ofstream output_file2(secondary_direction.c_str());
        std::ostream_iterator<std::string> output_iterator2(output_file2, "\n");
        std::copy(gObject->secondary_disp.begin(), gObject->secondary_disp.end(), output_iterator2);


        std::ofstream output_file3(primary_direction_meta.c_str());
        std::ostream_iterator<std::string> output_iterator3(output_file3, "\n");
        std::copy(gObject->primary_disp_meta.begin(), gObject->primary_disp_meta.end(), output_iterator3);

        std::ofstream output_file4(secondary_direction_meta.c_str());
        std::ostream_iterator<std::string> output_iterator4(output_file4, "\n");
        std::copy(gObject->secondary_disp_meta.begin(), gObject->secondary_disp_meta.end(), output_iterator4);
      }
      return 1;
    }//end of function

    /*\brief ProcessData
     * Processes the data point that is input by running it through the required
     * pipelines
     */
    template <class P>
    void TranslationEstimationPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // TranslationEstimation
      ProcessPipeline(&dataPoint, obj);
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     * This function receives a signal from the MPC with a new data point
     */
    template <class P>
    void TranslationEstimationPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      // Process the data point in a separate thread
      t_TranslationEstimationProcess = \
      boost::thread(&TranslationEstimationPipeline<P>::ProcessData, this, dataPoint, obj);
    }///end of function

    /*! \brief RequestQueue [SLOT]
     * This function receives a signal from the MPC that a new datapoint is
     * available for consumption
     */
    template <class P>
    void TranslationEstimationPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&TranslationEstimationPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     *  This function ensures that processing is finished
     */
    template <class P>
    void TranslationEstimationPipeline<P>::CompleteProcessing(void* obj)
    {
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();
        // and implement Global optimization
        Optimize(obj);
        // Indicate that this module has completed by marking the
        // approrpiate location in the module registry
        aObj->mModuleStatus[2] = true;
        // And Send a completion signal to the calling thread
        sig_Finished(mStatus);
      }
    }//end of function

    /*! \brief BeginProcessingQueue
     *  Monitors the queue and keeps the consumer alive unitl all data has been
     *  processed or the pipeline is aborted
     */
    template <class P>
    void TranslationEstimationPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      // Here, we downcast a default object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);

      GridMapGeneration* gmap = gObject->gMap.get();

      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetGridHeader().gridWidth * gmap->GetGridHeader().gridHeight;

      while(n_images < max_images)
      {
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_TranslationEstimationProcess.joinable())
        {
          t_TranslationEstimationProcess.join();
          ++n_images;
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
      }//end processing all images
    }//end of function

    /*! \brief AbortPipeline
     * Aborts the pipelines and cleanly exits
     */
    template <class P>
    void TranslationEstimationPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
    }//end of function

    // explicit instantiation of template class
    template class TranslationEstimationPipeline<ProducerDataType>;
  }//end of namespace vista
}//end of namespace spin
