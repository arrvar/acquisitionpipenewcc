// Pipeline includes
#include "Libraries/vista/Pipeline/Acquisition/Preprocessing/PreprocessingPipeline.h"
#include "Libraries/vista/Pipeline/Acquisition/TranslationEstimation/spinVistaTranslationEstimation.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/spinVistaAcquisitionObject.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "Utilities/ImageProcessing/ImageUtils/ImageUtils.h"
#include "Utilities/AbstractProcessing/spinAbstractProcessingPipeline.h"

namespace spin
{
  namespace vista
  {
    /*! \brief Destructor */
    template<class P>
    PreprocessingPipeline<P>::~PreprocessingPipeline()
    {
      mPlugins.clear();
      // Call the destructor in the sub routines as well
      AbortPipeline();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void PreprocessingPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelaySignalFinished
     *  A slot to relay an incoming signal
     */
    template <class P>
    void PreprocessingPipeline<P>::RelaySignal(spin::ModuleStatus p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void PreprocessingPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief ClearData
     *  A slot to clear superfluos data associated with FFT
     */
    template <class P>
    void PreprocessingPipeline<P>::ClearData(void* obj, spin::FovCompleted p)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);

      GridMapGeneration* gmap = gObject->gMap.get();
      GridInfo* gInfo         = &(gmap->GetGridHeader());
      std::map<pDouble, FOVStruct>* dFov = gmap->GetGridMapping();
      pDouble it(p.gYRow, p.gXCol);

      // clear FFT data
      (*dFov)[it].fft.resize(0,0);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline.
     */
    template<class P>
    int PreprocessingPipeline<P>::InstantiatePipeline(void* obj)
    {
      // Instantiate the next set of pipelines, if requested
      // Here, we downcast a default object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      if (gObject == NULL) return -6;

      if (gObject->EstimateTranslation)
      {
        mTranslationEstimation = std::make_shared<SpinTranslationEstimation<P>>();
        mTranslationEstimation.get()->InstantiatePipeline(obj);
        mTranslationEstimation.get()->sig_Finished.connect(boost::bind(&PreprocessingPipeline<P>::RelaySignal,this,_1));
        mTranslationEstimation.get()->sig_FOV.connect(boost::bind(&PreprocessingPipeline<P>::ClearData,this,obj,_1));
      }
      abort = false;

      // Connect callback relay signals
      if (gObject->mCDF_Fg.data() && gObject->mPDF_Focus.data())
      {
        gObject->hUtils.get()->sig_Logs.connect(boost::bind(&PreprocessingPipeline<P>::RelayLogs,this,_1));
      }

      // Check if any plugins are present at this level (level 0)
      if (gObject->mPlugins[1].size() > 0)
      {
        for (auto i: gObject->mPlugins[1])
        {
          std::shared_ptr< spin::SpinAbstractProcessingPipeline<P> > pPlugin = std::make_shared<SpinAbstractProcessingPipeline<P> >();
          pPlugin.get()->InstantiatePipeline(obj, &i);
          pPlugin.get()->sig_Finished.connect(boost::bind(&PreprocessingPipeline<P>::RelaySignal,this,_1));
          mPlugins.push_back(pPlugin);
        }
      }

      return 1;
    }//end of function

    /*! \brief ProcessPipeline
     * Processes the data that is input from the source. Internally it pushes
     * the data into an appropriate data structure and then to a queue for
     * any consumer to pick it up.
     * parameters :
     * @P: The pointer to a data point from the producer. It contains
     *     an image that will need to be processed
     */
    template<class P>
    int PreprocessingPipeline<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      if (gObject == NULL) return -1;

      GridMapGeneration* gmap = gObject->gMap.get();
      GridInfo* gInfo         = &(gmap->GetGridHeader());
      std::map<pDouble, FOVStruct>* dFov = gmap->GetGridMapping();
      pDouble it(producerDataPoint->gYRow, producerDataPoint->gXCol);

      // Get the Fg/BG threshold
      float thresh = gObject->fgbgThresh;
      int bg = 0;
      switch(gObject->channel)
      {
        case 0:
          {
            typedef spin::BlueChannelPixelAccessor<unsigned char> pChannel;
            spin::ComputeFFTOfRGBImage<pChannel,float>( &(*dFov)[it].Img, \
                                                        gObject->fft2d.get(),\
                                                        &(*dFov)[it].fft,\
                                                        gObject->subsampling,\
                                                        gObject->optimalFFT,\
                                                        gObject->imgWidth_d,\
                                                        gObject->imgHeight_d,false);
          }
          break;
        case 1:
          {
            typedef spin::GreenChannelPixelAccessor<unsigned char> pChannel;
            spin::ComputeFFTOfRGBImage<pChannel,float>( &(*dFov)[it].Img, \
                                                        gObject->fft2d.get(),\
                                                        &(*dFov)[it].fft,\
                                                        gObject->subsampling,\
                                                        gObject->optimalFFT,\
                                                        gObject->imgWidth_d,\
                                                        gObject->imgHeight_d,false);
          }
          break;
        case 2:
          {
            typedef spin::RedChannelPixelAccessor<unsigned char> pChannel;
            spin::ComputeFFTOfRGBImage<pChannel,float>( &(*dFov)[it].Img, \
                                                        gObject->fft2d.get(),\
                                                        &(*dFov)[it].fft,\
                                                        gObject->subsampling,\
                                                        gObject->optimalFFT,\
                                                        gObject->imgWidth_d,\
                                                        gObject->imgHeight_d,false);
          }
          break;
        case -1:
          {
            // FFT of Grayscale image is being computed
            spin::ComputeFFTOfGrayscaleImage<float>(&(*dFov)[it].Img_g, \
                                                    gObject->fft2d.get(),\
                                                    &(*dFov)[it].fft,\
                                                    gObject->subsampling,\
                                                    gObject->optimalFFT,\
                                                    gObject->imgWidth_d,\
                                                    gObject->imgHeight_d,false);
          }
          break;
        default:
          {
            typedef spin::GreenChannelPixelAccessor<unsigned char> pChannel;
            spin::ComputeFFTOfRGBImage<pChannel,float>( &(*dFov)[it].Img, \
                                                        gObject->fft2d.get(),\
                                                        &(*dFov)[it].fft,\
                                                        gObject->subsampling,\
                                                        gObject->optimalFFT,\
                                                        gObject->imgWidth_d,\
                                                        gObject->imgHeight_d,false);
          }
          break;
      }

      // Here, we determine if the AOI is a FG or BG. By default it is a FG
      if (gObject->mCDF_Fg.data() && gObject->mPDF_Focus.data())
      {
        // Check if it is a BG/FG
        bg = \
        gObject->hUtils.get()->BackgroundDetection( &(*dFov)[it].fft,\
                                                    &gObject->mCDF_Fg,\
                                                    &gObject->mCDF_Bg,\
                                                    &gObject->mPDF_Focus,\
                                                    &gObject->mMaskFgBg,\
                                                    &gObject->mMaskFocus,\
                                                    gObject->fgbgThresh,\
                                                    gObject->focusThresh,\
                                                    &(*dFov)[it].bgRatio,\
                                                    &(*dFov)[it].focusRatio,\
                                                    gObject->log_preprocessing);

        // Update the calibration image only if the AOI was deemed a background image
        if (bg == 1)
        {
          // at this time we keep track of the calibration image
          if ((*dFov)[it].bgRatio > gInfo->bgRatio)
          {
            if ((*dFov)[it].bgRatio > bgRatio1)
            {
              // This is the new calibration image
              bgRatio1 = (*dFov)[it].bgRatio;
              gInfo->calibImg=  gObject->slideDir +
                                "/" + (*dFov)[it].aoiName + \
                                gObject->file_extension;
              if (bgRatio2 == -1)
              {
                bgRatio2 = (*dFov)[it].bgRatio;
                gInfo->normImg=  gObject->slideDir +
                                  "/" + (*dFov)[it].aoiName + \
                                 gObject->file_extension;
              }
            }

            if ((*dFov)[it].bgRatio > bgRatio2 && (*dFov)[it].bgRatio < bgRatio1)
            {
              bgRatio2 = (*dFov)[it].bgRatio;
              gInfo->normImg=  gObject->slideDir +
                                "/" + (*dFov)[it].aoiName + \
                                gObject->file_extension;
            }
          }
        }
      }
      else
      if (gObject->mCDF_Fg.data())
      {
        // Check if it is a BG/FG
        bg = \
        gObject->hUtils.get()->BackgroundDetection( &(*dFov)[it].fft,\
                                                    &gObject->mCDF_Fg,\
                                                    &gObject->mCDF_Bg,\
                                                    &gObject->mMaskFgBg,\
                                                    gObject->fgbgThresh,\
                                                    &(*dFov)[it].bgRatio,\
                                                    gObject->log_preprocessing);

        // Update the calibration image only if the AOI was deemed a background image
        if (bg == 1)
        {
          // at this time we keep track of the calibration image
          if ((*dFov)[it].bgRatio > gObject->fgbgThresh)
          {
            // The number is an emperically determined value that will occur
            // when dealing with noisy images
            if ((*dFov)[it].bgRatio > bgRatio1)
            {
              // This is the new calibration image
              bgRatio1 = (*dFov)[it].bgRatio;
              gInfo->calibImg=  gObject->slideDir +
                                "/" + (*dFov)[it].aoiName + \
                                gObject->file_extension;
              if (bgRatio2 == -1)
              {
                bgRatio2 = (*dFov)[it].bgRatio;
                gInfo->normImg=  gObject->slideDir +
                                 "/" + (*dFov)[it].aoiName + \
                                 gObject->file_extension;
              }
            }

            if ((*dFov)[it].bgRatio > bgRatio2 && (*dFov)[it].bgRatio < bgRatio1)
            {
              bgRatio2 = (*dFov)[it].bgRatio;
              gInfo->normImg=  gObject->slideDir +
                                "/" + (*dFov)[it].aoiName + \
                                gObject->file_extension;
            }
          }
        }
      }

      // Mark this AOI is a background it bg is 1
      if (bg==1)
      {
        (*dFov)[it].isBackground=true;
      }

      (*dFov)[it].f_preprocess = true;
      return bg;
    }//end of function

    /*! \brief ProcessData
     * Chains up the process pipelines that need to be sequenced for the data
     * point
     */
    template <class P>
    void PreprocessingPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // Process the data point using the pipeline described in
      // ProcessPipeline.
      int v = ProcessPipeline(&dataPoint, obj);

      // Here, we downcast a default object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      // Clear FFT data accumulated here
      GridMapGeneration* gmap = gObject->gMap.get();
      GridInfo* gInfo         = &(gmap->GetGridHeader());
      std::map<pDouble, FOVStruct>* dFov = gmap->GetGridMapping();
      pDouble it(dataPoint.gYRow, dataPoint.gXCol);

      // Instantiate the next set of pipelines, if requested
      if (mPlugins.size() > 0)
      {
        P dataPoint_c;
        if (v==1)
        {
          dataPoint_c.isForeground = false;
        }
        else
        {
          // make a copy of the ITK Image data into an OpenCV data
          // This will lead to a memory spike (Memory leak?)
          dataPoint_c.isForeground = true;
          dataPoint_c.aoiName = (*dFov)[it].aoiName;
          dataPoint_c.gXCol = dataPoint.gXCol;
          dataPoint_c.gYRow = dataPoint.gYRow;
          dataPoint_c.Magnification = dataPoint.Magnification;
          // typecast the image to an RGB image
          dataPoint_c.Img_cv = \
          cv::Mat(gObject->mImageRegion.GetSize()[1], gObject->mImageRegion.GetSize()[0], CV_8UC3);

          // Copy the data to this buffer
          memcpy(dataPoint_c.Img_cv.data, \
                (*dFov)[it].Img->GetBufferPointer(),\
                3*dataPoint_c.Img_cv.rows*dataPoint_c.Img_cv.cols);
        }

        // Run all the consumers; 1/per plugin
        for (auto i: mPlugins)
        {
          i->ProcessPipeline(&dataPoint_c, obj);
        }

		    dataPoint_c.Img_cv.release();
      }

      // Check if the next pipeline has been enabled
      if (gObject->EstimateTranslation)
      {
        // Forward the data point to the next Pipeline or Queue
        mTranslationEstimation.get()->ProcessPipeline(&dataPoint, obj);
      }
      else
      {
        // clear FFT data
        spin::FovCompleted p;
        p.Module = 1;//indicates pre-processing
        p.gYRow  = dataPoint.gYRow;
        p.gXCol  = dataPoint.gXCol;
        ClearData(obj, p);
      }

      // For the sake of safety
      (*dFov)[it].Img = NULL;
      // Send a signal of completion for this FOV to the calling thread
      spin::FovCompleted p;
      p.Module = 1;//indicates pre-processing
      p.gYRow  = dataPoint.gYRow;
      p.gXCol  = dataPoint.gXCol;
      sig_FOV(p);
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     * This function ensures that processing is finished
     */
    template <class P>
    void PreprocessingPipeline<P>::CompleteProcessing(void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);

      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      // Create a vector of processes that can be run in parallel
      std::vector<std::shared_ptr<boost::thread> > tArray;

      if (gObject->EstimateTranslation)
      {
        std::shared_ptr<boost::thread> pT1(new boost::thread(&SpinTranslationEstimation<P>::CompleteProcessing,\
                                                             mTranslationEstimation.get()));
        tArray.push_back(pT1);
      }

      //Check if plugins also need to be finished processing
      if (mPlugins.size() > 0)
      {
        // Run all the consumers; 1/per plugin
        for (auto i: mPlugins)
        {
          std::shared_ptr<boost::thread> pT1(new boost::thread(&spin::SpinAbstractProcessingPipeline<P>::CompleteProcessing,\
                                                               i.get()));
          tArray.push_back(pT1);
        }
      }

      // Finish all processes
      for (int jThread = 0; jThread < tArray.size(); ++jThread)
      {
        tArray[jThread]->join();
      }

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();
      }

      return;
    }//end of function

    /*! \brief BeginProcessingQueue [SLOT]
     * This function Sends a request to the queue in the broker for data
     */
    template <class P>
    void PreprocessingPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      // Here, we downcast a default object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      GridMapGeneration* gmap = gObject->gMap.get();

      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetGridHeader().gridWidth * gmap->GetGridHeader().gridHeight;

      while(n_images < max_images)
      {
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_Preprocess.joinable())
        {
          t_Preprocess.join();
          ++n_images;
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
      }
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     * This function receives a signal from the MPC with a new data point
     */
    template <class P>
    void PreprocessingPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      // As the processing happens in a thread, we allocate memory for the
      // other structures that are needed. Typically, this would be memory
      // for thr FFT matrices
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      if (gObject == NULL) return;

      if (gObject->EstimateTranslation)
      {
        // And get the grid mapping
        GridMapGeneration* gmap = gObject->gMap.get();
        std::map<pDouble, FOVStruct>* dFov = gmap->GetGridMapping();
        // Build the index
        pDouble it(dataPoint.gYRow, dataPoint.gXCol);
        GridInfo* gInfo = &(gmap->GetGridHeader());

        // Now, in order to allocate memory for the FFT matrix, we check if
        // a.) Subsampling is required, and
        // b.) An optimal FFT needs to be computed
        int width  = gObject->mImageRegion.GetSize()[0] / gObject->subsampling;
        int height = gObject->mImageRegion.GetSize()[1] / gObject->subsampling;
        if (gObject->optimalFFT)
        {
          width = gObject->imgWidth_d / gObject->subsampling;
          height= gObject->imgHeight_d/ gObject->subsampling;
        }

        typedef spin::RMMatrix_Complex ComplexMatrix;
        (*dFov)[it].fft = ComplexMatrix::Zero(height,width);
      }

      // Process the data point in a separate thread
      t_Preprocess = boost::thread(&PreprocessingPipeline<P>::ProcessData,\
                                   this,dataPoint,obj);
    }//end of function

    /*! \brief RequestQueue [SLOT]
    * This function receives a signal from the MPC that a new datapoint is
    * available for consumption
    */
    template <class P>
    void PreprocessingPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&PreprocessingPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts the pipelines and cleanly exits
     */
    template <class P>
    void PreprocessingPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
      AbortOperations();
    }//end of function

    /*! \brief AbortOperations
     *	This function forwards the abort signal to other consumers and
     *	deallocates any memory objects that are part of this class alone.
     *	This however does not destroy the configuration for which this
     *  pipeline was primed with. Thereby enabling reacquisition instantly.
     */
    template <class P>
    void PreprocessingPipeline<P>::AbortOperations()
    {
      if (mTranslationEstimation.get() != NULL)
      {
        // Send a signal to abort the operations
        mTranslationEstimation.get()->AbortPipeline();
      }

      //Check if plugins also need to be aborted
      if (mPlugins.size() > 0)
      {
        // Run all the consumers; 1/per plugin
        for (auto i: mPlugins)
        {
          i->AbortPipeline();
        }
      }
    }//end of function

    // explicit instantiation of template class
    template class PreprocessingPipeline<ProducerDataType>;
  }//end of namespace vista
}//end of namespace spin
