#ifndef PREPROCESSINGPIPELINE_H_
#define PREPROCESSINGPIPELINE_H_

// pipeline includes
#include "AbstractClasses/AbstractConsumer.h"
#include "AbstractClasses/AbstractDataStructures.h"
#include "AbstractClasses/AbstractCallback.h"
// Boost includes
#include <boost/thread.hpp>

namespace spin
{
  template <class P>
  class SpinAbstractProcessingPipeline;

  namespace vista
  {
    // A set of forward declarations
    template <class P>
    class SpinTranslationEstimation;

    /*! \brief PreprocessingPipeline
     *  This class acts as the interface with the stitching pipeline and
     *  in essence is the interface for the library of the
     *  Spin Stitching Pipeline
     */
    template <class P>
    class PreprocessingPipeline : public AbstractConsumer<P>
    {
      public:
      	// Signals
      	spin::VoidSignal sig_GetData;

              // Default destructor
      	~PreprocessingPipeline();

      	/*! \brief InstantiatePipeline*/
      	int InstantiatePipeline(void* obj);

      	/*! \brief ProcessPipeline*/
      	int ProcessPipeline(P* dataPoint, void* obj);

      	/*! \brief ProcessData*/
      	void ProcessData(P dataPoint, void* obj);

      	/*! \brief CompleteProcessing*/
      	void CompleteProcessing(void* obj);

      	/*! \brief AbortPipeline*/
      	void AbortPipeline();

      	/*! \brief Reimplemented functions */
      	void ConsumeFromQueue(P dataPoint, void* obj);
      	void RequestQueue(void* obj);

        // Signals for metrics and logging
        spin::CallbackSignal sig_Logs;
        spin::CallbackSignal sig_Metrics;
        spin::ModuleSignal   sig_Finished;
        spin::FOVSignal      sig_FOV;
      private :
        // Thread to process the data
        boost::thread t_Process;
        boost::thread t_Preprocess;
        boost::mutex  abort_mutex;
        bool          abort;
        // Float value for bg ratio
        float bgRatio1 = -1;
        float bgRatio2 = 0;
        // An instance of the translation estimation module
        std::shared_ptr< SpinTranslationEstimation<P> > mTranslationEstimation;
        // An instance of plugins that also need to be run
        std::vector< std::shared_ptr< spin::SpinAbstractProcessingPipeline<P> > > mPlugins;

        /*! \brief Special functions*/
    	  void AbortOperations();
    	  void BeginProcessingQueue(void* obj);
        void RelayLogs(spin::CallbackStruct p);
    	  void RelayMetrics(spin::CallbackStruct p);
        void RelaySignal(spin::ModuleStatus p);
        void ClearData(void* obj, spin::FovCompleted p);

        spin::ModuleStatus mStatus;
    };//end of class
  }//end of namespace vista
}//end of namespace spin

#endif
