#ifndef PREPROCESSINGINTERFACE_H_
#define PREPROCESSINGINTERFACE_H_
// Pipeline includes
#include "AbstractClasses/AbstractProducer.h"
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  namespace vista
  {
    /*! \brief PreprocessingInterface
     *  This class is the interface between the image acquisition source and the
     *  preprocessing pipeline framework
     */
    template <class P>
    class PreprocessingInterface : public AbstractProducer
    {
      public:
        // Signals
        // Add data point to queue in broker
        boost::signals2::signal<void(P)> sig_AddQ;
        // Finished acquisition
        spin::VoidSignal sig_FinAcq;
        // Save image
        spin::CharSignal sig_SaveImageToDisk;
        // Abort Operations
        spin::VoidSignal sig_Abort;

        // Reimplemented function
        void AddToQueue(void* dataPoint);

    };//end of class
  }//end of namespace vista
}//end of namespace spin
#endif
