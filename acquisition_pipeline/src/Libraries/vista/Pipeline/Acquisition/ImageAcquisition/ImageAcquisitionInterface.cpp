#include "Libraries/vista/Pipeline/Acquisition/ImageAcquisition/ImageAcquisitionInterface.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  namespace vista
  {
    /*! \brief AddToQueue
     *  This function adds an object to the Pre-processing queue
     */
    template<class P>
    void ImageAcquisitionInterface<P>::AddToQueue(void* dataPoint)
    {
      // typecasts
      ProducerDataType* dp = static_cast<ProducerDataType*>(dataPoint);
      //emit the signal
      sig_AddQ(*dp);
      dp->Clear();
    }//end of function

    // explicit instantiation of template class
    template class ImageAcquisitionInterface<ProducerDataType>;
  }//end of stitching namespace
}//end of spin namespace
