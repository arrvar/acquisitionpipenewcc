// Pipeline includes
#include "Libraries/vista/Pipeline/Acquisition/ImageAcquisition/ImageAcquisitionPipeline.h"
#include "Libraries/vista/Pipeline/Acquisition/Preprocessing/spinVistaPreprocessing.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/spinVistaAcquisitionObject.h"

namespace spin
{
  namespace vista
  {
    /*! \brief Destructor */
    template<class P>
    ImageAcquisitionPipeline<P>::~ImageAcquisitionPipeline()
    {
      // Call the destructor in the sub routines as well
      AbortPipeline();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelaySignalFinished
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::RelaySignal(spin::ModuleStatus p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief ClearMemory
     *  A slot to indicate that the image is no longer required
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::ClearMemory(void* obj, spin::FovCompleted p)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      // And get the grid mapping
      GridMapGeneration* gmap = gObject->gMap.get();
      std::map<pDouble, FOVStruct>* dFov = gmap->GetGridMapping();
      // Build the index
      pDouble it(p.gYRow, p.gXCol);
      // Clear if a flag has been set
      if ( (!gObject->Preprocessing) || ( (*dFov)[it].f_preprocess == true) )
      {
        (*dFov)[it].Img = NULL;
        // Incerement processed images
        ++processedImages;
        ////std::cout<<"Clearing image: "<<(*dFov)[it].aoiName<<" "<<processedImages<<std::endl;
      }
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline.
     */
    template<class P>
    int ImageAcquisitionPipeline<P>::InstantiatePipeline(void* obj)
    {
      // Here, we downcast a default object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      if (gObject == NULL) return -1;

      // Instantiate all pipelines that need to be run
      if (gObject->Preprocessing)
      {
        mPreprocessing = std::make_shared<SpinPreprocessing<P>>();
        mPreprocessing.get()->InstantiatePipeline(obj);
        mPreprocessing.get()->sig_Finished.connect(boost::bind(&ImageAcquisitionPipeline<P>::RelaySignal, this, _1));
        mPreprocessing.get()->sig_FOV.connect(boost::bind(&ImageAcquisitionPipeline<P>::ClearMemory, this, obj, _1));
      }

      // In addition, we also associate a signal for FOV removal; for sanity checks
      this->sig_FOV.connect(boost::bind(&ImageAcquisitionPipeline<P>::ClearMemory, this, obj, _1));

      abort = false;

      // set processed images to 0
      processedImages = 0;

      return 1;
    }//end of function

    /*! \brief ProcessPipeline
     * Processes the data that is input from the source. Internally it pushes
     * the data into an appropriate data structure and then to a queue for
     * any consumer to pick it up.
     * parameters :
     * @P: The pointer to a data point from the producer. It contains
     *     an image that will need to be processed
     */
    template<class P>
    int ImageAcquisitionPipeline<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      if (gObject == NULL) return -1;

      // And get the grid mapping
      GridMapGeneration* gmap = gObject->gMap.get();
      std::map<pDouble, FOVStruct>* dFov = gmap->GetGridMapping();
      // Build the index
      pDouble it(producerDataPoint->gYRow, producerDataPoint->gXCol);
      GridInfo* gInfo = &(gmap->GetGridHeader());

      // We are acquiring the image from the disk
      typedef spin::RGBImageType RGBImageType;
      // Get the image name
      std::string inpImage =  gObject->slideDir + "/" + (*dFov)[it].aoiName + \
                              gObject->file_extension;
      gObject->ImageIO.get()->ReadImage(inpImage, &(*dFov)[it].Img, true);

      return 1;
    }//end of function

    /*! \brief ProcessData
     * Chains up the process pipelines that need to be sequenced for the data
     * point
    */
    template <class P>
    void ImageAcquisitionPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // Process the data point using the pipeline described in ProcessPipeline.
      ProcessPipeline(&dataPoint, obj);

      // Here, we downcast a default object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      if (gObject->Preprocessing)
      {
        // Forward the data point to the next Pipeline or Queue
        mPreprocessing.get()->ProcessPipeline(&dataPoint, obj);
      }
      else
      {
        // Send a signal of completion for this FOV to the calling thread
        spin::FovCompleted p;
        p.Module = 0;//indicates image acquisition
        p.gYRow  = dataPoint.gYRow;
        p.gXCol  = dataPoint.gXCol;
        //sig_FOV(p);
        ClearMemory(obj, p);
      }
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     * This function ensures that processing is finished
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::CompleteProcessing(void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);

      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      // Create a vector of processes that can be run in parallel
      std::vector<std::shared_ptr<boost::thread> > tArray;

      if (gObject->Preprocessing)
      {
        std::shared_ptr<boost::thread> pT1(new boost::thread(&SpinPreprocessing<P>::CompleteProcessing,\
                                                             mPreprocessing.get()));
        tArray.push_back(pT1);
      }

      // Finish all processes
      for (int jThread = 0; jThread < tArray.size(); ++jThread)
      {
        tArray[jThread]->join();
      }

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();
        // Remain in an infinite loop
        GridMapGeneration* gmap = gObject->gMap.get();
        // Get the number of images that need to be cleared
        int max_images = gmap->GetGridHeader().gridWidth * gmap->GetGridHeader().gridHeight;
        // remain in a perpetual loop
        while(processedImages < max_images)
        {
          if (processedImages >= max_images) return;
        }
      }
      return;
    }//end of function

    /*! \brief BeginProcessingQueue [SLOT]
     * This function Sends a request to the queue in the broker for data
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      GridMapGeneration* gmap = gObject->gMap.get();

      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetGridHeader().gridWidth * gmap->GetGridHeader().gridHeight;

      while(n_images < max_images)
      {
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_ImageAcquisition.joinable())
        {
          t_ImageAcquisition.join();
          ++n_images;
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
      }
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     *  This function receives a signal from the MPC with a new data point
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      // As the processing happens in a thread, we allocate memory for the
      // image outside the thread.
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      if (gObject == NULL) return;

      // And get the grid mapping
      GridMapGeneration* gmap = gObject->gMap.get();
      std::map<pDouble, FOVStruct>* dFov = gmap->GetGridMapping();
      // Build the index
      pDouble it(dataPoint.gYRow, dataPoint.gXCol);
      GridInfo* gInfo = &(gmap->GetGridHeader());

      // We are acquiring the image from the disk
      typedef spin::RGBImageType RGBImageType;
      // Get the image name
      std::string inpImage =  gObject->slideDir + "/" + (*dFov)[it].aoiName + \
                              gObject->file_extension;
      (*dFov)[it].Img = RGBImageType::New();
      (*dFov)[it].Img->SetRegions(gObject->mImageRegion);
      (*dFov)[it].Img->Allocate();

      // Process the data point in a separate thread. At the end of this
      // operation the image should have been read into memory.
      t_ImageAcquisition  = boost::thread(&ImageAcquisitionPipeline<P>::ProcessData,\
                                          this,dataPoint,obj);
    }//end of function

    /*! \brief RequestQueue [SLOT]
     *  This function receives a signal from the MPC that a new datapoint is
     *  available for consumption
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&ImageAcquisitionPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts the pipelines and cleanly exits
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
      AbortOperations();
    }//end of function

    /*! \brief AbortOperations
     *	This function forwards the abort signal to other consumers and
     *	deallocates any memory objects that are part of this class alone.
     *	This however does not destroy the configuration for which this
     *  pipeline was primed with. Thereby enabling reacquisition instantly.
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::AbortOperations()
    {
      if (mPreprocessing.get() != NULL)
      {
        // Send a signal to abort the operations
        mPreprocessing.get()->AbortPipeline();
      }
    }//end of function

    // explicit instantiation of template class
    template class ImageAcquisitionPipeline<ProducerDataType>;
  }//end of namespace vista
}//end of namespace spin
