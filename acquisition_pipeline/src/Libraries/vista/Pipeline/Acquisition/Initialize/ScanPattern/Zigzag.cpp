#include "Libraries/vista/Pipeline/Acquisition/Initialize/GridMapGeneration.h"
namespace spin
{
  namespace vista
  {
    /*! \brief ZigzagPattern
     *  This function populates information in the grid map object
     *  when a snake scanning pattern is selected
     */
    int GridMapGeneration::ZigzagPattern( bool computeMetrics, bool generateLogs)
    {
      typedef std::pair<int,int> pInt;

      // 1.) The scan is taking place across the columns of the grid. We assume that the
      // scan is prgressing from left to right
      if (gInfo.dType == DirectionType::WIDTH)
      {
        for (int yRow = 0; yRow < gInfo.gridHeight; ++yRow)
        for (int xCol = 0; xCol < gInfo.gridWidth ; ++xCol)
        {
          // The current node is (yRow,xCol) 
          // The potential neighbors are:
          // (yRow-1,xCol) ->secondary and (yRow,xCol-1)->primary
          pDouble d  = std::make_pair(yRow,xCol);
          gIdx.push_back(d);
          pDouble d2 = std::make_pair(std::max(0,yRow-1), xCol);
          pDouble d1 = std::make_pair(yRow, std::max(0,xCol-1));
          if (d != d1)
          {
            bufferFFT[d].tNeighbors.push_back(d1);
            if (computeMetrics)
            {
              OverlapMetric metrics;
              metrics.displacement_fftcost = 0;
              bufferFFT[d].tMetrics[d1] = metrics;
            }
          }

          if (d != d2)
          {
            bufferFFT[d].tNeighbors.push_back(d2);
            if (computeMetrics)
            {
              OverlapMetric metrics;
              metrics.displacement_fftcost = 0;
              bufferFFT[d].tMetrics[d2] = metrics;
            }
          }    
        }//finished checking all grid coordinates
      }//finished checking direction of Width
      else
      // 1.) The scan is taking place across the rows of the grid. We assume that the
      // scan is prgressing from top-to-bottom
      if (gInfo.dType == DirectionType::HEIGHT)
      {
        for (int xCol = 0; xCol < gInfo.gridWidth ; ++xCol)
        for (int yRow = 0; yRow < gInfo.gridHeight; ++yRow)        
        {
          // The current node is (yRow,xCol) 
          // The potential neighbors are:
          // (yRow-1,xCol) ->primary and (yRow,xCol-1)->secondary
          pDouble d  = std::make_pair(yRow,xCol);
          gIdx.push_back(d);
          pDouble d1 = std::make_pair(std::max(0,yRow-1), xCol);
          pDouble d2 = std::make_pair(yRow, std::max(0,xCol-1));
          if (d != d1)
          {
            bufferFFT[d].tNeighbors.push_back(d1);
            if (computeMetrics)
            {
              OverlapMetric metrics;
              metrics.displacement_fftcost = 0;
              bufferFFT[d].tMetrics[d1] = metrics;
            }
          }

          if (d != d2)
          {
            bufferFFT[d].tNeighbors.push_back(d2);
            if (computeMetrics)
            {
              OverlapMetric metrics;
              metrics.displacement_fftcost = 0;
              bufferFFT[d].tMetrics[d2] = metrics;
            }
          }         

        }//finished checking all grid coordinates
      }//finished checking for direction of height

      return 1;
    }//end of function
  }//end of namespace vista
}//end of namespace spin
