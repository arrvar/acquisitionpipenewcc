#include "Libraries/vista/Pipeline/Acquisition/Initialize/GridMapGeneration.h"
namespace spin
{
  namespace vista
  {
    /*! \brief SnakePattern
     *  This function populates information in the grid map object
     *  when a snake scanning pattern is selected
     */
    int GridMapGeneration::SnakePattern(bool computeMetrics, bool generateLogs)
    {
      typedef std::pair<int,int> pInt;

      // 1.) The scan is taking place along the columns of the grid.
      if (gInfo.dType == DirectionType::WIDTH)
      {
        for (int i = 0; i < gInfo.gridHeight; ++i)
        for (int j = 0; j < gInfo.gridWidth ; ++j)
        {
          //gridYRemoval(i,j) = i-1;
          //gridXRemoval(i,j) = j;
          // At this time, we insert the grid coordinates into a vector.
          // These coordinates will be the order in which the grid will
          // be scanned
          if (i % 2 == 0)
          {
            // This is a forward direction scan
            pDouble d = std::make_pair(gridXCols(i, j), gridYRows(i, j));
            gIdx.push_back(d);
            // Generate indices of neighboring aoi's
            pInt d1     = std::make_pair(i,(std::max)(0,j-1));
            pInt d2     = std::make_pair((std::max)(0,i-1),j);
            pDouble da  =std::make_pair(gridXCols(d1.first, d1.second),gridYRows(d1.first, d1.second));
            pDouble db  = std::make_pair(gridXCols(d2.first, d2.second),gridYRows(d2.first, d2.second));

            if (da != d)
            {
              bufferFFT[d].tNeighbors.push_back(da);
              if (computeMetrics)
              {
                OverlapMetric metrics;
                metrics.displacement_fftcost = 0;
                bufferFFT[d].tMetrics[da] = metrics;
              }
            }

            if (db != d)
            {
              bufferFFT[d].tNeighbors.push_back(db);
              if (computeMetrics)
              {
                OverlapMetric metrics;
                metrics.displacement_fftcost = 0;
                bufferFFT[d].tMetrics[db] = metrics;
              }
            }
          }
          else
          {
            // This is an aoi when scanning in the reverse direction
            int p = gInfo.gridWidth - 1 - j;
            pDouble d = std::make_pair(gridXCols(i, p), gridYRows(i, p));
            gIdx.push_back(d);
            // Generate indices of neighboring aoi's
            pInt d2     = std::make_pair((std::max)(0,i-1),p);
            pInt d1     = std::make_pair(i,(std::min)(gInfo.gridWidth-1,p+1));
            pDouble da  = std::make_pair(gridXCols(d1.first, d1.second), gridYRows(d1.first, d1.second));
            pDouble db  = std::make_pair(gridXCols(d2.first, d2.second), gridYRows(d2.first, d2.second));

            if (da != d)
            {
              bufferFFT[d].tNeighbors.push_back(da);
              if (computeMetrics)
              {
                OverlapMetric metrics;
                metrics.displacement_fftcost = 0;
                bufferFFT[d].tMetrics[da] = metrics;
              }
            }

            if (db != d)
            {
              bufferFFT[d].tNeighbors.push_back(db);
              if (computeMetrics)
              {
                OverlapMetric metrics;
                metrics.displacement_fftcost = 0;
                bufferFFT[d].tMetrics[db] = metrics;
              }
            }
          }
        }
      }//finished checking direction of Width
      else
      // 5b.) The scan is taking place along the rows of the grid.
      if (gInfo.dType == DirectionType::HEIGHT)
      {
        for (int j = 0; j < gInfo.gridWidth ; ++j)
        for (int i = 0; i < gInfo.gridHeight; ++i)
        {
          //gridYRemoval(i,j) = i;
          //gridXRemoval(i,j) = j-1;
          // At this time, we insert the grid coordinates into a vector.
          // These coordinates will be the order in which the grid will
          // be scanned
          if (j % 2 == 0)
          {
            // This is a forward direction scan
            pDouble d = std::make_pair(gridXCols(i, j), gridYRows(i, j));
            gIdx.push_back(d);
            // Generate indices of neighboring aoi's
            pInt d1     = std::make_pair((std::max)(0, i - 1), j);
            pInt d2     = std::make_pair(i, (std::max)(0, j - 1));
            pDouble da  = std::make_pair(gridXCols(d1.first, d1.second), gridYRows(d1.first, d1.second));
            pDouble db  = std::make_pair(gridXCols(d2.first, d2.second), gridYRows(d2.first, d2.second));

            if (da != d)
            {
              bufferFFT[d].tNeighbors.push_back(da);
              if (computeMetrics)
              {
                OverlapMetric metrics;
                metrics.displacement_fftcost = 0;
                bufferFFT[d].tMetrics[da] = metrics;
              }
            }

            if (db != d)
            {
              bufferFFT[d].tNeighbors.push_back(db);
              if (computeMetrics)
              {
                OverlapMetric metrics;
                metrics.displacement_fftcost = 0;
                bufferFFT[d].tMetrics[db] = metrics;
              }
            }
          }
          else
          {
            // This is an aoi when scanning in the reverse direction
            int p = gInfo.gridHeight - 1 - i;
            pDouble d = std::make_pair(gridXCols(p, j), gridYRows(p, j));
            gIdx.push_back(d);
            // Generate indices of neighboring aoi's
            pInt d1     = std::make_pair((std::min)(gInfo.gridHeight-1, p + 1), j);
            pInt d2     = std::make_pair(p, (std::max)(0, j - 1));
            pDouble da  = std::make_pair(gridXCols(d1.first, d1.second), gridYRows(d1.first, d1.second));
            pDouble db  = std::make_pair(gridXCols(d2.first, d2.second), gridYRows(d2.first, d2.second));

            if (da != d)
            {
              bufferFFT[d].tNeighbors.push_back(da);
              if (computeMetrics)
              {
                OverlapMetric metrics;
                metrics.displacement_fftcost = 0;
                bufferFFT[d].tMetrics[da] = metrics;
              }
            }

            if (db != d)
            {
              bufferFFT[d].tNeighbors.push_back(db);
              if (computeMetrics)
              {
                OverlapMetric metrics;
                metrics.displacement_fftcost = 0;
                bufferFFT[d].tMetrics[db] = metrics;
              }
            }
          }
        }
      }//finished checking for direction of Height

      return 1;

    }//end of function

  }//end of namespace vista
}//end of namespace spin
