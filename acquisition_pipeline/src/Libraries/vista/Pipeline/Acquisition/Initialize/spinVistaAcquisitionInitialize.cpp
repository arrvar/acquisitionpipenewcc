#include "Libraries/vista/Pipeline/Acquisition/Initialize/spinVistaAcquisitionInitialize.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/spinVistaAcquisitionObject.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/GridMapGeneration.h"
#include "Utilities/ImageProcessing/ImageUtils/ImageUtils.h"
#include "Utilities/Misc/PrimeFactorization.h"
#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <algorithm>
#include <cmath>

namespace spin
{
  namespace vista
  {
    /*! \brief CreateFgBgMask
     *  This is a function to create a mask in order to select
     *  certain regions from a FFT transformed image. Typically,
     *  an image that has undergone FFT will have its low-frequency
     *  content concentrated at the corners of the image. In order to
     *  generate a mask, we apply a square mask at these corners.
     */
    void CreateFgBgMask(spin::RMMatrix_Float* mask, int radius)
    {
      typedef spin::RMMatrix_Float RMMatrix_Float;
      int width = mask->cols();
      int height= mask->rows();
      RMMatrix_Float t = RMMatrix_Float::Ones(radius,radius);
      mask->block(0,0,radius,radius) = t;
      mask->block(0,width-radius-1,radius,radius) = t;
      mask->block(height-radius-1,0,radius,radius) = t;
      mask->block(height-radius-1,width-radius-1,radius,radius) = t;
    }//end of function

    /*! \brief CreateFocusMask
     *  This is a function to create a mask in order to select
     *  certain regions from a FFT transformed image for determining
     *  focus quality
     */
    void CreateFocusMask(spin::RMMatrix_Float* mask, int radius)
    {
      typedef spin::RMMatrix_Float RMMatrix_Float;
      int width = mask->cols();
      int height= mask->rows();
      (*mask) = RMMatrix_Float::Ones(height,width);
      RMMatrix_Float t = RMMatrix_Float::Zero(radius,radius);
      mask->block(0,0,radius,radius) = t;
      mask->block(0,width-radius-1,radius,radius) = t;
      mask->block(height-radius-1,0,radius,radius) = t;
      mask->block(height-radius-1,width-radius-1,radius,radius) = t;
    }//end of function

    /*! \brief ComputeFFTStuff
     *  This is a helper class for building FFT's and histograms of template images
     */
    template <class pChannel>
    int ComputeFFTStuff(  spin::RGBImageType::Pointer* bg, \
                          spin::RGBImageType::Pointer* fg, \
                          spinVistaAcquisitionObject* gObject)
    {
      // Compute CDF of magnitude of Low-frequency region of bg image
      spin::RMMatrix_Complex tFFT= spin::RMMatrix_Complex::Zero(0,0);
      int f = spin::ComputeFFTOfRGBImage<pChannel,float>( bg, \
                                                  gObject->fft2d.get(),\
                                                  &tFFT,\
                                                  gObject->subsampling,\
                                                  gObject->optimalFFT,\
                                                  gObject->imgWidth_d,\
                                                  gObject->imgHeight_d,false);

      gObject->hUtils.get()->ComputeCDFOfFFTMagnitude(&tFFT, \
                                                      &gObject->mCDF_Bg, \
                                                      &gObject->mMaskFgBg, \
                                                      true);
      // Compute CDF of magnitude of Low-frequency region of fg image
      spin::ComputeFFTOfRGBImage<pChannel,float>( fg, \
                                                  gObject->fft2d.get(),\
                                                  &tFFT,\
                                                  gObject->subsampling,\
                                                  gObject->optimalFFT,\
                                                  gObject->imgWidth_d,\
                                                  gObject->imgHeight_d,false);

      gObject->hUtils.get()->ComputeCDFOfFFTMagnitude(&tFFT, \
                                                      &gObject->mCDF_Fg, \
                                                      &gObject->mMaskFgBg, \
                                                      true);
      return 1;
    }//end of function

    /*! \brief ComputeFFTStuff_g
     *  This is a helper class for building FFT's and histograms of template
     *  grayscale images
     */
    int ComputeFFTStuff_g(spin::UCharImageType2D::Pointer* bg, \
                          spin::UCharImageType2D::Pointer* fg, \
                          spinVistaAcquisitionObject* gObject)
    {
      // Compute CDF of magnitude of Low-frequency region of bg image
      spin::RMMatrix_Complex tFFT= spin::RMMatrix_Complex::Zero(0,0);
      spin::ComputeFFTOfGrayscaleImage<float>( bg, \
                                               gObject->fft2d.get(),\
                                               &tFFT,\
                                               gObject->subsampling,\
                                               gObject->optimalFFT,\
                                               gObject->imgWidth_d,\
                                               gObject->imgHeight_d,false);

      gObject->hUtils.get()->ComputeCDFOfFFTMagnitude(&tFFT, \
                                                      &gObject->mCDF_Bg, \
                                                      &gObject->mMaskFgBg, \
                                                      true);
      // Compute CDF of magnitude of Low-frequency region of fg image
      spin::ComputeFFTOfGrayscaleImage<float>(fg, \
                                              gObject->fft2d.get(),\
                                              &tFFT,\
                                              gObject->subsampling,\
                                              gObject->optimalFFT,\
                                              gObject->imgWidth_d,\
                                              gObject->imgHeight_d,false);

      gObject->hUtils.get()->ComputeCDFOfFFTMagnitude(&tFFT, \
                                                      &gObject->mCDF_Fg, \
                                                      &gObject->mMaskFgBg, \
                                                      true);
      return 1;
    }//end of function

    /*! \brief ComputeFFTStuff
     *  This is a helper class for building FFT's and histograms of template images
     */
    template <class pChannel>
    int ComputeFFTStuff(  spin::RGBImageType::Pointer* bg, \
                          spin::RGBImageType::Pointer* fg, \
                          spin::RGBImageType::Pointer* focus, \
                          spinVistaAcquisitionObject* gObject)
    {
      // Compute FFT stuff of FG/BG images
      ComputeFFTStuff<pChannel>(bg, fg, gObject);

      // Compute CDF of magnitude of Low-frequency region of bg image
      spin::RMMatrix_Complex tFFT= spin::RMMatrix_Complex::Zero(0,0);

      // Compute PDF of magnitude of the well-focused image
      spin::ComputeFFTOfRGBImage<pChannel,float>( focus, \
                                                  gObject->fft2d.get(),\
                                                  &tFFT,\
                                                  gObject->subsampling,\
                                                  gObject->optimalFFT,\
                                                  gObject->imgWidth_d,\
                                                  gObject->imgHeight_d,false);

      gObject->hUtils.get()->ComputeCDFOfFFTMagnitude(&tFFT, \
                                                      &gObject->mPDF_Focus, \
                                                      &gObject->mMaskFocus, \
                                                      false);
      return 1;
    }//end of function

    /*! \brief PreprocessInitialization()
     *  This function initializes all elements associated with the
     *  pre-processing module of the pipeline
     */
    int PreprocessInitialization(spinVistaAcquisitionObject* gObject)
    {
      typedef spin::RGBImageType RGBImageType;

      boost::filesystem::path dir0(gObject->bgImage.c_str());
      if (!boost::filesystem::exists(dir0))
      {
        return -2;
      }
      /////////////////////////////////////////////////////////////////////////
      // Instantiate the ImageIO object with the calibration image
      // We assume that all images will conform with specifications
      // described by the calibration image
      gObject->ImageIO = std::make_shared<spin::RGBImageIO>(gObject->bgImage);

      /////////////////////////////////////////////////////////////////////////
      // Read bg image
      gObject->mCalImage = spin::RGBImageType::New();
      int v = gObject->ImageIO.get()->ReadImage(gObject->bgImage, &gObject->mCalImage);
      if (v != 1) return -3;

      // The following two images are optional in nature
      spin::RGBImageType::Pointer fg = spin::RGBImageType::Pointer();//NULL;
      boost::filesystem::path dir1(gObject->fgImage.c_str());
      if (boost::filesystem::exists(dir1))
      {
        // Read fg image
        fg = spin::RGBImageType::New();
        v = gObject->ImageIO.get()->ReadImage(gObject->fgImage, &fg);
        if (v != 1) return -4;
      }

      spin::RGBImageType::Pointer focus = spin::RGBImageType::Pointer();
      boost::filesystem::path dir2(gObject->focusImage.c_str());
      if (boost::filesystem::exists(dir2))
      {
        // Read well focused image
        focus = spin::RGBImageType::New();
        v = gObject->ImageIO.get()->ReadImage(gObject->focusImage, &focus);
        if (v != 1) return -5;
      }

      /////////////////////////////////////////////////////////////////////////
      // Set the image region
      gObject->mImageRegion = gObject->mCalImage->GetLargestPossibleRegion();

      /////////////////////////////////////////////////////////////////////////
      // Set the image dimensions
      gObject->imgHeight = gObject->mImageRegion.GetSize()[1];
      gObject->imgWidth  = gObject->mImageRegion.GetSize()[0];
      // In addition we also cacluate the optimal height and width
      // for computing the FFT
      spin::PrimeFactorization pf;
      std::vector<int> validInt{2,3,5};
      int h1 = gObject->imgHeight / gObject->subsampling;
      gObject->imgHeight_d = pf.NearestInteger(h1, &validInt);
      gObject->imgHeight_d *= gObject->subsampling;
      int w1 = gObject->imgWidth / gObject->subsampling;
      gObject->imgWidth_d  = pf.NearestInteger(w1, &validInt);
      gObject->imgWidth_d *= gObject->subsampling;
      validInt.clear();

      /////////////////////////////////////////////////////////////////////////
      if (gObject->Preprocessing)
      {
        // Instantiate various objects and masks
        if (gObject->optimalFFT)
        {
          if (gObject->subsampling > 1)
          {
            gObject->fft2d = \
            std::make_shared<spin::FFT2D<float> >(gObject->imgHeight_d/gObject->subsampling,\
                                                  gObject->imgWidth_d/gObject->subsampling);
            gObject->fftUtils1 = std::make_shared< spin::FFTTranslation<float> >(gObject->subsampling);
            gObject->fftUtils2 = std::make_shared< spin::FFTTranslation<float> >(gObject->subsampling);

            // Create a mask
            if (!(fg.IsNull()))
            {
              gObject->mMaskFgBg = spin::RMMatrix_Float::Zero(gObject->imgHeight_d/gObject->subsampling,\
                                                              gObject->imgWidth_d/gObject->subsampling);
            }
            if (!(focus.IsNull()))
            {
              gObject->mMaskFocus= gObject->mMaskFgBg;
            }
          }
          else
          {
            gObject->fft2d = \
            std::make_shared<spin::FFT2D<float> >(gObject->imgHeight_d,gObject->imgWidth_d);
            gObject->fftUtils1 = std::make_shared< spin::FFTTranslation<float> >(1);
            gObject->fftUtils2 = std::make_shared< spin::FFTTranslation<float> >(1);
            if (!(fg.IsNull()))
            {
              gObject->mMaskFgBg = spin::RMMatrix_Float::Zero(gObject->imgHeight_d,gObject->imgWidth_d);
            }
            if (!(focus.IsNull()))
            {
              gObject->mMaskFocus= gObject->mMaskFgBg;
            }
          }
        }
        else
        {
          if (gObject->subsampling > 1)
          {
            gObject->fft2d = \
            std::make_shared<spin::FFT2D<float> >(gObject->imgHeight/gObject->subsampling,\
                                                  gObject->imgWidth/gObject->subsampling);
            gObject->fftUtils1 = std::make_shared< spin::FFTTranslation<float> >(gObject->subsampling);
            gObject->fftUtils2 = std::make_shared< spin::FFTTranslation<float> >(gObject->subsampling);
            if (!(fg.IsNull()))
            {
              gObject->mMaskFgBg = spin::RMMatrix_Float::Zero(gObject->imgHeight/gObject->subsampling,\
                                                              gObject->imgWidth/gObject->subsampling);
            }
            if (!(focus.IsNull()))
            {
              gObject->mMaskFocus= gObject->mMaskFgBg;
            }
          }
          else
          {
            gObject->fft2d = \
            std::make_shared<spin::FFT2D<float> >(gObject->imgHeight,gObject->imgWidth);
            gObject->fftUtils1 = std::make_shared< spin::FFTTranslation<float> >(1);
            gObject->fftUtils2 = std::make_shared< spin::FFTTranslation<float> >(1);
            if (!(fg.IsNull()))
            {
              gObject->mMaskFgBg = spin::RMMatrix_Float::Zero(gObject->imgHeight,gObject->imgWidth);
            }
            if (!(focus.IsNull()))
            {
              gObject->mMaskFocus= gObject->mMaskFgBg;
            }
          }
        }

        if (!(fg.IsNull()))
        {
          // Instantiate a histogram object
          gObject->hUtils = std::make_shared<spin::HistogramUtils>(gObject->HistogramBins);
          // Create a low-frequency mask for FG/BG detection
          CreateFgBgMask(&gObject->mMaskFgBg, gObject->maskradius);
          if (!(focus.IsNull()))
          {
            // Create a high-frequency mask for Focus
            CreateFocusMask(&gObject->mMaskFocus, gObject->focusradius);
          }

          /////////////////////////////////////////////////////////////////////////
          // Build FFT objects for all channels
          switch(gObject->channel)
          {
            case 0:
            {
              typedef spin::BlueChannelPixelAccessor<unsigned char> pChannel;
              // Compute all FFT related stuff
              if (!(focus.IsNull()))
              v = ComputeFFTStuff<pChannel>(&gObject->mCalImage, &fg, &focus, gObject);
              else
              v = ComputeFFTStuff<pChannel>(&gObject->mCalImage, &fg, gObject);
              if (v != 1) return -6;
            }
            break;
            case 1:
            {
              typedef spin::GreenChannelPixelAccessor<unsigned char> pChannel;
              // Compute all FFT related stuff
              if (!(focus.IsNull()))
              v = ComputeFFTStuff<pChannel>(&gObject->mCalImage, &fg, &focus, gObject);
              else
              v = ComputeFFTStuff<pChannel>(&gObject->mCalImage, &fg, gObject);
              if (v != 1) return -6;
            }
            break;
            case 2:
            {
              typedef spin::RedChannelPixelAccessor<unsigned char> pChannel;
              // Compute all FFT related stuff
              if (!(focus.IsNull()))
              v = ComputeFFTStuff<pChannel>(&gObject->mCalImage, &fg, &focus, gObject);
              else
              v = ComputeFFTStuff<pChannel>(&gObject->mCalImage, &fg, gObject);
              if (v != 1) return -6;
            }
            break;
            default:
            {
              typedef spin::GreenChannelPixelAccessor<unsigned char> pChannel;
              // Compute all FFT related stuff
              if (!(focus.IsNull()))
              v = ComputeFFTStuff<pChannel>(&gObject->mCalImage, &fg, &focus, gObject);
              else
              v = ComputeFFTStuff<pChannel>(&gObject->mCalImage, &fg, gObject);
              if ( v != 1) return -6;
            }
            break;
          }
        }
      }
      return 1;
    }//end of function

    /*! \brief PreprocessInitialization_g()
     *  This function initializes all elements associated with the
     *  pre-processing module of the pipeline when dealing with grayscale images
     */
    int PreprocessInitialization_g(spinVistaAcquisitionObject* gObject)
    {
      typedef spin::UCharImageType2D ImageType;

      boost::filesystem::path dir0(gObject->bgImage.c_str());
      if (!boost::filesystem::exists(dir0))
      {
        return -2;
      }
      /////////////////////////////////////////////////////////////////////////
      // Instantiate the ImageIO object with the calibration image
      // We assume that all images will conform with specifications
      // described by the calibration image
      gObject->ImageIO = std::make_shared<spin::RGBImageIO>(gObject->bgImage);

      /////////////////////////////////////////////////////////////////////////
      // Read bg image
      ImageType::Pointer mCalImage = ImageType::New();
      int v = gObject->ImageIO.get()->ReadImage(gObject->bgImage, &mCalImage);
      if (v != 1) return -3;

      // The following two images are optional in nature
      ImageType::Pointer fg = ImageType::Pointer();//NULL;
      boost::filesystem::path dir1(gObject->fgImage.c_str());
      if (boost::filesystem::exists(dir1))
      {
        // Read fg image
        fg = ImageType::New();
        v = gObject->ImageIO.get()->ReadImage(gObject->fgImage, &fg);
        if (v != 1) return -4;
      }

      /////////////////////////////////////////////////////////////////////////
      // Set the image region
      gObject->mImageRegion = mCalImage->GetLargestPossibleRegion();

      /////////////////////////////////////////////////////////////////////////
      // Set the image dimensions
      gObject->imgHeight = gObject->mImageRegion.GetSize()[1];
      gObject->imgWidth  = gObject->mImageRegion.GetSize()[0];
      // In addition we also cacluate the optimal height and width
      // for computing the FFT
      spin::PrimeFactorization pf;
      std::vector<int> validInt{2,3,5};
      int h1 = gObject->imgHeight / gObject->subsampling;
      gObject->imgHeight_d = pf.NearestInteger(h1, &validInt);
      gObject->imgHeight_d *= gObject->subsampling;
      int w1 = gObject->imgWidth / gObject->subsampling;
      gObject->imgWidth_d  = pf.NearestInteger(w1, &validInt);
      gObject->imgWidth_d *= gObject->subsampling;
      validInt.clear();

      /////////////////////////////////////////////////////////////////////////
      if (gObject->Preprocessing)
      {
        // Instantiate various objects and masks
        if (gObject->optimalFFT)
        {
          if (gObject->subsampling > 1)
          {
            gObject->fft2d = \
            std::make_shared<spin::FFT2D<float> >(gObject->imgHeight_d/gObject->subsampling,\
                                                  gObject->imgWidth_d/gObject->subsampling);
            gObject->fftUtils1 = std::make_shared< spin::FFTTranslation<float> >(gObject->subsampling);
            gObject->fftUtils2 = std::make_shared< spin::FFTTranslation<float> >(gObject->subsampling);

            // Create a mask
            if (!(fg.IsNull()))
            {
              gObject->mMaskFgBg = spin::RMMatrix_Float::Zero(gObject->imgHeight_d/gObject->subsampling,\
                                                              gObject->imgWidth_d/gObject->subsampling);
            }
          }
          else
          {
            gObject->fft2d = \
            std::make_shared<spin::FFT2D<float> >(gObject->imgHeight_d,gObject->imgWidth_d);
            gObject->fftUtils1 = std::make_shared< spin::FFTTranslation<float> >(1);
            gObject->fftUtils2 = std::make_shared< spin::FFTTranslation<float> >(1);
            if (!(fg.IsNull()))
            {
              gObject->mMaskFgBg = spin::RMMatrix_Float::Zero(gObject->imgHeight_d,gObject->imgWidth_d);
            }
          }
        }
        else
        {
          if (gObject->subsampling > 1)
          {
            gObject->fft2d = \
            std::make_shared<spin::FFT2D<float> >(gObject->imgHeight/gObject->subsampling,\
                                                  gObject->imgWidth/gObject->subsampling);
            gObject->fftUtils1 = std::make_shared< spin::FFTTranslation<float> >(gObject->subsampling);
            gObject->fftUtils2 = std::make_shared< spin::FFTTranslation<float> >(gObject->subsampling);
            if (!(fg.IsNull()))
            {
              gObject->mMaskFgBg = spin::RMMatrix_Float::Zero(gObject->imgHeight/gObject->subsampling,\
                                                              gObject->imgWidth/gObject->subsampling);
            }
          }
          else
          {
            gObject->fft2d = \
            std::make_shared<spin::FFT2D<float> >(gObject->imgHeight,gObject->imgWidth);
            gObject->fftUtils1 = std::make_shared< spin::FFTTranslation<float> >(1);
            gObject->fftUtils2 = std::make_shared< spin::FFTTranslation<float> >(1);
            if (!(fg.IsNull()))
            {
              gObject->mMaskFgBg = spin::RMMatrix_Float::Zero(gObject->imgHeight,gObject->imgWidth);
            }
          }
        }

        if (!(fg.IsNull()))
        {
          // Instantiate a histogram object
          gObject->hUtils = std::make_shared<spin::HistogramUtils>(gObject->HistogramBins);
          // Create a low-frequency mask for FG/BG detection
          CreateFgBgMask(&gObject->mMaskFgBg, gObject->maskradius);

          /////////////////////////////////////////////////////////////////////////
          // Build FFT objects for the image
          v = ComputeFFTStuff_g(&mCalImage, &fg, gObject);
          if (v != 1) return -6;
        }
      }
      return 1;
    }//end of function

    /*! \brief Initialize()
     *  This function is used to parse grid information
     *  that will eventually result in the scan pattern. Before calling
     *  this function, it is assumed that this object is initialized.
     */
    int spinVistaAcquisitionInitialize::Initialize(void* obj, const char* ParameterFileName)
    {
      // Check if the parameter file parser is correct
      std::ifstream input(ParameterFileName);
      // populate tree structure pt
      using boost::property_tree::ptree;
      ptree pt;
      try
      {
        // parse the xml file
        read_xml(input, pt);
      }
      catch(const boost::property_tree::ptree_error &e)
      {
        return -1;
      }

      // Here, we downcast a default object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaAcquisitionObject* gObject = dynamic_cast<spinVistaAcquisitionObject*>(aObj);
      // Check if the downcast has worked
      if (gObject == NULL) return -2;

      // Default database
      gObject->dbFile = "";
      gObject->gridId = -1;

      // Initialize the Database IO
      gObject->db = std::make_shared<spin::vista::DatabaseIO>();
      // Initialize the calibration file
      gObject->calibrationFilename = "";
      // By default grayscale processing is disabled
      gObject->isGrayscale = false;
      // By default, unbiasedcost is not used (primarily used for hematology)
      gObject->unbiasedCost = false;

      // Ignore BG check when computing displacement
      gObject->ignoreBG = false;
      // Ignore Backlash check when computing displacement
      gObject->ignoreBacklash = false;
      // By default, we do not have dyadic length fft
      gObject->optimalFFT = false;
      // Do we prefer estimation displacement
      gObject->EstimateTranslation = false;
      // Default graph algorithm is MST
      gObject->graphType = 0;
      // If we do save the file, then default extension is bmp
      gObject->file_extension = ".bmp";
      // Apply a subsampling of 4
      gObject->subsampling = 4;
      // By default select the blue channel
      gObject->channel = 0;
      // Select a mask radius to check for fg/bg
      gObject->maskradius = 16;
      // Select a mask radius for focus
      gObject->focusradius = 32;
      // Select a threshold for fg/bg
      gObject->fgbgThresh = 8.0;
      // Select a threshold for focus
      gObject->focusThresh= 0.85;
      // #of histogram bins
      gObject->HistogramBins = 256;
      // Minimum size of CC to form a grid
      gObject->minCC = 25;
      // Default magnification
      gObject->magnification = 1; // 20x

      // Default flags for metrics and logging
      gObject->log_preprocessing          = false;
      gObject->log_translationestimation  = false;
      std::string logFile                 ="";

      gObject->calibration                = false;
      gObject->metrics_preprocessing          = false;
      gObject->metrics_translationestimation  = false;

      // Default thresholds
      double tolerance_disp1Y = 20.0;
      double tolerance_disp1X = 20.0;
      double tolerance_disp2X = 20.0;
      double tolerance_disp2Y = 20.0;
      gObject->disp1_Y.resize(2,-1);
      gObject->disp1_X.resize(2,-1);
      gObject->disp2_Y.resize(2,-1);
      gObject->disp2_X.resize(2,-1);
      gObject->default_horizontal.resize(2,-1);
      gObject->default_vertical.resize(2,-1);

      double estDisp1_X=0;
      double estDisp1_Y=0;
      double estDisp2_X=0;
      double estDisp2_Y=0;

      // Now, parse through all parameter settings and populate gObject
      BOOST_FOREACH(ptree::value_type& v, pt.get_child("vistaAcquisition"))
      {
        if (v.first == "Image_Database")
        {
          gObject->dbFile = v.second.get<std::string>("");
        }
        else
        if (v.first == "System_Calibration")
        {
          gObject->calibrationFilename = v.second.get<std::string>("");
        }
        else
        if (v.first == "Grayscale")
        {
          gObject->isGrayscale = true;
        }
        else
        if(v.first == "Image_Folder")
        {
          gObject->slideDir = v.second.get<std::string>("");
        }
        if(v.first == "Output_Folder")
        {
          gObject->outDir = v.second.get<std::string>("");
        }
        else
        if (v.first == "GridId")
        {
          gObject->gridId = v.second.get<int>("");
        }
        else
        if (v.first == "Logging_File")
        {
          logFile = v.second.get<std::string>("");
        }
        else
        if (v.first == "Common")
        {
          // Check for tags within PreProcessing
          BOOST_FOREACH(ptree::value_type& v1, v.second)
          {
            if (v1.first == "bg_template_img")
            {
              gObject->bgImage = v1.second.get<std::string>("");
            }
            else
            if (v1.first == "fg_template_img")
            {
              gObject->fgImage = v1.second.get<std::string>("");
            }
            else
            if (v1.first == "focus_template_img")
            {
              gObject->focusImage = v1.second.get<std::string>("");
            }
            else
            if (v1.first == "Input_Img_Extension")
            {
              gObject->file_extension = v1.second.get<std::string>("");
            }
            else
            if (v1.first == "Calibration")
            {
              gObject->calibration = true;
              gObject->calibration_dir = v1.second.get<std::string>("");
            }
            else
            if (v1.first == "Magnification")
            {
              gObject->magnification = v1.second.get<int>("");
            }
            else 
            if (v1.first == "IgnoreBGCheck")
            {
              gObject->ignoreBG = true;
            }
            else 
            if (v1.first == "IgnoreBacklashCheck")
            {
              gObject->ignoreBacklash = true;
            }
          }
        }
        else
        if (v.first == "Plugin")
        {
          spin::ModuleObject module;
          int h = 0;
          BOOST_FOREACH(ptree::value_type& v1, v.second)
          {
            if (v1.first=="Hierarchy")
              h = v1.second.get<int>("");
            else
            if (v1.first=="Name")
              module.pModuleName = v1.second.get<std::string>("");
            else
            if (v1.first=="Parameters")
              module.pParamFile = v1.second.get<std::string>("");
            else
            if (v1.first=="Path")
              module.pPath=v1.second.get<std::string>("");
          }
          bool f  = ( (module.pModuleName=="") || \
                      (module.pParamFile=="") || \
                      (module.pPath=="") );
          // If all parameters have been satisfied
          if (!f)
          {
            gObject->mPlugins[h].push_back(module);
          }
        }
        else
        if (v.first == "PreProcessing")
        {
          gObject->Preprocessing = true;
          // Check for tags within PreProcessing
          BOOST_FOREACH(ptree::value_type& v1, v.second)
          {
            if (v1.first == "subsample")
            {
              gObject->subsampling = v1.second.get<int>("");
            }
            else
            if (v1.first == "OptimalFFT")
            {
              gObject->optimalFFT = true;
            }
            else
            if (v1.first == "channel")
            {
              gObject->channel = v1.second.get<int>("");
            }
            else
            if (v1.first == "fg_bg_mask_radius")
            {
              gObject->maskradius = v1.second.get<int>("");
            }
            else
            if (v1.first == "fg_bg_threshold_ratio")
            {
              gObject->fgbgThresh = v1.second.get<float>("");
            }
            else
            if (v1.first == "focus_mask_radius")
            {
              gObject->focusradius = v1.second.get<int>("");
            }
            else
            if (v1.first == "focus_threshold_ratio")
            {
              gObject->focusThresh = v1.second.get<float>("");
            }
            else
            if (v1.first == "histogram_bins")
            {
              gObject->HistogramBins = v1.second.get<int>("");
            }
            else
            if (v1.first == "Logging")
            {
              gObject->log_preprocessing = true;
            }
            else
            if (v1.first == "Metrics")
            {
              gObject->metrics_preprocessing = true;
            }
          }
        }
        else
        if (v.first == "DisplacementEstimation")
        {
          gObject->EstimateTranslation = true;
          // Check for tags within PreProcessing
          BOOST_FOREACH(ptree::value_type& v1, v.second)
          {
            if (v1.first == "Logging")
            {
              gObject->log_translationestimation = true;
            }
            else
            if (v1.first == "Metrics")
            {
              gObject->metrics_translationestimation = true;
            }
            else
            if (v1.first == "EstimatedDisp1_Y")
            {
              estDisp1_Y = v1.second.get<double>("");
            }
            else
            if (v1.first == "ToleranceDisp1_Y")
            {
              tolerance_disp1Y = v1.second.get<double>("");
            }
            else
            if (v1.first == "EstimatedDisp1_X")
            {
              estDisp1_X = v1.second.get<double>("");
            }
            else
            if (v1.first == "ToleranceDisp1_X")
            {
              tolerance_disp1X = v1.second.get<double>("");
            }
            else
            if (v1.first == "EstimatedDisp2_Y")
            {
              estDisp2_Y = v1.second.get<double>("");
            }
            else if (v1.first == "ToleranceDisp2_Y")
            {
              tolerance_disp2Y = v1.second.get<double>("");
            }
            else
            if (v1.first == "EstimatedDisp2_X")
            {
              estDisp2_X = v1.second.get<double>("");
            }
            else if (v1.first == "ToleranceDisp2_X")
            {
              tolerance_disp2X = v1.second.get<double>("");
            }
            else
            if (v1.first == "Default_Horizontal_X")
            {
              gObject->default_horizontal[0] = v1.second.get<double>("");
            }
            else if (v1.first == "Default_Horizontal_Y")
            {
              gObject->default_horizontal[1] = v1.second.get<double>("");
            }
            else
            if (v1.first == "Default_Vertical_X")
            {
              gObject->default_vertical[0] = v1.second.get<double>("");
            }
            else if (v1.first == "Default_Vertical_Y")
            {
              gObject->default_vertical[1] = v1.second.get<double>("");
            }
            else
            if (v1.first == "MinCC")
            {
              gObject->minCC = v1.second.get<int>("");
            }
            else
            if (v1.first == "Model_File")
            {
              gObject->modelFile = v1.second.get<std::string>("");
            }
            else
            if (v1.first == "GraphType")
            {
              gObject->graphType = v1.second.get<int>("");
            }
            else
            if (v1.first == "UnbiasedCost")
            {
              gObject->unbiasedCost = true;
            }
          }
        }
      }// finish parsing all tags within "vista"

      // 2.) Instantiate a logger object
      bool generateLogs = gObject->log_translationestimation ||\
                          gObject->log_preprocessing;
      if (generateLogs)
      {
        gObject->loggingInitialized = true;
        // Instantiate a logging object
        gObject->logger = std::make_shared<spin::LoggerInterface>();
        if (logFile != "")
        {
          gObject->logger.get()->SetLoggingFile(logFile);
        }
        // and initialize it
        gObject->logger.get()->Initialize();
      }

      // 3.) Populate the variability in displacements
      gObject->disp1_X[0] = estDisp1_X*(1.0 - tolerance_disp1X/100.0);
      gObject->disp1_X[1] = estDisp1_X*(1.0 + tolerance_disp1X/100.0);
      gObject->disp1_Y[0] = estDisp1_Y*(1.0 - tolerance_disp1Y/100.0);
      gObject->disp1_Y[1] = estDisp1_Y*(1.0 + tolerance_disp1Y/100.0);

      gObject->disp2_X[0] = estDisp2_X*(1.0 - tolerance_disp2X/100.0);
      gObject->disp2_X[1] = estDisp2_X*(1.0 + tolerance_disp2X/100.0);
      gObject->disp2_Y[0] = estDisp2_Y*(1.0 - tolerance_disp2Y/100.0);
      gObject->disp2_Y[1] = estDisp2_Y*(1.0 + tolerance_disp2Y/100.0);

      // 4.)  At this time, we run the initialization pipeline
      int g ;
      if (gObject->isGrayscale)
        g = PreprocessInitialization_g(gObject);
      else
        g = PreprocessInitialization(gObject);
      if (g !=1) return -4;

      // 5.)  Parse the database
      bool computeMetrics = gObject->metrics_translationestimation ||\
                            gObject->metrics_preprocessing;
      gObject->gMap = std::make_shared<GridMapGeneration>();
      int ret = gObject->gMap.get()->Initialize(const_cast<char*>(gObject->dbFile.c_str()), \
                                                                  gObject->gridId,\
                                                                  gObject->magnification,\
                                                                  gObject->db.get(), computeMetrics, generateLogs);
      if (ret != 1) return -5;

      // 6.) Set the subsampling factor
      gObject->gMap.get()->GetGridHeader().subSampFactor = gObject->subsampling;
      // 7.) And the AOI width and height
      gObject->gMap.get()->GetGridHeader().aoiWidth  = gObject->imgWidth;
      gObject->gMap.get()->GetGridHeader().aoiHeight = gObject->imgHeight;
      gObject->gMap.get()->GetGridHeader().minCCSize = gObject->minCC;
      gObject->gMap.get()->GetGridHeader().fileExt   = gObject->file_extension;
      // 8.) Set the default calib image to the bg image. This is necessary if
      //     we do not have any true BG images in the data set.
      gObject->gMap.get()->GetGridHeader().calibImg  = gObject->bgImage;
      // 9.) Set the maximum number of images that can be acquired in this scan
      gObject->max_images = gObject->gMap.get()->GetGridHeader().gridWidth * \
                            gObject->gMap.get()->GetGridHeader().gridHeight;
      // 10.) Add some information about the grid
      gObject->gridHeight    = gObject->gMap.get()->GetGridHeader().gridHeight;
      gObject->gridWidth  	 = gObject->gMap.get()->GetGridHeader().gridWidth;
      gObject->scanDirection = static_cast<int>(gObject->gMap.get()->GetGridHeader().dType);
      gObject->scanType      = static_cast<int>(gObject->gMap.get()->GetGridHeader().sType);

      // 11.) Populate the calibration object. This becomes mandatory, but only
      //      if calibration is not set
      if (gObject->calibration == false)
      {
        ret = gObject->polygon.LoadModel(gObject->calibrationFilename);
        if (ret !=1) return -6;
      }
	  
      return 1;
    }//end of function
  }//end of namespace vista
}//end of namespace spin
