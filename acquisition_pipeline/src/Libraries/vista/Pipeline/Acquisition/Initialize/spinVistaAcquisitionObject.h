#ifndef SPINVISTAACQUISITIONOBJECT_H_
#define SPINVISTAACQUISITIONOBJECT_H_
#include "AbstractClasses/AbstractProcessingObject.h"
#include "Libraries/vista/Pipeline/Acquisition/DatabaseIO/DatabaseIO.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/GridMapGeneration.h"
#include "Utilities/ImageProcessing/FFTUtils/FFTTranslation.h"
#include "Utilities/ImageProcessing/FFTUtils/FFT2D.h"
#include "Utilities/ImageProcessing/HistogramUtils/HistogramUtils.h"
#include "Utilities/Geometry/PolygonUtils.h"

namespace spin
{
  namespace vista
  {
    /*! \brief spinVistaAcquisitionObject
     *  This structure contains all information that would be needed
     *  to process data acquired during whole slide stitching.
     */
    struct spinVistaAcquisitionObject : public spin::AbstractProcessingObject
    {
      // Default destructor
      ~spinVistaAcquisitionObject()
      {
        disp1_X.clear();
        disp1_Y.clear();
        disp2_X.clear();
        disp2_Y.clear();
        default_horizontal.clear();
        default_vertical.clear();
        mMaskFgBg.resize(0,0);
        mCDF_Fg.resize(0,0);
        mCDF_Bg.resize(0,0);
        mMaskFocus.resize(0,0);
        mPDF_Focus.resize(0,0);
        H_x.clear();
        H_y.clear();
        V_x.clear();
        V_y.clear();
        primary_disp.clear();
        primary_disp_meta.clear();
        secondary_disp.clear();
        secondary_disp_meta.clear();
      }//end of function

      /////////////////////////////////////////////////////////////////////////
      // List out all objects that will be used in the pipeline
      // FFT object
      std::shared_ptr< spin::FFT2D<float> > fft2d;
      // Histogram object
      std::shared_ptr<spin::HistogramUtils> hUtils;
      // An instance of a grid map object
      std::shared_ptr<GridMapGeneration> gMap;
      // Translation estimation objects
      std::shared_ptr<spin::FFTTranslation<float> > fftUtils1;
      std::shared_ptr<spin::FFTTranslation<float> > fftUtils2;
      // Database IO module
      std::shared_ptr<DatabaseIO> db;

      /////////////////////////////////////////////////////////////////////////
      // Acquisition parameters
      RGBImageType::RegionType mImageRegion;
      int gridId;
      std::string slideDir;

      /////////////////////////////////////////////////////////////////////////
      // Preprocessing parameters
      bool  Preprocessing;
      // Dimensions of the images
      // If an optimal dimension if required for FFT
      bool optimalFFT;
      // Dimensions of optimal image
      int imgWidth_d;
      int imgHeight_d;
      // Number of bins of histogram
      int HistogramBins;
      // The channel to be used for computing the fft
      int channel;
      // Subsampling to be effected
      int subsampling;
      // Radius of mask for ascertaining FG/BG
      int   maskradius;
      // The actual mask
      spin::RMMatrix_Float mMaskFgBg;
      // Histogram of the Fg/Bg template
      spin::RMMatrix_Float mCDF_Fg;
      spin::RMMatrix_Float mCDF_Bg;
      // Threshold for Fg/Bg determination
      float fgbgThresh;
      // Radius of mask for ascertaining focus
      int   focusradius;
      spin::RMMatrix_Float mMaskFocus;
      // Histogram of the focus template
      spin::RMMatrix_Float mPDF_Focus;
      // Threshold for well focused image
      float focusThresh;
      // Various image names
      std::string fgImage;
      std::string focusImage;

      /////////////////////////////////////////////////////////////////////////
      // Displacement estimation parameters
      bool EstimateTranslation;
      bool ignoreBG;
      bool ignoreBacklash;
      // Minimum connected component size
      int  minCC;
      // Type of graph
      int  graphType;
      // Displacement boundaries
      std::vector<double> disp1_Y;
      std::vector<double> disp1_X;
      std::vector<double> disp2_Y;
      std::vector<double> disp2_X;
      // Default displacements based on calibration
      std::vector<double> default_horizontal;
      std::vector<double> default_vertical;
      
      // Debug flags
      bool log_preprocessing;
      bool log_translationestimation;

      // Metric computation flags
      bool metrics_preprocessing;
      bool metrics_translationestimation;
      
      // If we want to capture calibration data
      std::vector<float> H_x;
      std::vector<float> H_y;
      std::vector<float> V_x;
      std::vector<float> V_y;
      std::vector< std::string > primary_disp;
      std::vector< std::string > secondary_disp;
      std::vector< std::string > primary_disp_meta;
      std::vector< std::string > secondary_disp_meta;
      std::string calibration_dir;

      // Polygon object
      std::string calibrationFilename;
      PolygonUtils polygon;

      // Grayscale processing
      bool isGrayscale;

      // Cost is unbiased
      bool unbiasedCost;
    };//end of struct
  }//end of namespace vista
}//end of namespace spin

#endif
