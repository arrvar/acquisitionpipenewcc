#ifndef GRIDMAPGENERATION_H_
#define GRIDMAPGENERATION_H_
#include <string>
#include <boost/bimap.hpp>
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "AbstractClasses/AbstractGraphHelper.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  namespace vista
  {
    /*! \brief A structure that holds information about
     *  weighted least-squares bundle optimization*/
    struct WLSStruct
    {
      ~WLSStruct()
      {
        HEdges.clear();
        VEdges.clear();
      }
      RMSparseMatrix_Double HDisp_Y;
      RMSparseMatrix_Double VDisp_Y;
      RMSparseMatrix_Double HDisp_X;
      RMSparseMatrix_Double VDisp_X;
      
      RMMatrix_Double AcrossCols_HDisp;
      RMMatrix_Double AcrossCols_VDisp;
      RMMatrix_Double AcrossRows_HDisp;
      RMMatrix_Double AcrossRows_VDisp;
      std::map<pDouble, int> HEdges;
      std::map<pDouble, int> VEdges;
    };//end of structure

    /*! \brief An enum to hold information about directions */
    enum DirectionType
    {
      WIDTH, // across columns
      HEIGHT // across rows
    };

    /*! \brief An enum to hold information about types of scans */
    enum ScanType
    {
      SNAKE,  // Snake pattern
      ZIGZAG, // Zig-zag pattern
      SPIRAL  // Spiral
    };

    /*! \brief A structure describing the grid information */
    struct GridInfo
    {
      ScanType      sType;
      DirectionType dType;
      double 	      subSampFactor;
      int           gridHeight;
      int           gridWidth;
      int           minCCSize;
      int           aoiWidth;
      int           aoiHeight;
      std::string   fileExt;
      std::string   calibImg="";
      std::string   normImg="";
      float         bgRatio=-1;
    };//end of structure

    /*! GridMapGeneration
     *  This class is a placeholder for all functions to estimate translation between
     *  two pairs of images
     */
    class GridMapGeneration
    {
      public:
        /*! \brief Default constructor*/
        GridMapGeneration(){}

        /*! \brief Default destructor*/
        ~GridMapGeneration()
        {
          Clear();
        }

        /*! \brief Constructor with grid coordinates as input*/
        int Initialize( char* dbFile, int GridId, int Magnification, void* db,
                        bool computeMetrics=false, bool generateLogs=false);

        // Define a buffer of FT images
        typedef std::map<pDouble, FOVStruct> gridFOV;
        // Define a buffer of index locations
        typedef std::vector< pDouble > gridIndex;
        // Define a buffer of mapping indices
        typedef std::map<pDouble, gridIndex> gridMapping;
        // Define a bi-directional mapping between 1D node index and a 2D index
        typedef boost::bimap< int, pDouble> nodeMapping;
        typedef nodeMapping::value_type position;
        // Define a sparse matrix that will hold the mappings between pairwise AOI's
        typedef RMSparseMatrix_Complex MMapping;
        // Define a sparse matrix that will hold the correlation cost between pariwise AOI's
        typedef RMSparseMatrix_Float   MCost;
        // Define a sparse matrix that will hold the MST
        typedef RMSparseMatrix_Int     MSTMapping;

        /*! \brief An API to get access to the gInfo Object*/
        GridInfo& GetGridHeader() { return gInfo; }
        /*! \brief An API to get access to the FFT object*/
        gridFOV* GetGridMapping() { return &bufferFFT; }
        /*! \brief An API to get grid indices based on the scanning pattern*/
        gridIndex* GetGridIndices() { return &gIdx; }
        /*! \brief An API to get access to the grid graph*/
        graph_t& GetGraph(){return Graph;}
        /*! \brief An API to get the weightmap associated with the graph*/
        edgeWeight_t& GetWeightMap(){return WeightMap;}
        /*! \brief An API to get the Node Mapping*/
        nodeMapping& GetNodeMap(){return NodeMap;}
        /*! \brief An API to get the Mapping matrix*/
        MMapping& GetTMapping(){return mMapping;}
        /*! \brief An API to get the Cost matrix*/
        MCost& GetTCost(){return mCost;}
        /*! \brief An API to get the MST mapping matrix*/
        MSTMapping& GetMSTMapping(){return mstMapping;}
        /*! \brief An API to get the components associated with WLS */
        WLSStruct& GetWLSStruct(){return wlsstruct;}
        /*! \brief An API to get the subsampling factor*/
        double GetSubSampFactor(){return gInfo.subSampFactor;}
        /*! \brief An API to get the GridId */
        int GetGridId(){return gridId;};
        /*! \brief An API to get the DB Path */
        std::string GetDBPath(){return dbPath;};
        /*! \brief An API to get the minimum connected component size*/
        int GetMinConnectedComponent(){return gInfo.minCCSize;}
        /*! \brief Clear Data */
        void Clear();

        /*! \brief An API to get values at a given index location*/
        int    GetXRemoval(int r, int c){return gridXRemoval(r,c);}
        int    GetYRemoval(int r, int c){return gridYRemoval(r,c);}
        double GetXCols(int r, int c){return gridXCols(r,c);}
        double GetYRows(int r, int c){return gridYRows(r,c);}

      private:
        // Define a few variables
        gridFOV           bufferFFT;
        gridIndex         gIdx;
        GridInfo          gInfo;
        RMMatrix_Double   gridYRows;
        RMMatrix_Double   gridXCols;
        RMMatrix_Int      gridYRemoval;
        RMMatrix_Int      gridXRemoval;
        graph_t           Graph;
        edgeWeight_t      WeightMap;
        nodeMapping       NodeMap;
        MMapping          mMapping;
        MCost             mCost;
        MSTMapping        mstMapping;
        std::string       dbPath;
        int               gridId;
        WLSStruct         wlsstruct;
        
      private:
        // Parse database
        int ParseDatabase(char* dbFile);
        // Implement some scan patterns
        int SnakePattern(bool cm=false, bool gl=false);
        int ZigzagPattern(bool cm=false, bool gl=false);
    };//end of class
  }//end of namespace vista
}//end of namespace spin
#endif
