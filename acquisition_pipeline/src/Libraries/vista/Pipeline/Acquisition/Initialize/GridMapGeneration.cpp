#include "Libraries/vista/Pipeline/Acquisition/Initialize/GridMapGeneration.h"
#include "Libraries/vista/Pipeline/Acquisition/DatabaseIO/DatabaseIO.h"

namespace spin
{
  namespace vista
  {

    /*! \brief GridMapGeneration
     *  This is an instantiation of a constructor wherein a set of grid coordinates
     *  are passed as a string array, separated by commas. We populate the
     *  respective object arrays from these values. By default, we assume that the
     *  overlap between neighboring AOI's is more than 50% in a single direction.
     *  @database       :     The name of the database
     *  @GridId         :     The grid Id that will be scanned. A database can
     *                        contain multiple grids
     *  @Magnification  :     The magnification at which this grid was acquired.
     *  @computeMetrics :     Do we want to generate metrics?
     *  @generateLogs   :     Do we want to generate logs?
     */
    int GridMapGeneration::Initialize(char* dataBase, int GridId, int Magnification,\
                                      void* db,\
                                      bool computeMetrics, bool generateLogs )
    {
      if (dataBase != NULL)
      {
        // Clear the data structure
        Clear();

        // Instantiate a database read object
        DatabaseIO* dbRead = static_cast<DatabaseIO*>(db);
        // Parse the database
        int v  = dbRead->ParseDatabase(dataBase, GridId, Magnification, this, generateLogs);
        if (v != 1)
        {
          return v;
        }

        // 2.) Allocate memory
        gridYRows     	= RMMatrix_Double::Zero(gInfo.gridHeight, gInfo.gridWidth);
        gridYRemoval  	= RMMatrix_Int::Zero(gInfo.gridHeight, gInfo.gridWidth);
        gridXCols     	= RMMatrix_Double::Zero(gInfo.gridHeight, gInfo.gridWidth);
        gridXRemoval  	= RMMatrix_Int::Zero(gInfo.gridHeight, gInfo.gridWidth);
        //pInfo.gridXCols	= RMMatrix_Double::Zero(gInfo.gridHeight, gInfo.gridWidth);
        //pInfo.gridYRows	= RMMatrix_Double::Zero(gInfo.gridHeight, gInfo.gridWidth);
        //fgMap             = RMMatrix_Double::Zero(gInfo.gridHeight, gInfo.gridWidth);
        mMapping.resize(gInfo.gridHeight*gInfo.gridWidth,gInfo.gridHeight*gInfo.gridWidth);
        mCost.resize(gInfo.gridHeight*gInfo.gridWidth,gInfo.gridHeight*gInfo.gridWidth);
        mstMapping.resize(gInfo.gridHeight*gInfo.gridWidth,gInfo.gridHeight*gInfo.gridWidth);

        // 3.) Populate the coordinates into a matrix
        auto count = 0;
        gridFOV::iterator it = bufferFFT.begin();
        while(it != bufferFFT.end())
        {
          // Populate the matrices
          gridYRows(it->second.index.first,it->second.index.second) = it->first.second;
          gridXCols(it->second.index.first,it->second.index.second) = it->first.first;

          // Set the node index
          it->second.nodeId = it->second.index.first + gInfo.gridHeight * it->second.index.second;

          // Populate a node map
          NodeMap.insert(position(it->second.nodeId,\
                                  std::make_pair( (double)it->second.index.first,
                                                  (double)it->second.index.second)) );
          // Add a node to the graph
          // http://www.boost.org/doc/libs/1_62_0/libs/graph/example/labeled_graph.cpp
          vertex_t u = boost::add_vertex(Graph);
          //increment the iterators
          ++it;
          ++count;
        }
        
        //std::cout<<bufferFFT.size()<<std::endl;
        
        // We now populate the weighted least-squares matrix
        int numNodes = gInfo.gridHeight * gInfo.gridWidth;
        wlsstruct.HDisp_X.resize(numNodes-gInfo.gridHeight, numNodes);
        wlsstruct.VDisp_X.resize(numNodes-gInfo.gridHeight, numNodes);
        wlsstruct.HDisp_Y.resize(numNodes-gInfo.gridWidth, numNodes);
        wlsstruct.VDisp_Y.resize(numNodes-gInfo.gridWidth, numNodes);
        
        wlsstruct.AcrossCols_HDisp = RMMatrix_Double::Zero(numNodes-gInfo.gridHeight,1);
        wlsstruct.AcrossCols_VDisp = RMMatrix_Double::Zero(numNodes-gInfo.gridWidth,1); 
        wlsstruct.AcrossRows_HDisp = RMMatrix_Double::Zero(numNodes-gInfo.gridHeight,1);
        wlsstruct.AcrossRows_VDisp = RMMatrix_Double::Zero(numNodes-gInfo.gridWidth,1);
               
        // In addition, we also populate a pair of LUT's to indicate which edges are
        // being populated
        // Horizontal edges
        count = 0;
        for (int y = 0; y < gInfo.gridHeight; ++y)
        for (int x = 1; x < gInfo.gridWidth ; ++x)
        {
          int refNode = NodeMap.right.at(std::make_pair(y,x));
          int tgtNode = NodeMap.right.at(std::make_pair(y,x-1));
          pDouble p = std::make_pair(refNode,tgtNode);
          wlsstruct.HEdges[p] = count;
          ++count;
        }
        
        // Vertical edges
        count = 0;        
        for (int x = 0; x < gInfo.gridWidth ; ++x)     
        for (int y = 1; y < gInfo.gridHeight; ++y) 
        {
          int refNode = NodeMap.right.at(std::make_pair(y,x));
          int tgtNode = NodeMap.right.at(std::make_pair(y-1,x));
          pDouble p = std::make_pair(refNode,tgtNode);
          wlsstruct.VEdges[p] = count;
          ++count;
        }

        // 4.) Now, fill up other values that will depend on the type of scan being employed
        if(gInfo.sType == ScanType::SNAKE)
        {
          SnakePattern(generateLogs);
        }
        else
        if (gInfo.sType== ScanType::ZIGZAG)
        {
          ZigzagPattern(generateLogs);
        }
        else
        if(gInfo.sType == ScanType::SPIRAL)
        {
          // Not yet implemented
        }

        // The first index for gridXRemoval and gridYRemoval should be 0
        gridXRemoval(0, 0) = 0;
        gridYRemoval(0, 0) = 0;

        // Also, build the weightmap
        WeightMap = boost::get(boost::edge_weight, Graph);

        return 1;
      }// If the database is valid

      return 0;
    }//end of function

    /*! \brief Clear
    *  This function is used to clear residual information from the data structure*/
    void GridMapGeneration::Clear()
    {
      // Clear all variables in bufferFFT
      gridFOV::iterator it = bufferFFT.begin();
      while (it != bufferFFT.end())
      {
        it->second.tNeighbors.clear();
        it->second.tMetrics.clear();
        it->second.fft.resize(0, 0);
        it->second.Img=NULL;
        ++it;
      }
      bufferFFT.clear();
      gIdx.clear();
      //Graph.clear();
      NodeMap.clear();
    }//end of function
  }//end of namespace vista
}//end of namespace spin
