#ifndef DATABASEIO_H
#define DATABASEIO_H

#include <QSqlDatabase>
#include <QStringList>

#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  namespace vista
  {
    class DatabaseIO
    {
      public:
        DatabaseIO()
        {
          m_db = QSqlDatabase::addDatabase(DATABASE_DRIVER, DATABASE_NAME);
        }

        ~DatabaseIO()
        {
          if (m_db.isOpen())
          {
            m_db.close();
          }
        }

        // signals for logs
        spin::CallbackSignal sig_Logs;
        /*! \brief ParseDatabase */
        int ParseDatabase(char* database, int gridId, int magnification, void* obj, bool logs=false);
        /*! \brief WriteToDatabase */
        //void WriteToDatabase(std::string& database, int gridId, void* obj, spin::CallbackStruct p);

      private:
        bool clearMstPath(int gridId);

        bool clearBlendingMetrics(int gridId);

        bool storeMstPath(int gridId, int rowIdx, int colIdx, QString mstPath);

        bool storeBlendingMetrics(int gridId, int rowIdx, int colIdx, QString blendingMetrics);

        bool updateBackgroundStatus(int gridId, QString aoiName, int backgroundStatus);

        bool getGridInfo(int gridId, void* obj);

        bool getImageInfo(int gridId, int magnification, void* obj);

        const static QString DATABASE_NAME;

        const static QString DATABASE_DRIVER;

        QSqlDatabase m_db;

    };//end of class
  }//end of namespace vista
}//end of namespace spin
#endif
