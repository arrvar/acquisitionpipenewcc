#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>

#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDriver>

#include "Libraries/vista/Pipeline/Acquisition/DatabaseIO/DatabaseIO.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/spinVistaAcquisitionObject.h"

using namespace spin;
using namespace vista;

const QString DatabaseIO::DATABASE_NAME = "SLIDE_DB";

const QString DatabaseIO::DATABASE_DRIVER = "QSQLITE";

bool DatabaseIO::getGridInfo(int gridId, void* obj)
{
  GridInfo* gInfo = static_cast <GridInfo*> (obj);

  bool retVal = true;

  QString selectQueryStr;
  selectQueryStr.sprintf("SELECT pattern, direction, row_count, column_count FROM "
                          "grid_info WHERE grid_id = %d", gridId);

  QSqlQuery selectQuery(m_db);
  if (selectQuery.exec(selectQueryStr))
  {
    while (selectQuery.next())
    {
      // Scan Pattern.
      int scanType = selectQuery.value(0).toInt();
      if (scanType == 0)
      {
        gInfo->sType = spin::vista::ScanType::SNAKE;
      }
      else if (scanType == 1)
      {
        gInfo->sType = spin::vista::ScanType::ZIGZAG;
      }
      else if (scanType == 2)
      {
        gInfo->sType = spin::vista::ScanType::SPIRAL;
      }

      // Scan Direction.
      int scanDirection = selectQuery.value(1).toInt();
      if (scanDirection == 1)
      {
        gInfo->dType = spin::vista::DirectionType::HEIGHT;
      }
      else if (scanDirection == 0)
      {
        gInfo->dType = spin::vista::DirectionType::WIDTH;
      }

      // Grid Height.
      int gridHeight = selectQuery.value(2).toInt();
      gInfo->gridHeight = gridHeight;

      // Grid Width.
      int gridWidth = selectQuery.value(3).toInt();
      gInfo->gridWidth = gridWidth;
    }
  }
  else
  {
    qDebug() << "DatabaseIO::getGridInfo select query error: " << selectQuery.lastError().text();
    retVal = false;
  }

  return retVal;
}

bool DatabaseIO::getImageInfo(int gridId, int magnification, void* obj)
{
  // typecast the object
  GridMapGeneration* gmap             = static_cast<GridMapGeneration*>(obj);
  GridInfo* gInfo                     = &gmap->GetGridHeader();
  std::map<pDouble, FOVStruct>* dFov  = gmap->GetGridMapping();

  bool retVal = true;

  QString selectQueryStr;
  selectQueryStr.sprintf("SELECT aoi_name, aoi_y, aoi_x, aoi_row_idx, "
                         "aoi_col_idx, background "
                         "FROM aoi "
                         "WHERE grid_id = %d AND magnification = %d",
                         gridId, magnification);

  QSqlQuery selectQuery(m_db);
  if (selectQuery.exec(selectQueryStr))
  {
    while (selectQuery.next())
    {
      FOVStruct fs;

      // Image name.
      QString imgName = selectQuery.value(0).toString();

      // Image x and y positions in the grid.
      double yPos = selectQuery.value(1).toDouble();
      double xPos = selectQuery.value(2).toDouble();
      double rowIdx = selectQuery.value(3).toDouble();
      double colIdx = selectQuery.value(4).toDouble();

      // do some basic checks. It may so happen that the user only
      // wants a subset of this grid
      if (rowIdx >=0 && rowIdx < (double)gInfo->gridHeight &&\
          colIdx >=0 && colIdx < (double)gInfo->gridWidth)
      {
        fs.aoiName = imgName.toStdString();
        std::pair<double, double> pt = std::make_pair(rowIdx, colIdx);
        fs.index = std::make_pair(rowIdx, colIdx);

        // Image background status. Backround = 1 indicates that it is a background area (no material present) and
        // Background = 0 indicates that it is not a background area (material present).
        int background = selectQuery.value(5).toInt();

        (*dFov)[pt] = fs;
      }
    }
  }
  else
  {
    qDebug() << "DatabaseIO::getImageInfo select query error: " << selectQuery.lastError().text();
    retVal = false;
  }

  return retVal;
}


/*! \brief ParseDatabase
  *  This function is used to parse a database and populate a grid map that will
  *  be used for estimating displacements.
  *  @database : The database that will be passed as an input.
  *  At the end of this function call, two data structures will be populated
  */
int DatabaseIO::ParseDatabase(char* database, int gridId, int magnification, void* obj, bool generateLogs)
{
  // typecast the object
  GridMapGeneration* gmap             = static_cast<GridMapGeneration*>(obj);
  GridInfo* gInfo                     = &gmap->GetGridHeader();
  std::map<pDouble, FOVStruct>* dFov  = gmap->GetGridMapping();

  m_db.setDatabaseName(database);
  m_db.open();

  if (m_db.isOpen() && m_db.isValid())
  {
    // Retrieve grid information.
    getGridInfo(gridId, gInfo);

    // Retrieve image information.
    getImageInfo(gridId, magnification, obj);
  }
  else
  {
    qDebug() << "Database connection failed : " << m_db.lastError().text();
  }
  m_db.close();

  return 1;
}//end of function
