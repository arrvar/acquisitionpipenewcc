#include "Libraries/vista/Pipeline/Panorama/Initialize/spinVistaPanoramaInitialize.h"
#include "Libraries/vista/Pipeline/Panorama/Initialize/spinVistaPanoramaObject.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include <itkExtractImageFilter.h>
#include <itkFlipImageFilter.h>
#include <itkImageRegionIterator.h>
#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <algorithm>
#include <fstream>

namespace spin
{
  namespace vista
  {
    /*! \brief CreateDefaultTile
     *  This is a function to create a default tile that will be used
     *  when no additional information from images is available. The
     *  default tile is merely a cropped region from the calibration
     *  image. However, in order to avoid visible banding effects in the
     *  background regions, we apply some form of blending at the
     *  image boundaries.
     */
    int CreateDefaultTile(spinVistaPanoramaObject* gObject)
    {
      /*
      // We need to load a base default tile irrespective of whether a tile
      // needs to be created or not
      // Check if the default tile exists
      boost::filesystem::path dir(gObject->defTile.c_str());
      if (!boost::filesystem::exists(dir))
      {
        return -1;
      }

      gObject->mDefaultTile = RGBImageType::New();
      spin::ReadITKImage<RGBImageType>(gObject->defTile,gObject->mDefaultTile);

      // Compute the min-max of the default tile
      std::vector<float> tileMin;
      std::vector<float> tileMax;
      //MinMaxRGBImage(&gObject->mDefaultTile, &gObject->mMin, &gObject->mMax);
      MinMaxRGBImage(&gObject->mDefaultTile, &tileMin, &tileMax);

      std::vector<float> calMin;
      std::vector<float> calMax;
      MinMaxRGBImage(&gObject->mCalImage, &calMin, &calMax);
      //std::cout<<calMax[0]<<" "<<calMax[1]<<" "<<calMax[2]<<std::endl;
      //std::cout<<tileMax[0]<<" "<<tileMax[1]<<" "<<tileMax[2]<<std::endl;

      // Check if the norm image exists. If this image is not present, we
      // will overwrite the default tile with a normalized version of this
      // and the calibration image
      boost::filesystem::path dir1(gObject->normImage.c_str());
      if (boost::filesystem::exists(dir1))
      {
        // load the norm image
        RGBImageType::Pointer normImage = RGBImageType::New();
        spin::ReadITKImage<RGBImageType>(gObject->normImage, normImage);
        // Do a white calibration on this image
        if (gObject->normType == 0)
        {
          spin::NormalizeRGBImageWithCalImage(&normImage,
                                              &gObject->mCalImage,
                                              &gObject->mMax,
                                              &normImage);
        }
        else
        if (gObject->normType == 1)
        {
          spin::NormalizeRGBImageWithCalImage(&normImage,
                                              &gObject->mCalImage,
                                              &normImage);
        }

        // From this image, we crop a central region from the image and save
        // it as a default tile
        typedef itk::ExtractImageFilter<spin::RGBImageType,spin::RGBImageType> crop;
        crop::Pointer pCrop = crop::New();
        spin::RGBImageType::SizeType size = normImage->GetLargestPossibleRegion().GetSize();
        spin::RGBImageType::SizeType size1 = normImage->GetLargestPossibleRegion().GetSize();
        spin::RGBImageType::IndexType index;
        index[0] = ((size[0])>>1) - ((gObject->tileWidth)>>1);
        index[1] = ((size[1])>>1) - ((gObject->tileHeight)>>1);
        size[0]  = gObject->tileWidth;
        size[1]  = gObject->tileHeight;
        spin::RGBImageType::RegionType region;
        region.SetSize(size);
        region.SetIndex(index);
        pCrop->SetExtractionRegion(region);
        pCrop->SetInput(normImage);
        pCrop->SetDirectionCollapseToIdentity();
        pCrop->Update();
        memcpy(gObject->mDefaultTile->GetBufferPointer(), pCrop->GetOutput()->GetBufferPointer(), 3 * size[0] * size[1]);
        normImage = NULL;
      }

      // Flip the image (along columns X-axis)
      typedef itk::FlipImageFilter<spin::RGBImageType> Flip;
      Flip::Pointer flip1 = Flip::New();
      itk::FixedArray<bool, 2> flipAxes;
      flipAxes[0] = true;
      flipAxes[1] = false;
      flip1->SetInput(gObject->mDefaultTile);
      flip1->SetFlipAxes(flipAxes);
      flip1->Update();
      // Now, we compute an average of both images
      typedef itk::ImageRegionIterator<RGBImageType> iterator;
      iterator it1(flip1->GetOutput(), flip1->GetOutput()->GetLargestPossibleRegion());
      iterator it2(gObject->mDefaultTile, flip1->GetOutput()->GetLargestPossibleRegion());
      it1.GoToBegin();
      it2.GoToBegin();
      while(!it1.IsAtEnd())
      {
        spin::RGBPixelType pix1 = it1.Get();
        spin::RGBPixelType pix2 = it2.Get();

        float r = std::floor((pix1.GetRed() + pix2.GetRed())/2.0 + 0.5);
        float g = std::floor((pix1.GetGreen() + pix2.GetGreen())/2.0 + 0.5);
        float b = std::floor((pix1.GetBlue() + pix2.GetBlue())/2.0 + 0.5);
        pix2.SetRed(static_cast<unsigned char>(r));
        pix2.SetGreen(static_cast<unsigned char>(g));
        pix2.SetBlue(static_cast<unsigned char>(b));
        it2.Set(pix2);
        ++it1;
        ++it2;
      }

      // Flip the image (along rows Y-axis)
      Flip::Pointer flip2 = Flip::New();
      flipAxes[0] = false;
      flipAxes[1] = true;
      flip2->SetInput(gObject->mDefaultTile);
      flip2->SetFlipAxes(flipAxes);
      flip2->Update();
      // Now, we compute an average of both images
      iterator it3(flip2->GetOutput(), flip2->GetOutput()->GetLargestPossibleRegion());
      iterator it4(gObject->mDefaultTile, flip2->GetOutput()->GetLargestPossibleRegion());
      it3.GoToBegin();
      it4.GoToBegin();
      while(!it3.IsAtEnd())
      {
        spin::RGBPixelType pix1 = it3.Get();
        spin::RGBPixelType pix2 = it4.Get();

        float r = std::floor((pix1.GetRed() + pix2.GetRed())/2.0 + 0.5);
        float g = std::floor((pix1.GetGreen() + pix2.GetGreen())/2.0 + 0.5);
        float b = std::floor((pix1.GetBlue() + pix2.GetBlue())/2.0 + 0.5);
        pix2.SetRed(static_cast<unsigned char>(r));
        pix2.SetGreen(static_cast<unsigned char>(g));
        pix2.SetBlue(static_cast<unsigned char>(b));
        it4.Set(pix2);
        ++it3;
        ++it4;
      }

      // This tile is used to compute the min/max
      MinMaxRGBImage(&gObject->mDefaultTile, &gObject->mMin, &gObject->mMax);


      // At this time, we scale the default tile such that we bring it to the
      typedef itk::ImageRegionIterator<RGBImageType> iterator;
      iterator it1(gObject->mDefaultTile, gObject->mDefaultTile->GetLargestPossibleRegion());
      it1.GoToBegin();

      while(!it1.IsAtEnd())
      {
        spin::RGBPixelType pix1 = it1.Get();
        // Apply the scaling value
        float r = std::floor(((pix1.GetRed() / tileMax[0]) * gObject->mMax[0] + 0.5) * (gObject->mMax[0]+30)/calMax[0]);
        float g = std::floor(((pix1.GetGreen() / tileMax[1]) * gObject->mMax[1] + 0.5)* (gObject->mMax[1]+30)/calMax[1]);
        float b = std::floor(((pix1.GetBlue() / tileMax[2]) * gObject->mMax[2] + 0.5)* (gObject->mMax[2]+30)/calMax[2]);
        pix1.SetRed(static_cast<unsigned char>(215));
        pix1.SetGreen(static_cast<unsigned char>(215));
        pix1.SetBlue(static_cast<unsigned char>(215));
        it1.Set(pix1);
        ++it1;
      }
      */

      // The pipeline for generating the default tile is as follows:
      // a.) We load the mCalib and normalize it with mNormalize;
      // b.) We scale it with mMax
      // c.) We crop out a region from the center of this normalized image
      // d.) We use this cropped region as the default tile
      spin::NormalizeRGBImageWithCalImage(&gObject->mNormImage,
                                          &gObject->mCalImage,
                                          &gObject->mMax,
                                          &gObject->mNormImage);
      // From this image, we crop a central region from the image and save
      // it as a default tile
      typedef itk::ExtractImageFilter<spin::RGBImageType,spin::RGBImageType> crop;
      crop::Pointer pCrop = crop::New();
      spin::RGBImageType::SizeType size = gObject->mNormImage->GetLargestPossibleRegion().GetSize();
      spin::RGBImageType::IndexType index;
      index[0] = ((size[0])>>1) - ((gObject->tileWidth)>>1);
      index[1] = ((size[1])>>1) - ((gObject->tileHeight)>>1);
      size[0]  = gObject->tileWidth;
      size[1]  = gObject->tileHeight;
      spin::RGBImageType::RegionType region;
      region.SetSize(size);
      region.SetIndex(index);
      pCrop->SetExtractionRegion(region);
      pCrop->SetInput(gObject->mNormImage);
      pCrop->SetDirectionCollapseToIdentity();
      pCrop->Update();

      // Allocate memory for default tile
      memcpy(gObject->mDefaultTile->GetBufferPointer(), pCrop->GetOutput()->GetBufferPointer(), 3 * size[0] * size[1]);
      return 1;
    }//end of function

    /*! \brief PanoramaInitialization()
     *  This function initializes all elements associated with the
     *  pre-processing module of the pipeline
     */
    int PanoramaInitialization(spinVistaPanoramaObject* gObject)
    {
      typedef spin::RGBImageType RGBImageType;

      // Check if the cal image exists
      boost::filesystem::path dir(gObject->bgImage.c_str());
      if (!boost::filesystem::exists(dir))
      {
        return -1;
      }
      /////////////////////////////////////////////////////////////////////////
      // Instantiate the ImageIO object with the calibration image
      // We assume that all images will conform with specifications
      // described by the calibration image
      gObject->ImageIO = std::make_shared<spin::RGBImageIO>(gObject->bgImage);

      // Read the normalization image
      gObject->mCalImage = spin::RGBImageType::New();
      int v = gObject->ImageIO.get()->ReadImage(gObject->bgImage, &gObject->mCalImage, false);
      if (v != 1) return -2;

      // By default we read the default tile image
      gObject->mDefaultTile = spin::RGBImageType::New();
      spin::ReadITKImage<RGBImageType>(gObject->defTile,gObject->mDefaultTile);

      // And check if the filler creation image exists
      boost::filesystem::path dir1(gObject->normImage.c_str());
      if (boost::filesystem::exists(dir1))
      {
        // Read the filler-creation image
        gObject->mNormImage = spin::RGBImageType::New();
        v = gObject->ImageIO.get()->ReadImage(gObject->normImage, &gObject->mNormImage, false);
        if (v !=1) return -3;

        // And create an in-situ default tile
        v = CreateDefaultTile(gObject);
        if ( v !=1 ) return -4;
      }

      // Assign the image region
      gObject->mImageRegion = gObject->mCalImage->GetLargestPossibleRegion();

      return 1;
    }//end of function

    /*! \brief Initialize()
     *  This function is used to parse grid information
     *  that will eventually result in the scan pattern. Before calling
     *  this function, it is assumed that this object is initialized.
     */
    int spinVistaPanoramaInitialize::Initialize(void* obj, const char* ParameterFileName)
    {
      // Check if the parameter file parser is correct
      std::ifstream input(ParameterFileName);
      // populate tree structure pt
      using boost::property_tree::ptree;
      ptree pt;
      try
      {
        // parse the xml file
        read_xml(input, pt);
      }
      catch(const boost::property_tree::ptree_error &e)
      {
        return -1;
      }

      // Here, we downcast a default object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);

      if (gObject == NULL) return -2;

      // Default database
      gObject->dbFile = "";
      // By default blending is disabled
      gObject->inlineBlending = false;
      // By default noise suppression is disabled
      gObject->noiseSuppression = false;
      // By Default we do not normalize the image
      gObject->Normalize = false;
      // Initialize the Database IO
      gObject->db = std::make_shared<DatabaseIO>();
      // Do We prefer inlineTiling
      gObject->inlineTiling = false;
      // Disable pyramid generation
      gObject->InlinePyramid = false;
      // Dimensions of tile
      gObject->tileWidth = 512;
      gObject->tileHeight= 512;
      gObject->TileExt = ".jpeg";
      // Disable tiling else all folders will be removed.
      gObject->TileDir = "."; // current directory
      // The model file
      std::string modelFile="";
      // Default values for sharpness
      gObject->sharpness_strength = 0.8;
      gObject->sharpness_kernel  = 5;
      // Default flags for metrics and logging
      gObject->log_blending               = false;
      gObject->log_tiling                 = false;
      gObject->metrics_blending           = false;
	    gObject->disable_blending           = false;
      gObject->blendingThresh             = -1;
      gObject->calibration                = false;
      gObject->delay                      = 50;// 50 msec delay between two images 
      gObject->mMax={220,220,220};
      std::string logFile                 ="";

      // 1.) Now, parse through all parameter settings and populate gObject
      BOOST_FOREACH(ptree::value_type& v, pt.get_child("vistaPanorama"))
      {
        if (v.first == "Logging_File")
        {
          logFile = v.second.get<std::string>("");
        }
        else
        if (v.first == "Normalization_Image")
        {
          gObject->bgImage = v.second.get<std::string>("");
        }
        else
        if (v.first == "Filler_Generation_Image")
        {
          gObject->normImage = v.second.get<std::string>("");
        }
        else
        if (v.first == "Normalize_Image")
        {
          int x = v.second.get<int>("");
          if (x ==0 || x == 1)
          {
            gObject->normType = x;
            gObject->Normalize = true;
          }
        }
        else
        if (v.first == "ScalingFactors")
        {
          std::string fact = v.second.get<std::string>("");
          std::vector<std::string> tRes;
          boost::split(tRes, fact, boost::is_any_of(","));
          if (tRes.size() == 3)
          {
            gObject->mMax[0] = std::stof(tRes[0]);
            gObject->mMax[1] = std::stof(tRes[1]);
            gObject->mMax[2] = std::stof(tRes[2]);
          }
          tRes.clear();
        }
        else
        if (v.first == "Default_Tile_Image")
        {
          gObject->defTile = v.second.get<std::string>("");
        }
        else
        if (v.first == "Model_File")
        {
          modelFile = v.second.get<std::string>("");
        }
        else
        if (v.first == "Delay")
        {
          gObject->delay = v.second.get<float>("");
        }
        else if (v.first == "Blending")
        {
          gObject->inlineBlending = true;
          // Check for tags within PreProcessing
          BOOST_FOREACH(ptree::value_type& v1, v.second)
          {
            if (v1.first == "Calibration")
            {
              gObject->calibration = true;
            }
            else
            if (v1.first == "NoiseFiltering")
            {
              gObject->noiseSuppression = true;
            }
            else
            if (v1.first == "Logging")
            {
              gObject->log_blending = true;
            }
			      else 
            if (v1.first == "Disable")
            {
              gObject->disable_blending = true;
            }
            else
            if (v1.first == "BlendingThreshold")
            {
              gObject->blendingThresh = v1.second.get<float>("");
            }
            else
            if (v1.first == "Plugin")
            {
              spin::ModuleObject module;
              int h = 0;
              BOOST_FOREACH(ptree::value_type& v2, v1.second)
              {
                if (v2.first=="Hierarchy")
                  h = v2.second.get<int>("");
                else
                if (v2.first=="Name")
                  module.pModuleName = v2.second.get<std::string>("");
                else
                if (v2.first=="Parameters")
                  module.pParamFile = v2.second.get<std::string>("");
                else
                if (v2.first=="Path")
                  module.pPath=v2.second.get<std::string>("");
              }
              bool f  = ( (module.pModuleName=="") || (module.pPath=="") );
              // If all parameters have been satisfied
              if (!f)
              {
                gObject->mPlugins[h].push_back(module);
                // increment plugin count
                ++h;
              }
            }
          }
        }//all tags associated with blending
        else if (v.first == "Tiling")
        {
          gObject->inlineTiling = true;

          // Check for tags within Tiling
          BOOST_FOREACH(ptree::value_type& v1, v.second)
          {
            if (v1.first == "Tile_Width")
            {
              gObject->tileWidth = v1.second.get<int>("");
            }
            else
            if (v1.first == "Tile_Height")
            {
              gObject->tileHeight = v1.second.get<int>("");
            }
            else
            if (v1.first == "TileDir")
            {
              gObject->TileDir = v1.second.get<std::string>("");
            }
            else
            if (v1.first == "Img_Format")
            {
              gObject->TileExt = "." + v1.second.get<std::string>("");
            }
            else
            if (v1.first == "Logging")
            {
              gObject->log_tiling = true;
            }
            else
            if (v1.first == "PyramidGeneration")
            {
              gObject->InlinePyramid = true;
            }
            else
            if (v1.first == "Sharpness_Kernel")
            {
              gObject->sharpness_kernel = v1.second.get<float>("");
            }
            else
            if (v1.first == "Sharpness_Strength")
            {
              gObject->sharpness_strength = v1.second.get<float>("");
            }
          }
        }
      }// finish parsing all tags within "vista"

      // 2.) Let us check if the respective directories exist or need to be created
      // Please note that we should have permissions to create these folders
      if (gObject->inlineTiling)
      {
        boost::filesystem::path dir(gObject->TileDir.c_str());
        if (!boost::filesystem::exists(dir))
        {
          if (!boost::filesystem::create_directory(dir))
          {
            return -3;
          }
        }
        else
        {
          // This directory exists, we delete the existing directory and then
          // create it again
          boost::filesystem::remove_all(dir);
          if (!boost::filesystem::create_directory(dir))
          {
            return -3;
          }
        }
      }

      // 3.) Instantiate a logger object
      bool generateLogs = gObject->log_tiling || gObject->log_blending;
      if (generateLogs)
      {
        // Instantiate a logging object
        gObject->logger = std::make_shared<spin::LoggerInterface>();
        if (logFile != "")
        {
          gObject->loggingInitialized = true;
          gObject->logger.get()->SetLoggingFile(logFile);
        }
        // and initialize it
        gObject->logger.get()->Initialize();
      }

      // 4.) Create a panorama object
      gObject->gMap = std::make_shared<PanoramaObject>();

      // 5.) Deserliaze the model file (if available)
      {
        boost::filesystem::path dir(modelFile.c_str());
        if (!boost::filesystem::exists(dir))
        {
          // Model file is invalid. Return to the calling function
          return -4;
        }
        // create and open an archive for input
        std::ifstream ifs(modelFile.c_str());
        boost::archive::binary_iarchive ia(ifs);
        // read class state from archive
        ia >>(*(gObject->gMap.get()));
      }

      // 6.)  At this time, we load the calibration image and default tiles
      // to memory. Note, the calibration image should have been parsed from the model.
      //gObject->bgImage    = gObject->gMap.get()->GetCalibrationImage();
      //gObject->normImage  = gObject->bgImage;//gObject->gMap.get()->GetNormImage();
      int g = PanoramaInitialization(gObject);
      if (g !=1) return -5;

      // get the file extension
      gObject->file_extension = gObject->gMap.get()->GetFileExtension();
      if (gObject->file_extension == "") return -6;
	  
	    // Set the tile dimensions
	    gObject->gMap->SetTileDimensions(gObject->tileHeight, gObject->tileWidth);
      return 1;
    }//end of function
  }//end of namespace vista
}//end of namespace spin
