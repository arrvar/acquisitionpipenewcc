#ifndef SPINVISTAPANORAMAINITIALIZE_H_
#define SPINVISTAPANORAMAINITIALIZE_H_
#ifndef VISTA_PLUGIN_FRAMEWORK
#include "Framework/Exports/spinAPI.h"
#endif

namespace spin
{
  namespace vista
  {
    /*! \brief A base class for initializing all objects/parameters
     *  associated with spinVista
     */
#ifndef VISTA_PLUGIN_FRAMEWORK
    class SPIN_API spinVistaPanoramaInitialize
#else
    class spinVistaPanoramaInitialize
#endif
    {
      public:
        // Constructor
        spinVistaPanoramaInitialize(){}
        // API to initialize
        int Initialize(void* obj, const char* param);
    };//end of class
  }//end of namespace
}//end of namespace
#endif
