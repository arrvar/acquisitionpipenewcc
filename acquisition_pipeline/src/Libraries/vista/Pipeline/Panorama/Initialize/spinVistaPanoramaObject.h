#ifndef SPINVISTAPANORAMAOBJECT_H_
#define SPINVISTAPANORAMAOBJECT_H_
#include "AbstractClasses/AbstractProcessingObject.h"
#include "Libraries/vista/Pipeline/Panorama/DatabaseIO/DatabaseIO.h"
#include "Utilities/Stitching/Panorama/PanoramaDataStructures.h"

namespace spin
{
  namespace vista
  {
    /*! \brief spinVistaPanoramaObject
     *  This structure contains all information that would be needed
     *  to process AOI's and tile them into a panorama
     */
    struct spinVistaPanoramaObject : public spin::AbstractProcessingObject
    {
      // Default destructor
      ~spinVistaPanoramaObject()
      {
        mMin.clear();
        mMax.clear();
      }//end of function

      // The default tile
      std::string defTile;
      // The parent directory where the images are located
      std::string imgDir;

      std::string normImage;
      // The image region
      RGBImageType::Pointer mNormImage;
      RGBImageType::RegionType mImageRegion;

      // An instance of a grid map object
      std::shared_ptr<PanoramaObject> gMap;

      // If Blending is required
      bool  inlineBlending;
      // Suppress noise in blended images
      bool  noiseSuppression = false;
      // If inline tiling is required
      bool  inlineTiling;
      // If inline pyramid generation is required
      bool  InlinePyramid;

      // Tile dimensions
      int   tileWidth;
      int   tileHeight;
      spin::RGBImageType::Pointer mDefaultTile;
      std::string TileExt;
      std::string TileDir;
      std::vector<float> mMin;
      std::vector<float> mMax;

      // Debug flags
      bool log_blending;
      bool log_tiling;

      // During calibration we would want to check some values
      bool calibration;
      // Metric computation flags
      bool metrics_blending;
      float blendingThresh;
      // If we want to disable blending (for calibration)
      bool disable_blending;

      // Database IO module
      std::shared_ptr<DatabaseIO> db;
      
      // delay in reading two images
      float delay;// in milliseconds

      // sharpness module
      float sharpness_strength;
      float sharpness_kernel;
    };//end of struct
  }//end of namespace vista
}//end of namespace spin

#endif
