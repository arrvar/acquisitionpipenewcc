#include "Libraries/vista/Pipeline/Panorama/ImageRead/ImageReadInterface.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  namespace vista
  {
    /*! \brief AddToQueue
     *  This function adds an object to the Pre-processing queue
     */
    template<class P>
    void ImageReadInterface<P>::AddToQueue(void* dataPoint)
    {
      // typecasts
      ProducerDataType* dp = static_cast<ProducerDataType*>(dataPoint);

      //emit the signal
      sig_AddQ(*dp);
      dp->rgbImage = NULL;
    }//end of function

    // explicit instantiation of template class
    template class ImageReadInterface<ProducerDataType>;
  }//end of stitching namespace
}//end of spin namespace
