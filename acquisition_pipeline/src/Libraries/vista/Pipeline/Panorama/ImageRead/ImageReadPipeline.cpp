// Pipeline includes
#include "Libraries/vista/Pipeline/Panorama/ImageRead/ImageReadPipeline.h"
#include "Libraries/vista/Pipeline/Panorama/Blending/spinVistaBlending.h"
#include "Libraries/vista/Pipeline/Panorama/Initialize/spinVistaPanoramaObject.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include "AbstractClasses/AbstractImageTypes.h"

namespace spin
{
  namespace vista
  {
    namespace
    {
      typedef spin::PanoramaObject PanoramaObject;
      typedef std::map<pDouble, spin::BlendingTilingObject> FOV;
    }

    /*! \brief Destructor */
    template<class P>
    ImageReadPipeline<P>::~ImageReadPipeline()
    {
      // Call the destructor in the sub routines as well
      AbortPipeline();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageReadPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelaySignalFinished
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageReadPipeline<P>::RelaySignal(spin::CallbackStruct p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageReadPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief ClearMemory
     *  A slot to indicate that the image is no longer required
     */
    template <class P>
    void ImageReadPipeline<P>::ClearMemory(void* obj, spin::FovCompleted p)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      // And get the grid mapping
      int grid = (gObject->gMap.get()->GetGridOfInterest());
      FOV* dFov = const_cast<FOV*>(gObject->gMap.get()->GetAOI(grid));
      // Build the index
      pDouble it(p.gYRow, p.gXCol);
      // And set the memory to be deleted by ITK's smart pointer
      (*dFov)[it].Img = NULL;
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline.
     */
    template<class P>
    int ImageReadPipeline<P>::InstantiatePipeline(void* obj, bool _blending)
    {
      // Instantiate the next set of pipelines, if requested
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      if (gObject == NULL) return -1;

      if (gObject->inlineBlending)
      {
        // Or during blending/tiling?
        mBlending = std::make_shared<SpinBlending<P>>();
        mBlending.get()->InstantiatePipeline(obj);
        mBlending.get()->sig_Finished.connect(boost::bind(&ImageReadPipeline<P>::RelaySignal,this,_1));
        mBlending.get()->sig_FOV.connect(boost::bind(&ImageReadPipeline<P>::ClearMemory,this,obj,_1));
      }

      this->sig_FOV.connect(boost::bind(&ImageReadPipeline<P>::ClearMemory, this, obj, _1));
      abort = false;

      // TODO: Some other initializations may be needed for acquisition from the camera
      return 1;
    }//end of function

    /*! \brief ProcessPipeline
     * Processes the data that is input from the source. Internally it pushes
     * the data into an appropriate data structure and then to a queue for
     * any consumer to pick it up.
     * parameters :
     * @P: The pointer to a data point from the producer. It contains
     *     an image that will need to be processed
     */
    template<class P>
    int ImageReadPipeline<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // Typecast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      if (gObject == NULL) return -1;

      // Get the grid of interest
      int grid = (gObject->gMap.get()->GetGridOfInterest());
      double neighbors[] = { -1,-1,-1,0,-1,1,0,-1,0,1,1,-1,1,0,1,1 };
      const spin::RMMatrix_Double *validIndex = gObject->gMap.get()->GetYRows(grid);
      // Get the grid of interest
      FOV* tFov = const_cast<FOV*>(gObject->gMap.get()->GetAOI(grid));
      if (!validIndex->data())
      {
        return -3;
      }

      // 1.) Build the index
      pDouble it(producerDataPoint->gYRow, producerDataPoint->gXCol);

      // 2.) Build the name
      std::string inpImage = gObject->gMap.get()->GetImageDirectory() + \
                            "/" + \
                            (*tFov)[it].aoiName + \
                            gObject->file_extension;
	    ////std::cout<<"==============="<<std::endl;
      //std::cout<<"Processing: "<<inpImage<<std::endl;

      // 3.) Check if this image was corrected
      if (!((*tFov)[it].corrected) )
      {
        // The image will be loaded from disk
        gObject->ImageIO.get()->ReadImage(inpImage, &(*tFov)[it].Img, true);
        if (gObject->Normalize)
        {
          if (gObject->normType == 0)
          {
            // And white corrected
            spin::NormalizeRGBImageWithCalImage(&(*tFov)[it].Img,
                                                &gObject->mCalImage,
                                                &gObject->mMax,
                                                &(*tFov)[it].Img);
          }
          else
          {
            // And white corrected
            spin::NormalizeRGBImageWithCalImage(&(*tFov)[it].Img,
                                                &gObject->mCalImage,
                                                &(*tFov)[it].Img);
          }
        }
		    // Mark this image as having undergone correction or loaded
		    (*tFov)[it].corrected = true;
      }

      // 4.) At this time we will populate neighbors of this image
      (*tFov)[it].bNeighbors.clear();
      int gHeight = (gObject->gMap.get()->GetOriginalGridHeight());
      int gWidth  = (gObject->gMap.get()->GetOriginalGridWidth());

      // Populate the list of neighbors
      for (int i = 0; i < 8; ++i)
      {
        double hY = it.first + neighbors[2 * i];
        double wX = it.second+ neighbors[2 * i + 1];

        // A neighbor is considered valid if:
        //a.) It satisfies the grid bounds, and
        if (hY >= 0 && wX >= 0 && hY < gHeight && wX < gWidth)
        {
          // b.) The index is part of the grid. This means that the
          //     value of gridYRows at this index will be a non-negative number
          if ( (*validIndex)((int)hY, (int)wX) >= 0)
          {
            pDouble nIdx = std::make_pair(hY, wX);
            // a valid neighbor
            (*tFov)[it].bNeighbors.push_back(nIdx);
          }
        }//finished checking for neighbor validity
      }//finished populating all 8-connected neighbors

      // Load all valid neighbors
      for (int i = 0; i < (*tFov)[it].bNeighbors.size(); ++i)
      {
        // Get the neighbor
        pDouble nIdx = (*tFov)[it].bNeighbors[i];
        // Check if it was corrected
        if ((*tFov)[nIdx].corrected == false )
        {
          // Load the neighbor image
          std::string nImage = gObject->gMap.get()->GetImageDirectory() + \
                              "/" + \
                              (*tFov)[nIdx].aoiName + \
                              gObject->file_extension;
          gObject->ImageIO.get()->ReadImage(nImage, &(*tFov)[nIdx].Img, true);
          if (gObject->Normalize)
          {
            if (gObject->normType == 0)
            {
              // And white correct it
              spin::NormalizeRGBImageWithCalImage(&(*tFov)[nIdx].Img,
                                                  &gObject->mCalImage,
                                                  &gObject->mMax,
                                                  &(*tFov)[nIdx].Img);
            }
            else
            {
              // And white correct it
              spin::NormalizeRGBImageWithCalImage(&(*tFov)[nIdx].Img,
                                                  &gObject->mCalImage,
                                                  &(*tFov)[nIdx].Img);
            }
			
          }
          // Finally, mark this neighbor image also as corrected
          (*tFov)[nIdx].corrected = true;
        }
      }
      
	  // Finally, we populate blending pairs. These are the pairs that 
	  // need to be blended before sending an AOI for tiling.
	  (*tFov)[it].blending_pairs.clear();
	  // 1.) Parent w.r.t its neighbors
	  for (int i = 0; i < (*tFov)[it].bNeighbors.size(); ++i)
	  {
	    (*tFov)[it].blending_pairs.push_back(std::make_pair(it,(*tFov)[it].bNeighbors[i]));
	  }
	  
	  // 2.) All neighbors amongst themselves
	  for (int i = 0; i < (*tFov)[it].bNeighbors.size(); ++i)
	  for (int j = i+1; j < (*tFov)[it].bNeighbors.size(); ++j)
	  {
	    (*tFov)[it].blending_pairs.push_back(std::make_pair((*tFov)[it].bNeighbors[i],(*tFov)[it].bNeighbors[j]));
	  }
	  
      return 1;
    }//finished loading this aoi

    /*! \brief ProcessData
     * Chains up the process pipelines that need to be sequenced for the data
     * point
    */
    template <class P>
    void ImageReadPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // Process the data point using the pipeline described in ProcessPipeline.
      ProcessPipeline(&dataPoint, obj);
      // Typecast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);

      if (gObject->inlineBlending)
      {
        // Forward the data point to the next Pipeline or Queue
        mBlending.get()->ProcessPipeline(&dataPoint, obj);
      }
      else
      {
        // Send a signal of completion for this FOV to the calling thread
        spin::FovCompleted p;
        p.Module = 0;//indicates image acquisition
        p.gYRow  = dataPoint.gYRow;
        p.gXCol  = dataPoint.gXCol;
        sig_FOV(p);
      }
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     * This function ensures that processing is finished
     */
    template <class P>
    void ImageReadPipeline<P>::CompleteProcessing(void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      if (gObject->inlineBlending)
      {
        // Complete the sub pipelines as well
        mBlending.get()->CompleteProcessing();
      }

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();

        // Update the background status of all the AoIs.
        CallbackStruct p;
        p.Module = "Image Acquisition Pipeline";
        sig_Finished(p);
      }
      return;
    }//end of function

    /*! \brief BeginProcessingQueue [SLOT]
     * This function Sends a request to the queue in the broker for data
     */
    template <class P>
    void ImageReadPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      PanoramaObject* gmap = gObject->gMap.get();
      int grid = (gmap->GetGridOfInterest());
      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetAOI(grid)->size();

      while( (n_images < max_images) )
      {
        //  //std::cout<<"p1 "<<n_images<<" "<<max_images<<std::endl;
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_ImageRead.joinable())
        {
          t_ImageRead.join();
          ++n_images;
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
      }
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     *  This function receives a signal from the MPC with a new data point. As
     *  we allocate data for images, we need to keep it outside the image
     *  read thread so that we can clear the memory within this thread only.
     */
    template <class P>
    void ImageReadPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      // Typecast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      if (gObject == NULL) return;

      // Get the grid of interest
      int grid = (gObject->gMap.get()->GetGridOfInterest());
      double neighbors[] = { -1,-1,-1,0,-1,1,0,-1,0,1,1,-1,1,0,1,1 };
      const spin::RMMatrix_Double *validIndex = gObject->gMap.get()->GetYRows(grid);
      // Get the grid of interest
      FOV* tFov = const_cast<FOV*>(gObject->gMap.get()->GetAOI(grid));
      if (!validIndex->data()) return;

      // 1.) Build the index
      pDouble it(dataPoint.gYRow, dataPoint.gXCol);

      // 2.) Build the name
      std::string inpImage = gObject->gMap.get()->GetImageDirectory() + \
                            "/" + \
                            (*tFov)[it].aoiName + \
                            gObject->file_extension;

      // 3.) Check if this image was loaded previously
      if (!((*tFov)[it].loaded) )
      {
        // The image will be loaded
        (*tFov)[it].Img = RGBImageType::New();
        (*tFov)[it].Img->SetRegions(gObject->mImageRegion);
        (*tFov)[it].Img->Allocate();
        // Mark this image as having undergone correction or loaded
        (*tFov)[it].loaded = true;
      }

      // 4.) At this time we will populate neighbors of this image
      (*tFov)[it].bNeighbors.clear();
      int gHeight = (gObject->gMap.get()->GetOriginalGridHeight());
      int gWidth  = (gObject->gMap.get()->GetOriginalGridWidth());

      // Populate the list of neighbors
      for (int i = 0; i < 8; ++i)
      {
        double hY = it.first + neighbors[2 * i];
        double wX = it.second+ neighbors[2 * i + 1];

        // A neighbor is considered valid if:
        //a.) It satisfies the grid bounds, and
        if (hY >= 0 && wX >= 0 && hY < gHeight && wX < gWidth)
        {
          // b.) The index is part of the grid. This means that the
          //     value of gridYRows at this index will be a non-negative number
          if ( (*validIndex)((int)hY, (int)wX) >= 0)
          {
            pDouble nIdx = std::make_pair(hY, wX);
            // a valid neighbor
            (*tFov)[it].bNeighbors.push_back(nIdx);
          }
        }//finished checking for neighbor validity
      }//finished populating all 8-connected neighbors

      // Load all valid neighbors
      for (int i = 0; i < (*tFov)[it].bNeighbors.size(); ++i)
      {
        // Get the neighbor
        pDouble nIdx = (*tFov)[it].bNeighbors[i];
        // Check if memory was allocated for this image previously. If not,
        // we allocate such memory.
        if ((*tFov)[nIdx].loaded == false )
        {
          (*tFov)[nIdx].Img = RGBImageType::New();
          (*tFov)[nIdx].Img->SetRegions(gObject->mImageRegion);
          (*tFov)[nIdx].Img->Allocate();
          // Finally, mark this neighbor image also as corrected
          (*tFov)[nIdx].loaded = true;
        }
      }

      // Process the data point in a separate thread
      t_ImageRead  = boost::thread( &ImageReadPipeline<P>::ProcessData,\
                                    this,dataPoint,obj);

    }//end of function

    /*! \brief RequestQueue [SLOT]
    * This function receives a signal from the MPC that a new datapoint is
    * available for consumption
    */
    template <class P>
    void ImageReadPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&ImageReadPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts the pipelines and cleanly exits
     */
    template <class P>
    void ImageReadPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
      AbortOperations();
    }//end of function

    /*! \brief AbortOperations
     *	This function forwards the abort signal to other consumers and
     *	deallocates any memory objects that are part of this class alone.
     *	This however does not destroy the configuration for which this
     *  pipeline was primed with. Thereby enabling reacquisition instantly.
     */
    template <class P>
    void ImageReadPipeline<P>::AbortOperations()
    {
      if (mBlending.get() != NULL)
      {
        // Send a signal to abort the operations
        mBlending.get()->AbortPipeline();
      }
    }//end of function

    // explicit instantiation of template class
    template class ImageReadPipeline<ProducerDataType>;
  }//end of namespace vista
}//end of namespace spin
