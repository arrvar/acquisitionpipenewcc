#ifndef SPINIMAGEREAD_H_
#define SPINIMAGEREAD_H_

// Pipeline includes
#include <memory>
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  // Forward declaration of Producer Consumer class
  template <class P>
  class MPC;

  namespace vista
  {
    // Forward declaration of a database class
    class DatabaseIO;
    // Other forward declarations
    template <class P>
    class ImageReadInterface;
    template <class P>
    class ImageReadPipeline;

    /*! \brief
     * This class acts as the interface with the image acquisition pipeline
     */
    template <class P>
    class SpinImageRead
    {
      public:
        /*! \brief Destructor*/
        ~SpinImageRead();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* producerDataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        int CompleteProcessing();

        /*! \brief AbortPipeline*/
        int AbortPipeline();

        // callback signal
        spin::CallbackSignal sig_Finished;
      private:
        bool pause;
        // Create objects for the producer level, broker and consumer level
        std::shared_ptr< ImageReadInterface<P> > mImageReadInterface;
        std::shared_ptr< ImageReadPipeline<P> > mImageReadPipeline;
        std::shared_ptr< spin::MPC<P> > mMPC;
        std::shared_ptr< DatabaseIO > db;

        /*! \brief ConnectSignals*/
        int ConnectSignals(void* obj);
        /*! \brief Relay Signal*/
        void RelaySignal(spin::CallbackStruct p);
        /*! \brief SetState*/
        void SetState(bool p);
      };
  }//end of namespace vista
}//end of namespace spin
#endif


