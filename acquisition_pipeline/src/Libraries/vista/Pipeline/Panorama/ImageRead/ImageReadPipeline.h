#ifndef IMAGEREADPIPELINE_H_
#define IMAGEREADPIPELINE_H_

// pipeline includes
#include "AbstractClasses/AbstractConsumer.h"
#include "AbstractClasses/AbstractDataStructures.h"
#include "AbstractClasses/AbstractCallback.h"
// Boost includes
#include <boost/thread.hpp>

namespace spin
{
  namespace vista
  {
    // A set of forward declarations
    template <class P>
    class SpinBlending;

    /*! \brief ImageReadPipeline
     *  This class acts as the interface with the blending pipeline
     */
    template <class P>
    class ImageReadPipeline : public AbstractConsumer<P>
    {
      public:
      // Signals
        spin::VoidSignal sig_GetData;

        // Default destructor
        ~ImageReadPipeline();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj, bool blending=false);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* dataPoint, void* obj);

        /*! \brief ProcessData*/
        void ProcessData(P dataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        void CompleteProcessing(void* obj);

        /*! \brief AbortPipeline*/
        void AbortPipeline();

        /*! \brief Reimplemented functions */
        void ConsumeFromQueue(P dataPoint, void* obj);
        void RequestQueue(void* obj);

        // Signals for metrics and logging
        spin::CallbackSignal sig_Logs;
        spin::CallbackSignal sig_Metrics;
        spin::CallbackSignal sig_Finished;
        spin::FOVSignal      sig_FOV;
      private :
        // Thread to process the data
        boost::thread t_Process;
        boost::thread t_ImageRead;
        boost::mutex  abort_mutex;
        bool          abort;
        // An instance of the Blending module
        std::shared_ptr< SpinBlending<P> > mBlending;
        bool blending;
        /*! \brief Special functions*/
        void AbortOperations();
        void BeginProcessingQueue(void* obj);
        void RelayLogs(spin::CallbackStruct p);
        void RelayMetrics(spin::CallbackStruct p);
        void RelaySignal(spin::CallbackStruct p);
        void ClearMemory(void* obj, spin::FovCompleted p);
    };//end of class
  }//end of namespace vista
}//end of namespace spin

#endif
