#ifndef TILINGINTERFACE_H_
#define TILINGINTERFACE_H_
// Pipeline includes
#include "AbstractClasses/AbstractProducer.h"
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  namespace vista
  {
    /*! \brief TilingInterface
     *  This class is the interface between the image acquisition source and the
     *  translation estimation pipeline framework
     */
    template <class P>
    class TilingInterface : public AbstractProducer
    {
      public:
        // Signals
        // Add data point to queue in broker
        boost::signals2::signal<void(P)> sig_AddQ;
        // Finished acquisition
        spin::VoidSignal sig_FinAcq;
        // Abort Operations
        spin::VoidSignal sig_Abort;

        // Reimplemented function
        void AddToQueue(void* dataPoint);
    };//end of class
  }//end of namespace vista
}//end of namespace spin
#endif

