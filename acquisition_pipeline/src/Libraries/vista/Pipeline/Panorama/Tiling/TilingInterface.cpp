#include "Libraries/vista/Pipeline/Panorama/Tiling/TilingInterface.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  namespace vista
  {
    /*! \brief AddToQueue
     *  This function adds an object to the Pre-processing queue
     */
    template<class P>
    void TilingInterface<P>::AddToQueue(void* dataPoint)
    {
      // typecasts
      ProducerDataType* dp = static_cast<ProducerDataType*>(dataPoint);
      //emit the signal
      sig_AddQ(*dp);
    }//end of function

    // explicit instantiation of template class
    template class TilingInterface<ProducerDataType>;
  }//end of stitching namespace
}//end of spin namespace
