#include "Libraries/vista/Pipeline/Panorama/Tiling/TilingPipeline.h"
#include "Libraries/vista/Pipeline/Panorama/Initialize/spinVistaPanoramaObject.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "AbstractClasses/AbstractDataStructures.h"
#include "Utilities/Stitching/Panorama/Tiling.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include <boost/filesystem.hpp>
#include <chrono>
#include <fstream>
#include <iterator>

namespace spin
{
  namespace vista
  {
    namespace
    {
      typedef spin::PanoramaObject PanoramaObject;
      typedef std::map<pDouble, spin::BlendingTilingObject> FOV;
    }

    /*! \brief Destructor
     * Finishes processing, deletes all objects and cleanly exits
     */
    template <class P>
    TilingPipeline<P>::~TilingPipeline()
    {
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void TilingPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelayMetrics
     *  A slot to relay an incoming signal
     */
    template <class P>
    void TilingPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief RelaySignalFinished
     *  A slot to relay an incoming signal
     */
    template <class P>
    void TilingPipeline<P>::RelaySignalFinished(spin::CallbackStruct p)
    {
      // Push the finished signal
      sig_Finished(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline.
     */
    template <class P>
    int TilingPipeline<P>::InstantiatePipeline(void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      if (gObject == NULL) return -1;

      PanoramaObject* gmap = gObject->gMap.get();

      // Instantiate a Tiling object
      tp = std::make_shared<spin::Tiling>();

      // Connect callback relay signals
      tp.get()->sig_Logs.connect(boost::bind(&TilingPipeline<P>::RelayLogs,this,_1));

      // And initialize the necessary data structures
      tp.get()->GenerateDataStructure(gmap, gObject->log_tiling);
      abort = false;

      return 1;
    }//end of function

    /*! \brief ProcessPipeline
     *  This function is the driver for cropping out a region (i.e. a Tile)
     *  from a group of images.
     */
    template <class P>
    int TilingPipeline<P>::ProcessPipeline(P* dataPoint, void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      if (gObject == NULL) return -1;

      // Get the grid map
      PanoramaObject* gmap = gObject->gMap.get();
      // The grid being analyzed
      int grid = (gmap->GetGridOfInterest());
      // And the list of AOI's in the grid
      FOV* dFov = const_cast<FOV*>(gmap->GetAOI(grid));
      // Get the index of the image within this grid
      pDouble it(dataPoint->gYRow, dataPoint->gXCol);

      // Check if the image has been loaded
      if((*dFov)[it].Img)
      {
        // Get the name of the tile directory
        std::string tileDir = (gmap->GetTileDirectory());
        auto start = std::chrono::high_resolution_clock::now();

        std::vector<spin::RGBImageType::Pointer> tileVector((*dFov)[it].tile_list.size());
        std::vector<std::string> tileNames((*dFov)[it].tile_list.size());
        // Allocate memory for images
        for (int i = 0; i < (*dFov)[it].tile_list.size(); ++i)
        {
          // Allocate memory for tile vector in this thread
          tileVector[i] = spin::RGBImageType::New();
          tileVector[i]->Graft(gObject->mDefaultTile);
        }

        for (int i = 0; i < (*dFov)[it].tile_list.size(); ++i)
        {
          {
            pDouble tileIdx = (*dFov)[it].tile_list[i];
            bool isWritten = tp.get()->CheckIfTileWrittenToDisk(&tileIdx);

            // If this tile was not previously written to disk)
            if(!isWritten)
            {
              // And populate this tile from all images that contribute to it
              //Note: http://stackoverflow.com/questions/3786360/confusing-template-error
              tp->template PopulateTile<spin::RGBImageType>(&tileIdx, &tileVector[i],gmap);
              // Finally, after the tile has been populated, we give it a name
              // Apparently. openseadragon needs it in an x_y coordinate system rather than the y_x
              // system that we use.
              std::string tilePath =  tileDir+"/"+\
                                      std::to_string((int)tileIdx.second) + "_" + \
                                      std::to_string((int)tileIdx.first) + gObject->TileExt;
              tileNames[i] = tilePath;
              // In addition, we save the tile name for further use
              (*tp->GetTileMap())[tileIdx].name = tilePath;
            }// finished check if this tile was not written
          }
        }// finished all tiles associated with this image

        for (int i = 0; i < (*dFov)[it].tile_list.size(); ++i)
        {
          gObject->ImageIO.get()->WriteImage(tileNames[i], &tileVector[i]);
          // Release memory for tile vector in this thread
          tileVector[i] = NULL;
        }

        // Release memory
        (*dFov)[it].base_tiles.clear();
        tileVector.clear();
        tileNames.clear();

        // Send a signal of completion for this FOV to the calling thread
        spin::FovCompleted p;
        p.Module = 2;//indicates tiling
        p.gYRow  = dataPoint->gYRow;
        p.gXCol  = dataPoint->gXCol;
        sig_FOV(p);
      }//finished generating tiles associated with this image

      return 1;
    }//end of function

    /*\brief ProcessData
     * Processes the data point that is input by running it through the required
     * pipelines
     */
    template <class P>
    void TilingPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // Tiling is effected
      ProcessPipeline(&dataPoint, obj);
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     * This function receives a signal from the MPC with a new data point
     */
    template <class P>
    void TilingPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      // Process the data point in a separate thread
      t_TilingProcess = boost::thread(&TilingPipeline<P>::ProcessData, this, dataPoint, obj);
    }///end of function

    /*! \brief RequestQueue [SLOT]
     * This function receives a signal from the MPC that a new datapoint is
     * available for consumption
     */
    template <class P>
    void TilingPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&TilingPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     *  This function ensures that processing is finished
     */
    template <class P>
    void TilingPipeline<P>::CompleteProcessing(void* obj)
    {
      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();
      }
      
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      if (gObject == NULL) return -1;

      if (!gObject->calibration)
      {
        // TODO: Here, we need to send a signal that the process is completed
        // This signal should be caught by an external function in order to
        // save the metrics in a database (or anywhere else)
        CallbackStruct p;
        p.Module="TilingPipeline";
        p.Descr ="CompleteProcessing";
        p.Finished=1;
        sig_Finished(p);
      }
      else
      {
        // Get the grid map
        PanoramaObject* gmap = gObject->gMap.get();
        std::vector<float> temp = gmap->GetMetrics();
        // save the blending metrics data to a calibration file
        std::ofstream output_file("./blending_metrics.txt");
        std::ostream_iterator<float> output_iterator(output_file, "\n");
        std::copy(temp.begin(), temp.end(), output_iterator);
      }
    }//end of function

    /*! \brief BeginProcessingQueue
     * Monitors the queue and keeps the consumer alive unitl all data has been
     * processed or the pipeline is aborted
     */
    template <class P>
    void TilingPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);

      PanoramaObject* gmap = gObject->gMap.get();
      int grid = (gmap->GetGridOfInterest());
      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetAOI(grid)->size();

      CallbackStruct p;
      p.Module="TilingPipeline";
      p.Descr ="Generating base tiles from aoi";
      while(n_images < max_images)
      {
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_TilingProcess.joinable())
        {
          t_TilingProcess.join();
          ++n_images;
          sig_Finished(p);
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
      }//end processing all images

      //emit a signal indicating that it is complete
      p.Finished=1;
      sig_Finished(p);
    }//end of function

    /*! \brief AbortPipeline
     * Aborts the pipelines and cleanly exits
     */
    template <class P>
    void TilingPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
    }//end of function

    // explicit instantiation of template class
    template class TilingPipeline<ProducerDataType>;
  }//end of namespace vista
}//end of namespace spin
