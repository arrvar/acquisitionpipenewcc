#ifndef SPINTILING_H_
#define SPINTILING_H_
#include <memory>
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  // Forward declaration of Producer Consumer class
  template <class P>
  class MPC;

  namespace vista
  {
    // Forward declaration of a database class
    class DatabaseIO;

    template <class P>
    class TilingInterface;
    template <class P>
    class TilingPipeline;

    /*! \brief
     * This class acts as the interface with the stitching pipeline and in essence
     * is the interface for the library of the Spin Stitching Pipeline
     */
    template <class P>
    class SpinTiling
    {
      public:
        /*! \brief Destructor*/
        ~SpinTiling();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* producerDataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        int CompleteProcessing();

        /*! \brief AbortPipeline*/
        int AbortPipeline();
        // callback signal
        spin::CallbackSignal    sig_Finished;
        spin::FOVSignal         sig_FOV;
      private:
        bool pause;
        // Create objects for the producer level, broker and consumer level
        std::shared_ptr< TilingInterface<P> > mTilingInterface;
        std::shared_ptr< TilingPipeline<P> > mTilingPipeline;
        std::shared_ptr< spin::MPC<P> > mMPC;
        std::shared_ptr< DatabaseIO > db;

        /*! \brief ConnectSignals*/
        int ConnectSignals(void* obj);
        /*! \brief Pause the producer */
        void PauseProducer(bool state);
        /*! \brief Relay Signal*/
        void RelaySignal(spin::CallbackStruct p);
        /*! \brief RelayFOVCompletion*/
        void RelayFOVCompletion(spin::FovCompleted p);
      };
  }//end of namespace vista
}//end of namespace spin
#endif
