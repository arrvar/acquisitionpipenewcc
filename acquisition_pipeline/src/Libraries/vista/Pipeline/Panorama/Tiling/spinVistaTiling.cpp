#include "Libraries/vista/Pipeline/Panorama/Tiling/spinVistaTiling.h"
#include "Libraries/vista/Pipeline/Panorama/Tiling/TilingInterface.h"
#include "Libraries/vista/Pipeline/Panorama/Tiling/TilingPipeline.h"
#include "Libraries/vista/Pipeline/Panorama/Initialize/spinVistaPanoramaObject.h"
#include "Libraries/vista/Pipeline/Panorama/DatabaseIO/DatabaseIO.h"
#include "Framework/MPC/MPC.h"
#include "Framework/Logger/LoggerInterface.h"

namespace spin
{
  namespace vista
  {
    /*! \brief ConnectSignals
     *  Connects signals and slots for all the members of this class here
     */
    template<class P>
    int SpinTiling<P>::ConnectSignals(void* obj)
    {
      // Connect the corresponding signals and slots
      mMPC.get()->sig_DataAvailable.connect(boost::bind(&TilingPipeline<P>::RequestQueue,mTilingPipeline,obj));
      mMPC.get()->sig_DataPoint.connect(boost::bind(&TilingPipeline<P>::ConsumeFromQueue,mTilingPipeline, _1, _2));
      mMPC.get()->sig_FinAcq.connect(boost::bind(&TilingPipeline<P>::CompleteProcessing,mTilingPipeline,obj));
      mMPC.get()->sig_QueueBurdened.connect(boost::bind(&SpinTiling<P>::PauseProducer,this, _1));
      mMPC.get()->sig_Abort.connect(boost::bind(&TilingPipeline<P>::AbortPipeline,mTilingPipeline));


      mTilingInterface.get()->sig_AddQ.connect(boost::bind(&MPC<P>::AddToQueue, mMPC, _1));
      mTilingInterface.get()->sig_FinAcq.connect(boost::bind(&MPC<P>::Finished, mMPC));
      mTilingInterface.get()->sig_Abort.connect(boost::bind(&MPC<P>::Abort, mMPC));
      mTilingPipeline.get()->sig_GetData.connect(boost::bind(&MPC<P>::PopFromQueue, mMPC, obj));
      mTilingPipeline.get()->sig_Finished.connect(boost::bind(&SpinTiling<P>::RelaySignal, this, _1));
      mTilingPipeline.get()->sig_FOV.connect(boost::bind(&SpinTiling<P>::RelayFOVCompletion, this, _1));

      // Typecast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      if (gObject == NULL) return -1;

      // Connect the logging signal
      if (gObject->loggingInitialized)
      {
        mMPC.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
        mTilingPipeline.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
        mTilingPipeline.get()->sig_Finished.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
      }

      return 1;
    }//end of function

    /*! \brief Destructor
     *  Clears all objects, joins all threads and exits
     */
    template<class P>
    SpinTiling<P>::~SpinTiling()
    {
      CompleteProcessing();
    }//end of function

    /*! \brief RelaySignal
     *  A slot to relay an incoming signal
     */
    template <class P>
    void SpinTiling<P>::RelaySignal(spin::CallbackStruct p)
    {
      // Push the log
      sig_Finished(p);
    }//end of function

    /*! \brief RelayFOVCompletion
     *  A slot to relay an FOV completion signal
     */
    template <class P>
    void SpinTiling<P>::RelayFOVCompletion(spin::FovCompleted p)
    {
      sig_FOV(p);
    }//end of function

    /*! \brief PauseProducer
     *  enables a boolean to either pause or resume the producer
     */
    template <class P>
    void SpinTiling<P>::PauseProducer(bool state)
    {
      pause = state;
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline. Creates the objects and makes all the
     *  connections if necessary. It does not configure any data structure or
     *  set any constants as they would be dependent on the grid that is being
     *  @obj      : The spinVistaObject that has been initialized prior
     *              to calling this function
     */
    template<class P>
    int SpinTiling<P>::InstantiatePipeline(void* obj)
    {
      // The producer can be either a camera or a File I/O interface
      mTilingInterface = std::make_shared<TilingInterface<P> >();

      // The consumer can be any process that can receive this data structure
      mTilingPipeline = std::make_shared<TilingPipeline<P> >();

      // Both the producer and consumer communicate with each other via a Broker
      mMPC = std::make_shared<spin::MPC<P> >();

      // Typecast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);

      if (gObject == NULL) return -1;

      // Connect the logging signal
      if (gObject->loggingInitialized)
      {
        mMPC.get()->EnableLogging();
      }

      // Instantiate the pipelines (lightweight in nature)
      mTilingPipeline.get()->InstantiatePipeline(obj);

      // Connect all the signals and slots
      ConnectSignals(obj);

      return 1;
    }//end of function

    /*! \brief CompleteProcessing
     *  Completes the pipeline till the end and cleanly exits
     */
    template<class P>
    int SpinTiling<P>::CompleteProcessing()
    {
      // Send a signal to indicate end of acquisition
      mTilingInterface.get()->sig_FinAcq();

      return 0;
    }//end of function

    /*! \brief ProcessPipeline
     *  Processes the data that is input from the source. Internally it pushes
     *  the data into an appropriate data structure and then to a queue for
     *  any consumer to pick it up.
     */
    template<class P>
    int SpinTiling<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // Push this data point into a queue, but check if this is in a paused state
      while(pause)
      {
        boost::this_thread::sleep(boost::posix_time::milliseconds(5));
      }

      // Push this data point into a queue
      mTilingInterface.get()->AddToQueue(producerDataPoint);

      return 0;
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts all operations and cleanly exits
     */
    template<class P>
    int SpinTiling<P>::AbortPipeline()
    {
      // Send a signal to indicate end of acquisition
      mTilingInterface.get()->sig_Abort();

      return 0;
    }//end of function

    // explicit instantiation of template class
    template class SpinTiling<ProducerDataType>;
  }//end of stitching namespace
}//end of spin namespace
