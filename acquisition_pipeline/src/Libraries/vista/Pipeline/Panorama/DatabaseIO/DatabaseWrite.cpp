#if 0
#include <iostream>

#ifdef USE_SQLITEDB
#include <sqlite3.h>
#endif

#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDriver>
#include <QElapsedTimer>

#include "Libraries/vista/Pipeline/DatabaseIO/DatabaseIO.h"
#include "Libraries/vista/Pipeline/Initialize/GridMapGeneration.h"

#define DEBUG_TIMER 0

using namespace spin;
using namespace vista;


bool DatabaseIO::clearMstPath(int gridId)
{
  bool retVal = true;

  QString deleteQueryStr;
  deleteQueryStr.sprintf("DELETE FROM grid_metrics WHERE grid_id = %d", gridId);

  QSqlQuery deleteQuery(m_db);
  if (!deleteQuery.exec(deleteQueryStr))
  {
    //std::cout << "DatabaseIO::storeMstPath delete query error: " << deleteQuery.lastError().text().toStdString() << std::endl;
    retVal = false;
  }

  return retVal;
}

bool DatabaseIO::clearBlendingMetrics(int gridId)
{
  bool retVal = true;

  QString deleteQueryStr;
  deleteQueryStr.sprintf("DELETE FROM grid_stitch_metrics");

  QSqlQuery deleteQuery(m_db);
  if (!deleteQuery.exec(deleteQueryStr))
  {
    //std::cout << "DatabaseIO::storeMstPath delete query error: " << deleteQuery.lastError().text().toStdString() << std::endl;
    retVal = false;
  }

  return retVal;
}

bool DatabaseIO::storeMstPath(int gridId, int rowIdx, int colIdx, QString mstPath)
{
  bool retVal = true;

  QString insertQueryStr;
  insertQueryStr.sprintf("INSERT INTO grid_metrics VALUES("
                          "%d, %d, %d, '%s')",
                          gridId, rowIdx, colIdx,
                          mstPath.toStdString().c_str());

  QSqlQuery insertQuery(m_db);
  if (!insertQuery.exec(insertQueryStr))
  {
    //std::cout << "DatabaseIO::storeMstPath insert query error: " << insertQuery.lastError().text().toStdString() << std::endl;
    retVal = false;
  }

  return retVal;
}

bool DatabaseIO::storeBlendingMetrics(int gridId, int rowIdx, int colIdx, QString blendingMetrics)
{
  bool retVal = true;

  QString insertQueryStr;
  insertQueryStr.sprintf("INSERT INTO grid_stitch_metrics VALUES("
                          "%d, %d, %d, '%s')",
                          gridId, rowIdx, colIdx,
                          blendingMetrics.toStdString().c_str());

  QSqlQuery insertQuery(m_db);
  if (!insertQuery.exec(insertQueryStr))
  {
    //std::cout << "DatabaseIO::storeBlendingMetrics insert query error: " << insertQuery.lastError().text().toStdString() << std::endl;
    retVal = false;
  }

  return retVal;
}

bool DatabaseIO::updateBackgroundStatus(int gridId, QString aoiName, int backgroundStatus)
{
  bool retVal = true;

  QString updateQueryStr;
  updateQueryStr.sprintf("UPDATE aoi SET background = %d "
                         "WHERE grid_id = %d AND aoi_name = %s",
                         backgroundStatus, gridId, aoiName.toStdString().c_str());

  QSqlQuery updateQuery(m_db);
  if (!updateQuery.exec(updateQueryStr))
  {
    //std::cout << "DatabaseIO::updateBackgroundStatus update query error: " << updateQuery.lastError().text().toStdString() << std::endl;
    retVal = false;
  }

  return retVal;
}

/*! \brief WriteToDatabase
  *  This function is a slot that is used to write to a database.
  */
void DatabaseIO::WriteToDatabase( std::string& database, int gridId,
                                  void* obj, spin::CallbackStruct p)
{
  typedef std::pair<double, double> pDouble;

  // typecast the object
  GridMapGeneration* gmap = static_cast<GridMapGeneration*>(obj);

  // Get the Grid Mapping
  std::map<pDouble, FOVStruct>* dFov = gmap->GetGridMapping();

  m_db.setDatabaseName(database.c_str());
  m_db.open();

  // Check if the databse is open and has a valid driver
  if (m_db.isOpen() && m_db.isValid())
  {
    if (p.Module == "TranslationEstimation")
    {
      if (p.Descr == "MST_Paths")
      {
        // Clear the table of MST path values first.
        clearMstPath(gridId);
#if DEBUG_TIMER
        QElapsedTimer timer;
        timer.start();
#endif
        // Start the database transaction.
        m_db.transaction();

        std::vector<pDouble>* dIdx = gmap->GetGridIndices();
        for (auto it : *dIdx)
        {
          std::vector<pDouble> path;
          path = (*dFov)[it].sPath;

          QString mstPath = "";
          for (int i = 0; i < path.size(); i++)
          {
            mstPath = mstPath + QString::number((int)path[i].first) + ","
              + QString::number((int)path[i].second) + ";";
          }

          int rowIdx = (int)it.first;
          int colIdx = (int)it.second;
          storeMstPath(gridId, rowIdx, colIdx, mstPath);
        }
        // Commit the database transaction.
        m_db.commit();
#if DEBUG_TIMER
        //std::cout << "MST write time: " << (qreal)timer.elapsed() / 1000.0 << std::endl;
#endif
      }
    }
    else if (p.Module == "Blending")
    {
      if (p.Descr == "BlendingMetrics")
      {
        // Clear the table of blending metrics first.
        clearBlendingMetrics(gridId);
#if DEBUG_TIMER
        QElapsedTimer timer;
        timer.start();
#endif
        // Start the database transaction.
        m_db.transaction();

        std::vector<pDouble>* dIdx = gmap->GetGridIndices();
        for (auto it : *dIdx)
        {
          QString blendingSad = "";
          QString blendingNormCorr = "";
          std::map<pDouble, StitchingMetrics>::iterator p = (*dFov)[it].tMetrics.begin();
          while (p != (*dFov)[it].tMetrics.end())
          {
            blendingSad = blendingSad + QString::number(p->second.blending_sad) + ",";
            blendingNormCorr = blendingNormCorr + QString::number(p->second.blending_normCorr) + ",";
            p++;
          }

          int rowIdx = (int)it.first;
          int colIdx = (int)it.second;
          QString blendingMetrics = blendingSad + blendingNormCorr;
          storeBlendingMetrics(gridId, rowIdx, colIdx, blendingMetrics);
        }
        // Commit the database transaction.
        m_db.commit();
#if DEBUG_TIMER
        //std::cout << "Blending metrics write time: " << (qreal)timer.elapsed() / 1000.0 << std::endl;
#endif
      }
    }
    else if (p.Module == "Preprocessing")
    {
      if (p.Descr == "Bg_Status_Update")
      {
#if DEBUG_TIMER
        QElapsedTimer timer;
        timer.start();
#endif
        // Start the database transaction.
        m_db.transaction();

        std::vector<pDouble>* dIdx = gmap->GetGridIndices();
        for (auto it : *dIdx)
        {
          QString aoiName((*dFov)[it].aoiName.c_str());
          int backgroundStatus = (*dFov)[it].Background;

          updateBackgroundStatus(gridId, aoiName, backgroundStatus);
        }
        // Commit the transaction.
        m_db.commit();
#if DEBUG_TIMER
        //std::cout << "Background status update time: " << (qreal)timer.elapsed() / 1000.0 << std::endl;
#endif
      }
    }
  }
  else
  {
    //std::cout << "Database connection failed : " << m_db.lastError().text().toStdString() << std::endl;
  }

  m_db.close();
}
#endif
