#ifndef BLENDINGPIPELINE_H_
#define BLENDINGPIPELINE_H_
#include <memory>
#include "AbstractClasses/AbstractConsumer.h"
#include "AbstractClasses/AbstractCallback.h"
#include <boost/thread.hpp>

namespace spin
{
  // Forward declarations
  class BlendingUtils;

  namespace vista
  {
    template <class P>
    class SpinTiling;

    /*! \brief BlendingPipeline
    *  This class is a pipeline for Blending images with its neighbours
    */
    template <class P>
    class BlendingPipeline : public AbstractConsumer<P>
    {
      public:
        /*! \brief Destructor*/
        ~BlendingPipeline();

        /*! \brief Instantiate pipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline (Overloaded)*/
        int ProcessPipeline(P* dataPoint, void* obj);

        /*! \brief AbortPipeline*/
        void AbortPipeline();

        /*! \brief Process the data from the producer*/
        void ProcessData(P dataPoint, void* obj);

        // Signals
        spin::VoidSignal sig_GetData;
        spin::CallbackSignal sig_Logs;
        spin::CallbackSignal sig_Metrics;
        spin::CallbackSignal sig_Finished;
        spin::FOVSignal      sig_FOV;

        //Derived functions
        void ConsumeFromQueue(P dataPoint, void* obj) ;
        void RequestQueue(void* obj);
        void CompleteProcessing(void* obj);

      private :
        boost::thread t_Process;
        boost::thread t_BlendingProcess;
        boost::mutex  abort_mutex;
        bool          abort;
        std::shared_ptr< SpinTiling<P> > mSpinTiling;
        std::shared_ptr< BlendingUtils > bu;

        // Special functions
        void AbortOperations();
        void BeginProcessingQueue(void* obj);
        void RelayLogs(spin::CallbackStruct p);
        void RelayMetrics(spin::CallbackStruct p);
        void RelaySignalFinished(spin::CallbackStruct p);
        void RelayFOVCompletion(spin::FovCompleted p);
    };//end of class
  }//end of stitching namespace
}//end of spin namespace
#endif
