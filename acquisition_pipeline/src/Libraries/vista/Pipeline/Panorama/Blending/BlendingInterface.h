#ifndef BLENDINGINTERFACE_H_
#define BLENDINGINTERFACE_H_
// Pipeline includes
#include "AbstractClasses/AbstractProducer.h"
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  namespace vista
  {
    /*! \brief StitchingInterface
    *  This class is the interface between the image acquisition source and the
    *  framework that stitches the images. This acts as a producer class which
    *  sends a data structure to a "broker" which stores all the information in a
    *  queue to be used by consumers
    */
    template <class P>
    class BlendingInterface : public AbstractProducer
    {
      public:
        // Signals
        // Add data point to queue in broker
        boost::signals2::signal<void(P)> sig_AddQ;
        // Finished acquisition
        spin::VoidSignal sig_FinAcq;
        // Abort Operations
        spin::VoidSignal sig_Abort;

        // Reimplemented function
        void AddToQueue(void* dataPoint);
    };//end of class
  }//end of stitching namespace
}//end of spin namespace

#endif
