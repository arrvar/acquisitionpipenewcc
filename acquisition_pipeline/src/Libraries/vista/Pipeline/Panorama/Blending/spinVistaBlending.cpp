#include "Libraries/vista/Pipeline/Panorama/Blending/spinVistaBlending.h"
#include "Libraries/vista/Pipeline/Panorama/Initialize/spinVistaPanoramaObject.h"
#include "Libraries/vista/Pipeline/Panorama/DatabaseIO/DatabaseIO.h"
#include "Libraries/vista/Pipeline/Panorama/Blending/BlendingInterface.h"
#include "Libraries/vista/Pipeline/Panorama/Blending/BlendingPipeline.h"
#include "Framework/MPC/MPC.h"
#include "Framework/Logger/LoggerInterface.h"

namespace spin
{
  namespace vista
  {
    /*! \brief PauseProducer
     *  enables a boolean to either pause or resume the producer
     */
    template <class P>
    void SpinBlending<P>::PauseProducer(bool state)
    {
      pause = state;
    }//end of function

    /*! \brief ConnectSignals
     *  Connects signals and slots for all the members of this class here
     */
    template<class P>
    int SpinBlending<P>::ConnectSignals(void* obj)
    {
      // Connect the corresponding signals and slots
      mMPC.get()->sig_DataAvailable.connect(boost::bind(&BlendingPipeline<P>::RequestQueue,mBlendingPipeline,obj));
      mMPC.get()->sig_DataPoint.connect(boost::bind(&BlendingPipeline<P>::ConsumeFromQueue,mBlendingPipeline, _1, _2));
      mMPC.get()->sig_FinAcq.connect(boost::bind(&BlendingPipeline<P>::CompleteProcessing,mBlendingPipeline,obj));
      mMPC.get()->sig_QueueBurdened.connect(boost::bind(&SpinBlending<P>::PauseProducer,this, _1));
      mMPC.get()->sig_Abort.connect(boost::bind(&BlendingPipeline<P>::AbortPipeline,mBlendingPipeline));


      mBlendingInterface.get()->sig_AddQ.connect(boost::bind(&MPC<P>::AddToQueue, mMPC, _1));
      mBlendingInterface.get()->sig_FinAcq.connect(boost::bind(&MPC<P>::Finished, mMPC));
      mBlendingInterface.get()->sig_Abort.connect(boost::bind(&MPC<P>::Abort, mMPC));
      mBlendingPipeline.get()->sig_GetData.connect(boost::bind(&MPC<P>::PopFromQueue, mMPC, obj));
      mBlendingPipeline.get()->sig_Finished.connect(boost::bind(&SpinBlending<P>::RelaySignal, this, _1));
      mBlendingPipeline.get()->sig_FOV.connect(boost::bind(&SpinBlending<P>::RelayFOVCompletion, this, _1));

      // Typecast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      if (gObject == NULL) return -1;

      // Connect the logging signal
      if (gObject->loggingInitialized)
      {
        mMPC.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
        mBlendingPipeline.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
        mBlendingPipeline.get()->sig_Finished.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
      }

      //Connect the metric signal
      if (gObject->metrics_blending)
      {
        // connect the metric signal
        //mBlendingPipeline.get()->sig_Metrics.connect(boost::bind(&spin::vista::DatabaseIO::WriteToDatabase, gObject->db.get(), \
        //                                                         gObject->dbFile, gObject->gridId, gObject->gMap.get(), _1));
      }
      return 1;
    }//end of function

    /*! \brief Destructor
     *  Clears all objects, joins all threads and exits
     */
    template<class P>
    SpinBlending<P>::~SpinBlending()
    {
      CompleteProcessing();
    }//end of function

    /*! \brief RelaySignal
     *  A slot to relay an incoming signal
     */
    template <class P>
    void SpinBlending<P>::RelaySignal(spin::CallbackStruct p)
    {
      // Push the log
      sig_Finished(p);
    }//end of function

    /*! \brief RelayFOVCompletion
     *  A slot to relay an FOV completion signal
     */
    template <class P>
    void SpinBlending<P>::RelayFOVCompletion(spin::FovCompleted p)
    {
      sig_FOV(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline. Creates the objects and makes all the
     *  connections if necessary. It does not configure any data structure or
     *  set any constants as they would be dependent on the grid that is being
     *  @obj      : The spinVistaObject that has been initialized prior
     *              to calling this function
     */
    template<class P>
    int SpinBlending<P>::InstantiatePipeline(void* obj)
    {
      // The producer can be either a camera or a File I/O interface
      mBlendingInterface = std::make_shared<BlendingInterface<P> >();

      // The consumer can be any process that can receive this data structure
      mBlendingPipeline = std::make_shared<BlendingPipeline<P> >();

      // Both the producer and consumer communicate with each other via a Broker
      mMPC = std::make_shared<MPC<P> >();

      // Typecast the object
      spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
      spinVistaPanoramaObject* gObject = dynamic_cast<spinVistaPanoramaObject*>(aObj);
      if (gObject == NULL) return -1;

      // Connect the logging signal
      if (gObject->loggingInitialized)
      {
        mMPC.get()->EnableLogging();
      }

      // Instantiate the pipelines (lightweight in nature)
      mBlendingPipeline.get()->InstantiatePipeline(obj);

      // Connect all the signals and slots
      ConnectSignals(obj);

      return 0;
    }//end of function

    /*! \brief CompleteProcessing
     *  Completes the pipeline till the end and cleanly exits
     */
    template<class P>
    int SpinBlending<P>::CompleteProcessing()
    {
      // Send a signal to indicate end of acquisition
      mBlendingInterface.get()->sig_FinAcq();
      return 0;
    }//end of function

    /*! \brief ProcessPipeline
     *  Processes the data that is input from the source. Internally it pushes
     *  the data into an appropriate data structure and then to a queue for
     *  any consumer to pick it up.
     */
    template<class P>
    int SpinBlending<P>::ProcessPipeline(P* pDataPoint, void* obj)
    {
      // Push this data point into a queue, but check if this is in a paused state
      while(pause)
      {
        boost::this_thread::sleep(boost::posix_time::milliseconds(5));
      }

      mBlendingInterface.get()->AddToQueue(pDataPoint);
      return 1;
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts all operations and cleanly exits
     */
    template<class P>
    int SpinBlending<P>::AbortPipeline()
    {
      // Send a signal to indicate end of acquisition
      mBlendingInterface.get()->sig_Abort();
      return 1;
    }//end of function

    // explicit instantiation of template class
    template class SpinBlending<ProducerDataType>;
  }//end of stitching namespace
}//end of spin namespace
