#ifndef SPINBLENDING_H_
#define SPINBLENDING_H_

// Pipeline includes
#include <memory>
#ifndef VISTA_PLUGIN_FRAMEWORK
#include "Framework/Exports/spinAPI.h"
#endif
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  // Forward declaration of Producer Consumer class
  template <class P>
  class MPC;

  namespace vista
  {
    // Forward declaration of a database class
    class DatabaseIO;

    // Other forward declarations
    template <class P>
    class BlendingInterface;
    template <class P>
    class BlendingPipeline;

    /*! \brief
     * This class acts as the interface with the blending pipeline
     */
    template <class P>
#ifndef VISTA_PLUGIN_FRAMEWORK
    class SPIN_API SpinBlending
#else
    class SpinBlending
#endif
    {
      public:
        /*! \brief Destructor*/
        ~SpinBlending();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* producerDataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        int CompleteProcessing();

        /*! \brief AbortPipeline*/
        int AbortPipeline();

        // callback signal
        spin::CallbackSignal    sig_Finished;
        spin::FOVSignal         sig_FOV;
      private:
	      bool pause;
        // Create objects for the producer level, broker and consumer level
        std::shared_ptr< BlendingInterface<P> > mBlendingInterface;
        std::shared_ptr< BlendingPipeline<P> > mBlendingPipeline;
        std::shared_ptr< spin::MPC<P> > mMPC;
        std::shared_ptr< DatabaseIO > db;

        /*! \brief ConnectSignals*/
        int ConnectSignals(void* obj);
	      /*! \brief Pause the producer */
	      void PauseProducer(bool state);
        /*! \brief Relay Signal*/
        void RelaySignal(spin::CallbackStruct p);
        /*! \brief RelayFOVCompletion*/
        void RelayFOVCompletion(spin::FovCompleted p);
      };
  }//end of namespace vista
}//end of namespace spin
#endif
