#include "AbstractClasses/AbstractImageTypes.h"
#include "AbstractClasses/AbstractDataStructures.h"
#include "Libraries/vista/Pipeline/Panorama/Initialize/spinVistaPanoramaObject.h"
#include "Libraries/vista/Pipeline/Panorama/Blending/BlendingPipeline.h"
#include "Libraries/vista/Pipeline/Panorama/Tiling/spinVistaTiling.h"
#include "Utilities/Stitching/Panorama/BlendingUtils.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include <chrono>

namespace spin
{
  namespace vista
  {
    namespace
    {
      typedef spin::PanoramaObject PanoramaObject;
    }

    /*! \brief Destructor
     * Finishes processing, deletes all objects and cleanly exits
     */
    template <class P>
    BlendingPipeline<P>::~BlendingPipeline()
    {
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void BlendingPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelaySignalFinished
     *  A slot to relay an incoming signal
     */
    template <class P>
    void BlendingPipeline<P>::RelaySignalFinished(spin::CallbackStruct p)
    {
      // Push the finished signal
      sig_Finished(p);
    }//end of function

    /*! \brief RelayMetrics
     *  A slot to relay an incoming signal
     */
    template <class P>
    void BlendingPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief RelayFOVCompletion
     *  A slot to relay an FOV completion signal
     */
    template <class P>
    void BlendingPipeline<P>::RelayFOVCompletion(spin::FovCompleted p)
    {
      sig_FOV(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  This function instantiates the pipeline required for
     *  Blending operations post translation estimation
     *  @obj  : An object holding information about the FOV's
     *          that need to be blended
     */
    template <class P>
    int BlendingPipeline<P>::InstantiatePipeline(void* obj)
    {
      typedef typename spin::BlendingUtils BlendingUtils;
      abort = false;
      spinVistaPanoramaObject* gObject = static_cast<spinVistaPanoramaObject*>(obj);
      // Instantiate the blending utils pipeline
      bu = std::make_shared<BlendingUtils>();
      // The blending pipeline will have only a single plugin. If more than one
      // plugin is enabled, the first available plugin is selected and the
      // rest are ignored.
      int k = bu->Initialize(&gObject->mPlugins[0][0]);
      if (k !=1) return -1;

      // Also check if we have to do inline tiling
      if (gObject->inlineTiling)
      {
        // At this time, we need to initialize the tiling object and
        // data structures associated with this module
        mSpinTiling = std::make_shared< SpinTiling<P> >();
        mSpinTiling.get()->InstantiatePipeline(obj);
        // connect a signal
        mSpinTiling.get()->sig_Finished.connect(boost::bind(&BlendingPipeline<P>::RelaySignalFinished, this, _1));
        mSpinTiling.get()->sig_FOV.connect(boost::bind(&BlendingPipeline<P>::RelayFOVCompletion, this, _1));
      }

      return 1;
    }//end of function

    /*! \brief ProcessPipeline
     *  Processes the image here
     */
    template <class P>
    int BlendingPipeline<P>::ProcessPipeline(P* dataPoint, void* obj)
    {
      // downcast the object
      spinVistaPanoramaObject* gObject = static_cast<spinVistaPanoramaObject*>(obj);
      // and extract the Panorama object
      PanoramaObject* gmap = gObject->gMap.get();
      // Build the index being processed
      pDouble it = pDouble(dataPoint->gYRow, dataPoint->gXCol);

      // The main call to the BlendingUtils class
      int val = bu.get()->BlendingPipeline(&it, gmap, \
                                           gObject->blendingThresh,\
                                           gObject->noiseSuppression,\
                                           gObject->disable_blending);

      return val;
    }//end of function

    /*! \brief ProcessData
     *  Processes the data point that is input by running it through the
     *  required pipelines
     */
    template <class P>
    void BlendingPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // Run the pipeline
      ProcessPipeline(&dataPoint, obj);

      // Typecast the object
      spinVistaPanoramaObject* gObject = static_cast<spinVistaPanoramaObject*>(obj);
      // Check if tiling is required
      if (gObject->inlineTiling)
      {
        // Pass this image to the tiling pipeline
        mSpinTiling.get()->ProcessPipeline(&dataPoint, obj);
      }
      else
      {
        // Send a signal of completion for this FOV to the calling thread
        spin::FovCompleted p;
        p.Module = 1;//indicates blending
        p.gYRow  = dataPoint.gYRow;
        p.gXCol  = dataPoint.gXCol;
        sig_FOV(p);
      }
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     *  This function receives a signal from the MPC with a new data point
     */
    template <class P>
    void BlendingPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      //Process the data point in a separate thread
      t_BlendingProcess = boost::thread(&BlendingPipeline<P>::ProcessData, this,dataPoint, obj);
    }

    /*! \brief RequestQueue [SLOT]
     * This function receives a signal from the MPC that a new datapoint is
     * available for consumption
     */
    template <class P>
    void BlendingPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&BlendingPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     * This function ensures that processing is finished
     */
    template <class P>
    void BlendingPipeline<P>::CompleteProcessing(void* obj)
    {
      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();

        /*// Write the stitching metrics to the database.
        CallbackStruct cMetric;
        cMetric.Module = "Blending";
        cMetric.Descr = "BlendingMetrics";
        cMetric.batch = true;
        sig_Metrics(cMetric);*/
      }

      spinVistaPanoramaObject* gObject = static_cast<spinVistaPanoramaObject*>(obj);
      if (gObject->inlineTiling)
      {
        mSpinTiling.get()->CompleteProcessing();
      }
    }//end of function

    /*! \brief BeginProcessingQueue
    *  Monitors the queue and keeps the consumer alive unitl all data has been
    *  processed or the pipeline is aborted
    */
    template <class P>
    void BlendingPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      spinVistaPanoramaObject* gObject = static_cast<spinVistaPanoramaObject*>(obj);
      PanoramaObject* gmap = gObject->gMap.get();
      int grid = (gmap->GetGridOfInterest());
      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetAOI(grid)->size();

      CallbackStruct p;
      p.Module = "BlendingPipeline";
      p.Descr ="Processing images for blending";

      while(n_images < max_images)
      {
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          p.Finished = 1;
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_BlendingProcess.joinable())
        {
          t_BlendingProcess.join();
          ++n_images;
          //p.Descr += " " + std::to_string(pTimeBlending);
          // Emit a signal to this effect
          sig_Finished(p);
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(5));
          continue;
        }
      }
      // Emit a signal to this effect
      p.Finished = 1;
      sig_Finished(p);
    }//end of function

    /*! \brief AbortPipeline
     * Aborts the pipelines and cleanly exits
     */
    template <class P>
    void BlendingPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
    }//end of function

    // explicit instantiation of template class
    template class BlendingPipeline<ProducerDataType>;
  }//end of namespace vista
}//end of namespace vista
