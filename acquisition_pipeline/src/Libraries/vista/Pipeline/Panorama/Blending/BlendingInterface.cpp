#include "Libraries/vista/Pipeline/Panorama/Blending/BlendingInterface.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  namespace vista
  {
    /*! \brief AddToQueue
     *  This function adds a new data object to the queue for processing.
     */
    template<class P>
    void BlendingInterface<P>::AddToQueue(void* dataPoint)
    {
      // typecast the data
      ProducerDataType* dp = static_cast<ProducerDataType*>(dataPoint);
      //emit the signal
      sig_AddQ((*dp));
    }//end of function

    // explicit instantiation of template class
    template class BlendingInterface<ProducerDataType>;
  }//end of stitching namepsace
}//end of spinnamespace
