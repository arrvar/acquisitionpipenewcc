#include "Libraries/vista/Interface/vistaInterface.h"

#ifdef PYTHON_INTERFACE
/*! \brief This is the main python wrapper for the online stitching pipeline*/
BOOST_PYTHON_MODULE(py_spinVista)
{
  boost::python::numeric::array::set_module_and_type("numpy", "ndarray");

  boost::python::class_<vistaInterface, boost::noncopyable>("vistaInterface")
    .def("Initialize", &vistaInterface::Initialize)
    .def("ComputeGridLocations", &vistaInterface::ComputeGridLocations)
    .def("FinishGridLocations", &vistaInterface::CompleteGridLocationsProcessing)
    .def("Tiling", &vistaInterface::InlineTiling)
    .def("FinishTiling", &vistaInterface::CompleteInlineTiling)
    .def("AbortGridLocationComputation", &vistaInterface::AbortComputeGridLocations)
    .def("AbortInlineBlending", &vistaInterface::AbortInlineTiling);
};//end of function
#endif