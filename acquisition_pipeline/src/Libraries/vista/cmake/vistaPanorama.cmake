#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building
#vistaPanorama
#------------------------------------------------------------------------------
SET(${PROJ}_PANORAMA_LIB_DIRS
    Common/AbstractClasses
    Common/Framework/ImageIO
    Common/Framework/Exports
    Common/Framework/Logger
    Common/Framework/MPC
    Common/Framework/GPU
    Common/Parameters
    Libraries/vista/Pipeline/Panorama
    Utilities/Stitching/Panorama
    Utilities/Stitching/Pyramid
    Utilities/ImageProcessing/ITKImageUtils
    Utilities/ImageProcessing/ColorSpace
    Utilities/Metrics
    Utilities/ImageProcessing/Sharpness
)

SET(${PROJ}_PANORAMA_Utilities_SRCS)
FOREACH(CONTENT ${${PROJ}_PANORAMA_LIB_DIRS})
  FILE(GLOB_RECURSE U_SRCS
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.cpp
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.hh
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.h
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.txx)
  SET(${PROJ}_PANORAMA_Utilities_SRCS ${${PROJ}_PANORAMA_Utilities_SRCS} ${U_SRCS})
ENDFOREACH()

#------------------------------------------------------------------------------
# Build a shared library so that acquisition can use it
#------------------------------------------------------------------------------
SET(vacqItf ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/vistaPanoramaInterface.cpp
            ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/vistaPanoramaInterface.h)
ADD_LIBRARY(spin${PROJ}Panorama SHARED ${vacqItf} ${${PROJ}_PANORAMA_Utilities_SRCS})
TARGET_COMPILE_OPTIONS( spin${PROJ}Panorama
                        PRIVATE "-DVISTA_PLUGIN_FRAMEWORK -DSPIN_EXPORTS")
TARGET_LINK_LIBRARIES(spin${PROJ}Panorama ${${PROJ}_EXT_LIBS})
install (TARGETS spin${PROJ}Panorama  DESTINATION	 ${CMAKE_INSTALL_PREFIX}) 
IF(NOT MSVC)
  SET_TARGET_PROPERTIES(spin${PROJ}Panorama PROPERTIES DEBUG_POSTFIX "_d" CXX_VISIBILITY_PRESET hidden)
ELSE()
  SET_TARGET_PROPERTIES(spin${PROJ}Panorama PROPERTIES  COMPILE_FLAGS "-DSPIN_EXPORTS"
                                                DEBUG_POSTFIX "_d")
ENDIF()

#------------------------------------------------------------------------------
#Check if a C++ Unit Test is requested
#------------------------------------------------------------------------------
SET(ut_spin${PROJ}Panorama OFF CACHE BOOL "Build C++ Unit test for ${PROJ}Panorama Module")
IF(ut_spin${PROJ}Panorama)
  ADD_EXECUTABLE(ut_spin${PROJ}Panorama ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/spinVistaPanorama.cpp)
  SET_TARGET_PROPERTIES(ut_spin${PROJ}Panorama PROPERTIES  COMPILE_FLAGS "-DVISTA_PLUGIN_FRAMEWORK")
  TARGET_LINK_LIBRARIES(ut_spin${PROJ}Panorama spin${PROJ}Panorama ${${PROJ}_EXT_LIBS})
  install (TARGETS ut_spin${PROJ}Panorama DESTINATION	 ${CMAKE_INSTALL_PREFIX}) 
ENDIF()

