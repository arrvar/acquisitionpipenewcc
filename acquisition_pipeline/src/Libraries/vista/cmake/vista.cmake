#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building vista
#------------------------------------------------------------------------------
SET(PROJ vista)
SET(${PROJ}_EXT_LIBS)

#------------------------------------------------------------------------------
#Include the third party libraries needed
#------------------------------------------------------------------------------
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/TPL.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/vistaAcquisition.cmake)
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/vistaPanorama.cmake)
