#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building
#vistaAcquisition
#------------------------------------------------------------------------------
SET(${PROJ}_ACQUISITION_LIB_DIRS
    Common/AbstractClasses
    Common/Framework/ImageIO
    Common/Framework/Exports
    Common/Framework/Logger
    Common/Framework/MPC
    Libraries/vista/Pipeline/Acquisition
    Utilities/Stitching/Acquisition
    Utilities/ImageProcessing/ImageUtils
    Utilities/ImageProcessing/FFTUtils
    Utilities/ImageProcessing/HistogramUtils
    Utilities/Misc
    Utilities/MatrixUtils
    Utilities/AbstractProcessing
    Utilities/Geometry
)

SET(${PROJ}_ACQUISITION_Utilities_SRCS)
FOREACH(CONTENT ${${PROJ}_ACQUISITION_LIB_DIRS})
  FILE(GLOB_RECURSE U_SRCS
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.cpp
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.hh
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.h
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.txx)
  SET(${PROJ}_ACQUISITION_Utilities_SRCS ${${PROJ}_ACQUISITION_Utilities_SRCS} ${U_SRCS})
ENDFOREACH()

#------------------------------------------------------------------------------
# Build a shared library so that acquisition can use it
#------------------------------------------------------------------------------
SET(vacqItf ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/vistaAcquisitionInterface.cpp
            ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/vistaAcquisitionInterface.h)
ADD_LIBRARY(spin${PROJ}Acquisition SHARED ${vacqItf} ${${PROJ}_ACQUISITION_Utilities_SRCS})
TARGET_COMPILE_OPTIONS(spin${PROJ}Acquisition PRIVATE "-DVISTA_PLUGIN_FRAMEWORK -DSPIN_EXPORTS")
TARGET_LINK_LIBRARIES(spin${PROJ}Acquisition ${${PROJ}_EXT_LIBS})
install (TARGETS spin${PROJ}Acquisition  DESTINATION	 ${CMAKE_INSTALL_PREFIX}) 
IF(NOT MSVC)
  SET_TARGET_PROPERTIES(spin${PROJ}Acquisition PROPERTIES DEBUG_POSTFIX "_d" CXX_VISIBILITY_PRESET hidden)
ELSE()
  SET_TARGET_PROPERTIES(spin${PROJ}Acquisition PROPERTIES  COMPILE_FLAGS "-DSPIN_EXPORTS"
                                                DEBUG_POSTFIX "_d")
ENDIF()

#------------------------------------------------------------------------------
#Check if a C++ Unit Test is requested
#------------------------------------------------------------------------------
SET(ut_spin${PROJ}Acquisition OFF CACHE BOOL "Build C++ Unit test for ${PROJ}Acquisition Module")
IF(ut_spin${PROJ}Acquisition)
  ADD_EXECUTABLE(ut_spin${PROJ}Acquisition ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/spinVistaAcquisition.cpp)
  SET_TARGET_PROPERTIES(ut_spin${PROJ}Acquisition PROPERTIES  COMPILE_FLAGS "-DVISTA_PLUGIN_FRAMEWORK")
  TARGET_LINK_LIBRARIES(ut_spin${PROJ}Acquisition ${${PROJ}_EXT_LIBS})
  install (TARGETS ut_spin${PROJ}Acquisition   DESTINATION	 ${CMAKE_INSTALL_PREFIX}) 
ENDIF()

