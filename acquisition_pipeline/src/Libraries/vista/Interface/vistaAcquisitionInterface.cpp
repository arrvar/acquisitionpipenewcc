#include "Libraries/vista/Interface/vistaAcquisitionInterface.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/spinVistaAcquisitionInitialize.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/spinVistaAcquisitionObject.h"
#include "Libraries/vista/Pipeline/Acquisition/ImageAcquisition/spinVistaImageAcquisition.h"
#include "Libraries/vista/Pipeline/Acquisition/Preprocessing/spinVistaPreprocessing.h"
#include "Libraries/vista/Pipeline/Acquisition/ImageWriter/spinVistaImageWriter.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include "opencv2/opencv.hpp"
// Boost includes
#include <boost/thread.hpp>

#ifdef VISTA_PLUGIN_FRAMEWORK
namespace spin
{
#endif
  namespace
  {
    typedef spin::RGBPixelType RGBPixelType;
    typedef spin::RGBImageType RGBImageType;
    typedef spin::vista::pDouble pDouble;
    typedef spin::vista::FOVStruct FOVStruct;
  }

  /*! \brief Default constructor */
  vistaAcquisitionInterface::vistaAcquisitionInterface()
  {
  }//end of function

  /*! \brief ClearAfterWriting
   *  A slot to clear image memory after writing
   */
  void vistaAcquisitionInterface::ClearAfterWriting(spin::FovCompleted p)
  {
    // Dynamically cast the object
    spin::vista::spinVistaAcquisitionObject* gObject1 = dynamic_cast<spin::vista::spinVistaAcquisitionObject*>(gObject.get());
    // Get the AOI information
    spin::vista::GridMapGeneration* gmap = gObject1->gMap.get();
    spin::vista::GridInfo* gInfo         = &(gmap->GetGridHeader());
    std::map<spin::vista::pDouble, spin::vista::FOVStruct>* dFov = gmap->GetGridMapping();
    spin::vista::pDouble it(p.gYRow, p.gXCol);
    // Mark the image for clearing if it has been written and proeprocessing has finished
    if (((*dFov)[it].f_preprocess==true) && ((*dFov)[it].f_write==true))
    (*dFov)[it].Img = NULL;
    (*dFov)[it].Img_g = NULL;
  }//end of function

  /*! \brief ClearAfterPreprocess
   *  A slot to clear ITK allocated memory
   */
  void vistaAcquisitionInterface::ClearAfterPreprocess(spin::FovCompleted p)
  {
    // Dynamically cast the object
    spin::vista::spinVistaAcquisitionObject* gObject1 = dynamic_cast<spin::vista::spinVistaAcquisitionObject*>(gObject.get());
    // Get the AOI information
    spin::vista::GridMapGeneration* gmap = gObject1->gMap.get();
    spin::vista::GridInfo* gInfo         = &(gmap->GetGridHeader());
    std::map<spin::vista::pDouble, spin::vista::FOVStruct>* dFov = gmap->GetGridMapping();
    spin::vista::pDouble it(p.gYRow, p.gXCol);
    // Mark the image for clearing if it has been written and proeprocessing has finished
    (*dFov)[it].Img = NULL;
    (*dFov)[it].Img_g = NULL;
  }//end of function

  /*! \brief Initialize
   *  This function is used to initialize the pipeline using a
   *  parameter file passed as an input
   *  @pFile        : The parameter file
   */
  int vistaAcquisitionInterface::Initialize(const char* pFile, bool fromDisk)
  {
    // Initialize gObject
    gObject = std::make_shared<spin::vista::spinVistaAcquisitionObject>();

    // Initialize a parser
    std::shared_ptr<spin::vista::spinVistaAcquisitionInitialize> parser = \
    std::make_shared<spin::vista::spinVistaAcquisitionInitialize>();

    // Check the global object can be populated
    int p = parser.get()->Initialize(gObject.get(), pFile);

    // If not, return the error code to the calling function
    if (p !=1) return p;

    if (fromDisk)
    {
      //Initialize an acquisition object
      acquisition = std::make_shared<pacquisition>();
      acquisition.get()->InstantiatePipeline(gObject.get());
    }
    else
    {
      // Initialize a pre-processing object
      preprocessing = std::make_shared<ppreprocessing>();
      preprocessing.get()->InstantiatePipeline(gObject.get());
      preprocessing.get()->sig_FOV.connect(boost::bind(&vistaAcquisitionInterface::ClearAfterPreprocess, this, _1));
    }

    return 1;
  }//end of function

  /*! \brief ProcessGridLocations
   *  This is the driver function to compute the displacements
   *  between neighboring images as part of the spin vista pipeline
   *  when images are being read from the disk
   */
  void vistaAcquisitionInterface::ProcessGridLocations( std::string InputImage,\
                                                        float gx, float gy,\
                                                        int background)
  {
    // Instantiate a ProducerDataType
    producer pData;
    // Populate the coordinates
    pData.gYRow = gy;
    pData.gXCol = gx;
    // Dynamically cast the object
    spin::vista::spinVistaAcquisitionObject* gObject1 = dynamic_cast<spin::vista::spinVistaAcquisitionObject*>(gObject.get());
    // Add the magnification (may be needed for associated plugins)
    pData.Magnification = gObject1->magnification;
    // And process it
    acquisition.get()->ProcessPipeline(&pData, gObject.get());
  }//end of function

  /*! \brief ProcessGridLocations
   *  This is the driver function to compute the displacements
   *  between neighboring images as part of the spin vista pipeline
   *  when images are being passed from a camera to this function.
   *  The following points need to be noted.
   *  a.) The input image is of type cv::Mat,
   *  b.) The coordinates of the grid should be present in gx and gy
   *  c.) Memory for the image should be allocated in this function
   */
  void vistaAcquisitionInterface::ProcessGridLocations( void* _InputImage,\
                                                        float gx, float gy,\
                                                        int background)
  {
    // Instantiate a ProducerDataType
    producer pData;
    // Populate the coordinates
    pData.gYRow = gy;
    pData.gXCol = gx;

    // Dynamically cast the object
    spin::vista::spinVistaAcquisitionObject* gObject1 = dynamic_cast<spin::vista::spinVistaAcquisitionObject*>(gObject.get());
    // Get the AOI information
    spin::vista::GridMapGeneration* gmap = gObject1->gMap.get();
    spin::vista::GridInfo* gInfo         = &(gmap->GetGridHeader());
    std::map<spin::vista::pDouble, spin::vista::FOVStruct>* dFov = gmap->GetGridMapping();
    spin::vista::pDouble it(pData.gYRow, pData.gXCol);
    (*dFov)[it].Img = RGBImageType::New();
    (*dFov)[it].Img->SetRegions(gObject1->mImageRegion);
    (*dFov)[it].Img->Allocate(); // Note: memory allocated here (will be deallocated in a slot)
    // Static-cast the Image
    cv::Mat* inpImg = static_cast<cv::Mat*>(_InputImage);
    // Copy the opencv data to the ITK image data
    memcpy((*dFov)[it].Img->GetBufferPointer(), inpImg->data, 3*inpImg->rows*inpImg->cols);
    // Add the magnification (may be needed for associated plugins)
    pData.Magnification = gObject1->magnification;
    // Send it to the pre-processing pipeline
    preprocessing.get()->ProcessPipeline(&pData, gObject.get());
    pData.Clear();
  }//end of function

  /*! \brief ProcessGridLocations_b
   *  This is the driver function to compute the displacements
   *  between neighboring images as part of the spin vista pipeline
   *  when images are being passed from a camera to this function. The difference
   *  between this and the other function is that we work with an unsigned char
   *  image rather than a RGB image
   *  The following points need to be noted.
   *  a.) The input image is of type unsigned char,
   *  b.) The coordinates of the grid should be present in gx and gy
   *  c.) Memory for the image should be allocated in this function
   */
  void vistaAcquisitionInterface::ProcessGridLocations_b( void* _InputImage,\
                                                          float gx, float gy,\
                                                          int background)
  {
    // Instantiate a ProducerDataType
    producer pData;
    // Populate the coordinates
    pData.gYRow = gy;
    pData.gXCol = gx;

    // Dynamically cast the object
    spin::vista::spinVistaAcquisitionObject* gObject1 = dynamic_cast<spin::vista::spinVistaAcquisitionObject*>(gObject.get());
    // Get the AOI information
    spin::vista::GridMapGeneration* gmap = gObject1->gMap.get();
    std::map<spin::vista::pDouble, spin::vista::FOVStruct>* dFov = gmap->GetGridMapping();
    spin::vista::pDouble it(pData.gYRow, pData.gXCol);
    (*dFov)[it].Img_g = UCharImageType2D::New();
    (*dFov)[it].Img_g->SetRegions(gObject1->mImageRegion);
    (*dFov)[it].Img_g->Allocate(); // Note: memory allocated here (will be deallocated in a slot)
    // Static-cast the Image
    cv::Mat* inpImg = static_cast<cv::Mat*>(_InputImage);
    // Copy the opencv data to the ITK image data
    memcpy((*dFov)[it].Img->GetBufferPointer(), inpImg->data, 3*inpImg->rows*inpImg->cols);
    // Add the magnification (may be needed for associated plugins)
    pData.Magnification = gObject1->magnification;
    // Send it to the pre-processing pipeline
    preprocessing.get()->ProcessPipeline(&pData, gObject.get());
    pData.Clear();
  }//end of function

  /*! \brief CompleteGridLocationsProcessing
   *  Finish the displacement estimation process in a separate thread
   */
  void vistaAcquisitionInterface::CompleteProcessGridLocations(bool fromDisk)
  {
    // Create a vector of processes that can be run in parallel
    std::vector<std::shared_ptr<boost::thread> > tArray;
    if (fromDisk)
    {
      std::shared_ptr<boost::thread> pT1(new boost::thread(&pacquisition::CompleteProcessing,\
                                                            acquisition.get()));
      tArray.push_back(pT1);

    }
    else
    {
      std::shared_ptr<boost::thread> pT1(new boost::thread(&ppreprocessing::CompleteProcessing,\
                                                            preprocessing.get()));
      tArray.push_back(pT1);
    }
    // Finish all processes
    for (int jThread = 0; jThread < tArray.size(); ++jThread)
    {
      tArray[jThread]->join();
    }

  }//end of function

  /*! \brief AbortProcessGridLocations
   *  This function sends a signal to abort the displacement
   *  estimation pipeline
   */
  void vistaAcquisitionInterface::AbortProcessGridLocations(bool fromDisk)
  {
    if (fromDisk)
    {
      acquisition->AbortPipeline();
    }
    else
    {
      preprocessing->AbortPipeline();
    }
  }//end of function

  // Default destructor
  vistaAcquisitionInterface::~vistaAcquisitionInterface()
  {
  }//end of function
#ifdef VISTA_PLUGIN_FRAMEWORK
}

// Needed for calling the module as a plugin
typedef spin::vistaAcquisitionInterface   vistaAcquisitionInterface;
extern "C" SPIN_API void* Vista_Module()
{
  void* q = NULL;
  // return the object type
  vistaAcquisitionInterface* obj = new vistaAcquisitionInterface();
  q = (void*)(obj);
  return q;
}//end of function
#endif
