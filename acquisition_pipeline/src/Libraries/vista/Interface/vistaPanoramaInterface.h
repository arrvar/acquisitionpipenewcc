#ifndef VISTAPANORAMAINTERFACE_H
#define VISTAPANORAMAINTERFACE_H
#ifndef VISTA_PLUGIN_FRAMEWORK
#include "AbstractClasses/AbstractCallback.h"
#else
#include "AbstractClasses/AbstractVistaModule.h"
#endif
#include "Framework/Exports/spinAPI.h"
#include <memory>

// Forward declaration of classes
namespace spin
{
#ifndef VISTA_PLUGIN_FRAMEWORK
  struct AbstractProcessingObject;
#endif
  namespace vista
  {
    struct ProducerDataType;
    template <class P> class SpinImageRead;
  }//end of namespace vista
}//end of namespace spin

/*! \brief vistaPanoramaInterface
 *  This is an interface class that is used to call the
 *  backend pipeline associated with spin Vista.
 */
#ifndef VISTA_PLUGIN_FRAMEWORK
class SPIN_API vistaPanoramaInterface
#else
namespace spin
{
class SPIN_API vistaPanoramaInterface : public spin::AbstractVistaModule
#endif
{
  public:
#ifndef VISTA_PLUGIN_FRAMEWORK
    typedef spin::AbstractProcessingObject sVObject;
#endif
    // Relevant typedefs
    typedef spin::vista::ProducerDataType producer;
    typedef spin::vista::SpinImageRead<producer> imagereader;

    // Constructor
    vistaPanoramaInterface();

    // Destructor
    ~vistaPanoramaInterface();

    // Initialize the pipeline
    int RunPanorama(const char* pFile);

    // Abort panorama generation
    void AbortPanorama();

    // Dummy initializations
    int Initialize(const char* pFile, bool f=true){}
    void ProcessGridLocations(std::string InputImage, \
                              float gx, float gy, \
                              int background){}
    void ProcessGridLocations( void* _InputImage, \
                               float gx, float gy,\
                               int background){}

    // A thread to finish computing the displacements
    void CompleteProcessGridLocations(bool f=true){}

    // Abort displacement estimation
    void AbortProcessGridLocations(bool f=true){}
#ifndef VISTA_PLUGIN_FRAMEWORK
    // Get a const access to gObject
    const std::shared_ptr<sVObject> GetGObject(){return gObject;}
#endif
  private:
    // Process to generate inline mosaic
    int GeneratePanorama(int p1, int p2);
#ifndef VISTA_PLUGIN_FRAMEWORK
    // Instantiate a spin vista object
    std::shared_ptr<sVObject>   gObject;
#endif
    // Instantiate a spin image reader object
    std::shared_ptr<imagereader>  reader;
};// end of StitchingInterface class
#ifdef VISTA_PLUGIN_FRAMEWORK
}
#endif
#endif // vistaInterface
