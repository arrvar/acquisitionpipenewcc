#include "Libraries/vista/Interface/vistaPanoramaInterface.h"
#include "Libraries/vista/Pipeline/Panorama/Initialize/spinVistaPanoramaInitialize.h"
#include "Libraries/vista/Pipeline/Panorama/Initialize/spinVistaPanoramaObject.h"
#include "Libraries/vista/Pipeline/Panorama/ImageRead/spinVistaImageRead.h"
#include "Utilities/Stitching/Pyramid/PyramidGeneration.h"
#include <boost/thread.hpp>

#ifdef VISTA_PLUGIN_FRAMEWORK
namespace spin
{
#endif
  namespace
  {
    typedef spin::RGBPixelType RGBPixelType;
    typedef spin::RGBImageType RGBImageType;
    typedef spin::vista::pDouble pDouble;
    typedef spin::vista::spinVistaPanoramaInitialize spinInitialize;
    typedef std::map<pDouble, spin::BlendingTilingObject> FOV;
  }

  /*! \brief Default constructor */
  vistaPanoramaInterface::vistaPanoramaInterface()
  {
  }//end of function

  /*! \brief RunPanorama
   *  This function is used to run the panorama generation pipeline using a
   *  parameter file passed as an input
   *  @pFile        : The parameter file
   */
  int vistaPanoramaInterface::RunPanorama(const char* pFile)
  {
    // Initialize pObject
    gObject = std::make_shared<spin::vista::spinVistaPanoramaObject>();

    // Dynamically cast the abstract class
    spin::vista::spinVistaPanoramaObject* gObject_c = \
    dynamic_cast<spin::vista::spinVistaPanoramaObject*>(gObject.get());
    if (gObject_c == NULL) return -1;

    // Initialize a parser
    std::shared_ptr<spinInitialize> parser = std::make_shared<spinInitialize>();
    // Check the global object can be populated
    int p = parser.get()->Initialize(gObject.get(), pFile);

    // If not, return the error code to the calling function
    if (p !=1) return p;

    //gObject_c->gMap.get()->PrintInfo();
    //exit(1);
	
    // Get a counter for the number of grids to be processed
    int numGrids = 0;
    // A pointer to the AOI's
    const std::map<int, FOV>* GridList = gObject_c->gMap.get()->GetAOIMap();
    // Iterate through all grids
    std::map<int, FOV>::const_iterator it = GridList->begin();

    // Process each grid separately
    while (it != GridList->end())
    {
      // Launch the pipeline
      if (GeneratePanorama(it->first, numGrids) !=1) return -6;

      // Increment the grid
      ++it;
      // and the counter
      ++numGrids;
    }
    return 1;
  }//end of function

  /*! \brief GeneratePanorama
   *  In this pipeline we implement a panorama generation module for
   *  a given grid
   */
  int vistaPanoramaInterface::GeneratePanorama(int gridActual, int grid)
  {
    // Dynamically cast the abstract class
    spin::vista::spinVistaPanoramaObject* gObject_c = \
    dynamic_cast<spin::vista::spinVistaPanoramaObject*>(gObject.get());
    if (gObject_c == NULL) return -1;

    // 1.) First, we set the grid into the grid object
    gObject_c->gMap.get()->SetGridOfInterest(gridActual);

    // 2.) In addition set the tile directory where base tiles will be dumped
    std::string tDir = gObject_c->TileDir;
    // We can support upto 1000 grids !!!
    if (grid >=0 && grid < 10)
    {
      tDir = tDir + "/00" + std::to_string(grid);
    }
    else
    if (grid >=10 && grid < 100)
    {
      tDir = tDir + "/0" + std::to_string(grid);
    }
    else
    {
      tDir = tDir + "/" + std::to_string(grid);
    }

    // 3.) Check if the directory previously exists. If it does,
    // we delete the previous folder and create a new one
    boost::filesystem::path dir(tDir.c_str());
    if (!boost::filesystem::exists(dir))
    {
      if (!boost::filesystem::create_directory(dir))
      {
        return -3;
      }
    }
    else
    {
      // This directory exists, we delete the existing directory and then
      // create it again
      boost::filesystem::remove_all(dir);
      if (!boost::filesystem::create_directory(dir))
      {
        return -3;
      }
    }

    // 3.a.) We save the default tile in this folder
    std::string tilePath =  tDir+"/dummy"+ gObject_c->TileExt;
    gObject_c->ImageIO.get()->WriteImage(tilePath, &gObject_c->mDefaultTile);

    // 4.) Having created the directory, we set it in the object
    gObject_c->gMap.get()->SetTileDirectory(tDir);

    // 5.) Get the grid of interest
    FOV* tFov = const_cast<FOV*>(gObject_c->gMap.get()->GetAOI(gridActual));

    // 6.) Instantiate an image reading pipeline
    reader = std::make_shared<imagereader>();
    reader.get()->InstantiatePipeline(gObject.get());

    // 7.) Iterate through all grid indices
    FOV::iterator it = tFov->begin();
    const spin::RMMatrix_Double *validIndex = gObject_c->gMap.get()->GetYRows(gridActual);
    if (!validIndex->data())
    {
      return -3;
    }

    while (it != tFov->end())
    {
      // a.) Instantiate a ProducerDataType
      producer pData;
      pData.gYRow = it->first.first;
      pData.gXCol = it->first.second;

      // b.) Process the current image
      reader.get()->ProcessPipeline(&pData, gObject.get());
      boost::this_thread::sleep(boost::posix_time::milliseconds(static_cast<long>(gObject_c->delay)));
      // c.) Increment the iterator
      ++it;
    }//finished processing all aoi's in the grid

    // 8.) Complete the processing (blocking call)
    reader.get()->CompleteProcessing();

    // 9.)Generate pyramid if requested
    if (gObject_c->InlinePyramid)
    {
      spin::PyramidGeneration pyr;
      pyr.Generate(tDir,gObject_c->gMap.get()->GetTileHeight(),gObject_c->TileExt);
    }

    return 1;
  }//end of function

  /*! \brief AbortPanorama
   *  This function sends a signal to abort the displacement
   *  estimation pipeline
   */
  void vistaPanoramaInterface::AbortPanorama()
  {
    reader.get()->AbortPipeline();
  }//end of function

  // Default destructor
  vistaPanoramaInterface::~vistaPanoramaInterface()
  {
  }//end of function
#ifdef VISTA_PLUGIN_FRAMEWORK
}

// Needed for calling the module as a plugin
typedef spin::vistaPanoramaInterface   vistaPanoramaInterface;
extern "C" SPIN_API void* Vista_Module()
{
  void* q = NULL;
  // return the object type
  vistaPanoramaInterface* obj = new vistaPanoramaInterface();
  q = (void*)(obj);
  return q;
}//end of function
#endif
