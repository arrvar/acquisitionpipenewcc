#ifndef VISTAACQUISITIONINTERFACE_H
#define VISTAACQUISITIONINTERFACE_H
#ifndef VISTA_PLUGIN_FRAMEWORK
#include "AbstractClasses/AbstractCallback.h"
#else
#include "AbstractClasses/AbstractVistaModule.h"
#endif
#include "Framework/Exports/spinAPI.h"
#include <memory>

// Forward declaration of classes
namespace spin
{
#ifndef VISTA_PLUGIN_FRAMEWORK
  struct AbstractProcessingObject;
#endif
  namespace vista
  {
    //struct spinVistaAcquisitionObject;
    struct ProducerDataType;
    template <class P> class SpinImageAcquisition;
    template <class P> class SpinPreprocessing;
    template <class P> class SpinImageWriter;
  }//end of namespace vista
}//end of namespace spin

/*! \brief vistaInterface
 *  This is an interface class that is used to call the
 *  backend pipeline associated with spin Vista.
 */
#ifndef VISTA_PLUGIN_FRAMEWORK
class SPIN_API vistaAcquisitionInterface
#else
namespace spin
{
class SPIN_API vistaAcquisitionInterface : public spin::AbstractVistaModule
#endif
{
  public:
    // Relevant typedefs
#ifndef VISTA_PLUGIN_FRAMEWORK
    typedef spin::AbstractProcessingObject sVObject;
#endif
    typedef spin::vista::ProducerDataType producer;
    typedef spin::vista::SpinImageAcquisition<producer> pacquisition;
    typedef spin::vista::SpinPreprocessing<producer>    ppreprocessing;
    typedef spin::vista::SpinImageWriter<producer>      pimagewriter;

    // Constructor
    vistaAcquisitionInterface();

    // Destructor
    ~vistaAcquisitionInterface();

    // Initialize the pipeline
    int Initialize(const char* pFile, bool f=true);

    // Computing displacements for every aoi
    void ProcessGridLocations(std::string InputImage, float gx, float gy, \
                              int background);
    // Compute displacements for every aoi
    void ProcessGridLocations( void* _InputImage, float gx, float gy,\
                               int background);
    // Compute displacements for every aoi
    void ProcessGridLocations_b( void* _InputImage, float gx, float gy, int background);

    // A thread to finish computing the displacements
    void CompleteProcessGridLocations(bool f=true);

    // Abort displacement estimation
    void AbortProcessGridLocations(bool f=true);

    // Dummy functions
    int RunPanorama(const char* pFile){}

    // Abort panorama generation
    void AbortPanorama(){}
  private:
#ifndef VISTA_PLUGIN_FRAMEWORK
    // Instantiate a spin vista object
    std::shared_ptr<sVObject>   gObject;
#endif
    // Instantiate a spin acquisition object
    std::shared_ptr<pacquisition>   acquisition;
    // Instantiate a spin preprocessing object
    std::shared_ptr<ppreprocessing>   preprocessing;
    // Instantiate a spin image writer object
    std::shared_ptr<pimagewriter>   imagewriter;
  private:
    // Clear memory
    void ClearAfterPreprocess(spin::FovCompleted p);
    void ClearAfterWriting(spin::FovCompleted p);
    //void CheckForCompletion(spin::ModuleStatus p);
};// end of vistaAcquisitionInterface class
#ifdef VISTA_PLUGIN_FRAMEWORK
}
#endif
#endif // vistaInterface
