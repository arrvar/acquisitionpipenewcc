#if 0
#include <iostream>
#include <cstdlib>
//#include "Libraries/vista/Pipeline/Panorama/Initialize/spinVistaPanoramaObject.h"
#include "Libraries/vista/Interface/vistaPanoramaInterface.h"
#include <chrono>

/*! \brief
 * This is the main driver to parse the database file to process incoming images
 * via a Multi process communication framework. The end output are the tiled
 * images which can be loaded onto a viewing software
 */
int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    //std::cout<< argv[0] << std::endl;
    //std::cout << " <path_to_parameter_file.xml>" << std::endl;
    exit(-1);
  }

  auto startProc = std::chrono::high_resolution_clock::now();
  // Get the name of the parameter file
  char* pFile = argv[1];

  // Instantiate a vistaInterface Object
  vistaPanoramaInterface vIf;
  // And begin the process
  vIf.Initialize(pFile);

  auto endProc = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsedA = endProc - startProc;
  //std::cout << "IO + Blending + Tiling: " << elapsedA.count() << std::endl;
  return 0;
}//end of function
#else
#include <dlibxx.hxx>
#include <iostream>
#include <boost/filesystem.hpp>
#include <memory>
#include <chrono>
#include "AbstractClasses/AbstractVistaModule.h"

int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    //std::cout<< argv[0] << std::endl;
    //std::cout << " <path_to_Module>  <path_to_parameter_file.xml>" << std::endl;
    exit(-1);
  }

  auto startProc = std::chrono::high_resolution_clock::now();
  {
    // The library module name
    std::string libname = std::string(argv[1]);
    // Check if the library exists in the given path
    boost::filesystem::path dfile(libname.c_str());
    if (!boost::filesystem::exists(dfile))
    {
      //std::cout<<"Library does not exist"<<std::endl;
      return -2; // the file does not exis
    }

    // And Instantiate the loader
    //std::shared_ptr<dlibxx::handle> lib = std::make_shared<dlibxx::handle>();
    dlibxx::handle lib;
    // Resolve symbols when they are referenced instead of on load.
    lib.resolve_policy(dlibxx::resolve::lazy);
    // Make symbols available for resolution in subsequently loaded libraries.
    lib.set_options(dlibxx::options::global);//| dlibxx::options::no_delete);
    // Load the library specified.
    lib.load(libname);
    // Check if the library could be loaded
    if (!lib.loaded())
    {
      //std::cout<<"Library could not be loaded: "<<lib->error()<<std::endl;
      return -3;
    }

    // Now, we try and create the plugin
    std::shared_ptr< spin::AbstractVistaModule > pModule = lib.template create<spin::AbstractVistaModule>("Vista_Module");
    // Check if the module was loaded
    if (!pModule.get())
    {
      //std::cout<<"Module could not be initialized: "<<std::endl;
      return -4;
    }

    // Instantiate the pipeline by parsing a parameter file
    // Check if the parameter file exists
    boost::filesystem::path pfile(argv[2]);
    if (!boost::filesystem::exists(pfile))
    {
      //std::cout<<"Parameter file does not exist"<<std::endl;
      return -5; // the file does not exis
    }
    int v = pModule->RunPanorama(argv[2]);
    if (v !=1)
    {
      //std::cout<<"Parameter file could not be parsed"<<std::endl;
      return -6;
    }
  }

  auto endProc = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsedA = endProc - startProc;
  //std::cout << "IO + Blending + Tiling: " << elapsedA.count() << std::endl;
  return 0;
}//end of function
#endif
