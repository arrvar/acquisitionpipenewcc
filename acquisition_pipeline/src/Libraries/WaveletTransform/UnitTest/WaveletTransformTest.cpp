#if 0
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "AbstractClasses/AbstractWT2D.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "Utilities/ImageProcessing/WaveletTransform/WT2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/DTCWT2D.h"
#include "Utilities/MatrixUtils/MatrixUtils.h"
#include "Framework/ImageIO/ImageIO.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/eigen.hpp"
#include <string>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    //std::cout<<argv[0]<<" <inpImage> <outImage>"<<std::endl;
    return -1;
  }
  
  std::string refImg  = std::string(argv[1]);
  std::string outImg1 = std::string(argv[2]);
  std::string outImg2 = std::string(argv[3]);
  // instantiate an object for reading images
  std::shared_ptr<spin::RGBImageIO> imageio = std::make_shared<spin::RGBImageIO>();
  cv::Mat img;
  imageio.get()->ReadImage2(refImg, &img);
  cv::Mat convMat[3];
  cv::split(img, convMat);
  convMat[1].convertTo(img,CV_32FC1);
  spin::RMMatrix_Float mat = spin::RMMatrix_Float::Zero(img.rows, img.cols);
  cv::cv2eigen(img,mat);

  
  // Initialize a wavelet transform object
  std::shared_ptr<spin::AbstractWT2D> wt_d = std::make_shared<spin::DTCWT2D>(8,0);
  wt_d.get()->Compute(&mat, NULL);
  spin::RMMatrix_Float dump1, dump2;
  wt_d.get()->RecomposeBands(&dump1,0);
  //wt_d.get()->RecomposeBands(&dump2,1);
  
  std::shared_ptr<spin::AbstractWT2D> wt_r = std::make_shared<spin::DTCWT2D>(wt_d.get());
  //spin::RMMatrix_Float dump2;
  wt_r.get()->Compute(NULL,&dump2);

  spin::FloatImageType2D::Pointer img1 = spin::FloatImageType2D::New();
  spin::FloatImageType2D::RegionType region;
  spin::FloatImageType2D::SizeType size;
  spin::FloatImageType2D::IndexType index;
  index[0]=0;
  index[1]=0;
  size[0] = dump1.cols();
  size[1] = dump1.rows();
  region.SetIndex(index);
  region.SetSize(size);
  img1->SetRegions(region);
  img1->Allocate();
  float* p = img1->GetBufferPointer();
  // copy the data
  memcpy(p, dump1.data(), size[0]*size[1]*sizeof(float));
  // and save the image
  spin::WriteITKImage<spin::FloatImageType2D>(outImg1,img1);
  
  memcpy(p, dump2.data(), size[0]*size[1]*sizeof(float));
  // and save the image
  spin::WriteITKImage<spin::FloatImageType2D>(outImg2,img1);
  return 1;
}//end of function
#endif

#if 0
int main()
{
  
  spin::RMMatrix_Float temp  = spin::RMMatrix_Float::Zero(4,5);
  temp<<1,2,8,5,
        9,6,9,5,
        4,2,1,3,
        3,7,9,8,
        1,4,5,5;
 
  spin::MatrixUtils<spin::RMMatrix_Float> mutils;
  spin::RMMatrix_Float maxRow;
  spin::RMMatrix_Float maxCol;
  mutils.RowwiseMaxima(&temp, &maxRow);
  mutils.ColwiseMaxima(&temp, &maxCol);
  //std::cout<<temp<<std::endl;
  //std::cout<<"===="<<std::endl;
  //std::cout<<maxRow<<std::endl;
  //std::cout<<"===="<<std::endl;
  //std::cout<<maxCol<<std::endl;
  return 1;
 
}
#endif

#if 1
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "Utilities/MatrixUtils/SparseMatrixUtils.h"
#include <iostream>
using namespace spin;
int main()
{
  // 6 rows and 5 columns
  int mRows = 6;
  int nCols = 5;
  RMMatrix_Float v1=RMMatrix_Float::Ones(nCols-1,1)* -1;
  RMMatrix_Float v2=RMMatrix_Float::Ones(nCols-1,1);
  RMSparseMatrix_Float v3;
  SparseMatrixUtils<float> smutils;
  smutils.BidiagonalMatrix(&v2, &v1, &v3);
  RMMatrix_Float d = RMMatrix_Float(v3);
  RMSparseMatrix_Float h_mRows = RMMatrix_Float::Identity(mRows,mRows).sparseView();  
  RMSparseMatrix_Float v3A;
  smutils.KroneckerProduct(&v3,&h_mRows,&v3A);
  
  
  //smutils.KroneckerProduct(&h_nCols,&v3,&v3B);
  
  //std::cout<<v3A.rows()<<" "<<v3A.cols()<<std::endl;
  //std::cout<<"==========="<<std::endl;
  //std::cout<<v3A<<std::endl;
  ////std::cout<<v3B.rows()<<" "<<v3B.cols()<<std::endl;
  ////std::cout<<"==========="<<std::endl;
  
  /*v3C.topRows(25) = v3A;
  v3C.bottomRows(25) = v3B;
  //std::cout<<v3C<<std::endl;
  //std::cout<<"==========="<<std::endl;*/
  
  
  v1=RMMatrix_Float::Ones(mRows-1,1)* -1;
  v2=RMMatrix_Float::Ones(mRows-1,1);
  smutils.BidiagonalMatrix(&v2, &v1, &v3);
  d = RMMatrix_Float(v3);
  RMSparseMatrix_Float h_nCols = RMMatrix_Float::Identity(nCols,nCols).sparseView();  
  RMSparseMatrix_Float v3B;
  smutils.KroneckerProduct(&h_nCols,&v3,&v3B);
  
  //smutils.KroneckerProduct(&h_nCols,&v3,&v3B);
  
  //std::cout<<v3B.rows()<<" "<<v3B.cols()<<std::endl;
  //std::cout<<"==========="<<std::endl;
  //std::cout<<v3B<<std::endl;
  return 1;
}




#endif
