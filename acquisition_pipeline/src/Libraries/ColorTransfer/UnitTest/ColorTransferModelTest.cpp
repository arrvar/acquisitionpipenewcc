#include "Utilities/ImageProcessing/ColorTransfer/ColorTransfer.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <string>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    //std::cout<<argv[0]<<" <ref_imageFolder> <modelName>"<<std::endl;
    return -1;
  }
  
  std::string refFld = std::string(argv[1]);
  std::string model  = std::string(argv[2]);

  spin::ColorTransfer ctransfer(refFld, model);
  return 1;
}//end of function
