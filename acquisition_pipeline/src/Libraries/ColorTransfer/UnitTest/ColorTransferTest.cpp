#include "Utilities/ImageProcessing/ColorTransfer/ColorTransfer.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <string>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    //std::cout<<argv[0]<<" <modelName> <inpImageFolder> <outImageFolder>"<<std::endl;
    return -1;
  }
  
  std::string model     = std::string(argv[1]);
  std::string inpFolder = std::string(argv[2]);
  std::string outFolder = std::string(argv[3]);

  spin::ColorTransfer ctransfer(model);
  ctransfer.PCANormalization(inpFolder, outFolder);
  return 1;
}//end of function
