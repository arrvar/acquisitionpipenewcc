#include "Libraries/ConvexHull/Interface/ConvexHullInterface.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "Utilities/ConvexHull/ConvexHullStruct.h"
#include <vector>
#include <string>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    //std::cout<<argv[0]<<" <inpImage> <maskImage>"<<std::endl;
    return -1;
  }

  std::string refImg = std::string(argv[1]);
  std::string mask   = "";
  if (argc > 2)
    mask = std::string(argv[2]);

  // Read the input image (and mask image if available)
  spin::RGBImageType::Pointer inp = spin::RGBImageType::New();
  spin::UCharImageType2D::Pointer mImg;

  spin::ReadITKImage<spin::RGBImageType>(refImg, inp);
  if (mask !="")
  {
    mImg = spin::UCharImageType2D::New();
    spin::ReadITKImage<spin::UCharImageType2D>(mask, mImg);
  }

  // An instance of a convex hull object
  spin::QhullOutput chull;
  spin::QhullLayer  chullLayer;

  // An instance of a convex hull interface object
  ConvexHullInterface cObject;
  int g = cObject.Compute(&inp, &mImg, &chullLayer);

  // We create a signature based on the convex hull peeling volumes
  spin::RMMatrix_Double chullSignature = spin::RMMatrix_Double::Zero(1, chullLayer.size());
  std::map<int, spin::QhullOutput>::iterator it =  chullLayer.begin();
  auto count = 0;
  double v = 0;
  while (it != chullLayer.end())
  {
    if (count == 0) v = it->second.volume;
    chullSignature(0,count) = it->second.volume/v;
    ++it;
    ++count;
  }
  chullLayer.clear();
  // save the stuff to //std::cout
  //std::cout<<chullSignature<<std::endl;
  return 1;
}//end of function

