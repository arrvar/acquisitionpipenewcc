#ifndef CONVEXHULLINTERFACE_H_
#define CONVEXHULLINTERFACE_H_
#include "Framework/Exports/spinAPI.h"
#include <string>

class SPIN_API ConvexHullInterface
{
  public:
    ConvexHullInterface(){}

    /*! \brief Compute */
    int Compute(void* _cImage, void* _cMask, void* _cB);
};//end of class
#endif
