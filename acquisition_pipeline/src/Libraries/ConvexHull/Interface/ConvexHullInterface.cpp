#include "Libraries/ConvexHull/Interface/ConvexHullInterface.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "Utilities/ConvexHull/ConvexHullUtils.h"
#include "Utilities/ConvexHull/ConvexHullStruct.h"
#include "itkImageRegionIterator.h"

/*! \brief NormalizeData
 *  This function is used to normalize the data by making it zero mean and
 *  applying a rotation along the principal coordinate axes
 */
template <class T>
int NormalizeData(T* input, T* normalized)
{
  typedef typename spin::RMMatrix_Double RMMatrix_Double;
  // Get the dimensions of the data
  int numFeatures = input->rows();
  int numDims     = input->cols();

  typedef typename T::Scalar Scalar;

  // Allocate a matrix of Ones
  T Ones = T::Ones(1,numFeatures);

  // Compute the mean of all features and store it in mean
  T mean = (Ones* (*input)) * static_cast<Scalar>(1.0)/static_cast<Scalar>(numFeatures);

  // Finally, subtract the mean feature from rows in data. Store the result in normalized
  RMMatrix_Double zc = ( (*input) - Ones.transpose() * mean).template cast<double>();

  // Now, compute the covariance matrix
  RMMatrix_Double cov = (zc.transpose()*zc)*1.0/zc.rows();

  // Compute the projection matrix from the data
  Eigen::JacobiSVD<RMMatrix_Double> svd(cov, Eigen::ComputeThinU|Eigen::ComputeThinV);

  // Apply a projection on the zero-centered data
  (*normalized) = (zc*svd.matrixU()).template cast<Scalar>();

  return 1;
}//end of function

/*! \brief Compute
 *  This is the interface function to compute a convex hull given an image
 *  and a corresponding mask. If no mask is provided, the entire image is used for
 *  computing a convex hull.
 */
int ConvexHullInterface::Compute(void* _cImage, void* _cMask, void* _cB)
{
  // Sanity checks
  spin::RGBImageType::Pointer* cImage = static_cast<spin::RGBImageType::Pointer*>(_cImage);
  if (!cImage) return -1;

  // Mask is optional
  spin::UCharImageType2D::Pointer* cMask = static_cast<spin::UCharImageType2D::Pointer*>(_cMask);
  bool useMask = true;
  if (!cMask) useMask = false;

  // The points will be stored in this array
  spin::RMMatrix_Double cInput;

  if (useMask)
  {
    // If we have a mask save data only within the mask
    typedef itk::ImageRegionIterator<spin::UCharImageType2D> iterator1;
    typedef itk::ImageRegionIterator<spin::RGBImageType> iterator2;
    iterator1 it1(*cMask, (*cMask)->GetLargestPossibleRegion());
    it1.GoToBegin();
    iterator2 it2(*cImage, (*cImage)->GetLargestPossibleRegion());
    it2.GoToBegin();
    while(!it1.IsAtEnd())
    {
      if (it1.Get() > 0)
      {
        // Get the RGB pixel at this location
        spin::RGBPixelType pix = it2.Get();
        // Create a temporary array to hold this data
        spin::RMMatrix_Double dummy = spin::RMMatrix_Double::Zero(1,3);
        dummy(0,0) = pix.GetRed()/255.0;
        dummy(0,1) = pix.GetGreen()/255.0;
        dummy(0,2) = pix.GetBlue()/255.0;
        // Concatenate this point
        spin::RMMatrix_Double temp = cInput;
        cInput.resize(temp.rows()+1,3);
        if (temp.rows() == 0)
          cInput = dummy;
        else
          cInput<<temp,dummy;
      }
      ++it1;
      ++it2;
    }
  }
  else
  {
    // If we do not have a mask use all data from the image is used
    typedef itk::ImageRegionIterator<spin::RGBImageType> iterator2;
    iterator2 it2(*cImage, (*cImage)->GetLargestPossibleRegion());
    it2.GoToBegin();
    while(!it2.IsAtEnd())
    {
      // Get the RGB pixel at this location
      spin::RGBPixelType pix = it2.Get();
      // Create a temporary array to hold this data
      spin::RMMatrix_Double dummy = spin::RMMatrix_Double::Zero(1,3);
      dummy(0,0) = pix.GetRed()/255.0;
      dummy(0,1) = pix.GetGreen()/255.0;
      dummy(0,2) = pix.GetBlue()/255.0;
      // Concatenate this point
      spin::RMMatrix_Double temp = cInput;
      cInput.resize(temp.rows()+1,3);
      if (temp.rows() == 0)
        cInput = dummy;
      else
        cInput<<temp,dummy;
      ++it2;
    }
  }

  // Normalize the data
  spin::RMMatrix_Double cInput2;

  NormalizeData<spin::RMMatrix_Double>(&cInput, &cInput2);

  // Compute the convex hull layers
  spin::ConvexHullUtils<spin::RMMatrix_Double> chull;
  int g = chull.ComputeConvexHullLayers(&cInput2, _cB);
  if ( g !=1) return g;

  return 1;
}//end of function
