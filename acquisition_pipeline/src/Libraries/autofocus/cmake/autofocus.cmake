#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building vista
#------------------------------------------------------------------------------
SET(PROJ vista)
SET(${PROJ}_EXT_LIBS)

#------------------------------------------------------------------------------
#Include the third party libraries needed 
#------------------------------------------------------------------------------
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/TPL.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building this
#library
#------------------------------------------------------------------------------
SET(${PROJ}_LIB_DIRS
    Common/Framework
    Libraries/vista/Pipeline
    Utilities/Stitching
    Utilities/Metrics
    Utilities/ImageProcessing/ITKImageUtils
    Utilities/ImageProcessing/FFTUtils
    Utilities/ImageProcessing/HistogramUtils
)

SET(${PROJ}_Utilities_SRCS) 
FOREACH(CONTENT ${${PROJ}_LIB_DIRS})
  FILE(GLOB_RECURSE U_SRCS
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.cpp
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.hh
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.h
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.txx)
  SET(${PROJ}_Utilities_SRCS ${${PROJ}_Utilities_SRCS} ${U_SRCS})
ENDFOREACH()

#------------------------------------------------------------------------------
# Build a shared library so that it can be used by other modules
#------------------------------------------------------------------------------
FILE(GLOB_RECURSE itf_${PROJ} ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/*.cpp 
                              ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/*.h)
ADD_LIBRARY(spin${PROJ} SHARED ${${PROJ}_Pipeline} ${itf_${PROJ}} ${${PROJ}_Utilities_SRCS})

TARGET_LINK_LIBRARIES(spin${PROJ} ${${PROJ}_EXT_LIBS})
IF(NOT MSVC)
  SET_TARGET_PROPERTIES(spin${PROJ} PROPERTIES DEBUG_POSTFIX "_d" CXX_VISIBILITY_PRESET hidden)
ELSE()
  SET_TARGET_PROPERTIES(spin${PROJ} PROPERTIES  COMPILE_FLAGS "-DVISTA_EXPORTS"
                                                DEBUG_POSTFIX "_d")
ENDIF()

#------------------------------------------------------------------------------
#Check if a C++ Unit Test is requested
#------------------------------------------------------------------------------
SET(ut_spin${PROJ} OFF CACHE BOOL "Build C++ Unit test for ${PROJ} Module")
IF(ut_spin${PROJ})
  ADD_EXECUTABLE(ut_spin${PROJ} ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/spinVista.cpp)
  TARGET_LINK_LIBRARIES(ut_spin${PROJ} spin${PROJ} ${${PROJ}_EXT_LIBS})
  IF(MSVC)
    ADD_CUSTOM_COMMAND(
      TARGET ut_spin${PROJ}  
      POST_BUILD 
      COMMAND ${CMAKE_COMMAND} 
              -DTARGETDIR=$<TARGET_FILE_DIR:ut_spin${PROJ}> 
              -DRELEASE_LIBS=${RELEASE_LIBS_TO_BE_COPIED}
              -DDEBUG_LIBS=${DEBUG_LIBS_TO_BE_COPIED}
              -DCFG=${CMAKE_CFG_INTDIR} -P "${LIBRARY_ROOT}/PostBuild.cmake"
      ) 
  ENDIF()
ENDIF()

#------------------------------------------------------------------------------
# Make a python module
#------------------------------------------------------------------------------
SET(Python_Wrapped_${PROJ} OFF CACHE BOOL "Build Python Wrapped ${PROJ} Module")
IF(Python_Wrapped_${PROJ})
  SET(Python_Wrapped_Libs ON)
  ADD_LIBRARY(py_spin${PROJ} SHARED 
              ${${PROJ}_Utilities_SRCS} ${itf_${PROJ}}
              ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Python/py_spinVista.cpp)
  TARGET_LINK_LIBRARIES(py_spin${PROJ} ${${PROJ}_EXT_LIBS})
  #Add specifics depending on whether this is compiled for windows of Linux
  IF(NOT MSVC)
    SET_TARGET_PROPERTIES(py_spin${PROJ} 
                          PROPERTIES  PREFIX "" COMPILE_FLAGS "-DPYTHON_INTERFACE")
  ELSE()
    SET_TARGET_PROPERTIES(py_spin${PROJ} 
                          PROPERTIES  PREFIX "" SUFFIX "pyd" COMPILE_FLAGS "-DPYTHON_INTERFACE")
    ADD_CUSTOM_COMMAND(
      TARGET py_spin${PROJ}  
      POST_BUILD 
      COMMAND ${CMAKE_COMMAND} 
              -DTARGETDIR=$<TARGET_FILE_DIR:py_spin${PROJ}> 
              -DRELEASE_LIBS=${RELEASE_LIBS_TO_BE_COPIED}
              -DDEBUG_LIBS=${DEBUG_LIBS_TO_BE_COPIED}
              -DCFG=${CMAKE_CFG_INTDIR} -P "${LIBRARY_ROOT}/PostBuild.cmake"
      ) 
  ENDIF()
ENDIF()

