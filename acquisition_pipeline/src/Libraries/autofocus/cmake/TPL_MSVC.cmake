SET(NonModular_List
  Eigen
)

IF(Python_Wrapped_Libs)
  SET(NonModular_List
    ${NonModular_List}
    Python35
  )
ENDIF()

SET(Modular_List
  Py35boost-1_59
  ITK
  Qt5
  OpenCV
)

FOREACH(mLib ${NonModular_List})
  SET(NonModularLibrary_${mLib} ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()

FOREACH(mLib ${Modular_List})
  SET(ModularLibrary_${mLib} ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()

#######################################################################################

SET(Boost_MANDATORY_PACKAGES
  filesystem
  thread
  date_time
  chrono
  system
  time
  timer
)

IF(Python_Wrapped_Libs)
  SET(Boost_MANDATORY_PACKAGES ${Boost_MANDATORY_PACKAGES}
      python
      python3
    )
ENDIF()

FOREACH(mLib ${Boost_MANDATORY_PACKAGES})
  SET(Boost_${mLib}_ENABLE ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()

#######################################################################################

SET(ITK_MANDATORY_PACKAGES
  IOBMP
  IONRRD
  IOPNG
  IOJPEG
  JPEG
  PNG
  VideoIO
  ImageFilterBase
  Transform
  Optimizers
  Optimizersv4
)

FOREACH(mLib ${ITK_MANDATORY_PACKAGES})
  SET(ITK_${mLib}_ENABLE ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()

#######################################################################################

SET(Qt5_MANDATORY_PACKAGES
  Core
  Sql
)

FOREACH(mLib ${Qt5_MANDATORY_PACKAGES})
  SET(Qt5_${mLib}_ENABLE ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()
ADD_DEFINITIONS(-DQT_NO_KEYWORDS)
#######################################################################################

#######################################################################################

SET(OpenCV_MANDATORY_PACKAGES
  core
  highgui
  imgcodecs
  imgproc
)

FOREACH(mLib ${OpenCV_MANDATORY_PACKAGES})
  SET(OpenCV_${mLib}_ENABLE ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()

FIND_PACKAGE(ThirdPartyLibraries_VS2015 REQUIRED)