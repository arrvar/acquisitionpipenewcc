#include <iostream>
#include <cstdlib>
#include "Libraries/vista/Pipeline/Initialize/spinVistaObject.h"
#include "Libraries/vista/Interface/vistaInterface.h"
#include <chrono>

/*! \brief
 * This is the main driver to parse the database file to process incoming images
 * via a Multi process communication framework. The end output are the tiled
 * images which can be loaded onto a viewing software
 */
int main(int argc, char* argv[])
{ 
  if (argc < 2)
  {
    //std::cout<< argv[0] << std::endl;
    //std::cout << " <path_to_parameter_file.xml>" << std::endl;
    exit(-1);
  }
  
  auto startProc = std::chrono::high_resolution_clock::now();
  // Get the name of the parameter file
  char* pFile = argv[1];
  // Instantiate a vistaInterface Object 
  vistaInterface vIf;
  // And initialize it
  vIf.Initialize(pFile);
  // Get an access to the global object
  typedef spin::stitching::spinVistaObject sVObject;
  std::shared_ptr<sVObject> pObject = vIf.GetObject();

  // Mimic the scan that will be undertaken by the X-Y stage
  // This loop represents the acquisition phase
  std::map<spin::stitching::pDouble, spin::stitching::FOVStruct>* dFov = \
  pObject.get()->gMap.get()->GetGridMapping();
  std::vector< spin::stitching::pDouble >* dIdx = pObject.get()->gMap.get()->GetGridIndices();
  for(auto it: (*dIdx))
  { 
    // Get the input image that will be processed
    std::string inpImage = pObject.get()->slideDir + "/" + (*dFov)[it].aoiName + pObject.get()->file_extension;
    //std::cout<<"Processing image: "<<inpImage<<std::endl;
    // Get its col, row indices 
    float gx = static_cast<float>(it.second);
    float gy = static_cast<float>(it.first);
    // Process it; By default we assume that it is a fg image
    vIf.ComputeGridLocations(inpImage,gx, gy, 0);
  }
  // Complete the process
  vIf.CompleteGridLocationsProcessing();
  
  if (pObject.get()->inlineBlending)
  {
    // Instantiate the tiling process
    vIf.InlineTiling();
    
    // Complete the process
    vIf.CompleteInlineTiling();
  }

  auto endProc = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsedA = endProc - startProc;
  //std::cout << "Preprocessing + Displacement Time + Blending + Tiling + IO: " << elapsedA.count() << std::endl;
  return 0;
}//end of function
