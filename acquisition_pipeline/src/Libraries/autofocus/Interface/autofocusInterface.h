#ifndef AUTOFOCUSINTERFACE_H
#define AUTOFOCUSINTERFACE_H
#include "Framework/Exports/spinAPI.h"
#include "AbstractClasses/AbstractCallback.h"
#include <memory>
#ifdef PYTHON_INTERFACE
#include <boost/python.hpp>
#include <boost/python/numeric.hpp>
#include <boost/python/tuple.hpp>
#include <numpy/ndarrayobject.h>
#include <numpy/noprefix.h>
#endif

// Forward declaration of classes 
namespace spin
{
  namespace autofocus
  {
    struct spinAFObject;
  }//end of namespace stitching
}//end of namespace spin

/*! \brief autofocusInterface
 *  This is an interface class that is used to call the 
 *  backend pipeline associated with spin autofocus.
 */
#ifndef PYTHON_INTERFACE
class SPIN_API autofocusInterface
#else
class autofocusInterface
#endif
{
  public:
    // Relevant typedefs
    typedef spin::autofocus::spinAFObject sAFObject;
#ifdef PYTHON_INTERFACE
    typedef boost::python::numeric::array narray;
#endif  
    // Constructor
    autofocusInterface();
    
    // Destructor
    ~autofocusInterface();

    // Initialize the auto focus module
    int Initialize(const char* pFile, int height, int width);
    
#ifdef PYTHON_INTERFACE
    // Interpolate a sparse grid
    void InterpolateGrid(narray InputGrid);
    // Interpolate a sparse grid
    narray InterpolateGrid();
    // Get a focus metric for an input image
    float GetFocusMetric(narray InputImage);
#else    
    // Interpolate a sparse grid
    void InterpolateGrid();
    // Get a focus metric for an input image
    float GetFocusMetric(void* InputImg);
#endif
    void SetFocus(int gy, int gx, float val);
    
    // Get a const access to gObject
    const std::shared_ptr<sAFObject> GetObject(){return gObject;}
  private:
    // Instantiate a spin autofocus object
    std::shared_ptr<sAFObject>   gObject;
};// end of autofocusInterface class
#endif // autofocusInterface