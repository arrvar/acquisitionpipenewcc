#include "Libraries/autofocus/Interface/autofocusInterface.h"
#include "Libraries/autofocus/Pipeline/Initialize/autofocusInitialize.h"
#include "Libraries/autofocus/Pipeline/Initialize/autofocusObject.h"
#include "Utilities/ImageProcessing/Interpolation/MBAInterpolation.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "itkImportImageFilter.h"

namespace
{ 
  typedef spin::RGBPixelType RGBPixelType;
  typedef spin::RGBImageType RGBImageType;
}

/*! \brief Default constructor */
autofocusInterface::autofocusInterface()
{
}//end of function

/*! \brief Initialize 
 *  This function is used to initialize the pipeline using a 
 *  parameter file passed as an input
 *  @pFile        : The parameter file
 *  @gridHeight   : The height of th grid
 *  @gridWidth    : The width of the grid
 */
int autofocusInterface::Initialize(const char* pFile, int gridHeight, int gridWidth)
{
  // Initialize pObject
  gObject = std::make_shared<sAFObject>();
  
  // Initialize a parser
  std::shared_ptr<spin::autofocus::spinAFInitialize> parser = std::make_shared<spin::autofocus::spinAFInitialize>();
  // Check the global object can be populated
  int p = parser.get()->Initialize(gObject.get(), pFile);

  // If not, return the error code to the calling function
  if (p !=1) return p;
  
  // Now, allocate memory for the focus map grid
  gObject.get()->FocusMap = spin::RMMatrix::Zero(gridHeight, gridWidth);
  
  return 1;
}//end of function

#ifndef PYTHON_INTERFACE
  /*! \brief InterpolateGrid
  *  This is the driver function to interpolate a sparsely populated
  *  grid of focus map values.
  */
  void autofocusInterface::InterpolateGrid()
  { 
    // Run the MBA innterpolation pipeline
    spin::MBAInterpolation mba;
    mba.Compute(&(gObject.get()->FocusMap),gObject.get()->MBALevels); 
  }//end of function

  /*! \brief GetFocusMetric
  *  This is the driver function to compute a focus metric
  */
  float autofocusInterface::GetFocusMetric(void* InputImage)
  { 
    // Run the MBA innterpolation pipeline
    return (gObject.get()->FocusMetric.get()->Compute(InputImage,gObject.get())); 
  }//end of function
#endif

#ifdef PYTHON_INTERFACE
  /*! \brief InterpolateGrid
   *  This is the driver function to interpolate a sparsely populated
   *  grid of focus map values.
   */
  void autofocusInterface::InterpolateGrid(narray InputGrid)
  { 
    // Typecast the input
    float* input = (float*)PyArray_DATA((PyArrayObject*)InputGrid.ptr());

    // Run the MBA interpolation pipeline
    spin::MBAInterpolation mba;
    mba.Compute(&(gObject.get()->FocusMap),gObject.get()->MBALevels); 
    
    // TODO: Copy the buffer from the focus map into InputGrid

  }//end of function

  /*! \brief InterpolateGrid
   *  This is the driver function to interpolate a sparsely populated
   *  grid of focus map values.
   */
  narray autofocusInterface::InterpolateGrid()
  { 
    // Run the MBA interpolation pipeline
    spin::MBAInterpolation mba;
    mba.Compute(&(gObject.get()->FocusMap),gObject.get()->MBALevels); 
    
    // TODO: Copy the buffer from the focus map into a numpy array and return it

  }//end of function

  /*! \brief GetFocusMetric
   *  This is the driver function to compute a focus metric
   *  @InputImage   : A numpy array
   */
  float autofocusInterface::GetFocusMetric(narray InputImage)
  { 
    // Typecast the input
    RGBPixelType* input_data = (RGBPixelType*)PyArray_DATA((PyArrayObject*)InputImage.ptr());
    // Get the dimensions of the image
    RGBImageType::SizeType size = gObject.get()->mCalImage->GetLargestPossibleRegion().GetSize();

    const unsigned int numPixels = size[0]*size[1];
    const bool importImageFilterWillOwnTheBuffer = false;
    
    // Instantiate a RGBImporter 
    RGBImportType::Pointer import = RGBImportType::New();
    import->SetRegion(gObject.get()->mCalImage->GetLargestPossibleRegion());
    import->SetOrigin(gObject.get()->mCalImage->GetOrigin());
    import->SetSpacing(gObject.get()->mCalImage->GetSpacing());
    import->SetImportPointer(input_data, numPixels, importImageFilterWillOwnTheBuffer);
    import->Update();
    
    // Compute the focus metric for this image
    RGBImageType::Pointer input = import->GetOutput();
    return (gObject.get()->FocusMetric.get()->Compute(&input,gObject.get())); 
  }//end of function
#endif

/*! \brief SetFocus
 *  This is the driver function to set a focus value at a given index location
 */
float autofocusInterface::SetFocus(int gy, int gx, float val)
{ 
  // Run the MBA innterpolation pipeline
  gObject.get()->FocusMap(gy, gx) = val;
}//end of function

