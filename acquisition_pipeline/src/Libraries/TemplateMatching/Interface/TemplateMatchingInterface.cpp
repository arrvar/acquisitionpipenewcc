#include "Libraries/TemplateMatching/Interface/TemplateMatchingInterface.h"
#include "Libraries/TemplateMatching/Pipeline/spinTemplateMatchingInitialize.h"
#include "Libraries/TemplateMatching/Pipeline/spinTemplateMatchingObject.h"
#include "Utilities/ImageProcessing/TemplateMatching/TemplateMatching.h"

/*! \brief Default constructor */
template <class T>
TemplateMatchingInterface<T>::TemplateMatchingInterface()
{
}//end of function

/*! \brief Initialize 
 *  This function is used to initialize the pipeline using a 
 *  parameter file passed as an input
 *  @pFile        : The parameter file
 */
template <class T>
int TemplateMatchingInterface<T>::Initialize(const char* pFile)
{
  // Initialize template matching object
  tmatch = std::make_shared<spin::spinTemplateMatchingObject<T> >();
  
  // Initialize a parser
  std::shared_ptr<spin::spinTemplateMatchingInitialize<T> > parser = \
  std::make_shared<spin::spinTemplateMatchingInitialize<T> >();
  // Check the global object can be populated
  int p = parser.get()->Initialize(tmatch.get(), pFile);

  // If not, return the error code to the calling function
  if (p !=1) return p; 
  return 1;
}//end of function

/*! \brief ProcessGridLocations
 *  This is the driver function to compute the displacements
 *  between neighboring images as part of the spin vista pipeline
 */
template <class T>
void TemplateMatchingInterface<T>::ProcessImage(void* img, void* p)
{ 
  // Read the image
  tmatch.get()->tmatch.get()->GetWeights(img, p);
}//end of function

template class TemplateMatchingInterface<float>;