#ifndef TEMPLATEMATCHINGINTERFACE_H
#define TEMPLATEMATCHINGINTERFACE_H
#include "Framework/Exports/spinAPI.h"
#include "AbstractClasses/AbstractCallback.h"
#include <memory>

// Forward declaration of classes 
namespace spin
{
  template <class T>
  struct spinTemplateMatchingObject;
}//end of namespace spin

/*! \brief vistaInterface
 *  This is an interface class that is used to call the 
 *  backend pipeline associated with spin Vista.
 */
template <class T>
class SPIN_API TemplateMatchingInterface
{
  public: 
    typedef spin::spinTemplateMatchingObject<T> sVObject;
    // Constructor
    TemplateMatchingInterface();
    
    // Destructor
    ~TemplateMatchingInterface(){}

    // Initialize the pipeline
    int Initialize(const char* pFile);

    // Process an image
    void ProcessImage(void* img, void* p);
    
    // Get a const access to gObject
    const std::shared_ptr<sVObject> GetGObject(){return tmatch;}
  private:
    std::shared_ptr<spin::spinTemplateMatchingObject<T> > tmatch;
};// end of TemplateMatchingInterface class
#endif // vistaInterface