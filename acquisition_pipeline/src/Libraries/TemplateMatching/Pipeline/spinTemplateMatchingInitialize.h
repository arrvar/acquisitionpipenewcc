#ifndef SPINVISTATEMPLATEMATCHINGINITIALIZE_H_
#define SPINVISTATEMPLATEMATCHINGINITIALIZE_H_
#include "Framework/Exports/spinAPI.h"

namespace spin 
{
  /*! \brief A base class for initializing all objects/parameters
   *  associated with spinTemplateMatching
   */
  template <class T>
  class SPIN_API spinTemplateMatchingInitialize
  {
    public:
      // Constructor
      spinTemplateMatchingInitialize(){}
      // API to initialize
      int Initialize(void* obj, const char* param);
  };//end of class
}//end of namespace

#endif
