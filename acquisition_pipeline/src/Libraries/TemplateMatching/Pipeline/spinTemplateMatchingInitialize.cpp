#include "Libraries/TemplateMatching/Pipeline/spinTemplateMatchingInitialize.h"
#include "Libraries/TemplateMatching/Pipeline/spinTemplateMatchingObject.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <algorithm>
#include <map>

namespace spin 
{   
  /*! \brief Initialize()
    *  This function is used to parse grid information
    *  that will eventually result in the scan pattern. Before calling
    *  this function, it is assumed that this object is initialized.
    */
  template <class T>
  int spinTemplateMatchingInitialize<T>::Initialize(void* obj, const char* ParameterFileName)
  {
    // Check if the parameter file parser is correct
    std::ifstream input(ParameterFileName);
    // populate tree structure pt
    using boost::property_tree::ptree;
    ptree pt;
    try
    {
      // parse the xml file
      read_xml(input, pt);
    }
    catch(const boost::property_tree::ptree_error &e)
    {
      return -1;
    }
    
    // Here, we downcast a default object 
    spinTemplateMatchingObject<T>* gObject = static_cast<spinTemplateMatchingObject<T>*>(obj);
    if (gObject == NULL) return -2;

    // and instantiate a template matching object
    gObject->tmatch = std::make_shared< TemplateMatching<T> >();
    
    // create a map of images that will form part of the template 
    std::map<int, std::string> templateImages;
    int Channel = 1;
    int Subsampling = 4;
    
    // Now, parse through all parameter settings and populate gObject
    BOOST_FOREACH(ptree::value_type& v, pt.get_child("TemplateMatching"))
    {
      if (v.first == "TemplateImage")
      {
        // Get the string 
        std::string pTag = v.second.get<std::string>("");
        // Now, split the string. The first part contains the index
        // and the second part contains the name of the string
        std::vector<std::string> tRes;
        boost::split(tRes, pTag, boost::is_any_of(","));
        if (tRes.size() == 2)
        {
          templateImages[std::stoi(tRes[0])]=tRes[1];
        }
        tRes.clear();
      }
      else
      if (v.first == "Channel")
        Channel = v.second.get<int>("");
      else
      if (v.first == "Subsampling")
        Subsampling = v.second.get<int>("");
    }
    
    // Initialize template matching object
    std::map<int, std::string>::iterator it = templateImages.begin();
    bool tObj=false;
    while (it != templateImages.end())
    {
      // Check if the image exists
      boost::filesystem::path dir(it->second);
      if (boost::filesystem::exists(dir))
      {
        std::cout<<"Reading Template: "<<it->second<<std::endl;
        // Read the image
        RGBImageType::Pointer img = RGBImageType::New();
        ReadITKImage<RGBImageType>(it->second, img);
        if (!tObj)
        {
          // Get the dimensions of the image
          RGBImageType::SizeType size = img->GetLargestPossibleRegion().GetSize();
          // and initialize gObject
          gObject->tmatch.get()->Initialize(templateImages.size(),size[1],size[0],Subsampling, Channel);
          // and ensure that this block is never accessed again
          tObj = true;
        }
        // Insert the template
        gObject->tmatch.get()->SetTemplate(&img, it->first);
      }
      ++it;
    }
    
    // Check if the data structure is valid
    if (templateImages.size() <=0 ) return -4;
    templateImages.clear();
    return 1;
  }//end of function
  
  template class spinTemplateMatchingInitialize<float>;
}//end of namespace spin
