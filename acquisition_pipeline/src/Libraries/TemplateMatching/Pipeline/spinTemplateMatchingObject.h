#ifndef SPINTEMPLATEMATCHINGOBJECT_H_
#define SPINTEMPLATEMATCHINGOBJECT_H_
#include <memory>
#include "Utilities/ImageProcessing/TemplateMatching/TemplateMatching.h"

namespace spin 
{
  /*! \brief spinVistaAcquisitionObject 
  *  This structure contains all information that would be needed 
  *  to process whole slide stitching.
  */
  template <class T>
  struct spinTemplateMatchingObject
  {
    std::shared_ptr< TemplateMatching<T> > tmatch;  
  };//end of struct
}//end of namespace spin

#endif

