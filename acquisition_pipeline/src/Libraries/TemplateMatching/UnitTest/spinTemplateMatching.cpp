#include "Libraries/TemplateMatching/Interface/TemplateMatchingInterface.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <vector>
#include <string>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    std::cout<<argv[0]<<" <inpImageFolder> <ParameterFile.xml>"<<std::endl;
    return -1;
  }
  
  std::string refImgFolder = std::string(argv[1]);
  std::string paramFile    = std::string(argv[2]);
  
  // We scan the ref image folder. It may contain either a single image or
  // multiple images.
  boost::filesystem::path dir1(refImgFolder);
  if (!boost::filesystem::exists(dir1))
  {
  return -1;
  }
  
  boost::filesystem::path dir2(paramFile);
  if (!boost::filesystem::exists(dir2))
  {
  return -2;
  }
  
  // instantiate a TemplateMatching interface object
  typedef TemplateMatchingInterface<float> _tmf;
  std::shared_ptr<_tmf> tmf = std::make_shared<_tmf>();
  // Intialize the object
  int p = tmf.get()->Initialize(paramFile.c_str());
  if (p !=1) return -3;
  
  // Now, for each image in the folder we run the pipeline
  boost::filesystem::directory_iterator it(refImgFolder), eod; 
  BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))   
  { 
    if(boost::filesystem::is_regular_file(p))
    {
      // Get the name of the image
      std::string refImg = p.string();
      std::cout<<"Processing Image: "<<refImg<<std::endl;
      spin::RGBImageType::Pointer img = spin::RGBImageType::New();
      spin::ReadITKImage<spin::RGBImageType>(refImg, img);
      // Assign memory for a vector
      spin::RMMatrix_Float vec;
      tmf.get()->ProcessImage(&img, &vec);
      std::cout<<"\t Measure: "<<vec.transpose()<<std::endl;
    }
  }
  return 1;
}//end of function
