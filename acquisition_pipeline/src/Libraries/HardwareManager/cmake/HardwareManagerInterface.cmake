INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/Common/pyboostcvconverter/include)  
#We need to include Boost_Python here
SET(Boost_LIBS ${Boost_LIBS} python36)
ADD_DEFINITIONS(-DNPY_NO_DEPRECATED_API=NPY_1_7_API_VERSION)
FIND_PACKAGE(Boost REQUIRED COMPONENTS "${Boost_LIBS}")
#Here, we add a global definition indicating that only dynamic linking is possible
#Also make sure that /MD flags are set for all your CMake projects
#https://svn.boost.org/trac/boost/ticket/6644
ADD_DEFINITIONS(-DBOOST_ALL_DYN_LINK )
#Include the path of the boost include directory
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
#and the library directories
LINK_DIRECTORIES(${Boost_LIBRARY_DIR})
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${Boost_LIBRARIES})

#Also find python libraries
FIND_PACKAGE(PythonLibs 3 REQUIRED)
INCLUDE_DIRECTORIES(${PYTHON_INCLUDE_PATH})

#Build the python module 
ADD_LIBRARY(py_spinHardwareManager SHARED 
            ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/py_spinHardwareManagerInterface.cpp
            ${PROJECT_SOURCE_DIR}/Common/pyboostcvconverter/src/pyboost_cv3_converter.cpp
            ${${PROJ}_ACQUISITION_Utilities_SRCS}
            )
TARGET_COMPILE_OPTIONS(py_spinHardwareManager PRIVATE "-DVISTA_PLUGIN_FRAMEWORK")
TARGET_LINK_LIBRARIES(py_spinHardwareManager ${${PROJ}_EXT_LIBS})
  
SET_TARGET_PROPERTIES(py_spinHardwareManager
                      PROPERTIES PREFIX "" COMPILE_FLAGS "-DPYTHON_INTERFACE")

