SET(Modular_List
	Qt5
	OpenCV
    Py35boost-1_59
    MicroManager
)

FOREACH(mLib ${Modular_List})
  SET(ModularLibrary_${mLib} ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()

#######################################################################################

SET(Qt5_MANDATORY_PACKAGES
	Core
	Sql
)

FOREACH(mLib ${Qt5_MANDATORY_PACKAGES})
  SET(Qt5_${mLib}_ENABLE ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()
ADD_DEFINITIONS(-DQT_NO_KEYWORDS)
SET(CMAKE_AUTOMOC ON)
#######################################################################################

#######################################################################################

SET(OpenCV_MANDATORY_PACKAGES
	core
	highgui
	imgcodecs
	imgproc
)

FOREACH(mLib ${OpenCV_MANDATORY_PACKAGES})
  SET(OpenCV_${mLib}_ENABLE ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()

#######################################################################################


#######################################################################################

SET(Boost_MANDATORY_PACKAGES
  filesystem
  thread
  date_time
  chrono
  system
  timer
)

IF(${PROJ}_Python_Wrapped_Libs)
  SET(Boost_MANDATORY_PACKAGES ${Boost_MANDATORY_PACKAGES} python python3)
ENDIF()

FOREACH(mLib ${Boost_MANDATORY_PACKAGES})
  SET(Boost_${mLib}_ENABLE ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()

#######################################################################################

#######################################################################################

SET(MicroManager_MANDATORY_PACKAGES
    MatrixVision
    VulcanScope
)

FOREACH(mLib ${MicroManager_MANDATORY_PACKAGES})
    SET(DeviceAdapter_${mLib}_ENABLE ON CACHE BOOL "User ${mLib} Library" FORCE)
ENDFOREACH()

#######################################################################################

FIND_PACKAGE(ThirdPartyLibraries_VS2015 REQUIRED)
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${EXT_LIBS})