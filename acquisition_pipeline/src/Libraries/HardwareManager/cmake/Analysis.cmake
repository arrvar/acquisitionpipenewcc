ADD_EXECUTABLE(ut_spinAnalysis
                 ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/AnalysisTest.cpp
                 ${${PROJ}_ACQUISITION_Utilities_SRCS}
                 )
  SET_TARGET_PROPERTIES(ut_spinAnalysis PROPERTIES  COMPILE_FLAGS "-DVISTA_PLUGIN_FRAMEWORK")
  TARGET_LINK_LIBRARIES(ut_spinAnalysis spinvistaAcquisition ${${PROJ}_EXT_LIBS})
