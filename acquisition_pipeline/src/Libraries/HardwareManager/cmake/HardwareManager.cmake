#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building hardware manager
#------------------------------------------------------------------------------
SET(PROJ HardwareManager)
SET(${PROJ}_EXT_LIBS)

#------------------------------------------------------------------------------
#Include the third party libraries needed 
#------------------------------------------------------------------------------
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/TPL.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building 
#vistaAcquisition
#------------------------------------------------------------------------------
SET(${PROJ}_ACQUISITION_LIB_DIRS
    Common/AbstractClasses
    Common/ErrorManagement
    Common/Framework/RegionDetection
    Common/Framework/Logger
    Common/Framework/Exports
    Common/Framework/HardwareManager
)

SET(${PROJ}_ACQUISITION_Utilities_SRCS) 
FOREACH(CONTENT ${${PROJ}_ACQUISITION_LIB_DIRS})
  FILE(GLOB_RECURSE U_SRCS
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.cpp
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.hh
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.h
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.txx)
  SET(${PROJ}_ACQUISITION_Utilities_SRCS ${${PROJ}_ACQUISITION_Utilities_SRCS} ${U_SRCS})
ENDFOREACH()

#SET(ZTrigger_Python_Wrapped_Libs OFF CACHE BOOL "Build Python Wrappers of ZTriggerInterface")
#IF(ZTrigger_Python_Wrapped_Libs)
#  INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/ZTrigger.cmake)
#ENDIF()

SET(${PROJ}_Python_Wrapped_Libs OFF CACHE BOOL "Build Python Wrappers of ${PROJ}Interface")
IF(${PROJ}_Python_Wrapped_Libs)
  INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/HardwareManagerInterface.cmake)
ENDIF()

SET(ut_spin${PROJ} OFF CACHE BOOL "Build C++ Unit test for ${PROJ}Acquisition Module")
IF(ut_spin${PROJ})
  ADD_EXECUTABLE(ut_spin${PROJ}
                 ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/spinVistaHardwareManager.cpp
                 ${${PROJ}_ACQUISITION_Utilities_SRCS}
                 )
  SET_TARGET_PROPERTIES(ut_spin${PROJ} PROPERTIES  COMPILE_FLAGS "-DVISTA_PLUGIN_FRAMEWORK")
  TARGET_LINK_LIBRARIES(ut_spin${PROJ} ${${PROJ}_EXT_LIBS})
  install (TARGETS ut_spin${PROJ}   DESTINATION	 ${CMAKE_INSTALL_PREFIX})  
ENDIF()

SET(ut_configFileReader OFF CACHE BOOL "Build C++ Unit test for ${PROJ}Acquisition Module")
IF(ut_configFileReader)
  ADD_EXECUTABLE(ut_configFileReader
                 ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/UnitTest_ConfigFileReader.cpp
                 ${${PROJ}_ACQUISITION_Utilities_SRCS}
                 )
  SET_TARGET_PROPERTIES(ut_configFileReader PROPERTIES  COMPILE_FLAGS "-DVISTA_PLUGIN_FRAMEWORK")
  TARGET_LINK_LIBRARIES(ut_configFileReader spinvistaAcquisition ${${PROJ}_EXT_LIBS})
  install (TARGETS ut_configFileReader   DESTINATION	 ${CMAKE_INSTALL_PREFIX})  
ENDIF()


