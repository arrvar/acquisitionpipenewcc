#define PY_ARRAY_UNIQUE_SYMBOL pbcvt_ARRAY_API

#include <vector>
#include <string>
#include <memory>
#include <tuple>
#include <iomanip>
#include <sstream>

#include <boost/python.hpp>
#include <boost/python/tuple.hpp>

#include <QDateTime>
#include <QElapsedTimer>
#include <QDir>
#include <QMetaType>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionInterface.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Framework/HardwareManager.h"
#include "Common/Framework/HardwareManager/Framework/IAcquisitionEventListener.h"
#include "Common/Framework/HardwareManager/Framework/ISystemConfigReader.h"
#include "Common/Framework/HardwareManager/Framework/StandardConfigReader.h"
#include "Common/Framework/HardwareManager/Framework/StandardDatabaseManager.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"

#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"
#include "Common/Framework/HardwareManager/Utils/Types.h"
#include "Common/Framework/HardwareManager/Utils/TriggerBestZ.h"
#include "Common/Framework/HardwareManager/Utils/GetFocusPlane.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/ZTriggerPipeline.h"
#include "Common/Framework/Logger/StandardLogger.h"

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionInterface.h"

#include <pyboostcvconverter/pyboostcvconverter.hpp>

namespace pbcvt
{
  using namespace boost::python;

  /**
   * @brief The SnapXYThread class is a thread class for capturing images
   * in XY-direction in trigger mode.
   */
  class SnapXYThread: public QThread
  {
    public:
    /**
    * @brief init To initialize the capture thread.
    * @param acqStackSize No. of images to capture in a stack.
    */
    void init(int acqStackSize)
    {
      m_imageStack.resize(acqStackSize);
    }

    /**
     * @brief m_err To store the capture status of the thread. True indicates
     * successful capture and false indicates error in capturing.
     */
    bool m_err;

    /**
     * @brief m_imageStack Vector container to store stack of images.
     */
    QVector <cv::Mat> m_imageStack;

  protected:
    /**
     * @brief run Reimplemented method that needs to run in the thread.
     */
    void run()
    {
      m_err = false;
      for(int idx = 0; idx < m_imageStack.size(); idx++)
      {
        void* _imgPtr = HW_MANAGER()->snapImage();

        if(_imgPtr != NULL)
        {
          cv::Mat inputImage(cv::Size(spin::Constants::FRAME_WIDTH,
                                      spin::Constants::FRAME_HEIGHT),
                                      CV_8UC4, _imgPtr);

          m_imageStack[idx] = inputImage.clone();
        }
        else
        {
          m_err = true;
          SPIN_LOG_ERROR(QObject::tr("Snap missed at index %1 "
                                     "of total %2").arg(idx)
                                                   .arg(m_imageStack.size()));
          break;
        }
      }
    }
  };// end of SnapXYThread class.

  /**
   * @brief The SnapXYThread class is a thread class for capturing images
   * in XY-direction in trigger mode.
   */
  class SnapZStackQcqThread: public QThread
  {
    public:
    /**
    * @brief init To initialize the capture thread.
    * @param acqStackSize No. of images to capture in a stack.
    */
    void init(int count, int stackCount)
    {

      m_imageZStack.resize(stackCount);
      m_imageXStack.resize(count);
    }

    /**
     * @brief m_err To store the capture status of the thread. True indicates
     * successful capture and false indicates error in capturing.
     */
    bool m_err;

    /**
     * @brief m_imageStack Vector container to store stack of images.
     */
    QVector <cv::Mat> m_imageZStack;
    QVector <cv::Mat> m_imageXStack;

  protected:
    /**
     * @brief run Reimplemented method that needs to run in the thread.
     */
    void run()
    {
      const unsigned buffSize = spin::Constants::FRAME_WIDTH *
          spin::Constants::FRAME_HEIGHT * 4;
      const int Remove_Images_From_Stack =CONFIG()->realConfig(
            spin::ConfigKeys::REMOVE_IMAGES_FROM_STACK);
      SPIN_LOG_ERROR(QObject::tr("entered capture in thread run"));
      m_err = false;
      for(int idx = 0; idx < m_imageXStack.size(); idx++)
      {
        // Capture the image and check for NULL.
        void* _imgPtr = HW_MANAGER()->snapImage();
        if(_imgPtr != Q_NULLPTR)
        {
          unsigned char* imgBuff = new unsigned char[buffSize];
          memcpy(imgBuff, _imgPtr, buffSize);

          const int stackSize = m_imageZStack.size();
          for(int stackIdx = 0; stackIdx < stackSize;
              stackIdx++)
          {
            void* _stackImgPtr = HW_MANAGER()->snapImage();
            if(_stackImgPtr != Q_NULLPTR)
            {
              if((stackIdx >= Remove_Images_From_Stack) &&
                 (stackIdx < stackSize))
              {
                unsigned char* stackImgBuff = new unsigned char[buffSize];
                memcpy(stackImgBuff, _stackImgPtr, buffSize);
              }
            }
            else
            {
              m_err = true;
              SPIN_LOG_ERROR(QObject::tr("Snap missed at stack trigger %1 of "
                                   "total %2 of AoI index %3").
                  arg(stackIdx + 1).
                  arg(m_imageZStack.size()).
                  arg(idx));
              break;
            }
          }
        }
        else
        {
          m_err = true;
          SPIN_LOG_ERROR(QObject::tr("Snap missed at index %1 of total %2").
              arg(idx).arg(m_imageXStack.size()));
          break;
        }
//      for(int idx = 0; idx < m_imageZStack.size(); idx++)
//      {
//        void* _imgPtr = HW_MANAGER()->snapImage();

//        if(_imgPtr != NULL)
//        {
//          cv::Mat inputImage(cv::Size(spin::Constants::FRAME_WIDTH,
//                                      spin::Constants::FRAME_HEIGHT),
//                                      CV_8UC4, _imgPtr);

//          m_imageZStack[idx] = inputImage.clone();
//        }
//        else
//        {
//          m_err = true;
//          SPIN_LOG_ERROR(QObject::tr("Snap missed at index %1 "
//                                     "of total %2").arg(idx)
//                                                   .arg(m_imageZStack.size()));
//          break;
//        }
//      }
//      SPIN_LOG_ERROR(QObject::tr("after capture"));
    }
    }
  }; // end of SnapZStackQcqThread class

  /**
   * @brief The HardwareManagerInterface class is an interface class between
   * Python and C++.
   */
  class HardwareManagerInterface
  {
    public:
      /**
       * @brief Initialize Initializes the application resources.
       * @param appPath Absolute path from where the application runs.
       * @return Returns 0 on successful initialization and -1 if it fails.
       */
      int Initialize(std::string appPath)
      {
        // Store the path till AppContext is initialized, after which this path
        // is set as attribute in AppContext.
        spin::AppContext::get().setInstallationPath(appPath);

        QString logFileName = QString("spin_vista") + QString(".log");
        QString logFilePath = spin::FileSystemUtil::
                getPathFromInstallationPath("etc/log_files/" + logFileName);

        // Set the application logging targets.
        spin::AbstractLogger* logger = new spin::StandardLogger();
        logger->setLogLevel(spin::Log_Debug);
        logger->addTarget(new spin::ConsoleTarget());
        logger->addTarget(new spin::FileTarget(logFilePath));

        spin::AppContext::get().setLogger(logger);

        // Create a shared write buffer and add it to the application context.
        spin::SharedImageBuffer* sharedWriteBuff =
            new spin::SharedImageBuffer();
        spin::AppContext::get().setSharedWriteBuffer(sharedWriteBuff);

        // Create a shared process buffer and add it to the application context.
        spin::SharedProcessBuffer* sharedProcessBuff =
            new spin::SharedProcessBuffer();
        spin::AppContext::get().setSharedProcessBuffer(sharedProcessBuff);

        int result = 0;
        if (!initComponents())
        {
          // Application failed to start.
          result = -1;
        }

        return result;
      }//end of function

      /**
       * @brief Connect To connect to the hardware.
       * @param magType Magnification type.
       * @return Returns 0 on successful initialization and -1 if it fails.
       */
      int Connect(int magType)
      {
        if(HW_MANAGER()->connect((spin::Magnification)magType))
        {
          HW_MANAGER()->setContinuousMode(spin::Z_TRIGGER);

          return 0;
        }
        else
        {
          return -1;
        }
      }

      /**
       * @brief ConfigureSlot To configure the hardware to a particular slot.
       * @param slotNum Slot number.
       * @return Returns 0 on successful initialization and -1 if it fails.
       */
      int ConfigureSlot(int slotNum)
      {
        if(spin::Utilities::loadSlotParams(slotNum))
        {
          return 0;
        }
        else
        {
          return -1;
        }
      }

      /**
       * @brief GetZPosition To get the current Z-Stage position.
       * @return Returns the current Z-Stage position in microns.
       */
      double GetZPosition()
      {
        if(HW_MANAGER()->isConnected())
        {
          return HW_MANAGER()->getZPosition();
        }
        else
        {
          return -1;
        }
      }

      /**
       * @brief SetZPosition To set the position of the Z-Stage.
       * @param zPos Z-Stage position in microns.
       * @param wait True indicates it is a synchronous call and false indicates
       * that it is asynchronous.
       */
      void SetZPosition(double zPos, int wait)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setZPosition(zPos, (wait == 1 ? true : false));
        }
      }

      /**
       * @brief IsConnected To check is a connection to the hardware has been
       * established.
       * @return Returns 0 on successful initialization and -1 if it fails.
       */
      int IsConnected()
      {
        if(HW_MANAGER()->isConnected())
        {
          return 0;
        }
        else
        {
          return -1;
        }
      }

      /**
       * @brief SetObjective To move the objective turret to the given
       * magnification.
       * @param magType Given magnification value.
       */
      void SetObjective(int magType)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setObjective((spin::Magnification)magType);
          // Set the exposure value of the camera for the given magnification.
          qreal camExpVal = spin::Utilities::
              getCamExpValue((spin::Magnification)magType);
          HW_MANAGER()->setCameraExposure(camExpVal);
        }
      }

      /**
       * @brief GetXYPosition To get the current XY-Stage position relative to
       * the configured slot.
       * @return Returns the current XY-Stage position in microns.
       */
      boost::python::tuple GetXYPosition()
      {
        if(HW_MANAGER()->isConnected())
        {
          std::cout<<"HW_MANAGER()->isConnected()"<<HW_MANAGER()->isConnected()<<std::endl;
          double xPos = 0, yPos = 0;
          HW_MANAGER()->getXYPosition(xPos, yPos);
          return boost::python::make_tuple(xPos, yPos);
        }
        else
        {
          return boost::python::make_tuple(0, 0);
        }
      }

      /**
       * @brief SetXYPosition To set the position of the XY-Stage relative to
       * the configured slot.
       * @param xPos X-Stage position in microns.
       * @param yPos Y-Stage position in microns.
       * @param wait True indicates it is a synchronous call and false indicates
       * that it is asynchronous.
       */
      void SetXYPosition(double xPos, double yPos, int wait)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setXYPosition(xPos, yPos, (wait == 1) ? true : false);
        }
      }

      /**
       * @brief GetAbsoluteXYPosition To get the current absolute position of
       * the XY-Stage.
       * @return Returns the current absolute position of the XY-Stage in
       * microns.
       */
      boost::python::tuple GetAbsoluteXYPosition()
      {
        if(HW_MANAGER()->isConnected())
        {
          double xPos = 0, yPos = 0;
          HW_MANAGER()->getAbsoluteXYPosition(xPos, yPos);
          return boost::python::make_tuple(xPos, yPos);
        }
        else
        {
          return boost::python::make_tuple(0, 0);
        }
      }

      /**
       * @brief SetAbsoluteXYPosition To set the absolute position of the
       * XY-Stage.
       * @param xPos X-Stage position in microns.
       * @param yPos Y-Stage position in microns.
       * @param wait True indicates it is a synchronous call and false indicates
       * that it is asynchronous.
       */
      void SetAbsoluteXYPosition(double xPos, double yPos, int wait)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setAbsoluteXYPosition(xPos, yPos,
                                              (wait == 1) ? true : false);
        }
      }

      /**
       * @brief SetCameraExposure To set the exposure value of the camera.
       * @param camExpVal Exposure value of the camera in milliseconds.
       */
      void SetCameraExposure(double camExpVal)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setCameraExposure(camExpVal);
        }
      }

      /**
       * @brief SetLedOff To turn OFF the LED.
       */
      void SetLedOff()
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setLedOff();
        }
      }

      /**
       * @brief SetLedOn To turn ON the LED.
       */
      void SetLedOn()
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setLedOn();
        }
      }

      /**
       * @brief CheckPosition To check if the stage is at the origin of the
       * configured slot.
       */
      void CheckPosition()
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->checkPosition();
        }
      }

      /**
       * @brief Process To capture a Z-Stack and find the best image in the
       * stack.
       * @param xPos X-Stage position in microns at which the stack has to be
       * acquired.
       * @param yPos Y-Stage position in microns at which the stack has to be
       * acquired.
       * @param startZ Lower limit of the Z-Stage in microns from where the
       * Z-Stack has to be captured.
       * @param stopZ Upper limit of the Z-Stage in microns upto where the
       * Z-Stack has to be captured.
       * @param focusStepSize Z-Stage step size in microns.
       * @param specType Specimen type.
       * @param slideNameStr Name of the slide.
       * @return Returns the Z-Stage value, region type (if blood smear),
       * focus metric value and the path where the image is written.
       */
      boost::python::tuple Process(double xPos, double yPos,
                                   double startZ, double stopZ,
                                   double focusStepSize,
                                   int specType, std::string slideNameStr)
      {
        if((startZ < stopZ) && HW_MANAGER()->isConnected())
        {
          QString slideName(QString::fromStdString(slideNameStr));
          spin::ZTriggerPipeline zTrigObj;
          zTrigObj.init((spin::SpecimenType)specType, startZ, stopZ,
                        focusStepSize, xPos, yPos, slideName);
          std::tuple<double, int, double, std::string> metricInfo =
              zTrigObj.process();
          SPIN_LOG_INFO(QObject::tr("Z Value: %1").
                        arg(std::get<0>(metricInfo)));
          return boost::python::make_tuple(std::get<0>(metricInfo),
                                           std::get<1>(metricInfo),
                                           std::get<2>(metricInfo),
                                           std::get<3>(metricInfo));
        }
        else
        {
          std::string imgPath;
          SPIN_LOG_INFO("Z Value: -1");
          return boost::python::make_tuple(-1, -1, -1, imgPath);
        }
      }

      /**
       * @brief ProcessFocusPlane To capture a Z-Stack and find the best focus
       * for starting focus sampling.
       * @param xPos X-Stage position in microns at which the stack has to be
       * acquired.
       * @param yPos Y-Stage position in microns at which the stack has to be
       * acquired.
       * @param startZ Lower limit of the Z-Stage in microns from where the
       * Z-Stack has to be captured.
       * @param stopZ Upper limit of the Z-Stage in microns upto where the
       * Z-Stack has to be captured.
       * @param focusStepSize Z-Stage step size in microns.
       * @return Returns best focus in the captured stack of images.
       */
      double ProcessFocusPlane(double xPos, double yPos,
                               double startZ, double stopZ,
                               double focusStepSize,
                               int specType)
      {
        SPIN_LOG_INFO(QObject::tr("X Pos: %1, Y Pos: %2, Start Z: %3, "
                                  "Stop Z: %4, Step Size: %5").
                      arg(xPos).arg(yPos).arg(startZ).
                      arg(stopZ).arg(focusStepSize));
        if(HW_MANAGER()->isConnected())
        {
          if(HW_MANAGER()->isConnected())
          {
            spin::GetFocusPlane gfp;

            // Move to location.
            HW_MANAGER()->setXYPosition(xPos, yPos, true);

            // Run Z-stack.
            double zStageSpeed = CONFIG()->realConfig(spin::ConfigKeys::
                                                      DEF_Z_STAGE_SPEED);
            gfp.init(startZ, stopZ, focusStepSize, zStageSpeed, specType);
            double bestZ = gfp.onStartCapture();
            if (bestZ>0)
            {
              HW_MANAGER()->setZPosition(bestZ, true);
            }
            SPIN_LOG_INFO(QObject::tr("Z Value: %1").arg(bestZ));
            return bestZ;
          }
        }
        SPIN_LOG_INFO("Z Value: -1");
        return -1;
      }

      double ProcessZStackAcq(double xPosStart, double yPosStart,
                              double xPosStop, double yPosStop,
                              double xStepSize, int count, double xySpeed,
                              int stackCount, std::string command)
      {
        if(HW_MANAGER()->isConnected())
        {
          SnapZStackQcqThread snapThread;
          snapThread.init(count, stackCount);

          double normalSpeed = HW_MANAGER()->getXYStageSpeed();
          double zStageSpeed = CONFIG()->realConfig(spin::ConfigKeys::
                                                    DEF_Z_STAGE_SPEED);
          HW_MANAGER()->setXYPosition(xPosStart,
                                      yPosStart, true);

          HW_MANAGER()->setZStageSpeed(zStageSpeed);
          HW_MANAGER()->setXYStageSpeed(xySpeed);

          // Setup camera for continuous mode
          HW_MANAGER()->setContinuousMode(spin::XY_TRIGGER);

          // Enable trigger mode
          double stageStepSize = HW_MANAGER()->getXYStageStepSize();
          HW_MANAGER()->enableXYTriggerMode(xStepSize / stageStepSize,
                                            spin::X_TRIGGER);
          HW_MANAGER()->sendDirectCommand(command);
          snapThread.start(QThread::TimeCriticalPriority);
          // Run Z-stack.
          HW_MANAGER()->setXYPosition(xPosStop,
                                      yPosStop, true);
          snapThread.wait();
          SPIN_LOG_ERROR(QObject::tr("after snapthread wait"));
          if(snapThread.m_err == false)
          {
            SPIN_LOG_ERROR(QObject::tr("writing images"));
            for(int idx = 0 ; idx < snapThread.m_imageZStack.size(); idx++)
            {
              QString fileName;
              fileName.sprintf("img_%d.bmp", idx);
              cv::imwrite(fileName.toStdString().c_str(),
                          snapThread.m_imageZStack[idx]);
            }
          }

          HW_MANAGER()->disableTriggerMode();
          HW_MANAGER()->stopContinuousMode();
          HW_MANAGER()->setXYStageSpeed(normalSpeed);
        }
        else
        {
          return -1;
        }
        return 0;
      }

      // Run an XY stack.
      double ProcessXY(double xPosStart, double yPosStart, double xStepSize,
                       int count, double xySpeed, double startOffset)
      {
        if(HW_MANAGER()->isConnected())
        {
          SnapXYThread snapThread;
          snapThread.init(count);

          // Move to location.
          HW_MANAGER()->setXYPosition(xPosStart -  startOffset,
                                      yPosStart, true);

          double normalSpeed = HW_MANAGER()->getXYStageSpeed();
          double zStageSpeed = CONFIG()->realConfig(spin::ConfigKeys::
                                                    DEF_Z_STAGE_SPEED);
          HW_MANAGER()->setZStageSpeed(zStageSpeed);
          HW_MANAGER()->setXYStageSpeed(xySpeed);

          // Setup camera continuous capture.
          HW_MANAGER()->setContinuousMode(spin::XY_TRIGGER);

          // Enable trigger mode
          double stageStepSize = HW_MANAGER()->getXYStageStepSize();
          HW_MANAGER()->enableXYTriggerMode(xStepSize / stageStepSize,
                                            spin::X_TRIGGER);
          // Set continous mode
          // Start the Capture Thread and wait for Trigger
          snapThread.start(QThread::TimeCriticalPriority);
          // Run Z-stack.
          HW_MANAGER()->setXYPosition(xPosStart+double((count+1)*xStepSize),
                                      yPosStart, true);
          snapThread.wait();
          if(snapThread.m_err == false)
          {
            for(int idx = 0 ; idx < snapThread.m_imageStack.size(); idx++)
            {
              QString fileName;
              fileName.sprintf("img_%d.bmp", idx);
              cv::imwrite(fileName.toStdString().c_str(),
                          snapThread.m_imageStack[idx]);
            }
          }
          HW_MANAGER()->disableTriggerMode();
          HW_MANAGER()->stopContinuousMode();
          HW_MANAGER()->setXYStageSpeed(normalSpeed);
        }
        else
        {
          return -1;
        }
        return 0;
      }

      // Disconnect from the hardware.
      int Disconnect()
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->stopContinuousMode();
          if(!HW_MANAGER()->disconnect())
          {
            return -1;
          }
        }

        return 0;
      }

      // Send stage to home position.
      void Home()
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->home();
        }
      }

      // Set LED brightness.
      void SetLedBrightness(double brightnessValue)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setLedBrightness(brightnessValue);
        }
      }

      // Set 1x LED brightness.
      void SetOneXLedBrightness(double brightnessValue)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setOneXLedBrightness(brightnessValue);
        }
      }

      // Snap image.
      int SnapImage(std::string imgPath)
      {
        if(HW_MANAGER()->isConnected())
        {
          void* _imgPtr = HW_MANAGER()->snapImage();
          if(_imgPtr != Q_NULLPTR)
          {
            cv::Mat inputImage(cv::Size(spin::Constants::FRAME_WIDTH,
                                        spin::Constants::FRAME_HEIGHT), CV_8UC4,
                               _imgPtr);
            cv::imwrite(imgPath, inputImage);

            return 0;
          }
        }

        return -1;
      }

      void SetZBacklash(double backlashValue)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setZBacklash(backlashValue);
        }
      }

      void SetPropertyStr(std::string devName, std::string propName,
                          std::string propValue)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setPropertyStr(devName, propName, propValue);
        }
      }

      void SetPropertyInt(std::string devName, std::string propName,
                          long propValue)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setPropertyInt(devName, propName, propValue);
        }
      }

      void SetPropertyReal(std::string devName, std::string propName,
                           double propValue)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setPropertyReal(devName, propName, propValue);
        }
      }

      int StartAcq(int slotNum, int magType, int specimenType,
                   std::string slideName, std::string gridName)
      {
        int result = -1;
        m_acqInterface = new spin::acquisition::AcquisitionInterface();
        if(m_acqInterface->Initialize(slotNum, magType, specimenType,
                                      QString::fromStdString(slideName),
                                      QString::fromStdString(gridName), true))
        {
          m_acqInterface->Process();
          result = 0;
        }

        return result;
      }

      void CompleteAcq()
      {
        delete m_acqInterface;
        m_acqInterface = Q_NULLPTR;
      }

    private:
      bool initComponents()
      {
        // Prepare system configuration.
        spin::SystemConfig* config = spin::SystemConfig::get();
        // Read configuration.
        spin::ISystemConfigReader* reader = new spin::StandardConfigReader();
        bool result = true;
        if(!reader->readConfig(config))
        {
          SPIN_LOG_FATAL("Error occured while reading system configuration.");
          result = false;
        }
        else
        {
          // Create database manager.
          spin::IDatabaseManager* dbMan = new spin::StandardDatabaseManager();

          // Create hardware manager.
          spin::HardwareManager* hwMan = new spin::HardwareManager();

          result = spin::AppContext::init(dbMan, hwMan, config);

          m_acqInterface = Q_NULLPTR;
          
          qRegisterMetaType<QList<int>>("QList<int>");
          qRegisterMetaType<QList<double>>("QList<double>");

        }
        delete reader;
        return result;
      }

      spin::acquisition::AcquisitionInterface* m_acqInterface;

  };//end of class

  // The boost-python wrapper
  BOOST_PYTHON_MODULE(py_spinHardwareManager)
  {
    boost::python::class_<HardwareManagerInterface, boost::noncopyable>("HardwareManagerInterface")
      .def("Initialize", &HardwareManagerInterface::Initialize)
      .def("Connect", &HardwareManagerInterface::Connect)
      .def("ConfigureSlot", &HardwareManagerInterface::ConfigureSlot)
      .def("Process", &HardwareManagerInterface::Process)
      .def("ProcessFocusPlane", &HardwareManagerInterface::ProcessFocusPlane)
      .def("ProcessXY", &HardwareManagerInterface::ProcessXY)
      .def("ProcessZStackAcq", &HardwareManagerInterface::ProcessZStackAcq)
      .def("GetZPosition", &HardwareManagerInterface::GetZPosition)
      .def("SetZPosition", &HardwareManagerInterface::SetZPosition)
      .def("GetXYPosition", &HardwareManagerInterface::GetXYPosition)
      .def("SetXYPosition", &HardwareManagerInterface::SetXYPosition)
      .def("GetAbsoluteXYPosition", &HardwareManagerInterface::GetAbsoluteXYPosition)
      .def("SetAbsoluteXYPosition", &HardwareManagerInterface::SetAbsoluteXYPosition)
      .def("SetObjective", &HardwareManagerInterface::SetObjective)
      .def("SetCameraExposure", &HardwareManagerInterface::SetCameraExposure)
      .def("CheckPosition", &HardwareManagerInterface::CheckPosition)
      .def("SetLedOff", &HardwareManagerInterface::SetLedOff)
      .def("SetLedOn", &HardwareManagerInterface::SetLedOn)
      .def("SetLedBrightness", &HardwareManagerInterface::SetLedBrightness)
      .def("SetOneXLedBrightness", &HardwareManagerInterface::SetOneXLedBrightness)
      .def("SetPropertyStr", &HardwareManagerInterface::SetPropertyStr)
      .def("SetPropertyInt", &HardwareManagerInterface::SetPropertyInt)
      .def("SetPropertyReal", &HardwareManagerInterface::SetPropertyReal)
      .def("SnapImage", &HardwareManagerInterface::SnapImage)
      .def("Home", &HardwareManagerInterface::Home)
      .def("IsConnected", &HardwareManagerInterface::IsConnected)
      .def("Disconnect", &HardwareManagerInterface::Disconnect)
      .def("StartAcq", &HardwareManagerInterface::StartAcq)
      .def("CompleteAcq", &HardwareManagerInterface::CompleteAcq);
  };//end of function
}//end of namespace
