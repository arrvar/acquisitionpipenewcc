#define PY_ARRAY_UNIQUE_SYMBOL pbcvt_ARRAY_API
#include <vector>
#include <string>
#include <memory>
#include <boost/python.hpp>
//#include <boost/python/numpy.hpp>
#include <boost/python/tuple.hpp>
#include <tuple>
#include <QDateTime>
#include <QElapsedTimer>
#include <QDir>
#include <iomanip>
#include <sstream>

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionInterface.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Framework/HardwareManager.h"
#include "Common/Framework/HardwareManager/Framework/IAcquisitionEventListener.h"
#include "Common/Framework/HardwareManager/Framework/ISystemConfigReader.h"
#include "Common/Framework/HardwareManager/Framework/StandardConfigReader.h"
#include "Common/Framework/HardwareManager/Framework/StandardDatabaseManager.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"

#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"
#include "Common/Framework/HardwareManager/Utils/Types.h"
#include "Common/Framework/HardwareManager/Utils/TriggerBestZ.h"
#include "Common/Framework/HardwareManager/Utils/GetFocusPlane.h"
#include "Common/Framework/HardwareManager/Utils/Constants.h"
#include "Common/Framework/HardwareManager/Utils/ZTriggerPipeline.h"
#include "Common/Framework/AutoFocus/AutoFocus.h"
#include "Common/Framework/Logger/StandardLogger.h"

//#error qmetatype.h must be included before any header file that defines Bool (if not done this way)???

//#include <numpy/ndarrayobject.h>
//#include <numpy/noprefix.h>
#include <pyboostcvconverter/pyboostcvconverter.hpp>

namespace pbcvt
{
  using namespace boost::python;
class SnapXYThread: public QThread
{
    public:
    /**
    * @brief SnapXYThread Default class constructor, initializes objects
    * owned by the class.
    * @param parent
    */
    SnapXYThread(QObject* parent = 0)
    {

    }

    /**
    * @brief ~SnapXYThread Default class destructor, destroys objects owned
    * by the class.
    */
    ~SnapXYThread()
    {

    }

    /**
    * @brief init To initialize the worker with focusStepSize and normalSpeed
    * internally this should handle trigger capture movement speed and return back with a normalSpeed.
    * @param focusStepSize distance between two captures(um).
    * @param normalSpeed starting position of Z to capture(mm/s).
    * @param acqStackSize for how many images to capture.
    */
    void init(int acqStackSize)
    {
        m_acqStackSize = acqStackSize;
        m_imageStack.resize(acqStackSize);
    }

    std::vector<cv::Mat>m_imageStack;
    bool m_err;

protected:
    void run()
    {
        m_err = false;
        ////std::cout<<"Thread Running"<<std::endl;
        for(int idx = 0; idx < m_acqStackSize; idx++)
        {
            void* _imgPtr = HW_MANAGER()->snapImage();

            if(_imgPtr != NULL)
            {
                cv::Mat inputImage(cv::Size(spin::Constants::FRAME_WIDTH,
                                            spin::Constants::FRAME_HEIGHT),
                                            CV_8UC4, _imgPtr);

                //cv::imwrite(std::to_string(idx) + ".bmp",inputImage);
                m_imageStack[idx] = inputImage.clone();
            }
            else
            {
                m_err = true;
                SPIN_LOG_ERROR(QObject::tr("Snap missed at index %1 "
                                           "of total %2").arg(idx)
                                                         .arg(m_acqStackSize)
                               );
                break;
            }
            _imgPtr = NULL;
        }
    }

private:
    int m_acqStackSize;
};//end of class

  /*! \brief an Interface class between python and c++*/
  class ZTriggerInterface
  {
    public:
      // Initialize
      int Initialize(std::string appPath)
      {
        // Store the path till AppContext is initialized, after which this path is
        // set as attribute in AppContext.
        spin::AppContext::get().setInstallationPath(appPath);

        QString timeStamp = QDateTime::currentDateTime().
                toString("yyyy_MM_dd-hh_mm_ss");
        QString logFileName = QString("spin_vista") + QString(".log");
        QString logFilePath = spin::FileSystemUtil::
                getPathFromInstallationPath("etc/log_files/" + logFileName);

        // Set the application logging targets.
        spin::AbstractLogger* logger = new spin::StandardLogger();
        logger->setLogLevel(spin::Log_Debug);
        logger->addTarget(new spin::ConsoleTarget());
        logger->addTarget(new spin::FileTarget(logFilePath));

        spin::AppContext::get().setLogger(logger);

        // Create a shared write buffer and add it to the application context.
        spin::SharedImageBuffer* sharedWriteBuff =
            new spin::SharedImageBuffer();
        spin::AppContext::get().setSharedWriteBuffer(sharedWriteBuff);

        // Create a shared process buffer and add it to the application context.
        spin::SharedProcessBuffer* sharedProcessBuff =
            new spin::SharedProcessBuffer();
        spin::AppContext::get().setSharedProcessBuffer(sharedProcessBuff);

        int result = 0;
        if (!initComponents())
        {
          // Application failed to start.
          result = -1;
        }

        return result;
      }//end of function

      // Connect to the hardware.
      int Connect(int slotNum, int magType)
      {
        // Load the slot parameters.
        if(spin::Utilities::loadSlotParams(slotNum))
        {
          if(HW_MANAGER()->connect((spin::Magnification)magType))
          {
            HW_MANAGER()->setObjective((spin::Magnification)magType);
            HW_MANAGER()->setContinuousMode(spin::Z_TRIGGER);

            return 0;
          }
          else
          {
            return -1;
          }
        }
        else
        {
          return -1;
        }
      }

      // Get Current Z Position
      double Position()
      {
        if(HW_MANAGER()->isConnected())
        {
          return HW_MANAGER()->getZPosition();
        }
        else
        {
          return -1;
        }
      }

      // Set Z Position.
      void SetZPosition(double zPos, int wait)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setZPosition(zPos, (wait == 1 ? true : false));
        }
      }

      // Set Z Position.
      int IsConnected()
      {
        if(HW_MANAGER()->isConnected())
        {
          return 1;
        }
        else
        {
          return 0;
        }
      }

      // Set Magnification.
      void SetMagnification(int magType)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setObjective((spin::Magnification)magType);
          // Set the exposure value of the camera for the given magnification.
          qreal camExpVal = spin::Utilities::
              getCamExpValue((spin::Magnification)magType);
          HW_MANAGER()->setCameraExposure(camExpVal);
        }
      }

      boost::python::tuple GetXYPosition()
      {
        if(HW_MANAGER()->isConnected())
        {
          double xPos = 0, yPos = 0;
          HW_MANAGER()->getXYPosition(xPos, yPos);
          return boost::python::make_tuple(xPos, yPos);
        }
        else
        {
          return boost::python::make_tuple(0, 0);
        }
      }

      void SetXYPosition(double xPos, double yPos, int wait)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setXYPosition(xPos, yPos, (wait == 1) ? true : false);
        }
      }

      void SetCameraExposure(double camExpVal)
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->setCameraExposure(camExpVal);
        }
      }

      void CheckPosition()
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->checkPosition();
        }
      }

      boost::python::tuple Process(double xPos, double yPos,
                                   double startZ, double stopZ,
                                   double focusStepSize,
                                   int specType, std::string slideNameStr)
      {
        SPIN_LOG_INFO(QObject::tr("X Pos: %1, Y Pos: %2, Start Z: %3, "
                                  "Stop Z: %4, Step Size: %5").
                      arg(xPos, 0, 'g', 10).
                      arg(yPos, 0, 'g', 10).
                      arg(startZ, -8, 'f', 3, '0').
                      arg(stopZ, -8, 'f', 3, '0').
                      arg(focusStepSize, 0, 'g', 10));
        if((startZ < stopZ) && HW_MANAGER()->isConnected())
        {
          double zPos = HW_MANAGER()->getZPosition();
          SPIN_LOG_INFO(QObject::tr("Current Z-Stage position: %1").
                        arg(zPos, -8, 'f', 3, '0'));
          QString slideName(QString::fromStdString(slideNameStr));
          spin::ZTriggerPipeline zTrigObj;
          zTrigObj.init((spin::SpecimenType)specType, startZ, stopZ,
                        focusStepSize, xPos, yPos, slideName);
          std::tuple<double, int, double, std::string> metricInfo =
              zTrigObj.process();
          SPIN_LOG_INFO(QObject::tr("Z Value: %1").
                        arg(std::get<0>(metricInfo), -8, 'f', 3, '0'));
          return boost::python::make_tuple(std::get<0>(metricInfo),
                                           std::get<1>(metricInfo),
                                           std::get<2>(metricInfo),
                                           std::get<3>(metricInfo));
        }
        else
        {
          std::string imgPath;
          SPIN_LOG_INFO("Z Value: -1");
          return boost::python::make_tuple(-1, -1, -1, imgPath);
        }
      }

      // Run a Z stack.
      boost::python::tuple Process_old(double xPos, double yPos,
                                   double startZ, double stopZ,
                                   double focusStepSize,
                                   int specType, std::string slideNameStr)
      {
        QElapsedTimer XYtimer;
        SPIN_LOG_INFO(QObject::tr("X Pos: %1, Y Pos: %2, Start Z: %3, "
                                  "Stop Z: %4, Step Size: %5").
                      arg(xPos).arg(yPos).arg(startZ).
                      arg(stopZ).arg(focusStepSize));
        if((startZ < stopZ) && HW_MANAGER()->isConnected())
        {
          QString slideName(QString::fromStdString(slideNameStr));
          spin::TriggerBestZ tbz(xPos, yPos);

          XYtimer.restart();
          // Move to location.
          HW_MANAGER()->setXYPosition(xPos, yPos, true);
          SPIN_LOG_INFO(QObject::tr("Set XY Time: %1").arg(XYtimer.elapsed()));

          // Run Z-stack.
          XYtimer.restart();
          double zStageSpeed = CONFIG()->realConfig(spin::ConfigKeys::
                                                    DEF_Z_STAGE_SPEED);
          tbz.init(startZ, stopZ, focusStepSize, zStageSpeed, specType);
          std::tuple<double,int,double,std::string> t =
              tbz.onStartCapture(slideName);
          SPIN_LOG_INFO(QObject::tr("Z Stack Function"
                                    "Time: %1").arg(XYtimer.elapsed()));
          if (std::get<0>(t)>0)
          {
            XYtimer.restart();
            HW_MANAGER()->setZPosition(std::get<0>(t), true);
            SPIN_LOG_INFO(QObject::tr("Set Z Time: %1").arg(XYtimer.elapsed()));
          }
          SPIN_LOG_INFO(QObject::tr("Z Value: %1").arg(std::get<0>(t)));
          return boost::python::make_tuple(std::get<0>(t),std::get<1>(t),
                                           std::get<2>(t),std::get<3>(t));
        }
        else
        {
          std::string imgPath;
          SPIN_LOG_INFO("Z Value: -1");
          return boost::python::make_tuple(-1,-1,-1,imgPath);
        }
      }
      // Run an XY stack.
      double ProcessXY(double xPosStart, double yPosStart, double xStepSize,
                       int count, double xySpeed, double startOffset)
      {
        if(HW_MANAGER()->isConnected())
        {
          SnapXYThread snapThread;
          snapThread.init(count);

          // Move to location.
          HW_MANAGER()->setXYPosition(xPosStart -  startOffset,
                                      yPosStart, true);
          HW_MANAGER()->setXYStageSpeed(xySpeed);
          // Enable trigger mode
          double stageStepSize = HW_MANAGER()->getXYStageStepSize();
          HW_MANAGER()->enableXYTriggerMode(xStepSize / stageStepSize,
                                            spin::X_TRIGGER);
          // Set continous mode
          HW_MANAGER()->setContinuousMode(spin::XY_TRIGGER);
          HW_MANAGER()->openEventCameraTimeLapse();
          // Start the Capture Thread and wait for Trigger
          snapThread.start(QThread::TimeCriticalPriority);
          // Run Z-stack.
          usleep(500);
          HW_MANAGER()->setXYPosition(xPosStart+double((count+1)*xStepSize),
                                      yPosStart, true);
          snapThread.wait();
          if(snapThread.m_err == false)
          {
              //std::cout<<"***********************************Image Stack: "<<snapThread.m_imageStack.size()<<std::endl;
              for(int idx = 0 ; idx < count; idx++)
              {
                std::ostringstream fileName;
                fileName << "triggerHTest_" << std::setw(5) << std::setfill('0') << idx << ".bmp";
                cv::Mat imgPtr = snapThread.m_imageStack[idx];
                cv::imwrite(fileName.str(), imgPtr.clone());
              }
              double v = HW_MANAGER()->getCameraTimeLapse();
              std::cout<<"Camera Delay: "<<v/1E+6<<"(ms)"<<std::endl;
              HW_MANAGER()->closeEventCameraTimeLapse();
          }
        }
        else
        {
          return -1;
        }
        return 0;
      }

      // Run for focus plane.
      double ProcessFocusPlane(double xPos, double yPos,
                               double startZ, double stopZ,
                               double focusStepSize,
                               int specType)
      {
        SPIN_LOG_INFO(QObject::tr("X Pos: %1, Y Pos: %2, Start Z: %3, "
                                  "Stop Z: %4, Step Size: %5").
                      arg(xPos, 0, 'g', 10).
                      arg(yPos, 0, 'g', 10).
                      arg(startZ, -8, 'f', 3, '0').
                      arg(stopZ, -8, 'f', 3, '0').
                      arg(focusStepSize, 0, 'g', 10));
        if((startZ < stopZ) && HW_MANAGER()->isConnected())
        {
          double zPos = HW_MANAGER()->getZPosition();
          SPIN_LOG_INFO(QObject::tr("Current Z-Stage position: %1").
                        arg(zPos, -8, 'f', 3, '0'));

          spin::GetFocusPlane gfp;

          // Move to location.
          HW_MANAGER()->setXYPosition(xPos, yPos, true);

          // Run Z-stack.
          double zStageSpeed = CONFIG()->realConfig(spin::ConfigKeys::
                                                    DEF_Z_STAGE_SPEED);
          gfp.init(startZ, stopZ, focusStepSize, zStageSpeed, specType);
          double bestZ = gfp.onStartCapture();
          if (bestZ>0)
          {
            HW_MANAGER()->setZPosition(bestZ, true);
          }
          SPIN_LOG_INFO(QObject::tr("Z Value: %1").
                        arg(bestZ, -8, 'f', 3, '0'));
          return bestZ;
        }
        else
        {
          SPIN_LOG_INFO("Z Value: -1");
          return -1;
        }
      }

      // Disconnect from the hardware.
      int Disconnect()
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->stopContinuousMode();
          if(!HW_MANAGER()->disconnect())
          {
            return -1;
          }
        }

        return 0;
      }

      int CaptureZStack(int magVal)
      {
        if(HW_MANAGER()->isConnected())
        {

          double zStepSize = spin::Utilities::
              getZStageStepSize(spin::Magnification(magVal));
          // Get current Z-Stage position.
          double zPos = HW_MANAGER()->getZPosition();
          double startZPos = zPos - 6;
          double stopZPos = zPos + 6;
          int zStackSize = int((stopZPos - startZPos) / zStepSize);
          // In mm/s.
          double zStageSpeed = CONFIG()->realConfig(spin::ConfigKeys::
                                                    DEF_Z_STAGE_SPEED);

          spin::SnapZThread snapThread;
          snapThread.init(zStepSize, zStageSpeed, zStackSize);
          snapThread.m_imageStack.resize(zStackSize);

          // Set the start Z position.
          HW_MANAGER()->setZPosition(startZPos, true);

          // Enable trigger mode.
          double zStepSizeInMicrons = CONFIG()->realConfig(spin::ConfigKeys::
                                                           STAGE_STEP_SIZE_Z);
          double zMicrostepping = CONFIG()->realConfig(spin::ConfigKeys::
                                                       STAGE_MICROSTEPPING_Z);
          HW_MANAGER()->enableZTriggerMode(zStepSize /
                                           (zStepSizeInMicrons /
                                            zMicrostepping));
          // Set continous mode.
          HW_MANAGER()->setContinuousMode(spin::Z_TRIGGER);

          // Start the capture thread and wait for trigger.
          snapThread.start(QThread::TimeCriticalPriority);

          // Go to end location to capture all images.
          HW_MANAGER()->setZPosition(stopZPos);

          // Wait till thread stops.
          snapThread.wait();

          // Disable trigger mode.
          HW_MANAGER()->disableTriggerMode();
          HW_MANAGER()->stopContinuousMode();

          if(snapThread.isRunning())
          {
            snapThread.quit();
          }

          while(snapThread.isRunning())
          {
            SPIN_LOG_DEBUG("Waiting for the trigger thread to stop.");
          }

          if(snapThread.m_err == true)
          {
            // Clear any existing OpenCV buffer.
            for(int idx = 0 ; idx < snapThread.m_imageStack.size(); idx++)
            {
              snapThread.m_imageStack[idx].release();
            }
            snapThread.m_imageStack.clear();
            snapThread.m_imageStack.resize(zStackSize);

            int idx = 0;
            double currZ = startZPos;
            while(currZ <= stopZPos)
            {
              HW_MANAGER()->setZPosition(currZ, true);

              void* _imgPtr = HW_MANAGER()->snapImage();

              if(_imgPtr != NULL)
              {
                cv::Mat inputImage(cv::Size(spin::Constants::FRAME_WIDTH,
                                            spin::Constants::FRAME_HEIGHT),
                                   CV_8UC4, _imgPtr);
                snapThread.m_imageStack[idx++] = inputImage.clone();
              }
              else
              {
                snapThread.m_imageStack[idx++] = cv::Mat::zeros(0, 0, CV_8UC4);
              }
              currZ += zStepSize;
            }
          }

          QDir etcDir(spin::FileSystemUtil::getPathFromInstallationPath("etc"));
          if(etcDir.exists("image_stack"))
          {
            etcDir.remove("image_stack");
          }
          etcDir.mkdir("image_stack");
          QString imagePath = spin::FileSystemUtil::
              getPathFromInstallationPath("etc/image_stack");
          for(int idx = 0 ; idx < snapThread.m_imageStack.size(); idx++)
          {
            QString imgName = "img_" + QString::number(idx + 1);
            imagePath = imagePath + "/" + imgName + ".jpeg";
            cv::imwrite(imagePath.toStdString(), snapThread.m_imageStack[idx]);
            snapThread.m_imageStack[idx].release();
          }
          snapThread.m_imageStack.clear();

          return 0;
        }
        else
        {
          return -1;
        }
      }

      void Home()
      {
        if(HW_MANAGER()->isConnected())
        {
          HW_MANAGER()->home();
        }
      }

    private:
      bool initComponents()
      {
        // Prepare system configuration.
        spin::SystemConfig* config = spin::SystemConfig::get();
        // Read configuration.
        spin::ISystemConfigReader* reader = new spin::StandardConfigReader();
        bool result = true;
        if(!reader->readConfig(config))
        {
          SPIN_LOG_FATAL("Error occured while reading system configuration.");
          result = false;
        }
        else
        {
          // Create database manager.
          spin::IDatabaseManager* dbMan = new spin::StandardDatabaseManager();

          // Create hardware manager.
          spin::HardwareManager* hwMan = new spin::HardwareManager();

          result = spin::AppContext::init(dbMan, hwMan, config);
        }
        delete reader;
        return result;
      }

  };//end of class

  // The boost-python wrapper
  BOOST_PYTHON_MODULE(py_spinZTrigger)
  {
    boost::python::class_<ZTriggerInterface, boost::noncopyable>("ZTriggerInterface")
      .def("Initialize", &ZTriggerInterface::Initialize)
      .def("Connect", &ZTriggerInterface::Connect)
      .def("Process", &ZTriggerInterface::Process)
      .def("ProcessXY", &ZTriggerInterface::ProcessXY)
      .def("ProcessFocusPlane", &ZTriggerInterface::ProcessFocusPlane)
      .def("Position", &ZTriggerInterface::Position)
      .def("SetZPosition", &ZTriggerInterface::SetZPosition)
      .def("GetXYPosition", &ZTriggerInterface::GetXYPosition)
      .def("SetXYPosition", &ZTriggerInterface::SetXYPosition)
      .def("SetMagnification", &ZTriggerInterface::SetMagnification)
      .def("SetCameraExposure", &ZTriggerInterface::SetCameraExposure)
      .def("CheckPosition", &ZTriggerInterface::CheckPosition)
      .def("Home", &ZTriggerInterface::Home)
      .def("IsConnected", &ZTriggerInterface::IsConnected)
      .def("Disconnect", &ZTriggerInterface::Disconnect);
  };//end of function
}//end of namespace
