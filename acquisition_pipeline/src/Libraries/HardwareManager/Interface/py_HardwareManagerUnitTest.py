import sys
from os.path import join, exists
from enum import Enum

from PyQt5.QtCore import QCoreApplication, QObject, QTimer, pyqtSlot

import py_spinHardwareManager as VistaAcquisition

class Magnification(Enum):
    MAG_UNKNOWN = -1

    MAG_HOME = 0

    MAG_20_X = 1

    MAG_40_X = 2

    MAG_100_X = 3

    MAG_10_X = 4
        

class AcquisitionPipeline():
    def __init__(self):
        self._acqPipeline = VistaAcquisition.HardwareManagerInterface()

    def InitPipeline(self, appPath):
        return self._acqPipeline.Initialize(appPath)
        
    def ConfigurePipeline(self, slotNum, magTtye, slideName, gridName):
        return self._acqPipeline.Configure(slotNum, magType, slideName, gridName)
        
    def ProcessPipeline(self):
        self._acqPipeline.Process()
        
    def CompletePipeline(self):
        self._acqPipeline.Completed()


class ParentClass(QObject):
    def __init__(self, parent=None):
        QObject.__init__(self, parent)
        
        self._appPath = appPath
        self._slideName = ""
        self._gridName = ""
        self._acqPipeline = AcquisitionPipeline()
        self._acqCheckTimer = QTimer(self)
        self._acqCheckTimer.setInterval(2000)
        self._acqCheckTimer.timeout.connect(self._onCheckStatus)
        
    @pyqtSlot()
    def _onCheckStatus(self):
        filePath = join(appPath, "workspaces", "acq_ws", self._slideName, self._gridName)
        postProcFilePath = join(filePath, "acq_post_proc.txt")
        if exists(postProcFilePath) is True:
            f = open(postProcFilePath, "r")
            outputStr = f.read()
            f.close()
            procStatus, statusMsg = outputStr.split(":")
            if procStatus == "completed":
                self._acqCheckTimer.stop()
                sys.exit(0)
        
    def startProcess(self, appPath, slotNum, magType, slideName, gridName):
        self._appPath = appPath
        self._slideName = slideName
        self._gridName = gridName
        ret = self._acqPipeline.InitPipeline(appPath)
        if ret == -1:
            print("Failed to initialize acquisition pipeline.")
            sys.exit(-1)
        
        ret = self._acqPipeline.ConfigurePipeline(slotNum, magType, slideName, gridName)
        if ret == -1:
            print("Failed to configure acquisition pipeline.");
            sys.exit(-1)

        self._acqCheckTimer.start()
        self._acqPipeline.ProcessPipeline()

if __name__ == "__main__":
    app = QCoreApplication(sys.argv)
    appPath = "/home/adminspin/office/spin_hemo_web_framework/branches/backend_new_acq"

    slotNum = 0
    slideName = ""
    magType = Magnification.MAG_20_X.value
    gridName = ""
    
    mainObj = ParentClass()
    mainObj.startProcess(appPath, slotNum, magType, slideName, gridName)    
   
    sys.exit(app.exec_())

