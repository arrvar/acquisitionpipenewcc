import sys
from os.path import join, exists
from enum import Enum

import py_spinHardwareManager as HmInterface

class Magnification(Enum):
    MAG_UNKNOWN = -1

    MAG_HOME = 0

    MAG_20_X = 1

    MAG_40_X = 2

    MAG_100_X = 3

    MAG_10_X = 4

class HardwareTest():
    def __init__(self, parent=None):       
        self._hwManagerInterface = HmInterface.HardwareManagerInterface()

    def connect(self, appPath, slotNum, magType):
        ret = self._hwManagerInterface.Initialize(appPath)
        if ret == -1:
            print("Failed to initialize pipeline.")
            sys.exit(-1)
        
        ret = self._hwManagerInterface.Connect(magType)
        if ret == -1:
            print("Failed to connect pipeline.");
            sys.exit(-1)

        configStatus = self._hwManagerInterface.ConfigureSlot(slotNum)
        if configStatus == -1:
            print("Failed to configure slot ", slotNum)
            self._hwManagerInterface.Disconnect()
            sys.exit(-1)

        self._hwManagerInterface.Home()
        self._hwManagerInterface.CheckPosition()
        self._hwManagerInterface.SetObjective(magType)
        self._hwManagerInterface.SetLedOn()

    def disconnect(self):
        self._hwManagerInterface.Home()
        self._hwManagerInterface.SetLedOff()
        self._hwManagerInterface.Disconnect()
            
    def testTrigger(self, xPos, yPos, startZ, stopZ, focusStepSize):
        bestZ = self._hwManagerInterface.Process(xPos, yPos, startZ, stopZ, focusStepSize, 0, "")
        print(bestZ)

    def testPlane(self, xPos, yPos, startZ, stopZ, focusStepSize):
        bestZ = self._hwManagerInterface.ProcessFocusPlane(xPos, yPos, startZ, stopZ, focusStepSize,0)
        print(bestZ)

    def testXY(self, xPosStart, yPosStart, xStepSize, imgCount, startOffset = 0):
        self._hwManagerInterface.ProcessXY(xPosStart, yPosStart, xStepSize, imgCount, startOffset)

if __name__ == "__main__":
    appPath = "/home/adminspin/spectral_insights/app"
    slotNum = 1
    slideName = "test_slide_1"
    magType = Magnification.MAG_20_X.value
    gridName = "grid_1"
    mainObj = HardwareTest()
    mainObj.connect(appPath, slotNum, magType)
    mainObj.testPlane(8531.25, 33125.0, 9200.0, 10200.0, 20)    
    mainObj.disconnect()
    sys.exit(0)

