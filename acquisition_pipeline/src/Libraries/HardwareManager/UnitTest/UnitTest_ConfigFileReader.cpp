#include <QCoreApplication>
#include <QStringList>
#include <QDebug>

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Framework/ISystemConfigReader.h"
#include "Common/Framework/HardwareManager/Framework/StandardConfigReader.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"

#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"
#include "Common/Framework/HardwareManager/Utils/Types.h"

#include "Common/Framework/Logger/StandardLogger.h"


bool initComponents()
{
  // Prepare system configuration.
  spin::SystemConfig* config = spin::SystemConfig::get();
  // Read configuration.
  spin::ISystemConfigReader* reader = new spin::StandardConfigReader();
  bool result = true;
  if(!reader->readConfig(config))
  {
    SPIN_LOG_FATAL("Error occured while reading system configuration.");
    result = false;
  }

  delete reader;
  return result;
}

int main(int argc, char *argv[])
{
  QCoreApplication app(argc, argv);
  QStringList argList = app.arguments();
  if(argList.size() != 2)
  {
    qDebug() << "Invalid arguments";
    qDebug() << argList.first();
    qDebug() << "<app_path>";
    exit(-1);
  }

  QString appPath(argList.at(1));

  qDebug() << "Application Path: " << appPath;

  // Store the path till AppContext is initialized, after which this path is
  // set as attribute in AppContext.
  spin::AppContext::get().setInstallationPath(appPath.toStdString());

  // Set the application logging targets.
  spin::AbstractLogger* logger = new spin::StandardLogger();
  logger->setLogLevel(spin::Log_Debug);
  logger->addTarget(new spin::ConsoleTarget());

  spin::AppContext::get().setLogger(logger);

  int result = 0;
  if(initComponents() == false)
  {
    result = -1;
  }

  return result;
}
