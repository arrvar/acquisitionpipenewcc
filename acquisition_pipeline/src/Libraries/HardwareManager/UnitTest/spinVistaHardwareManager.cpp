#include <QCoreApplication>
#include <QStringList>
#include <QMetaType>
#include <QDebug>

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Framework/HardwareManager.h"
#include "Common/Framework/HardwareManager/Framework/IAcquisitionEventListener.h"
#include "Common/Framework/HardwareManager/Framework/ISystemConfigReader.h"
#include "Common/Framework/HardwareManager/Framework/StandardConfigReader.h"
#include "Common/Framework/HardwareManager/Framework/StandardDatabaseManager.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"

#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/ConfigKeys.h"
#include "Common/Framework/HardwareManager/Utils/SharedImageBuffer.h"
#include "Common/Framework/HardwareManager/Utils/SharedProcessBuffer.h"
#include "Common/Framework/HardwareManager/Utils/Utilities.h"
#include "Common/Framework/HardwareManager/Utils/Types.h"

#include "Common/Framework/Logger/StandardLogger.h"

#include "Common/Framework/HardwareManager/Acquisition/AcquisitionInterface.h"

bool initComponents()
{
  // Prepare system configuration.
  spin::SystemConfig* config = spin::SystemConfig::get();
  // Read configuration.
  spin::ISystemConfigReader* reader = new spin::StandardConfigReader();
  bool result = true;
  if(!reader->readConfig(config))
  {
    SPIN_LOG_FATAL("Error occured while reading system configuration.");
    result = false;
  }
  else
  {
    // Create database manager.
    spin::IDatabaseManager* dbMan = new spin::StandardDatabaseManager();

    // Create hardware manager.
    spin::HardwareManager* hwMan = new spin::HardwareManager();

    result = spin::AppContext::init(dbMan, hwMan, config);
  }
  delete reader;
  return result;
//  return false;
}

int main(int argc, char *argv[])
{
  QCoreApplication app(argc, argv);
  QStringList argList = app.arguments();
  if(argList.size() != 7)
  {
    qDebug() << "Invalid arguments";
    qDebug() << argList.first();
    qDebug() << "<app_path> <slot_num> <magnification_type [1-20x,2-40x]> "
                "<specimen_type [0-Tissue Biopsy,2-Blood Smear]> "
                "<slide_name> <grid_name>";
    exit(-1);
  }

  qRegisterMetaType<QList<int>>("QList<int>");
  qRegisterMetaType<QList<double>>("QList<double>");

  QString appPath(argList.at(1));
  int slotNum = argList.at(2).toInt();
  int magType = argList.at(3).toInt();
  int specimenType = argList.at(4).toInt();
  QString slideName(argList.at(5));
  QString gridName(argList.at(6));

  qDebug() << "Application Path: " << appPath;
  qDebug() << "Slot Number: " << slotNum;
  qDebug() << "Magnification Type: " << magType;
  qDebug() << "Specimen Type: " << specimenType;
  qDebug() << "Slide Name: " << slideName;
  qDebug() << "Grid Name: " << gridName;

  // Store the path till AppContext is initialized, after which this path is
  // set as attribute in AppContext.
  spin::AppContext::get().setInstallationPath(appPath.toStdString());

  QString logFileName = QString("spin_vista") + QString(".log");
  QString logFilePath = spin::FileSystemUtil::
          getPathFromInstallationPath("etc/log_files/" + logFileName);

  // Set the application logging targets.
  spin::AbstractLogger* logger = new spin::StandardLogger();
  logger->setLogLevel(spin::Log_Debug);
  logger->addTarget(new spin::ConsoleTarget());
  logger->addTarget(new spin::FileTarget(logFilePath));

  spin::AppContext::get().setLogger(logger);

  // Create a shared write buffer and add it to the application context.
  spin::SharedImageBuffer* sharedWriteBuff =
      new spin::SharedImageBuffer();
  spin::AppContext::get().setSharedWriteBuffer(sharedWriteBuff);

  // Create a shared process buffer and add it to the application context.
  spin::SharedProcessBuffer* sharedProcessBuff =
      new spin::SharedProcessBuffer();
  spin::AppContext::get().setSharedProcessBuffer(sharedProcessBuff);

  int result = 0;
  spin::acquisition::AcquisitionInterface acqInterface;
  QObject::connect(&acqInterface,
                   &spin::acquisition::AcquisitionInterface::quitProcess,
                   &app, &QCoreApplication::quit);
  if (initComponents())
  {
    if(acqInterface.Initialize(slotNum, magType, specimenType,
                               slideName, gridName))
    {
      acqInterface.Process();

      return app.exec();
    }
    else
    {
      result = -1;
    }
  }
  else
  {
    result = -1;
  }

  return result;
}
