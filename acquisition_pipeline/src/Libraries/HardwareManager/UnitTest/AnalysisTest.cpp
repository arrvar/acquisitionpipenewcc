#include <QFile>
#include <QDebug>
#include <QString>
#include <QCoreApplication>

#include "Common/Framework/Logger/StandardLogger.h"

#include "Common/Framework/HardwareManager/Utils/Types.h"
#include "Common/Framework/HardwareManager/Utils/FileSystemUtil.h"
#include "Common/Framework/HardwareManager/Utils/AnalysisWorker.h"

#include "Common/Framework/HardwareManager/Framework/AppContext.h"
#include "Common/Framework/HardwareManager/Framework/SystemConfig.h"
#include "Common/Framework/HardwareManager/Framework/HardwareManager.h"
#include "Common/Framework/HardwareManager/Framework/StandardDatabaseManager.h"


int main(int argc, char *argv[])
{
    if (argc != 6) {
        qWarning() << "Incorrect arguments passed";
        qWarning() << "<executable> <app path> <workspace name> <slide name> <grid name> <analysis JSON file path>";
        return EXIT_FAILURE;
    }

    QCoreApplication app(argc, argv);
    QString appPath(static_cast<char*>(argv[1]));
    QString workspace(static_cast<char*>(argv[2]));
    QString slide(static_cast<char*>(argv[3]));
    QString grid(static_cast<char*>(argv[4]));
    QString jsonPath(static_cast<char*>(argv[5]));

    QFile *json = new QFile(jsonPath);
    QStringList grids = (QStringList() << QString("grid_1"));

    // Store the path till AppContext is initialized, after which this path is
    // set as attribute in AppContext.
    spin::AppContext::get().setInstallationPath(appPath.toStdString());

    QString timeStamp = QDateTime::currentDateTime().toString("yyyy_MM_dd-hh_mm_ss");
    QString logFileName = QString("spin_vista_") + timeStamp + QString(".txt");
    QString logFilePath = spin::FileSystemUtil::
          getPathFromInstallationPath("etc/log_files/" + logFileName);

    // Set the application logging targets.
    spin::AbstractLogger* logger = new spin::StandardLogger();
    logger->setLogLevel(spin::Log_Debug);
    logger->addTarget(new spin::ConsoleTarget());
    logger->addTarget(new spin::FileTarget(logFilePath));

    spin::AppContext::get().setLogger(logger);

    bool result = spin::AppContext::init(new spin::StandardDatabaseManager(),
                                         new spin::HardwareManager(),
                                         spin::SystemConfig::get());

    spin::AnalysisWorker analyser(workspace, slide, grids, json);
    analyser.init(spin::Magnification::MAG_20X);
    emit analyser.analysisStarted();

    return app.exec();
}
