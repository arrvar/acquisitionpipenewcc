#include <opencv2/opencv.hpp>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include "Utilities/ImageProcessing/ShapeUtilities/ShapeUtilities.h"
#include "AbstractClasses/AbstractMatrixTypes.h"

int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    //std::cout<<argv[0]<<" <inputFolder> <outputFolder> "<<std::endl;
    return -1;
  }

  std::string refImgFolder = std::string(argv[1]);
  std::string outFolder    = std::string(argv[2]);
  std::string outFileGC    = outFolder+"/gaussian_curvature.txt";
  std::string outFileCT    = outFolder+"/centroid_transform.txt";

  // We scan the ref image folder. It may contain either a single image or
  // multiple images.
  boost::filesystem::path dir(refImgFolder);
  if (!boost::filesystem::exists(dir))
  {
    return -1;
  }

  // Feature length specified by the user
  int length = 300;

  // Instantiate some variables
  spin::ShapeUtilities sUtils;
  //spin::RMMatrix_Double gcVals = spin::RMMatrix_Double::Zero(1,length);
  spin::RMMatrix_Double ctVals;// = spin::RMMatrix_Double::Zero(1,length);

  // Now, for each image in the folder we run the pipeline
  auto count = 0;
  boost::filesystem::directory_iterator it(refImgFolder), eod;
  BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))
  {
    if(boost::filesystem::is_regular_file(p))
    {
      // Get the name of the image
      std::string refImg = p.string();
      std::vector<std::string> temp;
      boost::split(temp, refImg, boost::is_any_of("/"));
      std::string imgName = temp[temp.size()-1];
      // Read the input image
      cv::Mat img = cv::imread(refImg);

      //std::cout<<"Processing: "<<refImg<<std::endl;
      // Create the output image
      std::string outImg = outFolder+"/"+imgName;
      // Create a std::map to hold the features
      std::map<int,spin::RMMatrix_Double> features;
      // Run the pipeline
      int h = sUtils.Compute1DFeatures(&img, &features, length, outImg);
      if (h !=1) return -1;
      // Populate the feature matrices
      if (count == 0)
      {
        //std::cout<<"here? "<<std::endl;
        //gcVals = features[0];
        ctVals = features[1];
      }
      else
      {
        //spin::RMMatrix_Double temp = gcVals;
        //gcVals.resize(temp.rows()+1,temp.cols());
        //gcVals<<temp,features[0];
        spin::RMMatrix_Double temp = ctVals;
        ctVals.resize(ctVals.rows()+1,temp.cols());
        ctVals<<temp,features[1];
      }
      ++count;
      features.clear();
    }
  }

  //std::cout<<ctVals.rows()<<std::endl;
  /*// Now, we save the files to output text files
  std::ofstream file1(outFileGC.c_str());
  if (file1.is_open())
  {
    file1 << gcVals << '\n';
  }
  file1.close();*/

  // Now, we save the files to output text files
  std::ofstream file2(outFileCT.c_str());
  if (file2.is_open())
  {
    file2 << ctVals << '\n';
  }
  file2.close();

  return 1;
}//end of function
