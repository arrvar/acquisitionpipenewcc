#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building vista
#------------------------------------------------------------------------------
SET(PROJ ShapeUtils)
SET(${PROJ}_EXT_LIBS)

#------------------------------------------------------------------------------
#Include the third party libraries needed 
#------------------------------------------------------------------------------
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/TPL_Linux.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building 
#vistaAcquisition
#------------------------------------------------------------------------------
SET(${PROJ}_LIB_DIRS
    Common/AbstractClasses
    Common/Framework
    Utilities/ImageProcessing/FFTUtils
    Utilities/ImageProcessing/ShapeUtilities
)

SET(${PROJ}_Utilities_SRCS) 
FOREACH(CONTENT ${${PROJ}_LIB_DIRS})
  FILE(GLOB_RECURSE U_SRCS
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.cpp
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.hh
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.h
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.txx)
  SET(${PROJ}_Utilities_SRCS ${${PROJ}_Utilities_SRCS} ${U_SRCS})
ENDFOREACH()

#------------------------------------------------------------------------------
#Build a C++ Unit Test
#------------------------------------------------------------------------------
SET(ut_spin${PROJ} OFF CACHE BOOL "Build C++ Unit tests for ${PROJ} Module")
IF(ut_spin${PROJ})
  ADD_EXECUTABLE(ut_spin${PROJ} ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/ShapeUtilsTest.cpp ${${PROJ}_Utilities_SRCS})
  TARGET_LINK_LIBRARIES(ut_spin${PROJ} ${${PROJ}_EXT_LIBS})
ENDIF()
