#include "Libraries/hemocalc/Interface/hemocalcInterface.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcInitialize.h"
#include "Libraries/hemocalc/Pipeline/ImageAcquisition/spinHemocalcImageAcquisition.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"

namespace
{
  typedef spin::RGBPixelType RGBPixelType;
  typedef spin::RGBImageType RGBImageType;
}

/*! \brief Default constructor */
hemocalcInterface::hemocalcInterface()
{
}//end of function

/*! \brief Initialize
 *  This function is used to initialize the pipeline using a
 *  parameter file passed as an input
 *  @pFile        : The parameter file
 */
int hemocalcInterface::Initialize(const char* pFile)
{
  // Initialize pObject
  gObject = std::make_shared<sVObject>();

  // Initialize a parser
  std::shared_ptr<spin::hemocalc::spinHemocalcInitialize> parser = \
  std::make_shared<spin::hemocalc::spinHemocalcInitialize>();
  // Check the global object can be populated
  int p = parser.get()->Initialize(gObject.get(), pFile);

  // If not, return the error code to the calling function
  if (p !=1) return p;

  //Initialize an image acquisition object
  acquisition = std::make_shared<pacquisition>();
  acquisition.get()->InstantiatePipeline(gObject.get());

  return 1;
}//end of function

/*! \brief Process
 *  This is the driver function to analyze AOI's in the hemocalc pipeline
 */
void hemocalcInterface::Process( std::string InputImage, int gX, int gY)
{
  // Instantiate a ProducerDataType
  producer pData;
  // Populate the coordinates
  pData.gYRow = gY;
  pData.gXCol = gX;
  // And process it
  acquisition.get()->ProcessPipeline(&pData, gObject.get());
}//end of function

/*! \brief CompleteProcessing
 *  This function waits for the process to finish and keeps the thread alive
 */
void hemocalcInterface::CompleteProcessing()
{
  acquisition.get()->CompleteProcessing();
}//end of function

/*! \brief AbortProcessGridLocations
 *  This function sends a signal to abort the analysis pipeline
 */
void hemocalcInterface::AbortProcessing()
{
  acquisition.get()->AbortPipeline();
}//end of function

// Default destructor
hemocalcInterface::~hemocalcInterface()
{
}//end of function
