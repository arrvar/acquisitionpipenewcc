#ifndef HEMOCALCINTERFACE_H
#define HEMOCALCINTERFACE_H
#include "Framework/Exports/spinAPI.h"
#include "AbstractClasses/AbstractCallback.h"
#include <memory>

// Forward declaration of classes
namespace spin
{
  namespace hemocalc
  {
    struct spinHemocalcObject;
    struct AnalysisDataType;
    template <class P> class SpinImageAcquisition;
  }//end of namespace hemocalc
}//end of namespace spin

/*! \brief hemocalcInterface
 *  This is an interface class that is used to call the
 *  backend pipeline associated with spin Hemocalc.
 */
class SPIN_API hemocalcInterface
{
  public:
    // Relevant typedefs
    typedef spin::hemocalc::spinHemocalcObject sVObject;
    typedef spin::hemocalc::AnalysisDataType producer;
    typedef spin::hemocalc::SpinImageAcquisition<producer> pacquisition;

    // Constructor
    hemocalcInterface();

    // Destructor
    ~hemocalcInterface();

    // Initialize the pipeline
    int Initialize(const char* pFile);

    // Run the pipeline at every AOI
    void Process(std::string img, int gX, int gY);

    // Complete the processing
    void CompleteProcessing();

    // Abort displacement estimation
    void AbortProcessing();

    // Get a const access to gObject
    const std::shared_ptr<sVObject> GetGObject(){return gObject;}
  private:
    // Instantiate a spin vista object
    std::shared_ptr<sVObject>   gObject;
    // Instantiate a spin image acquisition object
    std::shared_ptr<pacquisition>   acquisition;
};// end of vistaAcquisitionInterface class
#endif // vistaInterface
