#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building vista
#------------------------------------------------------------------------------
SET(PROJ hemocalc)
SET(${PROJ}_EXT_LIBS)

#------------------------------------------------------------------------------
#Include the third party libraries needed 
#------------------------------------------------------------------------------
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/TPL.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building 
#hemocalc
#------------------------------------------------------------------------------
SET(${PROJ}_LIB_DIRS
    Common/AbstractClasses
    Common/Framework
    Libraries/hemocalc/Pipeline
    Utilities/Pipelines/WBCDifferential
    Utilities/Pipelines/RBCDifferential
    Utilities/ImageProcessing/ITKImageUtils
)

SET(${PROJ}_Utilities_SRCS) 
FOREACH(CONTENT ${${PROJ}_LIB_DIRS})
  FILE(GLOB_RECURSE U_SRCS
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.cpp
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.hh
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.h
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.txx)
  SET(${PROJ}_Utilities_SRCS ${${PROJ}_Utilities_SRCS} ${U_SRCS})
ENDFOREACH()

#------------------------------------------------------------------------------
# Build a shared library so that acquisition can use it
#------------------------------------------------------------------------------
SET(hItf ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/hemocalcInterface.cpp 
            ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/hemocalcInterface.h)
ADD_LIBRARY(spin${PROJ} SHARED ${hItf} ${${PROJ}_Utilities_SRCS})

TARGET_LINK_LIBRARIES(spin${PROJ} ${${PROJ}_EXT_LIBS})
IF(NOT MSVC)
  SET_TARGET_PROPERTIES(spin${PROJ} PROPERTIES DEBUG_POSTFIX "_d" CXX_VISIBILITY_PRESET hidden)
ELSE()
  SET_TARGET_PROPERTIES(spin${PROJ} PROPERTIES  COMPILE_FLAGS "-DSPIN_EXPORTS"
                                                DEBUG_POSTFIX "_d")
ENDIF()

#------------------------------------------------------------------------------
#Check if a C++ Unit Test is requested
#------------------------------------------------------------------------------
SET(ut_spin${PROJ} OFF CACHE BOOL "Build C++ Unit test for ${PROJ} Module")
IF(ut_spin${PROJ})
  ADD_EXECUTABLE(ut_spin${PROJ} ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/spinHemocalc.cpp)
  TARGET_LINK_LIBRARIES(ut_spin${PROJ} spin${PROJ} ${${PROJ}_EXT_LIBS})
ENDIF()

