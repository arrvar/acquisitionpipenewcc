#ifndef HPREPROCESSINGPIPELINE_H_
#define HPREPROCESSINGPIPELINE_H_

// pipeline includes
#include "AbstractClasses/AbstractConsumer.h"
#include "AbstractClasses/AbstractDataStructures.h"
#include "AbstractClasses/AbstractCallback.h"
// Boost includes
#include <boost/thread.hpp>
#include <boost/timer/timer.hpp>

namespace spin
{
  namespace hemocalc
  {
    // A set of forward declarations
    template <class P>
    class SpinImageWriter;

    template <class P>
    class SpinWBCDifferential;

    template <class P>
    class SpinRBCDifferential;

    /*! \brief PreprocessingPipeline
     *  This main implementation of the pre-processing pipeline
     */
    template <class P>
    class PreprocessingPipeline : public spin::AbstractConsumer<P>
    {
      public:
        // Signals
        spin::VoidSignal sig_GetData;

            // Default destructor
        ~PreprocessingPipeline();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* dataPoint, void* obj);

        /*! \brief ProcessData*/
        void ProcessData(P dataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        void CompleteProcessing(void* obj);

        /*! \brief AbortPipeline*/
        void AbortPipeline();

        /*! \brief Reimplemented functions */
        void ConsumeFromQueue(P dataPoint, void* obj);
        void RequestQueue(void* obj);

        // Signals for metrics and logging
        spin::CallbackSignal sig_Logs;
        spin::CallbackSignal sig_Metrics;
        spin::CallbackSignal sig_Finished;
      private :
        // Thread to process the data
        boost::thread t_Process;
        boost::thread t_Preprocess;
        boost::mutex  abort_mutex;
        bool          abort;

        // Instances of all pipelines
        std::shared_ptr< SpinWBCDifferential<P> > mWBCDifferential;
        std::shared_ptr< SpinRBCDifferential<P> > mRBCDifferential;
        std::shared_ptr< SpinImageWriter<P> >     mImageWriter;

        /*! \brief Special functions*/
        void AbortOperations();
        void BeginProcessingQueue(void* obj);
        void RelayLogs(spin::CallbackStruct p);
        void RelayMetrics(spin::CallbackStruct p);
        void RelaySignal(spin::CallbackStruct p);
    };//end of class
  }//end of namespace hemocalc
}//end of namespace spin

#endif
