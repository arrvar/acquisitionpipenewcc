#ifndef HSPINPREPROCESSING_H_
#define HSPINPREPROCESSING_H_

// Pipeline includes
#include <memory>
#include "AbstractClasses/AbstractCallback.h"
#include "Framework/Exports/spinAPI.h"

namespace spin
{
  // Forward declaration of Producer Consumer class
  template <class P>
  class MPC;
  
  namespace hemocalc
  {
    // Forward declaration
    class DatabaseIO;
    template <class P> class PreprocessingInterface;
    template <class P> class PreprocessingPipeline;
    
    /*! \brief
     * This class acts as the interface with the stitching pipeline and in essence
     * is the interface for the library of the Spin Stitching Pipeline
     */
    template <class P>
    class SPIN_API SpinPreprocessing
    {
      public:
        /*! \brief Destructor*/
        ~SpinPreprocessing();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* producerDataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        int CompleteProcessing();

        /*! \brief AbortPipeline*/
        int AbortPipeline();
        
        // signal to indicate finish of process 
        spin::CallbackSignal     sig_Finished;
      private:
        bool pause;
        // Create objects for the producer level, broker and consumer level
        std::shared_ptr< PreprocessingInterface<P> > mPreprocessingInterface;
        std::shared_ptr< PreprocessingPipeline<P> >  mPreprocessingPipeline;
        std::shared_ptr< spin::MPC<P> > mMPC;
	std::shared_ptr<DatabaseIO> db;
        /*! \brief ConnectSignals*/
        int ConnectSignals(void* obj);
        /*! \brief Pause producer */
        void PauseProducer(bool state);
        /*! \brief RelaySignal*/
        void RelaySignal(spin::CallbackStruct p);
      };
  }//end of namespace hemocalc
}//end of namespace spin
#endif
