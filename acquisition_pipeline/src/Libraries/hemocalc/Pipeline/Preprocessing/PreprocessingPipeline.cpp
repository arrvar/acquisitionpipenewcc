// Pipeline includes
#include "Libraries/hemocalc/Pipeline/Preprocessing/PreprocessingPipeline.h"
#include "Libraries/hemocalc/Pipeline/Analysis/WBC/spinWBCDifferential.h"
#include "Libraries/hemocalc/Pipeline/Analysis/RBC/spinRBCDifferential.h"
#include "Libraries/hemocalc/Pipeline/ImageWriter/spinHemocalcImageWriter.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include <boost/scoped_array.hpp>

namespace spin
{
  namespace hemocalc
  {
    /*! \brief Destructor */
    template<class P>
    PreprocessingPipeline<P>::~PreprocessingPipeline()
    {
      // Call the destructor in the sub routines as well
      AbortPipeline();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void PreprocessingPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelaySignalFinished
     *  A slot to relay an incoming signal
     */
    template <class P>
    void PreprocessingPipeline<P>::RelaySignal(spin::CallbackStruct p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void PreprocessingPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline.
     */
    template<class P>
    int PreprocessingPipeline<P>::InstantiatePipeline(void* obj)
    {
      // Instantiate the next set of pipelines, if requested
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);

      // WBC differential (includes platelets)
      if (gObject->WBCDifferential)
      {
        mWBCDifferential = std::make_shared< SpinWBCDifferential<P> >();
        mWBCDifferential.get()->InstantiatePipeline(obj);
        mWBCDifferential.get()->sig_Finished.connect(boost::bind(&PreprocessingPipeline<P>::RelaySignal,this,_1));
      }

      // RBC Differential
      if (gObject->RBCDifferential)
      {
        mRBCDifferential = std::make_shared< SpinRBCDifferential<P> >();
        mRBCDifferential.get()->InstantiatePipeline(obj);
        mRBCDifferential.get()->sig_Finished.connect(boost::bind(&PreprocessingPipeline<P>::RelaySignal,this,_1));
      }

      abort = false;

      // Do we need to save the pre-processed images
      if (gObject->Preprocessing_ImageWrite)
      {
        mImageWriter = std::make_shared< SpinImageWriter<P> >();
        mImageWriter.get()->InstantiatePipeline(obj);
        mImageWriter.get()->sig_Finished.connect(boost::bind(&PreprocessingPipeline<P>::RelaySignal,this,_1));
      }
      return 0;
    }//end of function

    /*! \brief ProcessPipeline
     * Processes the data that is input from the source. Internally it pushes
     * the data into an appropriate data structure and then to a queue for
     * any consumer to pick it up.
     * parameters :
     * @P: The pointer to a data point from the producer. It contains
     *     an image that will need to be processed
     */
    template<class P>
    int PreprocessingPipeline<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);

      // Normalize the image with a calibration image
      if (producerDataPoint->rgbImagePrimary)
      {
        producerDataPoint->rgbImagePrimary_wc = spin::RGBImageType::New();
        producerDataPoint->rgbImagePrimary_wc->SetRegions(producerDataPoint->rgbImagePrimary->GetLargestPossibleRegion());
        producerDataPoint->rgbImagePrimary_wc->Allocate();

        spin::NormalizeRGBImageWithCalImage(&producerDataPoint->rgbImagePrimary, \
                                            &gObject->calibImage_Primary, \
                                            &producerDataPoint->rgbImagePrimary_wc);
      }

      if (producerDataPoint->rgbImageSecondary.size() > 0)
      {
        for (int j = 0; j < producerDataPoint->rgbImageSecondary.size(); ++j)
          spin::NormalizeRGBImageWithCalImage(&producerDataPoint->rgbImageSecondary[j], \
                                              &gObject->calibImage_Secondary[j], \
                                              &producerDataPoint->rgbImageSecondary[j]);
      }

      // TODO: Add more pre-processing steps that may be needed here
      return 1;
    }//end of function

    /*! \brief ProcessData
     * Chains up the process pipelines that need to be sequenced for the data
     * point
    */
    template <class P>
    void PreprocessingPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      // Process the data point using the pipeline described in
      // ProcessPipeline. It is pertinent that some information is loaded up
      // in rgbImagePrimary.
      if (dataPoint.rgbImagePrimary)
      {
        // Run the pre-processing pipeline
        int v = ProcessPipeline(&dataPoint, obj);
        if (v==1)
        {
          // Check if other pipelines are required
          if (gObject->WBCDifferential) // WBC differential
          {
            P wbc = dataPoint;
            mWBCDifferential.get()->ProcessPipeline(&wbc, obj);
          }
          if (gObject->RBCDifferential) // RBC differential
          {
            P rbc = dataPoint;
            mRBCDifferential.get()->ProcessPipeline(&rbc, obj);
          }
          if (gObject->Preprocessing_ImageWrite) // Normalized image writer
          {
            P wc = dataPoint;
            wc.saveImages = true;
            mImageWriter.get()->ProcessPipeline(&wc, obj);
          }
        }
      }
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     * This function ensures that processing is finished
     */
    template <class P>
    void PreprocessingPipeline<P>::CompleteProcessing(void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      // Create a vector of processes that can be run in parallel
      std::vector<std::shared_ptr<boost::thread> > tArray;

      if (gObject->WBCDifferential)
      {
        std::shared_ptr<boost::thread> pT1(new boost::thread(&SpinWBCDifferential<P>::CompleteProcessing,\
                                                             mWBCDifferential.get()));
        tArray.push_back(pT1);
      }

      if (gObject->RBCDifferential)
      {
        std::shared_ptr<boost::thread> pT1(new boost::thread(&SpinRBCDifferential<P>::CompleteProcessing,\
                                                             mRBCDifferential.get()));
        tArray.push_back(pT1);
      }

      if (gObject->Preprocessing_ImageWrite)
      {
        std::shared_ptr<boost::thread> pT1(new boost::thread(&SpinImageWriter<P>::CompleteProcessing,\
                                                             mImageWriter.get()));
        tArray.push_back(pT1);
      }
      // Finish all processes
      for (int jThread = 0; jThread < tArray.size(); ++jThread)
      {
        tArray[jThread]->join();
      }

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();
      }
      return;
    }//end of function

    /*! \brief BeginProcessingQueue [SLOT]
     * This function Sends a request to the queue in the broker for data
     */
    template <class P>
    void PreprocessingPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      GridMapGeneration* gmap = gObject->gMap.get();

      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetGridMapping()->size();

      while(n_images < max_images)
      {
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_Preprocess.joinable())
        {
          t_Preprocess.join();
          ++n_images;
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
      }
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     * This function receives a signal from the MPC with a new data point
     */
    template <class P>
    void PreprocessingPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      // Process the data point in a separate thread
      t_Preprocess = boost::thread(&PreprocessingPipeline<P>::ProcessData,\
                                   this,dataPoint,obj);
    }//end of function

    /*! \brief RequestQueue [SLOT]
    * This function receives a signal from the MPC that a new datapoint is
    * available for consumption
    */
    template <class P>
    void PreprocessingPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&PreprocessingPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts the pipelines and cleanly exits
     */
    template <class P>
    void PreprocessingPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
      AbortOperations();
    }//end of function

    /*! \brief AbortOperations
     *	This function forwards the abort signal to other consumers and
     *	deallocates any memory objects that are part of this class alone.
     *	This however does not destroy the configuration for which this
     *  pipeline was primed with. Thereby enabling reacquisition instantly.
     */
    template <class P>
    void PreprocessingPipeline<P>::AbortOperations()
    {
      if (mWBCDifferential.get() != NULL)
      {
        // Send a signal to abort the operations
        mWBCDifferential.get()->AbortPipeline();
      }
      if (mRBCDifferential.get() != NULL)
      {
        // Send a signal to abort the operations
        mRBCDifferential.get()->AbortPipeline();
      }
      if (mImageWriter.get() != NULL)
      {
        // Send a signal to abort the operations
        mImageWriter.get()->AbortPipeline();
      }
    }//end of function

    // explicit instantiation of template class
    template class PreprocessingPipeline<AnalysisDataType>;
  }//end of namespace hemocalc
}//end of namespace spin
