#include "Libraries/hemocalc/Pipeline/ImageAcquisition/ImageAcquisitionInterface.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  namespace hemocalc
  {
    /*! \brief AddToQueue
     *  This function adds an object to the Pre-processing queue
     */
    template<class P>
    void ImageAcquisitionInterface<P>::AddToQueue(void* dataPoint)
    {
      // typecasts
      AnalysisDataType* dp = static_cast<AnalysisDataType*>(dataPoint);

      //P dPoint = dataPoint;//*dp;
      //emit the signal
      sig_AddQ(*dp);
    }//end of function

    // explicit instantiation of template class
    template class ImageAcquisitionInterface<AnalysisDataType>;
  }//end of stitching namespace
}//end of spin namespace
