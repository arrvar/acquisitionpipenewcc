#ifndef HIMAGEACQUISITIONPIPELINE_H_
#define HIMAGEACQUISITIONPIPELINE_H_

// pipeline includes
#include "AbstractClasses/AbstractConsumer.h"
#include "AbstractClasses/AbstractCallback.h"
// Boost includes
#include <boost/thread.hpp>

namespace spin
{
  namespace hemocalc
  {
    // A set of forward declarations
    template <class P>
    class SpinPreprocessing;

    /*! \brief PreprocessingPipeline
     *  This class acts as the interface with the stitching pipeline and
     *  in essence is the interface for the library of the
     *  Spin Stitching Pipeline
     */
    template <class P>
    class ImageAcquisitionPipeline : public spin::AbstractConsumer<P>
    {
      public:
        // Signals
        spin::VoidSignal sig_GetData;

        // Default destructor
        ~ImageAcquisitionPipeline();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* dataPoint, void* obj);

        /*! \brief ProcessData*/
        void ProcessData(P dataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        void CompleteProcessing(void* obj);

        /*! \brief AbortPipeline*/
        void AbortPipeline();

        /*! \brief Reimplemented functions */
        void ConsumeFromQueue(P dataPoint, void* obj);
        void RequestQueue(void* obj);

        // Signals for metrics and logging
        spin::CallbackSignal sig_Logs;
        spin::CallbackSignal sig_Metrics;
        spin::CallbackSignal sig_Finished;
      private :
        // Thread to process the data
        boost::thread t_Process;
        boost::thread t_ImageAcquisition;
        boost::mutex  abort_mutex;
        bool          abort;
        // An instance of the Preprocessing module
        std::shared_ptr< SpinPreprocessing<P> > mPreprocessing;
        /*! \brief Special functions*/
        void AbortOperations();
        void BeginProcessingQueue(void* obj);
        void RelayLogs(spin::CallbackStruct p);
        void RelayMetrics(spin::CallbackStruct p);
        void RelaySignal(spin::CallbackStruct p);
    };//end of class
  }//end of namespace stitching
}//end of namespace spin

#endif
