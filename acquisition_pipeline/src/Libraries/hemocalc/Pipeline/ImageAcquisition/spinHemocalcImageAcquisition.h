#ifndef HSPINIMAGEACQUISITION_H_
#define HSPINIMAGEACQUISITION_H_

// Pipeline includes
#include <memory>
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  // Forward declaration of Producer Consumer class
  template <class P>
  class MPC;

  namespace hemocalc
  {
    // Other forward declarations
    template <class P>
    class ImageAcquisitionInterface;
    template <class P>
    class ImageAcquisitionPipeline;

    /*! \brief
     * This class acts as the interface with the image acquisition pipeline
     */
    template <class P>
    class SpinImageAcquisition
    {
      public:
        /*! \brief Destructor*/
        ~SpinImageAcquisition();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* producerDataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        int CompleteProcessing();

        /*! \brief AbortPipeline*/
        int AbortPipeline();

        // callback signal
        spin::CallbackSignal sig_Finished;
      private:
        bool pause;
        // Create objects for the producer level, broker and consumer level
        std::shared_ptr< ImageAcquisitionInterface<P> > mImageAcquisitionInterface;
        std::shared_ptr< ImageAcquisitionPipeline<P> > mImageAcquisitionPipeline;
        std::shared_ptr< spin::MPC<P> > mMPC;

        /*! \brief ConnectSignals*/
        int ConnectSignals(void* obj);
        /*! \brief Pause the producer */
        void PauseProducer(bool state);
        /*! \brief Relay Signal*/
        void RelaySignal(spin::CallbackStruct p);
      };
  }//end of namespace stitching
}//end of namespace spin
#endif


