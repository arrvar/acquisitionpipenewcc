// Pipeline includes
#include "Libraries/hemocalc/Pipeline/ImageAcquisition/ImageAcquisitionPipeline.h"
#include "Libraries/hemocalc/Pipeline/Preprocessing/spinHemocalcPreprocessing.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"

namespace spin
{
  namespace hemocalc
  {
    /*! \brief Destructor */
    template<class P>
    ImageAcquisitionPipeline<P>::~ImageAcquisitionPipeline()
    {
      // Call the destructor in the sub routines as well
      AbortPipeline();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelaySignalFinished
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::RelaySignal(spin::CallbackStruct p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline.
     */
    template<class P>
    int ImageAcquisitionPipeline<P>::InstantiatePipeline(void* obj)
    {
      // Instantiate the next set of pipelines, if requested
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);

      if (gObject->Preprocessing)
      {
        mPreprocessing = std::make_shared<SpinPreprocessing<P>>();
        mPreprocessing.get()->InstantiatePipeline(obj);
        mPreprocessing.get()->sig_Finished.connect(boost::bind(&ImageAcquisitionPipeline<P>::RelaySignal,this,_1));
      }

      abort = false;

      // TODO: Some other initializations may be needed for acquisition from the camera
      return 0;
    }//end of function

    /*! \brief ProcessPipeline
     * Processes the data that is input from the source. Internally it pushes
     * the data into an appropriate data structure and then to a queue for
     * any consumer to pick it up.
     * parameters :
     * @P: The pointer to a data point from the producer. It contains
     *     an image/images that will need to be processed
     */
    template<class P>
    int ImageAcquisitionPipeline<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      // And get the grid mapping
      GridMapGeneration* gmap = gObject->gMap.get();
      std::map<pIndex, FOVStruct>* dFov = const_cast<std::map<pIndex, FOVStruct>*>(gmap->GetGridMapping());

      // Build the index
      pIndex it(producerDataPoint->gYRow, producerDataPoint->gXCol);

      // We are acquiring the image from the disk
      typedef spin::RGBImageType RGBImageType;
      // Get the image names
      std::string inpImage1 =  (*dFov)[it].img_Primary;

      // Read the primary images
      producerDataPoint->rgbImagePrimary = RGBImageType::New();
      producerDataPoint->rgbImagePrimary->SetRegions(gObject->mImageRegion);
      producerDataPoint->rgbImagePrimary->Allocate();
      //gObject->ImageIO.get()->ReadImage(inpImage1, &producerDataPoint->rgbImagePrimary, false);

      spin::ReadITKImage<spin::RGBImageType>(inpImage1, producerDataPoint->rgbImagePrimary);
      // And any secondary images
      for (int j = 0; j < producerDataPoint->rgbImageSecondary.size(); ++j)
      {
        std::string inpImage2 =  (*dFov)[it].img_Secondary[j];
        producerDataPoint->rgbImageSecondary[j] = RGBImageType::New();
        producerDataPoint->rgbImageSecondary[j]->SetRegions(gObject->mImageRegion);
        producerDataPoint->rgbImageSecondary[j]->Allocate();
        //gObject->ImageIO.get()->ReadImage(inpImage2, &producerDataPoint->rgbImageSecondary[j], false);
        spin::ReadITKImage<spin::RGBImageType>(inpImage2, producerDataPoint->rgbImageSecondary[j]);
      }

      spin::CallbackStruct p;
      p.Module = "ImageAcquisitionPipeline";
      p.Descr  = "Process_Pipeline: "+inpImage1;
      p.Finished = 0;
      sig_Finished(p);
      return 1;
    }//end of function

    /*! \brief ProcessData
     * Chains up the process pipelines that need to be sequenced for the data
     * point
    */
    template <class P>
    void ImageAcquisitionPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // Process the data point using the pipeline described in ProcessPipeline.
      // We are reading the data from a disk
      int v = ProcessPipeline(&dataPoint, obj);

      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      if (gObject->Preprocessing)
      {
        // Make a copy of the data
        P dataPointPP = dataPoint;
        // Forward the data point to the next Pipeline (which in this case happens to be
        // pre-processing)
        mPreprocessing.get()->ProcessPipeline(&dataPointPP, obj);
      }
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     * This function ensures that processing is finished
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::CompleteProcessing(void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      if (gObject->Preprocessing)
      {
        // Complete the sub pipelines as well
        mPreprocessing.get()->CompleteProcessing();
      }

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();

        // Update the background status of all the AoIs.
        CallbackStruct p;
        p.Module = "Image Acquisition Pipeline";
        sig_Finished(p);
      }
      return;
    }//end of function

    /*! \brief BeginProcessingQueue [SLOT]
     * This function Sends a request to the queue in the broker for data
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      GridMapGeneration* gmap = gObject->gMap.get();

      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetGridMapping()->size();

      while(n_images < max_images)
      {
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_ImageAcquisition.joinable())
        {
          t_ImageAcquisition.join();
          ++n_images;
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(500));
        }
      }
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     * This function receives a signal from the MPC with a new data point
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      // Process the data point in a separate thread
      t_ImageAcquisition  = boost::thread(&ImageAcquisitionPipeline<P>::ProcessData,\
                                          this,dataPoint,obj);
    }//end of function

    /*! \brief RequestQueue [SLOT]
    * This function receives a signal from the MPC that a new datapoint is
    * available for consumption
    */
    template <class P>
    void ImageAcquisitionPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&ImageAcquisitionPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts the pipelines and cleanly exits
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
      AbortOperations();
    }//end of function

    /*! \brief AbortOperations
     *	This function forwards the abort signal to other consumers and
     *	deallocates any memory objects that are part of this class alone.
     *	This however does not destroy the configuration for which this
     *  pipeline was primed with. Thereby enabling reacquisition instantly.
     */
    template <class P>
    void ImageAcquisitionPipeline<P>::AbortOperations()
    {
      if (mPreprocessing.get() != NULL)
      {
        // Send a signal to abort the operations
        mPreprocessing.get()->AbortPipeline();
      }
    }//end of function

    // explicit instantiation of template class
    template class ImageAcquisitionPipeline<AnalysisDataType>;
  }//end of namespace stitching
}//end of namespace spin
