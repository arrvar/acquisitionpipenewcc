// Pipeline includes
#include "Libraries/hemocalc/Pipeline/ImageWriter/ImageWriterPipeline.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

namespace spin
{
  namespace hemocalc
  {
    /*! \brief Destructor */
    template<class P>
    ImageWriterPipeline<P>::~ImageWriterPipeline()
    {
      // Call the destructor in the sub routines as well
      AbortPipeline();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageWriterPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelaySignalFinished
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageWriterPipeline<P>::RelaySignal(spin::CallbackStruct p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void ImageWriterPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline.
     */
    template<class P>
    int ImageWriterPipeline<P>::InstantiatePipeline(void* obj)
    {
      abort = false;

      // TODO: Some other initializations may be needed for acquisition from the camera
      return 0;
    }//end of function

    /*! \brief ProcessPipeline
     * Processes the data that is input from the source. Internally it pushes
     * the data into an appropriate data structure and then to a queue for
     * any consumer to pick it up.
     * parameters :
     * @producerDataPoint : The pointer to a data point from the producer.
     * @obj               : The spinHemocalc object
     */
    template<class P>
    int ImageWriterPipeline<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      // And get the grid mapping
      GridMapGeneration* gmap = gObject->gMap.get();

      std::map<pIndex, FOVStruct>* dFov = const_cast<std::map<pIndex, FOVStruct>*>(gmap->GetGridMapping());
      // Build the index
      pIndex it(producerDataPoint->gYRow, producerDataPoint->gXCol);
      // At this point let us check what kind of images are being written to disk?
      if (producerDataPoint->saveImages)
      {
        // We are saving a RGB image present in producerDataPoint
        typedef spin::RGBImageType RGBImageType;
        // Get the image name
        std::vector<std::string> temp;
        boost::split(temp, (*dFov)[it].img_Primary, boost::is_any_of("/"));
        // From the image name we will derive the folder name
        std::string imgName = temp[temp.size()-1];
        // Split the image name
        temp.clear();
        boost::split(temp, imgName, boost::is_any_of("."));
        // From the image name we will derive the folder name
        imgName = temp[0];
        // Split the image name
        temp.clear();
        // Create the filename
        std::string imgNameRaw = gObject->outDir + "/"+imgName+"/"+imgName+gObject->file_extension_write;
        // Save the image
        spin::WriteITKImage<spin::RGBImageType>(imgNameRaw, producerDataPoint->rgbImagePrimary_wc);
      }

      spin::CallbackStruct p;
      p.Module = "ImageWriterPipeline";
      p.Finished = 0;
      sig_Finished(p);
      return 0;
    }//end of function

    /*! \brief ProcessData
     * Chains up the process pipelines that need to be sequenced for the data
     * point
    */
    template <class P>
    void ImageWriterPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // Process the data point using the pipeline described in ProcessPipeline.
      // It may so happen that the image may already have been populated
      if ( dataPoint.rgbImagePrimary )
      {
        // We are reading the data from a disk
        ProcessPipeline(&dataPoint, obj);
      }
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     * This function ensures that processing is finished
     */
    template <class P>
    void ImageWriterPipeline<P>::CompleteProcessing(void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();

        // Update the background status of all the AoIs.
        CallbackStruct p;
        p.Module = "Image Writer Pipeline";
        sig_Finished(p);
      }
      return;
    }//end of function

    /*! \brief BeginProcessingQueue [SLOT]
     * This function Sends a request to the queue in the broker for data
     */
    template <class P>
    void ImageWriterPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      GridMapGeneration* gmap = gObject->gMap.get();

      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetGridMapping()->size();

      while(n_images < max_images)
      {
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_ImageWriter.joinable())
        {
          t_ImageWriter.join();
          ++n_images;
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
      }
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     * This function receives a signal from the MPC with a new data point
     */
    template <class P>
    void ImageWriterPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      // Process the data point in a separate thread
      t_ImageWriter  = boost::thread(&ImageWriterPipeline<P>::ProcessData,this,dataPoint,obj);
    }//end of function

    /*! \brief RequestQueue [SLOT]
    * This function receives a signal from the MPC that a new datapoint is
    * available for consumption
    */
    template <class P>
    void ImageWriterPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&ImageWriterPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts the pipelines and cleanly exits
     */
    template <class P>
    void ImageWriterPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
      AbortOperations();
    }//end of function

    /*! \brief AbortOperations
     *	This function forwards the abort signal to other consumers and
     *	deallocates any memory objects that are part of this class alone.
     *	This however does not destroy the configuration for which this
     *  pipeline was primed with. Thereby enabling reacquisition instantly.
     */
    template <class P>
    void ImageWriterPipeline<P>::AbortOperations()
    {
    }//end of function

    // explicit instantiation of template class
    template class ImageWriterPipeline<AnalysisDataType>;
  }//end of namespace stitching
}//end of namespace spin
