#include "Libraries/hemocalc/Pipeline/ImageWriter/ImageWriterInterface.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  namespace hemocalc
  {
    /*! \brief AddToQueue
     *  This function adds an object to the Pre-processing queue
     */
    template<class P>
    void ImageWriterInterface<P>::AddToQueue(void* dataPoint)
    {
      // typecasts
      AnalysisDataType* dp = static_cast<AnalysisDataType*>(dataPoint);
      //emit the signal
      sig_AddQ(*dp);
    }//end of function

    // explicit instantiation of template class
    template class ImageWriterInterface<AnalysisDataType>;
  }//end of stitching namespace
}//end of spin namespace
