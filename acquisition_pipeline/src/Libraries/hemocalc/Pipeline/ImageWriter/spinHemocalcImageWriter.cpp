#include "Libraries/hemocalc/Pipeline/ImageWriter/spinHemocalcImageWriter.h"
#include "Libraries/hemocalc/Pipeline/ImageWriter/ImageWriterInterface.h"
#include "Libraries/hemocalc/Pipeline/ImageWriter/ImageWriterPipeline.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include "Framework/MPC/MPC.h"
#include "Framework/Logger/LoggerInterface.h"

namespace spin
{
  namespace hemocalc
  {
    /*! \brief PauseProducer
     *  enables a boolean to either pause or resume the producer
     */
    template <class P>
    void SpinImageWriter<P>::PauseProducer(bool state)
    {
      pause = state;
    }//end of function

    /*! \brief ConnectSignals
     *  Connects signals and slots for all the members of this class here
     */
    template<class P>
    int SpinImageWriter<P>::ConnectSignals(void* obj)
    {
      // Connect the corresponding signals and slots
      mMPC.get()->sig_DataAvailable.connect(boost::bind(&ImageWriterPipeline<P>::RequestQueue,mImageWriterPipeline,obj));
      mMPC.get()->sig_DataPoint.connect(boost::bind(&ImageWriterPipeline<P>::ConsumeFromQueue,mImageWriterPipeline, _1, _2));
      mMPC.get()->sig_FinAcq.connect(boost::bind(&ImageWriterPipeline<P>::CompleteProcessing,mImageWriterPipeline,obj));
      mMPC.get()->sig_Abort.connect(boost::bind(&ImageWriterPipeline<P>::AbortPipeline,mImageWriterPipeline));
      mMPC.get()->sig_QueueBurdened.connect(boost::bind(&SpinImageWriter<P>::PauseProducer,this, _1));

      mImageWriterInterface.get()->sig_AddQ.connect(boost::bind(&MPC<P>::AddToQueue, mMPC, _1));
      mImageWriterInterface.get()->sig_FinAcq.connect(boost::bind(&MPC<P>::Finished, mMPC));
      mImageWriterInterface.get()->sig_Abort.connect(boost::bind(&MPC<P>::Abort, mMPC));
      mImageWriterPipeline.get()->sig_GetData.connect(boost::bind(&MPC<P>::PopFromQueue, mMPC, obj));
      mImageWriterPipeline.get()->sig_Finished.connect(boost::bind(&SpinImageWriter<P>::RelaySignal, this, _1));

      // Typecast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);

      // Connect the logging signal
      if (gObject->loggingInitialized)
      {
        mMPC.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
        mImageWriterPipeline.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
        mImageWriterPipeline.get()->sig_Finished.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
      }

      return 1;
    }//end of function

    /*! \brief Destructor
     *  Clears all objects, joins all threads and exits
     */
    template<class P>
    SpinImageWriter<P>::~SpinImageWriter()
    {
      CompleteProcessing();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void SpinImageWriter<P>::RelaySignal(spin::CallbackStruct p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline. Creates the objects and makes all the
     *  connections if necessary. It does not configure any data structure or
     *  set any constants as they would be dependent on the grid that is being
     *  @obj      : The spinVistaAcquisitionObject that has been initialized prior
     *              to calling this function
     */
    template<class P>
    int SpinImageWriter<P>::InstantiatePipeline(void* obj)
    {
      // The producer can be either a camera or a File I/O interface
      mImageWriterInterface = std::make_shared<ImageWriterInterface<P> >();

      // The consumer can be any process that can receive this data structure
      mImageWriterPipeline = std::make_shared<ImageWriterPipeline<P> >();

      // Both the producer and consumer communicate with each other via a Broker
      mMPC = std::make_shared<MPC<P> >();

      // Typecast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);

      // Connect the logging signal
      if (gObject->loggingInitialized)
      {
        mMPC.get()->EnableLogging();
      }

      // Instantiate the pipelines (lightweight in nature)
      mImageWriterPipeline.get()->InstantiatePipeline(obj);

      // Connect all the signals and slots
      ConnectSignals(obj);

      return 0;
    }//end of function

    /*! \brief CompleteProcessing
     *  Completes the pipeline till the end and cleanly exits
     */
    template<class P>
    int SpinImageWriter<P>::CompleteProcessing()
    {
      // Send a signal to indicate end of acquisition
      mImageWriterInterface.get()->sig_FinAcq();

      return 0;
    }//end of function

    /*! \brief ProcessPipeline
     *  Processes the data that is input from the source. Internally it pushes
     *  the data into an appropriate data structure and then to a queue for
     *  any consumer to pick it up.
     */
    template<class P>
    int SpinImageWriter<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // Push this data point into a queue, but check if this is in a paused state
      while(pause)
      {
        boost::this_thread::sleep(boost::posix_time::milliseconds(5));
      }

      // Push this data point into a queue
      mImageWriterInterface.get()->AddToQueue(producerDataPoint);

      return 1;
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts all operations and cleanly exits
     */
    template<class P>
    int SpinImageWriter<P>::AbortPipeline()
    {
      // Send a signal to indicate end of acquisition
      mImageWriterInterface.get()->sig_Abort();

      return 0;
    }//end of function

    // explicit instantiation of template class
    template class SpinImageWriter<AnalysisDataType>;
  }//end of stitching namespace
}//end of spin namespace
