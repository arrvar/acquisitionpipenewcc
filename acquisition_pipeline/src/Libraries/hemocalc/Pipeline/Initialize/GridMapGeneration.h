#ifndef GRIDMAPGENERATION_H_
#define GRIDMAPGENERATION_H_
#include <string>
#include <boost/bimap.hpp>
#include "AbstractClasses/AbstractGraphHelper.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  namespace hemocalc
  {
    struct OverlapMetric
    {
      float displacement_fftcost;
    };//end of struct

    /*! \brief FOVStruct
     *  A structure to hold information about a given FOV
     */
    struct FOVStruct
    {
      ~FOVStruct()
      {
        Masks.clear();
        img_Secondary.clear();
      }
      // Name of the AOI's
      std::string                                    img_Primary;
      std::vector<std::string>                       img_Secondary;
      std::map<int, spin::UCharImageType2D::Pointer> Masks;
      std::string                                    out_Folder;
      // Other details about objects
    };//end of structure


    /*! GridMapGeneration
     *  This class is a placeholder for all functions to estimate translation between
     *  two pairs of images
     */
    class GridMapGeneration
    {
      public:
        typedef std::pair<int,int> pIndex;

        /*! \brief Default constructor*/
        GridMapGeneration(){}

        /*! \brief Default destructor*/
        ~GridMapGeneration()
        {
          Clear();
        }

        /*! \brief Constructor with grid coordinates as input*/
        int Initialize( std::string& primaryFolder, \
                        std::vector<std::string>& secondaryFolders,\
                        std::string& outputFolder);

        // Define a buffer of FT images
        typedef std::map< pIndex, FOVStruct> gridFOV;

        /*! \brief An API to get access to the FFT object*/
        const gridFOV* GetGridMapping() { return &aoiInfo; }
        /*! \brief Clear Data */
        void Clear();

      private:
        // Define a few variables
        gridFOV           aoiInfo;
        // create an output folder
        std::string CreateOutputFolder(std::string& p1, std::string& p2);
    };//end of class
  }//end of namespace stitching
}//end of namespace spin
#endif
