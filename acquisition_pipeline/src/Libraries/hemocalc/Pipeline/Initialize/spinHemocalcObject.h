#ifndef SPINHEMOCALCOBJECT_H_
#define SPINHEMOCALCOBJECT_H_
#include <cstdlib>
#include <string>
#include <memory>
#include <vector>
#include "AbstractClasses/AbstractDataStructures.h"
#include "Framework/Logger/LoggerInterface.h"
#include "Framework/ImageIO/ImageIO.h"
#include "Libraries/hemocalc/Pipeline/Initialize/GridMapGeneration.h"
#include "Libraries/hemocalc/Pipeline/Analysis/WBC/WBCDifferentialParams.h"
#include "Libraries/hemocalc/Pipeline/Analysis/RBC/RBCDifferentialParams.h"

namespace spin
{
  namespace hemocalc
  {
    /*! \brief spinHemocalcObject
     *  This structure contains all information that would be needed
     *  to implement a hematology pipeline analysis
     */
    struct spinHemocalcObject
    {
      // Default destructor
      ~spinHemocalcObject()
      {
        calibImage_Secondary.clear();
        cImage_Secondary.clear();
      }//end of function

      /////////////////////////////////////////////////////////////////////////
      // List out all objects that will be used in the pipeline
      // ImageIO object
      std::shared_ptr<spin::RGBImageIO> ImageIO;
      // An instance of a grid map object
      std::shared_ptr<GridMapGeneration> gMap;
      // Logger object
      std::shared_ptr<LoggerInterface> logger;
      // The full path of the database file
      std::string dbFile;

      /////////////////////////////////////////////////////////////////////////
      // Image parameters
      int                       gridId;
      std::string               slideDir;
      std::string               imgDir;
      std::string               outDir;
      std::string               magnification;

      /////////////////////////////////////////////////////////////////////////
      // Preprocessing parameters
      bool  Preprocessing;
      bool  Preprocessing_ImageWrite;
      // Image region
      RGBImageType::RegionType mImageRegion;
      // Various image names
      std::string cImage_Primary;
      RGBImageType::Pointer calibImage_Primary;
      std::string primaryFolder;

      std::vector<std::string> cImage_Secondary;
      std::map<int, spin::RGBImageType::Pointer> calibImage_Secondary;
      std::vector<std::string> secondaryFolder;
      // file extension (reading)
      std::string file_extension;
      // file extension (writing)
      std::string file_extension_write;

      /////////////////////////////////////////////////////////////////////////
      // WBC+Platelet
      bool WBCDifferential;
      std::shared_ptr<WBCDifferentialParams> wbcParams;

      /////////////////////////////////////////////////////////////////////////
      // RBC
      bool RBCDifferential;
      std::shared_ptr<RBCDifferentialParams> rbcParams;

      // Debug flags
      bool loggingInitialized;
      std::string logFile;
    };//end of struct
  }//end of namespace hemocalc
}//end of namespace spin

#endif

