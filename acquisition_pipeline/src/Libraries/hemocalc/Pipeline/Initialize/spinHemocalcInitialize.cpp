#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcInitialize.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include "Libraries/hemocalc/Pipeline/Initialize/GridMapGeneration.h"
#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <algorithm>

namespace spin
{
  namespace hemocalc
  {
    /*! \brief InitializeCommon()
     *  This function initializes all elements associated with the
     *  pre-processing module of the pipeline
     */
    int InitializeCommon(spinHemocalcObject* gObject)
    {
      typedef spin::RGBImageType RGBImageType;

      boost::filesystem::path dir0(gObject->cImage_Primary.c_str());
      if (!boost::filesystem::exists(dir0))
      {
        return -2;
      }

      /////////////////////////////////////////////////////////////////////////
      // Instantiate the ImageIO object with the calibration image
      // We assume that all images will conform with specifications
      // described by the calibration image
      gObject->ImageIO = std::make_shared<spin::RGBImageIO>(gObject->cImage_Primary);

      /////////////////////////////////////////////////////////////////////////
      // Frame the primary folder
      gObject->primaryFolder = gObject->imgDir + "/" + gObject->primaryFolder;

      /////////////////////////////////////////////////////////////////////////
      // Read bg image (primary)
      gObject->calibImage_Primary = spin::RGBImageType::New();
      int v = gObject->ImageIO.get()->ReadImage(gObject->cImage_Primary, &gObject->calibImage_Primary);
      if (v != 1) return -3;

      // Set the image region
      gObject->mImageRegion = gObject->calibImage_Primary->GetLargestPossibleRegion();

      // Read bg images (secondary)
      for (int j = 0; j < gObject->secondaryFolder.size(); ++j)
      {
        // Frame the secondary input folder
        gObject->secondaryFolder[j] = gObject->imgDir+"/"+gObject->secondaryFolder[j];
        // Check if this file exists
        boost::filesystem::path dir1(gObject->cImage_Secondary[j].c_str());
        if (!boost::filesystem::exists(dir1))
        {
          return -3;
        }
        // Frame and read the secondary calib image
        gObject->calibImage_Secondary[j] = spin::RGBImageType::New();
        v = gObject->ImageIO.get()->ReadImage(gObject->cImage_Secondary[j], &gObject->calibImage_Secondary[j]);
        if (v != 1) return -4;
      }

      //std::cout<<gObject->slideDir<<std::endl;
      /////////////////////////////////////////////////////////////////////////
      //Read all image names that need to be processed
      gObject->gMap = std::make_shared<GridMapGeneration>();
      v = gObject->gMap->Initialize(gObject->primaryFolder, gObject->secondaryFolder, gObject->outDir);
      if (v != 1) return -5;
      return 1;
    }//end of function

    /*! \brief Initialize()
     *  This function is used to parse grid information
     *  that will eventually result in the scan pattern. Before calling
     *  this function, it is assumed that this object is initialized.
     */
    int spinHemocalcInitialize::Initialize(void* obj, const char* ParameterFileName)
    {
      // Check if the parameter file parser is correct
      std::ifstream input(ParameterFileName);
      // populate tree structure pt
      using boost::property_tree::ptree;
      ptree pt;
      try
      {
        // parse the xml file
        read_xml(input, pt);
      }
      catch(const boost::property_tree::ptree_error &e)
      {
        return -1;
      }

      // Here, we downcast a default object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      if (gObject == NULL) return -2;

      // Default database
      gObject->dbFile = "";
      gObject->gridId = -1;

      // By default, we do not do anything
      gObject->WBCDifferential          = false;
      gObject->wbcParams                = std::make_shared<WBCDifferentialParams>();
      gObject->RBCDifferential          = false;
      gObject->rbcParams                = std::make_shared<RBCDifferentialParams>();

      // Preprocessing is enabled by default
      gObject->Preprocessing            = true;
      gObject->Preprocessing_ImageWrite = true;
      // If we do save/read the file, then default extension is bmp
      gObject->file_extension           = ".bmp";
      gObject->file_extension_write     = ".png";

      // Default flags for metrics and logging
      gObject->loggingInitialized       = false;
      gObject->logFile                  ="";

      std::string outFldr = "";
      // Now, parse through all parameter settings and populate gObject
      BOOST_FOREACH(ptree::value_type& v, pt.get_child("hemocalc"))
      {
        if (v.first == "Images")
        {
          BOOST_FOREACH(ptree::value_type& v1, v.second)
          {
            if (v1.first == "Root_Folder")
            {
              gObject->slideDir = v1.second.get<std::string>("");
              // At the same time, we also create a default output folder based on the input folder
              // which can be overwritten if the parameter file provides us with an
              // output folder
              gObject->outDir = gObject->slideDir+"/Results";
            }
            if (v1.first == "Image_Folder")
            {
              gObject->imgDir = v1.second.get<std::string>("");
            }
            else
            if (v1.first == "Primary")
            {
              gObject->primaryFolder = v1.second.get<std::string>("");
            }
            else
            if (v1.first == "Primary_Calib")
            {
              gObject->cImage_Primary = v1.second.get<std::string>("");
            }
            else
            if (v1.first == "Secondary")
            {
              gObject->secondaryFolder.push_back(v1.second.get<std::string>(""));
            }
            else
            if (v1.first == "Secondary_Calib")
            {
              gObject->cImage_Secondary.push_back(v1.second.get<std::string>(""));
            }
          }
        }
        else
        if(v.first == "Output_Folder")
        {
          outFldr= v.second.get<std::string>("");
        }
        else
        if (v.first == "Logging_File")
        {
          gObject->logFile = v.second.get<std::string>("");
        }
        else
        if (v.first == "WBCDifferential")
        {
          gObject->WBCDifferential          = true;
          // Parse tags associated with WBC differential
          ParseWBCDiffTags(&v, obj);
        }
        else
        if (v.first == "RBCDifferential")
        {
          gObject->RBCDifferential          = true;
          // Parse tags associated with RBC differential
          ParseRBCDiffTags(&v, obj);
        }
        else
        if (v.first == "Parasite")
        {
          // Parse tags associated with Parasites
          ParseParasiteTags(&v, obj);
        }
      }// finish parsing all tags within "hemocalc"

      // 2.) Check if the user has specified an output folder
      if (outFldr != "")
      {
        // replace the default output folder
        gObject->outDir = outFldr;
      }

      // 3.) Instantiate a logger object
      if (gObject->logFile != "")
      {
        gObject->loggingInitialized = true;
        // Instantiate a logging object
        gObject->logger = std::make_shared<spin::LoggerInterface>();
        gObject->logger.get()->SetLoggingFile(gObject->logFile);
        // and initialize it
        gObject->logger.get()->Initialize();
      }

      // 4.)  At this time, we run the initialization pipeline
      int g = InitializeCommon(gObject);
      if (g !=1) return -4;

      return 1;
    }//end of function
  }//end of namespace hemocalc
}//end of namespace spin
