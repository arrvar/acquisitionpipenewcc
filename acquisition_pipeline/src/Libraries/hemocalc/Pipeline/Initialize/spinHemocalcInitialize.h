#ifndef SPINHEMOCALCINITIALIZE_H_
#define SPINHEMOCALCINITIALIZE_H_
#include "Framework/Exports/spinAPI.h"

namespace spin
{
  namespace hemocalc
  {
    /*! \brief A base class for initializing all objects/parameters
     *  associated with spinVista
     */
    class SPIN_API spinHemocalcInitialize
    {
      public:
        // Constructor
        spinHemocalcInitialize(){}
        // API to initialize
        int Initialize(void* obj, const char* param);
      private:
        int ParseParasiteTags(void* v1, void* obj);
        int ParseWBCDiffTags(void* v1, void* obj);
        int ParseRBCDiffTags(void* v1, void* obj);
    };//end of class
  }//end of namespace hemocalc
}//end of namespace
#endif
