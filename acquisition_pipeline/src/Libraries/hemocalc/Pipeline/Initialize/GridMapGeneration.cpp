#include "Libraries/hemocalc/Pipeline/Initialize/GridMapGeneration.h"
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

namespace spin
{
  namespace hemocalc
  {
    /*! \brief CreateOutputFolder
     *  This function is used to create an output folder associated with
     *  every AOI that needs to be processed
     */
    std::string GridMapGeneration::CreateOutputFolder(std::string& v1, std::string& img)
    {
      std::string folder = "";
      // Create the unique output folder
      std::vector<std::string> temp;
      boost::split(temp, img, boost::is_any_of("."));
      folder = v1+"/"+temp[0];
      temp.clear();
      boost::filesystem::path dir(folder.c_str());
      if (!boost::filesystem::exists(dir))
      {
        if (!boost::filesystem::create_directory(dir))
        {
          folder="";
        }
      }
      else
      {
        // This directory exists, we delete the existing directory and then
        // create it again
        boost::filesystem::remove_all(dir);
        if (!boost::filesystem::create_directory(dir))
        {
          folder="";
        }
      }
      return folder;
    }//end of function

    /*! \brief GridMapGeneration
     *  This function parses through a folder containing images and saves it
     *  in a grid map structure. We assume the following directory structure
     *  for hemocalc:
     *  Slide:
     *   ---Grid
     *       ---- 1
     *       ---- 2
     */
    int GridMapGeneration::Initialize(std::string& primaryFolder, \
                                      std::vector<std::string>& secondaryFolders, \
                                      std::string& outputFolder)
    {
      // Sanity checks
      boost::filesystem::path inDir(primaryFolder.c_str());
      if (!boost::filesystem::exists(inDir))
      {
        return -1;
      }

      // Now, we check if the output folder is present. If it already exists, we will
      // delete it and create a new one.
      boost::filesystem::path dir(outputFolder.c_str());
      if (!boost::filesystem::exists(dir))
      {
        if (!boost::filesystem::create_directory(dir))
        {
          return -2;
        }
      }
      else
      {
        // This directory exists, we delete the existing directory and then
        // create it again
        boost::filesystem::remove_all(dir);
        if (!boost::filesystem::create_directory(dir))
        {
          return -2;
        }
      }

      if (secondaryFolders.size() > 0)
      {
        // List all images present in the folder using a boost iterator
        // Ideally, if an image is present is folder1, it should also
        // be present in folder2. If not, then the folder is not
        // added to the gridstructure
        int count = 0;
        boost::filesystem::directory_iterator it(primaryFolder), eod;
        BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))
        {
          if(boost::filesystem::is_regular_file(p))
          {
            // First check if this image is present in all other folders
            std::vector<std::string> temp;
            boost::split(temp, p.string(), boost::is_any_of("/"));
            std::string imgName = temp[temp.size()-1];
            temp.clear();
            // In addition, we need to create an output folder corresponding to this image name
            std::string v = CreateOutputFolder(outputFolder, imgName);
            if (v =="") return -3;

            // Place this image as a primary image
            aoiInfo[std::make_pair(0,count)].img_Primary   = primaryFolder+"/"+imgName;
            aoiInfo[std::make_pair(0,count)].out_Folder    = v;

            // Check if this image is also present in all secondary folders
            for (int j = 0; j < secondaryFolders.size(); ++j)
            {
              std::string img2 = secondaryFolders[j]+"/"+imgName;
              if (!boost::filesystem::exists(img2)) return -4;

              // Place this in a secondary folder
              aoiInfo[std::make_pair(0,count)].img_Secondary.push_back(img2);
            }
            ++count;
          }
        }
      }
      else
      {
        // List all images present in the folder using a boost iterator
        // Ideally, if an image is present is folder1, it should also
        // be present in folder2. If not, then the folder is not
        // added to the gridstructure
        int count = 0;
        boost::filesystem::directory_iterator it(primaryFolder), eod;
        BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))
        {
          if(boost::filesystem::is_regular_file(p))
          {
            // First check if this image is present in all other folders
            std::vector<std::string> temp;
            boost::split(temp, p.string(), boost::is_any_of("/"));
            std::string imgName = temp[temp.size()-1];
            temp.clear();
            // In addition, we need to create an output folder corresponding to this image name
            std::string v = CreateOutputFolder(outputFolder, imgName);
            if (v =="") return -3;

            // Place this image as a primary image
            aoiInfo[std::make_pair(0,count)].img_Primary   = primaryFolder+"/"+imgName;
            aoiInfo[std::make_pair(0,count)].out_Folder    = v;
            ++count;
          }
        }
      }

      return 1;
    }//end of function

    /*! \brief Clear
    *  This function is used to clear residual information from the data structure*/
    void GridMapGeneration::Clear()
    {
      aoiInfo.clear();
    }//end of function
  }//end of namespace stitching
}//end of namespace spin


