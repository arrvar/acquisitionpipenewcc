#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcInitialize.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <algorithm>

namespace spin
{
  namespace hemocalc
  {
    /*! \brief ParseWBCDiffTags
     *  This function is used to parse information pertaining to WBC differential
     */
    int spinHemocalcInitialize::ParseWBCDiffTags(void* _v1, void* obj)
    {
      // Here, we downcast a default object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      if (gObject == NULL) return -1;

      using boost::property_tree::ptree;
      ptree::value_type* v1 = static_cast<ptree::value_type*>(_v1);
      if (!v1) return -2;

      if (!gObject->wbcParams) return -3;

      // Now, parse through all parameter settings and populate gObject
      BOOST_FOREACH(ptree::value_type& v2, v1->second)
      {
        if (v2.first == "MaskGeneration")
        {
          gObject->wbcParams->computeMask = true;
          // All parameters pertaning to mask generation
          BOOST_FOREACH(ptree::value_type& v3, v2.second)
          {
            if (v3.first == "color_space")
            {
              gObject->wbcParams->colorSpace = v3.second.get<int>("");
            }
            else
            if (v3.first == "channel")
            {
              gObject->wbcParams->channel = v3.second.get<int>("");
            }
            else
            if (v3.first == "useWC")
            {
              gObject->wbcParams->useWCImage = true;
            }
            else
            if (v3.first == "thresholding_algorithm")
            {
              gObject->wbcParams->thresholdAlgorithm = v3.second.get<int>("");
            }
            else
            if (v3.first == "strel_preprocess")
            {
              gObject->wbcParams->strel_preprocess = v3.second.get<int>("");
            }
            else
            if (v3.first == "gaussian_sigma")
            {
              gObject->wbcParams->sigma = v3.second.get<float>("");
            }
            else
            if (v3.first == "platelet_eccentricity")
            {
              gObject->wbcParams->platelet_eccentricity = v3.second.get<float>("");
            }
            else
            if (v3.first == "platelet_minsize")
            {
              gObject->wbcParams->platelet_min_size = v3.second.get<int>("");
            }
            else
            if (v3.first == "platelet_maxsize")
            {
              gObject->wbcParams->platelet_max_size = v3.second.get<int>("");
            }
            else
            if (v3.first == "platelet_median_filter")
            {
              gObject->wbcParams->platelet_postprocess_medianFilter = v3.second.get<int>("");
            }
            else
            if (v3.first == "strel_postprocess_wbc")
            {
              gObject->wbcParams->wbc_postprocess_morphology = v3.second.get<int>("");
            }
            else
            if (v3.first == "wbc_minsize")
            {
              gObject->wbcParams->wbc_min_size = v3.second.get<int>("");
            }
            else
            if (v3.first == "wbc_maxsize")
            {
              gObject->wbcParams->wbc_max_size = v3.second.get<int>("");
            }
            else
            if (v3.first == "wbc_median_filter")
            {
              gObject->wbcParams->wbc_postprocess_medianFilter = v3.second.get<int>("");
            }
            else
            if (v3.first == "save_mask")
            {
              gObject->wbcParams->saveMask = true;
            }
          }
        }
        else
        if (v2.first == "Recognition")
        {
          // All parameters related with WBC recognition
          BOOST_FOREACH(ptree::value_type& v3, v2.second)
          {
            // All types of WBC's (major classes) and udentified
            BOOST_FOREACH(ptree::value_type& v4, v3.second)
            if (v3.first == "Neutrophil")
            {
              if (v3.first == "Spectral")
              {
              }
              else
              if (v3.first == "Spatial")
              {
              }
            }
            else
            if (v3.first == "Lymphocyte")
            {
              if (v3.first == "Spectral")
              {
              }
              else
              if (v3.first == "Spatial")
              {
              }
            }
            else
            if (v3.first == "Monocyte")
            {
              if (v3.first =="Spectral")
              {
              }
              else
              if (v3.first == "Spatial")
              {
              }
            }
            else
            if (v3.first == "Eosinophil")
            {
              if (v3.first == "Spectral")
              {
              }
              else
              if (v3.first == "Spatial")
              {
              }
            }
            else
            if (v3.first == "Basophil")
            {
              if (v3.first == "Spectral")
              {
              }
              else
              if (v3.first == "Spatial")
              {
              }
            }
            else
            if (v3.first == "Unidentified")
            {
              if (v3.first == "Spectral")
              {
              }
              else
              if (v3.first == "Spatial")
              {
              }
            }
          }
        }
      }
      return 1;
    }//end of function
  }//end of namespace
}//end of namespace

