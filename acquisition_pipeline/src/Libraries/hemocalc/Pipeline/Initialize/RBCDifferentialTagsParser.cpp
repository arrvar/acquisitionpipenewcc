#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcInitialize.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <algorithm>

namespace spin
{
  namespace hemocalc
  {
    /*! \brief ParseWBCDiffTags
     *  This function is used to parse information pertaining to RBC differential
     */
    int spinHemocalcInitialize::ParseRBCDiffTags(void* _v1, void* obj)
    {
      // Here, we downcast a default object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      if (gObject == NULL) return -1;

      using boost::property_tree::ptree;
      ptree::value_type* v1 = static_cast<ptree::value_type*>(_v1);
      if (!v1) return -2;

      if (!gObject->rbcParams) return -3;

      // Now, parse through all parameter settings and populate gObject
      BOOST_FOREACH(ptree::value_type& v2, v1->second)
      {
        if (v2.first == "MaskGeneration")
        {
          gObject->rbcParams->computeMask = true;
          // All parameters pertaning to mask generation
          BOOST_FOREACH(ptree::value_type& v3, v2.second)
          {
            if (v3.first == "color_space")
            {
              gObject->rbcParams->colorSpace = v3.second.get<int>("");
            }
            else
            if (v3.first == "channel")
            {
              gObject->rbcParams->channel = v3.second.get<int>("");
            }
            else
            if (v3.first == "thresholding_algorithm")
            {
              gObject->rbcParams->thresholdAlgorithm = v3.second.get<int>("");
            }
            else
            if (v3.first == "rbc_minsize")
            {
              gObject->rbcParams->rbc_min_size = v3.second.get<int>("");
            }
            else
            if (v3.first == "rbc_maxsize")
            {
              gObject->rbcParams->rbc_max_size = v3.second.get<int>("");
            }
            else
            if (v3.first == "rbc_median_filter")
            {
              gObject->rbcParams->rbc_postprocess_medianFilter = v3.second.get<int>("");
            }
            else
            if (v3.first == "save_mask")
            {
              gObject->rbcParams->saveMask = true;
            }
          }
        }
      }

      return 1;
    }//end of function

  }//end of namespace
}//end of namespace

