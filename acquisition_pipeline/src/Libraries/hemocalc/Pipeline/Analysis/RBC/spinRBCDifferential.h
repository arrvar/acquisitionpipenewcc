#ifndef SPINHEMOCALCRBCDIFFERNTIAL_H_
#define SPINHEMOCALCRBCDIFFERNTIAL_H_

// Pipeline includes
#include <memory>
#include "AbstractClasses/AbstractCallback.h"
#include "Framework/Exports/spinAPI.h"

namespace spin
{
  // Forward declaration of Producer Consumer class
  template <class P>
  class MPC;

  namespace hemocalc
  {
    // Forward declaration
    class DatabaseIO;
    template <class P> class RBCDifferentialInterface;
    template <class P> class RBCDifferentialPipeline;

    /*! \brief
     * This class acts as the interface with the main pipline and RBC differential
     */
    template <class P>
    class SPIN_API SpinRBCDifferential
    {
      public:
        /*! \brief Destructor*/
        ~SpinRBCDifferential();

        /*! \brief InstantiatePipeline*/
        int InstantiatePipeline(void* obj);

        /*! \brief ProcessPipeline*/
        int ProcessPipeline(P* producerDataPoint, void* obj);

        /*! \brief CompleteProcessing*/
        int CompleteProcessing();

        /*! \brief AbortPipeline*/
        int AbortPipeline();

        // signal to indicate finish of process
        spin::CallbackSignal     sig_Finished;
      private:
        bool pause;
        // Create objects for the producer level, broker and consumer level
        std::shared_ptr< RBCDifferentialInterface<P> > mRBCDifferentialInterface;
        std::shared_ptr< RBCDifferentialPipeline<P> >  mRBCDifferentialPipeline;
        std::shared_ptr< spin::MPC<P> > mMPC;
	std::shared_ptr<DatabaseIO> db;
        /*! \brief ConnectSignals*/
        int ConnectSignals(void* obj);
        /*! \brief Pause producer */
        void PauseProducer(bool state);
        /*! \brief RelaySignal*/
        void RelaySignal(spin::CallbackStruct p);
      };
  }//end of namespace hemocalc
}//end of namespace spin
#endif
