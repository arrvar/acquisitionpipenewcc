// Pipeline includes
#include "Libraries/hemocalc/Pipeline/Analysis/RBC/RBCDifferentialPipeline.h"
#include "Libraries/hemocalc/Pipeline/ImageWriter/spinHemocalcImageWriter.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include "Utilities/Pipelines/RBCDifferential/RBCMaskGeneration.h"

namespace spin
{
  namespace hemocalc
  {
    /*! \brief Destructor */
    template<class P>
    RBCDifferentialPipeline<P>::~RBCDifferentialPipeline()
    {
      // Call the destructor in the sub routines as well
      AbortPipeline();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void RBCDifferentialPipeline<P>::RelayLogs(spin::CallbackStruct p)
    {
      // Push the log
      sig_Logs(p);
    }//end of function

    /*! \brief RelaySignalFinished
     *  A slot to relay an incoming signal
     */
    template <class P>
    void RBCDifferentialPipeline<P>::RelaySignal(spin::CallbackStruct p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void RBCDifferentialPipeline<P>::RelayMetrics(spin::CallbackStruct p)
    {
      // Push the log
      sig_Metrics(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline.
     */
    template<class P>
    int RBCDifferentialPipeline<P>::InstantiatePipeline(void* obj)
    {
      // Instantiate the next set of pipelines, if requested
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);

      // Initialize objects that will be used
      // a.) Mask generation
      maskGeneration = std::make_shared<spin::RBCMaskGeneration>();
      // TODO: b.) RBC x-part differential etc.,
      return 1;
    }//end of function

    /*! \brief ProcessPipeline
     * Processes the data that is input from the source. Internally it pushes
     * the data into an appropriate data structure and then to a queue for
     * any consumer to pick it up.
     * parameters :
     * @P: The pointer to a data point from the producer. It contains
     *     an image that will need to be processed
     */
    template<class P>
    int RBCDifferentialPipeline<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      typedef std::pair<int,int> pIndex;

      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      GridMapGeneration* gmap = gObject->gMap.get();
      std::map<pIndex, FOVStruct>* dFov = const_cast<std::map<pIndex, FOVStruct>*>(gmap->GetGridMapping());
      pIndex it(producerDataPoint->gYRow, producerDataPoint->gXCol);

      // The steps of the RBC differential pipeline
      if (gObject->rbcParams->computeMask)
      {
        // 1.) Mask generation
        if (maskGeneration->RunPipeline(&(producerDataPoint->rgbImagePrimary), \
                                        NULL, \
                                        gObject->rbcParams.get(), \
                                        &((*dFov)[it].Masks),\
                                        (*dFov)[it].img_Primary,\
                                        gObject->outDir, \
                                        gObject->file_extension_write,\
                                        gObject->rbcParams->saveMask) !=1) return -1;
        // 2.) Recognition (recognition) TODO
      }
      else
      {
        // Something else? Maybe computing RBC differential without mask generation
      }
      return 1;
    }//end of function

    /*! \brief ProcessData
     * Chains up the process pipelines that need to be sequenced for the data
     * point
    */
    template <class P>
    void RBCDifferentialPipeline<P>::ProcessData(P dataPoint, void* obj)
    {
      // Run the pre-processing pipeline
      int v = ProcessPipeline(&dataPoint, obj);
    }//end of function

    /*! \brief CompleteProcessing [SLOT]
     * This function ensures that processing is finished
     */
    template <class P>
    void RBCDifferentialPipeline<P>::CompleteProcessing(void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      // If the pipeline has to abort then there's no point in forwarding a
      // signal that requests the other consumers to proceed
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        return;
      }
      abort_mutex.unlock();

      if(t_Process.joinable())
      {
        // Join the thread now
        t_Process.join();
      }
      return;
    }//end of function

    /*! \brief BeginProcessingQueue [SLOT]
     * This function Sends a request to the queue in the broker for data
     */
    template <class P>
    void RBCDifferentialPipeline<P>::BeginProcessingQueue(void* obj)
    {
      // downcast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);
      GridMapGeneration* gmap = gObject->gMap.get();

      // This function is moved into another thread
      int n_images = 0;
      // Get the number of images that need to be processed
      int max_images = gmap->GetGridMapping()->size();

      while(n_images < max_images)
      {
        // If the abort signal was called, exit the loop
        abort_mutex.lock();
        if (abort)
        {
          abort_mutex.unlock();
          break;
        }
        abort_mutex.unlock();

        if(t_RBCDiff.joinable())
        {
          t_RBCDiff.join();
          ++n_images;
        }
        else
        {
          sig_GetData();
          boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
      }
    }//end of function

    /*! \brief ConsumeFromQueue [SLOT]
     * This function receives a signal from the MPC with a new data point
     */
    template <class P>
    void RBCDifferentialPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
    {
      // Process the data point in a separate thread
      t_RBCDiff = boost::thread(&RBCDifferentialPipeline<P>::ProcessData,this,dataPoint,obj);
    }//end of function

    /*! \brief RequestQueue [SLOT]
    * This function receives a signal from the MPC that a new datapoint is
    * available for consumption
    */
    template <class P>
    void RBCDifferentialPipeline<P>::RequestQueue(void* obj)
    {
      if(!t_Process.joinable())
      {
        t_Process = boost::thread(&RBCDifferentialPipeline<P>::BeginProcessingQueue,this,obj);
      }
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts the pipelines and cleanly exits
     */
    template <class P>
    void RBCDifferentialPipeline<P>::AbortPipeline()
    {
      abort_mutex.lock();
      abort  = true;
      abort_mutex.unlock();
      AbortOperations();
    }//end of function

    /*! \brief AbortOperations
     *	This function forwards the abort signal to other consumers and
     *	deallocates any memory objects that are part of this class alone.
     *	This however does not destroy the configuration for which this
     *  pipeline was primed with. Thereby enabling reacquisition instantly.
     */
    template <class P>
    void RBCDifferentialPipeline<P>::AbortOperations()
    {
    }//end of function

    // explicit instantiation of template class
    template class RBCDifferentialPipeline<AnalysisDataType>;
  }//end of namespace hemocalc
}//end of namespace spin
