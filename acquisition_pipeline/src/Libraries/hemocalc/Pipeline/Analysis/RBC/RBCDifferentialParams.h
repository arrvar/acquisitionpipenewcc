#ifndef RBCDIFFERENTIALPARAMS_H_
#define RBCDIFFERENTIALPARAMS_H_

namespace spin
{
  namespace hemocalc
  {
    struct RBCDifferentialParams
    {
      int   colorSpace                        = 1;    // LAB
      int   channel                           = 1;    // a channel
      int   thresholdAlgorithm                = 0;    // Otsu
      int   rbc_postprocess_medianFilter      = 3;    // 7x7 median filter
      int   rbc_max_size                      = 8000; // 5K pixels at 100x
      int   rbc_min_size                      = 4000; // 4k pixels at 100x
      bool  saveMask                          = false;
      bool  computeMask                       = true;
    };//end of struct
  }//end of namespace hemocalc
}//end of namespace spin
#endif
