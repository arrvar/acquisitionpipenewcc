#ifndef WBCDIFFERENTIALPARAMS_H_
#define WBCDIFFERENTIALPARAMS_H_

namespace spin
{
  namespace hemocalc
  {
    struct WBCDifferentialParams
    {
      int   colorSpace                        = 0;    // LRGB
      int   channel                           = 1;    // G channel
      int   thresholdAlgorithm                = 0;    // Otsu
      int   strel_preprocess                  = 7;    // opening/closing/dilation/erosion size
      float sigma                             = 4.0;  // Gaussian smoothing variance
      float platelet_eccentricity             = 0.8;  // nearly circular objects
      int   platelet_min_size                 = 200;  // 200 pixels
      int   platelet_max_size                 = 800;  // 800 pixels
      int   platelet_postprocess_medianFilter = 2;    // 5x5 median filter
      int   wbc_postprocess_morphology        = 3;    // strel of 3
      int   wbc_postprocess_medianFilter      = 3;    // 7x7 median filter
      int   wbc_max_size                      = 35000;// 30k pixels at 100x
      int   wbc_min_size                      = 4000;// 4k pixels at 100x
      bool  saveMask                          = false;
      bool  computeMask                       = true;
      bool  useWCImage                        = false;
    };//end of struct
  }//end of namespace hemocalc
}//end of namespace spin
#endif
