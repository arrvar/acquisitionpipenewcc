#include "Libraries/hemocalc/Pipeline/Analysis/WBC/WBCDifferentialInterface.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  namespace hemocalc
  {
    /*! \brief AddToQueue
     *  This function adds an object to the WBC differential queue
     */
    template<class P>
    void WBCDifferentialInterface<P>::AddToQueue(void* dataPoint)
    {
      // typecasts
      AnalysisDataType* dp = static_cast<AnalysisDataType*>(dataPoint);
        
      //emit the signal
      sig_AddQ(*dp);      
    }//end of function
    
    // explicit instantiation of template class
    template class WBCDifferentialInterface<AnalysisDataType>;
  }//end of hemocalc namespace
}//end of spin namespace
