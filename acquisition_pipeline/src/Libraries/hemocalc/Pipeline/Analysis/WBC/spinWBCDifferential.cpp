#include "Libraries/hemocalc/Pipeline/Analysis/WBC/spinWBCDifferential.h"
#include "Libraries/hemocalc/Pipeline/Analysis/WBC/WBCDifferentialInterface.h"
#include "Libraries/hemocalc/Pipeline/Analysis/WBC/WBCDifferentialPipeline.h"
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include "Framework/MPC/MPC.h"
#include "Framework/Logger/LoggerInterface.h"

namespace spin
{
  namespace hemocalc
  {
    /*! \brief PauseProducer
     *  enables a boolean to either pause or resume the producer
     */
    template <class P>
    void SpinWBCDifferential<P>::PauseProducer(bool state)
    {
      pause = state;
    }//end of function

    /*! \brief ConnectSignals
     *  Connects signals and slots for all the members of this class here
     */
    template<class P>
    int SpinWBCDifferential<P>::ConnectSignals(void* obj)
    {
      // Connect the corresponding signals and slots
      mMPC.get()->sig_DataAvailable.connect(boost::bind(&WBCDifferentialPipeline<P>::RequestQueue,mWBCDifferentialPipeline,obj));
      mMPC.get()->sig_DataPoint.connect(boost::bind(&WBCDifferentialPipeline<P>::ConsumeFromQueue,mWBCDifferentialPipeline, _1, _2));
      mMPC.get()->sig_FinAcq.connect(boost::bind(&WBCDifferentialPipeline<P>::CompleteProcessing,mWBCDifferentialPipeline,obj));
      mMPC.get()->sig_Abort.connect(boost::bind(&WBCDifferentialPipeline<P>::AbortPipeline,mWBCDifferentialPipeline));
      mMPC.get()->sig_QueueBurdened.connect(boost::bind(&SpinWBCDifferential<P>::PauseProducer,this, _1));

      mWBCDifferentialInterface.get()->sig_AddQ.connect(boost::bind(&MPC<P>::AddToQueue, mMPC, _1));
      mWBCDifferentialInterface.get()->sig_FinAcq.connect(boost::bind(&MPC<P>::Finished, mMPC));
      mWBCDifferentialInterface.get()->sig_Abort.connect(boost::bind(&MPC<P>::Abort, mMPC));
      mWBCDifferentialPipeline.get()->sig_GetData.connect(boost::bind(&MPC<P>::PopFromQueue, mMPC, obj));
      mWBCDifferentialPipeline.get()->sig_Finished.connect(boost::bind(&SpinWBCDifferential<P>::RelaySignal, this, _1));

      // Typecast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);

      // Connect the logging signal
      if (gObject->loggingInitialized)
      {
        mMPC.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
        mWBCDifferentialPipeline.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
        mWBCDifferentialPipeline.get()->sig_Finished.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
      }

      return 0;
    }//end of function

    /*! \brief Destructor
     *  Clears all objects, joins all threads and exits
     */
    template<class P>
    SpinWBCDifferential<P>::~SpinWBCDifferential()
    {
      CompleteProcessing();
    }//end of function

    /*! \brief RelayLogs
     *  A slot to relay an incoming signal
     */
    template <class P>
    void SpinWBCDifferential<P>::RelaySignal(spin::CallbackStruct p)
    {
      sig_Finished(p);
    }//end of function

    /*! \brief InstantiatePipeline
     *  Instantiates the pipeline. Creates the objects and makes all the
     *  connections if necessary. It does not configure any data structure or
     *  set any constants as they would be dependent on the grid that is being
     *  @obj      : The spinVistaAcquisitionObject that has been initialized prior
     *              to calling this function
     */
    template<class P>
    int SpinWBCDifferential<P>::InstantiatePipeline(void* obj)
    {
      // The producer can be either a camera or a File I/O interface
      mWBCDifferentialInterface = std::make_shared<WBCDifferentialInterface<P> >();

      // The consumer can be any process that can receive this data structure
      mWBCDifferentialPipeline = std::make_shared<WBCDifferentialPipeline<P> >();

      // Both the producer and consumer communicate with each other via a Broker
      mMPC = std::make_shared<MPC<P> >();

      // Typecast the object
      spinHemocalcObject* gObject = static_cast<spinHemocalcObject*>(obj);

      // Connect the logging signal
      if (gObject->loggingInitialized)
      {
        mMPC.get()->EnableLogging();
      }

      // Instantiate the pipelines (lightweight in nature)
      mWBCDifferentialPipeline.get()->InstantiatePipeline(obj);

      // Connect all the signals and slots
      ConnectSignals(obj);

      return 0;
    }//end of function

    /*! \brief CompleteProcessing
     *  Completes the pipeline till the end and cleanly exits
     */
    template<class P>
    int SpinWBCDifferential<P>::CompleteProcessing()
    {
      // Send a signal to indicate end of acquisition
      mWBCDifferentialInterface.get()->sig_FinAcq();

      return 0;
    }//end of function

    /*! \brief ProcessPipeline
     *  Processes the data that is input from the source. Internally it pushes
     *  the data into an appropriate data structure and then to a queue for
     *  any consumer to pick it up.
     */
    template<class P>
    int SpinWBCDifferential<P>::ProcessPipeline(P* producerDataPoint, void* obj)
    {
      // Push this data point into a queue, but check if this is in a paused state
      while(pause)
      {
        boost::this_thread::sleep(boost::posix_time::milliseconds(5));
      }

      // Push this data point into a queue
      mWBCDifferentialInterface.get()->AddToQueue(producerDataPoint);

      return 1;
    }//end of function

    /*! \brief AbortPipeline
     *  Aborts all operations and cleanly exits
     */
    template<class P>
    int SpinWBCDifferential<P>::AbortPipeline()
    {
      // Send a signal to indicate end of acquisition
      mWBCDifferentialInterface.get()->sig_Abort();

      return 0;
    }//end of function

    // explicit instantiation of template class
    template class SpinWBCDifferential<AnalysisDataType>;
  }//end of hemocalc namespace
}//end of spin namespace
