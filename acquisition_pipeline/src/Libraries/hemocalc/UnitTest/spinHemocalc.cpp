#include <iostream>
#include <cstdlib>
#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include "Libraries/hemocalc/Interface/hemocalcInterface.h"
#include <chrono>

/*! \brief
 * This is the main driver to run a hematology pipeline
 */
int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    //std::cout<< argv[0] << std::endl;
    //std::cout << " <path_to_parameter_file.xml>" << std::endl;
    exit(-1);
  }

  auto startProc = std::chrono::high_resolution_clock::now();
  // Get the name of the parameter file
  char* pFile = argv[1];
  // Instantiate a hemocalcInterface Object
  hemocalcInterface hIf;
  // And initialize it
  int v = hIf.Initialize(pFile);
  if (v !=1) return -1;

  // Get an access to the global object
  typedef spin::hemocalc::spinHemocalcObject sVObject;
  std::shared_ptr<sVObject> pObject = hIf.GetGObject();

  // Loop through all images that need to be processed
  typedef std::map<spin::hemocalc::pIndex, spin::hemocalc::FOVStruct> FOV;
  FOV* dFov = const_cast<FOV*>(pObject.get()->gMap.get()->GetGridMapping());
  FOV::iterator it = dFov->begin();
  while(it != dFov->end())
  {
    // Get the input image that will be processed
    std::string inpImage = it->second.img_Primary;
    //std::cout<<"Processing image: "<<inpImage<<std::endl;

    // Get its col, row indices
    int gx = (it->first.second);
    int gy = (it->first.first);
    // Process it; By default we assume that it is a fg image
    hIf.Process(inpImage,gx, gy);
    // increment the iterator
    ++it;
  }
  // Complete the process
  hIf.CompleteProcessing();

  auto endProc = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsedA = endProc - startProc;
  //std::cout << "Analysis Time: " << elapsedA.count() << std::endl;
  return 0;
}//end of function
