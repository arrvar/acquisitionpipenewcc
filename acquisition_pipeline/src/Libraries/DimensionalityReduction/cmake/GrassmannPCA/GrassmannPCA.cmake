#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building spin${PROJ}_GrassmannPCA
#------------------------------------------------------------------------------
FILE(GLOB_RECURSE GPCA_SRCS
${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/GrassmannPCA/*.cpp 
${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/GrassmannPCA/*.h
${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/GrassmannPCA/*.hpp
${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/GrassmannPCA/*.txx
)

ADD_LIBRARY(spinDR_GrassmannPCA SHARED ${GPCA_SRCS} $<TARGET_OBJECTS:dr_common> )
TARGET_LINK_LIBRARIES(spinDR_GrassmannPCA ${${PROJ}_EXT_LIBS})
SET_TARGET_PROPERTIES(spinDR_GrassmannPCA PROPERTIES  PREFIX "")
