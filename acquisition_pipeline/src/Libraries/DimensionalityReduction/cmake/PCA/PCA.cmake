#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building spin${PROJ}_PCA
#------------------------------------------------------------------------------
SET(PCA_SRCS
${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/PCA/PCA.cpp 
)

ADD_LIBRARY(spinDR_PCA SHARED ${PCA_SRCS} $<TARGET_OBJECTS:dr_common> )
TARGET_LINK_LIBRARIES(spinDR_PCA ${${PROJ}_EXT_LIBS})
SET_TARGET_PROPERTIES(spinDR_PCA PROPERTIES  PREFIX "")
