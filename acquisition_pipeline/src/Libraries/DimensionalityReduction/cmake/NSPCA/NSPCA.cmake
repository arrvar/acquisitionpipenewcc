#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building spin${PROJ}_NSPCA
#------------------------------------------------------------------------------
SET(NSPCA_SRCS
${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/NSPCA/NSPCA.cpp 
)

ADD_LIBRARY(spinDR_NSPCA SHARED ${NSPCA_SRCS} $<TARGET_OBJECTS:dr_common> )
TARGET_LINK_LIBRARIES(spinDR_NSPCA ${${PROJ}_EXT_LIBS})
SET_TARGET_PROPERTIES(spinDR_NSPCA PROPERTIES  PREFIX "")
