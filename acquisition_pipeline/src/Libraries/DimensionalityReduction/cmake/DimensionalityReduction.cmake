#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building vista
#------------------------------------------------------------------------------
SET(PROJ DimensionalityReduction)
SET(${PROJ}_EXT_LIBS)

#------------------------------------------------------------------------------
#Include the third party libraries needed 
#------------------------------------------------------------------------------
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/TPL_Linux.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

#------------------------------------------------------------------------------
#Build a set of files that will be used across the board
#------------------------------------------------------------------------------
SET(DR_SRCS_COMMON
  ${PROJECT_SOURCE_DIR}/Utilities/MatrixUtils/EigenUtils.h 
  ${PROJECT_SOURCE_DIR}/Utilities/MatrixUtils/RandomizedSVD.h 
  ${PROJECT_SOURCE_DIR}/Utilities/MatrixUtils/RandomizedSVD.cpp
  ${PROJECT_SOURCE_DIR}/Common/AbstractClasses/AbstractMatrixTypes.h 
  ${PROJECT_SOURCE_DIR}/Common/AbstractClasses/DRObject.h 
  ${PROJECT_SOURCE_DIR}/Common/AbstractClasses/DRParams.h
  ${PROJECT_SOURCE_DIR}/Common/AbstractClasses/AbstractDimensionalityReduction.h 
  ${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/AbstractDimensionalityReduction.cpp
  ${PROJECT_SOURCE_DIR}/Common/DataIO/EigenHdf5IO.h
  ${PROJECT_SOURCE_DIR}/Common/DataIO/eigenHdf5.h
  ${PROJECT_SOURCE_DIR}/Common/DataIO/EigenDataStructure.h
  ${PROJECT_SOURCE_DIR}/Common/DataIO/FileStructure.h
  ${PROJECT_SOURCE_DIR}/Libraries/DataIO/Interface/DataIOInterface.h
)
ADD_LIBRARY(dr_common OBJECT ${DR_SRCS_COMMON})

#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building 
#DimensionalityReduction
#------------------------------------------------------------------------------
SET(DR_SRCS
${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/DimensionalityReductionInterface.h
${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/DimensionalityReductionInterface.cpp
)

#------------------------------------------------------------------------------
#Build the dimensionality reduction interface library
#------------------------------------------------------------------------------
ADD_LIBRARY(spinDR SHARED ${DR_SRCS} $<TARGET_OBJECTS:dr_common> )
TARGET_LINK_LIBRARIES(spinDR ${${PROJ}_EXT_LIBS})
SET_TARGET_PROPERTIES(spinDR PROPERTIES  PREFIX "")
#------------------------------------------------------------------------------
#List all the modules within DR that are implemented
#------------------------------------------------------------------------------
SET(DR_PLUGINS
  PCA
  GrassmannPCA
  NSPCA
  LaplacianEigenmap
)

FOREACH(plugin ${DR_PLUGINS})
  INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/${plugin}/${plugin}.cmake)
ENDFOREACH()

#------------------------------------------------------------------------------
#Build a C++ Unit Test
#------------------------------------------------------------------------------
SET(ut_spin${PROJ} OFF CACHE BOOL "Build C++ Unit tests for ${PROJ} Module")
IF(ut_spin${PROJ})
  ADD_EXECUTABLE(ut_spin${PROJ} ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/DRTest.cpp)
  TARGET_LINK_LIBRARIES(ut_spin${PROJ} spinDR ${${PROJ}_EXT_LIBS} spinDataIO)
ENDIF()
