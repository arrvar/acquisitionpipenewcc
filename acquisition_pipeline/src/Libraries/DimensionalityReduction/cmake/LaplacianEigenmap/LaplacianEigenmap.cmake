#--------------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building Laplacian Eigenmap
#--------------------------------------------------------------------------------------
SET(LE_SRCS
  ${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/LaplacianEigenmap/LaplacianEigenmap.cpp 
  ${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/LaplacianEigenmap/AbstractKernelLaplacianEigenmap.h
)

ADD_LIBRARY(spinDR_LE SHARED ${LE_SRCS} $<TARGET_OBJECTS:dr_common> )
TARGET_LINK_LIBRARIES(spinDR_LE ${${PROJ}_EXT_LIBS})
SET_TARGET_PROPERTIES(spinDR_LE PROPERTIES  PREFIX "")

#--------------------------------------------------------------------------------------
#List out plugins to be built
#--------------------------------------------------------------------------------------
SET(LE_Kernels
AdditiveSymmetricChiSquaredDivergence
)

#--------------------------------------------------------------------------------------
#Build each plugin
#--------------------------------------------------------------------------------------
FOREACH(kernel ${LE_Kernels})
  ADD_LIBRARY(spinDR_LE_${kernel} SHARED ${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/LaplacianEigenmap/Kernels/${kernel}.cpp 
                                              ${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/LaplacianEigenmap/AbstractKernelLaplacianEigenmap.h
                                              ${PROJECT_SOURCE_DIR}/Utilities/MatrixUtils/EigenUtils.h
                                              $<TARGET_OBJECTS:dr_common> )
  TARGET_LINK_LIBRARIES(spinDR_LE_${kernel} ${${PROJ}_EXT_LIBS})
  SET_TARGET_PROPERTIES(spinDR_LE_${kernel} PROPERTIES  PREFIX "")
ENDFOREACH()