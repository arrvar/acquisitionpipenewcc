#------------------------------------------------------------------------------
#Plugin for Additive symmetric chi-squared distribution
#------------------------------------------------------------------------------
SET(PROJ_X ASCD)

#------------------------------------------------------------------------------
#Build the library
#------------------------------------------------------------------------------
ADD_LIBRARY(spinLEKernel_${PROJ_X}_plugin SHARED 
            ${PROJECT_SOURCE_DIR}/Utilities/ML/DimensionalityReduction/LaplacianEigenmap/Kernels/AdditiveSymmetricChiSquaredDivergence.cpp
            $<TARGET_OBJECTS:LECommon> )
            
TARGET_LINK_LIBRARIES(spinLEKernel_${PROJ_X}_plugin ${${PROJ}_EXT_LIBS})
SET_TARGET_PROPERTIES(spinLEKernel_${PROJ_X}_plugin PROPERTIES  PREFIX "")
