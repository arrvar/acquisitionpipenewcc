#ifndef DIMENSIONALITYREDUCTIONINTERFACE_H_
#define DIMENSIONALITYREDUCTIONINTERFACE_H_
#include "Framework/Exports/spinAPI.h"
#include <string>
#include <memory>

namespace
{
  // Forward declaration
  template <class T>
  struct DRModule;
}//end of namespace

/*! \brief The interface to the dimensionality reduction pipeline*/
template <class T>
class SPIN_API DimensionalityReductionInterface
{
  public:
    // default constructor
    DimensionalityReductionInterface(){}

    // default destructor
    ~DimensionalityReductionInterface(){}

    /*! \brief Initialize */
    int Initialize(void* _params, std::string path=".", bool f = false);

    /*! \brief Compute Projection*/
    int ComputeProjectionMatrix(T* _cInput, bool p = false);

    /*! \brief Project new data*/
    int ProjectNewData(T* _cInput, T* _cOutput);

    /*! \brief Get Data */
    int GetData(void* _data);
  private:
    std::shared_ptr< DRModule<T> > module;
};//end of class
#endif
