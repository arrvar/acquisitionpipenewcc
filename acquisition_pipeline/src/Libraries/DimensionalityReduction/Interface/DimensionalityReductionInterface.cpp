#include "Libraries/DimensionalityReduction/Interface/DimensionalityReductionInterface.h"
#include "AbstractClasses/AbstractDimensionalityReduction.h"
#include "AbstractClasses/DRObject.h"
#include "AbstractClasses/DRParams.h"
#include <dlibxx.hxx>
#include <boost/filesystem.hpp>
#include <iostream>

namespace
{
  template <class T>
  struct DRModule
  {
    typedef spin::AbstractDimensionalityReduction<T> DRType;
    ~DRModule()
    {
      // Check if the library was loaded successfully.
      lib.get()->close();
    }
    std::shared_ptr<dlibxx::handle> lib;
    std::shared_ptr< DRType > pModule;
  };//end of struct
}//end of namespace

/*! \brief Initialize
 *  This function intializes a DimensionalityReduction module either
 *  during training, or during out-of-sample projection
 *  @_params    : The parameters (during training), or projection matrix
 *                object (during prediction).
 *  @f          : true(during training) or false(default: prediction)
 */
template <class T>
int DimensionalityReductionInterface<T>::Initialize(void* _params, std::string path, bool f)
{
  typedef typename ::DRModule<T>::DRType DRType;
  std::string libname = "";
  int type = 1;

  if (f)
  {
    // initialization is happening during training
    spin::DRParams* params = static_cast<spin::DRParams*>(_params);
    if (!params) return -1;
    // Frame the algorithm
    libname = path+"/spinDR_"+params->algo+".so";
    type = params->type;
  }
  else
  {
    // initialization is happening during prediction
    spin::DRObject<T>* dObj = static_cast<spin::DRObject<T>*>(_params);
    if (!dObj) return -1;
    // Frame the algorithm
    libname = path+"/spinDR_"+dObj->algorithm+".so";
    type = dObj->type;
  }

  // Check if the library exists in the given path
  boost::filesystem::path dfile(libname.c_str());
  if (!boost::filesystem::exists(dfile))
  {
    return -2; // the file does not exis
  }

  // Instantiate a DRModule object
  module = std::make_shared< ::DRModule<T> >();
  // And Instantiate the loader
  module.get()->lib = std::make_shared<dlibxx::handle>();
  // Resolve symbols when they are referenced instead of on load.
  module.get()->lib.get()->resolve_policy(dlibxx::resolve::lazy);
  // Make symbols available for resolution in subsequently loaded libraries.
  module.get()->lib.get()->set_options(dlibxx::options::global | dlibxx::options::no_delete);
  // Load the library specified.
  module.get()->lib.get()->load(libname);
  // Check if the library could be loaded
  if (!module.get()->lib.get()->loaded()) return -3;

  // Now, we try and create the plugin
  if (f)
  {
    // Create a copy of the object.
    spin::DRParams* params = static_cast<spin::DRParams*>(_params);

    std::shared_ptr<spin::DRParams> tParams = std::make_shared<spin::DRParams>(params);

    module.get()->pModule = module.get()->lib.get()->template create<DRType>("DR_Plugin", &type, tParams.get(), &f);
  }
  else
  {
    // initialization is happening during prediction
    spin::DRObject<T>* dObj = static_cast<spin::DRObject<T>*>(_params);

    // Make a copy of the object
    std::shared_ptr< spin::DRObject<T> > drObj = std::make_shared<spin::DRObject<T> >(dObj);

    module.get()->pModule = module.get()->lib.get()->template create<DRType>("DR_Plugin", &type, drObj.get(), &f);
  }

  // Check if the module was loaded
  if (!module.get()->pModule.get()) return -4;
  // For some modules this is a pass through
  int v = module.get()->pModule.get()->InitializeKernel(f);
  if (v !=1) return -5;

  return 1;
}//end of function

/*! \brief ComputeProjection
 *  This function is used to compute a projection matrix associated with
 *  any given dimensionality reduction algorithm
 *  @cInput             : The input matrix used for computing the projection matrix.
 *                        The # of features should always be more than feature length.
 *  @computeProjection  : Do we need to transform the training data? (default: false)
 *  @serialize          : Are we serializing?
 */
template <class T>
int DimensionalityReductionInterface<T>::ComputeProjectionMatrix( T* cInput, \
                                                                  bool computeProjection)
{
  // Do some sanity checks
  if (!cInput->data()) return -1;
  // Also we make sure that the # of data points > feature vector length
  if (cInput->rows() < cInput->cols()) return -2;

  // Compute the projectionMatrix
  int v = module.get()->pModule.get()->ComputeProjectionMatrix(cInput, computeProjection);
  if (v !=1) return -3;

  return 1;
}//end of function

/*! \brief GetData
 *  Having computed the projection matrix, we use this function to
 *  copy a DRObject to another object that has been defined, externally
 */
template <class T>
int DimensionalityReductionInterface<T>::GetData(void* _data)
{
  spin::DRObject<T>* data = static_cast<spin::DRObject<T>*>(_data);
  if (!data) return -1;

  // get the data from the current computation
  (*data) = module.get()->pModule.get()->GetData();
  return 1;
}//end of function

/*! \brief ProjectNewData
 *  This function is used to project new data given a projection matrix
 */
template <class T>
int DimensionalityReductionInterface<T>::ProjectNewData(T* cInput, T* cOutput)
{
  // Sanity checks
  if (!module.get()->pModule.get()) return -1;
  if (!cInput->data()) return -2;

  // Project the data and save it to output
  int v = module.get()->pModule.get()->ProjectNewData(cInput, cOutput);
  if (v !=1) return -3;

  return 1;
}//end of function

// explicit template instantiations
template class DimensionalityReductionInterface<spin::RMMatrix_Float>;
template class DimensionalityReductionInterface<spin::RMMatrix_Double>;
template class DimensionalityReductionInterface<spin::CMMatrix_Float>;
template class DimensionalityReductionInterface<spin::CMMatrix_Double>;
