#include "Libraries/DataIO/Interface/DataIOInterface.h"
#include "Libraries/DimensionalityReduction/Interface/DimensionalityReductionInterface.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "AbstractClasses/DRParams.h"
#include "DataIO/EigenHdf5IO.h"
#include "DataIO/EigenDataStructure.h"
#include "DataIO/FileStructure.h"
#include <iostream>


int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    //std::cout<<argv[0]<<" <DataFile.h5>"<<std::endl;
    return -1;
  }

  typedef spin::RMMatrix_Float Matrix;
  typedef spin::EigenDataStructure<Matrix> data;
  std::string p = std::string(argv[1]);
  std::shared_ptr< DataIOInterface<Matrix> > dataIO = std::make_shared< DataIOInterface<Matrix> >(p, false);

  // Read all tags
  dataIO.get()->ReadData();

  // Get access to the data
  std::shared_ptr<data> pData = std::make_shared<data>();
  dataIO.get()->GetData(pData.get());

  // Now, we instantiate the dimensionality reduction pipeline
  std::shared_ptr< DimensionalityReductionInterface<Matrix> > dR = std::make_shared< DimensionalityReductionInterface<Matrix> >();

  // Create an instance of DRParams. This will ideally be present in another global object
  std::shared_ptr<spin::DRParams> params = std::make_shared<spin::DRParams>();
  params.get()->algo    = "PCA";
  params.get()->samples = pData.get()->Input.rows();
  params.get()->rank    = 4;

  // Now, we try to initialize the pipeline by loading the plugin
  int h = dR.get()->Initialize(params.get(), ".", true);
  //std::cout<<"loaded Module: "<<h<<std::endl;
  // Compute the projection matrix
  if (h==1)
  {
    h = dR.get()->ComputeProjectionMatrix(&pData.get()->Input,true);
    //std::cout<<"Computed projection Matrix: "<<h<<std::endl;
  }
  else
  {
    //std::cout<<"Module not loaded"<<std::endl;
  }

  return 1;
}
