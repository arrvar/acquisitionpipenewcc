#include <iostream>
#include <cstdlib>
#include <chrono>
#include "opencv2/opencv.hpp"
#include <dlibxx.hxx>
#include "boost/filesystem.hpp"
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "AbstractClasses/AbstractPlugin.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include <memory>
#include "itkImageFileWriter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkCastImageFilter.h"
//#include <dlibxx.hxx>
//#include <iostream>
//#include <boost/filesystem.hpp>
//#include <memory>
//#include <chrono>

/*! \brief
 * This is the main driver to generate a pyramid
 */
int main(int argc, char* argv[])
{ 
  if (argc < 3)
  {
    //std::cout<< argv[0] << std::endl;
    //std::cout << "<path to module> <input image path> <path_to_xml_file>" << std::endl;
    exit(-1);
  }
  std::string libname = std::string(argv[1]);
  boost::filesystem::path dfile(libname.c_str());
  if (!boost::filesystem::exists(dfile))
  {
    //std::cout<<"Library does not exist"<<std::endl;
    return -2; 
  }
  std::shared_ptr<dlibxx::handle>lib = std::make_shared<dlibxx::handle>();
  lib->resolve_policy(dlibxx::resolve::lazy);
//  lib->set_options(dlibxx::resolve::lazy);
  lib->set_options(dlibxx::options::global);
  lib->load(libname);
  if(!lib->loaded())
  {
    //std::cout<<"Library could not be loaded: "<<lib->error()<<std::endl;
    return -3;
  }
  std::shared_ptr<spin::AbstractPlugin> cModule = lib->template create<spin::AbstractPlugin>("Processing_Plugin");
  if (!cModule.get())
  {
    //std::cout<<"Module could not be initialized: "<<std::endl;
    return -4;
  }
//  boost::filesystem::path pfile(argv[2]);
//  if(!boost::filesystem::exists(pfile))
//  {
//    //std::cout<<"Parameter file does not exist"<<std::endl;
//    return -5; 
//  }  
  
  cv::Mat a1;
  int v = cModule->InstantiatePipeline(argv[3], &a1);
  if (v != 1)
  {
    //std::cout<<"Parameter file could not be parsed"<<std::endl;
    return -5; 
  }
  char* inputImagePath = argv[2]; 
  typedef itk::RGBPixel<float> RGBPixelType;
  typedef itk::Image<RGBPixelType, 3>RGBInptImage;
  typedef itk::RGBPixel<unsigned char>OutputType;
  typedef itk::Image<OutputType, 3>OutputImageType;
  typedef itk::ImageFileReader<RGBInptImage>ReaderType;
  typedef itk::ImageFileWriter<OutputImageType>WriterType;
  
  RGBInptImage::Pointer out_img = RGBInptImage::New();
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(inputImagePath);
  reader->Update();
  RGBInptImage::Pointer in_img = RGBInptImage::New();
  in_img = reader->GetOutput();
//  typedef itk::CastImageFilter< RGBInptImage, OutputImageType >FilterType;
//  FilterType::Pointer filter = FilterType::New();
//  filter->SetInput( reader->GetOutput() );
////  
////  
  char* outPath = "ProcessedImg.png";
//  WriterType::Pointer writer = WriterType::New();
//  writer->SetFileName(outPath);
//  writer->SetInput(filter->GetOutput());
//  writer->Write();
  
  
//  cv::Mat inputImg = cv::imread(inputImagePath, CV_LOAD_IMAGE_COLOR); 
  cv::Mat outMask;
//  void* inputImg;
//  inputImg = input;
//  cModule->In
  //std::cout<<"Before process"<<std::endl;
//  cModule->ProcessPipeline(&inputImg,outPath, &outMask);
  cModule->ProcessPipeline(&in_img,outPath, &out_img);
  //std::cout<<"Before process"<<std::endl;
}//end of function
