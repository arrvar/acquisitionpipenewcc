#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building colorDeconvolution
#------------------------------------------------------------------------------
SET (PROJCD ColorDeconvolution)
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJCD}/cmake/TPL_Linux.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

#------------------------------------------------------------------------------
#Check if a C++ Unit Test is requested
#------------------------------------------------------------------------------
SET(ut_spin${PROJCD} OFF CACHE BOOL "Build C++ Unit test for ${PROJCD} Module")
IF(ut_spin${PROJCD}) 
    ADD_EXECUTABLE(ut_spin${PROJCD} ${PROJECT_SOURCE_DIR}/Libraries/${PROJCD}/UnitTest/ColorDeconvolution_UnitTest.cpp)
    SET_TARGET_PROPERTIES(ut_spin${PROJCD} PROPERTIES  COMPILE_FLAGS)
    TARGET_LINK_LIBRARIES(ut_spin${PROJCD} ${${PROJCD}_EXT_LIBS} -ldl)
ENDIF()


	

	
	
