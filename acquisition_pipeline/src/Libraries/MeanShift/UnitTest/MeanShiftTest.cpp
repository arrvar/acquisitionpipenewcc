#include "Utilities/ImageProcessing/MeanShift/MeanShift.h"
#include "opencv2/opencv.hpp"
#include <string>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc < 6)
  {
    //std::cout<<argv[0]<<" <ImgName> <ImgOut>"<<std::endl;
    return -1;
  }
  
  std::string input     = std::string(argv[1]);
  std::string output    = std::string(argv[2]);
  cv::Mat inputImage = cv::imread(input);
  cv::Mat outputImage;
  float space_sigma     = std::stod(argv[3]);
  float range_sigma     = std::stod(argv[4]);
  float pers_thr        = std::stod(argv[5]);


  spin::MeanShift ms;
  ms.Compute(&inputImage, space_sigma, range_sigma, pers_thr, &outputImage);
  cv::imwrite(output, outputImage);

  return 1;
}//end of function
