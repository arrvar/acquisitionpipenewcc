#ifndef FOCUSFUSIONINTERFACE_H_
#define FOCUSFUSIONINTERFACE_H_
#include "Framework/Exports/spinAPI.h"
#include <string>

namespace spin
{
  class SPIN_API FocusFusionInterface
  {
    public:
      FocusFusionInterface(){}

      /*! \brief Compute */
      int Compute(void* _cImg, int levels, int type, void* _img);
  };//end of class
}
#endif
