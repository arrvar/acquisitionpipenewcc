#define PY_ARRAY_UNIQUE_SYMBOL pbcvt_ARRAY_API
#include <vector>
#include <string>
#include <memory>
#include <boost/python.hpp>
#include <boost/python/numeric.hpp>
#include <boost/python/tuple.hpp>
#include <numpy/ndarrayobject.h>
#include <numpy/noprefix.h>

#include <pyboostcvconverter/pyboostcvconverter.hpp>
#include "AbstractClasses/AbstractImageTypes.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/eigen.hpp"
#include "Libraries/FocusFusion/Interface/FocusFusionInterface.h"

namespace pbcvt
{
  using namespace boost::python;

  /*! \brief an Interface class between python and c++*/
  class FocusStack
  {
    public:
      FocusStack()
      {
        red.clear();
        green.clear();
        blue.clear();
      }
      
      ~FocusStack()
      {
        red.clear();
        green.clear();
        blue.clear();
      }
          
      /*! \brief This function converts an OpenCV mat to a RGBTriplet */
      void c2LAB(PyObject* image, spin::RGBTriplet* channels)
      {
        cv::Mat inputImage = pbcvt::fromNDArrayToMat(image);
        cv::Mat convMat;
        inputImage.convertTo(convMat,CV_32FC3);
        convMat *= 1./255;
        // conversion of BGR image to LAB
        cv::cvtColor(convMat, convMat, CV_BGR2Lab);
        // Split the channels
        cv::Mat dummy;
        std::vector<cv::Mat> vec(3,dummy);
        cv::split(convMat,vec);
        // Convert each image to an eigen matrix 
        channels->c1 = spin::RMMatrix_Float::Zero(vec[0].rows, vec[0].cols);
        channels->c2 = spin::RMMatrix_Float::Zero(vec[0].rows, vec[0].cols);
        channels->c3 = spin::RMMatrix_Float::Zero(vec[0].rows, vec[0].cols);
        cv::cv2eigen(vec[0],channels->c1); // L
        cv::cv2eigen(vec[1],channels->c2); // a
        cv::cv2eigen(vec[2],channels->c3); // b
        vec.clear();
      }//end of function
      
      /*! \brief This function is used to push an image from python to c++ */
      void PushImage(PyObject* image)
      {
        //Extract the channels
        spin::RGBTriplet triplet;
        c2LAB(image, &triplet);
        //place all channels in respective stacks 
        red.push_back(triplet.c1);
        green.push_back(triplet.c2);
        blue.push_back(triplet.c3);
      }//end of function
      
      // This function is used to run the focus stacking algorithm
      void Compute(int levels, char* path)
      {
        if (red.size() > 1 && green.size() > 1 && blue.size() > 1)
        {
          std::shared_ptr<FocusFusionInterface> fitf = std::make_shared<FocusFusionInterface>();
          std::string outImg = std::string(path); 
          fitf.get()->Compute(&red,&green,&blue,levels,outImg);
        }
      }//end of function
      
    private:
      std::vector<spin::RMMatrix_Float> red;
      std::vector<spin::RMMatrix_Float> green;
      std::vector<spin::RMMatrix_Float> blue;
  };//end of class 


  #if (PY_VERSION_HEX >= 0x03000000)

    static void *init_ar() 
    {
  #else
    static void init_ar()
    {
  #endif
        Py_Initialize();

        import_array();
        return NUMPY_IMPORT_ARRAY_RETVAL;
    }

  /*! \brief This is the main python wrapper for the online stitching pipeline*/
  BOOST_PYTHON_MODULE(py_spinFocusFusion)
  {
    //using namespace XM;
    init_ar();

    //initialize converters
    to_python_converter<cv::Mat, pbcvt::matToNDArrayBoostConverter>();
    pbcvt::matFromNDArrayBoostConverter();
    
    boost::python::numeric::array::set_module_and_type("numpy", "ndarray");

    boost::python::class_<FocusStack, boost::noncopyable>("FocusFusionInterface")
      .def("PushImage", &FocusStack::PushImage)
      .def("Compute", &FocusStack::Compute);
  };//end of function
}//end of namespace
