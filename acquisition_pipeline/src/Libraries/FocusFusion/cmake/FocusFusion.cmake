#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building vista
#------------------------------------------------------------------------------
SET(PROJ FocusFusion)
SET(${PROJ}_EXT_LIBS)

#------------------------------------------------------------------------------
#Include the third party libraries needed
#------------------------------------------------------------------------------
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/TPL_Linux.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common
                    ${PROJECT_SOURCE_DIR}/Common/pyboostcvconverter/include)

#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building
#vistaAcquisition
#------------------------------------------------------------------------------
SET(${PROJ}_LIB_DIRS
    Common/AbstractClasses
    Common/Framework/Exports
    Utilities/ImageProcessing/ITKImageUtils
    Utilities/ImageProcessing/WaveletTransform
    Utilities/ImageProcessing/WaveletUtils/ImageFusion
    Utilities/MatrixUtils
)

SET(${PROJ}_Utilities_SRCS)
FOREACH(CONTENT ${${PROJ}_LIB_DIRS})
  FILE(GLOB_RECURSE U_SRCS
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.cpp
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.hh
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.h
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.txx)
  SET(${PROJ}_Utilities_SRCS ${${PROJ}_Utilities_SRCS} ${U_SRCS})
ENDFOREACH()

#------------------------------------------------------------------------------
# Build a shared library so that other projects can link into it
#------------------------------------------------------------------------------
SET(fItf ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/FocusFusionInterface.cpp
         ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/FocusFusionInterface.h)
ADD_LIBRARY(spin${PROJ} SHARED ${fItf} ${${PROJ}_Utilities_SRCS})

TARGET_LINK_LIBRARIES(spin${PROJ} ${${PROJ}_EXT_LIBS})

#------------------------------------------------------------------------------
#Build a C++ Unit Test
#------------------------------------------------------------------------------
SET(ut_spin${PROJ} OFF CACHE BOOL "Build C++ Unit tests for ${PROJ} Module")
IF(ut_spin${PROJ})
  ADD_EXECUTABLE(ut_spin${PROJ} ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/FocusFusionTest.cpp)
                                #${PROJECT_SOURCE_DIR}/Common/pyboostcvconverter/src/pyboost_cv3_converter.cpp)
  TARGET_LINK_LIBRARIES(ut_spin${PROJ} spin${PROJ} ${${PROJ}_EXT_LIBS})
ENDIF()

#------------------------------------------------------------------------------
#Build a python wrapper
#------------------------------------------------------------------------------
#ADD_LIBRARY(py_spin${PROJ} SHARED ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/FocusFusionInterfacePython.cpp
#            ${PROJECT_SOURCE_DIR}/Common/pyboostcvconverter/src/pyboost_cv3_converter.cpp)
#TARGET_LINK_LIBRARIES(py_spin${PROJ} ${${PROJ}_EXT_LIBS} spin${PROJ})
#SET_TARGET_PROPERTIES(py_spin${PROJ} PROPERTIES  PREFIX "")
