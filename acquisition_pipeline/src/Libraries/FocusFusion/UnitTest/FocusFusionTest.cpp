#if 1
#include "Libraries/FocusFusion/Interface/FocusFusionInterface.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <vector>
#include <string>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc < 4)
  {
    //std::cout<<argv[0]<<" <inpImageFolder> <levels> <outImageName>"<<std::endl;
    return -1;
  }

  std::string refImgFolder = std::string(argv[1]);
  int levels = atoi(argv[2]);
  std::string outImg = std::string(argv[3]);

  // We scan the ref image folder. It may contain either a single image or
  // multiple images.
  boost::filesystem::path dir(refImgFolder);
  if (!boost::filesystem::exists(dir))
  {
    return -1;
  }

  typedef spin::RMMatrix_Float Matrix;
  typedef std::vector<Matrix> vMat;
  typedef std::vector<vMat> vImage;
  vImage tImages;
  spin::RGBImageType::RegionType region;
  // Now, for each image in the folder we run the pipeline
  boost::filesystem::directory_iterator it(refImgFolder), eod;
  BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))
  {
    if(boost::filesystem::is_regular_file(p))
    {
      // Get the name of the image
      std::string refImg = p.string();
      //std::cout<<"Reading: "<<refImg<<std::endl;
      spin::RGBImageType::Pointer pImg = spin::RGBImageType::New();
      spin::ReadITKImage<spin::RGBImageType>(refImg, pImg);
      region = pImg->GetLargestPossibleRegion();
      //Convert the image to a vector of matrices
      vMat tMat;
      spin::Image2VectorOfMatrices<float>(&pImg, &tMat);
      // Place this vector in a global vector of Images
      tImages.push_back(tMat);
    }
  }

  // Allocate memory for the fused image
  spin::RGBImageType::Pointer pOut = spin::RGBImageType::New();
  pOut->SetRegions(region);
  pOut->Allocate();
  // Run the focus fusion test
  spin::FocusFusionInterface fitf;
  int g = fitf.Compute(&tImages,levels,1,&pOut);
  if (g !=1)
  {
    //std::cout<<"Error in Image fusion: "<<g<<std::endl;
  }
  // Save the fused image
  spin::WriteITKImage<spin::RGBImageType>(outImg, pOut);
  return 1;
}//end of function
#else
#define PY_ARRAY_UNIQUE_SYMBOL pbcvt_ARRAY_API
#include <iostream>
#include <pyboostcvconverter/pyboostcvconverter.hpp>
#include <numpy/arrayobject.h>
#include <numpy/ndarrayobject.h>
#include <opencv2/opencv.hpp>

int main(int argc, char* argv[])
{
  cv::Mat img = cv::imread(argv[1]);
  Py_Initialize();
  try
  {
    PyObject *pImg = pbcvt::fromMatToNDArray(img);
    cv::Mat test = pbcvt::fromNDArrayToMat(pImg);
  }
  catch(...)
  {
    //std::cout<<"Exception"<<std::endl;
  }
  return 1;
}
#endif
