#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building vista
#------------------------------------------------------------------------------
SET(PROJ DataIO)
SET(${PROJ}_EXT_LIBS)

#------------------------------------------------------------------------------
#Include the third party libraries needed 
#------------------------------------------------------------------------------
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/TPL_Linux.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

#------------------------------------------------------------------------------
#Build a set of files that will be used across the board
#------------------------------------------------------------------------------
SET(DATAIO_COMMON
  ${PROJECT_SOURCE_DIR}/Common/AbstractClasses/AbstractMatrixTypes.h 
  ${PROJECT_SOURCE_DIR}/Common/DataIO/EigenHdf5IO.h
  ${PROJECT_SOURCE_DIR}/Common/DataIO/EigenHdf5IO.cpp
  ${PROJECT_SOURCE_DIR}/Common/DataIO/eigenHdf5.h
  ${PROJECT_SOURCE_DIR}/Common/DataIO/EigenDataStructure.h
  ${PROJECT_SOURCE_DIR}/Common/DataIO/FileStructure.h
  ${PROJECT_SOURCE_DIR}/Common/ModelIO/ModelIO.h
  ${PROJECT_SOURCE_DIR}/Common/ModelIO/ModelIO.cpp
  ${PROJECT_SOURCE_DIR}/Common/ModelIO/SerializedModel.h
)

#------------------------------------------------------------------------------
#Build DataIO shared library
#------------------------------------------------------------------------------
ADD_LIBRARY(dataio_common OBJECT ${DATAIO_COMMON})
ADD_LIBRARY(spin${PROJ} SHARED  ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/DataIOInterface.cpp 
                                ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/DataIOInterface.h
                                ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/ModelIOInterface.cpp 
                                ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/Interface/ModelIOInterface.h
                                $<TARGET_OBJECTS:dataio_common> )
TARGET_LINK_LIBRARIES(spin${PROJ} ${${PROJ}_EXT_LIBS})
SET_TARGET_PROPERTIES(spin${PROJ} PROPERTIES  PREFIX "")

#------------------------------------------------------------------------------
#Build a C++ Unit Test
#------------------------------------------------------------------------------
SET(ut_spin${PROJ} OFF CACHE BOOL "Build C++ Unit tests for ${PROJ} Module")
IF(ut_spin${PROJ})
  ADD_EXECUTABLE(ut_spin${PROJ} ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/DataIOTest.cpp)
  TARGET_LINK_LIBRARIES(ut_spin${PROJ} spin${PROJ} spinDR ${${PROJ}_EXT_LIBS})
ENDIF()
