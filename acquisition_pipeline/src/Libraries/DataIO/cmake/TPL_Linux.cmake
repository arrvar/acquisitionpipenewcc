#-----------------------------------------------------------------------------------#
#Boost
#-----------------------------------------------------------------------------------#
SET(Boost_LIBS
    filesystem
    thread
    date_time
    chrono
    system
    timer
    serialization
    iostreams
    python3
)
FIND_PACKAGE(Boost REQUIRED COMPONENTS "${Boost_LIBS}")
#Here, we add a global definition indicating that only dynamic linking is possible
#Also make sure that /MD flags are set for all your CMake projects
#https://svn.boost.org/trac/boost/ticket/6644
ADD_DEFINITIONS(-DBOOST_ALL_DYN_LINK )
#Include the path of the boost include directory
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
#and the library directories
LINK_DIRECTORIES(${Boost_LIBRARY_DIR})
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${Boost_LIBRARIES})

#-----------------------------------------------------------------------------------#
#Eigen
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(Eigen3 REQUIRED)
INCLUDE_DIRECTORIES(${EIGEN3_INCLUDE_DIR})

#-----------------------------------------------------------------------------------#
#Python3
#-----------------------------------------------------------------------------------#
SET(Python_ADDITIONAL_VERSIONS 3.5 3.6)
FIND_PACKAGE(PythonLibs 3 REQUIRED)
INCLUDE_DIRECTORIES(${PYTHON_INCLUDE_PATH})
ADD_DEFINITIONS(-DNPY_NO_DEPRECATED_API=NPY_1_7_API_VERSION)
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${PYTHON_LIBRARIES})

#-----------------------------------------------------------------------------------#
#DLIBXX
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(DLIBXX REQUIRED)
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${DLIBXX_LIBRARIES})

#-----------------------------------------------------------------------------------#
#HDF5
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(HDF5New)
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${HDF5New_LIBRARIES})

INCLUDE(${PROJECT_SOURCE_DIR}/cmake/CMake_Flags.cmake)
