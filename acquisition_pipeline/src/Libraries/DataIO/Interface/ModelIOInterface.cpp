#include "Libraries/DataIO/Interface/ModelIOInterface.h"
#include "ModelIO/ModelIO.h"

namespace
{
  struct ModelIOStruct
  {
    typedef spin::ModelIO dataStruct;
    ModelIOStruct(std::string& p, bool train, int type)
    {
      dataIO = std::make_shared<dataStruct>();
      dataIO.get()->Initialize(p,train,type);
    }
    ~ModelIOStruct(){}
    std::shared_ptr<dataStruct> dataIO;
  };//end of struct
}//end of namespace

/*! \brief The default constructor
 *  This function instantiates a DataIOStruct object
 */
ModelIOInterface::ModelIOInterface(std::string& p, bool train, int type)
{
  model = std::make_shared<::ModelIOStruct>(p,train,type);
}//end of function

/*! \brief Serialize an object to the model
 */
int ModelIOInterface::Serialize(const char* tag, void* obj)
{
  return model.get()->dataIO.get()->Serialize(tag, obj);
}//end of function

/*! \brief Deserialize an object to the model
 */
int ModelIOInterface::Deserialize(const char* tag, void* obj)
{
  return model.get()->dataIO.get()->Deserialize(tag, obj);
}//end of function

/*! \brief This function reads data from a previously opened file
 */
int ModelIOInterface::SaveModel(std::string& p)
{
  return model.get()->dataIO.get()->SaveModel(p);
}//end of function
