#include "Libraries/DataIO/Interface/DataIOInterface.h"
#include "DataIO/EigenHdf5IO.h"

namespace
{
  struct DataIOStruct
  {
    typedef spin::EigenHdf5IO dataStruct;
    DataIOStruct(std::string& p, bool overwrite)
    {
      dataIO = std::make_shared<dataStruct>(p, overwrite);
    }
    ~DataIOStruct(){}
    std::shared_ptr<dataStruct> dataIO;
  };//end of struct
}//end of namespace

/*! \brief The default constructor
 *  This function instantiates a DataIOStruct object
 */
DataIOInterface::DataIOInterface(std::string& p, bool overwrite)
{
  data = std::make_shared< ::DataIOStruct>(p,overwrite);
}//end of function

/*! \brief Create a group in a file. Note, the required data structures
 *  should have been initialized prior to calling this function
 */
int DataIOInterface::CreateGroup(std::string& group)
{
  return data.get()->dataIO.get()->CreateGroup(group);
}//end of function

/*! \brief This function reads data from a previously opened file
 */
int DataIOInterface::ReadData(void* tags, int type, void* _data)
{
  return data.get()->dataIO.get()->ReadData(tags, type, _data);
}//end of function

/*! \brief This function reads data from a previously opened file
 */
int DataIOInterface::WriteData(std::string& tag, int type, void* _data)
{
  return data.get()->dataIO.get()->WriteData(tag, type, _data);
}//end of function
