#ifndef DATAIOINTERFACE_H_
#define DATAIOINTERFACE_H_
#include "Framework/Exports/spinAPI.h"
#include <string>
#include <memory>

namespace 
{
  struct DataIOStruct;
}//end of namespace

/*! \brief An interface class for reading/writing data from HDF5 files*/
class SPIN_API DataIOInterface
{
  public:
    /*! \brief default constructor */
    DataIOInterface(std::string& p, bool overwrite);
    
    /*! \brief CreateGroup */
    int CreateGroup(std::string& group);
    
    /*! \brief Read Tags */
    int ReadData(void* tags, int type, void* data);
    
    /*! \brief Write Data */
    int WriteData(std::string& tag, int type, void* data);
    
  private:
    std::shared_ptr< DataIOStruct > data; 
};//end of class
#endif
