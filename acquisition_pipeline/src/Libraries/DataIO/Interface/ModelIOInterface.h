#ifndef MODELIOINTERFACE_H_
#define MODELIOINTERFACE_H_
#include "Framework/Exports/spinAPI.h"
#include <string>
#include <memory>

namespace
{
  struct ModelIOStruct;
}//end of namespace

/*! \brief An interface class for reading/writing model from/to HDF5 files*/
class SPIN_API ModelIOInterface
{
  public:
    /*! \brief default constructor */
    ModelIOInterface(std::string& p, bool train, int type);

    /*! \brief Serialize */
    int Serialize(const char* tag, void* obj);

    /*! \brief Deserialize */
    int Deserialize(const char* tag, void* obj);

    /*! \brief SaveModel */
    int SaveModel(std::string& p);

  private:
    std::shared_ptr< ::ModelIOStruct > model;
};//end of class
#endif
