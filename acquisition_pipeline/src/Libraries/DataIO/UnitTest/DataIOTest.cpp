#include "Libraries/DataIO/Interface/DataIOInterface.h"
#include "Libraries/DimensionalityReduction/Interface/DimensionalityReductionInterface.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "AbstractClasses/DRParams.h"
#include "AbstractClasses/DRObject.h"
#include "DataIO/EigenHdf5IO.h"
#include "DataIO/EigenDataStructure.h"
#include "DataIO/FileStructure.h"
#include "ModelIO/ModelIO.h"
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc < 4)
  {
    //std::cout<<argv[0]<<" <DataFile.h5> <Method> <model.h5>"<<std::endl;
    return -1;
  }

  typedef spin::RMMatrix_Float Matrix;
  int type = 1;

  typedef spin::EigenDataStructure<Matrix> data;
  std::string p = std::string(argv[1]);
  std::shared_ptr< DataIOInterface > dataIO = std::make_shared< DataIOInterface >(p, false);

  // Create a data structure to parse the data from a H5 file
  std::shared_ptr< data > ds = std::make_shared<data>();

  // Read data (we parse all tags present in a conformant data file)
  if (dataIO.get()->ReadData(NULL,type,ds.get()))
  {
    // Data was successfully loaded
    // We now instantiate a model object
    std::shared_ptr<spin::ModelIO> model = std::make_shared<spin::ModelIO>();
    std::string q = std::string(argv[3]);
    // Initialize the model
    model.get()->Initialize(q, true, type);

    // Now, Instantiate a dimensionality reduction pipeline
    std::shared_ptr< DimensionalityReductionInterface<Matrix> > dR = std::make_shared< DimensionalityReductionInterface<Matrix> >();
    // Create an instance of DRParams. This will ideally be present in another global object
    std::shared_ptr<spin::DRParams> params = std::make_shared<spin::DRParams>();
    params.get()->algo    = argv[2];
    params.get()->samples = ds.get()->Input.rows();
    params.get()->rank    = 4;

    ////std::cout<<ds.get()->Input<<std::endl;
    ////std::cout<<"================="<<std::endl;
    // Now, we try to initialize the pipeline by loading the plugin
    if (dR.get()->Initialize(params.get(), ".", true) ==1)
    {
      // Compute the projection matrix and serialize it
      if (dR.get()->ComputeProjectionMatrix(&ds.get()->Input,true) ==1 )
      {
        // We serialize the projection matrix
        spin::DRObject<Matrix> drObj;
        dR.get()->GetData(&drObj);
        //std::cout<<"During encoding: "<<std::endl;
        //std::cout<<&drObj<<std::endl;
        //std::cout<<"===="<<std::endl;
        model.get()->Serialize("DR",&drObj);
      }
      else
      {
        //std::cout<<"Projection Matrix could not be computed"<<std::endl;
        return -3;
      }

      // Save the model
      model.get()->SaveModel(q);
    }
    else
    {
      //std::cout<<"Module could not be loaded "<<std::endl;
      return -1;
    }
  }
  else
  {
    //std::cout<<"Data could not be read "<<std::endl;
    return -2;
  }

  // We now load the data saved in the model, deserialize it and subsequently
  std::string q = std::string(argv[3]);
  std::shared_ptr<spin::ModelIO> model2 = std::make_shared<spin::ModelIO>();
  if (model2.get()->Initialize(q,false))
  {
    // We could read the model. Let us try and deserialize the objects in the model
    spin::DRObject<Matrix> drObj;
    model2.get()->Deserialize("DR",&drObj);
    //std::cout<<"During decoding: "<<std::endl;
    //std::cout<<&drObj<<std::endl;
    //std::cout<<"===="<<std::endl;
  }
  return 1;
}
