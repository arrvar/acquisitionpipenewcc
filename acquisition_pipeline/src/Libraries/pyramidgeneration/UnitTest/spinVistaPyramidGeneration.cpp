#include <iostream>
#include <cstdlib>
#include <chrono>

#include "Utilities/Stitching/Pyramid/PyramidGeneration.h"

int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    std::cout<< argv[0] << std::endl;
    std::cout << " <path_to_tile_directory> <1-run/0-don't run>" << std::endl;
    std::cout << "Pyramid Generation: 0.0" << std::endl;
    return 0;
  }
  
  auto startProc = std::chrono::high_resolution_clock::now();
  // Get the name of the parameter file
  
  std::string tileDir = std::string(argv[1]);
  int runPyrGen = std::stoi(std::string(argv[2]));

  if(runPyrGen == 1)
  {
    spin::PyramidGeneration pyr;
    float kernel = 1;
    float strength = 0.6;
    bool sharpness = false;
    int p = pyr.Generate(tileDir,sharpness,kernel,strength);
    if (p != 1) return p;
    auto endProc = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsedA = endProc - startProc;
    std::cout << "Pyramid Generation: " << elapsedA.count() << std::endl;
  }
  else
  {
    std::cout << "Pyramid Generation: 0.0" << std::endl;
  }

  return 0;
}
