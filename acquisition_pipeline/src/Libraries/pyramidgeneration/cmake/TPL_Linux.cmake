#-----------------------------------------------------------------------------------#
#Boost
#-----------------------------------------------------------------------------------#
SET(Boost_LIBS
    filesystem
    thread
    date_time
    chrono
    system
    timer
    serialization
)
FIND_PACKAGE(Boost REQUIRED COMPONENTS "${Boost_LIBS}")
#Here, we add a global definition indicating that only dynamic linking is possible
#Also make sure that /MD flags are set for all your CMake projects
#https://svn.boost.org/trac/boost/ticket/6644
ADD_DEFINITIONS(-DBOOST_ALL_DYN_LINK )
#Include the path of the boost include directory
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
#and the library directories
LINK_DIRECTORIES(${Boost_LIBRARY_DIR})
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${Boost_LIBRARIES})


#-----------------------------------------------------------------------------------#
#OpenCV
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(OpenCV REQUIRED)
INCLUDE_DIRECTORIES(${OpenCV_INCLUDE_DIRS})
SET(${PROJ}_EXT_LIBS ${${PROJ}_EXT_LIBS} ${OpenCV_LIBS} ${OpenCV_LIBRARIES})
