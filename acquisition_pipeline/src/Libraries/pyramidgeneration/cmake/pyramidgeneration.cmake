#------------------------------------------------------------------------------
#List out all files and folders that would ne necessary for building pyramidgeneration
#------------------------------------------------------------------------------
SET(PROJ pyramidgeneration)
SET(${PROJ}_EXT_LIBS)

#------------------------------------------------------------------------------
#Include the third party libraries needed 
#------------------------------------------------------------------------------
INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/cmake/TPL.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

#------------------------------------------------------------------------------
#Check if a C++ Unit Test is requested
#------------------------------------------------------------------------------
SET(ut_spin${PROJ} OFF CACHE BOOL "Build C++ Unit test for ${PROJ} Module")
IF(ut_spin${PROJ})
  ADD_EXECUTABLE(ut_spin${PROJ} ${PROJECT_SOURCE_DIR}/Libraries/${PROJ}/UnitTest/spinVistaPyramidGeneration.cpp
                                ${PROJECT_SOURCE_DIR}/Utilities/Stitching/Pyramid/PyramidGeneration.cpp
                                ${PROJECT_SOURCE_DIR}/Utilities/Stitching/Pyramid/PyramidGeneration.h)
  TARGET_LINK_LIBRARIES(ut_spin${PROJ} ${${PROJ}_EXT_LIBS})
  install (TARGETS ut_spin${PROJ}  DESTINATION ${CMAKE_INSTALL_PREFIX}) 
  IF(MSVC)
    ADD_CUSTOM_COMMAND(
        TARGET ut_spin${PROJ}
        POST_BUILD 
        COMMAND ${CMAKE_COMMAND} 
        -DTARGETDIR=$<TARGET_FILE_DIR:ut_spin${PROJ}> 
        -DRELEASE_LIBS=${RELEASE_LIBS_TO_BE_COPIED}
        -DDEBUG_LIBS=${DEBUG_LIBS_TO_BE_COPIED}
        -DCFG=${CMAKE_CFG_INTDIR} -P "${LIBRARY_ROOT}/PostBuild.cmake"
        )
  ENDIF()
ENDIF()
