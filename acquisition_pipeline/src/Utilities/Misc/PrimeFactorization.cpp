#include "Utilities/Misc/PrimeFactorization.h"
#include <cstdlib>
#include <cmath>
#include <algorithm>

/*! \brief Code copied from 
 *  http://www.geeksforgeeks.org/prime-factorization-using-sieve-olog-n-multiple-queries/
 *  and modified 
 */
namespace spin
{
  // Calculating SPF (Smallest Prime Factor) for every
  // number till MAXN.
  // Time Complexity : O(nloglogn)
  void PrimeFactorization::sieve()
  {
    // Allocate memory in spf
    spf.assign(maxN,0);
    spf[1] = 1;
    
    // Mark smaallest prome factor for
    // every number to be itself. For every even number 
    // mark smallest prime as 2
    for (int i=2; i < maxN; ++i)
    {
      if ( ((i>>1) << 1) == i )
        spf[i] = 2;
      else
        spf[i] = i;
    }

    for (int i=3; i*i<maxN; ++i)
    {
      // checking if i is prime
      if (spf[i] == i)
      {
        // marking SPF for all numbers divisible by i
        for (int j=i*i; j<maxN; j+=i)
          // marking spf[j] if it is not 
          // previously marked
          if (spf[j]==j) spf[j] = i;
      }
    }
  }//end of sieve

  // A O(log n) function returning primefactorization
  // by dividing by smallest prime factor at every step
  std::vector<int> PrimeFactorization::Factorize(int x)
  {
    std::vector<int> ret;
    while (x != 1)
    {
      ret.push_back(spf[x]);
      x = x / spf[x];
    }
    return ret;
  }//end of function
  
  /*! \brief NearestInteger
   *  Give an integer, this function takes as an input a vector 
   *  containing a set of prime numbers. It subsequently modifies 
   *  the input integer until the smallest prime factors match 
   *  the input vector. Matches can be in two scenarios:
   *  a.) At most all prime factors specified in the input 
   *      vector will be present
   *  b.) No prime factor found should be different from the 
   *      input prime factors.
   *  c.) The smallest prime factor can be a single number that
   *      should also be present in the input vector
   */
  int PrimeFactorization::NearestInteger(int x, std::vector<int>* inpVec)
  {
    if(!inpVec || inpVec->size() <= 0) return -1;
    
    int count = x;
    bool flag = false;
    // Sort the vector in ascending order
    std::sort(inpVec->begin(), inpVec->end());
    
    while (!flag)
    {
      // Compute the prime factor decomposition of count
      std::vector<int> pfd = Factorize(count);
      // Sort the vector in ascending order
      std::sort(pfd.begin(), pfd.end());
      // Check if pfd contains any element other than 
      // elements in inpVec. 
      for (auto k: pfd)
      {
        if(std::find(inpVec->begin(), inpVec->end(), k) != inpVec->end()) 
        {
          // The element is contained in inpVec. Set flag to true
          flag = true;
        }
        else
        {
          // This element is not found in inpVec
          flag = false;
        }
      }  
      
      // We now check if flag has changed value. If it has then the conditions are
      // satisfied. Else we increment the count
      if (flag)
      {
        return count;
      }
      
      // We go to the next integer
      ++count;   
      // clear pfd
      pfd.clear();
    }
    
    return -1;
  }//end of function
}//end of namespace