#ifndef PRIMEFACTORIZATION_H_
#define PRIMEFACTORIZATION_H_
#include <vector>

namespace spin
{
  class PrimeFactorization 
  {
    public:
      PrimeFactorization(int n=100001)
      {
        maxN = n;
        sieve();
      }
      ~PrimeFactorization()
      {
        spf.clear();
      }
            
      /*! \brief prime factorization of an integer */
      std::vector<int> Factorize(int x);
      
      int NearestInteger(int x, std::vector<int>* inpVec);
    private:
      std::vector<int> spf;
      /*! \brief sieve */
      void sieve();
      int maxN;
  };//end of class
}//end of namespace
#endif