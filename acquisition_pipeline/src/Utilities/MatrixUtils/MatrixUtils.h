#ifndef MATRIXUTILS_H_
#define MATRIXUTILS_H_

namespace spin
{
  /*! \brief This class is a placeholder for various utilities that involve
   *  matrix analysis
   */
  template <class T>
  class MatrixUtils 
  {
    public:
      MatrixUtils(){}
      
      // Gradient
      void Gradient(T* A, T* gXCols, T* gYRows);
      
      // Symmetric Padding of columns
      void SymmetricPaddingOfCols(T* A, int left, int right, T* B);
      
      // Symmetric Padding of rows
      void SymmetricPaddingOfRows(T* A, int left, int right, T* B);
      
      // Row Maxima
      void RowwiseMaxima(T* A, T* B, int h, int w, int M=-1);
      
      // Col Maxima
      void RowwiseMinima(T* A, T* B, int h, int w, int M=-1);
      
      // Col Maxima
      void ColwiseMaxima(T* A, T* B);
      
      // Col Minima
      void ColwiseMinima(T* A, T* B);
      
      // Row Mean
      void RowwiseMean(T* A, T* B);
      
      // Col mean
      void ColwiseMean(T* A, T* B);
      
      // Mode-Filter
      void ModeFilter(void* A, int H, int W, int n, void* B);
  };//end of class
}//end of namespace
#endif
