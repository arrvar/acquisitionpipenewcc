#include "Utilities/MatrixUtils/RandomizedSVD.h"
#include "Utilities/MatrixUtils/EigenUtils.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <cmath>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

namespace spin
{
  struct SVDObject
  {
    typedef Eigen::JacobiSVD<RMMatrix_Double> JacobiSVD;
    SVDObject(int r, int c, int p)
    {
      svd = std::make_shared<JacobiSVD>(r,c);
      power = p;
    }
    
    void Compute(RMMatrix_Double* pMat)
    {
      svd.get()->compute(*pMat, Eigen::ComputeThinU | Eigen::ComputeThinV);
    };//end of function
    
    std::shared_ptr<JacobiSVD> svd;
    int power;
  };//end of struct
  
  /*! \brief The functions in this file implements the Randomized SVD algorithm as described in the following
   *  paper:
   *  N.Halko, P.G.Martinsson and J.A.Tropp, "Finding Structure with Randomness: Stochastic algorithms for 
   *  constructing approximate matrix decompositions". A "MATLAB" style description is presented in Slide 6 of
   *  the following presentation:
   *  https://amath.colorado.edu/faculty/martinss/Talks/2010_mmds_martinsson.pdf 
   *  There are two ways in which this class can be used:
   *  a.) Stand alone, or
   *  b.) Interfaced with any external module.
   */
   
  /*! \brief SampleTwoGaussian
   *  This function generates a 2D Random matrix
   */
  void SampleTwoGaussian(double* f1, double* f2)
  {
    double v1   = (static_cast<double>(rand()) + 1.0) / (static_cast<double>(RAND_MAX) + 2.0);
    double v2   = (static_cast<double>(rand()) + 1.0) / (static_cast<double>(RAND_MAX) + 2.0);
    double len  = sqrtf(-2.0 * log(v1));
    *f1 = (len * cos(2.0 * M_PI * v2));
    *f2 = (len * sin(2.0 * M_PI * v2));
  }//end of function

  /*! \brief GaussianRandomMatrixGenerator
   *  This function creates a random matrix that will be used to initialize the
   *  Randomized-SVD process
   */
  template <class T>
  void GaussianRandomMatrixGenerator(T* mat)
  {
    typedef typename T::Scalar ScalarType;
    for (int i = 0; i < (*mat).rows(); ++i)
    {      
      for (int j = 0; (j + 1) < (*mat).cols(); j += 2)
      {
        double f1=0, f2=0;
        SampleTwoGaussian(&f1, &f2);
        (*mat)(i, j)      = static_cast<ScalarType>(f1);
        (*mat)(i, j + 1)  = static_cast<ScalarType>(f2);
      }
      for (int j = 0; j < (*mat).cols(); j++)
      {
        double f1=0, f2=0;
        SampleTwoGaussian(&f1, &f2);
        (*mat)(i, j) = static_cast<ScalarType>(f1);
      }
    }
  }//end of function

  /*! \brief svdDriver
   *  This function is used to compute a SVD of a matrix using the Eigen::JacobiSVD routine.
   *  It has to be ensured that the # of rows > # of columns 
   */
  int svdDriver(RMMatrix_Double* pMat, SVDObject* ksvd)
  {
    if (!pMat) return -1;
    if (pMat->rows() < pMat->cols()) return -2;
    
    // implement the svd routine
    ksvd->Compute(pMat);
    return 1;
  }//end of function
    
  /*! \brief ComputeSVD
   *  This function is used to compute a SVD of a matrix using the Eigen::JacobiSVD routine.
   *  It has to be ensured that the # of rows > # of columns. In addition, this function 
   *  returns the output eigenvectors as a separate output. Optionally, we also return the 
   *  eigenvalues.
   */
  template <class T>
  int RandomizedSVD<T>::ComputeSVD(T* mat, T* eigVec, bool v, T* eigVal)
  {
    typedef typename T::Scalar Scalar;
    
    if (!mat) return -1;
    if (mat->rows() < mat->cols()) return -2;
    
    SVDObject* ksvd = svd.get();
    
    // 1.) Make a copy of this matrix to a RMMatrix_Double matrix 
    RMMatrix_Double pMat = mat->template cast<double>();
    
    // 2.) Make the input matrix symmetric
    RMMatrix_Double B = pMat * pMat.transpose();
    B = (B + B.transpose()) * Scalar{0.5};
    
    // 3.) Implement the svd routine
    svdDriver(&B, ksvd);
    
    // 4.) Return the output eigenvectors
    *eigVec = (B * ksvd->svd.get()->matrixU()).template cast<Scalar>();
    
    if (v)
    {
      // 5.) And the inverse of the eigenvalues (singular values)
      *eigVal = (ksvd->svd.get()->singularValues()).template cast<Scalar>();
    }
    
    return 1;
  }//end of function

  /*! \brief GetRand()
   *  This function gives us access to the Random Matrix 
   */
  template <class T>
  T RandomizedSVD<T>::GetRand()
  {
    return mOmega;  
  }//end of function
 
  /*! \brief GetPower()
   *  This function returns the power associated with matrix conditioning
   */
  template <class T>
  int RandomizedSVD<T>::GetPower()
  {
    return svd.get()->power;
  }//end of function

  /*! \brief Constructor
   *  This constructor is used to initialize the class when used by an 
   *  external module. 
   */
  template <class T>
  RandomizedSVD<T>::RandomizedSVD(int numSamples, int rank, int power)
  {
    if (numSamples < rank || rank >= numSamples || rank <=0 || power < 1) return ;
    
    // Build the random initial matrix
    typedef typename T::Scalar ScalarType;
    mOmega = T::Zero(numSamples, rank);
    GaussianRandomMatrixGenerator<T>(&mOmega);
    
    // Instantiate the Jacobi SVD object    
    svd = std::make_shared<SVDObject>(rank, rank, power);
  }//end of function
  
    // explicit instantiations
  template class RandomizedSVD<RMMatrix_Float>;
  template class RandomizedSVD<CMMatrix_Float>;
  template class RandomizedSVD<RMMatrix_Double>;
  template class RandomizedSVD<CMMatrix_Double>;

}//end of namespace
