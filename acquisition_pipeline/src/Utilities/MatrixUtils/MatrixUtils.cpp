#include "Utilities/MatrixUtils/MatrixUtils.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <iostream>

namespace spin
{
  /*! \brief This function computes the 3-tap gradient of a matrix 
   *  A and returns the output in gXCols and gYRows
   */
  template <class T>
  void MatrixUtils<T>::Gradient(T* A, T* gXCols, T* gYRows)
  {
    //https://forum.kde.org/viewtopic.php?f=74&t=93054
    (*gXCols) = A->rightCols(A->cols()-1) - A->leftCols(A->cols()-1);
    (*gYRows) = A->bottomRows(A->rows()-1) - A->topRows(A->rows()-1); 
  }//end of function
  
  /*! \brief This function extends the matrix along columns using symmetric 
   *  extension. The dimensions to extend are given by "left" and "right",
   *  while the output matrix is returned in B
   */
  template <class T>
  void MatrixUtils<T>::SymmetricPaddingOfCols(T* A, int left, int right, T* B)
  {
    if (left >=0 && right >=0 && left < A->cols() && right < A->cols())
    {
      // Create permutation matrices
      T permutationLeft, permutationRight, a1, a2;
      if (left > 1)
      {
        permutationLeft = T::Identity(left,left).rowwise().reverse().eval();
        a1 = A->leftCols(left)*permutationLeft;
      }
      else
      if (left == 1)
      {
        a1 = A->leftCols(1);
      }
      
      if (right > 1)
      {
        permutationRight = T::Identity(right,right).rowwise().reverse().eval();
        a2 = A->rightCols(right)*permutationRight;
      }
      else
      if (right == 1)
      {
        a2 = A->rightCols(1);
      }
      
      (*B) = T::Zero(A->rows(), a1.cols() + A->cols() + a2.cols());
      if (a1.size() > 0 && a2.size() > 0)
      {
        // Now, pad a1 and a2 to A and assign it to B        
        (*B)<<a1,(*A),a2;     
      }
      else 
      if (a1.size() == 0 && a2.size() > 0)
      {
        (*B)<<(*A),a2;
      }
      else 
      if (a1.size() > 0 && a2.size() == 0)
      {
        (*B)<<a1,(*A);
      }
    }
  }//end of function
  
  /*! \brief This function extends the matrix along rows using symmetric 
   *  extension. The dimensions to extend are given by "top" and "bottom",
   *  while the output matrix is returned in B
   */
  template <class T>
  void MatrixUtils<T>::SymmetricPaddingOfRows(T* A, int top, int bottom, T* B)
  {
    if (top >=0 && bottom >=0 && top < A->rows() && bottom < A->rows())
    {
      // Create permutation matrices
      T permutationTop, permutationBottom, a1, a2;
      if (top > 1)
      {
        permutationTop = T::Identity(top,top).rowwise().reverse().eval();
        a1 = permutationTop * A->topRows(top);
      }
      else
      if (top == 1)
      {
        a1 = A->topRows(1);
      }
      
      if (bottom > 1)
      {
        permutationBottom = T::Identity(bottom,bottom).rowwise().reverse().eval();
        a2 = permutationBottom * A->bottomRows(bottom);
      }
      else
      if (bottom == 1)
      {
        a2 = A->bottomRows(1);
      }
      
      // Now, pad a1 and a2 to A and assign it to B
      (*B) = T::Zero(a1.rows() + A->rows() + a2.rows(), A->cols());
      
      if (a1.size() > 0 && a2.size() > 0)
      {
        // Now, pad a1 and a2 to A and assign it to B
        (*B)<<a1,(*A),a2;     
      }
      else 
      if (a1.size() == 0 && a2.size() > 0)
      {
        (*B)<<(*A),a2;
      }
      else 
      if (a1.size() > 0 && a2.size() == 0)
      {
        (*B)<<a1,(*A);
      }
    }
  };//end of class

  /*! \brief This function computes the maximum value along every row of a 
   *  matrix (after computing the abs) and returns the value, as well as 
   *  the index in each row. In case of multiple maxima in every row, a single 
   *  index is returned. Thus, if the input matrix is of dimension M x N,
   *  the output matrix will be of dimension M x 2 where the first column 
   *  contains the maximum value in each row, while the second column 
   *  contains the index location
   */
  template <class T>
  void MatrixUtils<T>::RowwiseMaxima(T* A, T* B, int height, int width, int M)
  {
    // Get the pixel type  
    typedef Eigen::VectorXd VectorXd; 
    bool modeFilter = false;
    if ( M >= 1) modeFilter = true;
    
    if (A->rows() > 0 && A->cols() > 1)
    {
      // Get the number of columns
      int nC = A->cols();
      int nR = A->rows();
      
      // Allocate some resources
      RMMatrix_Double v1     = (VectorXd::LinSpaced(nC,1,nC)).matrix();
      
      RMMatrix_Double v2     = RMMatrix_Double::Ones(A->rows(),1);
      
      RMMatrix_Double v3     = RMMatrix_Double::Ones(1,nC);
      
      // We need the absolute value of each row. Hence, we save the data in a holding 
      // matrix
      RMMatrix_Double A1     = A->array().abs().template cast <double>()+1e-8;
      
      // Get the maximum value in each row and save it in an array 
      RMMatrix_Double maxVal = A1.rowwise().maxCoeff();
      
      RMMatrix_Double fin    = ((A1 - maxVal * v3).array()== 0).template cast<double>().matrix();
      
      RMMatrix_Double g      = Eigen::pow(2.0,(VectorXd::LinSpaced(nC,nC-1,0)).array()).matrix();
      
      RMMatrix_Double h      = v3 * (1.0/g.array()).matrix().asDiagonal();
      
      // Get the location of the indices
      RMMatrix_Double maxIdx = (((fin*g*h).array().floor())==1.0).template cast<double>().matrix()*v1.matrix();
      
      // Check if mode-filtering is required
      if (modeFilter)
      {
        ModeFilter(&maxIdx, height, width, M, &maxIdx);
      }
      
      // Having obtained the index, we now get the actual values from the data matrix. 
      RMMatrix_Double dump   = 1.0/(maxIdx.array());
      fin     = (((dump.asDiagonal())*(v2*v1.transpose())).array()==1.0).template cast<double>().matrix();
      
      maxVal  = (fin.cwiseProduct(A->template cast<double>())).rowwise().sum();
      
      // We now populate the indices
      *B = T::Zero(A->rows(),2);
      
      *B<< (maxVal.template cast<typename T::Scalar>()), (maxIdx.template cast<typename T::Scalar>());
    }
    else
    if (A->rows() > 0 && A->cols() == 1)
    {
      *B = T::Zero(A->rows(),2);
      
      *B<< (*A),(T::Ones(A->rows(),1));
    }
  }//end of function
  
  /*! \brief This function computes the minimum value along every row of a 
   *  matrix (after computing the abs) and returns the value, as well as 
   *  the index of the minimum value in each row. In case of multiple minima in every row, a single 
   *  index is returned. Thus, if the input matrix is of dimension M x N,
   *  the output matrix will be of dimension M x 2 where the first column 
   *  contains the minimum value in each row, while the second column 
   *  contains the index location
   */
  template <class T>
  void MatrixUtils<T>::RowwiseMinima(T* A, T* B, int height, int width, int M)
  {
    // Get the pixel type 
    typedef Eigen::VectorXd VectorXd;
    bool modeFilter = false;
    if ( M >= 1) modeFilter = true;
    
    if (A->rows() > 0 && A->cols() > 1)
    {
      // Get the number of columns
      int nC = A->cols();
      
      int nR = A->rows();
      
      // Allocate some resources
      RMMatrix_Double v1     = (VectorXd::LinSpaced(nC,1,nC)).matrix();
      
      RMMatrix_Double v2     = RMMatrix_Double::Ones(A->rows(),1);
      
      RMMatrix_Double v3     = RMMatrix_Double::Ones(1,nC);
      
      // We need the absolute value of each row. Hence, we save the data in a holding 
      // matrix
      RMMatrix_Double A1     = A->array().abs().template cast <double>()+1e-8;
      
      // Get the minimum value in each row and save it in an array 
      RMMatrix_Double minVal = A1.rowwise().minCoeff();
      
      RMMatrix_Double fin    = ((A1 - minVal * v3).array()== 0).template cast<double>().matrix();
      
      RMMatrix_Double g      = Eigen::pow(2.0,(VectorXd::LinSpaced(nC,nC-1,0)).array()).matrix();
      
      RMMatrix_Double h      = v3 * (1.0/g.array()).matrix().asDiagonal();
      
      // Get the location of the indices
      RMMatrix_Double minIdx = (((fin*g*h).array().floor())==1.0).template cast<double>().matrix()*v1.matrix();
      
      // Check if mode-filtering is required
      if (modeFilter)
      {
        ModeFilter(&minIdx, height, width, M, &minIdx);
      }
      
      // Having obtained the index, we now get the actual values from the data matrix. 
      RMMatrix_Double dump   = 1.0/(minIdx.array());
      
      fin     = (((dump.asDiagonal())*(v2*v1.transpose())).array()==1.0).template cast<double>().matrix();
      
      minVal  = (fin.cwiseProduct(A->template cast<double>())).rowwise().sum();
      
      // We now populate the indices
      *B = T::Zero(A->rows(),2);
      
      *B<< (minVal.template cast<typename T::Scalar>()), (minIdx.template cast<typename T::Scalar>());
    }
    else
    if (A->rows() > 0 && A->cols() == 1)
    {
      *B = T::Zero(A->rows(),2);
      
      *B<< (*A),(T::Ones(A->rows(),1));
    }
  }//end of function
  
  /*! \brief This function computes the maximum value along every column of a 
   *  matrix and returns the value, as well as the index of the maximum
   *  value in each column. In case of multiple maxima in every column, a single 
   *  index is returned. Thus, if the input matrix is of dimension M x N,
   *  the output matrix will be of dimension 2 x N where the first row 
   *  contains the maximum value in each column, while the second row 
   *  contains the index location
   */
  template <class T>
  void MatrixUtils<T>::ColwiseMaxima(T* A, T* B)
  {
    // Get the pixel type 
    typedef Eigen::VectorXf VectorXf;
    
    if (A->rows() > 1 && A->cols() > 0)
    {
      // Get the number of columns
      int nR = A->rows();
      // Get the maximum value in each column and save it in an array 
      RMMatrix_Float maxVal = A->array().abs().colwise().maxCoeff().template cast<float>();
      RMMatrix_Float dump   = static_cast<float>(1.)/(maxVal.array());
      RMMatrix_Float fin    = (((A->template cast<float>())*(dump.asDiagonal())).array()==1.0).template cast<float>().matrix();
      RMMatrix_Float g      = Eigen::pow(2.0,(VectorXf::LinSpaced(nR,nR-1,0)).array()).matrix();
      RMMatrix_Float h      = (1.0/g.array()).matrix().asDiagonal();
      // Get the location of the indices
      RMMatrix_Float maxIdx = (VectorXf::LinSpaced(nR,1,nR)).transpose()*((h*RMMatrix_Float::Ones(nR,1)*((g.transpose()*fin).transpose()).transpose()).array().floor()==1.0).template cast<float>().matrix();
      // We now populate the indices
      *B = T::Zero(2,A->cols());
      *B<< (maxVal.template cast<typename T::Scalar>()), (maxIdx.template cast<typename T::Scalar>());
    }
    else
    if (A->rows() == 1 && A->cols() > 0)
    {
      *B = T::Zero(2,A->cols());
      *B<< (*A),(T::Ones(1,A->cols()));
    }
  }//end of function
  
  /*! \brief ColwiseMinima 
   *  This function computes the minimum value along every column of a 
   *  matrix and returns the value, as well as the index of the minimum
   *  value in each column. In case of multiple maxima in every column, a single 
   *  index is returned. Thus, if the input matrix is of dimension M x N,
   *  the output matrix will be of dimension 2 x N where the first row 
   *  contains the minimum value in each column, while the second row 
   *  contains the index location
   */
  template <class T>
  void MatrixUtils<T>::ColwiseMinima(T* A, T* B)
  {
    // Get the pixel type 
    typedef Eigen::VectorXf VectorXf;
    
    if (A->rows() > 1 && A->cols() > 0)
    {
      // Get the number of columns
      int nR = A->rows();
      // Get the maximum value in each column and save it in an array 
      RMMatrix_Float minVal = A->array().abs().colwise().minCoeff().template cast<float>();
      RMMatrix_Float dump   = static_cast<float>(1.)/(minVal.array());
      RMMatrix_Float fin    = (((A->template cast<float>())*(dump.asDiagonal())).array()==1.0).template cast<float>().matrix();
      RMMatrix_Float g      = Eigen::pow(2.0,(VectorXf::LinSpaced(nR,nR-1,0)).array()).matrix();
      RMMatrix_Float h      = (1.0/g.array()).matrix().asDiagonal();
      // Get the location of the indices
      RMMatrix_Float maxIdx = (VectorXf::LinSpaced(nR,1,nR)).transpose()*((h*RMMatrix_Float::Ones(nR,1)*((g.transpose()*fin).transpose()).transpose()).array().floor()==1.0).template cast<float>().matrix();
      
      // We now populate the indices
      *B = T::Zero(2,A->cols());
      *B<< (minVal.template cast<typename T::Scalar>()), (maxIdx.template cast<typename T::Scalar>());
    }
    else
    if (A->rows() == 1 && A->cols() > 0)
    {
      *B = T::Zero(2,A->cols());
      *B<< (*A),(T::Ones(1,A->cols()));
    }
  }//end of function
  
  /*! \brief RowWiseMean
   *  This function computes the row-wise mean of a 2-D Matrix
   */
  template <class T>
  void MatrixUtils<T>::RowwiseMean(T* A, T* B)
  {
    (*B) = A->rowwise().mean();
  }
  
  /*! \brief ColWiseMean
   *  This function computes the row-wise mean of a 2-D Matrix
   */
  template <class T>
  void MatrixUtils<T>::ColwiseMean(T* A, T* B)
  {
    (*B) = A->colwise().mean();
  }

  /*! \brief ModeFilter 
   *  In this function, we take a matrix and do the following:
   *  a.) Examine a NxN neighborhood around each element.
   *  b.) Compute the mode of all values around the pixel
   *  c.) Replace the central pixel by its mode.
   */
  template <class T>
  void MatrixUtils<T>::ModeFilter(void* _A, int height, int width, int N, void* _B)
  {
    // Typecast the data
    RMMatrix_Double* A = static_cast<RMMatrix_Double*>(_A);
    // Reshape the data
    RMMatrix_Double d = Eigen::Map<RMMatrix_Double>(A->data(),height,width);
    
    // Allocate memory for the output data 
    RMMatrix_Double F = RMMatrix_Double::Zero(height,width);
    
    // The dirty part!!!
    // Plenty of parallelization possible here
    for (int h = 0; h < height; ++h)
    for (int w = 0; w < width; ++w)
    {
      std::map<double, int> count;
      for (int h1 = h-N; h1 <= h+N; ++h1)
      for (int w1 = w-N; w1 <= w+N; ++w1)
      {
        if (h1 >=0 && w1 >=0 && h1 < height && w1 < width)
        {
          double g = d(h1,w1);
          std::map<double,int>::iterator it = count.find(g);
          if (it != count.end())
          {
            // The index already exists
            ++count[g];
          }
          else
          {
            // The idex is new
            count[g] = 1;
          }
        }
      }
      
      //Find the mode
      //Cannot handle duplicates :(
      int maxVal = -1;
      double mIdx = -1;
      std::map<double,int>::iterator it = count.begin();
      while (it != count.end())
      {
        if (it->second > maxVal)
        {
          maxVal = it->second;
          mIdx = it->first;
        }
        ++it;
      }
      // Place the maximally occurring value
      F(h,w) = mIdx;
      // and clear the temporary data
      count.clear();
    }
    
    // Revert the matrix back to its original shape
    RMMatrix_Double* B = static_cast<RMMatrix_Double*>(_B);
    (*B) = Eigen::Map<RMMatrix_Double>(F.data(),A->rows(),A->cols());
  }//end of function
  
  // explicit template definitions
  template class MatrixUtils<RMMatrix_UChar>;
  template class MatrixUtils<RMMatrix_UShort>;
  template class MatrixUtils<RMMatrix_Int>;
  template class MatrixUtils<RMMatrix_Float>;
  template class MatrixUtils<RMMatrix_Double>;
  
  template class MatrixUtils<CMMatrix_Float>;
  template class MatrixUtils<CMMatrix_Double>;
}//end of namespace
