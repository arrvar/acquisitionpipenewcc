#include "Utilities/MatrixUtils/SparseMatrixUtils.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <unsupported/Eigen/KroneckerProduct>
#include <iostream>

namespace spin
{
  /*! \brief BidiagonalMatrix
   *  This function creates a bi-diagonal matrix with the following 
   *  constraints:
   *  a.) The bottom and top diagonals are of the same length
   *  b.) The matrix is non-square
   *  c.) The length of the diagonal defines the # of rows in the matrix
   *  For example
   *  A=|-1  1  0  0|
   *    | 0 -1  1  0|
   *    | 0  0 -1 -1|
   *  is a 3x4 bi-diagoonal matrix
   */
  template <class T>
  int SparseMatrixUtils<T>::BidiagonalMatrix(void* _dTop, void* _dBot, void* _bdOutput)
  {
    // Typecast the diagonal elements which are 1-D dense matrices
    typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> Matrix;
    typedef Eigen::SparseMatrix<T, Eigen::ColMajor> SparseMatrix;
    typedef Eigen::SparseMatrix<T, Eigen::RowMajor> SparseMatrixRow;
    
    // sanity checks
    Matrix* dTop = static_cast<Matrix*>(_dTop);
    Matrix* dBot = static_cast<Matrix*>(_dBot);
    if (dTop->rows() != dBot->rows()) return -1;
    if (dTop->cols() !=1) return -2;
    if (dBot->cols() !=1) return -3;
    SparseMatrixRow* bdOutput = static_cast<SparseMatrixRow*>(_bdOutput);
    if (!bdOutput) return -4;
    
    // Convert the arrays to diagonal matrices
    SparseMatrix bottom = SparseMatrix(dBot->asDiagonal());
    SparseMatrix top    = SparseMatrix(dTop->asDiagonal());
    // Due to restrictions on block operations in sparse matrices 
    // (https://eigen.tuxfamily.org/dox/group__TutorialSparse.html#title7,)
    // we conform to API restrictions
    SparseMatrix bottom1(dBot->rows(), dBot->rows()+1);
    bottom1.leftCols(bottom.cols()) = bottom;
    SparseMatrix top1(dTop->rows(), dTop->rows()+1);
    top1.rightCols(top.cols()) = top;
    // Add these two matrices
    (*bdOutput) = bottom1+top1;
    return 1;
  }//end of function
  
  /*! \brief KroneckerProduct
   *  This function is a simple API To compute a kronecker product between two sparse 
   *  matrices and return another sparse matrix
   */
  template <class T>
  int SparseMatrixUtils<T>::KroneckerProduct(void* _A, void* _B, void* _C)
  {
    typedef Eigen::SparseMatrix<T, Eigen::RowMajor> SparseMatrix;
    SparseMatrix* A = static_cast<SparseMatrix*>(_A);
    if (!A) return -1;
    SparseMatrix* B = static_cast<SparseMatrix*>(_B);
    if (!B) return -2;
    SparseMatrix* C = static_cast<SparseMatrix*>(_C);
    if (!C) return -3;
    
    (*C) = Eigen::KroneckerProductSparse<SparseMatrix,SparseMatrix>(*A,*B);
    return 1;
  }//end of function
  
  // explicit template instantiations
  template class SparseMatrixUtils<float>;
  template class SparseMatrixUtils<double>;  

}//end of namespace