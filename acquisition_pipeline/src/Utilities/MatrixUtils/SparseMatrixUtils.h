#ifndef SPARSEMATRIXUTILS_H_
#define SPARSEMATRIXUTILS_H_

namespace spin
{
  /*! \brief This class is a placeholder for various utilities that involve
   *  sparse matrix analysis
   */
  template <class T>
  class SparseMatrixUtils 
  {
    public:
      SparseMatrixUtils(){}
      
      // Bidiagonal matrix intialization
      int BidiagonalMatrix(void* a, void* b, void* c);
      
      // Kronecker product
      int KroneckerProduct(void* A, void* B, void* C);
  };//end of class
}//end of namespace
#endif
