#ifndef EIGENUTILS_H_
#define EIGENUTILS_H_
#include <cmath>
#include <cstdlib>

/*! \brief This file contains unary functions that can be applied to every element
 *  of an Eigen matrix */
namespace spin 
{
  namespace EigenUtils 
  {
    /*! \brief inverse */
    template <class T>
    T inverse(T x)
    {
      if (x == 0) return 0;
      
      return static_cast<T>( static_cast<double>(1.0)/static_cast<double>(x) );
    }//end of function 
    
    /*! \brief logx */
    template <class T>
    T logx(T x)
    {
      if (x <=0) return 0;
      
      return static_cast<T>( log(static_cast<double>(1.0)/static_cast<double>(x) ) );
    }//end of function 

    /*! \brief xlogx */
    template <class T>
    T xlogx(T x)
    {
      if (x <=0) return 0;
      
      return static_cast<T>( static_cast<double>(x) * log(static_cast<double>(1.0)/static_cast<double>(x) ) );
    }//end of function 
    
    /*! \brief power2_0 */
    template <class T>
    T power2_0(T x)
    {      
      return static_cast<T>( static_cast<double>(x*x));
    }//end of function 

    /*! \brief power2_5 */
    template <class T>
    T power2_5(T x)
    {      
      return static_cast<T>( pow(static_cast<double>(x), 2.5));
    }//end of function 
    
    /*! \brief power0_5 */
    template <class T>
    T power0_5(T x)
    { 
      if (x <= 0 ) return 0;
      return static_cast<T>( pow(static_cast<double>(x), 0.5));
    }//end of function 

    /*! \brief inverse1_5 */
    template <class T>
    T inverse1_5(T x)
    { 
      if (x <= 0 ) return 0;
      return static_cast<T>( pow(1.0/static_cast<double>(x), 1.5));
    }//end of function 
    
    /*! \brief inverse_sqrt */
    template <class T>
    T inverse_sqrt(T x)
    { 
      if (x <= 0 ) return 0;
      return static_cast<T>( pow(1.0/static_cast<double>(x), 0.5));
    }//end of function 
    
  };//end of namespace
};//end of namespace
#endif
