#ifndef RANDOMIZEDSVD_H_
#define RANDOMIZEDSVD_H_
#include <memory>

namespace spin
{
  // Forward declaration of a SVDObject
  struct SVDObject;
  
  /*! \brief RandomizedSVD 
   *  This class is a placeholder for the Randomized SVD algorithms
   *  wherein a SVD or other related tasks can be achieved
   */
  template <class T>
  class RandomizedSVD 
  {
    public:
      RandomizedSVD(int s, int r, int p = 2);
            
      // Compute SVD
      int ComputeSVD(T* mat, T* eigVec, bool p = false, T* eigVal=NULL);
      
      // Access a random matrix
      T GetRand();
      
      // Get the power 
      int GetPower();
    private:
      std::shared_ptr<SVDObject> svd;
      T mOmega;
  };//end of class 
};//end of namespace
#endif
