#ifndef PYRAMIDGENERATION_H_
#define PYRAMIDGENERATION_H_
#include <string>

namespace spin
{
  class PyramidGeneration 
  {
    public:
      PyramidGeneration(){}
      /*! \brief API for the driver to run pyramid generation inline */
      int Generate(std::string v, int tileDim, std::string ext);
      /*! \brief API for the driver to run pyramid generation offline */
      int Generate(std::string v, bool sharpness, float kernel, float strength);
      /*! \brief Get time */
      double GetTime(){return pTimePyramid;}
    private:
      double pTimePyramid=0;
  };//end of class
}
#endif
