#include "Utilities/Stitching/Pyramid/PyramidGeneration.h"
#include <string>
#include "boost/filesystem.hpp"
#include "boost/algorithm/string/predicate.hpp"
#include <boost/algorithm/string.hpp>
#include "opencv2/core.hpp"
#include "opencv2/opencv.hpp"
#include <cmath>
#include <chrono>
#include <string>
#include <algorithm>
#include <fstream>
#ifdef USE_OPENMP
#include <omp.h>
#endif


#ifndef MAXLEVEL_PYRAMID
#define MAXLEVEL_PYRAMID 30
#endif

namespace spin
{
  struct currLevel
  {
    std::string folderPath;
    int level;
    int x;
    int y;
  };

  namespace fs = boost::filesystem;
  int calculateLevel(int rows, int cols, int tileDim)
  {
    // 2 ^ (n - 1) <= W | H
    // n - number of levels
    // W | H width or height whichever is larger
    int i;
    for (i = 0; i < MAXLEVEL_PYRAMID; ++i)
    {
      if (rows>cols)
      {
        if (pow(2, i) > rows * tileDim)
        {
          return i;
        }
      }
      else
      {
        if (pow(2, i) > cols * tileDim)
        {
          return i;
        }
      }
    }
    return 0;
  }

  // A pipeline to implement Unsharp Masking using in-built functions provided
  // by opencv as explained here
  // https://www.idtools.com.au/unsharp-masking-python-opencv/
  // This API is to be used primarily with Pyramid Generation
  int Sharpen(void* _imgIn, void* _imgOut, float sigma, float strength)
  {
    cv::Mat* inputImage = static_cast<cv::Mat*>(_imgIn);
    cv::Mat* outputImage = static_cast<cv::Mat*>(_imgOut);

    // We need to process each channel separately
    cv::Mat convMat;
    inputImage->convertTo(convMat,CV_32FC3);
    // Split the channels
    cv::Mat channels[3];
    cv::split(convMat, channels);
    cv::Mat dummy;
    std::vector<cv::Mat> dump(3,dummy);
    int kernel = static_cast<int>(sigma);

    for (int i = 0; i < 3; ++i)
    {
      cv::Mat temp;
      // Median filtering
      cv::medianBlur(channels[i],temp,kernel);
      // Compute Laplacian
      cv::Laplacian(temp,temp,CV_32FC1);
      // Do the unsharp masking
      temp = channels[i]-strength*temp;
      // Saturate the pixels in either direction
      //https://stackoverflow.com/questions/33750824/best-way-to-indexing-a-matrix-in-opencv
      for (int r = 0; r < temp.rows; ++r)
      {
        float* pA = temp.ptr<float>(r);
        for (int c =0; c < temp.cols; ++c)
        {
          if (pA[c] > 255.0) pA[c] = 255.0;
          else
          if (pA[c] < 0) pA[c] = 0;
        }
      }
      dump[i] = temp;
    }//finished enhancing every channel

    // Merge channels
    cv::merge(dump,convMat);
    // convert channels to CV_8UC3
    convMat.convertTo(*outputImage,CV_8UC3);

    channels[0].release();
    channels[1].release();
    channels[2].release();
    dump[0].release();
    dump[1].release();
    dump[2].release();
    dump.clear();
    convMat.release();

    return 1;
  }//end of function

  
  /*! \brief WriteDZI
   *  This function is used to write the meta information as a 
   *  DZI file 
   */
  void WriteDZI(long pyrWidth, long pyrHeight, int TileDim, std::string fmt, std::string path)
  {
    std::string v = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n";
    v += "<Image xmlns=\"http://schemas.microsoft.com/deepzoom/2008\" \n";
    v += "\tFormat=\""+fmt+"\"\n";
    v += "\tOverlap=\"0\"\n";
    v += "\tTileSize=\""+std::to_string(TileDim)+"\"\n";
    v += "\t>\n";
    v += "\t<Size \n";
    v += "\t\tHeight=\""+std::to_string(pyrHeight*TileDim)+"\"\n";
    v += "\t\tWidth=\""+std::to_string(pyrWidth*TileDim)+"\"\n";
    v += "\t/>\n";
    v += "</Image>";
    
    // Make the file name
    std::string fName = path+".dzi";
    
    std::ofstream out(fName.c_str());
    out << v;
    out.close();
  }//end of function
  
  /*! \brief joinGrid
   *  This function is used to join tiles from a grid and create the next level 
   *  in the pyramid structures
   */
  void joinGrid(std::string folderPath, int level, int x, int y, std::string ext,\
                bool sharpness, float kernel, float strength)
  {
    std::string p = ext;
    cv::Mat blank;
    std::string fPath = folderPath + std::to_string(level) + "/";
    //check if it is not end - row or end - column
    if (boost::filesystem::exists(fPath + std::to_string(x+1) + "_" + std::to_string(y+1) + p))
    {
      cv::Mat imTL = cv::imread(fPath + std::to_string(x) + "_" + std::to_string(y) + p);
      cv::Mat imTR = cv::imread(fPath + std::to_string(x+1) + "_" + std::to_string(y) + p);
      cv::Mat imBL = cv::imread(fPath + std::to_string(x) + "_" + std::to_string(y+1) + p);
      cv::Mat imBR = cv::imread(fPath + std::to_string(x+1) + "_" + std::to_string(y+1) + p);
      cv::Mat blank1(imTL.rows + imBL.rows, imTL.cols + imTR.cols, imTL.type());
      blank = blank1;
      imTL.copyTo(blank(cv::Rect(0, 0, imTL.cols, imTL.rows)));
      imTR.copyTo(blank(cv::Rect(imTL.cols, 0, imTR.cols, imTR.rows)));
      imBL.copyTo(blank(cv::Rect(0, imTL.rows, imBL.cols, imBL.rows)));
      imBR.copyTo(blank(cv::Rect(imTL.cols, imTL.rows, imBR.cols, imBR.rows)));
    }
    //for end - column
    else 
    if (fs::exists(fPath + std::to_string(x + 1) + "_" + std::to_string(y) + p))
    {
      cv::Mat imTL = cv::imread(fPath + std::to_string(x) + "_" + std::to_string(y) + p);
      cv::Mat imTR = cv::imread(fPath + std::to_string(x+1) + "_" + std::to_string(y) + p);
      cv::Mat blank1(imTL.rows, imTL.cols + imTR.cols, imTL.type());
      blank = blank1;
      imTL.copyTo(blank(cv::Rect(0, 0, imTL.cols, imTL.rows)));
      imTR.copyTo(blank(cv::Rect(imTL.cols, 0, imTR.cols, imTR.rows)));
    }
    //for end - row
    else 
    if (fs::exists(fPath + std::to_string(x) + "_" + std::to_string(y + 1) + p))
    {
      cv::Mat imTL = cv::imread(fPath +  std::to_string(x) + "_" + std::to_string(y) + p);
      cv::Mat imBL = cv::imread(fPath + std::to_string(x) + "_" + std::to_string(y+1) + p);
      cv::Mat blank1(imTL.rows + imBL.rows, imTL.cols, imTL.type());
      blank = blank1;      
      imTL.copyTo(blank(cv::Rect(0, 0, imTL.cols, imTL.rows)));
      imBL.copyTo(blank(cv::Rect(0, imTL.rows, imBL.cols, imBL.rows)));
    }
    //for bottom - right end
    else 
    if (fs::exists(fPath + std::to_string(x) + "_" + std::to_string(y) + p))
    {
      cv::Mat imTL = cv::imread(fPath + std::to_string(x) + "_" + std::to_string(y) + p);
      cv::Mat blank1(imTL.rows, imTL.cols, imTL.type());
      blank = blank1;
      imTL.copyTo(blank(cv::Rect(0, 0, imTL.cols, imTL.rows)));
    }
    
    // Save the image after resizing
    if(blank.rows!=0 && blank.cols!=0)
    {
      if(blank.cols<2 || blank.rows<2)
      {
        cv::resize(blank, blank, cv::Size(1, 1));        
      }
      else
      {
        cv::resize(blank, blank, cv::Size(blank.cols / 2, blank.rows / 2), 0.5, 0.5, cv::INTER_LINEAR);
      }

      // Do we need to sharpen the image
//      if (sharpness)
//      {
//        cv::Mat d;
//        Sharpen(&blank, &d, kernel, strength);
//        blank = d;
//      }
      cv::imwrite(folderPath+ std::to_string(level-1) + "/" \
                            + std::to_string(x/2) + "_" + std::to_string(y/2) + p, blank);
    }
    blank.release();
  }//end of function

  /*! \brief startGeneration
   *  This function creates the necessary folders for storing the pyramid 
   *  and populates each folder by running sequential levels of tile generation
   */
  int startGeneration(std::string folderPath, int rows, int cols, int tileDim, \
                      std::string ext, bool sharpness, float kernel, float strength)
  {
    int level = calculateLevel(rows, cols, tileDim);
    int d = 1;
    // Create a folder that will contain the pyramid
    std::string pyrFolder = folderPath + "_files";
    // Check if this folder already exists
    // If it does, we delete it prior to creating another folder 
    boost::filesystem::path dir(pyrFolder.c_str());
    if (!boost::filesystem::exists(dir))
    {
      if (!boost::filesystem::create_directory(dir))
      {
        return -3;
      }
    }
    else
    {
      // This directory exists, we delete the existing directory and then
      // create it again
      boost::filesystem::remove_all(dir);
      if (!boost::filesystem::create_directory(dir))
      {
        return -3;
      }
    }
    
    int pos = folderPath.rfind("/");
    std::string parentFolder = folderPath.substr(0,pos+1);	
    boost::filesystem::rename(folderPath, parentFolder+std::to_string(level));
    boost::filesystem::rename(parentFolder+std::to_string(level), folderPath + "_files/" + std::to_string(level));
    folderPath = folderPath + "_files/";
    
    // Traverse through all levels
    while (level)
    {
      std::string fPath = folderPath + std::to_string(level-1) + "/";
      if (!fs::exists(fPath))
      {
        boost::filesystem::create_directory(fPath);
      }
      // Build a lookup table of all indcies that would need to be traversed
      std::vector <currLevel> iterList;
      for (int cx = 0; cx <= std::ceil(cols / d); cx += 2)
      for (int cy = 0; cy <= std::ceil(rows / d); cy += 2)
      {
        currLevel temp;
        temp.x = cx;
        temp.y = cy;
        temp.folderPath = folderPath;
        temp.level = level;
        iterList.push_back(temp);
      }
      
#ifdef USE_OPENMP
      #pragma omp parallel for num_threads(8)
#endif
      for (int index = 0; index < iterList.size(); ++index)
      {
        joinGrid(iterList[index].folderPath, \
                 iterList[index].level, \
                 iterList[index].x, \
                 iterList[index].y, \
                 ext, sharpness, kernel, strength);
      }
      iterList.clear();
      level -= 1;
      d *= 2;
    }
    return 0;
  }//end of function

  /*! \brief checkDummy
   *  This function achieves two tasks:
   *  a.) It checks for a dummy file and,
   *  b.) Computes the dimensions of the max rows and cols that 
   *      would be needed to generate the pyramid
   */
  int checkDummy(std::string folderPath, int tileDim, std::string ext, \
                 bool sharpness, float kernel, float strength)
  {
    if (!folderPath.empty())
    {	
      if(boost::algorithm::ends_with(folderPath, "/"))
      {
        folderPath = folderPath.substr(0,folderPath.length()-1);
      }
	    
      int maxRows = 0, maxCols = 0;
      int log;
      boost::filesystem::path apk_path(folderPath);
      boost::filesystem::directory_iterator end;
      std::string dummyPath;
      
      //iterate over the directory to find out max row and col
#ifdef USE_OPENMP
      #pragma omp parallel
#endif
      for (boost::filesystem::directory_iterator i(apk_path); i != end; ++i)
      {
        const boost::filesystem::path cp = (*i);
        int pos = cp.string().rfind("/");
        std::string name = cp.string().substr(pos + 1);
        //skip over the dummy file
        if(name.find("dummy") == std::string::npos)
        {
          int c = stoi(name.substr(0, name.rfind("_")));
          int r = stoi(name.substr(name.rfind("_")+1));
          if (maxRows < r)
            maxRows = r;
          if (maxCols < c)
            maxCols = c;
        }
      }
      
      //add folderpath to the dummy file
      if (!boost::ends_with(folderPath, "/"))
      {
        dummyPath = folderPath + "/" + "dummy"+ext;
      }
      else
      {
        dummyPath = folderPath + "dummy"+ext;
      }
      
      //for openseadragon 2 power problem add padding
      for (log = 1; log < 6; ++log)
      {
        bool exists = false;
        for (int j = 0; j < 50; ++j)
        {
          if (log2((maxCols + log) * tileDim) == j)
            exists = true;
          if (log2((maxRows + log) * tileDim) == j)
            exists = true;
        }
        if (exists)
        {
          continue;
        }
        else
        {
          break;
        }
      }
      
      int padding = log;

      //check through the files if not present copy dummy
#ifdef USE_OPENMP
      #pragma omp parallel for num_threads(8)
#endif
      for (int cx = 0; cx < maxCols + padding; ++cx)
      {
        for (int cy = 0; cy < maxRows + padding; ++cy)
        {
          //start with the folder name
          std::string fName = folderPath;
          if (!boost::ends_with(fName, "/"))
          {
            fName += "/";
          }
          
          std::string intStr = "";
          //x_y
          intStr = std::to_string(cx);
          fName += intStr;
          intStr = std::to_string(cy);
          fName += "_";
          fName += intStr;
          
          //folder+x_y+ext
          fName += ext;
          if (!fs::exists(fName))
          {
            fs::copy_file(dummyPath, fName);
//            if (sharpness)
//            {
//              // load the image
//              cv::Mat temp = cv::imread(fName);
//              cv::Mat tempOut;
//              Sharpen(&temp, &tempOut, kernel, strength);
//              cv::imwrite(fName, tempOut);
//              temp.release();
//              tempOut.release();
//            }
          }
          else
          {
            if (sharpness)
            {
              // load the image
              cv::Mat temp = cv::imread(fName);
              cv::Mat tempOut;
              Sharpen(&temp, &tempOut, kernel, strength);
              cv::imwrite(fName, tempOut);
              temp.release();
              tempOut.release();
            }
          }
        }
      }
      
      //delete the dummy file only if it exists
      if(fs::exists(dummyPath))
      {
        //delete the dummy file
        //std::cout << "deleting dummy.jpeg file from file path: " << dummyPath << std::endl;
        boost::filesystem::remove(dummyPath);
      }
      // Write out a dzi file
      long pyrHeight = maxRows + padding;
      long pyrWidth  = maxCols + padding;
      // remove the leading dot from fmt
      std::string fmt = ext;
      fmt.erase(0,1);
      WriteDZI(pyrWidth, pyrHeight, tileDim, fmt, folderPath);
      int val = startGeneration(folderPath, maxRows + padding, maxCols + padding, tileDim, ext,\
                                sharpness, kernel, strength);
    }
    return 0;
  }//end of function	
  
  /*! \brief Generate
   *  This is the main driver to generate a pyramind given a set of base tiles
   */
  int PyramidGeneration::Generate(std::string tileDir, \
                                  int tileDim, \
                                  std::string ext)
  {
    // Check if the tile directory is valid. A simple check we employ
    // is to find if the tile directory contains a dummy file. If it does not
    // contain that we return to the calling function
    std::string v = tileDir + "/dummy."+ext;
    boost::filesystem::path dir(v.c_str());
    if (!boost::filesystem::exists(dir))
    {
      // The file exists. We start a timer
      auto startProc = std::chrono::high_resolution_clock::now();
      checkDummy(tileDir, tileDim, ext, true, 5, 0.35);
      auto endProc = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double> elapsedA = endProc - startProc;
      pTimePyramid += elapsedA.count();
      return 1;
    }
    return -1;
  }
  
  /*! \brief Generate
   *  This is the main driver to generate a pyramind given a set of base tiles
   *  without any additional parameters
   */
  int PyramidGeneration::Generate(std::string tileDir, bool sharpness, float kernel, float strength)
  {
    // Check if the tile directory is valid. A simple check we employ
    // is to find if the tile directory contains a dummy file. If it does not
    // contain that we return to the calling function. We search for a
    // few patterns for extensions
    std::vector<std::string> exts={"jpeg","jpg","tif","tiff","bmp","png","JPEG","JPG","TIF","TIFF","BMP","PNG"};
    std::string v ="";
    std::string ext = "";
    bool dummyPresent = false;
    for (auto extValid : exts)
    {
      // Set the extensions
      ext = "."+extValid;
      // build the file name 
      v = tileDir + "/dummy."+extValid;
      // check if the file name exists
      boost::filesystem::path dir(v.c_str());
      if (boost::filesystem::exists(dir))
      {
          dummyPresent = true;
          //std::cout << "dummy.jpeg present" << std::endl;
          break;
      }
    }
    // Check if the dummy file name is present
    // if not we take dimensions from 0_0 with assumption that all
    // tiles are present
    if (!dummyPresent)
    {
        //std::cout << "dummy.jpeg not present, generating one..." << std::endl;
        for (auto extValid : exts)
        {
          // Set the extensions
          ext = "."+extValid;
          // build the file name
          v = tileDir + "/0_0."+extValid;
          // check if the file name exists
          //std::cout<<v<<std::endl;
          boost::filesystem::path dir(v.c_str());
          if (boost::filesystem::exists(dir))
          {
            // Copy the file as dummy.jpeg.
            std::string pathFromStr = tileDir + "/0_0." + extValid;
            std::string pathToStr = tileDir + "/dummy." + extValid;
            boost::filesystem::path pathFrom(pathFromStr);
            boost::filesystem::path pathTo(pathToStr);
            boost::filesystem::copy_file(pathFrom, pathTo, boost::filesystem::copy_option::overwrite_if_exists);
            v = pathToStr;
            break;
          }
        }
    }
    exts.clear();
    // Check if the dummy file name is valid
    if (v == "")
    {
      std::cout << "dummy.jpeg file name is invalid: " << v << std::endl;
      return -1;
    }
    // We have found a valid dummy file name. We now read the file and get 
    // its dimensions. Currently, we only support square tiles.
    cv::Mat dummy = cv::imread(v);
    int tileDim = 0;
    // As the tile dimensions have been satisfied, we proceed to the 
    // pyramid generation pipeline
    if (dummy.rows == dummy.cols)
    {
      tileDim = dummy.rows;
      // The file exists. We start a timer
      auto startProc = std::chrono::high_resolution_clock::now();
      checkDummy(tileDir, tileDim, ext, sharpness, kernel, strength);
      auto endProc = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double> elapsedA = endProc - startProc;
      pTimePyramid += elapsedA.count();
      return 1;
    }
    else
    {
      std::cout << "dimensions of the dummy.jpeg file do not match!" << std::endl;
      std::cout << "rows: " << dummy.rows << "\tdcols: " << dummy.cols << std::endl;
      return -2;
    }
  }//end of function
}//end of namespace

