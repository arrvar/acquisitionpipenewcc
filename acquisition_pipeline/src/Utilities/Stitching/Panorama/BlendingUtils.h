#ifndef BLENDINGUTILS_H_
#define BLENDINGUTILS_H_
#include "AbstractClasses/AbstractCallback.h"
#include <map>
#include <memory>

namespace spin
{
  // Forward declaration
  struct APModule;

  /*! \brief BlendingUtils
   *  This function is used as a placeholder for implementing blending
   *  between overlapping images
   */
  class BlendingUtils
  {
    public:
      // Default constructor
      BlendingUtils(){}
      // Default destructor
      ~BlendingUtils(){}
      // Signals for metrics and logging
      spin::CallbackSignal sig_Logs;
      spin::CallbackSignal sig_Metrics;

      /*! \brief Initialize */
      int Initialize(void* _q);
      /*! \brief BlendingPipeline*/
      int BlendingPipeline(void* idx, void* gObj, float t=-1,\
                           bool useMedian = false,\
                           bool disable_blending=false,\
                           bool generateLogs=false);
    private:
      // Module to be loaded
      std::shared_ptr<APModule> module;
      // Pairwise blending of images
      int PairwiseBlending( int r1, int c1, void* r, \
                            int r2, int c2, void* t,\
                            float* t1, float* t2);
  };//end of class
}//end of namespace
#endif
