#ifndef TILING_H_
#define TILING_H_
#include "AbstractClasses/AbstractCallback.h"
#include "Utilities/Stitching/Panorama/PanoramaDataStructures.h"

namespace spin
{ 
  /*! \brief Tiling
   *  This is the driver class to populate tile generation
   *  during a whole slide stitching process
   */
  class Tiling 
  {
    public:     
      // Default constructor
      Tiling(){}
      
      // Default destructor
      ~Tiling()
      {
        TilesMap::iterator it = map_AllTilesData.begin();
        while (it != map_AllTilesData.end())
        {
          it->second.fov_data.clear();
          ++it;
        }
        map_AllTilesData.clear();
      }
            
      // check if tile written to disk 
      bool CheckIfTileWrittenToDisk(void* tileIdx);
      
      // Initialize data structure 
      int GenerateDataStructure(void* gObject, bool logs=false);
      
      // Populate a given tile
      template <typename T>
      int PopulateTile(void* tileIdx, void* tile, void* pObj);
      
      // Signals for returning logs & metrics
      spin::CallbackSignal sig_Logs;
      
      // Get information about tiles
      TilesMap* GetTileMap()
      {
        return &map_AllTilesData;
      }//end of function
    private:
      TilesMap map_AllTilesData;
  };//end of class
};//end of namespace
#endif
