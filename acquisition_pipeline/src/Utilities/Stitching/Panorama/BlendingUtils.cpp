#include "AbstractClasses/AbstractDataStructures.h"
#include "AbstractClasses/AbstractPlugin.h"
#include "AbstractClasses/AbstractModuleObject.h"
#include "Utilities/Stitching/Panorama/PanoramaDataStructures.h"
#include "Utilities/Stitching/Panorama/BlendingUtils.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include "itkMedianImageFilter.h"
#include <dlibxx.hxx>
#ifdef USE_OPENCL
#include "Framework/GPU/GPUStruct.h"
#endif
#include "Parameters/KMeansParams.h"
namespace spin
{
  struct APModule
  {
    typedef spin::AbstractPlugin APType;
    ~APModule(){}
    dlibxx::handle lib;
    std::shared_ptr< APType > pModule;
#ifdef USE_OPENCL
  // Initialize GPU stuff
  GPUStruct gpu;
#endif
  };//end of struct
}//end of namespace

namespace spin
{
  namespace
  {
    typedef std::map<pDouble, BlendingTilingObject>   FOV;
    typedef itk::MedianImageFilter<RGBImageType, RGBImageType> MedianFilterType;
    typedef RGBImageType::RegionType RegionType;
    typedef spin::RMMatrix_Float Matrix;
    typedef std::vector<Matrix> vMat;
    typedef std::vector<vMat> vImage;
  }

  /*! \brief PairwiseBlending
   *  This function is a driver to take in two images, along with their coordinates in
   *  a global panorama canvas. The overlapping region is computed and blending is
   *  applied on the overlapping region.
   *  @ref_YRow       : The row (y) index of the reference image in the global panorama image
   *  @ref_XCol       : The col (x) index of the reference image in the global panorama image
   *  @refImg         : The actual reference AOI image
   *  @tgt_YRow       : The row (y) index of the target image in the global panorama image
   *  @tgt_XCol       : The col (x) index of the target image in the global panorama image
   *  @tgtImg         : The actual target AOI image
   */
  int BlendingUtils::PairwiseBlending(int ref_YRow, int ref_XCol, void* refImg, \
                                      int tgt_YRow, int tgt_XCol, void* tgtImg,\
                                      float* metricThresh, float* metricVal)
  {
    // Typecast the data
    RGBImageType::Pointer* pRefImg = static_cast<RGBImageType::Pointer*>(refImg);
    RGBImageType::Pointer* pTgtImg = static_cast<RGBImageType::Pointer*>(tgtImg);

    // Get the dimensions of the image
    RGBImageType::SizeType pSize = (*pRefImg)->GetLargestPossibleRegion().GetSize();

    // Allocate a region overlap object
    RegionStruct rstruct;
    RGBImageType::SizeType    refSizeType;
    RGBImageType::SizeType    tgtSizeType;
    RGBImageType::IndexType   refIdxType;
    RGBImageType::IndexType   tgtIdxType;

    // Compute the extent of the overlapping regions
    int DispWidth = tgt_XCol - ref_XCol;
    int DispHeight= tgt_YRow - ref_YRow;

    // a.) Along the columns
    if (DispWidth > 0)
    {
      if(DispWidth > pSize[0] )
      {
        return -1;
      }
      // The target image "leads" the reference image.
      tgtIdxType[0]   =0;
      tgtSizeType[0]  =pSize[0]-DispWidth;
      refIdxType[0]   =DispWidth;
      refSizeType[0]  =tgtSizeType[0];
    }
    else
    {
      if((std::abs(DispWidth) > pSize[0]) || (std::abs(DispWidth) <= 0))
      {
        return -2;
      }
      // The reference image "leads" the target image
      refIdxType[0]   =0;
      refSizeType[0]  =pSize[0]-abs(DispWidth);
      tgtIdxType[0]   =abs(DispWidth);
      tgtSizeType[0]  =refSizeType[0];
    }

    // b.) Along the rows
    if (DispHeight > 0)
    {
      if(DispHeight > pSize[1])
      {
        return -3;
      }

      // The target image "leads" the reference image
      tgtIdxType[1]   =0;
      tgtSizeType[1]  =pSize[1]-DispHeight;
      refIdxType[1]   =DispHeight;
      refSizeType[1]  =tgtSizeType[1];
    }
    else
    {
      if((std::abs(DispHeight) > pSize[1]) || (std::abs(DispHeight) <= 0))
      {
        return -4;
      }

      // The reference image "leads" the target image
      refIdxType[1]   =0;
      refSizeType[1]  =pSize[1]-abs(DispHeight);
      tgtIdxType[1]   =abs(DispHeight);
      tgtSizeType[1]  =refSizeType[1];
    }

    // Double check to make sure that the sizes are all real and positive
    if((refSizeType[0] <= 0) || (refSizeType[1] <= 0) ||\
       (tgtSizeType[0] <= 0) || (tgtSizeType[1] <= 0))
    {
      return -5;
    }

    // Build the regions of interest
    rstruct.refOverlapRegion.SetSize(refSizeType);
    rstruct.refOverlapRegion.SetIndex(refIdxType);
    rstruct.tgtOverlapRegion.SetSize(tgtSizeType);
    rstruct.tgtOverlapRegion.SetIndex(tgtIdxType);
    rstruct.metricThresh = *metricThresh;

    // Run the pipeline
    int g = module->pModule->ProcessPipeline(refImg,tgtImg,&rstruct);
    if (g !=1) return -6;
    // return the blending metric
    *metricVal = rstruct.metricVal;
    return 1;
  }//end of function

  /*! \brief Initialize
   *  This function is used to initialize the plugin that will be used for
   *  implementing the actual blending utility.
   */
  int BlendingUtils::Initialize(void* _q)
  {
    ModuleObject* q = static_cast<ModuleObject*>(_q);
    // sanity check
    if (!q) return -1;
    // Frame the module being loaded
    std::string libname = q->pPath+"/spin_plugin_"+q->pModuleName+".so";

    // Check if the library exists in the given path
    boost::filesystem::path dfile(libname.c_str());
    if (!boost::filesystem::exists(dfile))
    {
      return -2; // the file does not exis
    }

    // Instantiate an APModule object
    module = std::make_shared<APModule>();
    // And Instantiate the loader
    //module->lib = std::make_shared<dlibxx::handle>();
    // Resolve symbols when they are referenced instead of on load.
    module->lib.resolve_policy(dlibxx::resolve::lazy);
    // Make symbols available for resolution in subsequently loaded libraries.
    module->lib.set_options(dlibxx::options::global);// | dlibxx::options::no_delete);
    // Load the library specified.
    module->lib.load(libname);
    // Check if the library could be loaded
    if (!module->lib.loaded())
    {
      //std::cout<<module->lib->error()<<std::endl;
      return -3;
    }

    // Now, we try and create the plugin
    module->pModule = module->lib.template create<APModule::APType>("Processing_Plugin");
    // Check if the module was loaded
    if (!module->pModule.get()) return -4;

    // Every module will have a parameter file. We try to parse it to check the efficacy
    // and health of the module. If no parameter file is specified, we ignore
    // this check and assume that the module will run with default settings.
    if (q->pParamFile !="")
    {
      int v = module->pModule->InstantiatePipeline(q->pParamFile.c_str());
      if (v !=1) return -5;
    }

    return 1;
  }//end of function

  /*! \brief BlendingPipeline
   *  This function implements a blending between an incoming image and
   *  the neighbours that are available in the map
   *  @idx        : The FOV that is current being blended
   *  @gObj       : A structure that holds information pertaining to the FOV's
   *  @thresh     : A threshold to qualify a blending
   */
  int BlendingUtils::BlendingPipeline(void* idx, void* gObj, float thresh,\
                                      bool useMedian,\
                                      bool disable_blending,\
                                      bool generateLogs)
  {
    // 1.) Downcast the index and fft utils object and assign other data
    pDouble* gridRef             = static_cast<pDouble* >(idx);
    PanoramaObject* gridObj      = static_cast<PanoramaObject*>(gObj);
    int grid                     = gridObj->GetGridOfInterest();
    FOV* tFOV                    = const_cast<FOV*>(gridObj->GetAOI(grid));

    CallbackStruct cLogs;
    if (generateLogs)
    {
      // Instantiate callback structures
      cLogs.Module    = "File:=BlendingUtils.cpp; Function:=BlendingPipeline;";
    }

    if (!disable_blending)
    {
      // Iterate through blending_pairs. If they have already been blended,
      // we ignore them
      for(int i = 0; i < (*tFOV)[*gridRef].blending_pairs.size(); ++i)
      {
        // Neighbor inded
        pDouble nIdx = (*tFOV)[*gridRef].blending_pairs[i].second;
        pDouble pIdx = (*tFOV)[*gridRef].blending_pairs[i].first;
        // Check if the neighbor was previously blended with this parent
        if((*tFOV)[nIdx].blended) continue;
        if((*tFOV)[pIdx].blended) continue;

        // Get the co-ordinates of the target image
        int tgt_YRow = (*(gridObj->GetYRows(grid)))((int)nIdx.first, (int)nIdx.second);
        int tgt_XCol = (*(gridObj->GetXCols(grid)))((int)nIdx.first, (int)nIdx.second);

        // Get the co-ordinates of the reference image
        int ref_YRow = (*(gridObj->GetYRows(grid)))((int)pIdx.first, (int)pIdx.second);
        int ref_XCol = (*(gridObj->GetXCols(grid)))((int)pIdx.first, (int)pIdx.second);

        // Pairwise blending. If the neighbors dont overlap
        // the blending module will not be called!
        int pCost = PairwiseBlending( ref_YRow, ref_XCol, &((*tFOV)[pIdx].Img),\
                                      tgt_YRow, tgt_XCol, &((*tFOV)[nIdx].Img),\
                                      &thresh, \
                                      &((*tFOV)[pIdx].tMetrics[nIdx].nmi));
        if (pCost == 1)
        {
          // At the same time, push this into a metrics folder.
          // This can be retrived during calibration.
          gridObj->SetMetric(((*tFOV)[pIdx].tMetrics[nIdx].nmi));
        }
      }//finished all neighbors of this image
    }

    // Update information to indicate that this image has been blended
    (*tFOV)[*gridRef].blended = true;

    // Emit a signal
    if (generateLogs)
    {
      sig_Logs(cLogs);
    }

    // Clear neighbor container
    (*tFOV)[*gridRef].bNeighbors.clear();
    (*tFOV)[*gridRef].blending_pairs.clear();
    return 1;
  }// End of function
}//end of namespace
