#include "Utilities/Stitching/Panorama/Tiling.h"
#include "itkPasteImageFilter.h"
#include "itkImageRegionIteratorWithIndex.h"
#include "itkExtractImageFilter.h"

namespace spin 
{ 
  namespace
  {
    typedef std::map<pDouble, BlendingTilingObject> FOV;
  }

  /*! \brief PasteData
   *  This function is used to populate a tile given an index as well as a
   *  corresponding image. This function is internal to the tiling pipeline
   *  and is called within a loop from the main driver function
   */
  template <typename T>
  int PasteData(BoundingBox* bbox, void* _tile, void* _image)
  {
    // typecast the data
    typename T::Pointer* tile = static_cast<typename T::Pointer*>(_tile);
    typename T::Pointer* image = static_cast<typename T::Pointer*>(_image);
    typedef typename itk::PasteImageFilter <T, T> PasteImageFilterType;
    typename T::IndexType destinationIndex;
    typename T::IndexType desiredStart;
    typename T::SizeType  desiredSize;
    
    destinationIndex[0] = bbox->destIndex.second;
    destinationIndex[1] = bbox->destIndex.first;
    desiredStart[0]     = bbox->x;
    desiredStart[1]     = bbox->y;
    desiredSize[0]      = bbox->width;// width
    desiredSize[1]      = bbox->height;// height
	
    typename T::RegionType desiredRegion(desiredStart, desiredSize);
    typename PasteImageFilterType::Pointer pasteFilter = PasteImageFilterType::New();
    pasteFilter->SetSourceImage(*image);
    pasteFilter->SetDestinationImage(*tile);
    pasteFilter->SetSourceRegion(desiredRegion);
    pasteFilter->SetDestinationIndex(destinationIndex);
    try
    {
      pasteFilter->Update();
      (*tile)->Graft(pasteFilter->GetOutput());
    }
    catch(itk::ExceptionObject &e)
    {
      return -1;
    }

    return 1;
  }//end of function
  
  /*! \brief PopulateTile
   *  This function is the main driver to populate a tile from a given
   *  set of images
   */
  template <typename T>
  int Tiling::PopulateTile( void* idx, void* tile, void* gObj)
  {
    // Downcast objects
    pDouble* tileIdx            = static_cast<pDouble* >(idx);
    PanoramaObject* gridObj     = static_cast<PanoramaObject*>(gObj);
    int grid                    = gridObj->GetGridOfInterest();
    FOV* tFov                   = const_cast<FOV*>(gridObj->GetAOI(grid));
    
    // Populate the tile with all the image data required
    for(auto& fov_it : map_AllTilesData[*tileIdx].fov_data)
    {
      // Get a pointer to the image under consideration
      typename T::Pointer image = (*tFov)[fov_it.first].Img;
      // If the image has not been loaded do nothing
      if(!image) continue;
      // call the internal function for pasting data from image to tile
      /*if (tileIdx->second==2 && tileIdx->first==27)
      {
        //std::cout<<"xxxxxxxxxx"<<std::endl;
        //std::cout<<"Offending Tile: "<<(*tFov)[fov_it.first].aoiName<<std::endl;
        //std::cout<<map_AllTilesData[*tileIdx].fov_data[fov_it.first].destIndex.second<<" "<<map_AllTilesData[*tileIdx].fov_data[fov_it.first].destIndex.first<<std::endl;
        //std::cout<<map_AllTilesData[*tileIdx].fov_data[fov_it.first].x<<" "<<map_AllTilesData[*tileIdx].fov_data[fov_it.first].y<<std::endl;
      }*/
      BoundingBox bbox = map_AllTilesData[*tileIdx].fov_data[fov_it.first];
      PasteData<RGBImageType>(&bbox, tile, &image);
    }
    
    // Indicate that information pertaining to this tile has been written
    map_AllTilesData[*tileIdx].isWritten = true;

    return 1;
  }//end of function
  
  // Check if the tile was previously written to disk
  bool Tiling::CheckIfTileWrittenToDisk(void* idx)
  {
    pDouble* tileIdx = static_cast<pDouble* >(idx);
    return map_AllTilesData[*tileIdx].isWritten;
  }//end of function

  // Explicit template instantiations
  template int Tiling::PopulateTile<RGBImageType>(void*,void*,void*);
}//end of spin namespace
 
