#ifndef TILINGDATASTRUCTURES_H_
#define TILINGDATASTRUCTURES_H_
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "AbstractClasses/AbstractImageTypes.h"
// include headers that implement a archive in simple text format
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include <iostream>

namespace spin
{
  typedef std::pair<double,double> pDouble;

  struct StitchingMetrics
  {
    float nmi;
  };//end of struct

  // A structure holding information about the tile name and image
  struct TileDataType
  {
    ~TileDataType(){rgbImage = NULL;}
    RGBImageType::Pointer rgbImage;
    std::string name;
  };

  // A bounding box to define the rectangular ROI
  struct BoundingBox
  {
    int x = 0;
    int y = 0;
    int width = 0;
    int height = 0;
    pDouble destIndex = pDouble(0,0);
  };//end of struct

  typedef std::map<pDouble,BoundingBox> fovMap;

  // Data structure to hold the tile data for the aoi<->tile map
  struct TileData
  {
    ~TileData()
    {
      fov_data.clear();
    }
    // A vector holding information about parent AOI's
    // as well as regions that need to be copied from those AOI's
    fovMap  fov_data;
    pDouble position;
    int     counter;
    bool    isWritten;
    // Name of the tile stored in disk
    std::string name;
  };//end of struct

  // Map object to contain information on all tiles in the grid
  typedef std::map <pDouble, TileData> TilesMap;

  /*! \brief A Structure to hold information pertaining to AOI's
   *  that will be used during blending and tiling to create a panorama
   */
  struct BlendingTilingObject
  {
    ~BlendingTilingObject()
    {
      base_tiles.clear();
      tile_list.clear();
      bNeighbors.clear();
      tMetrics.clear();
	    blending_pairs.clear();
    }

    // Name of AOI
    std::string                               aoiName;
    // The index location of this AOI within the scanning grid (global)
    pDouble                                   index;
    // A list of all neighbors (blending)
    std::vector< pDouble >                    bNeighbors;
	  // A list of pairs that need to be blended before this image is 
	  // dispatched for tiling
	  std::vector< std::pair<pDouble, pDouble> > blending_pairs;
    // A list of metrics between all neighbors
    std::map< pDouble, StitchingMetrics>      tMetrics;
    // The actual image
    RGBImageType::Pointer                     Img;
    // list of all the tiles this aoi contributes to
    std::vector< pDouble >                    tile_list;
    // list of all the tile images and their names in a vector
    std::vector< TileDataType >               base_tiles;
    // Is the image blended with all of its neighbours?
    bool                                      blended = false;
    // Has the image been white corrected?
    bool                                      corrected = false;
    // Has memory been allocated for the image?
    bool                                      loaded = false;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
      ar & aoiName;
      ar & index;
    }//end of class

  };//end of struct

  /*! \brief PanoramaObject
   *  This class is a placeholder for various functions and parameters
   *  associated with building a panorama from a set of valid AOI's
   */
  class PanoramaObject
  {
    public:
      PanoramaObject()
      {
        // Default settings
        tWidth   = 512;
        tHeight  = 512;
        // Corresponds to Matrix Vision cameras
        aoiWidth = 1936;
        aoiHeight= 1216;
        bMetrics.clear();
        aoi.clear();
      }
      ~PanoramaObject()
      {
        aoi.clear();
        bMetrics.clear();
      }
	  
      /*! \brief Set file extension */
      void SetFileExtension(std::string h)
      {
        file_extension = h;
      }

      /*! \brief Get file extension */
      const std::string GetFileExtension()
      {
        return file_extension;
      }

      /*! \brief Set Calibration Image */
      void SetCalibrationImage(std::string cImg)
      {
        calImg = cImg;
      }//end of function

      /*! \brief Get Calibration Image */
      const std::string GetCalibrationImage()
      {
        std::string h = calImg;
        return h;
      }//end of function

      /*! \brief Set Norm Image */
      void SetNormImage(std::string cImg)
      {
        normImg = cImg;
      }//end of function

      /*! \brief Get Norm Image */
      const std::string GetNormImage()
      {
        std::string h = normImg;
        return h;
      }//end of function

      /*! \brief Set Image Directory */
      void SetImageDirectory(std::string h)
      {
        imgDir = h;
      }

      /*! \brief Get Image Directory */
      const std::string GetImageDirectory()
      {
        return imgDir;
      }

      /*! \brief Set Tile Directory */
      void SetTileDirectory(std::string h)
      {
        tileDir = h;
      }

      /*! \brief Get Tile Directory */
      const std::string GetTileDirectory()
      {
        return tileDir;
      }

      /*! \brief Set Original Grid Dimensions */
      void SetOriginalGridDimensions(int h, int w)
      {
        gridWidth = w;
        gridHeight= h;
      }

      /*! \brief Set Image Dimensions */
      void SetImageDimensions(int h, int w)
      {
        aoiWidth = w;
        aoiHeight= h;
      }

      /*! \brief Set the Y coordinates of the grid */
      void SetYRows(RMMatrix_Double* y, int gIdx)
      {
        gridYRows[gIdx] = *y;
      }

      /*! \brief Set the X coordinates of the grid */
      void SetXCols(RMMatrix_Double* x, int gIdx)
      {
        gridXCols[gIdx] = *x;
      }

      /*! \brief Set the Panorama dimensions */
      void SetPanoramaDimensions (long h, long w, int gIdx)
      {
        panoramaWidth[gIdx] = w;
        panoramaHeight[gIdx]= h;
      }

      /*! \brief Set Grid Dimensions */
      void SetTileDimensions(int h, int w)
      {
        tWidth = w;
        tHeight= h;
      }

      /*! \brief Set Grid Of Interest */
      void SetGridOfInterest(int g)
      {
        gridOfInterest = g;
      }

      /*! \brief Set Grid Of Interest */
      const int GetGridOfInterest()
      {
        return gridOfInterest;
      }

      /*! \brief SetAOIInfo */
      void SetAOIInfo(std::string& name, pDouble idx, int gIdx)
      {
        aoi[gIdx][idx].aoiName = name;
        aoi[gIdx][idx].index   = idx;
      }//end of function

      /*! \brief Get number of grids */
      const int GetNumGrids(){return aoi.size();}

      /*! \brief Get The Y coordinates of the grid */
      const RMMatrix_Double* GetYRows(int gIdx){return &gridYRows[gIdx];}

      /*! \brief Get the X coordinates of the grid */
      const RMMatrix_Double* GetXCols(int gIdx){return &gridXCols[gIdx];}

      /*! \brief Get Panorama Height */
      const long GetPanoramaHeight(int gIdx){return panoramaHeight[gIdx];}

      /*! \brief Get Panorama Width */
      const long GetPanoramaWidth(int gIdx){return panoramaWidth[gIdx];}

      /*! \brief Get Image Width */
      const int GetAOIWidth(){return aoiWidth;}

      /*! \brief Get Image Height */
      const int GetAOIHeight(){return aoiHeight;}

      /*! \brief Get tile Width */
      const int GetTileWidth(){return tWidth;}

      /*! \brief Get tile Height */
      const int GetTileHeight(){return tHeight;}

      /*! \brief Get original grid Width */
      const int GetOriginalGridWidth(){return gridWidth;}

      /*! \brief Get original grid Height */
      const int GetOriginalGridHeight(){return gridHeight;}

      /*! \brief GetAOIMap */
      const std::map<int, std::map<pDouble, BlendingTilingObject> >* GetAOIMap()
      {
        return &aoi;
      }

      /*! \brief GetAOI */
      const std::map<pDouble, BlendingTilingObject>* GetAOI(int gIdx){return &aoi[gIdx];}

      /*! \brief SetMetric */
      void SetMetric(float m)
      {
        bMetrics.push_back(m);
      }//end of function
      
      /*! \brief GetMetrics */
      std::vector<float> GetMetrics()
      {
        return bMetrics;
      }//end of function
      
      /*! \brief Utility Function */
      void PrintInfo()
      {
        //std::cout<<calImg<<std::endl;
        //std::cout<<"============="<<std::endl;
        //std::cout<<normImg<<std::endl;
        //std::cout<<"============="<<std::endl;
        //std::cout<<aoiWidth<<" "<<aoiHeight<<std::endl;
        //std::cout<<"============="<<std::endl;
        //std::cout<<gridWidth<<" "<<gridHeight<<std::endl;
        //std::cout<<"============="<<std::endl;
        //std::cout<<imgDir<<std::endl;
        //std::cout<<"============="<<std::endl;
        //std::cout<<aoi.size()<<std::endl;
        //std::cout<<"============="<<std::endl;

        for (int i = 0; i < aoi.size(); ++i)
        {
          //std::cout<<gridYRows[i]<<std::endl;
          //std::cout<<"============="<<std::endl;
          //std::cout<<gridXCols[i]<<std::endl;
          //std::cout<<"============="<<std::endl;
          //std::cout<<panoramaWidth[i]<<" "<<panoramaHeight[i]<<std::endl;
          //std::cout<<"============="<<std::endl;
          std::map<pDouble, BlendingTilingObject>::iterator it = aoi[i].begin();
          while(it != aoi[i].end())
          {
            //std::cout<<it->second.aoiName<<" -> "<<it->second.index.second<<" "<<it->second.index.first<<std::endl;
            ++it;
          }
        }
      }
    private:
      // Calibration image
      std::string calImg="";
      // Normalization image
      std::string normImg="";
      // Image directory
      std::string imgDir="";
      // File extension
      std::string file_extension="";

      // Grid coordinates
      std::map<int, RMMatrix_Double> gridYRows;
      std::map<int, RMMatrix_Double> gridXCols;

      // Original image dimensions
      int             aoiWidth;
      int             aoiHeight;

      // Panorama Dimensions
      std::map<int, long>            panoramaHeight;
      std::map<int, long>            panoramaWidth;

      // Tile dimension
      int             tWidth;
      int             tHeight;
      std::string     tileDir="";

      // List of valid AOI's in the panorama
      std::map<int, std::map<pDouble, BlendingTilingObject> > aoi;

      // Grid of interest
      int gridOfInterest;
      int gridWidth;
      int gridHeight;
      
      // A vector containing blending metrics of all image pairs
      // and is primarily used during calibration
      std::vector<float> bMetrics;
    private:
      friend class boost::serialization::access;
      // When the class Archive corresponds to an output archive, the
      // & operator is defined similar to <<.  Likewise, when the class Archive
      // is a type of input archive the & operator is defined similar to >>.
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version)
      {
        ar & imgDir;
        ar & file_extension;
        ar & gridYRows;
        ar & gridXCols;
        ar & aoiWidth;
	      ar & aoiHeight;
	      ar & panoramaHeight;
	      ar & panoramaWidth;
        ar & aoi;
        ar & gridWidth;
        ar & gridHeight;
        ar & calImg;
        ar & normImg;
      }//end of class
  };//end of class
}//end of namespace
#endif
