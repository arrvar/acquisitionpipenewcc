#include "Utilities/Stitching/Panorama/Tiling.h"
// Boost includes
#include <boost/foreach.hpp>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/index/rtree.hpp>

namespace boost 
{
  namespace geometry 
  {
    namespace index 
    {
      template <typename Box>
      struct indexable< boost::shared_ptr<Box> >
      {
	    typedef boost::shared_ptr<Box> V;
	    typedef Box const& result_type;
	    result_type operator()(V const& v) const { return *v; }
      };
    }// end of namespace index
  }// end of namespace geometry
} // end of namespace boost

namespace spin 
{ 
  namespace
  {
    typedef boost::geometry::model::point<double, 2, boost::geometry::cs::cartesian> point;
    typedef boost::geometry::model::box<point>  box;
    typedef std::pair<box, pDouble>             value;
  };//end of namespace
  
  /*! \brief GenerateDataStructure
   *  API to implement pair-wise image tiling. This generates the data structure
   *  which will be referred to while populating the tiles. This is called after
   *  all displacement optimizations have taken place in the stitching pipeline
   *  @gObject          : The PanoramaObject containing information about tiles
   *  @generateLogs     : Check if we need to generate logs 
   */
  int Tiling::GenerateDataStructure(void* gObject, bool generateLogs)
  {
    // typecast the data
    PanoramaObject* gmap = static_cast<PanoramaObject*>(gObject);
    if (!gmap) return -1;
    int grid       = gmap->GetGridOfInterest();
    // Get pointers to the Y and X coordinates for this grid
    const RMMatrix_Double* gridYRows = gmap->GetYRows(grid);
    const RMMatrix_Double* gridXCols = gmap->GetXCols(grid);
    // Get the panorama dimensions
    long panoramaHeight = gmap->GetPanoramaHeight(grid);
    long panoramaWidth  = gmap->GetPanoramaWidth(grid);
    // Get the tile dimensions
    int tHeight         = gmap->GetTileWidth();
    int tWidth          = gmap->GetTileHeight();    

    CallbackStruct cLogs;
    // Instantiate a logging struct if required
    if (generateLogs)
    {      
      cLogs.Module = "File: Tiling; Module: GenerateDataStructure";
    }
    
    // Create a matrix that will hold all the Row and column positions of the 
    // tiles. 
    int num_w_tiles = std::ceil((float)panoramaWidth / (float)tWidth);
    int num_h_tiles = std::ceil((float)panoramaHeight / (float)tHeight);
    
    // In addition, we ensure that the # of tiles are divisible by 4
    num_w_tiles = std::ceil((float)num_w_tiles/4.0) * 4;
    num_h_tiles = std::ceil((float)num_h_tiles/4.0) * 4;

    // The panorama dimensions would need to be modified to accommodate all the tiles
    // TODO: Why do we need this?
    long panoramaHeight_modified = tHeight * num_h_tiles * 2;
    long panoramaWidth_modified  = tWidth  * num_w_tiles * 2;

    if (num_w_tiles <=1 || num_h_tiles <=1) 
    {
      if (generateLogs)
      {
        cLogs.Error="Tiles are invalid: "+std::to_string(num_w_tiles);
        cLogs.Error+=", "+std::to_string(num_h_tiles);
        cLogs.Result=-2;
        sig_Logs(cLogs);
      }
      return -3;
    }

    // create the rtree using default constructor
    const int max_elements = 4;
    const int min_elements = 2;
    // create the rtree using default constructor
    boost::geometry::index::rtree< value, boost::geometry::index::quadratic<max_elements, min_elements> > rtree;
    
    // We iterate through all tile coordinates positions and create a range-tree data structure
    auto counter = 0;
    for (int r = 0; r < panoramaHeight_modified; r+= tHeight)
    for (int c = 0; c < panoramaWidth_modified ; c+= tWidth)
    {
      // The tile index
      pDouble tileIdx(r/tHeight, c/tWidth);
      
      // Point at top-left corner
      point TopLeft(r,c); // top-left
      // and the bottom right corner
      point BottomRight(r+tHeight-1,c+tWidth-1);
      // Create a bounding box with these points
      box   BoundingBox(TopLeft, BottomRight);
      
      // Insert it into the range tree
      rtree.insert(std::make_pair(BoundingBox, tileIdx));
    }// finished iterating through all tile indices

    // Now, iterate through all AOI's and do the following:
    // a.) We first find the tiles that potentially overlap
    //     with the given AOI
    // b.) Having identified the tiles, we compute the overlap
    //     or intersection region of a tile with the given AOI.
    //     This region is translated into a local coordinate
    //     space such that a ROI from the image AOI is selected.
    typedef std::map<pDouble, BlendingTilingObject> FOV;
    FOV* dFov = const_cast<FOV*>(gmap->GetAOI(grid));
    int imgWidth  = gmap->GetAOIWidth();
    int imgHeight = gmap->GetAOIHeight();
    
    // Iterate through all the AOI's
    FOV::iterator it = dFov->begin();
    while(it != dFov->end())
    {
      // Get the index of the AOI being processed
      pDouble fovIdx = it->first;
      
      // Build the bounding box associated with this AOI
      pDouble imgStart((*gridYRows)((int)fovIdx.first, (int)fovIdx.second), (*gridXCols)((int)fovIdx.first, (int)fovIdx.second));
      pDouble imgEnd(imgStart.first + imgHeight - 1, imgStart.second + imgWidth - 1);

      // Get the corresponding region points
      int x1 = (*gridXCols)((int)fovIdx.first, (int)fovIdx.second);
      int y1 = (*gridYRows)((int)fovIdx.first, (int)fovIdx.second);
      int x2 = x1 + imgWidth - 1;
      int y2 = y1 + imgHeight - 1;

      // find values intersecting some area defined by a box
      point topLeft( y1 , x1 );
      point bottomRight( y2 , x2 );
      // The query routine
      box query_box(topLeft, bottomRight);
      std::vector<value> result_s;
      // The following line returns all tiles that intersect with this
      // AOI. The result is stored in result_s
      rtree.query(boost::geometry::index::intersects(query_box), std::back_inserter(result_s));

      // For all tiles that overlap with this image, we compute the
      // overlap area in a local coordinate space
      BOOST_FOREACH(value const&p, result_s)
      {
        // Get the tile (as a box) that is being processed
        box pTile = p.first;
        // Assign an output overlap region to a dummy variable
        box pOverlap;
        // Compute the overlap region of the tile with the AOI
        boost::geometry::intersection(pTile,query_box,pOverlap);
        
        // Get the two corners of the overlapping region
        point pnt1 = pOverlap.min_corner(); // y1,x1
        point pnt2 = pOverlap.max_corner(); // y2,x2  
        
        // Get the size of the overlapping region and populate a 
        // bounding box structure to hold this information
        BoundingBox mBoundingBox;
        mBoundingBox.width  = static_cast<int>(pnt2.get<1>() - pnt1.get<1>() + 1);
        mBoundingBox.height = static_cast<int>(pnt2.get<0>() - pnt1.get<0>() + 1);
        // We transform the starting point to a local coordinate space
        // in the AOI from which image will be copied to the tile
        mBoundingBox.x      = static_cast<int>(pnt1.get<1>() - x1);
        mBoundingBox.y      = static_cast<int>(pnt1.get<0>() - y1);
        // Similary, we transform the coordinates where this data will be pasted 
        // into the tile to a local coordinate space
        point tileCoord = pTile.min_corner();
        mBoundingBox.destIndex = pDouble(pnt1.get<0>() - tileCoord.get<0>(),\
                                         pnt1.get<1>() - tileCoord.get<1>());
        // Get the tile index as a pDouble
        pDouble tIdx = p.second;  

        // Populate the data structures with the relevant information
        if(map_AllTilesData.find(tIdx)!=map_AllTilesData.end())
        {
          map_AllTilesData[tIdx].isWritten = false;
          //map_AllTilesData[tIdx].position  = minPnt;
          map_AllTilesData[tIdx].fov_data.insert(std::pair<pDouble, BoundingBox>(fovIdx, mBoundingBox));
        }
        else
        {
          TileData mTileData;
          mTileData.isWritten = false;
          //mTileData.position = minPnt;
          mTileData.fov_data.insert(std::pair<pDouble, BoundingBox>(fovIdx, mBoundingBox));
          map_AllTilesData.insert(std::pair<pDouble, TileData>(tIdx, mTileData));
        }

        // populate the list of tiles
        it->second.tile_list.push_back(tIdx);
      }// End of BOOST for each through the outputs of the query
      
      // Free memory
      result_s.clear();
      
      // Next Image
      ++it;
    }// End of FOR through each AOI

    rtree.clear();
    
    //At this time, we send a signal that tile coordinates are available to
    //be written to the database
    if (generateLogs)
    {
      sig_Logs(cLogs);
    }

    //exit(1);
    return 1;
  }// End of function GenerateDataStructure
}//end of namespace