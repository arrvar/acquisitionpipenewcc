#include "AbstractClasses/AbstractDataStructures.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/GridMapGeneration.h"
#include "Utilities/Stitching/Panorama/PanoramaDataStructures.h"
#include "Utilities/Stitching/Acquisition/TranslationEstimation.h"
#include <complex>
#include <algorithm>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/connected_components.hpp>
#include <fstream>
namespace spin
{
  namespace MST
  {
    typedef spin::vista::pDouble 		pDouble;
    typedef spin::vista::GridInfo		GridInfo;
    typedef spin::vista::DirectionType      DirectionType;
    typedef spin::vista::GridMapGeneration  GridMapGeneration;
    typedef spin::vista::WLSStruct          WLSStruct;

    struct MappingLNode
    {
      int localIndex;
      int globalIndex;
      pDouble lIdx;
      pDouble gIdx;
      Complex pOffset;
      int cComponent;
    };//end of struct
  }

  /*! \brief GlobalOptimization
   *  This is the driver function to compute the position of the AOI's in a global
   *  panorama image. The following steps are employed in this process:
   *  a.) A minimum spanning tree is implemented on the grid graph previously
   *      constructed
   *  b.) From the MST, shortest paths to the root node are computed. Essentially
   *      this gives us a list of AOI's with which a global context should be
   *      computed for any given aoi. 
   *  c.) During traversal, the 2D position of an AOI is also computed by vector
   *      summation of positions present in the shortest path. Care is taken to
   *      account for changes in direction along rows or colmns of the grid
   *
   *  @gObj         : The grid map object constaining information about AOI locations
   *                  as well as the graph from ---which a MST will be computed
   *
   *  This function also populates a data structure which contains some relevant
   *  details required to visualize/debug the MST
   */
  int TranslationEstimation::GlobalOptimizationMST( void* gObj, \
                                                    std::string slideDir, \
                                                    std::string& modelFile)
  {
    typedef MST::GridMapGeneration::gridFOV      FOV;
    typedef MST::GridMapGeneration::nodeMapping  NMapping;
    typedef MST::GridMapGeneration::MMapping     Mapping;
    typedef MST::GridMapGeneration::MSTMapping   MSTMapping;
    typedef MST::GridMapGeneration::MCost        Cost;
    typedef MST::WLSStruct                        WLSStruct;

    MST::GridMapGeneration* gridObj = static_cast<MST::GridMapGeneration*>(gObj);
    FOV* tFOV                       = gridObj->GetGridMapping();
    MSTMapping* mstmapping          = &(gridObj->GetMSTMapping());
    NMapping* nmap                  = &(gridObj->GetNodeMap());
    Mapping* mapping                = &(gridObj->GetTMapping());
    MST::GridInfo* gInfo            = &(gridObj->GetGridHeader());
    Cost* mappingCost               = &(gridObj->GetTCost());
    int tSize                       = gridObj->GetMinConnectedComponent();
    WLSStruct* wls                  = &(gridObj->GetWLSStruct());

    int h = GlobalOptimizationWLS(gObj);
    if (h !=1)
    {
      //std::cout<<"Warning: WLS failed with error: "<<h<<std::endl;
    }

    // Initialize a model
    std::shared_ptr<PanoramaObject> model = std::make_shared<PanoramaObject>();
    // Populate necessary information
    model.get()->SetImageDimensions(gInfo->aoiHeight, gInfo->aoiWidth);
    model.get()->SetImageDirectory(slideDir);

    // Instantiate a callback struct
    CallbackStruct cLogs;
    cLogs.Module = "File: TranslationEstimation.cpp; Module: GlobalOptimizationMST";

    // 1.) Before we begin the process, we clear redundant FFT's
    FreeResidualData(gObj);

    // 2.) First, we estimate the number of connected components. In order
    //     to do that, we need to add relevant edges to the graph g.
    graph_t* g = &(gridObj->GetGraph());
        
    //https://stackoverflow.com/questions/28854640/eigen-sparse-matrix-get-indices-of-nonzero-elements
    for (int k=0; k < mstmapping->outerSize(); ++k)
    {
      for (RMSparseMatrix_Int::InnerIterator it(*mstmapping,k); it; ++it)
      {
        // Add an edge between the nodes
        edge_t e;
        bool inserted = false;
        // The following line seems to cause memory-spikes in TranslationEstimation. Why?
        boost::tie(e, inserted) = boost::add_edge(it.row(), it.col(), *g);
      }
    }
    std::vector<int> component(boost::num_vertices(*g));
    int num = boost::connected_components(*g, &component[0]);

    // 3.) Create a map that will hold these connected components
    RMMatrix_Int ccMap = RMMatrix_Int::Zero(gInfo->gridHeight, gInfo->gridWidth);
    std::map<int, std::vector<int> > mapCC;
    std::vector<int>::size_type i;
    for (i = 0; i != component.size(); ++i)
    {
      // Get the connected component of the particular node
      int ccNode = component[i];
      // Place the respective node in the given connected component
      mapCC[component[i]].push_back(i);
      // In addition,we populate ccMap
      pDouble qIdx = nmap->left.at(i);
      ccMap((int)qIdx.first,(int)qIdx.second) = ccNode;
    }

    // 4.) Based on tSize, we dowselect only k-connected components
    std::map<int, std::vector<int> >::iterator it = mapCC.begin();
    std::map<int, std::vector<int> > validCC;
    while (it != mapCC.end())
    {
      if (it->second.size() > tSize)
      {
        validCC[it->first] = it->second;
      }
      it->second.clear();
      ++it;
    }

    // We return to the calling function if no valid connected components
    // can be generated
    if (validCC.size() <=0 ) return -1;

    // 5.) Having down-selected the connected components, we traverse through all
    // of them sequentially and build grid-coordinates. A vector of data structures
    // is defined to hold this information.
    std::map<int, std::vector<MST::MappingLNode> > localMapping;
    std::map<int, std::vector<int> >::iterator vt = validCC.begin();
    auto countIdx = 0;
    while (vt != validCC.end())
    {
      // Having obtained the largest connected component, we build a graph based on the nodes
      std::vector<int> nodesLargestCC = vt->second;
      // sort the nodes in ascending order
      std::sort(nodesLargestCC.begin(), nodesLargestCC.end());
      // Build a LUT based on local nodes numbering to global nodes numbering
      std::map< int, int> G2LMapping;
      std::map< int, int> L2GMapping;
      auto lNode=0;
      // Initialize a boost graph
      graph_t       LocalGraph;
      edgeWeight_t  LocalWeightMap;
      for (auto node_j: nodesLargestCC)
      {
        // Map the local node to the global node
        L2GMapping[lNode] = node_j;
        // Map the global node to the local node
        G2LMapping[node_j] = lNode;
        // Add a vertex to the graph (with local node numbering)
        vertex_t u = boost::add_vertex(LocalGraph);
        ++lNode;
      }

      // Having initialized the graph, we now populate the edges based on the
      // cost between each node and their 4-connected neighbors
      double neighbors[]={-1,0,0,-1,1,0,0,1};
      for (int c = 0; c < nodesLargestCC.size(); ++c)
      {
        // Get the global index
        int gIdx = nodesLargestCC[c];

        // Get the mapping for this index
        pDouble qIdx = nmap->left.at(gIdx);

        // Get the neighbors of this global index in global coordinate space
        std::vector<pDouble> pNeighbors(4);
        pNeighbors[0] = std::make_pair(qIdx.first+neighbors[0],qIdx.second+neighbors[1]);
        pNeighbors[1] = std::make_pair(qIdx.first+neighbors[2],qIdx.second+neighbors[3]);
        pNeighbors[2] = std::make_pair(qIdx.first+neighbors[4],qIdx.second+neighbors[5]);
        pNeighbors[3] = std::make_pair(qIdx.first+neighbors[6],qIdx.second+neighbors[7]);

        // Now, check if these neighbors are valid. One way to check these are valid is
        // to apply a right mapping and check if the indices are valid ones
        for (int k = 0; k < 4; ++k)
        {
          if (nmap->right.find(pNeighbors[k]) != nmap->right.end())
          {
            // This is a valid neighbor. Let us now check if it belongs to
            // the same connected component
            int vComp = ccMap((int)pNeighbors[k].first,(int)pNeighbors[k].second);
            if (vComp == vt->first)
            {
              // Yes it belongs to the same connected component.Now
              // add an edge. In order to achieve that, we get the
              // cost between these global nodes.
              int nIdx = nmap->right.at(pNeighbors[k]);
              double cost = static_cast<double>(mappingCost->coeffRef(gIdx,nIdx));
              // Having obtained the cost, we now add an edge in the local-coordinate space.
              // Get the local node index for the neighbor
              int lIdx = G2LMapping[nIdx];
              // Check if the edge was added earlier
              if ( !(boost::edge(c, lIdx, LocalGraph).second))
              {
                // It was not added; We can add it again
                edge_t e2;
                bool inserted = false;
                boost::tie(e2, inserted) = boost::add_edge(c, lIdx, LocalGraph);
                if (inserted)
                {
                  // Add an inverse of the cost as the arc weight.
                  LocalWeightMap[e2] = 1.0 / (1e-8 + cost);
                }
              }//Neighbor Node belongs to the same CC
            }//finished adding an edge between the two nodes
          }//Finished adding edges for all valid neighbors (4-connected)
        }//finished building graph for local coordinate space
      }//finished parsing all nodes in the connected component

      // Having built a graph for the connected component, we build a MST
      std::vector < vertex_t > p(boost::num_vertices(LocalGraph));
      boost::prim_minimum_spanning_tree(LocalGraph, &p[0]);

      // Get the minimum value of the coordinates so as to scale them
      double mMin_Y = 0;
      double mMin_X = 0;

      // The first AOI is the root for the MST. We place it in a local mapping structure
      MST::MappingLNode tMappingRoot;
      tMappingRoot.localIndex = 0;
      tMappingRoot.globalIndex= L2GMapping[tMappingRoot.localIndex];
      tMappingRoot.gIdx = nmap->left.at(tMappingRoot.globalIndex);
      tMappingRoot.pOffset = Complex(0,0);
      tMappingRoot.cComponent = vt->first;
      // And push this into a vector
      localMapping[vt->first].push_back(tMappingRoot);
      model.get()->SetAOIInfo((*tFOV)[tMappingRoot.gIdx ].aoiName, tMappingRoot.gIdx , countIdx);
      
      // Extract the paths, from each node to the root node
      for (std::size_t i = p.size() - 1; i != 0; --i)
      {
        // Convert the local node to a global node
        int gNode1 = L2GMapping[(int)i];
        // Get the node index in a std::pair format
        pDouble pIdx = nmap->left.at(gNode1);
        // Instantiate an offset
        Complex pOffset(0, 0);

        // The graph is built using local nodes, but displacements
        // have been computed globally. Hence we need to keep track of
        // both indices.
        int jLocal = i;
        int jGlobal= gNode1;
        
        // Traverse through all the nodes until we reach the root node
        while ((p[jLocal] != jLocal))
        {
          // Get the global node index of the neighbor
          int gNode2 = L2GMapping[(int)p[jLocal]];
          // Get the displacement vector from j to gNode2
          Complex disp = mapping->coeffRef(jGlobal, gNode2);
          // and add it to pOffset
          pOffset += disp;
          // Now, we move to the next node (in the global space)
          jGlobal = gNode2;
          // And update the next node in the local space
          jLocal  = p[jLocal];
        } // End of While p[jLocal] != jLocal

        // Update the min coordinate values
        if (pOffset.real() < mMin_Y) mMin_Y = pOffset.real();
        if (pOffset.imag() < mMin_X) mMin_X = pOffset.imag();

        // Place the information in a structure
        MST::MappingLNode tMapping;
        tMapping.localIndex = (int)i;
        tMapping.globalIndex= gNode1;
        tMapping.gIdx = pIdx;
        tMapping.pOffset = pOffset;
        tMapping.cComponent = vt->first;
        // And push this into a vector
        localMapping[vt->first].push_back(tMapping);

        // In addition, we add this aoi to the list of AOI's to be
        // processed further
        model.get()->SetAOIInfo((*tFOV)[pIdx].aoiName, pIdx, countIdx);
      }//finished extracting shortest paths for all nodes

      // Free memory
      p.clear();

      // Now, we correct the offsets for all coordinates and get the dimensions of the local grid
      long hMin = 1e10;
      long hMax = -1e10;
      long wMin = 1e10;
      long wMax = -1e10;

      // Allocate memory for holding grid coordinates
      RMMatrix_Double gridYRows = RMMatrix_Double::Ones(gInfo->gridHeight, gInfo->gridWidth)*(-1);
      RMMatrix_Double gridXCols = RMMatrix_Double::Ones(gInfo->gridHeight, gInfo->gridWidth)*(-1);

      for (int localNode = 0; localNode < localMapping[vt->first].size(); ++localNode)
      {
        // Convert the coordinates to integers
        Complex t = localMapping[vt->first][localNode].pOffset;
        // Subtract the minimum values to make them non-negative
        t = Complex(std::floor(t.real()-mMin_Y), std::floor(t.imag()-mMin_X));
        // Keep track of the range
        if (t.real() > hMax) hMax = (long)t.real();
        if (t.real() < hMin) hMin = (long)t.real();
        if (t.imag() > wMax) wMax = (long)t.imag();
        if (t.imag() < wMin) wMin = (long)t.imag();

        // update the offset
        localMapping[vt->first][localNode].pOffset = t;
        gridYRows((int)localMapping[vt->first][localNode].gIdx.first, (int)localMapping[vt->first][localNode].gIdx.second) = t.real();
        gridXCols((int)localMapping[vt->first][localNode].gIdx.first, (int)localMapping[vt->first][localNode].gIdx.second) = t.imag();
      }
      
      // Free memory
      localMapping[vt->first].clear();
      G2LMapping.clear();
      L2GMapping.clear();

      // Compute panorama information
      long pHeight = (hMax - hMin) + 1 + gInfo->aoiHeight;
      long pWidth  = (wMax - wMin) + 1 + gInfo->aoiWidth;

      // We now populate a panorama grid by calling the appropriate model object
      model.get()->SetPanoramaDimensions(pHeight, pWidth, countIdx);
      model.get()->SetYRows(&gridYRows, countIdx);
      model.get()->SetXCols(&gridXCols, countIdx);

      // Go to the next connected component
      ++vt;
      // increment the count
      ++countIdx;

#ifdef DEBUG
      std::ofstream file2("B_XCoordinates_afterMST2.txt");
      std::ofstream file3("B_YCoordinates_afterMST2.txt");
      if (file2.is_open())
      {
        file2 << gridXCols;
        file2.close();
        file3 << gridYRows;
        file3.close();
      }
#endif
    }//finished all connected components

    // We also set some miscellaneous information
    model.get()->SetOriginalGridDimensions(gInfo->gridHeight, gInfo->gridWidth);
    model.get()->SetFileExtension(gInfo->fileExt);
    model.get()->SetCalibrationImage(gInfo->calibImg);
    model.get()->SetNormImage(gInfo->normImg);

    // We serialize the model
    {
      std::ofstream ofs(modelFile.c_str());
      boost::archive::binary_oarchive oa(ofs);
      // write class instance to archive
      oa << (*(model.get()));
    }

    
    return 1;
  }//end of function
}//end of namespace
