#include "Utilities/Stitching/Acquisition/TranslationEstimation.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/GridMapGeneration.h"
#include <boost/bimap.hpp>
#include <complex>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <unsupported/Eigen/SparseExtra>
#include<Eigen/IterativeLinearSolvers>	

namespace spin
{ 
  namespace WLS
  {
    typedef spin::vista::pDouble 		        pDouble;
    typedef spin::vista::GridInfo		        GridInfo;
    typedef spin::vista::DirectionType      DirectionType;
    typedef spin::vista::GridMapGeneration  GridMapGeneration;
    typedef spin::vista::WLSStruct          WLSStruct;

    struct MappingLNode
    {
      int localIndex;
      int globalIndex;
      pDouble lIdx;
      pDouble gIdx;
      Complex pOffset;
      int cComponent;
    };//end of struct
  }

  /*! \brief GlobalOptimizationWLS
   *  This is the driver function to apply a weighted least-squares
   *  optimization on computed displacements. For more details on this algorithm
   *  please refer to Sec. 1.3 in
   *  http://www.maths-in-industry.org/miis/id/eprint/173/contents
   *  "Stitching IC Images"
   */
  int TranslationEstimation::GlobalOptimizationWLS( void* gObj)
  {
    typedef WLS::GridMapGeneration::gridFOV      FOV;
    typedef WLS::GridMapGeneration::nodeMapping  NMapping;
    typedef WLS::GridMapGeneration::MMapping     Mapping;
    typedef WLS::GridMapGeneration::MSTMapping   MSTMapping;
    typedef WLS::GridMapGeneration::MCost        Cost;
    typedef WLS::WLSStruct                       WLSStruct;

    WLS::GridMapGeneration* gridObj = static_cast<WLS::GridMapGeneration*>(gObj);
    FOV* tFOV                       = gridObj->GetGridMapping();
    NMapping* nmap                  = &(gridObj->GetNodeMap());
    WLS::GridInfo* gInfo            = &(gridObj->GetGridHeader());
    Mapping* mapping                = &(gridObj->GetTMapping());
    Cost* mappingCost               = &(gridObj->GetTCost());
    int tSize                       = gridObj->GetMinConnectedComponent();
    WLSStruct* wls                  = &(gridObj->GetWLSStruct());

    // In order to build the matrices, we need to keep in mind the following
    // a.) The nodes have been numbered in a column major (i.e., vertical scan
    // format). 
    // b.) As a result, the equations shown in the paper above will not match
    // 1-1. We note the following points:
    // c.) The vertical displacements correspond to the -1 1... matrix
    // d.) The horizontal displacements correspond to the other matrix.
    
    // Solve for y-coordinates (Rows)
    RMMatrix_Double ATemp(wls->HDisp_X.rows()+wls->HDisp_Y.rows(),wls->HDisp_Y.cols());
    RMMatrix_Double A1 = wls->HDisp_Y;
    RMMatrix_Double A2 = wls->HDisp_X;
    ATemp<<A1,A2;
    RMSparseMatrix_Double A = ATemp.sparseView();
    
    RMMatrix_Double B_XCoordinates(wls->HDisp_X.rows()+wls->HDisp_Y.rows(),1);
    B_XCoordinates<<wls->AcrossCols_VDisp,wls->AcrossCols_HDisp;
    RMMatrix_Double B_YCoordinates(wls->VDisp_X.rows()+wls->VDisp_Y.rows(),1);
    B_YCoordinates<<wls->AcrossRows_VDisp,wls->AcrossRows_HDisp;

    typedef Eigen::LeastSquaresConjugateGradient<RMSparseMatrix_Double> LSQRSolver;
    LSQRSolver solver;
    solver.compute(A);
    if (solver.info() != Eigen::Success) return -5;
    CMMatrix_Double XCoordinates = solver.solve(B_XCoordinates);
    if (solver.info() != Eigen::Success) return -6;
    CMMatrix_Double YCoordinates = solver.solve(B_YCoordinates);
    if (solver.info() != Eigen::Success) return -7;

#ifdef DEBUG
    std::ofstream file2a("B_XCoordinates_raw.txt");
    std::ofstream file3a("B_YCoordinates_raw.txt");
    std::ofstream file2aa("B_XCoordinates_solved.txt");
    std::ofstream file3aa("B_YCoordinates_solved.txt");
    std::ofstream file4a("Indices.txt");

    if (file2a.is_open())
    {
      file2a << B_XCoordinates;
      file2a.close();
      file3a << B_YCoordinates;
      file3a.close();
      file4a << ATemp;
      file4a.close();
      file2aa << XCoordinates;
      file3aa << YCoordinates;
      file2aa.close();
      file3aa.close();
    }
#endif

    // Reshape rge vectors to 2D matrices
    CMMatrix_Double gridYRows = Eigen::Map<CMMatrix_Double>(YCoordinates.data(),gInfo->gridHeight,gInfo->gridWidth);
    CMMatrix_Double gridXCols = Eigen::Map<CMMatrix_Double>(XCoordinates.data(),gInfo->gridHeight,gInfo->gridWidth);

#ifdef DEBUG
    std::ofstream file2("B_XCoordinates_computed1.txt");
    std::ofstream file3("B_YCoordinates_computed1.txt");
    if (file2.is_open())
    {
      file2 << gridXCols;
      file2.close();
      file3 << gridYRows;
      file3.close();
    }
#endif

    // At this time, we update node mapping to reflect new displacements
    for (int y = 0; y < gInfo->gridHeight; ++y)
    for (int x = 0; x < gInfo->gridWidth ; ++x)
    {
      // Get the reference node
      pDouble refNode = std::make_pair(y,x);
      // Get the two target nodes
      pDouble tgtNodeVertical   = std::make_pair(std::max(0,y-1),x);
      pDouble tgtNodeHorizontal = std::make_pair(y, std::max(0,x-1));
      // get the node mappings
      int refNodeMapping           = nmap->right.at(refNode);
      int tgtNodeVerticalMapping   = nmap->right.at(tgtNodeVertical);
      int tgtNodeHorizontalMapping = nmap->right.at(tgtNodeHorizontal);
      // check if the nodes are unique
      if (refNodeMapping != tgtNodeVerticalMapping)
      {
        // get the respective displacements
        Complex tgt2ref(gridXCols(y,x)-gridXCols(y-1,x), gridYRows(y,x) - gridYRows(y-1,x));
        mapping->coeffRef(refNodeMapping, tgtNodeVerticalMapping) = tgt2ref;
        Complex ref2tgt(-gridXCols(y,x)+gridXCols(y-1,x), -gridYRows(y,x) + gridYRows(y-1,x));
        mapping->coeffRef(tgtNodeVerticalMapping, refNodeMapping) = ref2tgt;
      }
      // check if the nodes are unique
      if (refNodeMapping != tgtNodeHorizontalMapping)
      {
        // get the respective displacements
        Complex tgt2ref(gridXCols(y,x)-gridXCols(y,x-1), gridYRows(y,x) - gridYRows(y,x-1));
        mapping->coeffRef(refNodeMapping, tgtNodeHorizontalMapping) = tgt2ref;
        Complex ref2tgt(-gridXCols(y,x)+gridXCols(y,x-1), -gridYRows(y,x) + gridYRows(y,x-1));
        mapping->coeffRef(tgtNodeHorizontalMapping, refNodeMapping) = ref2tgt;
      }
    }

    return 1;
  }//end of function
}//end of namespace







