#ifndef TRANSLATIONESTIMATION_H_
#define TRANSLATIONESTIMATION_H_
#include "AbstractClasses/AbstractCallback.h"
#include <utility>
#include <vector>
#include <map>
#include <complex>

namespace spin
{  
  /*! \brief class TranslationEstimation
   *  This class is a placeholder for all functions to estimate translation between
   *  two pairs of images
   */
  class TranslationEstimation
  {
    public:
      typedef  std::pair<double,double> pDouble;
      typedef  std::pair<pDouble, pDouble> pDouble2;
      typedef  std::complex<float>  Complex;

      /*! \brief Default constructor*/
      TranslationEstimation(){}
      /*! \brief Default destructor*/
      ~TranslationEstimation(){}
      
      /*! \brief An API to implement a translation pipeline given image names*/
      int ComputeDisplacement(void* idx, \
                              void* fftUtilsObj1,\
                              void* fftUtilsObj2,\
                              void* _fft2d,\
                              bool unbiasedCost,\
                              void* gObj,\
                              std::vector<double> disp1_X, \
                              std::vector<double> disp1_Y, \
                              std::vector<double> disp2_X, \
                              std::vector<double> disp2_Y,\
                              std::vector<double> def_Hor, \
                              std::vector<double> def_Ver,\
                              bool ignoreBg, bool ignoreBacklash,\
                              void* calib,\
                              bool generateLogs=false,\
                              bool calibration=false,\
                              std::vector<std::string>* calib_pd = nullptr,\
                              std::vector<std::string>* calib_sd = nullptr,\
                              std::vector<std::string>* calib_pd_meta = nullptr,\
                              std::vector<std::string>* calib_sd_meta = nullptr);
      /*! \brief An API to implement global optimization of computed displacements using MST*/
      int GlobalOptimizationMST(void* gridObj, std::string imgDir, std::string& mFile);
      
      /*! \brief An API to implement global optimization of computed displacements using SSPath*/
      int GlobalOptimizationSSP(void* gridObj, std::string imgDir, std::string& mFile);
      
      /*! \brief An API for weighted least squares global optimization */
      int GlobalOptimizationWLS(void* obj);

      /*! \brief signals for callbacks */
      spin::CallbackSignal sig_Logs;
      spin::CallbackSignal sig_Metrics;
      spin::FOVSignal      sig_FOV;
    private:
      // Free residual data
      void FreeResidualData(void* gObj);
      // Variables
      std::pair<int,int> mModDimensions;
  };//end of class
}
#endif
