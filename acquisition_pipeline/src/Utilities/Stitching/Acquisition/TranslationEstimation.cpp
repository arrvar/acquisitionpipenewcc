#include "AbstractClasses/AbstractDataStructures.h"
#include "Libraries/vista/Pipeline/Acquisition/Initialize/GridMapGeneration.h"
#include "Utilities/Stitching/Acquisition/TranslationEstimation.h"
#include "Utilities/ImageProcessing/FFTUtils/FFTTranslation.h"
#include "Utilities/Geometry/PolygonUtils.h"
#include <complex>
namespace spin
{
  namespace
  {
    typedef RMMatrix_Complex ComplexMatrix;
    typedef FFTTranslation<float> FFTUtils;
    typedef spin::vista::pDouble pDouble;
    typedef spin::vista::GridInfo GridInfo;
    typedef spin::vista::DirectionType DirectionType;
    typedef spin::vista::GridMapGeneration GridMapGeneration;
    typedef spin::vista::WLSStruct WLSStruct;
    typedef GridMapGeneration::gridFOV FOV;
    typedef GridMapGeneration::nodeMapping NMapping;
    typedef GridMapGeneration::MMapping Mapping;
    typedef GridMapGeneration::MCost Cost;
    typedef GridMapGeneration::MSTMapping MSTMapping;
  }

  /*! \brief PopulateMappingMatrix
   *  This function populates a sparse matrix that holds information about
   *  displacements between neighboring AOI's
   */
  void PopulateMappingMatrix( int gridHeight, int gridWidth, \
                              void* ref, void* tgt, void* disp,
                              void* nmapping, void* mmapping, \
                              void* cmapping, void* mstmapping,\
                              void* _wlsStruct, \
                              float tCost, bool unbiasedCost)
  {
    // typecast all data
    typedef boost::bimap< int, std::pair<double, double> > nodemapping;
    pDouble* gridRef                    = static_cast<pDouble*>(ref);
    pDouble* gridTgt                    = static_cast<pDouble*>(tgt);
    pDouble* tDisp                      = static_cast<pDouble*>(disp);
    nodemapping* nmap                   = static_cast<nodemapping*>(nmapping);
    RMSparseMatrix_Complex* vMapping    = static_cast<RMSparseMatrix_Complex*>(mmapping);
    RMSparseMatrix_Float*   cMapping    = static_cast<RMSparseMatrix_Float*>(cmapping);
    RMSparseMatrix_Int* mstMapping      = static_cast<RMSparseMatrix_Int*>(mstmapping);   
    WLSStruct* wlsStruct                = static_cast<WLSStruct*>(_wlsStruct);

    // Next, we get the 1D indices using nmap
    int refIdx = nmap->right.at(*gridRef);
    int tgtIdx = nmap->right.at(*gridTgt);

    /////////////////////////////////////////////////////////////////////////////
    // Here, we populate all data structures pertaining to Minimum spanning tree
    /////////////////////////////////////////////////////////////////////////////
    // Create two complex numbers for the displacement,
    // i.e., one vector corresponds to refIdx->tgtIdx and the other
    // to tgtIdx->refIdx
    Complex ref2tgt(tDisp->second, tDisp->first);
    Complex tgt2ref(-tDisp->second, -tDisp->first);
    // Finally, we populate the two indices in the sparse matrix with these values
    vMapping->insert(refIdx, tgtIdx) = tgt2ref;//ref2tgt;
    vMapping->insert(tgtIdx, refIdx) = ref2tgt;//tgt2ref;
    cMapping->insert(refIdx, tgtIdx) = tCost;
    cMapping->insert(tgtIdx, refIdx) = tCost;
    // And the respective nodes for the MST mapping
    mstMapping->insert(refIdx, tgtIdx) = 1.0;
    
    /////////////////////////////////////////////////////////////////////////////
    // Here, we populate all data structures pertaining to weighted least squares
    // bundle adjustment. 
    /////////////////////////////////////////////////////////////////////////////
    pDouble pEdge = std::make_pair(refIdx, tgtIdx);    
    if (gridRef->first == gridTgt->first)
    {
      // Check if this edge exists in list of horizontal edges or vertical edges
      std::map<pDouble, int>:: iterator ht = wlsStruct->HEdges.find(pEdge);  
      if ( ht != wlsStruct->HEdges.end())
      {
        // This edge is present in HEdges. So we extract the following:
        // a.) edgeCOunt
        int eCount = ht->second;
        double eCost = static_cast<double>(tCost);
        if (unbiasedCost) // This is primarily used for hematology (PBS)
        if (eCost != 0.01) eCost = 1.0;

        // b.) And set the two respective nodes
        wlsStruct->HDisp_X.coeffRef(eCount, ht->first.first)  =  eCost;
        wlsStruct->HDisp_X.coeffRef(eCount, ht->first.second) = -eCost;
        wlsStruct->VDisp_X.coeffRef(eCount, ht->first.first)  =  eCost;
        wlsStruct->VDisp_X.coeffRef(eCount, ht->first.second) = -eCost;
        // c.) Set the displacement value in the appropriate location
        wlsStruct->AcrossCols_HDisp(eCount,0) = -tDisp->second * eCost;//primary direction
        wlsStruct->AcrossRows_HDisp(eCount,0) = -tDisp->first* eCost;//secondry direction
      }
    }
    
    if (gridRef->second == gridTgt->second)
    {
      std::map<pDouble, int>::iterator vt = wlsStruct->VEdges.find(pEdge);  
      // Check if this edge exists in list of vertical edges
      if ( vt != wlsStruct->VEdges.end())
      {
        // This edge is present in VEdges. So we extract the following:
        // a.) edgeCOunt
        int eCount = vt->second;
        double eCost = static_cast<double>(tCost);
        if (unbiasedCost) // This is primarily used for hematology (PBS)
        if (eCost != 0.01) eCost = 1.0;
        ////std::cout<<"Placing Vertical: "<<vt->first.first<<" "<<vt->first.second<<": "<<eCount<<" "<<tDisp->first<<" "<<tDisp->second<<std::endl;
        // b.) And set the two respective nodes
        wlsStruct->HDisp_Y.coeffRef(eCount, vt->first.first)  =  eCost;
        wlsStruct->HDisp_Y.coeffRef(eCount, vt->first.second) = -eCost;
        wlsStruct->VDisp_Y.coeffRef(eCount, vt->first.first)  =  eCost;
        wlsStruct->VDisp_Y.coeffRef(eCount, vt->first.second) = -eCost;
        // c.) Set the displacement value in the appropriate location
        wlsStruct->AcrossCols_VDisp(eCount,0) = -tDisp->second* eCost;//secondary direction
        wlsStruct->AcrossRows_VDisp(eCount,0) = -tDisp->first* eCost;//primary direction
      }
    }
  }//end of function

  /*! \brief DisplacementPipeline
   *  This function is the actual driver to compute the displacements between
   *  neighboring images
   */
  void DisplacementPipeline(FOV* tFOV,\
                            pDouble* gridToBeRegistered,\
                            pDouble* gridStatic,\
                            std::pair<int,int>* mModDimensions,\
                            int subSampFact,
                            void* fft2d,
                            FFTTranslation<float>* fftUtils,
                            std::vector<double>& disp_Y,\
                            std::vector<double>& disp_X,\
                            std::vector<double>& default_value,\
                            double* cost, pDouble* gshift, int tP,
                            bool ignoreBg, bool ignoreBacklash,\
                            PolygonUtils* polygon, int scanDir,\
                            bool calibration,\
                            std::vector<std::string>* calib_disp,\
                            std::vector<std::string>* calib_disp_meta
                           )
  {
    // Check if both AOI's have been deemed as foreground
    bool checkFG = true;
    if (!ignoreBg)
    {
      checkFG = (*tFOV)[*gridToBeRegistered].isBackground==false && \
                (*tFOV)[*gridStatic].isBackground==false;
    }
    
    // default cost is a small value
    *cost = 0;
    
    if (checkFG)
    {
      // Compute the raw displacement
      TShift tPair;
      bool check1 = (gridStatic->first == gridToBeRegistered->first) && (gridStatic->second > gridToBeRegistered->second);
      bool check2 = (gridStatic->first > gridToBeRegistered->first) && (gridStatic->second == gridToBeRegistered->second);
      if ( (check1==true) || (check2==true) )
      {
        fftUtils->ComputeTranslationShift( &((*tFOV)[*gridStatic].fft), \
                                           &((*tFOV)[*gridToBeRegistered].fft),\
                                           fft2d, &tPair);
        // Place the displacements in the respective vector
        *gshift = std::make_pair(tPair.wXShift, tPair.hYShift);
        // Compute the corrected displacement
        fftUtils->CorrectOffset( gridToBeRegistered, gridStatic, gshift, mModDimensions, subSampFact);
      }
      else
      {
        fftUtils->ComputeTranslationShift( &((*tFOV)[*gridToBeRegistered].fft), \
                                           &((*tFOV)[*gridStatic].fft),\
                                           fft2d, &tPair);
        // Place the displacements in the respective vector
        *gshift = std::make_pair(tPair.wXShift, tPair.hYShift);
        // Compute the corrected displacement
        fftUtils->CorrectOffset( gridStatic, gridToBeRegistered, gshift, mModDimensions, subSampFact);
      }
	    // The base cost
	    *cost = tPair.cost;
      
      // Do the following as we need to accumulate logs of displacement
      // estimation
      if (calibration)
      {
        std::string c = std::to_string(gshift->first)+"\t"+std::to_string(gshift->second);
        calib_disp->push_back(c);
        c = (*tFOV)[*gridToBeRegistered].aoiName+"\t"+(*tFOV)[*gridStatic].aoiName+"\t";
        c += std::to_string((*tFOV)[*gridToBeRegistered].bgRatio)+"\t"+std::to_string((*tFOV)[*gridStatic].bgRatio);
        calib_disp_meta->push_back(c);
      }
      else
      {
        // At this time, check if the displacement is valid
        bool check = false;
        polygon->CheckPointInsidePolygon(scanDir,gshift, &check);
        if (check == false)
        {
          // We have received a displacement that does not lie within the
          // safe operating zone.
          *cost = 0;
        }
      }
    }
    else
    {
      *cost = 0;
    }
    
    // At this point, check if the cost is 0. If so, this means that we have an
    // unviable displacement vector and need to replace it with a failsafe.
    // Avoid this step if we are doing only calibration.
    if (!calibration)
    {
      if (*cost == 0)
      {
        RMMatrix_Float f;
        polygon->GetCentroid(scanDir, &f);
        //polygon->GetClosestPoint(scanDir, &f);
        gshift->first   = (double)(f(0,0));// x-coordinate
        gshift->second  = (double)(f(0,1));// y-coordinate
        *cost = 0.01;//a small value
      }
    }
    //std::cout<<"Image Pair being registered: "<<gridStatic->first<<","<<gridStatic->second<<"<-----"<<gridToBeRegistered->first<<","<<gridToBeRegistered->second<<" =  "<<gshift->first<<" "<<gshift->second<<std::endl;
  }//end of function

  /*! \brief FreeResidualData
   *  At the end of the scan process, there are a few remnants of FFT data present in
   *  the grid. This function is used to clear them
   */
  void TranslationEstimation::FreeResidualData(void* gObj)
  {
    typedef GridMapGeneration::gridFOV 		FOV;
    // 1.) Downcast the data
    GridMapGeneration* gridObj    = static_cast<GridMapGeneration*>(gObj);
    FOV* tFOV                     = gridObj->GetGridMapping();

    // 2.) Free FFT data if not already done
    std::vector< spin::vista::pDouble >* dIdx = gridObj->GetGridIndices();
    for(auto it: (*dIdx))
    {
      (*tFOV)[it].fft.resize(0, 0);
    }
  }//end of function

  /*! \brief ComputeDisplacement
   *  This is the driver function to compute the translation between two FOV's
   *  when traversing over a grid. Prior to calling this function, =
   *  it is assumed that grid co-ordinates, as well as image names have been populated.
   *  @idx          : The index in the grid of the reference aoi
   *  @fftUtilsObj1 : The FFT of the reference aoi
   *  @fftUtilsObj2 : The FFT of the target aoi
   *  @fft2d        : The FFT object
   *  @gObj         : The grid map object
   *  @biasSecondary: A bias term associated with secondary AOI's
   *  @generateLogs : If logs need to be genarated.
   *  The translation is estimated from the two FFT's
   */
  int TranslationEstimation::ComputeDisplacement( void* idx, \
                                                  void* fftUtilsObj1,\
                                                  void* fftUtilsObj2,\
                                                  void* fft2d,\
                                                  bool unbiasedCost,\
                                                  void* gObj,\
                                                  std::vector<double> disp1_X, \
                                                  std::vector<double> disp1_Y, \
                                                  std::vector<double> disp2_X, \
                                                  std::vector<double> disp2_Y,\
                                                  std::vector<double> default_horizontal,\
                                                  std::vector<double> default_vertical,\
                                                  bool ignoreBg, \
                                                  bool ignoreBacklash,\
                                                  void* calib,\
                                                  bool generateLogs,\
                                                  bool calibration,\
                                                  std::vector<std::string>* calib_pd,\
                                                  std::vector<std::string>* calib_sd,\
                                                  std::vector<std::string>* calib_pd_meta,\
                                                  std::vector<std::string>* calib_sd_meta
                                                )
  {
    // 1.) Downcast the index and fft utils object and assign other data
    pDouble* gridRef            = static_cast<pDouble* >(idx);
    FFTUtils* fftUtils1         = static_cast<FFTUtils* >(fftUtilsObj1);
    FFTUtils* fftUtils2         = static_cast<FFTUtils* >(fftUtilsObj2);
    GridMapGeneration* gridObj  = static_cast<GridMapGeneration*>(gObj);
    FOV* tFOV                   = gridObj->GetGridMapping();
    Mapping* mapping            = &(gridObj->GetTMapping());
    Cost* mappingCost           = &(gridObj->GetTCost());
    MSTMapping* mstmapping      = &(gridObj->GetMSTMapping());
    NMapping* nmap              = &(gridObj->GetNodeMap());
    graph_t* g                  = &(gridObj->GetGraph());
    edgeWeight_t* ew            = &(gridObj->GetWeightMap());
    GridInfo* gInfo             = &(gridObj->GetGridHeader());
    int subSampFact             = (int)gridObj->GetSubSampFactor();
    WLSStruct* wls              = &(gridObj->GetWLSStruct());
    PolygonUtils* polygon       = static_cast<PolygonUtils*>(calib);

    // Instantiate a callback struct
    CallbackStruct cLogs;
    // 2.) Enable logging if requested
    if (generateLogs)
    {
      cLogs.Module = "File: DisplacementEstimation.cpp";
    }

    // 3.) Now, check if the neighbors are empty
    if ((*tFOV)[*gridRef].tNeighbors.size() == 0)
    {
      // This is the first image that has arrived. We return to the calling function
      return 1;
    }
    else
    {
      // Do some sanity checks
      if (!(*tFOV)[*gridRef].fft.data()) return -1;
      // 3a.) Get a pointer to the indices of the neighbors
      std::vector< pDouble >* pVec = &((*tFOV)[*gridRef].tNeighbors);
      std::pair<int,int> mModDimensions = \
      std::make_pair((*tFOV)[*gridRef].fft.rows()*subSampFact,(*tFOV)[*gridRef].fft.cols()*subSampFact);
      std::vector< double > tCost(pVec->size());
      std::vector< pDouble> gShift(pVec->size());

      // 3b.) We need to compute the displacements for all pairs. At most there will be
      // 2 images, while at least there will be 1 image.
      if (pVec->size() == 1)
      {
        if (!(*tFOV)[(*pVec)[0]].fft.data()) return -2;
        // There is only a single neighbor. We check if this is between cols or between rows.
        if (((*pVec)[0].second == gridRef->second) && ((*pVec)[0].first != gridRef->first))
        {
          if (gInfo->dType == spin::vista::DirectionType::HEIGHT)
          {
            // Run a single displacement estimation
            DisplacementPipeline( tFOV, &((*pVec)[0]), gridRef, \
                                  &mModDimensions,subSampFact,fft2d,fftUtils1,\
                                  disp1_Y, disp1_X, default_vertical,\
                                  &tCost[0], &gShift[0],0,\
                                  ignoreBg, ignoreBacklash,\
                                  polygon,(int)PolygonSOZ::ACROSS_ROWS,\
                                  calibration, calib_pd, calib_pd_meta);
          }
          else
          {
            // Run a single displacement estimation
            DisplacementPipeline( tFOV, &((*pVec)[0]), gridRef, \
                                  &mModDimensions,subSampFact,fft2d,fftUtils1,\
                                  disp1_Y, disp1_X, default_vertical,\
                                  &tCost[0], &gShift[0],0,\
                                  ignoreBg, ignoreBacklash,\
                                  polygon,(int)PolygonSOZ::ACROSS_ROWS,\
                                  calibration, calib_sd, calib_sd_meta);

          }
        }
        else
        if (((*pVec)[0].first == gridRef->first) && ((*pVec)[0].second != gridRef->second))
        {
          if (gInfo->dType == spin::vista::DirectionType::WIDTH)
          {
            // Run a single displacement estimation
            DisplacementPipeline( tFOV, &((*pVec)[0]), gridRef, \
                                  &mModDimensions,subSampFact,fft2d,fftUtils1,\
                                  disp2_Y, disp2_X, default_horizontal,\
                                  &tCost[0], &gShift[0], 1,\
                                  ignoreBg, ignoreBacklash, \
                                  polygon,(int)PolygonSOZ::ACROSS_COLS,\
                                  calibration, calib_pd, calib_pd_meta);
          }
          else
          {
            DisplacementPipeline( tFOV, &((*pVec)[0]), gridRef, \
                                  &mModDimensions,subSampFact,fft2d,fftUtils1,\
                                  disp2_Y, disp2_X, default_horizontal,\
                                  &tCost[0], &gShift[0], 1,\
                                  ignoreBg, ignoreBacklash, \
                                  polygon,(int)PolygonSOZ::ACROSS_COLS,\
                                  calibration, calib_sd, calib_sd_meta);

          }
        }
      }
      else
      if (pVec->size() == 2)
      {
        if (!(*tFOV)[(*pVec)[0]].fft.data()) return -2;
        if (!(*tFOV)[(*pVec)[1]].fft.data()) return -3;
        if (gInfo->dType == spin::vista::DirectionType::HEIGHT)
        {
          // Run a displacement estimation
          DisplacementPipeline( tFOV, &((*pVec)[0]), gridRef, \
                                &mModDimensions,subSampFact,fft2d,fftUtils1,\
                                disp1_Y, disp1_X, default_vertical,\
                                &tCost[0], &gShift[0],0,\
                                ignoreBg, ignoreBacklash,\
                                polygon,(int)PolygonSOZ::ACROSS_ROWS,\
                                calibration, calib_pd, calib_pd_meta);
          // Run a displacement estimation
          DisplacementPipeline( tFOV, &((*pVec)[1]), gridRef, \
                                &mModDimensions,subSampFact,fft2d,fftUtils2,\
                                disp2_Y, disp2_X,default_horizontal,\
                                &tCost[1], &gShift[1],1,\
                                ignoreBg, ignoreBacklash,\
                                polygon,(int)PolygonSOZ::ACROSS_COLS,\
                                calibration, calib_sd, calib_sd_meta);
        }
        else
        if (gInfo->dType == spin::vista::DirectionType::WIDTH)
        {
          // Run a displacement estimation
          DisplacementPipeline( tFOV, &((*pVec)[0]), gridRef, \
                                &mModDimensions,subSampFact,fft2d,fftUtils1,\
                                disp2_Y, disp2_X, default_horizontal,\
                                &tCost[0], &gShift[0],1,\
                                ignoreBg, ignoreBacklash,\
                                polygon,(int)PolygonSOZ::ACROSS_COLS,\
                                calibration, calib_pd, calib_pd_meta);
          // Run a displacement estimation
          DisplacementPipeline( tFOV, &((*pVec)[1]), gridRef, \
                                &mModDimensions,subSampFact,fft2d,fftUtils2,\
                                disp1_Y, disp1_X, default_vertical,\
                                &tCost[1], &gShift[1],0,\
                                ignoreBg, ignoreBacklash,\
                                polygon,(int)PolygonSOZ::ACROSS_ROWS,\
                                calibration, calib_sd, calib_sd_meta);

        }
      }

      // 4.) Having finished computing the displacements, we now build the edge map
      //     for MST computation. This is done in a serial fashion
      for (int j = 0; j < pVec->size(); ++j)
      {
        // Check if displacement was deemed valid.
        if (1)//tCost[j] > 0)
        {
          bool check1 = (gridRef->first == (*pVec)[j].first) && (gridRef->second > (*pVec)[j].second);
          bool check2 = (gridRef->first > (*pVec)[j].first) && (gridRef->second == (*pVec)[j].second);
          if ( (check1==true) || (check2==true) )
          {
            // Populate the mapping matrix
            PopulateMappingMatrix(gInfo->gridHeight, gInfo->gridWidth,\
                                  gridRef, &((*pVec)[j]), &gShift[j],\
                                  nmap, mapping, mappingCost, mstmapping, wls,
                                  tCost[j], unbiasedCost);
          }
          else
          {
            // Populate the mapping matrix
            PopulateMappingMatrix(gInfo->gridHeight, gInfo->gridWidth,\
                                  &((*pVec)[j]), gridRef, &gShift[j],\
                                  nmap, mapping, mappingCost, mstmapping, wls,
                                  tCost[j], unbiasedCost);
          }
          // Save this cost
          (*tFOV)[*gridRef].tMetrics[(*pVec)[j]].displacement_fftcost = tCost[j];
        }
      }//finished building edges for all nodes
    }//finished checking displacement

    // A dumb way of removing redundant data
    if (gInfo->dType == spin::vista::DirectionType::WIDTH)
    {
      int h1 = (int)gridRef->first;
      if ((h1-2) > 0)
      {
        for (int q = 0; q < gInfo->gridWidth; ++q)
        {
          (*tFOV)[std::make_pair((h1-2), q)].fft.resize(0, 0);
        }
      }
    }
    else
    if (gInfo->dType == spin::vista::DirectionType::HEIGHT)
    {
      int h1 = (int)gridRef->second;
      if ((h1-2) > 0)
      {
        for (int q = 0; q < gInfo->gridHeight; ++q)
        {
          (*tFOV)[std::make_pair(q,(h1-2))].fft.resize(0, 0);
        }
      }
    }    

    return 1;
  }//end of function
}//end of namespace
