#include "Utilities/ConvexHull/ConvexHullUtils.h"
#include "Utilities/ConvexHull/ConvexHullStruct.h"
#include "Utilities/MatrixUtils/EigenUtils.h"
#include "Qhull.h"
#include "QhullQh.h"
#include "QhullFacetList.h"
#include "QhullVertexSet.h"
#include "PointCoordinates.h"
#include <vector>
#include <map>

namespace spin
{
  /*! \brief Default constructor */
  template <class T>
  ConvexHullUtils<T>::ConvexHullUtils()
  {
    // Instantiate a QHull object
    //qhullObj = std::make_shared<orgQhull::Qhull>();
  }//end of namespace

  /*! \brief Default destructor */
  template <class T>
  ConvexHullUtils<T>::~ConvexHullUtils()
  {
  }//end of namespace

  /*! \brief RemainingData
   *  This function is used to check if the data that remains after generating
   *  a convex hull layer satisfies the percent remaining creiterion.
   */
  template <class T>
  bool RemainingData( T* input, \
                      RMMatrix_Double* hull, \
                      RMMatrix_Double* indicator, \
                      double actPoints, \
                      double percent, T* remainingPoints)
  {
    typedef typename T::Scalar Scalar;
    RMMatrix_Double inputClone = input->template cast<double>();

    // Allocate memory for remainingPoints
    *remainingPoints = T::Zero(inputClone.rows() - hull->rows(), inputClone.cols());
    auto count = 0;
    for (int idx = 0; idx < indicator->rows(); ++idx)
    {
      if ((*indicator)(idx,0) ==1)
      {
        remainingPoints->row(count) = inputClone.row(idx).template cast<Scalar>();
        // increment count
        ++count;
      }
    }
    //
    // At this point we check if the # of points in remaining points satisfies
    // the percent requested by the user.
    double k = remainingPoints->rows()/actPoints;
    if (k < percent) return true;
    // The percentage is not satisfied
    return false;
  }//end of function

  /*! \brief ComputeConvexHull
   *  This function is used to compute a convex hull given a set of points
   *  @input    : The input points stored as rows of an eigen matrix
   *  @_output  : The convex hull indicated by an Eigen matrix
   */
  template <class T>
  int ConvexHullUtils<T>::ComputeConvexHull(T* input, void* _output, void* _indicator)
  {
    // First check for data sanity
    if (!input->data()) return -1;

    // Next, we choose to build convex hulls for points having
    // dimensions 2,3 or 4
    if (input->cols() <2 || input->cols() >4) return -2;

    // Typecast the output to a vector of points
    QhullOutput* output = static_cast<QhullOutput*>(_output);

    // Make a copy of the data.
    RMMatrix_Double p = input->template cast<double>();

    // And create an indicator matrix
    RMMatrix_Double* indicator = static_cast<RMMatrix_Double*>(_indicator);
    *indicator = RMMatrix_Double::Ones(p.rows(),1);

    // Finally, run the convex hull pipeline
    char flags[64] = "qhull Qt Qx";
    try
    {
      std::shared_ptr<orgQhull::Qhull> qhullObj = std::make_shared<orgQhull::Qhull>();
      // Run the convex hull pipeline
      qhullObj->runQhull("",p.cols(),p.rows(),p.data(),flags);
      // Save the volume of this convex hull
      output->volume = qhullObj->volume();

      // Traverse through all the facets and extract the QHull coordinates
      for ( orgQhull::QhullFacetList::iterator it = qhullObj->facetList().begin(); \
            it != qhullObj->facetList().end(); ++it)
      {
        if ((it->isGood()))
        {
          for ( orgQhull::QhullVertexSet::iterator vIt = it->vertices().begin(); \
                vIt != it->vertices().end(); ++vIt)
          {
            // extract the coordinates of this point
            orgQhull::QhullVertex v = *vIt;
            orgQhull::QhullPoint pt = (*vIt).point();

            // Get the index of this point
            countT idx = (*vIt).point().id();
            // Was it previously visited?
            if ( (*indicator)((int)idx, 0) == 1.0)
            {
              // It wasnt visited. We map the data to an eigen matrix
              RMMatrix_Double dummy = Eigen::Map<RMMatrix_Double>((*vIt).point().coordinates(),1,p.cols());
              // Now, we populate the hull
              if (!output->hull.data())
              {
                output->hull = dummy;
              }
              else
              {
                // Append the point to the output data
                RMMatrix_Double temp = output->hull;
                output->hull = RMMatrix_Double::Zero(temp.rows()+1,p.cols());
                (output->hull)<<temp,dummy;
              }
              // And we mark this index as visited so that we do not add
              // it again to the hull
              (*indicator)((int)idx,0) = 0;
            }
          }
        }
      }
    }
    catch(...)
    {
      return -3;
    }

    return 1;
  }//end of function

  /*! \brief ComputeConvexHullLayers
   *  This function is used to compute a set of convex hull layers
   *  given a set of points. As this is an iterative process, we terminate
   *  the process when K% of points remain.
   *  @input    : The input points stored as rows of an eigen matrix
   *  @percent  : The percent of points that must remain for iterations to stop
   *  @_output  : The convex hull layers
   */
  template <class T>
  int ConvexHullUtils<T>::ComputeConvexHullLayers(T* input, void* _output, \
                                                  int iterations, double percent)
  {
    // First check for data sanity
    if (!input->data()) return -1;

    // Next, we choose to build convex hulls for points having
    // dimensions 2,3 or 4
    if (input->cols() <2 || input->cols() >4) return -2;

    // Typecast the output
    std::map<int, QhullOutput>* output = static_cast<std::map<int,QhullOutput>*>(_output);
    if (!output) return -3;

    // Make a copy of the input
    T inputClone = *input;
    // Instantiate remaining points
    T remainingPoints;

    // Continue the iterations until percent is satisfied
    double actPoints = input->rows();
    auto count = 0;
    while(1)
    {
      // Compute a convex hull layer
      QhullOutput layer;
      RMMatrix_Double indicator;
      int v = ComputeConvexHull(&inputClone, &layer, &indicator);
      if (v !=1) return -4;
      // Place the result in output
      (*output)[count] = layer;
      // Check if we need to stop the layer generation because the # of points satisfies the
      // remaining points criterion
      bool check = RemainingData<T>(&inputClone, &layer.hull, &indicator, actPoints, percent, &inputClone);

      indicator.resize(0,0);
      // If the condition is satisfied, or the count reaches the maximum # of iterations,
      // we break from the loop
      if (check || (count >= iterations)) break;
      ++count;
    }

    return 1;
  }//end of function

  // explicit templates
  template class ConvexHullUtils<RMMatrix_Float>;
  template class ConvexHullUtils<RMMatrix_Double>;
}//end of namespace
