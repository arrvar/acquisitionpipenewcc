#ifndef CONVEXHULLSTRUCT_H_
#define CONVEXHULLSTRUCT_H_

#include "AbstractClasses/AbstractMatrixTypes.h"
#include <map>

namespace spin
{
  /*! \brief A structure to hold information about a convex hull
   *  and its associated volume
   */
  struct QhullOutput
  {
    // The volume
    double volume;
    // The convex hull coordinates in a RMMatrix
    RMMatrix_Double hull;
  };//end of struct
  
  // A map containing information about various convex hull layers
  typedef std::map<int, QhullOutput> QhullLayer;
}//end of namespace
#endif
