#ifndef CONVEXHULLUTILS_H
#define CONVEXHULLUTILS_H
#include <memory>

namespace orgQhull
{
  class Qhull;
}//end of namespace

namespace spin
{
  /*! \brief This class is a placeholder for all utilities that
   *  pertain to use of a convex hull
   */
  template <class T>
  class ConvexHullUtils
  {
    public:
      // default constructor
      ConvexHullUtils();
      // default destructor
      ~ConvexHullUtils();
      // Compute convex hull
      int ComputeConvexHull(T* input, void* _output, void* _indicator);
      // Compute convex hull layers
      int ComputeConvexHullLayers(T* input, void* _output, int iterations = 200, double percent = 0.01);
    private:
      //std::shared_ptr<orgQhull::Qhull> qhullObj;
  };//end of class
}//end of namespace
#endif
