#ifndef HISTOGRAMUTILS_H_
#define HISTOGRAMUTILS_H_
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  /*! \brief HistogramUtils 
  *  This class is a placeholder for image processing utilities that can be used 
  *  across the board and using histograms of images.
  */
  class HistogramUtils
  {
    public:
      HistogramUtils(int histBins);
      ~HistogramUtils();
      
      // Signals for metrics and logging
      spin::CallbackSignal sig_Logs;
      
      /*! \brief API to compute CDF of magnitude of a 2D complex image*/
      void ComputeCDFOfFFTMagnitude(void* fft, void* out, void* mask, bool computeCDF=true);
      /*! \brief API to check if image is foregroud or background*/
      int  BackgroundDetection( void* _tgtFFT, \
                                void* _refHistFG,\
                                void* _refHistBG,\
                                void* _refFocus,\
                                void* _maskFgBg,\
                                void* _maskFocus,\
                                float threshFgBg,\
                                float threshFocus,\
                                float* ratio1,\
                                float* ratio2,\
                                bool generateLogs);   
      /*! \brief API to check if image is foreground or background */
      int BackgroundDetection(  void* _tgtFFT, \
                                void* _refHistFG,\
                                void* _refHistBG,\
                                void* _maskFgBg,\
                                float threshFgBg,\
                                float* ratioFgBg,\
                                bool generateLogs);
    private:
      RMMatrix_Float tView;
  };//end of class
}
#endif
