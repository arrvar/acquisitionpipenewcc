#include "Utilities/ImageProcessing/HistogramUtils/HistogramUtils.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <cmath>
#include <iostream>
#ifndef BINSIZE
#define BINSIZE 256
#endif

namespace spin
{
  /*! \brief roundD
  *  This function rounds a number to a nearest integer
  */
  float scale255(float x)
  {
    if (x < 0) return 0;
    if (x >=255) return 255;
    x = floor(x*255.0+0.5);
    return x;
  }//end of function

  /*! \brief CheckForFocus
   *  This function computes the ratio of Shannon's entropies between a reference
   *  pdf and a target pdf.
   */
  float CheckForFocus(RMMatrix_Float* tgt, void* _ref)
  {
    RMMatrix_Float* ref = static_cast<RMMatrix_Float*>(_ref);
    float refEntropy = 0;
    float tgtEntropy = 0;

    for (int j = 0; j < ref->cols(); ++j)
    {
      if ((*tgt)(0,j) > 0)
      {
        float g = (*tgt)(0,j);
        tgtEntropy -= g * log(g);
      }
      if ((*ref)(0,j) > 0)
      {
        float g = (*ref)(0,j);
        refEntropy -= g * log(g);
      }
    }
    float ratio = 0;
    if (refEntropy > 0)
    {
      ratio = tgtEntropy/refEntropy;
    }

    return ratio;
  }//end of function

  /*! \brief Constructor
   *  This is the default constructor
   */
  HistogramUtils::HistogramUtils(int histBins)
  {
    tView = RMMatrix_Float::Ones(histBins,histBins).triangularView<Eigen::Upper>();
  }//end of constructor

  /*! \brief Destructor
   *  This is the default destructor
   */
  HistogramUtils::~HistogramUtils()
  {
    tView.resize(0,0);
  }//end of constructor

  /*! \brief ComputeCDFOfFFTMagnitude
   *  This function computes a non-regularized probability distribution function of the magnitude of
   *  a 2D FFT. If computeCDF is enabled, then the cumulative distribution function is computed.
   *  @fft        : The reference FFT that is passed as an input
   *  @out1       : The output PDF/CDF
   *  @mask       : A mask that will constrain building the histogram from specified regions
   *                within the mask.
   *  @computeCDF : Whether a CDF needs to be computed
   */
  void HistogramUtils::ComputeCDFOfFFTMagnitude(void* fft, void* out1, \
                                                void* _mask, bool computeCDF)
  {
    // typecast the data
    RMMatrix_Float* tgt1 = static_cast<RMMatrix_Float*>(out1);
    RMMatrix_Float* mask = static_cast<RMMatrix_Float*>(_mask);
    RMMatrix_Complex* ref = static_cast<RMMatrix_Complex*>(fft);

    // Compute the magnitude of the FFT Matrix
    RMMatrix_Float fftMag = ref->array().abs();
    // Compute the dynamic range of fftMag
    float mMax = fftMag.maxCoeff();
    float mMin = fftMag.minCoeff();
    // We quantize the data such that it lies between 0-255
    fftMag = ((fftMag.array() - mMin) / (mMax - mMin)).unaryExpr(std::ptr_fun(scale255));
    // And traverse through the array to build the histogram
    // ToDo: Can parallelize this operation using coarse-grained parallelism
    long sum = fftMag.rows() * fftMag.cols();

    (*tgt1) = RMMatrix_Float::Zero(1, tView.rows());
    float* p = fftMag.data();
    float* m = mask->data();
    long count = 0;
    for (long h = 0; h < sum; ++h)
    {
      int j = (int)p[h];
      if (m[h] > 0 && j >=0 && j < 256)
      {
        ++(*tgt1)(0, j);
        ++count;
      }
    }

    // convert the histogram to an non-regularized pdf
    // ToDo: We can regularize this pdf using a kernel (e.g., Nadaraya-Watson estimator)
    (*tgt1) = tgt1->array() / count;

    // Check if a cdf needs to be computed
    if (computeCDF)
    {
      (*tgt1) = (*tgt1) *tView;
    }

    fftMag.resize(0,0);
  }//end of function

  /*! \brief BackgroundDetection
   *  This function is a driver to detect whether an unseen AOI is "closer" to a templated background
   *  or a foreground AOI. An FFT of the AOI is computed followed by a histogram of the magnitude
   *  of the FFT values. A distance metric is computed between this histogram and two previously
   *  computed histograms of a templated background and foreground image. A call is made based on the
   *  closeness of this histogram w.r.t either of the two histograms. We employ the E.M.D to compute
   *  this closeness. In addition, this function will also check with a second histogram and
   *  determine the level of focus the incoming AOI has w.r.t to the reference histogram.
   *  @_tgtFFT      : The FFT of the target AOI
   *  @_refHistFG   : CDF corresponding to the FFT Magnitude of the foreground template
   *  @_refHistBG   : CDF corresponding to the FFT Magnitude of the background template
   *  @_refFocus    : PDF corresponding to the FFT Magnitude of a best focused template image
   *  @_maskFgBg    : The mask within which a Fg/Bg metric will be computed
   *  @_maskFocus   : The mask within which a Focus metric will be computed
   *  @threshFgBg   : Threshold for determining if the aoi is a Fg or Bg
   *  @threshFocus  : Threshold for determining if the aoi is in/out of focus
   *  @ratioFgBg    : The ratio when compared with a pure bg image
   *  @ratioFocus   : When compared with a well focused image
   *  At the end of this process, a return value of 1 indicates a background, while a value of 0
   *  indicates a foreground.
   */
  int HistogramUtils::BackgroundDetection(void* _tgtFFT, \
                                          void* _refHistFG,\
                                          void* _refHistBG,\
                                          void* _refFocus,\
                                          void* _maskFgBg,\
                                          void* _maskFocus,\
                                          float threshFgBg,\
                                          float threshFocus,\
                                          float* ratioFgBg,\
                                          float* ratioFocus,\
                                          bool generateLogs)
  {
    typedef RMMatrix_Complex ComplexMatrix;
    // Instantiate callback structures
    CallbackStruct cLogs;
    cLogs.Module    = "File:=HistogramUtils.cpp; Function:=BackgroundDetection;";

    // typecast the objects
    ComplexMatrix* tgtFFT = static_cast<ComplexMatrix*>(_tgtFFT);
    RMMatrix_Float* refCDF_FG = static_cast<RMMatrix_Float*>(_refHistFG);
    RMMatrix_Float* refCDF_BG = static_cast<RMMatrix_Float*>(_refHistBG);

    // Now, we compute the histogram of the Magnitude of the FFT
    RMMatrix_Float tgtCDF, tgtPDF;
    ComputeCDFOfFFTMagnitude(tgtFFT, &tgtCDF, _maskFgBg, true);

    // Compute the sum of absolute difference between the the reference CDFs and the tgt CDF
    float bgVal = 0;
    float fgVal = 0;
    for (int i = 0; i < tgtCDF.cols(); ++i)
    {
      bgVal += fabs(tgtCDF(0, i) - (*refCDF_BG)(0, i));
      fgVal += fabs(tgtCDF(0, i) - (*refCDF_FG)(0, i));
    }
    float ratio1 = 0;
    if (bgVal != 0) ratio1 = fgVal/bgVal;
    int v = 0;

    // At this time, we also check if this AOI is well focused
    ComputeCDFOfFFTMagnitude(tgtFFT, &tgtPDF, _maskFocus, false);
    float ratio2 = CheckForFocus(&tgtPDF, _refFocus);
    if (ratio2 < threshFocus || ratio1 > threshFgBg || bgVal == 0) v = 1;

    // pass the values
    *ratioFgBg = ratio1;
    *ratioFocus= ratio2;

    if (generateLogs)
    {
      cLogs.Descr+= "Ratio fgVal/bgVal ("+std::to_string(ratio1)+") <-> Thresh ("+std::to_string(threshFgBg)+")\n";
      cLogs.Descr+= "Ratio Focus ("+std::to_string(ratio2)+") <-> Thresh ("+std::to_string(threshFocus)+")  = "+std::to_string(v)+"\n";
      sig_Logs(cLogs);
    }

    tgtCDF.resize(0,0);
    tgtPDF.resize(0,0);
    return v;
  }//end of function

  /*! \brief BackgroundDetection
   *  This function is a driver to detect whether an unseen AOI is "closer" to a templated background
   *  or a foreground AOI. An FFT of the AOI is computed followed by a histogram of the magnitude
   *  of the FFT values. A distance metric is computed between this histogram and two previously
   *  computed histograms of a templated background and foreground image. A call is made based on the
   *  closeness of this histogram w.r.t either of the two histograms. We employ the E.M.D to compute
   *  this closeness.
   *  @_tgtFFT      : The FFT of the target AOI
   *  @_refHistFG   : CDF corresponding to the FFT Magnitude of the foreground template
   *  @_refHistBG   : CDF corresponding to the FFT Magnitude of the background template
   *  @_maskFgBg    : The mask within which a Fg/Bg metric will be computed
   *  @threshFgBg   : Threshold for determining if the aoi is a Fg or Bg
   *  @ratioFgBg    : The ratio when compared with a pure bg image
   *  At the end of this process, a return value of 1 indicates a background, while a value of 0
   *  indicates a foreground.
   */
  int HistogramUtils::BackgroundDetection(void* _tgtFFT, \
                                          void* _refHistFG,\
                                          void* _refHistBG,\
                                          void* _maskFgBg,\
                                          float threshFgBg,\
                                          float* ratioFgBg,\
                                          bool generateLogs)
  {
    typedef RMMatrix_Complex ComplexMatrix;
    // Instantiate callback structures
    CallbackStruct cLogs;
    cLogs.Module    = "File:=HistogramUtils.cpp; Function:=BackgroundDetection;";

    // typecast the objects
    ComplexMatrix* tgtFFT = static_cast<ComplexMatrix*>(_tgtFFT);
    RMMatrix_Float* refCDF_FG = static_cast<RMMatrix_Float*>(_refHistFG);
    RMMatrix_Float* refCDF_BG = static_cast<RMMatrix_Float*>(_refHistBG);

    // Now, we compute the histogram of the Magnitude of the FFT
    RMMatrix_Float tgtCDF, tgtPDF;
    ComputeCDFOfFFTMagnitude(tgtFFT, &tgtCDF, _maskFgBg, true);

    // Compute the sum of absolute difference between the the reference CDFs and the tgt CDF
    float bgVal = 0;
    float fgVal = 0;
    for (int i = 0; i < tgtCDF.cols(); ++i)
    {
      bgVal += fabs(tgtCDF(0, i) - (*refCDF_BG)(0, i));
      fgVal += fabs(tgtCDF(0, i) - (*refCDF_FG)(0, i));
    }
    float ratio1 = 0;
    if (bgVal != 0) ratio1 = fgVal/bgVal;
    int v = 0;
    if (ratio1 > threshFgBg || bgVal == 0) v = 1;

    // pass the values
    *ratioFgBg = ratio1;

    if (generateLogs)
    {
      cLogs.Descr+= "Ratio fgVal/bgVal ("+std::to_string(ratio1)+") <-> Thresh ("+std::to_string(threshFgBg)+")\n";
      sig_Logs(cLogs);
    }

    tgtCDF.resize(0,0);
    tgtPDF.resize(0,0);
    return v;
  }//end of function
}//end of namespace
