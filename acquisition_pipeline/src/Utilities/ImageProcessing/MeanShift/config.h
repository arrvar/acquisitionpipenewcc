/*! \file
  \verbatim
  
    Copyright (c) 2007, Sylvain Paris and Fr�do Durand

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use, copy,
    modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.

  \endverbatim
*/


#ifndef __CONFIG__
#define __CONFIG__


// select the type of data to be processed

#define GRAY_IMAGE     
// #define COLOR_IMAGE
// #define GRAY_MOVIE
// #define COLOR_MOVIE

// ######################################

// select the type of data structure to be used

// do not modify the following three lines
#define DENSE         0 // for small data set
#define SPARSE        1 // for large data set
#define ADAPTIVE      2 // for intermediate data set

// uncomment the selected option only
#define ARRAY_DENSITY DENSE
// #define ARRAY_DENSITY SPARSE
// #define ARRAY_DENSITY ADAPTIVE

// ######################################

// #define USE_PCA_REDUCTION // uncomment to use PCA reduction on feature points
#define PCA_DIMENSION 5 // set the target dimension for the feature points.

// ######################################

#define LABEL_ON_THE_FLY // comment to perform the full computation before simplifying the Morse-Smale complex (slower)

// ######################################

#define LOG_DENSITY  // comment to use the density instead of the log density to compute the persistence
#define COLOR_BIAS   // comment to use only the persistence to merge clusters without color similarity

// ######################################

#define APPLY_SIZE_THRESHOLD // comment to prevent tiny clusters to be aggregated to bigger ones

// ######################################

// #define OUTPUT_HIERARCHY // uncomment to output cluster numbers in "full.png" and the table of mergers in "hierarchy.txt"
                            // disable LABEL_ON_THE_FLY and APPLY_SIZE_THRESHOLD

// You should not need to modify anything beyond this point.
// ######################################
// ######################################
// ######################################
// ######################################
// ######################################


#ifdef OUTPUT_HIERARCHY
#  undef LABEL_ON_THE_FLY
#  undef APPLY_SIZE_THRESHOLD
#endif

#ifdef GRAY_IMAGE
# define IMAGE_MODE
# define GRAY_MODE
#endif

#ifdef COLOR_IMAGE
# define IMAGE_MODE
# define COLOR_MODE
#endif

#ifdef GRAY_MOVIE
# define MOVIE_MODE
# define GRAY_MODE
#endif

#ifdef COLOR_MOVIE
# define MOVIE_MODE
# define COLOR_MODE
#endif

// ######################################

#if defined(IMAGE_MODE)
#  define S_DIMENSION 2
#endif

#if defined(MOVIE_MODE)
#  define S_DIMENSION 3
#endif

// ######################################

#if defined(GRAY_MODE)
#  define R_DIMENSION 1
#endif

#if defined(COLOR_MODE)
#  define R_DIMENSION 3
#endif

// ######################################

#define INPUT_FEATURE_DIMENSION (R_DIMENSION + S_DIMENSION)

// ######################################

#define SPARSE_ARRAY_UNDERLYING_STRUCTURE GNU_HASH_MAP

// ######################################

// #define DENSITY_ONLY

// ######################################

#ifdef USE_PCA_REDUCTION

#define FEATURE_DIMENSION PCA_DIMENSION

#define GET_FEATURE(input) \
  feature_type feature; \
  reduce_input_feature(input,&feature);

#else

#define FEATURE_DIMENSION INPUT_FEATURE_DIMENSION

#define GET_FEATURE(input) \
  feature_type& feature = input;

#endif

// ######################################


// #define PROCESS_EMPTY_BINS

#define USE_LARGE_NEIGHBORHOOD

#define THIN_BOUNDARIES

// #define RANDOM_OFFSET_FILLING
// #define LINEARLY_INTERPOLATED_FILLING

#define COLOR_CODED_CLUSTERS





void display_options(){

  std::cout<<"\nActivated options:"
#ifdef IMAGE_MODE
	   <<"   IMAGE_MODE"
#endif
#ifdef MOVIE_MODE
	   <<"   MOVIE_MODE"
#endif
#ifdef GRAY_MODE
	   <<"   GRAY_MODE"
#endif
#ifdef COLOR_MODE
	   <<"   COLOR_MODE"
#endif
#if (ARRAY_DENSITY == DENSE)
	   <<"   (ARRAY_DENSITY == DENSE)"
#endif
#if (ARRAY_DENSITY == SPARSE)
	   <<"   (ARRAY_DENSITY == SPARSE)"
#endif
#if (ARRAY_DENSITY == ADAPTIVE)
	   <<"   (ARRAY_DENSITY == ADAPTIVE)"
#endif
#ifdef USE_LARGE_NEIGHBORHOOD
	   <<"   USE_LARGE_NEIGHBORHOOD"
#endif
#ifdef RANDOM_OFFSET_FILLING
	   <<"   RANDOM_OFFSET_FILLING"
#endif
#ifdef LINEARLY_INTERPOLATED_FILLING
	   <<"   LINEARLY_INTERPOLATED_FILLING"
#endif
#ifdef LABEL_ON_THE_FLY
	   <<"   LABEL_ON_THE_FLY"
#endif
#ifdef COLOR_CODED_CLUSTERS
	   <<"   COLOR_CODED_CLUSTERS"
#endif
	   <<"   (FEATURE_DIMENSION = "<<FEATURE_DIMENSION
#ifdef USE_PCA_REDUCTION
	   <<" / "<<INPUT_FEATURE_DIMENSION
#endif
	   <<")"
#ifdef DENSITY_ONLY
	   <<"   DENSITY_ONLY"
#endif
#ifdef LOG_DENSITY
	   <<"   LOG_DENSITY"
#endif
#ifdef COLOR_BIAS
	   <<"   COLOR_BIAS"
#endif
#ifdef OUTPUT_HIERARCHY
	   <<"   OUTPUT_HIERARCHY"
#endif
	   <<"\n"<<std::endl;
}

#endif
