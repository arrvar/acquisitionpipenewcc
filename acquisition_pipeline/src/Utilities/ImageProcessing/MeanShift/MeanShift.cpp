#include "Utilities/ImageProcessing/MeanShift/MeanShift.h"
#include "Utilities/ImageProcessing/MeanShift/type_definition.h"
#include "opencv2/opencv.hpp"
#include <iomanip>
#include <limits>
#include <sstream>
#include <cstring>
#include <iostream>

namespace spin
{
  namespace
  {
    using namespace std; 
    
    size_type n_bins;

    size_type padding;

    realN_type         min_coord;
    input_feature_type sigma;
    intN_type          size;

    real_type n_substeps;
    size_type n_iterations;

    saddle_list_type  saddle;
    cluster_list_type cluster;
    count_list_type cluster_size;

    size_type width;
    size_type height;

    label_type boundary_offset = numeric_limits<label_type>::max() / 2;

    #ifdef MOVIE_MODE
    size_type depth;
    #endif


    #ifdef USE_PCA_REDUCTION

    pca_matrix_type      pca_matrix;
    back_pca_matrix_type back_pca;
    input_feature_type   pca_mean;

    #endif


    bool verbose = false;
    
    Chrono init_chrono("init",false);
    Chrono creation_chrono("creation",false);
    Chrono filter_chrono("filter",false);
    Chrono sort_chrono("sort",false);
    Chrono safe_labels_chrono("node labels",false);
    #ifndef LABEL_ON_THE_FLY
    Chrono prune_chrono("prune",false);
    #endif
    Chrono pixel_chrono("pixel labels",false);

    template<typename Vector>
    inline realN_type point_from_index(const Vector& index)
    {

      realN_type res;

      for(size_type n=0;n<space_dimension;n++){
        res[n] = (index[n] + min_coord[n] - padding) * sigma[n];
      }

      return res;
    }


    template<typename Vector> // kind of hack, allows for a (N+1)-vector or a N-vector
    inline intN_type index_of_real_index(const Vector& r)
    {
      
      intN_type index;

      for(size_type n=0;n<space_dimension;n++){    
        index[n] = static_cast<size_type>(r[n] + 0.5);
      }
      return index;
    }




    template<typename Vector> // kind of hack, allows for a (N+1)-vector or a N-vector
    inline realN_type real_index_of_feature(const Vector& p)
    {
      
      realN_type index;

      for(size_type n=0;n<space_dimension;n++){    
        index[n] = p[n] - min_coord[n] + padding;
      }
      return index;
    }





    template<typename Vector> // kind of hack, allows for a (N+1)-vector or a N-vector
    inline intN_type index_of_feature(const Vector& p)
    {
      
      return index_of_real_index(real_index_of_feature(p));
    }




    inline void input_feature_from_pixel(const size_type           x,
                                        const size_type           y,
    #ifdef MOVIE_MODE
                                        const size_type           f,
    #endif
                                        const input_data_type&    image,
                                        input_feature_type* const feat)
    {
      
      input_feature_type& feature  = *feat;

      feature[0] = x;
      feature[1] = y;
      
    #ifdef IMAGE_MODE
    #ifdef GRAY_MODE
      feature[2] = image(PIXEL);
    #endif

    #ifdef COLOR_MODE
      const vector_color_type& clr = image(PIXEL);
      feature[2] = clr[0];
      feature[3] = clr[1];
      feature[4] = clr[2];
    #endif

    #ifdef TEXTURE_MODE
      const vector_texture_type& txt = image(PIXEL);
      for(size_type n = 0;n < R_DIMENSION;++n){
        feature[2+n] = txt[n];
      }
    #endif

    #endif // END  OF image_mode
      
    #ifdef MOVIE_MODE
      feature[2] = f;
    #ifdef GRAY_MODE
      feature[3] = image(PIXEL);
    #endif

    #ifdef COLOR_MODE
      const vector_color_type& clr = image(PIXEL);
      feature[3] = clr[0];
      feature[4] = clr[1];
      feature[5] = clr[2];
    #endif
    #endif // END  OF movie_mode   

      for(size_type n = 0;n < INPUT_FEATURE_DIMENSION;++n){
        feature[n] /= sigma[n];
      }  
    }


    inline void reduce_input_feature(const input_feature_type& input,
                                    feature_type* const feat)
    {
      
      feature_type& feature = *feat;

    #ifdef USE_PCA_REDUCTION

      feature = pca_matrix * (input - pca_mean);
      
    #else

      feature = input;

      Message::warning
      <<"reduce_input_feature: should not be here with PCA reduction disabled"
      <<Message::done;
      
    #endif // END OF use_pca_reduction
    }



    void init(const input_data_type&    image,
              const real_type           space_sigma,
    #ifdef MOVIE_MODE
              const real_type           time_sigma,
    #endif
              const real_type           range_sigma,
              const real_type           sampling_factor,
              const real_type           n_sub,
              feature_array_type* const f_image)
    {
      
      // Request memory space
      // ######################################

      feature_array_type& feature_image = *f_image;
      feature_image.resize(SIZES);

    #ifdef USE_PCA_REDUCTION

      input_feature_array_type input_feature_array(SIZES);
      
    #else
        
      feature_array_type& input_feature_array = feature_image;

    #endif // END OF use_pca_reduction


      
      init_chrono.start();
      
      // Set parameters
      // ######################################
      
      n_substeps = n_sub;
      
      n_iterations =
        static_cast<size_type>(n_substeps / (sampling_factor * sampling_factor));

      padding = n_iterations / 2 + 1; 
    //   padding = n_iterations / 2 + 2; 


      // Set sigmas
      // ######################################
      
      sigma[0] = space_sigma;
      sigma[1] = space_sigma;

    #ifdef IMAGE_MODE
      sigma[2] = range_sigma;

    #ifdef COLOR_MODE
      sigma[3] = range_sigma;
      sigma[4] = range_sigma;
    #endif

    #ifdef TEXTURE_MODE
      for(size_type n = 1;n < R_DIMENSION;++n){
        sigma[2+n] = range_sigma;
      }
    #endif
      
    #endif

    #ifdef MOVIE_MODE
      
      sigma[2] = time_sigma;
      sigma[3] = range_sigma;

    #ifdef COLOR_MODE
      sigma[4] = range_sigma;
      sigma[5] = range_sigma;
    #endif
    #endif

      sigma *= sampling_factor;


      
      // Create feature image
      // ######################################

      
      FOR_ALL_PIXELS {
        
        input_feature_type& input_feature = input_feature_array(PIXEL);
        input_feature_from_pixel(PIXEL,image,&input_feature);
      
      } // END OF for_all_pixels


      
    #ifdef USE_PCA_REDUCTION

      // Compute PCA if needed
      // ######################################
      
      vector<input_feature_type> eigen_vector;
      vector<real_type>          eigen_value;

      Matrix_tools::PCA(input_feature_array.begin(),
                        input_feature_array.end(),
                        &pca_mean,&eigen_vector,&eigen_value,11);

      for(size_type n = 0;n < FEATURE_DIMENSION;++n){

        const size_type m = INPUT_FEATURE_DIMENSION - n - 1;
        
        input_feature_type& v = eigen_vector[m];

        for(size_type i = 0;i < INPUT_FEATURE_DIMENSION;++i){
          pca_matrix(n,i) = v[i];
        }
      }
      
      back_pca = pca_matrix.transpose();
      
      // Compute reduced coordinates
      // ######################################
      
      FOR_ALL_PIXELS {
        GET_FEATURE(input_feature_array(PIXEL));
        feature_image(PIXEL) = feature;
      }
      
      
    #endif


      // Compute space size
      // ######################################
      
      realN_type max_coord;
      Geom_tools::get_bounding_box(feature_image.begin(),
                                  feature_image.end(),
                                  &min_coord,&max_coord);
      
      size = index_of_feature(max_coord);

      for(size_type n = 0;n < space_dimension;++n){
        size[n] += padding + 1;
      }

      init_chrono.stop();
      
      
    }

    void init_relabel(label_list_type* const relabel)
    {

      const size_type n = cluster.size();
      relabel->resize(n);
      for(size_type i = 0;i < n;i++){
        (*relabel)[i] = i;
      }
    }

    inline real_type color_square_distance(
    #if (ARRAY_DENSITY == ADAPTIVE)
      const SR_domain_type::dense_type& SR_domain,  
    #else
      const SR_domain_type& SR_domain,
    #endif
      const saddle_type&    saddle)
    {

    #if defined(USE_PCA_REDUCTION) || defined(DENSITY_ONLY)

      const SR_value_type& s1 = SR_domain[cluster[saddle.minor].summit_index];
      const SR_value_type& s2 = SR_domain[cluster[saddle.major].summit_index];

      const real_type d1 = DENSITY(s1);
      const real_type d2 = DENSITY(s2);
      
      feature_type f1,f2;

      for(size_type i = 0;i < FEATURE_DIMENSION;i++){
        f1[i] = s1[i];
        f2[i] = s2[i];
      }

        
      const input_feature_type v1 = back_pca * f1;
      const input_feature_type v2 = back_pca * f2;
      
      
    #else

      const SR_value_type& v1 = SR_domain[cluster[saddle.minor].summit_index];
      const SR_value_type& v2 = SR_domain[cluster[saddle.major].summit_index];

      const real_type d1 = DENSITY(v1);
      const real_type d2 = DENSITY(v2);

    #endif
      
      real_type d = 0;
      
      for(size_type i = S_DIMENSION;i < INPUT_FEATURE_DIMENSION;i++){
        const real_type r = v1[i] / d1 - v2[i] / d2;
        d += r * r;
      }

      return d;

    }

    inline real_type persistence(
    #if (ARRAY_DENSITY == ADAPTIVE)
      const SR_domain_type::dense_type& SR_domain,  
    #else
      const SR_domain_type& SR_domain,
    #endif
      const saddle_type&    saddle)
    {

      const real_type topological =
    #ifdef LOG_DENSITY
        log(cluster[saddle.minor].summit_density) - log(DENSITY(saddle.value));
    #else
        cluster[saddle.minor].summit_density - DENSITY(saddle.value);
    #endif

    #ifdef COLOR_BIAS
      const real_type color = sqrt(color_square_distance(SR_domain,saddle));
      
      return topological * color;
    #else
      return topological;
    #endif
    }

    void update_saddle(
    #if (ARRAY_DENSITY == ADAPTIVE)
      const SR_domain_type::dense_type& SR_domain,
    #else  
      const SR_domain_type&  SR_domain,
    #endif
      const label_list_type& relabel,
      saddle_type* const     saddle)
    {

      label_type& M = saddle->major;
      label_type& m = saddle->minor;
      
      M = get_label(M,relabel);
      m = get_label(m,relabel);

      if (cluster[M] < cluster[m]) swap(M,m);

      saddle->persistence = (m == M)
        ? numeric_limits<real_type>::max()
        : persistence(SR_domain,*saddle);
    }

    void prune_persistence(const SR_domain_type&  SR_dom,
                          const real_type        threshold,
                          label_list_type* const relabel)
    {

    #if (ARRAY_DENSITY == ADAPTIVE)
      
      SR_domain_type::dense_type& SR_domain = *(SR_dom.dense);
      
    #else
      
      const SR_domain_type& SR_domain = SR_dom;
      
    #endif

    #ifdef OUTPUT_HIERARCHY
      ofstream out_hierarchy("hierarchy.txt");
    #endif
      
      bool collapsed = true;

      while(collapsed){

        collapsed = false;
        
        for(saddle_list_type::iterator
              s     = saddle.begin(),
              s_end = saddle.end();
            (s != s_end);s++){
          
          update_saddle(SR_domain,*relabel,&(*s));      
        }
        
        const saddle_type& s = *min_element(saddle.begin(),saddle.end());
        
        if (s.persistence < threshold){
          (*relabel)[s.minor] = s.major;
          collapsed = true;
          
    #ifdef OUTPUT_HIERARCHY
          out_hierarchy << s.minor << "\t->\t" << s.major << '\n';
    #endif
        }
      }
      
    #ifdef OUTPUT_HIERARCHY
      out_hierarchy.flush();
      out_hierarchy.close();
    #endif
    }

    real_type get_persistence_of_pair(const label_type l1,
                                      const label_type l2)
    {

      for(saddle_list_type::iterator
            s     = saddle.begin(),
            s_end = saddle.end();
          (s != s_end);s++){

        if (((s->major == l1) && (s->minor == l2)) ||
            ((s->major == l2) && (s->minor == l1))){
          return s->persistence;
        }
      }
      
      return -1;
    
    }

    void fill_SR_domain(const feature_array_type&   feature_image,
                        SR_domain_type* const       SR_dom,
                        bool_SR_domain_type* const  occ)
    {

      // Request memory space
      // ######################################
      
    #if (ARRAY_DENSITY == ADAPTIVE)
      
      SR_dom->sparse = new SR_domain_type::sparse_type;
      SR_dom->dense = NULL;
      SR_domain_type::sparse_type& SR_domain = *(SR_dom->sparse);

      occ->sparse = new bool_SR_domain_type::sparse_type;
      bool_SR_domain_type::sparse_type& occupancy = *(occ->sparse);
      
    #else
      
      SR_domain_type&      SR_domain = *SR_dom;
      bool_SR_domain_type& occupancy = *occ;
      
    #endif

      SR_domain.resize(size);  
      occupancy.resize(size);


      
      creation_chrono.start();


      
      // Create random LUT
      // ######################################  

    #ifdef RANDOM_OFFSET_FILLING

      const size_type n_offsets = 151;
      vector<real_type> random_offset(n_offsets);

      for(size_type n = 0;n < n_offsets;++n){
        random_offset[n] = Math_tools::random(-0.5,0.5);
      }

      size_type offset = 0;
      
    #endif


      
      // Fill in space
      // ######################################
    
      SR_value_type value;
      DENSITY(value) = 1.0;

    #ifdef DENSITY_ONLY

      FOR_ALL_PIXELS {  
        const intN_type i = index_of_feature(feature_image(PIXEL));
        SR_domain[i] += value;
        occupancy[i]  = true;
      }
      
    #else
      
      realN_type q;

      FOR_ALL_PIXELS {  
        q = real_index_of_feature(feature_image(PIXEL));
            
        for(size_type i = 0;i<space_dimension;i++){
          
    #ifdef RANDOM_OFFSET_FILLING
          value[i] = q[i] + random_offset[offset];
          offset   = (offset + 1) % n_offsets;
    #else
          value[i] = q[i];
    #endif
        }
          
        const intN_type i = index_of_real_index(q);
    
    #ifdef LINEARLY_INTERPOLATED_FILLING
        Math_tools::write_through_Nlinear_interpolation(value,&SR_domain,q);
    #else
        SR_domain[i] += value;
    #endif
        
        occupancy[i] = true;
      }

    #endif

      creation_chrono.stop();
      
    }

    void create_filter_mask(const size_type    n_iter,
                            vector<real_type>* const m)
    {

      const real_type delta = 0.5 / n_substeps;

      const size_type size = 2 * n_iter + 1;
      
      vector<real_type>& mask = *m;
      mask.resize(size);
      mask[n_iterations] = 1.0;

      vector<real_type> buffer(mask);
      
      for(size_type iter = 0;iter < n_iter;iter++){
        
        for(size_type
              n     = n_iter - iter,
              n_end = n_iter + iter;
            n <= n_end;n++){

          const real_type v = mask[n] * delta;
          
          buffer[n-1] += v;
          buffer[n]   += -2.0 * v;
          buffer[n+1] += v;
        }

        mask = buffer;
      }
    }

    void filter_SR_domain(SR_domain_type* const SR_dom,
                          const real_type       /*sampling_factor*/)
    {

      
    #if (ARRAY_DENSITY == ADAPTIVE)
      
      SR_domain_type::sparse_type& SR_domain = *(SR_dom->sparse);

      typedef SR_domain_type::sparse_type buffer_type;
      
    #else
      
      SR_domain_type& SR_domain = *SR_dom;

      typedef SR_domain_type buffer_type;
      
    #endif

      
    #if ARRAY_DENSITY == DENSE
      
      const real_type delta = 0.5 / n_substeps;
      
      SR_domain_type buffer(size);

      intN_type start_index(vector<key_coef_type>(space_dimension,1));

      const intN_type end_index = size - start_index;

      for(size_type n = 0;n < SR_domain_type::dimension;n++){

        intN_type zero_index,offset_index;
        offset_index[n] = 1;

        const SR_domain_type::difference_type offset
          = &(SR_domain[offset_index]) - &(SR_domain[zero_index]);

        for(size_type iter = 0;iter < n_iterations;iter++){

          memcpy(&(*buffer.begin()),
                &(*SR_domain.begin()),
                SR_domain.size() * sizeof(SR_domain_type::value_type));
            
          intN_type index = start_index;
          do{
            
            const SR_value_type& b = buffer[index];

            if (DENSITY(b) > 0){
              const SR_value_type v  = b * delta;
              
              SR_value_type* const ptr = &(SR_domain[index]);
              *(ptr-offset) += v;
              *(ptr+offset) += v;
              *ptr          -= v * 2.0;
            }
          }

          while(SR_domain.advance(&index,start_index,end_index));

        } // END OF for iter
      } // END OF for n

    #endif



      
    #if (ARRAY_DENSITY == SPARSE) || (ARRAY_DENSITY == ADAPTIVE)
      
      buffer_type buffer;
      
      buffer.resize(size);
      vector<real_type> mask;
      create_filter_mask(n_iterations,&mask);
      
      for(size_type n=0;n<buffer_type::dimension;n++){

        SR_domain.swap(buffer); SR_domain.clear();

        for(buffer_type::iterator
              i     = buffer.begin(),
              i_end = buffer.end();
            i != i_end;
            i++){
          
          intN_type index        = i->first;
          const SR_value_type& v = i->second;

          index[n] -= n_iterations;
          
          for(size_type
                o     = 0,
                o_end = 2 * n_iterations + 1;
              o <= o_end;o++,index[n]++){

            SR_domain[index] += v * mask[o];
          } // END OF for o      
        } // END OF for i   
      } // END OF for n

    #endif
      
    }

    void build_sorted_bin_list(const SR_domain_type&      SR_dom,
    #ifdef PROCESS_EMPTY_BINS
                              const bool_SR_domain_type&,
    #else
                              const bool_SR_domain_type& occ,
    #endif
                              
                              index_list_type* const     bin_list)
    {

      index_list_type& index_list = *bin_list;

      valued_index_list_type valued_index_list;

    #if (ARRAY_DENSITY == ADAPTIVE)
      
      SR_domain_type::sparse_type& SR_domain      = *(SR_dom.sparse);
      bool_SR_domain_type::sparse_type& occupancy = *(occ.sparse);

      typedef SR_domain_type::sparse_type domain_type;
      
    #else
      
      const SR_domain_type&      SR_domain = SR_dom;
      const bool_SR_domain_type& occupancy = occ;

      typedef SR_domain_type domain_type;

    #endif


      
    #if (ARRAY_DENSITY == DENSE)
      
      static intN_type all_one(vector<key_coef_type>(space_dimension,1));

      const intN_type start_index = all_one;
      const intN_type end_index   = size - all_one;
      
      intN_type index = start_index;
      do{

    //     const real_type v = SR_domain[index][value_dimension-1];
        const real_type v = DENSITY(SR_domain[index]);

    #ifdef PROCESS_EMPTY_BINS
        if (v > 0)
    #else
          if ((v > 0) && occupancy[index])
    #endif
          {
            const valued_index_type valued_index(v,index);
            valued_index_list.push_back(valued_index);
          }
      }
      while(SR_domain.advance(&index,start_index,end_index));
      
    #endif


      
      
    #if (ARRAY_DENSITY == SPARSE) || (ARRAY_DENSITY == ADAPTIVE)

      for(domain_type::iterator
            i     = const_cast<domain_type&>(SR_domain).begin(),
            i_end = const_cast<domain_type&>(SR_domain).end();
          i != i_end;i++){

    //     const real_type v = i->second[value_dimension-1];
        const real_type v = DENSITY(i->second);
        
    #ifdef PROCESS_EMPTY_BINS
        if (v > 0)
    #else
          if ((v > 0) && occupancy[i->first])
    #endif
          {
            const valued_index_type valued_index(v,i->first);
            valued_index_list.push_back(valued_index);
          }
        
      }
      
    #endif

      sort(valued_index_list.begin(),valued_index_list.end());

      index_list.clear();
      index_list.reserve(valued_index_list.size());

      for(valued_index_list_type::reverse_iterator
            i     = valued_index_list.rbegin(),
            i_end = valued_index_list.rend();
          i != i_end;
          i++){

        index_list.push_back(i->data);
      }
    }

    label_type classify_point(
      
    #if (ARRAY_DENSITY == ADAPTIVE)
      const SR_domain_type::dense_type&       SR_domain,
      const label_SR_domain_type::dense_type& label,
    #else
      const SR_domain_type&       SR_domain,
      const label_SR_domain_type& label,
    #endif
      const realN_type&           point)
    {

      label_type l = NO_LABEL;
      realN_type p = point;
      realN_type ms,prev_ms;
      
      size_type n = 0;

    #ifdef DENSITY_ONLY
      const size_type max_iterations = 20;
    #else
      const size_type max_iterations = 20;
    #endif
      
      do{

    #ifdef DENSITY_ONLY

        Math_tools::Nlinear_interpolation_gradient(SR_domain,p,&ms);
        ms.normalize();
        ms *= 0.2;

        
    #else
        
        const SR_value_type mean =
          Math_tools::Nlinear_interpolation(SR_domain,p);

        const real_type density = DENSITY(mean);
        
        for(size_type n=0;n<space_dimension;n++){
          ms[n] = mean[n] / density - p[n];
        }

        
    #endif
        
        const real_type dot = ms * prev_ms;
        
        if (dot < 0){
          ms -= prev_ms * dot;
        }
        
        const real_type sq_norm = ms.square_norm();
        const real_type thr = 0.2;
        if (sq_norm < thr*thr){
          ms /= 1.0 / thr * sqrt(sq_norm);
        }
        
        p += ms;
        prev_ms = ms.unit();

        l = label[index_of_real_index(p)];
      }
      while((l <= 0) && (++n < max_iterations));
      return l;
    }

    void build_neighbor_list(index_list_type* const neighbor)
    {

      index_list_type& n = *neighbor;

    #ifdef USE_LARGE_NEIGHBORHOOD
      
      const size_type size = Math_tools::power<space_dimension>(3) - 1;
      n.resize(size);
      
      intN_type all_one(vector<key_coef_type>(space_dimension,1));
      intN_type all_three(vector<key_coef_type>(space_dimension,3));

      Array_ND<space_dimension,bool> proxy(all_three);

      size_type i = 0;
      intN_type index;
      do{
        if (index != all_one){
          n[i] = index - all_one;
          i++;
        }
      }
      while(proxy.advance(&index));
      
    #else
      
      const size_type size = space_dimension * 2;
      n.resize(size);

      for(size_type d = 0;d < space_dimension;d++){

        intN_type index;
        index[d] = -1;
        n[2*d]   = index;
        
        index[d] = 1;
        n[2*d+1] = index;
      }

    #endif
    }

    void safe_label(const SR_domain_type&       SR_dom,
                    const index_list_type&      index_list,
    #ifdef LABEL_ON_THE_FLY
                    const real_type             persistence_threshold,
    #endif
                    label_SR_domain_type* const lab,
                    label_list_type* const      relab)
    {

    #if (ARRAY_DENSITY == ADAPTIVE)
      
      SR_domain_type::dense_type& SR_domain   = *(SR_dom.dense);

      lab->sparse = NULL;
      lab->dense = new label_SR_domain_type::dense_type;
      label_SR_domain_type::dense_type& label = *(lab->dense);
      
    #else
      
      const SR_domain_type& SR_domain = SR_dom;
      label_SR_domain_type& label     = *lab;
      
    #endif
      
      static intN_type all_one(vector<key_coef_type>(space_dimension,1));
      static intN_type all_two(vector<key_coef_type>(space_dimension,2));
      
      label_list_type& relabel = *relab;
      relabel.clear();
      relabel.push_back(0);

      size_type labelled_bins = 0;
      size_type easy_bins = 0;
      size_type saved_bins = 0;

      label.resize(size);
      
      label_type max_ink = 0;

      const size_type n_neighbors = Math_tools::power<space_dimension>(3)-1;

      index_list_type neighbor_offset(n_neighbors);
      build_neighbor_list(&neighbor_offset);

      for(index_list_type::const_iterator
            i     = index_list.begin(),
            i_end = index_list.end();
          i != i_end;
          i++){

        const intN_type& index = *i;
        
        valued_label_list_type neighbor;
        label_set_type touching_cluster;
          
        for(size_type o=0 ; o<n_neighbors ; ++o){
        
          const intN_type sub_index = index + neighbor_offset[o];
        
          const label_type l = label[sub_index];

          if (l > 0){
            const real_type d   = DENSITY(SR_domain[sub_index]);
            neighbor.push_back(valued_label_type(d,l));
            touching_cluster.insert(l);
          }
        }

        label_type ink = NO_LABEL;

        switch (touching_cluster.size()){
          
        case 0: // new summit found, create a new cluster
          max_ink++;
          ink = max_ink;
          cluster.push_back(cluster_type(ink,index,
                                        DENSITY(SR_domain[index])));
          relabel.push_back(max_ink);
          break;
          
        case 1: // non-ambiguous labeling, just copy the label
          ink = neighbor[0].data;
          break;

        default: // ambiguous labeling, two or more clusters nearby
        {
          const label_type default_label
            = max_element(neighbor.begin(),neighbor.end())->data;
          ink = - 2 * default_label;

          label_set_type::iterator l1 = touching_cluster.begin();
          label_set_type::iterator post_l1 = l1;
          post_l1++;
            
          label_set_type::iterator l1_end = touching_cluster.end();
          for(; post_l1 != l1_end ; l1 = post_l1 , ++post_l1){

            cluster_type& c1 = cluster[*l1];
              
            label_set_type::iterator l2 = l1;
            l2++;
            for(label_set_type::iterator l2_end = touching_cluster.end();
                l2 != l2_end;l2++){

              cluster_type& c2 = cluster[*l2];

    #ifdef LABEL_ON_THE_FLY
              if ((get_label(c1.label,relabel) == get_label(c2.label,relabel))){
                ink = - 2 * c1.label - 1;
              }
              else 
    #endif
              if (!cluster_type::are_neighbors(c1,c2,relabel)){
                
                saddle_type s(c1,c2,SR_domain[index]);

    #ifdef LABEL_ON_THE_FLY
                update_saddle(SR_domain,relabel,&s);
                
                if (persistence(SR_domain,s) < persistence_threshold){
                  relabel[s.minor] = s.major;
                  ink = - 2 * s.major - 1; // indicate that it does not need to be refined
                  if (c2 < c1) c1.neighbor.insert(c1.neighbor.end(),
                                                  c2.neighbor.begin(),
                                                  c2.neighbor.end());
                  else c2.neighbor.insert(c2.neighbor.end(),
                                          c1.neighbor.begin(),
                                          c1.neighbor.end());
                }
                else{
                  saddle.push_back(s);	      
                  c1.neighbor.push_back(c2.label);
                  c2.neighbor.push_back(c1.label);
                }
    #else			    
                saddle.push_back(s);	      
                c1.neighbor.push_back(c2.label);
                c2.neighbor.push_back(c1.label);
    #endif
              } // END OF if !are_neighbors
            } // END OF for l2
          } // END OF for l1
                
        } // END OF default case 
        } // END OF switch touching_cluster.size()
        
        if (ink > 0) labelled_bins++;
        
        label[index] = ink;
      }
      
      if (verbose) cout<<"  Created clusters: "<<max_ink<<'\n'
                      <<"  Labelled bins: "<<labelled_bins<<" on "<<index_list.size()
                      <<" ("<<(100*labelled_bins/index_list.size())<<"%)\n"
                      <<"  Easy bins:     "<<easy_bins
                      <<" ("<<(100*easy_bins/index_list.size())<<"%)\n"
                      <<"  Saved bins:    "<<saved_bins
                      <<" ("<<(100*saved_bins/index_list.size())<<"%)"
                      <<endl;
    }

    void label_pixels(const input_data_type&      input,
                      const SR_domain_type&       SR_dom,
                      const label_SR_domain_type& lab,
                      const bool                  fill_holes,
                      output_label_type* const    out)
    {

    #if (ARRAY_DENSITY == ADAPTIVE)
      
      SR_domain_type::dense_type& SR_domain   = *(SR_dom.dense);
      label_SR_domain_type::dense_type& label = *(lab.dense);
      
    #else
      
      const SR_domain_type& SR_domain   = SR_dom;
      const label_SR_domain_type& label = lab;
      
    #endif

      output_label_type& label_image = *out;
      label_image.assign(SIZES,NO_LABEL);

      intN_type index;
      size_type labelled_pixels = 0;

      cluster_size.assign(cluster.size(),0);
        
      if (fill_holes){
        
        FOR_ALL_PIXELS {	  

          input_feature_type input_feature;
          input_feature_from_pixel(PIXEL,input,&input_feature);

          GET_FEATURE(input_feature);

          const realN_type r = real_index_of_feature(feature);
          const intN_type i  = index_of_real_index(r);
          
          label_type l = label[i];

          if (l <= 0){
            
            label_type point_label;
            if (l%2 == 0){
              point_label = classify_point(SR_domain,label,r);
            }
            else{
              point_label = -l/2;
            }

            l = (point_label > 0) ? point_label :
              ((point_label < 0 ) ? -point_label/2 : -l/2);
          }
          
          if (l > 0){
            labelled_pixels++;
            cluster_size[l]++;
            label_image(PIXEL) = l;
          }

        } 
      }
      else{

        FOR_ALL_PIXELS {	  
          input_feature_type input_feature;
          input_feature_from_pixel(PIXEL,input,&input_feature);

          GET_FEATURE(input_feature);

          const realN_type r = real_index_of_feature(feature);
          const intN_type i  = index_of_real_index(r);	  
          label_type l       = label[i];      

          if ((l<0) && (l%2 != 0)){
            l = -l/2;
          }
          
          if (l>0){
            labelled_pixels++;
            cluster_size[l]++;
            label_image(PIXEL) = l;
          }     
        } 
      }

      if (verbose) cout<<"  Labelled pixels: "<<labelled_pixels
                      <<" on "<<input.size()<<" ("
                      <<(static_cast<long int>(labelled_pixels)*100/input.size())<<"%)"
                      <<endl;  
    }

    void draw_boundaries(const input_data_type&   input,
                        const SR_domain_type&    SR_dom,
                        const output_label_type& label_data,
                        const label_list_type&   relabel,
                        rgb_data_type* const     out)
    {

      const real_type shade_factor = 0.0;
      
    #if (ARRAY_DENSITY == ADAPTIVE)
      
      SR_domain_type::dense_type& SR_domain = *(SR_dom.dense);
      
    #else
      
      const SR_domain_type& SR_domain = SR_dom;
      
    #endif
      
      real_type max_persistence = 0;
      
      for(saddle_list_type::iterator
            s     = saddle.begin(),
            s_end = saddle.end();
          s != s_end;s++){

        update_saddle(SR_domain,relabel,&(*s));
        max_persistence = max(persistence(SR_domain,*s),max_persistence);
      }

      
      rgb_data_type& output = *out;
      output.resize(SIZES);

      real_type gray = 1.0;
      
      for(size_type x = 0,x_end = width;x < x_end;x++){

        const size_type xm = (x > 0) ? (x - 1) : x; 
        const size_type xp = (x < width-1) ? (x + 1) : x; 
        
        for(size_type y = 0,y_end = height;y < y_end;y++){

          const size_type ym = (y > 0) ? (y - 1) : y; 
          const size_type yp = (y < height-1) ? (y + 1) : y; 
          
    #ifdef MOVIE_MODE
          for(size_type f = 0,f_end = depth;f < f_end;f++){

    // 	const size_type fm = (f > 0) ? (f - 1) : f; 
    // 	const size_type fp = (f < depth-1) ? (f + 1) : f; 

            const label_type l       = get_label(label_data(x,y, f),relabel);
            const label_type l_up    = get_label(label_data(x,yp,f),relabel);
            const label_type l_down  = get_label(label_data(x,ym,f),relabel);
            const label_type l_left  = get_label(label_data(xp,y,f),relabel);
            const label_type l_right = get_label(label_data(xm,y,f),relabel);

            const bool up    = (l != l_up)    && (yp != height-1) && (ym != 0);
            const bool down  = (l != l_down)  && (yp != height-1) && (ym != 0);
            const bool left  = (l != l_left)  && (xp != width-1) && (xm != 0);
            const bool right = (l != l_right) && (xp != width-1) && (xm != 0);
            
            const bool is_boundary = (up || down || left || right);
            
            gray = -1.0;

            if (up)    gray = max(gray,get_persistence_of_pair(l,l_up));
            if (down)  gray = max(gray,get_persistence_of_pair(l,l_down));
            if (left)  gray = max(gray,get_persistence_of_pair(l,l_left));
            if (right) gray = max(gray,get_persistence_of_pair(l,l_right));

            gray = (gray < 0) ? 1.0 : pow(gray / max_persistence,0.5f);	
            
    #endif

    #ifdef IMAGE_MODE	
            const label_type l       = get_label(label_data(x,y),relabel);
            const label_type l_up    = get_label(label_data(x,yp),relabel);
            const label_type l_down  = get_label(label_data(x,ym),relabel);
            const label_type l_left  = get_label(label_data(xp,y),relabel);
            const label_type l_right = get_label(label_data(xm,y),relabel);

            const bool up    = (l != l_up)    && (yp != height-1) && (ym != 0);
            const bool down  = (l != l_down)  && (yp != height-1) && (ym != 0);
            const bool left  = (l != l_left)  && (xp != width-1) && (xm != 0);
            const bool right = (l != l_right) && (xp != width-1) && (xm != 0);
            
            const bool is_boundary = (up || down || left || right);

            gray = -1.0;

            if (up)    gray = max(gray,get_persistence_of_pair(l,l_up));
            if (down)  gray = max(gray,get_persistence_of_pair(l,l_down));
            if (left)  gray = max(gray,get_persistence_of_pair(l,l_left));
            if (right) gray = max(gray,get_persistence_of_pair(l,l_right));

            gray = (gray < 0) ? 1.0 : pow(gray / max_persistence,0.5f);	
    #endif

            double r,g,b;

            if (is_boundary){
              gray = 0.1 + 0.9 * gray;
              r = gray;
              g = gray;
              b = gray;
            }
            else{
              
    #ifdef COLOR_MODE
              const vector_color_type& v = input(PIXEL) * shade_factor;
              RGB_converter_to_Lab<real_type>::
                from_cartesian_to_RGB(v[0],v[1],v[2],&r,&g,&b);	  
    #endif

    #ifdef GRAY_MODE
              const real_type v = input(PIXEL) * shade_factor;
              RGB_converter_to_Lab<real_type>::
                from_cartesian_to_RGB(v,0,0,&r,&g,&b);
    #endif
            
            }

            output(PIXEL) = rgb_color_type(r,g,b);
                    
    #ifdef MOVIE_MODE
          } // END OF for f
    #endif
        } // END OF for y
      } // END OF for x
    }

    void color_cluster(const SR_domain_type&    SR_dom,
                      const output_label_type& label_data,
                      const size_type          size_threshold,
                      /*const*/ label_list_type&   relabel,
                      rgb_data_type* const     out)
    {

    #if (ARRAY_DENSITY == ADAPTIVE)
      
      SR_domain_type::dense_type& SR_domain = *(SR_dom.dense);
      
    #else
      
      const SR_domain_type& SR_domain = SR_dom;
      
    #endif
      
      rgb_data_type& output = *out;
      
      output.assign(SIZES,rgb_color_type::black);

      size_type colored_pixels = 0;  

      
      FOR_ALL_PIXELS {
        
        label_type l = get_label(label_data(PIXEL),relabel);

    #ifdef APPLY_SIZE_THRESHOLD
        if ((x > 0) && (l > 0) && (cluster_size[l] < size_threshold)){
          const label_type ll = get_label(label_data(PREVIOUS_PIXEL),relabel);
          relabel[l] = ll;
          l = ll;
        }
    #endif
        
        if (l > 0){
          
    #ifdef COLOR_CODED_CLUSTERS
          
          unsigned char r,g,b;

    #ifdef OUTPUT_HIERARCHY
          
          b = l % 256; l /= 256;
          g = l % 256; l /= 256;
          r = l % 256;      
          
    #else
          
          Color_tools::RGB_from_index(l,&r,&g,&b);

    #endif // OUTPUT_HIERARCHY

          output(PIXEL) = rgb_color_type(r/255.0,g/255.0,b/255.0);
    
    #else // COLOR_CODED_CLUSTERS
          
          double r,g,b;
          const SR_value_type v = SR_domain[cluster[l].summit_index];
          const real_type     d = DENSITY(v);
          const realN_type    p = point_from_index(v / d);
          
    #ifdef GRAY_MODE
          
          RGB_converter_to_Lab<real_type>::
            from_cartesian_to_RGB(p[S_DIMENSION],0,0,&r,&g,&b);

          output(PIXEL) = rgb_color_type(r,g,b);
          
    #endif // GRAY_MODE

    #ifdef COLOR_MODE
          RGB_converter_to_Lab<real_type>::
            from_cartesian_to_RGB(p[S_DIMENSION + 0],
                                  p[S_DIMENSION + 1],
                                  p[S_DIMENSION + 2],&r,&g,&b);

          output(PIXEL) = rgb_color_type(r,g,b);
    #endif // COLOR_MODE
          
    #endif // COLOR_CODED_CLUSTERS
          
              
          colored_pixels++;
        }	
      } 
    }

    void report_occupancy_ratio(const SR_domain_type& SR_domain)
    {

    #if (ARRAY_DENSITY == DENSE) 
      
      size_type count = 0;

      for(SR_domain_type::const_iterator i = SR_domain.begin();
          i != SR_domain.end();i++){

        count += (DENSITY(*i) != 0) ? 1 : 0;
      }

      cout<<"Occupancy ratio: "<<count<<"/"<<SR_domain.size()<<" = "
          <<(100*count/SR_domain.size())<<"%"<<endl;
      
    #endif


      
    #if (ARRAY_DENSITY == SPARSE)
      cout<<"Occupancy ratio: "<<SR_domain.size()<<"/"<<n_bins<<" = "
          <<(100*SR_domain.size()/n_bins)<<"% [sparse structure]"<<endl;  
    #endif

      
      
    #if (ARRAY_DENSITY == ADAPTIVE)

      if (SR_domain.dense != NULL){
        size_type count = 0;

        for(SR_domain_type::dense_type::const_iterator i = SR_domain.dense->begin();
            i != SR_domain.dense->end();i++){

          count += (DENSITY(*i) != 0) ? 1 : 0;
        }

        cout<<"Occupancy ratio: "<<count<<"/"<<SR_domain.dense->size()<<" = "
            <<(100*count/SR_domain.dense->size())<<"%"<<endl;
      }
      else{
        cout<<"Occupancy ratio: "<<SR_domain.sparse->size()<<"/"<<n_bins<<" = "
            <<(100*SR_domain.sparse->size()/n_bins)<<"% [sparse structure]"<<endl;  
      }
      
    #endif
    }

    void write_rgb_data(const string  header, const rgb_data_type& data)
    {
      #ifdef IMAGE_MODE
        PPM_tools::write_ppm((header + string(".ppm")).c_str(),data);
      #endif

      #ifdef MOVIE_MODE
        
        rgb_image_type output(width,height);
        
        for(size_type f = 0,f_end = depth;f < f_end;f++){
          
          for(size_type x = 0,x_end = width;x < x_end;x++){
            for(size_type y = 0,y_end = height;y < y_end;y++){

              output(x,y) = data(x,y,f);
            }
          }

          ostringstream name_out;
          name_out<<"movie/"<<header<<setw(4)<<setfill('0')<<f<<".png";
          PPM_tools::write_ppm(name_out.str().c_str(),output);    
        }
        
      #endif
    }

#if (ARRAY_DENSITY == ADAPTIVE)
    void sparse_to_dense(SR_domain_type* const      SR_dom)
    {
      SR_domain_type& SR_domain      = *SR_dom;
      
      SR_domain.dense = new SR_domain_type::dense_type(size);

      SR_domain_type::dense_type&  dense  = *(SR_domain.dense);
      SR_domain_type::sparse_type& sparse = *(SR_domain.sparse);

      static intN_type all_one(vector<key_coef_type>(space_dimension,1));
      static const intN_type corner1 = intN_type();
      static const intN_type corner2 = size - all_one;
      
      for(SR_domain_type::sparse_type::iterator
            s     = sparse.begin(),
            s_end = sparse.end();
          s != s_end;
          ++s){

        const intN_type& index = s->first;

        if (Geom_tools::inside_bounding_box(corner1,corner2,index)){
          dense[s->first] = s->second;
        }
      }

      delete SR_domain.sparse;
      SR_domain.sparse = NULL;
    }
#endif
  }
  
  /*! \brief Mean-Shift segmentation using opencv Mat matrices */
  int MeanShift::Compute( void* _img, \
                          float p1, float p2, float p3, \
                          void* _imgOut)
  {
    // typecast the input
    cv::Mat* img = static_cast<cv::Mat*>(_img);
    if (!img->data) return -1;
    
    // convert the data to L,A,B space
    cv::Mat convMat;
    img->convertTo(convMat,CV_32FC3);
    convMat *= 1./255;
    // conversion of BGR image to LAB
    cv::cvtColor(convMat, convMat, CV_BGR2Lab);
    cv::Mat splitImg[3];
    cv::split(convMat, splitImg);
    
    string input_name;
    real_type space_sigma     = static_cast<real_type>(p1);
    real_type range_sigma     = static_cast<real_type>(p2);
    real_type pers_thr        = static_cast<real_type>(p3);
    real_type sampling_factor = 1.0;
    real_type n_sub           = 2.0;
  
#ifdef MOVIE_MODE
    real_type time_sigma;
#endif

    cluster.push_back(cluster_type());
  
#ifdef IMAGE_MODE
    const size_type size_threshold = static_cast<size_type>(space_sigma * space_sigma / 2.0);
#endif

#ifdef IMAGE_MODE
    width  = convMat.cols;
    height = convMat.rows;

    input_data_type input;
    input.resize(SIZES);

    #ifdef GRAY_MODE
      for(size_type x = 0;x < width;x++)
      for(size_type y = 0;y < height;y++)
      {
        input(x,y) = static_cast<real_type>(splitImg[0].at<float>(y,x));
      }        
    #endif
    
    #ifdef COLOR_MODE
      for(size_type x = 0;x < width;x++)
      for(size_type y = 0;y < height;y++)
      {
        input(x,y)[0] = static_cast<real_type>(splitImg[0].at<float>(y,x));
        input(x,y)[1] = static_cast<real_type>(splitImg[1].at<float>(y,x));
        input(x,y)[2] = static_cast<real_type>(splitImg[2].at<float>(y,x));
      }
    #endif
#endif

    SR_domain_type      SR_domain;
    bool_SR_domain_type occupancy;
    feature_array_type  feature_image;
    init( input, space_sigma,
          #ifdef MOVIE_MODE
          time_sigma,
          #endif      
          range_sigma,
          sampling_factor,n_sub,
          &feature_image);
    
    n_bins = 1;
    fill_SR_domain(feature_image,&SR_domain,&occupancy);
    filter_SR_domain(&SR_domain,sampling_factor);
    label_SR_domain_type label;
    index_list_type index_list;
    build_sorted_bin_list(SR_domain,occupancy,&index_list);
#if (ARRAY_DENSITY == ADAPTIVE)
    sparse_to_dense(&SR_domain);
#endif
    label_list_type safe_relabel;
    safe_label( SR_domain,index_list,
                #ifdef LABEL_ON_THE_FLY
                pers_thr,
                #endif
                &label,&safe_relabel);
    output_label_type label_data;
    rgb_data_type     output;

    label_list_type no_relabel;
    init_relabel(&no_relabel);

    label_pixels(input,SR_domain,label,false,&label_data);
  
#ifdef LABEL_ON_THE_FLY
    color_cluster(SR_domain,label_data,0,safe_relabel,&output);
#else  
    color_cluster(SR_domain,label_data,0,no_relabel,&output);
#endif
    label_pixels(input,SR_domain,label,true,&label_data);
    
#ifdef LABEL_ON_THE_FLY 
    color_cluster(SR_domain,label_data,size_threshold,safe_relabel,&output); 
#else
    label_list_type relabel;
    init_relabel(&relabel);

    #ifdef OUTPUT_HIERARCHY
      color_cluster(SR_domain,label_data,size_threshold,relabel,&output);
    #endif
    prune_persistence(SR_domain,pers_thr,&relabel);
  
    #ifndef OUTPUT_HIERARCHY  
      color_cluster(SR_domain,label_data,size_threshold,relabel,&output);
    #endif
#endif
   
#ifdef IMAGE_MODE
    cv::Mat* outImg = static_cast<cv::Mat*>(_imgOut);
    cv::Mat fData[3];
    fData[0] = cv::Mat::zeros(height,width,CV_8UC1);
    fData[1] = cv::Mat::zeros(height,width,CV_8UC1);
    fData[2] = cv::Mat::zeros(height,width,CV_8UC1);
    
    for(size_type x = 0;x < width;x++)
    for(size_type y = 0;y < height;y++)
    {
      char r,g,b;
      output(x,y).get_RGB(&r,&g,&b);
      fData[0].at<unsigned char>(y,x) = static_cast<unsigned char>(b);
      fData[1].at<unsigned char>(y,x) = static_cast<unsigned char>(g);
      fData[2].at<unsigned char>(y,x) = static_cast<unsigned char>(r);
    } 
    
    // Merge the channels
    cv::merge(fData,3,*outImg);
#endif
    return 1;
  }//end of function
}