/*! \file

\verbatim

Copyright (c) 2004, Sylvain Paris and Francois Sillion
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    
    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of ARTIS, GRAVIR-IMAG nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

\endverbatim


 *  This file contains code made by Sylvain Paris under supervision of
 * Fran�ois Sillion for his PhD work with <a
 * href="http://www-artis.imag.fr">ARTIS project</a>. ARTIS is a
 * research project in the GRAVIR/IMAG laboratory, a joint unit of
 * CNRS, INPG, INRIA and UJF.
 *
 *  Use <a href="http://www.stack.nl/~dimitri/doxygen/">Doxygen</a>
 * with DISTRIBUTE_GROUP_DOC option to produce an nice html
 * documentation.
 *
 *  This file defines the functions to read and write data in a binary
 * form into a STL stream. Bit order (little or big endian) is controled
 * through the REVERSE_BIT_ORDER variable.
 * Here is a simple example.
  \code

  // WRITE
  
  float f = 0.4f;

  std::ofstream out("my_file");

  IO_tools::write_4_bytes(f,out);

  out.flush();
  out.close();

  // READ

  float g;

  std::ifstream in("my_file");
  
  IO_tools::read_4_bytes(&f,in);

  in.close();
  
  \endcode
 *
 * Basic functions are also implemented to find tokens in a stream.
 */


#ifndef __IO_TOOLS__
#define __IO_TOOLS__


#include <iostream>

#include "msg_stream.h"


namespace IO_tools{

//! Write a 4-byte variable into a stream.
  template<typename T> inline void write_4_bytes(const T& v,
						 std::ostream& out);

//! Write a 2-byte variable into a stream.
  template<typename T> inline void write_2_bytes(const T& v,
						 std::ostream& out);

//! Read a 4-byte variable from a stream.
  template<typename T> inline void read_4_bytes(T* result,
						std::istream& in);

//! Read a 2-byte variable from a stream.
  template<typename T> inline void read_2_bytes(T* result,
						std::istream& in);

//! If REVERSE_BIT_ORDER is defined, inverse the bit order.
  template<typename T>
  inline void convert_bit_order(const T& var,
				T* result);

//! Read from the stream until the next corresponding character.
  inline void go_to_next(const char c,
			 std::istream& in);

//! Read from the stream until the end of the given token.
  inline void go_to_next_token(const std::string token,
			       std::istream& in);

//! Read from the stream until the next character contained within a string.
  inline char go_to_next_of_list(const std::string token_list,
				 std::istream& in);
 


/*
  ###########################################################
  ###########################################################
  ###########################################################
  ##############                               ##############
  ##############  I M P L E M E N T A T I O N  ##############
  ##############                               ##############
  ###########################################################
  ###########################################################
  ###########################################################
*/



  template<typename T>
  inline void convert_bit_order(const T& var,
				T* result){
    
#ifdef REVERSE_BIT_ORDER
    int i;
    char* in  = reinterpret_cast<char*>(&const_cast<T&>(var));
    char* out = reinterpret_cast<char*>(result);
#endif

    int size = sizeof(T);
    switch (size){
    case 1:
      *result = var;
      return;
    case 2:
    case 4:
    
#ifdef REVERSE_BIT_ORDER
      for(i=0;i<size;i++){
	out[i] = in[size-1-i];
      }
#else
      *result = var;
#endif
    
      return;
      
    default:
      Message::error<<"convert_bit_order: variable size("
		    <<size<<") not handled"<<Message::done;
      break;
    }
  }

  template<typename T> inline void write_4_bytes(const T& v,
						 std::ostream& out){
#ifdef DEBUG
    if (sizeof(T)!=4) {
      Message::error<<"write_4_bytes: the variable is not coded with 4 bytes"
		    <<Message::done;
    }
#endif

    T buf;
    convert_bit_order(v,&buf);
    char* byte = (char*)(&buf);
    for(int i=0;i<4;i++){
      out.put(byte[i]);
    }
  }


  template<typename T> inline void write_2_bytes(const T& v,
						 std::ostream& out){
#ifdef DEBUG
    if (sizeof(T)!=2) {
      Message::error<<"write_2_bytes: the variable is not coded with 2 bytes"
		    <<Message::done;
    }
#endif

    T buf;
    convert_bit_order(v,&buf);
    char* byte = (char*)(&buf);
    for(int i=0;i<2;i++){
      out.put(byte[i]);
    }
  }

  template<typename T> inline void read_4_bytes(T* result,
						std::istream& in){
#ifdef DEBUG
    if (sizeof(T)!=4) {
      Message::error<<"read_4_bytes: the variable is not coded with 4 bytes"
		    <<Message::done;
    }
#endif

    T buf;
    char* byte = (char*)(&buf);

    for(int i=0;i<4;i++){
      in.get(byte[i]);
    }

    convert_bit_order(buf,result);
  }



  
  template<typename T> inline void read_2_bytes(T* result,
						std::istream& in){
#ifdef DEBUG
    if (sizeof(T)!=2) {
      Message::error<<"read_2_bytes: the variable is not coded with 2 bytes"
		    <<Message::done;
    }
#endif

    T buf;
    char* byte = (char*)(&buf);

    for(int i=0;i<2;i++){
      in.get(byte[i]);
    }

    convert_bit_order(buf,result);
  }



  
  void go_to_next(const char c,
		  std::istream& in){
    char ch;
    
    do{ 
      in.get(ch);
      if (!in.good()) {
	Message::error<<"go_to_next: error in input stream\nwas looking for '"
		      <<c<<"'"
		      <<Message::done;
      }
    }
    while (ch!=c);
  }

  /*!  Be careful, it is a basic version that does not recognize BAR
    in BBAR i.e. there is no finite automata. It is just enough to parse simplistic files.
  */
  void go_to_next_token(const std::string token,
			std::istream& in){

    const unsigned int token_size = token.size();
    
    if (token_size<1) {
      Message::error<<"go_to_next_token: token size too small ("
		    <<token_size<<")"<<Message::done;
    }

    char ch;
    unsigned int size_found = 0;
    
    while (size_found<token_size) {   
      in.get(ch);
    
      if (!in.good()) {
	Message::error<<"go_to_next_token: error in input stream"
		      <<Message::done;
      }
    
      if (ch==token[size_found]) {
	size_found++;
      }
      else {
	size_found=0;
      }
    }
  }

  
  char go_to_next_of_list(const std::string token_list,
			  std::istream& in){
    char ch;
    bool found;

    const unsigned int list_size = token_list.size();
    
    do{ 
      in.get(ch);
      if (!in.good()) {
	Message::error<<"go_to_next: error in input stream"
		      <<Message::done;
      }

      found = false;
      for(unsigned int i=0;i<list_size;i++){
	found = found||(ch==token_list[i]);
      }
    }
    while (!found);
     
    return ch;
  }



}

#endif
