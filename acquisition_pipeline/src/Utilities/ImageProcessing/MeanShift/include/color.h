/*!
  \file

\verbatim

Copyright (c) 2004, Sylvain Paris and Francois Sillion
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    
    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of ARTIS, GRAVIR-IMAG nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

\endverbatim

   These files contain code made by Sylvain Paris under supervision of
  Fran�ois Sillion for his PhD work with <a
  href="http://artis.imag.fr">ARTIS project</a>. ARTIS is a
  research project in the GRAVIR/IMAG laboratory, a joint unit of
  CNRS, INPG, INRIA and UJF.
 
   Source code can be downloaded from
  <a href="http://artis.imag.fr/Members/Sylvain.Paris/index.html#code">Sylvain Paris' page</a>. <!--'-->
  The class Basic_color represents a color in 3D space. It is optimized
  for operations within this space (mean, distance,...). It is not adapted for
  reguler queries to the RGB component.

  The class must be instanciated with a \e trait that provides two functions:
  \e from_RGB_to_cartesian() and \e from_cartesian_to_RGB() and a typedef for
  the type of the components within the color space: \e component_type.

  Usual typedefs are defined.
  
 */

#ifndef __COLOR__
#define __COLOR__

#include <cmath>

#include <algorithm>
#include <iterator>
#include <list>
#include <vector>

#ifndef WITHOUT_LIMITS
#include <limits>
#endif

#include "io_tools.h"
#include "msg_stream.h"

#ifndef WITHOUT_STATIC_CONST  // in case of trouble with gcc 2.96
#define CONST_STATIC const
#else
#define CONST_STATIC
#endif

/*

  ###############
  # typedef ... #
  ###############
  
 */

template<typename RGB_converter> class Basic_color;

template<typename Real> class RGB_color;
template<typename Real> class HSV_color;
template<typename Real> class XYZ_color;
template<typename Real> class Luv_color;
template<typename Real> class Lab_color;
template<typename Real> class Grey_level_color;

template<typename Real> class RGB_converter_to_RGB;
template<typename Real> class RGB_converter_to_HSV;
template<typename Real> class RGB_converter_to_XYZ;
template<typename Real> class RGB_converter_to_Luv;
template<typename Real> class RGB_converter_to_Lab;
template<typename Real> class RGB_converter_to_grey_level;

typedef RGB_color<float>         RGB_f_color;
typedef RGB_color<double>        RGB_d_color;

typedef HSV_color<float>         HSV_f_color;
typedef HSV_color<double>        HSV_d_color;

typedef XYZ_color<float>         XYZ_f_color;
typedef XYZ_color<double>        XYZ_d_color;

typedef Luv_color<float>         Luv_f_color;
typedef Luv_color<double>        Luv_d_color;

typedef Lab_color<float>         Lab_f_color;
typedef Lab_color<double>        Lab_d_color;

typedef Grey_level_color<float>  Grey_level_f_color;
typedef Grey_level_color<double> Grey_level_d_color;


template<typename RGB_converter>
std::ostream& operator<<(std::ostream& s,
			 const Basic_color<RGB_converter>& c);

/*

  #####################
  # class Basic_color #
  #####################

*/

//! Class representing a color. Optimized for operations within the
//! color space (mean, distance,...)
/*!
  An object with the following functions must provided:
  - void from_RGB_to_cartesian(const double,const double,const double b,Real* const,Real* const, Real* const)
  - static void from_cartesian_to_RGB(const Real,const Real,const Real,double* const,double* const,double* const)

  The coordinate type must also be given (float by default).
 */
template<typename RGB_converter>
class Basic_color{

public:

  typedef typename RGB_converter::component_type component_type;
  typedef component_type                         distance_type;

  typedef RGB_converter  converter_type;
  
  Basic_color();
  
  Basic_color(const char r,  const char g,  const char b);
  Basic_color(const float r, const float g, const float b);
  Basic_color(const double r,const double g,const double b);
  
  Basic_color(const Basic_color<RGB_converter>& c);
  
  inline Basic_color<RGB_converter>&
  operator=(const Basic_color<RGB_converter>& c);

  inline bool operator==(const Basic_color<RGB_converter>& c) const;
  inline bool operator!=(const Basic_color<RGB_converter>& c) const;
  
  //@{
  //! Return the RGB components.
  
  inline void get_RGB(char* const r,  char* const g,  char* const b) const;
  inline void get_RGB(float* const r, float* const g, float* const b) const;
  inline void get_RGB(double* const r,double* const g,double* const b) const;  
  //@}

  //@{
  //! Return the 3D position in the color space.
  
  inline void get_space_position(component_type* const x_space,
				 component_type* const y_space,
				 component_type* const z_space) const;

  template<typename Space_vector>
  inline void get_space_position(Space_vector* const space_vector) const;
  //@}

  
  //@{
  //! Change the 3D position in the color space.
  
  inline void set_space_position(const component_type x_space,
				 const component_type y_space,
				 const component_type z_space);

  template<typename Space_vector>
  inline void set_space_position(const Space_vector& space_vector);
  //@}

  //! Test if the color is not initialzed.
  inline bool is_not_a_color() const;

  //! Write the color in a binary format to a stream.
  inline void write_to_bytes(std::ostream& out) const;

  //! Read the color in a binary format from a stream.
  inline void read_from_bytes(std::istream& in);

  //! Compute the square distance in the color space.
  inline static distance_type
  square_distance(const Basic_color<RGB_converter>& c1,
		  const Basic_color<RGB_converter>& c2);

  //! Compute the distance in the color space.
  inline static distance_type
  distance(const Basic_color<RGB_converter>& c1,
	   const Basic_color<RGB_converter>& c2);
  
  //@{
  //! Compute the mean color of a color set.
  
  template<typename Basic_color_iterator>
  static void mean(Basic_color_iterator first,
		   Basic_color_iterator last,
		   Basic_color<RGB_converter>* const result);

  template<typename Basic_color_iterator,typename Weight_iterator>
  static void mean(Basic_color_iterator first_clr,
		   Basic_color_iterator last_clr,
		   Weight_iterator first_w,
		   Weight_iterator last_w,
		   Basic_color<RGB_converter>* const result);
  //@}

  //@{
  //! Compute a robust mean of a color set. The colors whose distance to the mean is over the standard deviation are discarded.

  template<typename Basic_color_iterator>
  static void robust_mean(Basic_color_iterator first,
			  Basic_color_iterator last,
			  Basic_color<RGB_converter>* const result,
			  const unsigned int n_iter = 1);

  template<typename Basic_color_iterator,typename Weight_iterator>
  static void robust_mean(Basic_color_iterator first_clr,
			  Basic_color_iterator last_clr,
			  Weight_iterator first_w,
			  Weight_iterator last_w,
			  Basic_color<RGB_converter>* const result,
			  const unsigned int n_iter = 1);
  //@}

  //! Display the main colors noth in RGB and the color space coordinate system.
  //! Mainly a debug tool.
  static void main_conversion_output(std::ostream& out = std::cout) ;

  //@{
  //! Compute the variance of color set.
  
  template<typename Basic_color_iterator>
  inline static distance_type
  square_deviation(const Basic_color_iterator first,
		   const Basic_color_iterator last);

  template<typename Basic_color_iterator>
  static distance_type
  square_deviation(const Basic_color_iterator first,
		   const Basic_color_iterator last,
		   Basic_color<RGB_converter>* const mean_color);
  //@}

  //@{
  //! Predefined color.

  static const Basic_color<RGB_converter> black;
  static const Basic_color<RGB_converter> grey;
  static const Basic_color<RGB_converter> white;
  
  static const Basic_color<RGB_converter> red;
  static const Basic_color<RGB_converter> green;
  static const Basic_color<RGB_converter> blue;

  static const Basic_color<RGB_converter> cyan;
  static const Basic_color<RGB_converter> magenta;
  static const Basic_color<RGB_converter> yellow;
  //@}

  static void cycle(const double alpha,
		    Basic_color<RGB_converter>* const result,
		    const Basic_color<RGB_converter>& clr_0 = red,
		    const Basic_color<RGB_converter>& clr_1 = blue,
		    const Basic_color<RGB_converter>& clr_2 = yellow,
		    const Basic_color<RGB_converter>& clr_3 = magenta);
  
  //! "ASCII" output
  friend std::ostream&
  operator<<<RGB_converter>(std::ostream& s,
			    const Basic_color<RGB_converter>& c);

private:
  //@{
  //! Coordinate in the color space.
  
  component_type x,y,z;
  //@}
};


/*

  ###################
  # class RGB_color #
  ###################

 */


template<typename Real>
class RGB_color
  :public Basic_color<RGB_converter_to_RGB<Real> >{

public:
  
  RGB_color()
    :Basic_color<RGB_converter_to_RGB<Real> >(){}
  
  RGB_color(const char r,  const char g,  const char b)
    :Basic_color<RGB_converter_to_RGB<Real> >(r,g,b){}

  RGB_color(const float r, const float g, const float b)
    :Basic_color<RGB_converter_to_RGB<Real> >(r,g,b){}

  RGB_color(const double r,const double g,const double b)
    :Basic_color<RGB_converter_to_RGB<Real> >(r,g,b){}
  
  RGB_color(const Basic_color<RGB_converter_to_RGB<Real> >& c)
    :Basic_color<RGB_converter_to_RGB<Real> >(c){}

};






/*

  ###################
  # class HSV_color #
  ###################

 */


template<typename Real>
class HSV_color
  :public Basic_color<RGB_converter_to_HSV<Real> >{

public:
  
  HSV_color()
    :Basic_color<RGB_converter_to_HSV<Real> >(){}
  
  HSV_color(const char r,  const char g,  const char b)
    :Basic_color<RGB_converter_to_HSV<Real> >(r,g,b){}
  
  HSV_color(const float r, const float g, const float b)
    :Basic_color<RGB_converter_to_HSV<Real> >(r,g,b){}
  
  HSV_color(const double r,const double g,const double b)
    :Basic_color<RGB_converter_to_HSV<Real> >(r,g,b){}
  
  HSV_color(const Basic_color<RGB_converter_to_HSV<Real> >& c)
    :Basic_color<RGB_converter_to_HSV<Real> >(c){}

};



/*

  ###################
  # class XYZ_color #
  ###################

 */


template<typename Real>
class XYZ_color
  :public Basic_color<RGB_converter_to_XYZ<Real> >{

public:
  
  XYZ_color()
    :Basic_color<RGB_converter_to_XYZ<Real> >(){}
  
  XYZ_color(const char r,  const char g,  const char b)
    :Basic_color<RGB_converter_to_XYZ<Real> >(r,g,b){}
  
  XYZ_color(const float r, const float g, const float b)
    :Basic_color<RGB_converter_to_XYZ<Real> >(r,g,b){}
  
  XYZ_color(const double r,const double g,const double b)
    :Basic_color<RGB_converter_to_XYZ<Real> >(r,g,b){}
  
  XYZ_color(const Basic_color<RGB_converter_to_XYZ<Real> >& c)
    :Basic_color<RGB_converter_to_XYZ<Real> >(c){}

};





/*

  ###################
  # class Luv_color #
  ###################

 */


template<typename Real>
class Luv_color
  :public Basic_color<RGB_converter_to_Luv<Real> >{

public:
  
  Luv_color()
    :Basic_color<RGB_converter_to_Luv<Real> >(){}
  
  Luv_color(const char r,  const char g,  const char b)
    :Basic_color<RGB_converter_to_Luv<Real> >(r,g,b){}
  
  Luv_color(const float r, const float g, const float b)
    :Basic_color<RGB_converter_to_Luv<Real> >(r,g,b){}
  
  Luv_color(const double r,const double g,const double b)
    :Basic_color<RGB_converter_to_Luv<Real> >(r,g,b){}
  
  Luv_color(const Basic_color<RGB_converter_to_Luv<Real> >& c)
    :Basic_color<RGB_converter_to_Luv<Real> >(c){}

};





/*

  ###################
  # class Lab_color #
  ###################

 */


template<typename Real>
class Lab_color
  :public Basic_color<RGB_converter_to_Lab<Real> >{

public:
  
  Lab_color()
    :Basic_color<RGB_converter_to_Lab<Real> >(){}
  
  Lab_color(const char r,  const char g,  const char b)
    :Basic_color<RGB_converter_to_Lab<Real> >(r,g,b){}
  
  Lab_color(const float r, const float g, const float b)
    :Basic_color<RGB_converter_to_Lab<Real> >(r,g,b){}
  
  Lab_color(const double r,const double g,const double b)
    :Basic_color<RGB_converter_to_Lab<Real> >(r,g,b){}
  
  Lab_color(const Basic_color<RGB_converter_to_Lab<Real> >& c)
    :Basic_color<RGB_converter_to_Lab<Real> >(c){}

};










/*

  ##########################
  # class Grey_level_color #
  ##########################

 */


template<typename Real>
class Grey_level_color
  :public Basic_color<RGB_converter_to_grey_level<Real> >{

public:
  
  Grey_level_color()
    :Basic_color<RGB_converter_to_grey_level<Real> >(){}

  Grey_level_color(const char c)
    :Basic_color<RGB_converter_to_grey_level<Real> >(c,c,c){}

  Grey_level_color(const float f)
    :Basic_color<RGB_converter_to_grey_level<Real> >(f,f,f){}

  Grey_level_color(const double d)
    :Basic_color<RGB_converter_to_grey_level<Real> >(d,d,d){}
  
  Grey_level_color(const char r,  const char g,  const char b)
    :Basic_color<RGB_converter_to_grey_level<Real> >(r,g,b){}
  
  Grey_level_color(const float r, const float g, const float b)
    :Basic_color<RGB_converter_to_grey_level<Real> >(r,g,b){}
  
  Grey_level_color(const double r,const double g,const double b)
    :Basic_color<RGB_converter_to_grey_level<Real> >(r,g,b){}
  
  Grey_level_color(const Basic_color<RGB_converter_to_grey_level<Real> >& c)
    :Basic_color<RGB_converter_to_grey_level<Real> >(c){}

};


/*

  ##############################
  # class RGB_converter_to_HSV #
  ##############################

*/


//! Class to create a HSV color.
/*!
  The template parameter is type of the coordinates.
*/
template<typename Real>
class RGB_converter_to_HSV{
  
public:

  typedef Real component_type;
  
  inline static void from_RGB_to_cartesian(const double r,
					   const double g,
					   const double b,
					   component_type* const x,
					   component_type* const y,
					   component_type* const z);

  inline static void from_cartesian_to_RGB(const component_type x,
					   const component_type y,
					   const component_type z,
					   double* const r,
					   double* const g,
					   double* const b);

private:
  inline static void RGB_to_HSV(const double r,const double g,const double b,
				double* const h,double* const s,double* const v);
  
  inline static void HSV_to_RGB(const double h,const double s,const double v,
				double* const r,double* const g,double* const b);
};




/*

  ##############################
  # class RGB_converter_to_RGB #
  ##############################

*/


//! Class to create a RGB color.
/*!
  The template parameter is type of the coordinates.
*/
template<typename Real>
class RGB_converter_to_RGB{
  
public:

  //! Type des composantes des couleurs.
  typedef Real component_type;
  
  inline static void from_RGB_to_cartesian(const double r,
					   const double g,
					   const double b,
					   component_type* const x,
					   component_type* const y,
					   component_type* const z);

  inline static void from_cartesian_to_RGB(const component_type x,
					   const component_type y,
					   const component_type z,
					   double* const r,
					   double* const g,
					   double* const b);
};



//! Class to create a XYZ color.
/*!
  The template parameter is type of the coordinates.
*/
template<typename Real>
class RGB_converter_to_XYZ{
  
public:

  typedef Real component_type;
  
  inline static void from_RGB_to_cartesian(const double r,
					   const double g,
					   const double b,
					   component_type* const x,
					   component_type* const y,
					   component_type* const z);

  inline static void from_cartesian_to_RGB(const component_type x,
					   const component_type y,
					   const component_type z,
					   double* const r,
					   double* const g,
					   double* const b);
};



//! Class to create a Luv color.
/*!
  The template parameter is type of the coordinates.
*/
template<typename Real>
class RGB_converter_to_Luv{
  
public:

  typedef Real component_type;
  
  inline static void from_RGB_to_cartesian(const double r,
					   const double g,
					   const double b,
					   component_type* const x,
					   component_type* const y,
					   component_type* const z);

  inline static void from_cartesian_to_RGB(const component_type x,
					   const component_type y,
					   const component_type z,
					   double* const r,
					   double* const g,
					   double* const b);
private:
  //@{
  
  static const double Xn;
  static const double Yn;
  static const double Zn;
  //@}

  //@{
  //! Conversion function.
  
  static double L2Y(const double l);
  static double Y2L(const double y);
  //@}
};






//! Class to create a Lab color.
/*!
  The template parameter is type of the coordinates.
*/
template<typename Real>
class RGB_converter_to_Lab{
  
public:

  typedef Real component_type;
  
  inline static void from_RGB_to_cartesian(const double r,
					   const double g,
					   const double b,
					   component_type* const x,
					   component_type* const y,
					   component_type* const z);

  inline static void from_cartesian_to_RGB(const component_type x,
					   const component_type y,
					   const component_type z,
					   double* const r,
					   double* const g,
					   double* const b);
private:
  //@{
  
  static const double Xn;
  static const double Yn;
  static const double Zn;
  //@}
};






//! Class to create a grey level "color".
/*!
  Only \e x is used.
*/
template<typename Real>
class RGB_converter_to_grey_level{
  
public:

  typedef Real component_type;
  
  inline static void from_RGB_to_cartesian(const double r,
					   const double g,
					   const double b,
					   component_type* const x,
					   component_type* const y,
					   component_type* const z);

  inline static void from_cartesian_to_RGB(const component_type x,
					   const component_type y,
					   const component_type z,
					   double* const r,
					   double* const g,
					   double* const b);

  inline static void set_min_value(const double value);
  inline static void set_max_value(const double value);
};




/*

  #######################
  # class Color_to_grey #
  #######################

 */

//! Function object that converts a color in a grey level
/*! \e Grey must be in : char, float ou double. \e Color must be a \e
  Basic_color. */
template<typename Color,typename Grey>
class Color_to_grey{
public:
  Color_to_grey(){}

  Grey operator()(const Color& c){
    Grey r,g,b;
    c.get_RGB(&r,&g,&b);
    return (r+g+b)/3;
  }
};





/*

  #######################
  # class Grey_to_color #
  #######################

*/

//! Function object. Inverse of Color_to_grey.
template<typename Grey,typename Color>
class Grey_to_color{
public:
  Grey_to_color(){}

  inline Color operator()(const Grey g){ return Color(g,g,g); }
};




/*

  ########################
  # class Black_or_white #
  ########################
  
*/

//! Function object that returns black (false) or white (true) according to the parameter.
template<typename Color>
class Black_or_white{
public:
  Black_or_white(){}

  inline Color operator()(const bool b){ return (b?(Color(1.0,1.0,1.0)):
						 (Color(0.0,0.0,0.0))); }
};









/*
  
  #############################################
  #############################################
  #############################################
  ######                                 ######
  ######   I M P L E M E N T A T I O N   ######
  ######                                 ######
  #############################################
  #############################################
  #############################################
  
*/







/*

  #####################
  # class Basic_color #
  #####################

*/

#ifndef WITHOUT_LIMITS

template<typename RGB_converter>
Basic_color<RGB_converter>::
Basic_color()
  :x(std::numeric_limits<component_type>::signaling_NaN()),
   y(std::numeric_limits<component_type>::signaling_NaN()),
   z(std::numeric_limits<component_type>::signaling_NaN()){}

#else

template<typename RGB_converter>
Basic_color<RGB_converter>::
Basic_color()
  :x(),y(),z(){

}

#endif

template<typename RGB_converter>
Basic_color<RGB_converter>::
Basic_color(const char r,const char g,const char b){

  RGB_converter::from_RGB_to_cartesian(static_cast<double>(r)/255,
				       static_cast<double>(g)/255,
				       static_cast<double>(b)/255,
				       &x,&y,&z);
}


template<typename RGB_converter>
Basic_color<RGB_converter>::
Basic_color(const float r,const float g,const float b){

  RGB_converter::from_RGB_to_cartesian(static_cast<double>(r),
				       static_cast<double>(g),
				       static_cast<double>(b),
				       &x,&y,&z);
}


template<typename RGB_converter>
Basic_color<RGB_converter>::
Basic_color(const double r,const double g,const double b){

  RGB_converter::from_RGB_to_cartesian(r,g,b,&x,&y,&z);
}


template<typename RGB_converter>
Basic_color<RGB_converter>::
Basic_color(const Basic_color<RGB_converter>& c)
  :x(c.x),y(c.y),z(c.z){
}


template<typename RGB_converter>
Basic_color<RGB_converter>&
Basic_color<RGB_converter>::
operator=(const Basic_color<RGB_converter>& c){

  if (this!=&c){
    x = c.x;
    y = c.y;
    z = c.z;
  }
  
  return *this;
}


template<typename RGB_converter>
bool
Basic_color<RGB_converter>::
operator==(const Basic_color<RGB_converter>& c) const{
  
  return ((x==c.x)&&(y==c.y)&&(z==c.z));
}


template<typename RGB_converter>
bool
Basic_color<RGB_converter>::
operator!=(const Basic_color<RGB_converter>& c) const{
  
  return ((x!=c.x)||(y!=c.y)||(z!=c.z));
}


template<typename RGB_converter>
void
Basic_color<RGB_converter>::
get_RGB(char* const r,char* const g,char* const b) const{

  double R,G,B;
  
  RGB_converter::from_cartesian_to_RGB(x,y,z,&R,&G,&B);

  *r = static_cast<char>(rint(R*255));
  *g = static_cast<char>(rint(G*255));
  *b = static_cast<char>(rint(B*255));  
}


template<typename RGB_converter>
void
Basic_color<RGB_converter>::
get_RGB(float* const r,float* const g,float* const b) const{

  double R,G,B;
  
  RGB_converter::from_cartesian_to_RGB(x,y,z,&R,&G,&B);

  *r = static_cast<float>(R);
  *g = static_cast<float>(G);
  *b = static_cast<float>(B);  
}


template<typename RGB_converter>
void
Basic_color<RGB_converter>::
get_RGB(double* const r,double* const g,double* const b) const{
  RGB_converter::from_cartesian_to_RGB(x,y,z,r,g,b);
}


/*!
  Be careful: (x,y,z) does not always correspond to the 3 parameters of the color in HSV.
*/
template<typename RGB_converter>
void
Basic_color<RGB_converter>::
get_space_position(component_type* const x_space,
		   component_type* const y_space,
		   component_type* const z_space) const{
  *x_space = x;
  *y_space = y;
  *z_space = z;
}


/*!
  Be careful : (x,y,z) do not always correspond to the 3 classical
  color components.
  
  Space_vector must implement a operator [] and a typedef value_type.
*/
template<typename RGB_converter>
template<typename Space_vector>
void
Basic_color<RGB_converter>::
get_space_position(Space_vector* const space_vector) const{
  
  typedef typename Space_vector::value_type type;
  
  (*space_vector)[0] = static_cast<type>(x);
  (*space_vector)[1] = static_cast<type>(y);
  (*space_vector)[2] = static_cast<type>(z);
}


template<typename RGB_converter>
void
Basic_color<RGB_converter>::
set_space_position(const component_type x_space,
		   const component_type y_space,
		   const component_type z_space){
  x = x_space;
  y = y_space;
  z = z_space;
}


/*!
  See get_space_position().
 */
template<typename RGB_converter>
template<typename Space_vector>
void
Basic_color<RGB_converter>::
set_space_position(const Space_vector& space_vector){
  x = static_cast<component_type>(space_vector[0]);
  y = static_cast<component_type>(space_vector[1]);
  z = static_cast<component_type>(space_vector[2]);
}


template<typename RGB_converter>
bool
Basic_color<RGB_converter>::
is_not_a_color() const{
  return (is_signaling_NaN(x)||is_signaling_NaN(y)||is_signaling_NaN(z));  
}


template<typename RGB_converter>
void
Basic_color<RGB_converter>::
write_to_bytes(std::ostream& out) const{

  using namespace IO_tools;
  
  int size = sizeof(component_type);

  switch (size){
    
  case 2:
    write_2_bytes(x,out); 
    write_2_bytes(y,out);
    write_2_bytes(z,out);
    return;

  case 4:
    write_4_bytes(x,out); 
    write_4_bytes(y,out);
    write_4_bytes(z,out);
    return;

  default:
    Message::error<<"Basic_color::write_to_bytes : sizeof(component_type) = "<<size
		  <<" non implemented"<<Message::done;
  }
  
  return;
}


template<typename RGB_converter>
void
Basic_color<RGB_converter>::
read_from_bytes(std::istream& in){

  using namespace IO_tools;

  int size = sizeof(component_type);

  switch (size){
    
  case 2:
    read_2_bytes(&x,in); 
    read_2_bytes(&y,in);
    read_2_bytes(&z,in);
    return;

  case 4:
    read_4_bytes(&x,in); 
    read_4_bytes(&y,in);
    read_4_bytes(&z,in);
    return;

  default:
    Message::error<<"Basic_color::read_to_bytes : sizeof(component_type) = "<<size
		  <<" non implemenrted"<<Message::done;
  }
}


template<typename RGB_converter>
typename Basic_color<RGB_converter>::distance_type
Basic_color<RGB_converter>::
square_distance(const Basic_color<RGB_converter>& c1,
		const Basic_color<RGB_converter>& c2){

  const distance_type dx = c1.x - c2.x;
  const distance_type dy = c1.y - c2.y;
  const distance_type dz = c1.z - c2.z;

  return (dx*dx + dy*dy + dz*dz);
}


/*!
  Slower than square_distance(). Use sqrtf() to be improved for 'double'.
*/
template<typename RGB_converter>
typename Basic_color<RGB_converter>::distance_type
Basic_color<RGB_converter>::
distance(const Basic_color<RGB_converter>& c1,
	 const Basic_color<RGB_converter>& c2){
  return sqrtf(square_distance(c1,c2));
}


template<typename RGB_converter>
template<typename Basic_color_iterator>
void
Basic_color<RGB_converter>::
mean(Basic_color_iterator first,
     Basic_color_iterator last,
     Basic_color<RGB_converter>* const result){

  unsigned int n     = 0;
  result->x = 0;
  result->y = 0;
  result->z = 0;  
  
  for(Basic_color_iterator c=first;c!=last;c++){
    result->x += c->x;
    result->y += c->y;
    result->z += c->z;
    n++;
  }

  result->x /= n;
  result->y /= n;
  result->z /= n;
}


template<typename RGB_converter>
template<typename Basic_color_iterator,typename Weight_iterator>
void
Basic_color<RGB_converter>::
mean(Basic_color_iterator first_clr,
     Basic_color_iterator last_clr,
     Weight_iterator      first_w,
     Weight_iterator      last_w,
     Basic_color<RGB_converter>* const result){

  component_type n = 0;
  result->x        = 0;
  result->y        = 0;
  result->z        = 0;
  
  Basic_color_iterator c;
  Weight_iterator w;
  for(c=first_clr,w=first_w;
      (c!=last_clr)&&(w!=last_w);
      c++,w++){
    result->x += c->x * (*w);
    result->y += c->y * (*w);
    result->z += c->z * (*w);
    n         += (*w);
  }

  if (n==0){
    Message::warning<<"Basic_color::mean : the weight sum is null\n"<<Message::done;
    *result = Basic_color<RGB_converter>();
  }
  else{
    result->x /= n;
    result->y /= n;
    result->z /= n;
  }
}


template<typename RGB_converter>
template<typename Basic_color_iterator>
void
Basic_color<RGB_converter>::
robust_mean(Basic_color_iterator first,
	    Basic_color_iterator last,
	    Basic_color<RGB_converter>* const result,
	    const unsigned int n_iter){
  
  mean(first,last,result);

  if (n_iter==0){
    return;
  }
  
  int n             = 0;
  distance_type sum = 0;

  std::list<distance_type> dist;

  // Compute the variamce.
  
  for(Basic_color_iterator c=first;c!=last;c++){
    distance_type sq = square_distance(*c,*result);
    dist.push_back(sq);
    sum += sq;
    n++;
  }

  distance_type sq_dev;

  if (n>0) {
    sq_dev = (sum/n);
  }

#ifndef WITHOUT_LIMITS
  else{
    sq_dev = std::numeric_limits<distance_type>::signaling_NaN();
    Message::warning<<"no color to compute the variance"<<Message::done;
  }
#endif

  // Keep only the nearest ones.
  typedef typename std::iterator_traits<Basic_color_iterator>::value_type
    color_type;
  
  std::list<color_type> near_one;

  n = 0;
  Basic_color_iterator               c;
  typename std::list<distance_type>::iterator d;
  for(c=first,d=dist.begin();c!=last;c++,d++){
    if ((*d)<=sq_dev){
      near_one.push_back(*c);
      n++;
    }
  }

  
  robust_mean(near_one.begin(),near_one.end(),result,n_iter-1);
}


template<typename RGB_converter>
template<typename Basic_color_iterator,typename Weight_iterator>
void
Basic_color<RGB_converter>::
robust_mean(Basic_color_iterator first_clr,
		 Basic_color_iterator last_clr,
		 Weight_iterator first_w,
		 Weight_iterator last_w,
		 Basic_color<RGB_converter>* const result,
		 const unsigned int n_iter){

  mean(first_clr,last_clr,first_w,last_w,result);

  if (n_iter==0){
    return;
  }
  
  component_type n   = 0;
  distance_type  sum = 0;

  std::list<distance_type> dist;

  // Compute the variance.
  Basic_color_iterator c;
  Weight_iterator w;
  for(c=first_clr,w=first_w;
      (c!=last_clr)&&(w!=last_w);
      c++,w++){

    distance_type sq = square_distance(*c,*result);
    dist.push_back(sq);
    sum += (*w) * sq;
    n += *w;
  }

  distance_type sq_dev;

  if (n>0) {
    sq_dev = (sum/n);
  }

#ifndef WITHOUT_LIMITS
  else{
    sq_dev = std::numeric_limits<distance_type>::signaling_NaN();
    Message::warning<<"no color to compute the variance"<<Message::done;
  }
#endif

  // Keep only the nearest one.
  typedef typename std::iterator_traits<Basic_color_iterator>::value_type
    color_type;
  typedef typename std::iterator_traits<Weight_iterator>::value_type
    weight_type;
  
  std::list<color_type> near_clr;
  std::list<weight_type> near_w;

  n = 0;
  typename std::list<distance_type>::iterator d;
  for(c=first_clr, w=first_w, d=dist.begin();
      (c != last_clr) && (w != last_w);
      c++, w++, d++){
    
    if ((*d)<=sq_dev){
      near_clr.push_back(*c);
      near_w.push_back(*w);
      n++;
    }
  }

  
  robust_mean(near_clr.begin(),near_clr.end(),
	      near_w.begin(),near_w.end(),
	      result,n_iter-1);

}


template<typename RGB_converter>
template<typename Basic_color_iterator>
typename Basic_color<RGB_converter>::distance_type
Basic_color<RGB_converter>::
square_deviation(Basic_color_iterator first,
		 Basic_color_iterator last){

  Basic_color<RGB_converter> mean_color;

  return square_deviation(first,last,&mean_color);
}


template<typename RGB_converter>
template<typename Basic_color_iterator>
typename Basic_color<RGB_converter>::distance_type
Basic_color<RGB_converter>::
square_deviation(Basic_color_iterator first,
		 Basic_color_iterator last,
		 Basic_color<RGB_converter>* const mean_color){

  mean(first,last,mean_color);

  int n             = 0;
  distance_type sum = 0;
  
  for(Basic_color_iterator c=first;c!=last;c++){
    sum += square_distance(*c,*mean_color);
    n++;
  }

#ifndef WITHOUT_LIMITS
  return ((n>0)?(sum/n):(std::numeric_limits<distance_type>::signaling_NaN()));
#else
  return (sum/n);
#endif
}


template<typename RGB_converter>
void
Basic_color<RGB_converter>::
main_conversion_output(std::ostream& out){

  Basic_color<RGB_converter> clr;
  component_type r,g,b;

  clr = white;
  clr.get_RGB(&r,&g,&b);
  out<<"r: "<<r<<"\tg: "<<g<<"\tb: "<<b<<"\n"
     <<"->\tx: "<<clr.x<<"\ty: "<<clr.y<<"\tz: "<<clr.z<<"\n"<<std::endl;

  clr = grey;
  clr.get_RGB(&r,&g,&b);
  out<<"r: "<<r<<"\tg: "<<g<<"\tb: "<<b<<"\n"
     <<"->\tx: "<<clr.x<<"\ty: "<<clr.y<<"\tz: "<<clr.z<<"\n"<<std::endl;

  clr = black;
  clr.get_RGB(&r,&g,&b);
  out<<"r: "<<r<<"\tg: "<<g<<"\tb: "<<b<<"\n"
     <<"->\tx: "<<clr.x<<"\ty: "<<clr.y<<"\tz: "<<clr.z<<"\n"<<std::endl;

  clr = red;
  clr.get_RGB(&r,&g,&b);
  out<<"r: "<<r<<"\tg: "<<g<<"\tb: "<<b<<"\n"
     <<"->\tx: "<<clr.x<<"\ty: "<<clr.y<<"\tz: "<<clr.z<<"\n"<<std::endl;

  clr = green;
  clr.get_RGB(&r,&g,&b);
  out<<"r: "<<r<<"\tg: "<<g<<"\tb: "<<b<<"\n"
     <<"->\tx: "<<clr.x<<"\ty: "<<clr.y<<"\tz: "<<clr.z<<"\n"<<std::endl;
  
  clr = blue;
  clr.get_RGB(&r,&g,&b);
  out<<"r: "<<r<<"\tg: "<<g<<"\tb: "<<b<<"\n"
     <<"->\tx: "<<clr.x<<"\ty: "<<clr.y<<"\tz: "<<clr.z<<"\n"<<std::endl;
  
  clr = cyan;
  clr.get_RGB(&r,&g,&b);
  out<<"r: "<<r<<"\tg: "<<g<<"\tb: "<<b<<"\n"
     <<"->\tx: "<<clr.x<<"\ty: "<<clr.y<<"\tz: "<<clr.z<<"\n"<<std::endl;

  clr = magenta;
  clr.get_RGB(&r,&g,&b);
  out<<"r: "<<r<<"\tg: "<<g<<"\tb: "<<b<<"\n"
     <<"->\tx: "<<clr.x<<"\ty: "<<clr.y<<"\tz: "<<clr.z<<"\n"<<std::endl;
  
  clr = yellow;
  clr.get_RGB(&r,&g,&b);
  out<<"r: "<<r<<"\tg: "<<g<<"\tb: "<<b<<"\n"
     <<"->\tx: "<<clr.x<<"\ty: "<<clr.y<<"\tz: "<<clr.z<<"\n"<<std::endl;

  char one = 1;
  clr = Basic_color<RGB_converter>(one,one,one);
  clr.get_RGB(&r,&g,&b);
  out<<"r: "<<r<<"\tg: "<<g<<"\tb: "<<b<<"\n"
     <<"->\tx: "<<clr.x<<"\ty: "<<clr.y<<"\tz: "<<clr.z<<"\n"<<std::endl;

}


template<typename RGB_converter>
const Basic_color<RGB_converter>
Basic_color<RGB_converter>::white(1.0f, 1.0f, 1.0f);

template<typename RGB_converter>
const Basic_color<RGB_converter>
Basic_color<RGB_converter>::grey(0.5f, 0.5f, 0.5f);

template<typename RGB_converter>
const Basic_color<RGB_converter>
Basic_color<RGB_converter>::black(0.0f, 0.0f, 0.0f);

template<typename RGB_converter>
const Basic_color<RGB_converter>
Basic_color<RGB_converter>::red(1.0f, 0.0f, 0.0f);

template<typename RGB_converter>
const Basic_color<RGB_converter>
Basic_color<RGB_converter>::green(0.0f, 1.0f, 0.0f);

template<typename RGB_converter>
const Basic_color<RGB_converter>
Basic_color<RGB_converter>::blue(0.0f, 0.0f, 1.0f);

template<typename RGB_converter>
const Basic_color<RGB_converter>
Basic_color<RGB_converter>::cyan(0.0f, 1.0f, 1.0f);

template<typename RGB_converter>
const Basic_color<RGB_converter>
Basic_color<RGB_converter>::magenta(1.0f, 0.0f, 1.0f);

template<typename RGB_converter>
const Basic_color<RGB_converter>
Basic_color<RGB_converter>::yellow(1.0f, 1.0f, 0.0f);


template<typename RGB_converter>
void
Basic_color<RGB_converter>::
cycle(const double alpha,
      Basic_color<RGB_converter>* const result,
      const Basic_color<RGB_converter>& clr_0,
      const Basic_color<RGB_converter>& clr_1,
      const Basic_color<RGB_converter>& clr_2,
      const Basic_color<RGB_converter>& clr_3){

  const double a = alpha - floor(alpha/(2.0*M_PI))*2.0*M_PI;
  
  std::vector<Basic_color<RGB_converter> > color(2);
  std::vector<double>                      weight(2);


  if ((a<0) || (a>2.0*M_PI)){
    Message::error<<"Basic_color<RGB_converter>::cycle : valeur de a"
		  <<" non valable : "<<a<<Message::done;
  }
  
  if ((a>=0) && (a<=M_PI_2)){
    color[0]  = clr_0;
    weight[0] = M_PI_2 - a;
    
    color[1]  = clr_1;
    weight[1] = M_PI_2 - weight[0]; 
  }

  if ((a>M_PI_2) && (a<=M_PI)){
    color[0]  = clr_1;
    weight[0] = M_PI - a;
    color[1]  = clr_2;
    weight[1] = M_PI_2 - weight[0]; 
  }

  if ((a>M_PI) && (a<=1.5*M_PI)){
    color[0]  = clr_2;
    weight[0] = 1.5*M_PI - a;
    color[1]  = clr_3;
    weight[1] = M_PI_2 - weight[0]; 
  }

  if ((a>1.5*M_PI) && (a<=2.0*M_PI)){
    color[0]  = clr_3;
    weight[0] = 2*M_PI - a;
    color[1]  = clr_0;
    weight[1] = M_PI_2 - weight[0]; 
  }

  mean(color.begin(),color.end(),weight.begin(),weight.end(),result);
}


/*

  ##############################
  # class RGB_converter_to_HSV #
  ##############################

*/

template<typename Real>
void
RGB_converter_to_HSV<Real>::
from_RGB_to_cartesian(const double r,
		      const double g,
		      const double b,
		      component_type* const x,
		      component_type* const y,
		      component_type* const z){
  
  double h,s,v;
  RGB_to_HSV(r,g,b,&h,&s,&v);

#ifndef M_PI
  const double theta = 2 * 3.14159265358979323846 * h;
#else
  const double theta = 2 * M_PI * h;
#endif

  const float f = s*v;

  *x = static_cast<component_type>(f * cos(theta));
  *y = static_cast<component_type>(f * sin(theta));
  *z = static_cast<component_type>(v);
}


template<typename Real>
void
RGB_converter_to_HSV<Real>::
from_cartesian_to_RGB(const component_type x,
		      const component_type y,
		      const component_type z,
		      double* const r,
		      double* const g,
		      double* const b){

  const double X = static_cast<double>(x);
  const double Y = static_cast<double>(y);
  const double f = sqrt(X*X + Y*Y);
  const double v = static_cast<double>(z);
  const double s = (v>0.0)?(f/v):(0.0);

#ifndef M_PI
  double theta = atan2(Y,X) / (2 * 3.14159265358979323846);
#else
  double theta = atan2(Y,X) / (2 * M_PI);
#endif

  const double h = (theta>0) ? theta : (theta + 1);
  
  HSV_to_RGB(h,s,v,r,g,b);
}


template<typename Real>
void
RGB_converter_to_HSV<Real>::
RGB_to_HSV(const double r,  const double g,  const double b,
	   double* const h, double* const s, double* const v){

  const double min   = std::min(r,std::min(g,b));
  const double max   = std::max(r,std::max(g,b));
  const double delta = max - min;
  
  *v = max; 
    
  if (max!=0)
    *s = delta / max;    
  else {
    *s = 0;
    *h = 0;
    return;
  }
    
  if (delta==0) { 
    *s = 0;
    *h = 0;
    return;
  }

  if(r==max) {
      *h = ( g - b ) / delta;        // between yellow & magenta
  }
  else if( g == max ) {
    *h = 2 + ( b - r ) / delta;    // between cyan & yellow
  }
  else {
    *h = 4 + ( r - g ) / delta;    // between magenta & cyan
  }
    
  *h /= 6;    
  if(*h<0){
    *h += 1;
  }

}
  
template<typename Real>
void
RGB_converter_to_HSV<Real>::
HSV_to_RGB(const double h,const double s,const double v,
	   double* const r,double* const g,double* const b){
  
  if(s==0) { // achromatic (grey)
    *r = v;
    *g = v;
    *b = v;
    return;
  }
  
  const int i    = static_cast<int>(floor(6*h));
  const double f = 6*h - i;     
  const double p = v * ( 1 - s );
  const double q = v * ( 1 - s * f );
  const double t = v * ( 1 - s * ( 1 - f ) );
 
  switch(i) {
  case 6:
  case 0:
    *r = v;
    *g = t;
    *b = p;
    break;
    
  case 1:
    *r = q;
    *g = v;
    *b = p;
    break;
    
  case 2:
    *r = p;
    *g = v;
    *b = t;
    break;
    
  case 3:
    *r = p;
    *g = q;
    *b = v;
    break;
    
  case 4:
    *r = t;
    *g = p;
    *b = v;
    break;
    
  default:  
    *r = v;
    *g = p;
    *b = q;
    break;
  }   
}

//@}


/*

  ##############################
  # class RGB_converter_to_RGB #
  ##############################

*/


template<typename Real>
void
RGB_converter_to_RGB<Real>::
from_RGB_to_cartesian(const double r,
		      const double g,
		      const double b,
		      component_type* const x,
		      component_type* const y,
		      component_type* const z){
  
  *x = static_cast<component_type>(r);
  *y = static_cast<component_type>(g);
  *z = static_cast<component_type>(b);
}

template<typename Real>
void
RGB_converter_to_RGB<Real>::
from_cartesian_to_RGB(const component_type x,
		      const component_type y,
		      const component_type z,
		      double* const r,
		      double* const g,
		      double* const b){
  
  *r = static_cast<double>(x);
  *g = static_cast<double>(y);
  *b = static_cast<double>(z);
}



/*

  ##############################
  # class RGB_converter_to_XYZ #
  ##############################

*/

//! Assume CIE-RGB.
template<typename Real>
void
RGB_converter_to_XYZ<Real>::
from_RGB_to_cartesian(const double r,
		      const double g,
		      const double b,
		      component_type* const x,
		      component_type* const y,
		      component_type* const z){

  const double rg = std::pow(r,2.2);
  const double gg = std::pow(g,2.2);
  const double bg = std::pow(b,2.2);
  
  *x = static_cast<component_type>(rg*0.488718 + gg*0.310680  + bg*0.200602);
  *y = static_cast<component_type>(rg*0.176204 + gg*0.812985  + bg*0.0108109);
  *z = static_cast<component_type>(rg*0.0      + gg*0.0102048 + bg*0.989795);

//   *x = static_cast<component_type>(r*0.412453 + g*0.357580 + b*0.180423);
//   *y = static_cast<component_type>(r*0.212671 + g*0.715160 + b*0.072169);
//   *z = static_cast<component_type>(r*0.019334 + g*0.119193 + b*0.950227);
}


//! Assume CIE-RGB.
template<typename Real>
void
RGB_converter_to_XYZ<Real>::
from_cartesian_to_RGB(const component_type x,
		      const component_type y,
		      const component_type z,
		      double* const r,
		      double* const g,
		      double* const b){

//   const double R =
//     static_cast<double>(x*3.240479 + y*-1.537150 + z*-0.498535);
//   const double G =
//     static_cast<double>(x*-0.969256 + y*1.875992 + z*0.041556);
//   const double B =
//     static_cast<double>(x*0.055648 + y*-0.204043 + z*1.057311);

  const double R =
    static_cast<double>(x*2.37067    + y*-0.900040 + z*-0.470634);
  const double G =
    static_cast<double>(x*-0.513885  + y*1.42530   + z*0.0885814);
  const double B =
    static_cast<double>(x*0.00529818 + y*-0.0146949 + z*1.00940);

  *r = std::pow(std::max(0.0,R),1.0/2.2);
  *g = std::pow(std::max(0.0,G),1.0/2.2);
  *b = std::pow(std::max(0.0,B),1.0/2.2);
  /*
  if (R<0.0)      { *r = 0.0; }
  else if (R>1.0) { *r = 1.0; }
  else            { *r = std::pow(R,1.0/2.2); }

  if (G<0.0)      { *g = 0.0; }
  else if (G>1.0) { *g = 1.0; }
  else            { *g = std::pow(G,1.0/2.2); }

  if (B<0.0)      { *b = 0.0; }
  else if (B>1.0) { *b = 1.0; }
  else            { *b = std::pow(B,1.0/2.2); }
  */
}



/*

  ##############################
  # class RGB_converter_to_Luv #
  ##############################
*/


// Reference white is CIE E

template<typename Real>
const double RGB_converter_to_Luv<Real>::Xn = 1.0;
template<typename Real>
const double RGB_converter_to_Luv<Real>::Yn = 1.0;
template<typename Real>
const double RGB_converter_to_Luv<Real>::Zn = 1.0;

// template<typename Real>
// const double RGB_converter_to_Luv<Real>::Xn = 0.95045592705;
// template<typename Real>
// const double RGB_converter_to_Luv<Real>::Yn = 1.0;
// template<typename Real>
// const double RGB_converter_to_Luv<Real>::Zn = 1.0890577508;

template<typename Real>
double
RGB_converter_to_Luv<Real>::L2Y(const double l){

  return (l>8) ?
    Yn*pow((l+16)/116,3.0) :
    Yn*l/24389.0*27.0;
}

template<typename Real>
double
RGB_converter_to_Luv<Real>::Y2L(const double y){

  const double ratio = y/Yn;

  return (ratio>216.0/24389.0) ?
    116 * pow(ratio,1.0/3.0) - 16 :
    24389.0/27.0 * ratio;
}


template<typename Real>
void
RGB_converter_to_Luv<Real>::
from_RGB_to_cartesian(const double r,
		      const double g,
		      const double b,
		      component_type* const x,
		      component_type* const y,
		      component_type* const z){
  
//   const double X = r*0.412453 + g*0.357580 + b*0.180423;
//   const double Y = r*0.212671 + g*0.715160 + b*0.072169;
//   const double Z = r*0.019334 + g*0.119193 + b*0.950227;

  double X,Y,Z;

  RGB_converter_to_XYZ<double>::from_RGB_to_cartesian(r,g,b,&X,&Y,&Z);

  *x = static_cast<component_type>(Y2L(Y / Yn));
  const double tmp = Xn + 15 * Yn + 3 * Zn;
  const double unp = 4 * Xn / tmp;
  const double vnp = 9 * Yn / tmp;
  const double tmp2 = X + 15 * Y + 3 * Z;
  const double up = (X==0)? 0 : 4 * X / tmp2;
  const double vp = (Y==0)? 0 : 9 * Y / tmp2;
  *y = static_cast<component_type>(13 * (*x) * (up - unp));
  *z = static_cast<component_type>(13 * (*x) * (vp - vnp));
}


template<typename Real>
void
RGB_converter_to_Luv<Real>::
from_cartesian_to_RGB(const component_type x,
		      const component_type y,
		      const component_type z,
		      double* const r,
		      double* const g,
		      double* const b){
  
  const double Y = L2Y(static_cast<double>(x));
  const double tmp = Xn + 15 * Yn + 3 * Zn;
  const double unp = 4 * Xn / tmp;
  const double vnp = 9 * Yn / tmp;
  const double Q = ((x==0) ? 0
    : static_cast<double>(y) / (13 * static_cast<double>(x))) + unp;
  const double R = ((x==0) ? 0
    : static_cast<double>(z) / (13 * static_cast<double>(x))) + vnp;
  const double A = 3 * Y * (5 * R - 3);
  const double Z = (R == 0) ? 0 : (((Q - 4) * A - 15 * Q * R * Y) / (12 * R));
  const double X = (R == 0) ? 0 : (-(A / R + 3 * Z));


//   const double red   = X*3.240479  + Y*-1.537150 + Z*-0.498535;
//   const double green = X*-0.969256 + Y*1.875992  + Z*0.041556;
//   const double blue  = X*0.055648  + Y*-0.204043 + Z*1.057311;

  double red,green,blue;
  
  RGB_converter_to_XYZ<double>::from_cartesian_to_RGB(X,Y,Z,&red,&green,&blue);
/*
  if (red<0.0)      { *r = 0.0; }
  else if (red>1.0) { *r = 1.0; }
  else              { *r = red; }

  if (green<0.0)      { *g = 0.0; }
  else if (green>1.0) { *g = 1.0; }
  else                { *g = green; }

  if (blue<0.0)      { *b = 0.0; }
  else if (blue>1.0) { *b = 1.0; }
  else               { *b = blue; }
*/
}







/*

  ##############################
  # class RGB_converter_to_Lab #
  ##############################
*/


// Reference white is CIE E

template<typename Real>
const double RGB_converter_to_Lab<Real>::Xn = 1.0;
template<typename Real>
const double RGB_converter_to_Lab<Real>::Yn = 1.0;
template<typename Real>
const double RGB_converter_to_Lab<Real>::Zn = 1.0;


template<typename Real>
void
RGB_converter_to_Lab<Real>::
from_RGB_to_cartesian(const double r,
		      const double g,
		      const double b,
		      component_type* const x,
		      component_type* const y,
		      component_type* const z){

  double X,Y,Z;

  RGB_converter_to_XYZ<double>::from_RGB_to_cartesian(r,g,b,&X,&Y,&Z);

  const double xr = X/Xn;
  const double yr = Y/Yn;
  const double zr = Z/Zn;

  const double fx = (xr>216.0/24389.0)
    ? std::pow(xr,1.0/3.0)
    : (24389.0/27.0*xr + 16.0) / 116.0;

  const double fy = (yr>216.0/24389.0)
    ? std::pow(yr,1.0/3.0)
    : (24389.0/27.0*yr + 16.0) / 116.0;

  const double fz = (zr>216.0/24389.0)
    ? std::pow(zr,1.0/3.0)
    : (24389.0/27.0*zr + 16.0) / 116.0;

  *x = 116.0*fy - 16.0;
  *y = 500.0 * (fx-fy);
  *z = 200.0 * (fy-fz);
}


template<typename Real>
void
RGB_converter_to_Lab<Real>::
from_cartesian_to_RGB(const component_type x,
		      const component_type y,
		      const component_type z,
		      double* const r,
		      double* const g,
		      double* const b){

  const double yr = (x>8)
    ? std::pow((x+16.0) / 116.0,3.0)
    : x / 24389.0 * 27.0;
  
  const double fy = (yr>216.0/24389.0)
    ? (x+16.0) / 116.0
    : (24389.0/27.0*yr+16.0) / 116.0;

  const double fx = y / 500.0 + fy;
  const double fz = fy - z / 200.0;

  const double xr = (fx > std::pow(216.0/24389.0,1.0/3.0))
    ? std::pow(fx,3.0)
    : (116.0*fx - 16.0) / 24389.0 * 27.0;

  const double zr = (fz > std::pow(216.0/24389.0,1.0/3.0))
    ? std::pow(fz,3.0)
    : (116.0*fz - 16.0) / 24389.0 * 27.0;

  const double X = xr*Xn;
  const double Y = yr*Yn;
  const double Z = zr*Zn;

  RGB_converter_to_XYZ<double>::from_cartesian_to_RGB(X,Y,Z,r,g,b);

/*
  double red,green,blue;

  RGB_converter_to_XYZ<double>::from_cartesian_to_RGB(X,Y,Z,&red,&green,&blue);

  if (red<0.0)      { *r = 0.0; }
  else if (red>1.0) { *r = 1.0; }
  else              { *r = red; }

  if (green<0.0)      { *g = 0.0; }
  else if (green>1.0) { *g = 1.0; }
  else                { *g = green; }

  if (blue<0.0)      { *b = 0.0; }
  else if (blue>1.0) { *b = 1.0; }
  else               { *b = blue; }
*/
}









/*

  #####################################
  # class RGB_converter_to_grey_level #
  #####################################
  
*/



template<typename Real>
void
RGB_converter_to_grey_level<Real>::
from_RGB_to_cartesian(const double r,
		      const double g,
		      const double b,
		      component_type* const x,
		      component_type* const y,
		      component_type* const z){

  *x = static_cast<component_type>((r+g+b)/3.0);
  *y = static_cast<component_type>(0);
  *z = static_cast<component_type>(0);
}


template<typename Real>
void
RGB_converter_to_grey_level<Real>::
from_cartesian_to_RGB(const component_type x,
		      const component_type y,
		      const component_type z,
		      double* const r,
		      double* const g,
		      double* const b){
  
  *r = static_cast<double>(x);
  *g = static_cast<double>(x);
  *b = static_cast<double>(x);
}



/*

  #########################
  # Fonctions hors classe #
  #########################

 */

template<typename RGB_converter>
std::ostream& operator<<(std::ostream& s,
			 const Basic_color<RGB_converter>& c){

  s<<c.x<<"\t"<<c.y<<"\t"<<c.z<<"\t";
  
  return s;
}

#undef CONST_STATIC

#endif

