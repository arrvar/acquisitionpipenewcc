/*! \file
  \verbatim
  
    Copyright (c) 2007, Sylvain Paris and Fr�do Durand

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use, copy,
    modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.

  \endverbatim
*/


#ifndef __COLOR_TOOLS__
#define __COLOR_TOOLS__

#include <cmath>

#include <vector>

#include "geom.h"
#include "math_tools.h"



namespace Color_tools{


  template<typename Color>
  double HSV_saturation(const Color& clr);

  template<typename Color>
  double angular_saturation(const Color& clr);

  template<typename Color>
  double HSV_hue(const Color& clr);

  inline void RGB_from_index(const unsigned int   index,
			     unsigned char* const r,
			     unsigned char* const g,
			     unsigned char* const b);

  
/*
  
  #############################################
  #############################################
  #############################################
  ######                                 ######
  ######   I M P L E M E N T A T I O N   ######
  ######                                 ######
  #############################################
  #############################################
  #############################################
  
*/

  

  template<typename Color>
  double HSV_saturation(const Color& clr){
    
    double r,g,b;
    clr.get_RGB(&r,&g,&b);
  
    const double min   = std::min(r,std::min(g,b));
    const double max   = std::max(r,std::max(g,b));
    const double delta = max - min;
  
    if (max!=0){
      return (delta / max);               // s
    }
    else {  // r = g = b = 0     s = 0, v is undefined
      return 0.0;
    }
  }

  
  template<typename Color>
  double angular_saturation(const Color& clr){
    using namespace Geometry;

    double r,g,b;
    clr.get_RGB(&r,&g,&b);
  
    Vec3d clr_v = Vec3d(r,g,b).normalize();

    Vec3d w = Vec3d(1,1,1).normalize();

    return (clr_v^w).norm() / sin(0.25*M_PI);
  }




  template<typename Color>
  double HSV_hue(const Color& clr){

    using namespace std;

    double r,g,b,h;

    clr.get_RGB(&r,&g,&b);
    
    const double min   = std::min(r,std::min(g,b));
    const double max   = std::max(r,std::max(g,b));
    const double delta = max - min;
      
    if (max==0){
      return 0;
    }
    
    if (delta==0) { //cas non pris en compte
      return 0;
    }
    
    if(r==max) {
      h = ( g - b ) / delta;        // between yellow & magenta
    }
    else if( g == max ) {
      h = 2 + ( b - r ) / delta;    // between cyan & yellow
    }
    else {
      h = 4 + ( r - g ) / delta;    // between magenta & cyan
    }
    
    h /= 6;                              // dans [0:1]
    if(h<0){
      h += 1;
    }

    return h;
  }



  void RGB_from_index(const unsigned int   index,
		      unsigned char* const r,
		      unsigned char* const g,
		      unsigned char* const b){

    std::vector<unsigned int> index_bit(32);
    std::vector<unsigned int> r_bit(8);
    std::vector<unsigned int> g_bit(8);
    std::vector<unsigned int> b_bit(8);

    Math_tools::to_bit_array(index,&index_bit);

    for(unsigned int n=0;n<8;n++){

      r_bit[7-n] = index_bit[3*n];
      g_bit[7-n] = index_bit[3*n + 1];
      b_bit[7-n] = index_bit[3*n + 2];
    }

    unsigned int R,G,B;
    
    Math_tools::from_bit_array(r_bit,&R);
    Math_tools::from_bit_array(g_bit,&G);
    Math_tools::from_bit_array(b_bit,&B);

    R = 255 - R;
    B = 255 - B;
    G = 255 - G;

    if ((R<64) && (G<64) && (B<64)){

      R = 255 - R;
      G = 255 - G;
      B = 255 - B;
    }

    if (R + G + B < 224){
      R += 32;
      G += 32;
      B += 32;
    }
    
    *r = R;
    *g = G;
    *b = B;
  }

  

}


#endif
