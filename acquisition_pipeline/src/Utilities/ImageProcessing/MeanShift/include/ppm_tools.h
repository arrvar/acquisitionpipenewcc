/*! \file
  \verbatim
  
    Copyright (c) 2007, Sylvain Paris and Fr�do Durand

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use, copy,
    modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.

  \endverbatim
*/

#ifndef __PPM_TOOLS__
#define __PPM_TOOLS__

#include <fstream>
#include <iostream>

namespace PPM_tools{

  template<typename IMAGE>
  void read_ppm(const char*  filename,
		IMAGE* const data);


  template<typename IMAGE>
  void write_ppm(const char*  filename,
		 const IMAGE& data);



/*
  ###########################################################
  ###########################################################
  ###########################################################
  ##############                               ##############
  ##############  I M P L E M E N T A T I O N  ##############
  ##############                               ##############
  ###########################################################
  ###########################################################
  ###########################################################
*/





  template<typename IMAGE>
  void read_ppm(const char*  filename,
		IMAGE* const data){

    using namespace std;
  
    IMAGE& image = *data;
  
    ifstream ppm_in(filename,ios::binary);

    string magic_number("  ");
  
    ppm_in.get(magic_number[0]);
    ppm_in.get(magic_number[1]);

    if (magic_number != std::string("P6")){
      cerr<<"error: unrecognized file format\n"<<filename<<" is not a PPM file.\n"<<endl;
      exit(2);   
    }

    unsigned width,height,bpp;
  
    ppm_in>>width>>height>>bpp;

    if (bpp != 255){
      cerr<<"error: unsupported maximum value ("<<bpp<<")\n"<<"It must be 255."<<endl;
      exit(3);   
    }

    image.resize(width,height);
  
    char ch;
    ppm_in.get(ch); // Trailing white space.

    char r,g,b;
  
    for(unsigned y=0;y<height;y++){
      for(unsigned x=0;x<width;x++){

	ppm_in.get(r);
	ppm_in.get(g);
	ppm_in.get(b);
       
	const unsigned char R = static_cast<unsigned char>(r);
	const unsigned char G = static_cast<unsigned char>(g);
	const unsigned char B = static_cast<unsigned char>(b);
	
 	image(x,y) = typename IMAGE::value_type(R/255.0,G/255.0,B/255.0);
                      
// 	image(x,y)[0] = static_cast<double>(R) / 255.0;
// 	image(x,y)[1] = static_cast<double>(G) / 255.0;
// 	image(x,y)[2] = static_cast<double>(B) / 255.0;
      }
    }
  
    ppm_in.close();
  }




  
  template<typename IMAGE>
  void write_ppm(const char*  filename,
		 const IMAGE& data){
    
    using namespace std;
    
    ofstream ppm_out(filename,ios::binary);

    ppm_out<<"P6";
    ppm_out<<' ';
    ppm_out<<data.width();
    ppm_out<<' ';
    ppm_out<<data.height();
    ppm_out<<' ';
    ppm_out<<"255";
    ppm_out<<'\n';
  
    for(unsigned y=0;y<data.height();y++){    
      for(unsigned x=0;x<data.width();x++){

// 	const double R = data(x,y)[0] * 255.0;
// 	const double G = data(x,y)[1] * 255.0;
// 	const double B = data(x,y)[2] * 255.0;
      
// 	const char r = static_cast<unsigned char>(Math_tools::clamp(0.0,255.0,R));
// 	const char g = static_cast<unsigned char>(Math_tools::clamp(0.0,255.0,G));
// 	const char b = static_cast<unsigned char>(Math_tools::clamp(0.0,255.0,B));

	char r,g,b;
	data(x,y).get_RGB(&r,&g,&b);
	
	ppm_out<<r<<g<<b;
      }
    }
  
    ppm_out.flush();
    ppm_out.close();
  }

  
} // END OF namespace PPM_tools
  
#endif
