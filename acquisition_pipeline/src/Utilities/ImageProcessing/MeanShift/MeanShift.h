#ifndef MEANSHIFT_H_
#define MEANSHIFT_H_

namespace spin
{
  class MeanShift 
  {
    public:
      int Compute( void* _img, float p1, float p2, float p3, void* _imgOut);
  };//end of class
}
#endif
