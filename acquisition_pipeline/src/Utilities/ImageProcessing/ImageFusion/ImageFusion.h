#ifndef IMAGEFUSION_H_
#define IMAGEFUSION_H_
#include <memory>
#include <string>

namespace spin
{
  // Forward declaration
  class  RGBImageIO;
  class  FocusMeasure;
  
  /*! \brief ColorTransfer
   *  This is a placeholder for all routines pertaining to ImageFusion
   */
  class ImageFusion
  {
    public:
      // Default constructor
      ImageFusion(int height, int width, int radius = -1);
      // Default destructor
      ~ImageFusion(){}
      //PCA based image fusion
      int PCAFusion(void* images, std::string& fName);
    private:
      std::shared_ptr<RGBImageIO> imageio;
      std::shared_ptr<FocusMeasure> fmeasure;
  };//end of class  
}
#endif
