#include "Utilities/ImageProcessing/ImageFusion/ImageFusion.h"
#include "Utilities/ImageProcessing/FocusMeasure/FocusMeasure.h"
#include "Framework/ImageIO/ImageIO.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "opencv2/opencv.hpp"
#include <vector>
#ifdef USE_OPENMP
#include <omp.h>
#endif

namespace spin
{
  /*! \brief ConditionData
   *  This functions takes a 3-channel data and conditions it
   *  by subtracting the mean. Subsequently a SVD is performed to obtain the weighting factors
   */
  void ConditionData(RMMatrix_Float* _mat, RMMatrix_Float* weight, RMMatrix_Float* projMat)
  {
    // Make a copy of the matrix
    RMMatrix_Float mat = (*_mat);
    // Compute the mean of all channels
    RMMatrix_Float mean = (RMMatrix_Float::Ones(1,mat.rows()) * (mat))/(mat.rows());
    // Subtract the mean from the data
    mat = (mat) - RMMatrix_Float::Ones(mat.rows(),1) * mean;
    // and build the 3x3 covariance matrix. Add a small offset to ensure that 
    // the matrix is strictly positive definite. At the same time we weight the covariance
    RMMatrix_Float eye = RMMatrix_Float::Identity(mat.cols(),mat.cols())*1.0e-6;
    RMMatrix_Float dMat = ((*weight) * mat.transpose() * (mat) * (*weight))/mat.rows() + eye;
    // Implement a SVD on the 3x3 matrix to compute the transformation matrices
    Eigen::JacobiSVD<RMMatrix_Double> svd(dMat.template cast<double>(), Eigen::ComputeThinU|Eigen::ComputeThinV);
    
    // Save the weight corresponding to the eigenvector associated with
    // the largest eigenvalue
    *projMat = (svd.matrixU().block(0,0,svd.matrixU().rows(),1)).template cast<float>();
    
    float sum = projMat->array().sum();
    *projMat = projMat->array()/(1e-8+sum);
  }//end of function
  
  /*! \brief PCAFusion
   *  This function is used to fuse a set of images and save a composite image to disk
   */
  int ImageFusion::PCAFusion(void* _imgVec, std::string& fName)
  {
    //typecast the data
    std::vector<cv::Mat>* imgVec = static_cast<std::vector<cv::Mat>*>(_imgVec);
    if (imgVec->size() <=0) return -1;
    int numImg = imgVec->size();
    int height = (*imgVec)[0].rows;
    int width  = (*imgVec)[1].cols;
    
    // A data structure to hold weights
    RMMatrix_Float weight = RMMatrix_Float::Zero(numImg,numImg);
    // Allocate memory for holding intermediate objects
    RMMatrix_Float c_L = RMMatrix_Float::Zero(height*width, numImg);
    RMMatrix_Float c_a = RMMatrix_Float::Zero(height*width, numImg);
    RMMatrix_Float c_b = RMMatrix_Float::Zero(height*width, numImg);
    
    // Build focus metrics for each channel and populate the weight matrix
#ifdef USE_OPENMP
#pragma omp parallel for
#endif
    for (int i = 0; i < numImg; ++i)
    {
      {
        // Extract the channel
        RMMatrix_Float d, extract;
        imageio.get()->ExtractChannel(&((*imgVec)[i]), 0, &extract, &d);
        // And populate the weight matrix
        fmeasure.get()->ComputeFocusMeasure(&extract, &(weight(i,i)));
        // In addition populate respective channels
        c_L.block(0,i,height*width,1) = d.block(0,0,height*width,1);
        c_a.block(0,i,height*width,1) = d.block(0,1,height*width,1);
        c_b.block(0,i,height*width,1) = d.block(0,2,height*width,1);
      }
    }
    
    // Compute the weighting factor for each channel
    RMMatrix_Float s_L, s_a, s_b; 
    ConditionData(&c_L, &weight, &s_L);
    ConditionData(&c_a, &weight, &s_a);
    ConditionData(&c_b, &weight, &s_b);

    // Fuse each channel of the image
    RMMatrix_Float L = c_L * s_L;
    RMMatrix_Float a = c_a * s_a;
    RMMatrix_Float b = c_b * s_b;
    // And concatenate the columns
    RMMatrix_Float fused(height*width,3);
    fused <<L,a,b;
    
    ////std::cout<<fused.rows()<<" "<<fused.cols()<<std::endl;
    //exit(1);
    // Finally, save the image
    int p = imageio.get()->WriteLABImage(fName, &fused, height);
    
    return p;
  }//end of function
}//end of namespace