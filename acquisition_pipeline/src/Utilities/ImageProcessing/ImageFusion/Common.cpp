#include "Utilities/ImageProcessing/ImageFusion/ImageFusion.h"
#include "Utilities/ImageProcessing/FocusMeasure/FocusMeasure.h"
#include "Framework/ImageIO/ImageIO.h"

namespace spin
{
  /*! \brief Default constructor */
  ImageFusion::ImageFusion(int height, int width, int radius)
  {
    if (radius <=0 ) radius = 16;
    fmeasure = std::make_shared<FocusMeasure>(height, width, radius);
    imageio = std::make_shared<RGBImageIO>();
  }//end of function
}