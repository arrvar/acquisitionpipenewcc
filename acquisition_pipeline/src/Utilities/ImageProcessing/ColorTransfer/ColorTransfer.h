#ifndef COLORTRANSFER_H_
#define COLORTRANSFER_H_
#include <memory>
#include <string>

namespace spin
{
  // Forward declaration
  struct CTStruct;
  class  RGBImageIO;
  
  /*! \brief ColorTransfer
   *  This is a placeholder for all color normalization 
   */
  class ColorTransfer
  {
    public:
      // Default constructor
      ColorTransfer(std::string& refFolder, std::string& model);
      // Overloaded constructor
      ColorTransfer(std::string& model);
      // Default destructor
      ~ColorTransfer(){}
      // Reinhard's color correction
      int PCANormalization(std::string& imgIn, std::string& imgOut);
    private:
      std::shared_ptr<CTStruct> model;
      std::shared_ptr<RGBImageIO> imageio;
  };//end of class
  
}
#endif
