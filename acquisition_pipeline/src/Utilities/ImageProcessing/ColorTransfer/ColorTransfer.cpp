#include "Utilities/ImageProcessing/ColorTransfer/ColorTransfer.h"
#include "Utilities/ImageProcessing/ColorTransfer/CTStruct.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "Framework/ImageIO/ImageIO.h"
#include <fstream>
#include <string>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

/*! \brief The functions described in this class use PCA to align color-spaces
 *  between a "user-define palatte" (hereby refered to as template) and an unknown
 *  "source" image. For a better understanding of this process, please refer to 
 *  "A. Abadpour and S. Kasaei. An efficient pca-based color transfer method"
 *  Journal of Visual Communication and Image Representation, 18(1):15–34, 2007.
 */
namespace spin
{
  /*! \brief ConditionData
   *  This functions takes a 3-channel data and conditions it
   *  by subtracting the mean
   */
  void ConditionData(MyData* dTemp)
  {
    // Compute the mean of all channels
    dTemp->mean = (RMMatrix_Float::Ones(1,dTemp->mat.rows()) * dTemp->mat)/(dTemp->mat.rows());
    // Subtract the mean from the data
    dTemp->mat = dTemp->mat - RMMatrix_Float::Ones(dTemp->mat.rows(),1) * dTemp->mean;
    // and build the 3x3 covariance matrix. Add a small offset to ensure that 
    // the matrix is strictly positive definite
    RMMatrix_Float eye = RMMatrix_Float::Identity(dTemp->mat.cols(),dTemp->mat.cols())*1.0e-6;
    RMMatrix_Float dMat = (dTemp->mat.transpose() * dTemp->mat)/dTemp->mat.rows() + eye;
    // Implement a SVD on the 3x3 matrix to compute the transformation matrices
    Eigen::JacobiSVD<RMMatrix_Double> svd(dMat.template cast<double>(), Eigen::ComputeThinU|Eigen::ComputeThinV);
    // Save the transformation matrix and the eigenvalues
    RMMatrix_Double t = svd.matrixU() * (svd.singularValues().cwiseSqrt()).asDiagonal() * svd.matrixV().transpose();
    dTemp->projMat = t.template cast<float>();
  }//end of function
  
  /*! \brief ImageStatistics
   *  This functions reads an image from the disk and generates
   *  statistics
   */
  int ImageStatistics(std::string& refImgFolder, MyData* dTemp, RGBImageIO* imageio)
  {
    // We scan the ref image folder. It may contain either a single image or
    // multiple images.
    boost::filesystem::path dir(refImgFolder);
    if (!boost::filesystem::exists(dir))
    {
      return -1;
    }
    
    // Reset input matrix
    dTemp->mat.resize(0,0);
    // Now, for each image in the folder we run the pipeline
    boost::filesystem::directory_iterator it(refImgFolder), eod; 
    BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))   
    { 
      if(boost::filesystem::is_regular_file(p))
      {
        // Get the name of the image
        std::string refImg = p.string();
        //std::cout<<refImg<<std::endl;
        // Read the data and convert it to a matrix
        RMMatrix_Float temp;
        dTemp->imgRows = imageio->ReadLABImage(refImg, &temp);
        // Append the matrix
        if (dTemp->mat.rows()>0)
        {
          RMMatrix_Float f = dTemp->mat;
          dTemp->mat.resize(dTemp->mat.rows() + temp.rows(), temp.cols());
          dTemp->mat << f, temp;
        }
        else
        {
          dTemp->mat = temp;
        }
      }
    }
    
    // sanity check
    if (dTemp->mat.rows() <=0) return -2;
    
    // Condition the data
    ConditionData(dTemp);
    return 1;
  }//end of function
   
  /*! \brief Constructor 
   *  In this function, we do the following:
   *  a.) Set an input image folder
   *  b.) Set a modelfile name
   *  This constructor is primarily used when creating a projection 
   *  matrix for color transfer.
   */
  ColorTransfer::ColorTransfer(std::string& refImgFolder, std::string& modelFile)
  {
    // Check if the modelFile has been initialized
    if (modelFile == "") return;
    
    // Initialize model
    model = std::make_shared<CTStruct>();
    // Initialize imageio
    imageio = std::make_shared<RGBImageIO>();
    // Instantiate a new instance of MyData
    MyData dTemp;
    // Compute Statistics
    ImageStatistics(refImgFolder, &dTemp, imageio.get());
    
    // Populate the model
    model.get()->SetProjection(dTemp.projMat);
    model.get()->SetMean(dTemp.mean);

    // Serialize and save the model into a file
    std::ofstream ofs(modelFile.c_str());
    boost::archive::binary_oarchive oa(ofs);
    // write class instance to archive
    oa << (*(model.get()));
  }//end of function
  
  /*! \brief Constructor 
   *  In this function, we parse a color transfer model from a
   *  file and deserialize it
   */
  ColorTransfer::ColorTransfer(std::string& modelFile)
  {
    // Check if the modelFile has been initialized
    if (modelFile == "") return;
    // Initialize model
    model = std::make_shared<CTStruct>();
    // Initialize imageio
    imageio = std::make_shared<RGBImageIO>();
    
    // deserialize the model
    std::ifstream ifs(modelFile.c_str());
    boost::archive::binary_iarchive ia(ifs);
    // read class state from archive
    ia >>(*(model.get()));
  }//end of function
  
  /*! \brief PCANormalization 
   *  This function applies a previously computed transformation
   *  matrix to a set of input images present in the folder "inImhFolder".
   *  The transformed images are saved in "outImgFolder"
   */
  int ColorTransfer::PCANormalization(std::string& inImgFolder, std::string& outImgFolder)
  {
    if (!model.get()) return -1;
    
    // We scan the input image folder. It may contain either a single image or
    // multiple images.
    boost::filesystem::path dir(inImgFolder);
    if (!boost::filesystem::exists(dir))
    {
      return -1;
    }
    
    // Check if the output folder exists. If not, we create itr
    boost::filesystem::path dir2(outImgFolder);
    if (!boost::filesystem::exists(dir2))
    {
      // Create the directory
      if (!boost::filesystem::create_directory(dir2))
      {
        return -2;
      }
    }
    
    // Now, for each image in the folder we run the pipeline
    boost::filesystem::directory_iterator it(inImgFolder), eod; 
    BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))   
    { 
      if(boost::filesystem::is_regular_file(p))
      {
        // Get the name of the image
        std::string refImg = p.string();
        // Read the data and convert it to a matrix
        MyData dTemp;
        // Read the data
        dTemp.imgRows = imageio.get()->ReadLABImage(refImg, &dTemp.mat);
        // Condition it
        ConditionData(&dTemp);
        // And transform the data
        RMMatrix_Float T =  dTemp.mat * dTemp.projMat.inverse()*model.get()->GetProjection() + \
                            RMMatrix_Float::Ones(dTemp.mat.rows(),1) * model.get()->GetMean();
        // Split the input string to get the image name
        std::vector<std::string> tRes;
        boost::split(tRes, refImg, boost::is_any_of("/"));
        if (tRes.size() > 0)
        {
          std::string outImg = outImgFolder+"/"+tRes[tRes.size()-1];
          int p = imageio.get()->WriteLABImage(outImg, &T, dTemp.imgRows);
          if (p !=1) return -3;
          tRes.clear();
        }
      }//finished checking if a regular file
    }//finished checking all files
    return 1;
  }//end of function
}//end of namespace
