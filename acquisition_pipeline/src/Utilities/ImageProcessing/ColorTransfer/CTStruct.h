#ifndef CTSTRUCT_H_
#define CTSTRUCT_H_
#include <vector>
#include <map>
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

namespace spin
{
  class CTStruct 
  {
    public:
      CTStruct(){}
      ~CTStruct(){}
      RMMatrix_Float GetMean(){return mean;}
      void SetMean(RMMatrix_Float a){mean = a;}
      RMMatrix_Float GetProjection(){return projMat;}
      void SetProjection(RMMatrix_Float a){projMat = a;}
    private:
      RMMatrix_Float mean;
      RMMatrix_Float projMat;
      friend class boost::serialization::access;
      // When the class Archive corresponds to an output archive, the
      // & operator is defined similar to <<.  Likewise, when the class Archive
      // is a type of input archive the & operator is defined similar to >>.
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version)
      {
        ar & mean;
        ar & projMat;
      }//end of class
  };//end of class
  
  /*! \brief Serializer/Deserializer for model */
  
  /*! \brief A structure to hold image statistics */
  struct MyData
  {
    RMMatrix_Float mat, mean, projMat, eigVals;
    int imgRows;
  };//end of data 
}//end of namespace
#endif
