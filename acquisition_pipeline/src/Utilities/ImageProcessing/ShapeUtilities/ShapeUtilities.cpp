#include "Utilities/ImageProcessing/ShapeUtilities/ShapeUtilities.h"
#include "Utilities/ImageProcessing/FFTUtils/FFTNormalization1D.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/eigen.hpp"

namespace spin
{
  int GenerateBlob(cv::Mat* img, cv::Mat* binImg)
  {
    cv::Mat split[3];
    cv::split(*img, split);
    // Exctract the green channels
    cv::Mat gImg = split[1];
    // Apply dilation on the image
    int dilation_size = 3;
    cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(2*dilation_size + 1, 2*dilation_size + 1), cv::Point(-1, -1));
    cv::morphologyEx(gImg, gImg, cv::MORPH_DILATE, element);
    // And threshold it
    cv::Mat dump;
    cv::threshold(gImg, dump, 0, 255, CV_THRESH_BINARY_INV | CV_THRESH_OTSU);
    *binImg = dump.clone();
    return 1;
  }//end of function

  /*! \brief This function generates a contour from a binary blob */
  int GetContour(void* _img, void* _contPoints, void* _centroid, std::string path)
  {
    cv::Mat* img = static_cast<cv::Mat*>(_img);
    std::vector<cv::Point>* contPoints = static_cast<std::vector<cv::Point>*>(_contPoints);
    cv::Point2f* centroid = static_cast<cv::Point2f*>(_centroid);

    // Generate a blob
    cv::Mat binImg;
    GenerateBlob(img, &binImg);

    // Check if the image needs to be saved
    if (path !="")
    {
      cv::imwrite(path, binImg);
    }

    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::Mat foo = binImg.clone();

    //  Extarct the Biggest cont out, assuming you will get only cell.
    cv::findContours(foo, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, cv::Point(0, 0));

    ////std::cout<<contours.size()<<std::endl;
    // Get the contour with the largest area
    int contID = 0;
    double contArea = 0;
    for (int i = 0; i < contours.size(); i++)
    {
      double a = cv::contourArea(contours[i]);
      if (a > contArea)
      {
        contArea = a;
        contID = i;
      }
    }

    // Get the contour
    *contPoints = contours[contID];

    // centroid of the biggest contour
    cv::Moments mu = cv::moments(*contPoints, false);
    *centroid = cv::Point2f(static_cast<float>(mu.m10 / mu.m00), static_cast<float>(mu.m01 / mu.m00));

    // Free memory
    for (auto i: contours)
      i.clear();
    hierarchy.clear();
    return 1;
  }//end of function

  /*! \brief GetFeatures
  *  This is the driver function for obtaining shape features associated with
  *  an RGB image. The RGB image is thresholded and a binary blob is obtained.
  *  From the binary blob, a set of shape features are obtained:
  *  a.) Curvature, and
  *  b.) Centroid transform
  */
  int ShapeUtilities::Compute1DFeatures(void* _img, void* _features, int length, std::string path)
  {
    // Get the contour after thresholding the image
    std::vector<cv::Point> contour;
    cv::Point2f centroid;
    // Try and get the centroid
    int h = GetContour(_img, &contour, &centroid, path);
    if (h !=1) return -1;

    // Allocate memory for the Gaussian Curvature and the Centroid Transform
    int l = contour.size();
    //cv::Mat mGC         = cv::Mat::zeros(1, l-2, CV_64F);
    cv::Mat mCT         = cv::Mat::zeros(1, l, CV_64F);
    //cv::Mat mGC_r       = cv::Mat::zeros(1, length, CV_64F);
    cv::Mat mCT_r       = cv::Mat::zeros(1, length, CV_64F);

    /*// a.) Compute the curvature
    std::vector<float> y1(contour.size(),0);
    std::vector<float> y2(contour.size(),0);
    std::vector<float> x1(contour.size(),0);
    std::vector<float> x2(contour.size(),0);

    for (int i = 1; i < y1.size(); ++i)
    {
      y1[i]= contour[i].y - contour[i-1].y;
      x1[i]= contour[i].x - contour[i-1].x;
    }

    for (int i = 2; i < y2.size(); ++i)
    {
      y2[i]= y1[i] - y1[i-1];
      x2[i]= x1[i] - x1[i-1];
      //std::cout<<y2[i]<<" "<<x2[i]<<std::endl;
    }
    */

    for (int i = 0; i < l; ++i)
    {
      /*if (x1[i]!=0 ||)
      double d =  std::pow(static_cast<double>((x1[i]*x1[i]+y1[i]+y1[i])),1.5);
      if (d !=0)
      mGC.at<double>(0,i-2) = static_cast<double>((x1[i]*y2[i] - x2[i]*y1[i]))/d;
      else
      mGC.at<double>(0,i-2) = 0;
      */
      double k1 = (contour[i].y - centroid.y);
      double k2 = (contour[i].x - centroid.x);

      mCT.at<double>(0,i) = sqrt(k1*k1+k2*k2);
    }

    // Resize the vectors such that they have lengths of "length"
    //cv::resize(mGC, mGC_r, cv::Size(mGC_r.cols, mGC_r.rows), 0, 0, CV_INTER_LINEAR);
    cv::resize(mCT, mCT_r, cv::Size(mCT_r.cols, mCT_r.rows), 0, 0, CV_INTER_LINEAR);

    // Scale the centroid transform such that it lies between 0-1
    double min, max;
    cv::minMaxIdx(mCT_r, &min, &max);
    mCT_r = (mCT_r-min) /(max-min);

    // convert the data to eigen matrices
    //RMMatrix_Double eGC_r = RMMatrix_Double::Zero(mGC_r.rows, mGC_r.cols);
    RMMatrix_Double eCT_r = RMMatrix_Double::Zero(mCT_r.rows, mCT_r.cols);
    //cv::cv2eigen(mGC_r, eGC_r);
    cv::cv2eigen(mCT_r, eCT_r);

    // clear redundant memory
    //y1.clear();
    //y2.clear();
    ///x1.clear();
    //x2.clear();
    contour.clear();

    // finally, normalize the data using FFT
    std::map<int, RMMatrix_Double>* features = static_cast<std::map<int, RMMatrix_Double>*>(_features);
    //(*features)[0] = RMMatrix_Double::Zero(eGC_r.rows(), eGC_r.cols());
    (*features)[1] = RMMatrix_Double::Zero(eCT_r.rows(), eCT_r.cols());

    spin::FFTNormalization1D<double> fftNorm(length);
    //fftNorm.CommputeNormalization(eGC_r.data(), (*features)[0].data(), length);
    fftNorm.CommputeNormalization(eCT_r.data(), (*features)[1].data(), length);

    return 1;
  }//end of function
}//end of namespace
