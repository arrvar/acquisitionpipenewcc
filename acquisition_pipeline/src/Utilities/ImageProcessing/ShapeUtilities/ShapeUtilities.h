#ifndef SHAPEUTILITIES_H_
#define SHAPEUTILITIES_H_
#include <string>

namespace spin
{
  /*! \brief This class is a placeholder for all aspects related to
   *  generaing 1D features from binary shapes
   */
  class ShapeUtilities
  {
    public:
      ShapeUtilities(){}
      ~ShapeUtilities(){}

      /*! \brief Compute 1D features */
      int Compute1DFeatures(void* _img, void* _features, int length=300, std::string path="");
  };//end of class
}//end of namespace
#endif
