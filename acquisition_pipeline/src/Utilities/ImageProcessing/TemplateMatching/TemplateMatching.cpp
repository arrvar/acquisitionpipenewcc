#include "Utilities/ImageProcessing/TemplateMatching/TemplateMatching.h"
#include "Utilities/ImageProcessing/FFTUtils/FFT2D.h"
#include "Utilities/ImageProcessing/HistogramUtils/HistogramUtils.h"
#include "Utilities/ImageProcessing/ImageUtils/ImageUtils.h"
#include "Utilities/Misc/PrimeFactorization.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "AbstractClasses/AbstractImageTypes.h"
#ifdef USE_OPENMP
#include <omp.h>
#endif
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/*! \brief Functions defined in this file are used to do the following:
 *  a.) Compute a series of Complex FFT's of template images and populate a map of CDF's 
 *  b.) Get a new image and compute a series of metrics between the reference CDF and the target CDF 
 */ 
namespace spin
{
  struct ImgInfo
  {
    int o_Height;
    int o_Width;
    int subsampling;
    int channel;
    /*! The following matrix holds entropy values of templates*/
    RMMatrix_Float entropy;
    /*! The following matrix holds the pdf*/
    RMMatrix_Float pdf, cdf;
  };//end of struct
  
  /*! \brief ComputeFFTStuff
   *  This is a helper class for building FFT's and histograms of template images
   */
  template <class pChannel, class T>
  int ComputeFFTStuff(spin::RGBImageType::Pointer* img, FFT2D<T>* fft2d, \
                      HistogramUtils* hUtils, ImgInfo* info, \
                      bool training, int p, \
                      void* _checks)
  {
    // Compute CDF of magnitude of img
    RMMatrix_Complex tFFT;
    ComputeFFTOfRGBImage<pChannel,T>( img, fft2d, &tFFT,\
                                      info->subsampling,\
                                      true,\
                                      info->o_Width,\
                                      info->o_Height,false);
    RMMatrix_Float tPDF;
    RMMatrix_Float mask = RMMatrix_Float::Ones(tFFT.rows(),tFFT.cols());
    hUtils->ComputeCDFOfFFTMagnitude(&tFFT, &tPDF, &mask, false);
    // Compute the entropy
    float entropy = 0;
    for (int j = 0; j < tPDF.cols(); ++j)
    {
      float g = tPDF(0,j);
      if (g > 0)
      {
        entropy -= g * log(g);
      }
    }
    
    // Are we in the training mode?
    if (training)
    {
      // Place the entropy in the correct location
      info->pdf(p,0) = entropy;
    }
    else
    {
      RMMatrix_Float* checks = static_cast<RMMatrix_Float*>(_checks);
      // Do a sanity check 
      if (!checks->data()) return -1;
      
      for (int i = 0; i < info->entropy.rows(); ++i)
      {
        // We compute the ratio of the ref entropy with each of the target entropy
        (*checks)(i,0) = entropy / info->pdf(i,0);
      }
    }
  
    return 1;
  }//end of function

  /*! \brief ComputeFFTStuff
   *  This is a helper class for building FFT's and histograms of template images
   
  template <class pChannel>
  int ComputeFFTStuff(spin::RGBImageType::Pointer* img, FFT2D<float>* fft2d, HistogramUtils* hUtils, \
                      ImgInfo* info, RMMatrix_Float* tCDF)
  {
    // Compute CDF of magnitude of img
    RMMatrix_Complex tFFT;
    ComputeFFTOfRGBImage<pChannel,float>( img, fft2d, &tFFT, info->subsampling, true,\
                                          info->o_Width, info->o_Height,false);
    hUtils->ComputeCDFOfFFTMagnitude(&tFFT, tCDF);
    
    return 1;
  }//end of function*/
  
  
  /*! \brief Initialize
   *  This function initializes various objects that will be used
   *  in computing templates.
   *  @n            : The # of template images that will be considered
   *  @height       : The height of the images that will be used 
   *  @width        : The width of the images that will be used
   *  @subsampling  : Subsampling to be effected on the images for computing the FFT 
   *  @channel      : The channel on which the template matching will be done
   */
  template <class T>
  int TemplateMatching<T>::Initialize(int n, int height, int width, int subsampling, int channel)
  {
    if (n >0 && height > 0 && width > 0)
    {
      spin::PrimeFactorization pf;
      std::vector<int> validInt{2,3,5};
      int h1    = height / subsampling;
      int h1N   = pf.NearestInteger(h1, &validInt);
      h1N       *= subsampling;
      int w1    = width / subsampling;
      int w1N   = pf.NearestInteger(w1, &validInt);
      w1N      *= subsampling;
      validInt.clear();
      
      // Initialize objects
      fft2d = std::make_shared<FFT2D<T> >(h1N/subsampling,w1N/subsampling);
      info  = std::make_shared<ImgInfo>();
      hUtils= std::make_shared<HistogramUtils>(256);
      
      // fill in details
      info->o_Height    = h1N;
      info->o_Width     = w1N;
      info->subsampling = subsampling;
      info->channel     = channel;
      info->entropy     = RMMatrix_Float::Zero(n,1);
      info->pdf         = RMMatrix_Float::Zero(n,1);
      info->cdf         = RMMatrix_Float::Zero(n,1);
      return 1;
    }
    
    return -1;
  }//end of function
  
  /*! \brief SetTemplate
   *  This function sets an image as a template by computing the entropy
   *  of the FFT magnitude. The resultant weight is placed at the index location
   *  q.
   */
  template <class T>
  int TemplateMatching<T>::SetTemplate(void* p, int q)
  {
    // Sanity checks
    if (q < 0 || q > info.get()->cdf.rows()) return -1;
    
    // Typecast the data
    RGBImageType::Pointer* img = static_cast<RGBImageType::Pointer*>(p);
    if (!img) return -2;
    
    switch(info.get()->channel)
    {
      case 0:
      {
        typedef BlueChannelPixelAccessor<unsigned char> pChannel;                
        // Compute all FFT related stuff
        int v = ComputeFFTStuff<pChannel,T>(img, fft2d.get(), hUtils.get(), info.get(), true, q, NULL);     
        if (v != 1) return -3;
      }
      break;
      case 1:
      {
        typedef spin::GreenChannelPixelAccessor<unsigned char> pChannel;            
        // Compute all FFT related stuff
        int v = ComputeFFTStuff<pChannel,T>(img, fft2d.get(), hUtils.get(), info.get(), true, q, NULL);     
        if (v != 1) return -3;
      }
      break;
      case 2:
      {
        typedef spin::RedChannelPixelAccessor<unsigned char> pChannel;
        // Compute all FFT related stuff
        int v = ComputeFFTStuff<pChannel,T>(img, fft2d.get(), hUtils.get(), info.get(), true, q, NULL);     
        if (v != 1) return -3;
      }
      break;          
      default:
      {
        typedef spin::GreenChannelPixelAccessor<unsigned char> pChannel;  
        // Compute all FFT related stuff
        int v = ComputeFFTStuff<pChannel,T>(img, fft2d.get(), hUtils.get(), info.get(), true, q, NULL);     
        if (v != 1) return -3;
      }
      break;          
    }
    
    return 1;
  }//end of function
  
  /*! \brief GetWeight
   *  This function inputs a new image and computes the CDF of the magnitude of
   *  the FFT of the image. It then computes a ratio of the differences between 
   *  the new image and the templates previously computed and populates a vector
   *  q.
   */
  template <class T>
  int TemplateMatching<T>::GetWeights(void* pImg, void* _out)
  {
    // Typecast the data
    RGBImageType::Pointer* img = static_cast<RGBImageType::Pointer*>(pImg);
    if (!img) return -1;
   
    // typecast the output
    RMMatrix_Float* out = static_cast<RMMatrix_Float*>(_out);
    if (!out) return -3;
    
    // Allocate memory for out
    *out = RMMatrix_Float::Zero(info->entropy.rows(),1);
    
    // Compute the entropy ratios w.r.t all templates
    switch(info.get()->channel)
    {
      case 0:
      {
        typedef BlueChannelPixelAccessor<unsigned char> pChannel;                
        // Compute all FFT related stuff
        int v = ComputeFFTStuff<pChannel,T>(img, fft2d.get(), hUtils.get(), info.get(), false, -1, _out);     
        if (v != 1) return -4;
      }
      break;
      case 1:
      {
        typedef spin::GreenChannelPixelAccessor<unsigned char> pChannel;            
        // Compute all FFT related stuff
        int v = ComputeFFTStuff<pChannel,T>(img, fft2d.get(), hUtils.get(), info.get(), false, -1, _out);     
        if (v != 1) return -4;
      }
      break;
      case 2:
      {
        typedef spin::RedChannelPixelAccessor<unsigned char> pChannel;
        // Compute all FFT related stuff
        int v = ComputeFFTStuff<pChannel,T>(img, fft2d.get(), hUtils.get(), info.get(), false, -1, _out);     
        if (v != 1) return -4;
      }
      break;          
      default:
      {
        typedef spin::GreenChannelPixelAccessor<unsigned char> pChannel;  
        // Compute all FFT related stuff
        int v = ComputeFFTStuff<pChannel,T>(img, fft2d.get(), hUtils.get(), info.get(), false, -1, _out);     
        if (v != 1) return -4;
      }
      break;          
    }
    
    return 1;
  }//end of function
  
  template class TemplateMatching<float>;
  template class TemplateMatching<double>;
}//end of namespace