#ifndef TEMPLATEMATCHING_H_
#define TEMPLATEMATCHING_H_
#include <memory>

namespace spin
{
  /*! \brief This class is a placeholder for all functions
   *  related to 2D template based image matching
   */
  //forward declarations
  template <class T>
  class FFT2D;  
  class HistogramUtils;
  struct ImgInfo;
  
  template <class T>
  class TemplateMatching 
  {
    public:
      // default constructor
      TemplateMatching(){}
      // default destructor
      ~TemplateMatching(){}
      
      /*! \brief Initialize */
      int Initialize(int n, int h, int w, int s, int c);
      
      /*! \brief InsertTemplate*/
      int SetTemplate(void* p, int q);
      
      /*! \brief GetWeights */
      int GetWeights(void* img, void* q);
    private:
      std::shared_ptr< FFT2D<T> > fft2d;
      std::shared_ptr< HistogramUtils > hUtils;
      std::shared_ptr<ImgInfo> info;
  };//end of class
}//end of namespace
#endif
