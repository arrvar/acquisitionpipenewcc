#include "Utilities/ImageProcessing/Clustering/SelfTunedSpectralClustering/SelfTunedSpectralClustering.h"
#include "AbstractClasses/AbstractMatrixTypes.h"

/*!\brief The functions described in this file implement the following algorithm
 * "Self tuned spectral clustering", as described in this web-page
 * http://www.vision.caltech.edu/lihi/Demos/SelfTuningClustering.html
 * 
 * For more details on the algorithm, Refer to Section 4 of the paper.
 * In this implementation, we omit Step 2 (Local scaling) of the algorithm.
 */

namespace spin
{
  /*! \brief ComputeEigenVectors
   *  This function combines three steps of the aforementioned algorithm 
   *  and returns the k-largest eigenvectors of the Normalized-affinity
   *  matrix. The steps are as follows:
   *  a.) Zero-mean the data
   *  b.) Build the "Kernel dissimilarity" (rather than the affinity) matrix
   *      as described in the paper. This step avoids the need for explicitly
   *      building the affinity N xN gram matrix (where N being all the pixels
   *      of the image)
   *  c.) Run a randomized SVD on the normalized affinity matrix to obtain the
   *      k-largest eigenvectors.
   *  d.) Renormalize the rows such that each vector has unit norm (This step is 
   *      described in Algorithm 1 of the paper).
   */
  int SelfTunedSpectralClustering::ComputeEigenVectors(void* _input, void* _output)
  {
    // sanity checks
    RMMatrix_Float* input = static_cast<RMMatrix_Float*>(_input);
    if (!input->data()) return -1;
    
    // We need to compute the 
    
  }//end of function
}//end of namespace
