#ifndef FOCUSMEASURE_H_
#define FOCUSMEASURE_H_
#include <memory>

namespace spin
{
  struct FocusRadius;
  
  class FocusMeasure 
  {
    public:
      // Default constructor
      FocusMeasure(int h, int w, int r = 16);
      //Default destructor
      ~FocusMeasure(){}
      //Compute focus measure 
      int ComputeFocusMeasure(void* _img, float* focus);
    private:
      std::shared_ptr<FocusRadius> fradius;
  };//end of class
}//end of namespace
#endif