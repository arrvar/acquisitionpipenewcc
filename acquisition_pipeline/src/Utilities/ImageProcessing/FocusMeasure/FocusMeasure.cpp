#include "Utilities/ImageProcessing/FocusMeasure/FocusMeasure.h"
#include "Utilities/ImageProcessing/FFTUtils/FFT2D.h"
#include "Utilities/Misc/PrimeFactorization.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <cmath>

namespace spin
{
  struct FocusRadius
  {
    RMMatrix_Float maskHF;
    int heightOptimal;
    int widthOptimal;
    std::shared_ptr< spin::FFT2D<float> > fft2d;
  };//end of struct
  
  /*! \brief scale255
  *  This function rounds a number to a nearest integer
  */
  float scale255(float x)
  {
    if (x < 0) return 0;
    if (x >=255) return 255;
    x = floor(x*255.0+0.5);
    return x;
  }//end of function
  
  /*! \brief Constructor */
  FocusMeasure::FocusMeasure(int height, int width, int radius)
  {
    // Initialize FocusRadius
    fradius = std::make_shared<FocusRadius>();
    // We calculate the optimal height and width for computing the FFT 
    spin::PrimeFactorization pf;
    std::vector<int> validInt{2,3,5};
    fradius.get()->heightOptimal = pf.NearestInteger(height, &validInt);
    fradius.get()->widthOptimal  = pf.NearestInteger(width, &validInt);
    validInt.clear();
    // Allocate memory for the maask
    fradius.get()->maskHF = RMMatrix_Float::Ones(fradius.get()->heightOptimal,fradius.get()->widthOptimal);
    // Mask out all low-frequency regions
    RMMatrix_Float t = RMMatrix_Float::Zero(radius,radius);
    fradius.get()->maskHF.block(0,0,radius,radius) = t;
    fradius.get()->maskHF.block(0,width-radius-1,radius,radius) = t;
    fradius.get()->maskHF.block(height-radius-1,0,radius,radius) = t;
    fradius.get()->maskHF.block(height-radius-1,width-radius-1,radius,radius) = t;
    
    // Initialize a kissfft object
    fradius.get()->fft2d = std::make_shared< FFT2D<float> >(fradius.get()->heightOptimal,fradius.get()->widthOptimal);
  }//end of function
  
  /*! \brief ComputeFocusMeasure 
   *  In this function, we compute the focus measure associated with each image.
   *  A grayscale FFT is computed and the low-frequency region is masked out.
   *  A pdf is generated from the high frequency components and a Shanons-entropy 
   *  measure is returned 
   */
  int FocusMeasure::ComputeFocusMeasure(void* _img, float* focus)
  {
    RMMatrix_Float* img = static_cast<RMMatrix_Float*>(_img);
    // check if the data is correct
    if (!img->data()) return -1;
    // Paste the image into a bigger matrix 
    RMMatrix_Complex imgFFT = RMMatrix_Complex::Zero(fradius.get()->heightOptimal,fradius.get()->widthOptimal);
    RMMatrix_Complex outFFT;
    imgFFT.real().block(0,0,img->rows(),img->cols()) = (*img);
    // Compute the FFT
    int ret = fradius.get()->fft2d.get()->ComputePeriodicImageFFT(&imgFFT, &outFFT);
    if (ret != 1) return -2;
    
    // Compute the magnitude of the FFT Matrix
    RMMatrix_Float fftMag = outFFT.array().abs();
    // Compute the dynamic range of fftMag
    float mMax = fftMag.maxCoeff();
    float mMin = fftMag.minCoeff();
    // We quantize the data such that it lies between 0-255
    fftMag = ((fftMag.array() - mMin) / (mMax - mMin)).unaryExpr(std::ptr_fun(scale255));
    
    // And traverse through the array to build the histogram
    // ToDo: Can parallelize this operation using coarse-grained parallelism
    long sum = fftMag.rows() * fftMag.cols();

    // The histogram will be of length 256
    RMMatrix_Float tgt1 = RMMatrix_Float::Zero(1, 256);
    float* p = fftMag.data();
    float* m = fradius.get()->maskHF.data();
    long count = 0;
    for (long h = 0; h < sum; ++h)
    {
      if (m[h] > 0)
      {
        ++tgt1(0, (int)p[h]);
        ++count;
      }
    }
    
    // Compute Shannon's entropy
    *focus = 0;
    for (int i = 0; i < tgt1.cols(); ++i)
    {
      if (tgt1(0,i) > 0)
      {
        float c = tgt1(0,i)/count;
        (*focus) -= (c * std::log(c));
      }
    }
    
    (*focus) = std::sqrt(*focus);
    return 1;
  }//end of function
}//end of namespace