/*=========================================================================

  Program:   Insight Segmentation & Registration Toolkit
  Module:    $RCSfile: itkRGBToLabColorSpacePixelAccessor.h,v $
  Language:  C++
  Date:      $Date: 2009-03-03 15:08:46 $
  Version:   $Revision: 1.4 $

  Copyright (c) Insight Software Consortium. All rights reserved.
  See ITKCopyright.txt or http://www.itk.org/HTML/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __itkRGBToRGBColorSpacePixelAccessor_h
#define __itkRGBToRGBColorSpacePixelAccessor_h


#include "itkRGBPixel.h"
#include "itkVector.h"
#include "vnl/vnl_math.h"
#include "itkNumericTraits.h"

namespace itk
{
namespace Accessor
{
/**
 * \class RGBToLabColorSpacePixelAccessor
 * \brief Give access to a RGBPixel as a Vector type.
 *
 * This class is intended to be used as parameter of
 * an ImageAdaptor to make an RGBPixel image appear as being
 * an image of Vector pixel type in RGB Color Space.
 *
 * \sa ImageAdaptor
 * \ingroup ImageAdaptors
 *
 */

template <class TInput, class TOutput>
class ITK_EXPORT RGBToRGBColorSpacePixelAccessor
{
public:
  /** Standard class typedefs. */
  typedef   RGBToRGBColorSpacePixelAccessor        Self;

 /** External typedef. It defines the external aspect
   * that this class will exhibit */
  typedef  Vector<TOutput,3>     ExternalType;

  /** Internal typedef. It defines the internal real
   * representation of data */
  typedef   RGBPixel<TInput>    InternalType;

  /** Write access to the RGBToLabColorSpace component */
  inline void Set( InternalType & output, const ExternalType & input ) const
    {
    output[0] = static_cast<TOutput>(input[0]); // R
    output[1] = static_cast<TOutput>(input[1]); // G
    output[2] = static_cast<TOutput>(input[2]); // B
    }

  /** Read access to the RGBToLabColorSpace component */
  inline ExternalType Get( const InternalType & input ) const
    {
    ExternalType output;
    output[0] = static_cast<TOutput>(input[0]); // R
    output[1] = static_cast<TOutput>(input[1]); // G
    output[2] = static_cast<TOutput>(input[2]); // B

    return output;
    }

private:
};

}  // end namespace Accessor
}  // end namespace itk

#endif
