#include "Utilities/ImageProcessing/WaveletTransform/WT2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/WTStruct2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/AbstractLifting2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/WaveletFilters.h"

// Include all lifting filters
#include "Utilities/ImageProcessing/WaveletTransform/LiftingFilters.h"

namespace spin
{
  /*! \brief Default constructor (forward transform)*/
  WT2D::WT2D(int lev, int f)
  {
    // And initialize other data structures
    bands = std::make_shared<pyramidStructWT>();
    forward = 1;// forward transform
    wtinfo.levels = lev;
    wtinfo.filter = f;
  }//end of function

  /*! \brief Default constructor (inverse transform)*/
  WT2D::WT2D(void* _obj)
  {
    WT2D* obj = static_cast<WT2D*>(_obj);
    // Check if the object exists
    if (!obj) return;

    // pass basic information
    wtinfo.levels = obj->GetInformation().levels;
    wtinfo.height = obj->GetInformation().height;
    wtinfo.width  = obj->GetInformation().width;
    wtinfo.filter = obj->GetInformation().filter;
    // copy the bands
    pyramidStructWT* pb = const_cast<pyramidStructWT*>(obj->GetBands());
    bands = std::make_shared<pyramidStructWT>(pb);

    // As this is an inverse transform. We need to set
    // the permutation matrix structure for interacling the approximation
    // and detail signals
    pMap = std::make_shared<pyramidPermutationMatrix>();
    this->Initialize(wtinfo.levels, wtinfo.height, wtinfo.width, pMap.get());
    forward = 0;// inverse transform
  }//end of function

  /*! \brief Default destructor */
  WT2D::~WT2D()
  {
  }//end of function

  /*! \brief Set the bands
   *  This function is used to set previously computed wavelet
   *  bands if using this class for inverting a wavelet transform.
   *  This function may also be used when computing the dual-tree
   *  complex wavelet transform.
   */
  void WT2D::SetBands(void* _pb)
  {
    pyramidStructWT* pb = static_cast<pyramidStructWT*>(_pb);
    bands.get()->pyramid_wt = pb->pyramid_wt;
  }//end of function

  /*! \brief Get the bands
   *  This function is used to get previously computed wavelet
   *  bands if we want to perform data analysis on the bands.
   */
  void WT2D::GetBands(void* _pb)
  {
    pyramidStructWT* pb = static_cast<pyramidStructWT*>(_pb);
    // Copy the band information
    pb->pyramid_wt = bands.get()->pyramid_wt;
  }//end of function

  /*! \brief Compute
   *  This function is used to compute either a forward or inverse
   *  discrete wavelet transform. Depending on the type of transform
   *  being implemented, the data will either be a set of bands (inverse)
   *  or a 2D image (forward).
   *  It is assumed that all required data would have been computed/enabled
   *  prior to calling this function.
   */
  int WT2D::Compute(void* data, void* output)
  {
    // Initialize the data structures
    std::shared_ptr<AbstractLifting2D> filter;
    WFilters t = static_cast<WFilters>(wtinfo.filter);
    switch(t)
    {
      case WFilters::NK13_19:
      {
        filter = std::make_shared<Lifting13_19_2D>();
        break;
      }
      case WFilters::NKQS_18A:
      {
        filter = std::make_shared<LiftingQS_18A_2D>();
        break;
      }
      case WFilters::NKQS_18B:
      {
        filter = std::make_shared<LiftingQS_18B_2D>();
        break;
      }
      default:
      {
        filter = std::make_shared<Lifting13_19_2D>();
        break;
      }
    }

    // Now, check if we are implementing a forward or backward transform
    if (forward==1)
    {
      // We are inmplenting a forward wavelet transform
      // We need a valid data to be input
      RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(data);
      if (!mat->data()) return -1;

      // We also check if the dimensions are valid w.r.t # of levels
      // of decomposition
      int height = mat->rows();
      int width  = mat->cols();
      // At this time, we check for the sanity of levels.
      int actLevels = this->CheckLevels(height,width);
      if (wtinfo.levels > actLevels) wtinfo.levels = actLevels;

      // Check if the # of levels of decomposition is valid
      if ( ((height>>wtinfo.levels)<<wtinfo.levels) != height) return -2;
      if ( ((width>>wtinfo.levels)<<wtinfo.levels) != width) return -3;
      wtinfo.height = height;
      wtinfo.width  = width;

      // Memory for intermediate data
      RMMatrix_Float dummy;
      std::vector<RMMatrix_Float> dTemp(2, dummy);
      // Do the decimation at all levels
      for (int l = 0; l < wtinfo.levels; ++l)
      {
        WTStruct2D l_bands;
        // 1.) Across the rows (decimate the columns)
        int h = filter.get()->Forward( mat, 1, false, &dTemp[0], &dTemp[1]);
        // 2.) Across the columns (decimate the rows)
        filter.get()->Forward( &dTemp[0], 0, false, &l_bands.LL, &l_bands.LH);
        filter.get()->Forward( &dTemp[1], 0, false, &l_bands.HL, &l_bands.HH);
        l_bands.height = l_bands.HL.rows();
        l_bands.width  = l_bands.HL.cols();
        // 3.) Place the bands in the map
        bands.get()->pyramid_wt[l]=l_bands;
        // 4.) Clear redundant data
        dTemp[0].resize(0,0);
        dTemp[1].resize(0,0);
        // 5.) Set the image for the next level to the LL image
        mat = &(bands.get()->pyramid_wt[l].LL);
        // 6.) Note: we do not have to preserve the previous LL subbands
        if (l > 0) bands.get()->pyramid_wt[l-1].LL.resize(0,0);
      }
    }//forward transform
    else
    {
      // Typecast the output
      RMMatrix_Float* outputR = static_cast<RMMatrix_Float*>(output);
      if (!outputR) return -1;

      // We are implenting an inverse wavelet transform
      // We need a valid data to be input
      if (!bands.get()) return -2;
      if (bands.get()->pyramid_wt.size() <= 0) return -3;
      // Also, the permutation maps should have been initialized here
      if(!pMap.get()) return -4;
      if (pMap.get()->pyramid_pMatrix.size() <= 0 || \
          pMap.get()->pyramid_pMatrix.size() != bands.get()->pyramid_wt.size()) return -5;

      // Memory for intermediate data
      RMMatrix_Float dummy;
      std::vector<RMMatrix_Float> dTemp(2, dummy);
      // Do the decimation at all levels
      for (int l = wtinfo.levels-1; l >=0 ; --l)
      {
        WTStruct2D* l_bands = &(bands.get()->pyramid_wt[l]);
        PermutationMatrix* p_Matrix = &(pMap.get()->pyramid_pMatrix[l]);
        // 1.) Across the columns (upsample the rows)
        filter.get()->Inverse(&l_bands->HL, &l_bands->HH, 0, false, p_Matrix, &dTemp[1]);
        filter.get()->Inverse(&l_bands->LL, &l_bands->LH, 0, false, p_Matrix, &dTemp[0]);
        // 2.) Across the rows (upsample the columns) and place the
        //     reconstructed band in the LL band of
        if (l != 0)
        {
          filter.get()->Inverse( &dTemp[0], &dTemp[1], 1, false, p_Matrix, &(bands.get()->pyramid_wt[l-1].LL));
        }
        else
        {
          // We merely populate the output
          filter.get()->Inverse( &dTemp[0], &dTemp[1], 1, false, p_Matrix, outputR);
        }

        // Free the intermdiate output
        dTemp[0].resize(0,0);
        dTemp[1].resize(0,0);
      }
    }//inverse transform
    return 1;
  }//end of function

  /*! \brief RecomposeBands
   *  This function is used to recompose bands as they would
   *  appear in a 2D image after wavelet transform. This is primarily
   *  used for display or for running encoders. The output is stored
   *  in output as a floating point matrix data.
   */
  int WT2D::RecomposeBands(void* _output, int treeIdx)
  {
    // sanity checks
    if (!bands.get()) return -1;
    if (bands.get()->pyramid_wt.size() <=0) return -2;

    // typecast the output
    RMMatrix_Float* output = static_cast<RMMatrix_Float*>(_output);

    // Set the dimensions of the image
    int height_half = bands.get()->pyramid_wt[0].HL.rows();
    int width_half  = bands.get()->pyramid_wt[0].HL.cols();

    // Check for discrepancies
    if (height_half <=0 || width_half <=0) return -3;

    int height = height_half<<1;
    int width  = width_half<<1;

    // Set the dimensions of the image
    (*output) = RMMatrix_Float::Zero(height, width);

    // Now, traverse through all levels and place the bands in the
    // respective location. Essentially, the bands are arranged as
    // LL  HL
    // LH  HH
    // at a given level.
    for (int i = 0; i < wtinfo.levels; ++i)
    {
      // Pad the HL band
      output->block(0,width_half,height_half,width_half) = bands.get()->pyramid_wt[i].HL;
      // Pad the HH band
      output->block(height_half,width_half,height_half,width_half) = bands.get()->pyramid_wt[i].HH;
      // Pad the LH band
      output->block(height_half,0,height_half,width_half) = bands.get()->pyramid_wt[i].LH;
      if (i == wtinfo.levels-1)
      {
        // if we are at the last level, pad the LL band
        output->block(0,0,height_half,width_half) = bands.get()->pyramid_wt[i].LL;
      }

      // decrement the dimension sizes
      width_half = width_half >> 1;
      height_half= height_half>> 1;
    }

    return 1;
  }//end of function

}//end of namespace
