#ifndef WTSTRUCT2D_H_
#define WTSTRUCT2D_H_
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <boost/container/map.hpp>
namespace spin
{
  /*! \brief This structure holds information about wavelet subbands
   * obtained after a maximal decimation
   */ 
  struct WTStruct2D 
  {
    // Constructors
    WTStruct2D(){}
    // Copy constructor 
    WTStruct2D(WTStruct2D* v)
    {
      LL      = v->LL;
      HL      = v->HL;
      LH      = v->LH;
      HH      = v->HH;
      filter  = v->filter;
      height  = v->height;
      width   = v->width;
    }
    
    RMMatrix_Float LL;//index 00
    RMMatrix_Float HL;//index 01
    RMMatrix_Float LH;//index 10
    RMMatrix_Float HH;//index 11
    // dimensions of subband at this level
    int height;
    int width;
    int filter;
  };//end of struct

  // A structure to hold wavelet bands at multiple levels
  struct pyramidStructWT
  {
    pyramidStructWT(){}
    ~pyramidStructWT()
    {
      pyramid_wt.clear();
    }
    
    pyramidStructWT(pyramidStructWT* p)
    {
      this->pyramid_wt = p->pyramid_wt;
    }
    
    boost::container::map<int,WTStruct2D> pyramid_wt;
  };//end of struct
  
  // A structure to hold dual tree wavelet bands at multiple levels
  struct pyramidStructDTCWT
  {
    pyramidStructDTCWT(){}
    ~pyramidStructDTCWT()
    {
      pyramid_dtcwt.clear();
    }
    pyramidStructDTCWT(pyramidStructDTCWT* p)
    {
      this->pyramid_dtcwt = p->pyramid_dtcwt;
    }
    
    boost::container::map<int, boost::container::map<int,WTStruct2D> > pyramid_dtcwt;
  };//end of struct
  
  /*! \brief A structure to hold a set of permutation matrices
   */
  struct PermutationMatrix
  {
    RMSparseMatrix_Float stage0_even;
    RMSparseMatrix_Float stage0_odd;
    RMSparseMatrix_Float stage1_even;
    RMSparseMatrix_Float stage1_odd;
  };//end of struct
  
  struct pyramidPermutationMatrix
  {
     ~pyramidPermutationMatrix()
    {
      pyramid_pMatrix.clear();
    }
    boost::container::map<int,PermutationMatrix> pyramid_pMatrix;
  };//end of struct
}
#endif
