#ifndef UWT2D_H_
#define UWT2D_H_
#include "AbstractClasses/AbstractWT2D.h"
#include <memory>
#include <map>

namespace spin
{
  // forward declarations
  struct pyramidStructWT;

  /*! \brief A structure to hold miscellaneous information about a wavelet transform*/
  struct WTInformation
  {
    int height;
    int width;
    int levels;
    int filter;
  };//end of struct

  /*! \brief  A placeholder for all undecimated 2D-Wavelet transforms */
  class UWT2D : public AbstractWT2D
  {
    public:
      /*! \brief default constructor (forward transform)*/
      UWT2D(int l, int f);

      /*! \brief default constructor (inverse transform)*/
      UWT2D(void* obj);

      /*! \brief default constructor (inverse transform)*/
      UWT2D(void* _pb, WTInformation* info);

      /*! \brief default destructor */
      ~UWT2D();

      /*! \brief Compute */
      int Compute(void* _img, void* _out);

      /*! \brief GetBands */
      const pyramidStructWT* GetBands(){ return bands.get();}

      /*! \brief GetBands */
      void GetBands(void* b);

      /*! \brief SetBands */
      void SetBands(void* pb);

      /*! \brief Recompose bands*/
      int RecomposeBands(void* outMat, int t=0){return 1;}

      /*! \brief GetInformation*/
      const WTInformation GetInformation(){return wtinfo;}
    private:
      std::shared_ptr<pyramidStructWT> bands;
      int forward;
      WTInformation wtinfo;
  };//end of class
}//end of namespace
#endif
