#include "Utilities/ImageProcessing/WaveletTransform/UWT2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/WTStruct2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/AbstractLifting2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/WaveletFilters.h"

// Include all lifting filters
#include "Utilities/ImageProcessing/WaveletTransform/LiftingFilters.h"
namespace spin
{
  /*! \brief Default constructor (forward transform)*/
  UWT2D::UWT2D(int lev, int f)
  {
    // And initialize other data structures
    bands = std::make_shared<pyramidStructWT>();
    forward = 1;// forward transform
    wtinfo.levels = lev;
    wtinfo.filter = f;
  }//end of function

  /*! \brief Default constructor (inverse transform)*/
  UWT2D::UWT2D(void* _obj)
  {
    UWT2D* obj = static_cast<UWT2D*>(_obj);
    // Check if the object exists
    if (!obj) return;

    // pass basic information
    wtinfo.levels = obj->GetInformation().levels;
    wtinfo.filter = obj->GetInformation().filter;
    // copy the bands
    pyramidStructWT* pb = const_cast<pyramidStructWT*>(obj->GetBands());
    bands = std::make_shared<pyramidStructWT>(pb);
    forward = 0;// inverse transform
  }//end of function

  /*! \brief Default destructor */
  UWT2D::~UWT2D()
  {
  }//end of function

  /*! \brief Default constructor (inverse transform)*/
  UWT2D::UWT2D(void* _pb, WTInformation* info)
  {
    // don't do anything if information is not set.
    if (!info) return;

    // typecast the information
    wtinfo.levels = info->levels;
    wtinfo.filter = info->filter;

    bands = std::make_shared<pyramidStructWT>();
    SetBands(_pb);
    forward = 0;// inverse transform
  }//end of function

  /*! \brief Set the bands
   *  This function is used to set previously computed wavelet
   *  bands if using this class for inverting a wavelet transform.
   *  This function may also be used when computing the dual-tree
   *  complex wavelet transform.
   */
  void UWT2D::SetBands(void* _pb)
  {
    pyramidStructWT* pb = static_cast<pyramidStructWT*>(_pb);
    bands.get()->pyramid_wt = pb->pyramid_wt;
  }//end of function

  /*! \brief Get the bands
   *  This function is used to get previously computed wavelet
   *  bands if we want to perform data analysis on the bands.
   */
  void UWT2D::GetBands(void* _pb)
  {
    pyramidStructWT* pb = static_cast<pyramidStructWT*>(_pb);
    // Copy the band information
    pb->pyramid_wt = bands.get()->pyramid_wt;
  }//end of function

  /*! \brief Compute
   *  This function is used to compute either a forward or inverse
   *  discrete wavelet transform. Depending on the type of transform
   *  being implemented, the data will either be a set of bands (inverse)
   *  or a 2D image (forward).
   *  It is assumed that all required data would have been computed/enabled
   *  prior to calling this function.
   */
  int UWT2D::Compute(void* data, void* output)
  {
    // Initialize the data structures
    std::shared_ptr<AbstractLifting2D> filter;
    WFilters t = static_cast<WFilters>(wtinfo.filter);
    switch(t)
    {
      case WFilters::NK13_19:
      {
        filter = std::make_shared<Lifting13_19_2D>();
        break;
      }
      case WFilters::NKQS_18A:
      {
        filter = std::make_shared<LiftingQS_18A_2D>();
        break;
      }
      case WFilters::NKQS_18B:
      {
        filter = std::make_shared<LiftingQS_18B_2D>();
        break;
      }
      default:
      {
        filter = std::make_shared<Lifting13_19_2D>();
        break;
      }
    }

    // Now, check if we are implementing a forward or backward transform
    if (forward==1)
    {
      // We are inmplenting a forward wavelet transform
      // We need a valid data to be input
      RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(data);
      if (!mat->data()) return -1;

      // We also check if the dimensions are valid w.r.t # of levels
      // of decomposition
      int height = mat->rows();
      int width  = mat->cols();
      wtinfo.height = height;
      wtinfo.width  = width;

      // Memory for intermediate data
      RMMatrix_Float dummy;
      std::vector<RMMatrix_Float> dTemp(2, dummy);
      // Do the computation at all levels
      for (int l = 0; l < wtinfo.levels; ++l)
      {
        WTStruct2D l_bands;
        // 1.) Filter Across the rows
        int h = filter.get()->Forward( mat, 1, true, &dTemp[0], &dTemp[1]);
        // 2.) Filter Across the columns
        filter.get()->Forward( &dTemp[0], 0, true, &l_bands.LL, &l_bands.LH);
        filter.get()->Forward( &dTemp[1], 0, true, &l_bands.HL, &l_bands.HH);
        l_bands.height = l_bands.HL.rows();
        l_bands.width  = l_bands.HL.cols();
        // 3.) Place the bands in the map
        bands.get()->pyramid_wt[l]=l_bands;
        // 4.) Clear redundant data
        dTemp[0].resize(0,0);
        dTemp[1].resize(0,0);
        // 5.) Set the image for the next level to the LL image
        mat = &(bands.get()->pyramid_wt[l].LL);
        // 6.) Note: we do not have to preserve the previous LL subbands
        if (l > 0) bands.get()->pyramid_wt[l-1].LL.resize(0,0);
      }
    }//forward transform
    else
    {
      // Typecast the output
      RMMatrix_Float* outputR = static_cast<RMMatrix_Float*>(output);
      if (!outputR) return -1;

      // We are implenting an inverse wavelet transform
      // We need a valid data to be input
      if (!bands.get()) return -2;
      if (bands.get()->pyramid_wt.size() <= 0) return -3;

      // Memory for intermediate data
      RMMatrix_Float dummy;
      std::vector<RMMatrix_Float> dTemp(2, dummy);
      // Do the decimation at all levels
      for (int l = wtinfo.levels-1; l >=0 ; --l)
      {
        WTStruct2D* l_bands = &(bands.get()->pyramid_wt[l]);
        // 1.) Across the columns (upsample the rows)
        filter.get()->Inverse(&l_bands->HL, &l_bands->HH, 0, true, NULL, &dTemp[1]);
        filter.get()->Inverse(&l_bands->LL, &l_bands->LH, 0, true, NULL, &dTemp[0]);
        // 2.) Across the rows (upsample the columns) and place the
        //     reconstructed band in the LL band of
        if (l != 0)
        {
          filter.get()->Inverse(&dTemp[0], &dTemp[1], 1, true, \
                                NULL, &(bands.get()->pyramid_wt[l-1].LL));
        }
        else
        {
          // We merely populate the output
          filter.get()->Inverse(&dTemp[0], &dTemp[1], 1, true, \
                                NULL, outputR);
        }

        // Free the intermdiate output
        dTemp[0].resize(0,0);
        dTemp[1].resize(0,0);
      }
    }//inverse transform
    return 1;
  }//end of function
}//end of namespace
