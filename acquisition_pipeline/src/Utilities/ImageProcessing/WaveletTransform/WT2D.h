#ifndef WT2D_H_
#define WT2D_H_
#include "AbstractClasses/AbstractWT2D.h"
#include <memory>
#include <map>

namespace spin 
{
  // forward declarations
  struct pyramidStructWT;
  struct pyramidPermutationMatrix;
  
  /*! \brief A structure to hold miscellaneous information about a wavelet transform*/
  struct WTInformation
  {
    int height;
    int width;
    int levels;
    int filter;
  };//end of struct
  
  /*! \brief  A placeholder for all 2D-Wavelet transforms */
  class WT2D : public AbstractWT2D
  {
    public:     
      /*! \brief default constructor (forward transform)*/
      WT2D(int l, int f);
      
      /*! \brief default constructor (inverse transform)*/
      WT2D(void* obj);
      
      /*! \brief default destructor */
      ~WT2D();
      
      /*! \brief Compute */
      int Compute(void* _img, void* _out);
      
      /*! \brief GetBands */
      const pyramidStructWT* GetBands(){ return bands.get();}
      
      /*! \brief GetBands */
      void GetBands(void* b);
      
      /*! \brief SetBands */
      void SetBands(void* pb);
      
      /*! \brief Recompose bands*/
      int RecomposeBands(void* outMat, int t=0);
      
      /*! \brief GetInformation*/
      const WTInformation GetInformation(){return wtinfo;}
    private:
      std::shared_ptr<pyramidStructWT> bands;
      std::shared_ptr<pyramidPermutationMatrix> pMap;
      int forward;
      WTInformation wtinfo;
  };//end of class 
}//end of namespace
#endif
