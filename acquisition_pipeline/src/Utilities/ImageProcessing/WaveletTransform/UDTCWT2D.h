#ifndef UDTCWT2D_H_
#define UDTCWT2D_H_
#include "AbstractClasses/AbstractWT2D.h"
#include <memory>
#include <map>

namespace spin
{
  // forward declarations
  struct pyramidStructDTCWT;

  /*! \brief A structure to hold miscellaneous information about a wavelet transform*/
  struct DTCWTInformation
  {
    int height;
    int width;
    int levels;
    int filter;
  };//end of struct

  /*! \brief  A placeholder for undecimated dual-tree CWT */
  class UDTCWT2D : public AbstractWT2D
  {
    public:
      /*! \brief default constructor (forward transform)*/
      UDTCWT2D(int l, int f);

      /*! \brief default constructor (inverse transform)*/
      UDTCWT2D(void* obj);

      /*! \brief default constructor (inverse transform)*/
      UDTCWT2D(void* obj, DTCWTInformation* info);

      /*! \brief default destructor */
      ~UDTCWT2D();

      /*! \brief Compute */
      int Compute(void* _img, void* _out);

      /*! \brief GetBands */
      const pyramidStructDTCWT* GetBands(){ return bands.get();}

      /*! \brief SetBands */
      void SetBands(void* pb);

      /*! \brief GetBands */
      void GetBands(void* b);

      /*! \brief Recompose bands*/
      int RecomposeBands(void* outMat, int t=0);
      
      /*! \brief GetInformation*/
      const DTCWTInformation GetInformation(){return dtcwtinfo;}
    private:
      std::shared_ptr<pyramidStructDTCWT> bands;
      int forward;
      DTCWTInformation dtcwtinfo;
    private:
      void InverseWaveletTransform(int t, void* _f1, void* _f2, void* p);
      void ForwardWaveletTransform(void* d, int t, void* _f1, void* _f2);
  };//end of class
}//end of namespace
#endif
