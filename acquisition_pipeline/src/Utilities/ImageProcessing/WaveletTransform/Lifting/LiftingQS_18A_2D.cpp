#include "Utilities/ImageProcessing/WaveletTransform/Lifting/LiftingQS_18A_2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/WTStruct2D.h"
#include "Utilities/MatrixUtils/MatrixUtils.h"

namespace spin
{
  // Define the coefficients of lifting stages
  const float lstageQS_18A[][2]={ {-0.52970   ,   0},
                                  {0.45816    ,   0.41364},
                                  {-1.2883    ,  -0.55893},
                                  {-0.045542  ,   0.50182},
                                  {2.7505     ,   0.32585},
                                  {-0.23464   ,  -0.18821},
                                  {1.0016     ,   2.6645},
                                  {0.12572    ,  -0.19045},
                                  {-0.74095   ,   2.18940},
                                  {-0.21596   ,   0},
                                  {0.47753    ,  -2.09411}
                               };

  /*! \brief The Foward lifting steps
   *  @_mat     : The data matrix.
   *  @_dir     : The direction (row/col) along which
   *              symmetric extension is needed
   *  @_udwt    : A flag indicating undecimated wavelet transform
   *  @_mat1    : The approximate data
   *  @_mat2    : The detail data
   *  At the end of the operation, _mat1 contains the approximate version
   *  of the signal, while _mat2 contains the detail version of the signal
   */
  int LiftingQS_18A_2D::Forward(void* _mat, int dir, bool _udwt,\
                                void* _mat1, void* _mat2)
  {
    // typecast the data and do some sanity checks
    RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(_mat);
    if (!mat->data()) return -1;

    // typecast the data and do some sanity checks
    RMMatrix_Float* mat_s = static_cast<RMMatrix_Float*>(_mat1);
    RMMatrix_Float* mat_d = static_cast<RMMatrix_Float*>(_mat2);

    if (!(_udwt))
    {
      // Step 1: Implement a Lazy Forward transform
      int p = this->Lazy_Forward(_mat, dir, _mat1, _mat2);
      if (p != 1) return -2;
    }
    else
    {
      // Stage 1: In an undecimated wavelet transform, we make copies of the
      //          data.
      (*mat_d) = (*mat)*0.5;
      (*mat_s) = (*mat)*0.5;
    }

    int cols = mat_d->cols();
    int rows = mat_d->rows();

    // Instantiate a MatrixUtils object
    MatrixUtils<RMMatrix_Float> mutils;

    // Implement the lifting steps here
    RMMatrix_Float temp;
    if (dir == 1)
    {
      // Step 1
      *mat_s = *mat_s - (*mat_d) * lstageQS_18A[0][0];
      // Step 2
      mutils.SymmetricPaddingOfCols(mat_s,0,1,&temp);
      *mat_d = *mat_d - temp.block(0,1,rows,cols)*lstageQS_18A[1][1] - (*mat_s)*lstageQS_18A[1][0];
      // Step 3
      mutils.SymmetricPaddingOfCols(mat_d,1,0,&temp);
      *mat_s = *mat_s - (*mat_d)*lstageQS_18A[2][1] - temp.block(0,0,rows,cols)*lstageQS_18A[2][0];
      // Step 4
      mutils.SymmetricPaddingOfCols(mat_s,0,1,&temp);
      *mat_d = *mat_d - temp.block(0,1,rows,cols)*lstageQS_18A[3][1] - (*mat_s)*lstageQS_18A[3][0];
      // Step 5
      mutils.SymmetricPaddingOfCols(mat_d,1,0,&temp);
      *mat_s = *mat_s - (*mat_d)*lstageQS_18A[4][1] - temp.block(0,0,rows,cols)*lstageQS_18A[4][0];
      // Step 6
      mutils.SymmetricPaddingOfCols(mat_s,0,1,&temp);
      *mat_d = *mat_d - temp.block(0,1,rows,cols)*lstageQS_18A[5][1] - (*mat_s)*lstageQS_18A[5][0];
      // Step 7
      mutils.SymmetricPaddingOfCols(mat_d,1,0,&temp);
      *mat_s = *mat_s - (*mat_d)*lstageQS_18A[6][1] - temp.block(0,0,rows,cols)*lstageQS_18A[6][0];
      // Step 8
      mutils.SymmetricPaddingOfCols(mat_s,0,1,&temp);
      *mat_d = *mat_d - temp.block(0,1,rows,cols)*lstageQS_18A[7][1] - (*mat_s)*lstageQS_18A[7][0];
      // Step 9
      mutils.SymmetricPaddingOfCols(mat_d,1,0,&temp);
      *mat_s = *mat_s - (*mat_d)*lstageQS_18A[8][1] - temp.block(0,0,rows,cols)*lstageQS_18A[8][0];
      // Step 10
      *mat_d = *mat_d - (*mat_s) * lstageQS_18A[9][0];
    }
    else
    {
      // Step 1
      *mat_s = *mat_s - (*mat_d) * lstageQS_18A[0][0];
      // Step 2
      mutils.SymmetricPaddingOfRows(mat_s,0,1,&temp);
      *mat_d = *mat_d - temp.block(1,0,rows,cols)*lstageQS_18A[1][1] - (*mat_s)*lstageQS_18A[1][0];
      // Step 3
      mutils.SymmetricPaddingOfRows(mat_d,1,0,&temp);
      *mat_s = *mat_s - (*mat_d)*lstageQS_18A[2][1] - temp.block(0,0,rows,cols)*lstageQS_18A[2][0];
      // Step 4
      mutils.SymmetricPaddingOfRows(mat_s,0,1,&temp);
      *mat_d = *mat_d - temp.block(1,0,rows,cols)*lstageQS_18A[3][1] - (*mat_s)*lstageQS_18A[3][0];
      // Step 5
      mutils.SymmetricPaddingOfRows(mat_d,1,0,&temp);
      *mat_s = *mat_s - (*mat_d)*lstageQS_18A[4][1] - temp.block(0,0,rows,cols)*lstageQS_18A[4][0];
      // Step 6
      mutils.SymmetricPaddingOfRows(mat_s,0,1,&temp);
      *mat_d = *mat_d - temp.block(1,0,rows,cols)*lstageQS_18A[5][1] - (*mat_s)*lstageQS_18A[5][0];
      // Step 7
      mutils.SymmetricPaddingOfRows(mat_d,1,0,&temp);
      *mat_s = *mat_s - (*mat_d)*lstageQS_18A[6][1] - temp.block(0,0,rows,cols)*lstageQS_18A[6][0];
      // Step 8
      mutils.SymmetricPaddingOfRows(mat_s,0,1,&temp);
      *mat_d = *mat_d - temp.block(1,0,rows,cols)*lstageQS_18A[7][1] - (*mat_s)*lstageQS_18A[7][0];
      // Step 9
      mutils.SymmetricPaddingOfRows(mat_d,1,0,&temp);
      *mat_s = *mat_s - (*mat_d)*lstageQS_18A[8][1] - temp.block(0,0,rows,cols)*lstageQS_18A[8][0];
      // Step 10
      *mat_d = *mat_d - (*mat_s) * lstageQS_18A[9][0];
    }

    // The scaling operations
    float K1 = -lstageQS_18A[10][0];
    float K2 = (1.0-1.0/K1);
    float K3 = K2 * (-1.0/K1);
    *mat_s = *mat_s + *mat_d;
    *mat_d = *mat_d - (*mat_s)*K2;

    // The final approximate signal
    *mat_s = *mat_s - (*mat_d)*K1;
    // The final detail signal
    *mat_d = *mat_d - (*mat_s)*K3;
    return 1;
  }//end of function

  /*! \brief The Inverse lifting steps
   *  Observe the lifting steps performed in the forward process. Retrace the steps
   *  in an exact opposite manner culminating in an inverse lifting transform.
   *  @_mat1    : The approximate signal
   *  @_mat2    : The detail signal
   *  @_dir     : The direction (row/col) along which
   *              symmetric extension is needed
   *  @_udwt    : If an undecimated wavelet transform is being implemented
   *  @_p       : The permutation matrices for interleaving
   *  @_mat     : The reconstructed matrix
   */
  int LiftingQS_18A_2D::Inverse(void* _mat1, void* _mat2, int dir, \
                                bool _udwt, void* _p, \
                                void* _mat)
  {
    // typecast the data and do some sanity checks
    RMMatrix_Float* mat_s = static_cast<RMMatrix_Float*>(_mat1);
    if (!mat_s->data()) return -1;

    RMMatrix_Float* mat_d = static_cast<RMMatrix_Float*>(_mat2);
    if (!mat_d->data()) return -2;

    // Check if the dimensions are correct
    if ((mat_s->rows() != mat_d->rows()) || \
        (mat_s->cols() != mat_d->cols()) ) return -3;

    int cols = mat_d->cols();
    int rows = mat_d->rows();

    float K1 = -lstageQS_18A[10][0];
    float K2 = (1.0-1.0/K1);
    float K3 = K2 * (-1.0/K1);

    // The detail signal
    *mat_d = *mat_d + (*mat_s)*K3;
    // The approximate signal
    *mat_s = *mat_s + (*mat_d)*K1;

    // The scaling operation
    *mat_d = *mat_d + (*mat_s)*K2;
    *mat_s = *mat_s - *mat_d;

    // Instantiate a MatrixUtils object
    MatrixUtils<RMMatrix_Float> mutils;
    RMMatrix_Float temp;
    // Implement the lifting steps here
    if (dir == 1)
    {
      // Step 1
      *mat_d = *mat_d + (*mat_s) * lstageQS_18A[9][0];
      // Step 2
      mutils.SymmetricPaddingOfCols(mat_d,1,0,&temp);
      *mat_s = *mat_s + (*mat_d)*lstageQS_18A[8][1] + temp.block(0,0,rows,cols)*lstageQS_18A[8][0];
      // Step 3
      mutils.SymmetricPaddingOfCols(mat_s,0,1,&temp);
      *mat_d = *mat_d + temp.block(0,1,rows,cols)*lstageQS_18A[7][1] + (*mat_s)*lstageQS_18A[7][0];
      // Step 4
      mutils.SymmetricPaddingOfCols(mat_d,1,0,&temp);
      *mat_s = *mat_s + (*mat_d)*lstageQS_18A[6][1] + temp.block(0,0,rows,cols)*lstageQS_18A[6][0];
      // Step 5
      mutils.SymmetricPaddingOfCols(mat_s,0,1,&temp);
      *mat_d = *mat_d + temp.block(0,1,rows,cols)*lstageQS_18A[5][1] + (*mat_s)*lstageQS_18A[5][0];
      // Step 6
      mutils.SymmetricPaddingOfCols(mat_d,1,0,&temp);
      *mat_s = *mat_s + (*mat_d)*lstageQS_18A[4][1] + temp.block(0,0,rows,cols)*lstageQS_18A[4][0];
      // Step 7
      mutils.SymmetricPaddingOfCols(mat_s,0,1,&temp);
      *mat_d = *mat_d + temp.block(0,1,rows,cols)*lstageQS_18A[3][1] + (*mat_s)*lstageQS_18A[3][0];
      // Step 8
      mutils.SymmetricPaddingOfCols(mat_d,1,0,&temp);
      *mat_s = *mat_s + (*mat_d)*lstageQS_18A[2][1] + temp.block(0,0,rows,cols)*lstageQS_18A[2][0];
      // Step 9
      mutils.SymmetricPaddingOfCols(mat_s,0,1,&temp);
      *mat_d = *mat_d + temp.block(0,1,rows,cols)*lstageQS_18A[1][1] + (*mat_s)*lstageQS_18A[1][0];
      // Step 10
      *mat_s = *mat_s + (*mat_d) * lstageQS_18A[0][0];
    }
    else
    {
      // Step 1
      *mat_d = *mat_d + (*mat_s) * lstageQS_18A[9][0];
      // Step 2
      mutils.SymmetricPaddingOfRows(mat_d,1,0,&temp);
      *mat_s = *mat_s + (*mat_d)*lstageQS_18A[8][1] + temp.block(0,0,rows,cols)*lstageQS_18A[8][0];
      // Step 3
      mutils.SymmetricPaddingOfRows(mat_s,0,1,&temp);
      *mat_d = *mat_d + temp.block(1,0,rows,cols)*lstageQS_18A[7][1] + (*mat_s)*lstageQS_18A[7][0];
      // Step 4
      mutils.SymmetricPaddingOfRows(mat_d,1,0,&temp);
      *mat_s = *mat_s + (*mat_d)*lstageQS_18A[6][1] + temp.block(0,0,rows,cols)*lstageQS_18A[6][0];
      // Step 5
      mutils.SymmetricPaddingOfRows(mat_s,0,1,&temp);
      *mat_d = *mat_d + temp.block(1,0,rows,cols)*lstageQS_18A[5][1] + (*mat_s)*lstageQS_18A[5][0];
      // Step 6
      mutils.SymmetricPaddingOfRows(mat_d,1,0,&temp);
      *mat_s = *mat_s + (*mat_d)*lstageQS_18A[4][1] + temp.block(0,0,rows,cols)*lstageQS_18A[4][0];
      // Step 7
      mutils.SymmetricPaddingOfRows(mat_s,0,1,&temp);
      *mat_d = *mat_d + temp.block(1,0,rows,cols)*lstageQS_18A[3][1] + (*mat_s)*lstageQS_18A[3][0];
      // Step 8
      mutils.SymmetricPaddingOfRows(mat_d,1,0,&temp);
      *mat_s = *mat_s + (*mat_d)*lstageQS_18A[2][1] + temp.block(0,0,rows,cols)*lstageQS_18A[2][0];
      // Step 9
      mutils.SymmetricPaddingOfRows(mat_s,0,1,&temp);
      *mat_d = *mat_d + temp.block(1,0,rows,cols)*lstageQS_18A[1][1] + (*mat_s)*lstageQS_18A[1][0];
      // Step 10
      *mat_s = *mat_s + (*mat_d) * lstageQS_18A[0][0];
    }

    if (! (_udwt))
    {
      // Finally, we implement a Lazy Inverse transform and populate the output
      int p = this->Lazy_Inverse(_mat1, _mat2, dir, _p, _mat);
      if (p != 1) return p;
    }
    else
    {
      // Finally, we implement a summation as part of the last stage of an
      // undecimated wavelet transform.
      RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(_mat);
      (*mat) = (*mat_s)+(*mat_d);
    }
    return 1;
  }//end of function
}//end of namespace
