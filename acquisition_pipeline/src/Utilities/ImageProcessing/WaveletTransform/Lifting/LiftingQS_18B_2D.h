#ifndef LIFTINGQS_18B_2D_H
#define LIFTINGQS_18B_2D_H
#include "Utilities/ImageProcessing/WaveletTransform/AbstractLifting2D.h"

namespace spin
{
  class LiftingQS_18B_2D : public AbstractLifting2D
  {
    public:
      LiftingQS_18B_2D(){}
      // The forward lifting step along a direction
      int Forward( void* _mat, int dir, bool _udwt, void* _mat1, void* _mat2);
      // The inverse Lifting step along a direction
      int Inverse( void* _mat1, void* _mat2, int dir, bool _udwt, void* _p, void* _mat);
  };//end of class
};//end of function
#endif
