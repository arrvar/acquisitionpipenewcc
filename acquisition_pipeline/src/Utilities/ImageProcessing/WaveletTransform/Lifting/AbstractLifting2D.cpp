#include "Utilities/ImageProcessing/WaveletTransform/AbstractLifting2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/WTStruct2D.h"
#include <iostream>

namespace spin
{
  /*! \brief The functions described in this file pertain to
   *  Lazy-wavelet transform. In this, the signal is split into two
   *  components; even and odd (implicitly assuming that the data is
   *  of even length). 
   *  Furthermore, the dimension along which the lazy wavelet
   *  transform needs to be effected is also specified. This is
   *  primarily the first step during a forward wavelet transform, and
   *  the last step in an inverse wavelet transform.
   */

  /*! \brief Lazy_forward
   *  This is a 2D forward lazy wavelet transform. The data is split into its
   *  even and odd components and placed in a data structure given by WTStruct2D.
   *  Typically, we follow a convetion of applying the LWT along rows (y-direction)
   *  first, followed by along columns (x-direction).
   *  @_mat         : The input matrix that is being decimated
   *  @_direction   : The direction of transform :1 along x-axis/cols,
   *                                             :0 along y-axis/rows
   *  @_outMat1     : The approximate signal (will always be even)
   *  @_outMat2     : The detail sinal (will be even)
   */
  int AbstractLifting2D::Lazy_Forward(void* _mat, int direction, \
                                      void* _outMat1, \
                                      void* _outMat2)
  {
    // Sanity checks
    RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(_mat);
    if (!mat->data()) return -1;
    if (direction <=0 && direction > 1) return -2;
    int width = mat->cols();
    int height= mat->rows();
    if ( (direction == 0) && (((height>>1)<<1) != height) ) return -3;
    if ( (direction == 1) && (((width>>1)<<1) != width) ) return -4;

    // The even components
    RMMatrix_Float* mat1 = static_cast<RMMatrix_Float*>(_outMat1);
    // The odd components
    RMMatrix_Float* mat2 = static_cast<RMMatrix_Float*>(_outMat2);

    //https://stackoverflow.com/questions/20368446/extract-every-other-row-or-column-of-an-eigen-matrix-as-a-new-matrix
    //https://stackoverflow.com/questions/21491907/eigen-get-a-matrix-from-a-map
    //The following steps only work with column-major matrices.
    CMMatrix_Float matC = (*mat);
    if (direction ==1)
    {
      // We are decimating the columns (i.e., along x-axis)
      const int dim1 = height;
      const int dim2 = width>>1;
      // even columns
      (*mat1) = CMMatrix_Float::Map(matC.data(),dim1,dim2,Eigen::OuterStride<>(2*dim1));
      // odd columns
      (*mat2) = CMMatrix_Float::Map(matC.data()+dim1,dim1,dim2,Eigen::OuterStride<>(2*dim1));
    }
    else
    {
      // We are decimating the rows (i.e., along x-axis)
      const int dim1 = height>>1;
      const int dim2 = width;
      // even rows
      (*mat1) = CMMatrix_Float::Map(matC.data(),dim1,dim2, Eigen::Stride<Eigen::Dynamic,2>(height,2));
      (*mat2) = CMMatrix_Float::Map(matC.data()+1,dim1,dim2, Eigen::Stride<Eigen::Dynamic,2>(height,2));
    }

    return 1;
  }//end of function

  /*! \brief Lazy_Inverse
   *  This is a 2D inverse lazy wavelet transform. Two matrices containing data (and having the same
   *  dimensions) are interleaved so as to produce a concatenated matrix. Instead of looping through
   *  the specified dimension, we implement the process using matrix multiplication. It is assumed
   *  that the permutation matrices have been created prior to calling this function.
   */
  int AbstractLifting2D::Lazy_Inverse(void* _inMat1, void* _inMat2, int direction, void* _pMat, void* _outMat)
  {
    // Sanity checks
    RMMatrix_Float* mat1 = static_cast<RMMatrix_Float*>(_inMat1);
    if (!mat1->data()) return -1;

    RMMatrix_Float* mat2 = static_cast<RMMatrix_Float*>(_inMat2);
    if (!mat2->data()) return -2;

    if (direction <=0 && direction > 1) return -3;

    PermutationMatrix* pMat = static_cast<PermutationMatrix*>(_pMat);
    if (!pMat) return -4;

    RMMatrix_Float* outMat = static_cast<RMMatrix_Float*>(_outMat);

    int width1 = mat1->cols();
    int height1= mat1->rows();
    int width2 = mat2->cols();
    int height2= mat2->rows();

    if (width1 != width2) return -5;
    if (height1 != height2) return -6;

    if (direction == 1)
    {
      // The interlacing
      *outMat = (*mat1)*pMat->stage1_even + (*mat2)*pMat->stage1_odd;
    }
    else
    {
      // The interlacing
      *outMat = pMat->stage0_even*(*mat1) + pMat->stage0_odd*(*mat2);
    }

    return 1;
  }//end of function

}//end of namespace
