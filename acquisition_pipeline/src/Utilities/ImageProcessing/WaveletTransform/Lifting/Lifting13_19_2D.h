#ifndef LIFTING13_19_2D_H
#define LIFTING13_19_2D_H
#include "Utilities/ImageProcessing/WaveletTransform/AbstractLifting2D.h"

namespace spin
{
  class Lifting13_19_2D : public AbstractLifting2D
  {
    public:
      Lifting13_19_2D(){}
      // The forward lifting step along a direction
      int Forward( void* _mat, int dir, bool _udwt, void* _mat1, void* _mat2);
      // The inverse Lifting step along a direction
      int Inverse( void* _mat1, void* _mat2, int dir, bool _udwt, void* _p, void* _mat);
  };//end of class
};//end of function
#endif
