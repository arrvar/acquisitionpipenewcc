#include "Utilities/ImageProcessing/WaveletTransform/Lifting/Lifting13_19_2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/WTStruct2D.h"
#include "Utilities/MatrixUtils/MatrixUtils.h"

namespace spin
{
  // Define the coefficients of lifting stages
  const float lstage13_19[][2]={{0.0375   ,-0.2375},
                                {-0.066964, 0.424107},
                                {0.039375 ,-0.249375},
                                {0.98995  , 1.01015}
                               };

  /*! \brief The Foward lifting steps
   *  @_mat     : The data matrix.
   *  @_dir     : The direction (row/col) along which
   *              symmetric extension is needed
   *  @_udwt    : A flag that indicates if an undecimated wavelet transform
   *              is sought. If so, then the lazy wavelet transform is not
   *              performed.
   *  @_mat1    : The approximate data
   *  @_mat2    : The detail data
   *  At the end of the operation, _mat1 contains the approximate version
   *  of the signal, while _mat2 contains the detail version of the signal
   */
  int Lifting13_19_2D::Forward( void* _mat, int dir, bool _udwt,\
                                void* _mat1, void* _mat2)
  {
    // typecast the data and do some sanity checks
    RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(_mat);
    if (!mat->data()) return -1;

    // typecast the data and do some sanity checks
    RMMatrix_Float* mat_d = static_cast<RMMatrix_Float*>(_mat1);
    RMMatrix_Float* mat_s = static_cast<RMMatrix_Float*>(_mat2);

    // Stage 1: Implement a Lazy Forward transform
    if(! (_udwt) )
    {
      int p = this->Lazy_Forward(_mat, dir, _mat1, _mat2);
      if (p != 1) return -2;
    }
    else
    {
      // Stage 1: In an undecimated wavelet transform, we make copies of the
      //          data.
      (*mat_d) = (*mat)*0.5;
      (*mat_s) = (*mat)*0.5;
    }

    int cols = mat_d->cols();
    int rows = mat_d->rows();

    // Instantiate a MatrixUtils object
    MatrixUtils<RMMatrix_Float> mutils;
    // Implement the lifting steps here
    RMMatrix_Float temp;
    if (dir == 1)
    {
      // Lifting steps across columns
      // Stage 2 := s_1[n] = a_1{d_0[n+1]+d_0[n-2]} + a_2{d_o[n]+d_o[n-1]} + s_o[n]
      // Do not modify the approximate data and pad the detail data
      mutils.SymmetricPaddingOfCols(mat_d,1,2,&temp);
      *mat_s =  *mat_s + \
                (temp.block(0,0,rows,cols)+temp.block(0,3,rows,cols))*lstage13_19[0][0] + \
                (temp.block(0,1,rows,cols)+temp.block(0,2,rows,cols))*lstage13_19[0][1];

      // Stage 3 := d_1[n] = d_0[n] + b_1{s_1[n+2]+s_1[n-1]} + b_2{s_1[n]+s_1[n+1]}
      // Do not modify the detail data and pad the approximate data
      mutils.SymmetricPaddingOfCols(mat_s,2,1,&temp);
      *mat_d =  *mat_d + \
                (temp.block(0,0,rows,cols)+temp.block(0,3,rows,cols))*lstage13_19[1][0] + \
                (temp.block(0,1,rows,cols)+temp.block(0,2,rows,cols))*lstage13_19[1][1];

      // Lifting steps across columns
      // Stage 4 := s_1[n] = a_1{d_0[n+1]+d_0[n-2]} + a_2{d_o[n]+d_o[n-1]} + s_o[n]
      // Do not modify the approximate data and pad the detail data
      mutils.SymmetricPaddingOfCols(mat_d,1,2,&temp);
      *mat_s =  *mat_s + \
                (temp.block(0,0,rows,cols)+temp.block(0,3,rows,cols))*lstage13_19[2][0] + \
                (temp.block(0,1,rows,cols)+temp.block(0,2,rows,cols))*lstage13_19[2][1];
    }
    else
    {
      // Stage 2 := s_1[n] = a_1{d_0[n+1]+d_0[n-2]} + a_2{d_o[n]+d_o[n-1]} + s_o[n]
      // Do not modify the approximate data and pad the detail data
      mutils.SymmetricPaddingOfRows(mat_d,1,2,&temp);
      *mat_s =  *mat_s + \
                (temp.block(0,0,rows,cols)+temp.block(3,0,rows,cols))*lstage13_19[0][0] + \
                (temp.block(1,0,rows,cols)+temp.block(2,0,rows,cols))*lstage13_19[0][1];

      // Stage 3 := d_1[n] = d_0[n] + b_1{s_1[n+2]+s_1[n-1]} + b_2{s_1[n]+s_1[n+1]}
      // Do not modify the detail data and pad the approximate data
      mutils.SymmetricPaddingOfRows(mat_s,2,1,&temp);
      *mat_d =  *mat_d + \
                (temp.block(0,0,rows,cols)+temp.block(3,0,rows,cols))*lstage13_19[1][0] + \
                (temp.block(1,0,rows,cols)+temp.block(2,0,rows,cols))*lstage13_19[1][1];

      // Stage 4 := s_1[n] = a_1{d_0[n+1]+d_0[n-2]} + a_2{d_o[n]+d_o[n-1]} + s_o[n]
      // Do not modify the approximate data and pad the detail data
      mutils.SymmetricPaddingOfRows(mat_d,1,2,&temp);
      *mat_s =  *mat_s + \
                (temp.block(0,0,rows,cols)+temp.block(3,0,rows,cols))*lstage13_19[2][0] + \
                (temp.block(1,0,rows,cols)+temp.block(2,0,rows,cols))*lstage13_19[2][1];
    }

    // Stage 5 : The scaling operations
    float K1 = lstage13_19[3][0];
    float K2 = (1.0-1.0/K1);
    float K3 = K2 * (-1.0/K1);
    *mat_s = *mat_s - *mat_d;
    *mat_d = *mat_d + (*mat_s)*K2;

    // Stage 6 : The final approximate signal
    *mat_s = *mat_s + (*mat_d)*K1;
    // Stage 6 : The final detail signal
    *mat_d = *mat_d + (*mat_s)*K3;
    return 1;
  }//end of function

  /*! \brief The Inverse lifting steps
   *  Observe the lifting steps performed in the forward process. Retrace the steps
   *  in an exact opposite manner culminating in an inverse lifting transform.
   *  @_mat1    : The approximate signal
   *  @_mat2    : The detail signal
   *  @_dir     : The direction (row/col) along which
   *              symmetric extension is needed
   *  @_udwt    : A flag to check if an undecimated inverse wavelet transform
   *              is sought. If so, the next parameter is ignored.
   *  @_p       : The permutation matrices for interleaving
   *  @_mat     : The reconstructed matrix
   */
  int Lifting13_19_2D::Inverse( void* _mat1, void* _mat2, int dir, \
                                bool _udwt, void* _p, \
                                void* _mat)
  {
    // typecast the data and do some sanity checks
    RMMatrix_Float* mat_s = static_cast<RMMatrix_Float*>(_mat2);
    if (!mat_s->data()) return -1;

    RMMatrix_Float* mat_d = static_cast<RMMatrix_Float*>(_mat1);
    if (!mat_d->data()) return -2;

    // Check if the dimensions are correct
    if ((mat_s->rows() != mat_d->rows()) || \
        (mat_s->cols() != mat_d->cols()) ) return -3;

    int cols = mat_d->cols();
    int rows = mat_d->rows();

    float K1 = lstage13_19[3][0];
    float K2 = (1.0-1.0/K1);
    float K3 = K2 * (-1.0/K1);

    // Stage 1 : The detail signal
    *mat_d = *mat_d - (*mat_s)*K3;
    // Stage 1 : The approximate signal
    *mat_s = *mat_s - (*mat_d)*K1;

    // Stage 2 : The scaling operation
    *mat_d = *mat_d - (*mat_s)*K2;
    *mat_s = *mat_s + *mat_d;

    // Instantiate a MatrixUtils object
    MatrixUtils<RMMatrix_Float> mutils;

    // Implement the lifting steps here
    RMMatrix_Float temp;
    if (dir == 1)
    {
      // Stage 3 := s_1[n] = s_0[n] - a_1{d_0[n+1]+d_0[n-2]} - a_2{d_o[n]+d_o[n-1]}
      // Do not modify the approximate data and pad the detail data
      mutils.SymmetricPaddingOfCols(mat_d,1,2,&temp);
      *mat_s =  *mat_s - \
                (temp.block(0,0,rows,cols)+temp.block(0,3,rows,cols))*lstage13_19[2][0] - \
                (temp.block(0,1,rows,cols)+temp.block(0,2,rows,cols))*lstage13_19[2][1];

      // Stage 4 := d_1[n] = d_0[n] - b_1{s_1[n+2]+s_1[n-1]} - b_2{s_1[n]+s_1[n+1]}
      // Do not modify the detail data and pad the approximate data
      mutils.SymmetricPaddingOfCols(mat_s,2,1,&temp);
      *mat_d =  *mat_d - \
                (temp.block(0,0,rows,cols)+temp.block(0,3,rows,cols))*lstage13_19[1][0] - \
                (temp.block(0,1,rows,cols)+temp.block(0,2,rows,cols))*lstage13_19[1][1];

      // Stage 5 := s_1[n] = s_0[n] - a_1{d_0[n+1]+d_0[n-2]} - a_2{d_o[n]+d_o[n-1]}
      // Do not modify the approximate data and pad the detail data
      mutils.SymmetricPaddingOfCols(mat_d,1,2,&temp);
      *mat_s =  *mat_s - \
                (temp.block(0,0,rows,cols)+temp.block(0,3,rows,cols))*lstage13_19[0][0] - \
                (temp.block(0,1,rows,cols)+temp.block(0,2,rows,cols))*lstage13_19[0][1];
    }
    else
    {
      // Stage 3 := s_1[n] = s_0[n] - a_1{d_0[n+1]+d_0[n-2]} - a_2{d_o[n]+d_o[n-1]}
      // Do not modify the approximate data and pad the detail data
      mutils.SymmetricPaddingOfRows(mat_d,1,2,&temp);
      *mat_s =  *mat_s - \
                (temp.block(0,0,rows,cols)+temp.block(3,0,rows,cols))*lstage13_19[2][0] - \
                (temp.block(1,0,rows,cols)+temp.block(2,0,rows,cols))*lstage13_19[2][1];

      // Stage 4 := d_1[n] = d_0[n] - b_1{s_1[n+2]+s_1[n-1]} - b_2{s_1[n]+s_1[n+1]}
      // Do not modify the detail data and pad the approximate data
      mutils.SymmetricPaddingOfRows(mat_s,2,1,&temp);
      *mat_d =  *mat_d - \
                (temp.block(0,0,rows,cols)+temp.block(3,0,rows,cols))*lstage13_19[1][0] - \
                (temp.block(1,0,rows,cols)+temp.block(2,0,rows,cols))*lstage13_19[1][1];

      // Stage 5 := s_1[n] = s_0[n] - a_1{d_0[n+1]+d_0[n-2]} - a_2{d_o[n]+d_o[n-1]}
      // Do not modify the approximate data and pad the detail data
      mutils.SymmetricPaddingOfRows(mat_d,1,2,&temp);
      *mat_s =  *mat_s - \
                (temp.block(0,0,rows,cols)+temp.block(3,0,rows,cols))*lstage13_19[0][0] - \
                (temp.block(1,0,rows,cols)+temp.block(2,0,rows,cols))*lstage13_19[0][1];
    }

    if (! (_udwt))
    {
      // Finally, we implement a Lazy Inverse transform and populate the output
      int p = this->Lazy_Inverse(_mat1, _mat2, dir, _p, _mat);
      if (p != 1) return p;
    }
    else
    {
      // Finally, we implement a summation as part of the last stage of an
      // undecimated wavelet transform.
      RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(_mat);
      (*mat) = (*mat_s)+(*mat_d);
    }
    return 1;
  }//end of function
}//end of namespace
