#include "Utilities/ImageProcessing/WaveletTransform/DTCWT2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/WTStruct2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/AbstractLifting2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/WaveletFilters.h"
#include "Utilities/ImageProcessing/WaveletTransform/LiftingFilters.h"
#include <boost/thread.hpp>
#include <boost/scoped_array.hpp>
#include <iostream>

namespace spin
{
  /*! \brief Default constructor (forward transform)*/
  DTCWT2D::DTCWT2D(int lev, int f)
  {
    // Instantiate the bands object
    bands = std::make_shared<pyramidStructDTCWT>();
    forward = 1;// forward transform
    dtcwtinfo.levels = lev;
    dtcwtinfo.filter = f;
  }//end of function

  /*! \brief Default constructor (inverse transform)*/
  DTCWT2D::DTCWT2D(void* _obj)
  {
    DTCWT2D* obj = static_cast<DTCWT2D*>(_obj);

    // Check if the object exists
    if (!obj) return;

    // pass basic information
    dtcwtinfo.levels = obj->GetInformation().levels;
    dtcwtinfo.height = obj->GetInformation().height;
    dtcwtinfo.width  = obj->GetInformation().width;
    dtcwtinfo.filter = obj->GetInformation().filter;

    // copy the bands
    pyramidStructDTCWT* pb = const_cast<pyramidStructDTCWT*>(obj->GetBands());
    bands = std::make_shared<pyramidStructDTCWT>(pb);

    // As this is an inverse transform. We need to set
    // the permutation matrix structure for interacling the approximation
    // and detail signals
    pMap = std::make_shared<pyramidPermutationMatrix>();
    this->Initialize(dtcwtinfo.levels, dtcwtinfo.height, dtcwtinfo.width, pMap.get());
    forward = 0;// inverse transform
  }//end of function

  /*! \brief Default constructor (inverse transform)*/
  DTCWT2D::DTCWT2D(void* _pb, DTCWTInformation* info)
  {
    // don't do anything if information is not set.
    if (!info) return;

    // typecast the information
    dtcwtinfo.levels = info->levels;
    dtcwtinfo.height = info->height;
    dtcwtinfo.width  = info->width;
    dtcwtinfo.filter = info->filter;

    bands = std::make_shared<pyramidStructDTCWT>();
    SetBands(_pb);
    // As this is an inverse transform. We need to set
    // the permutation matrix structure for interacling the approximation
    // and detail signals
    pMap = std::make_shared<pyramidPermutationMatrix>();
    this->Initialize(dtcwtinfo.levels, dtcwtinfo.height, dtcwtinfo.width, pMap.get());
    forward = 0;// inverse transform
  }//end of function

  /*! \brief Default destructor */
  DTCWT2D::~DTCWT2D()
  {
  }//end of function

  /*! \brief Set the bands
   *  This function is used to set previously computed wavelet
   *  bands if using this class for inverting a wavelet transform.
   *  This function may also be used when computing the dual-tree
   *  complex wavelet transform.
   */
  void DTCWT2D::SetBands(void* _pb)
  {
    pyramidStructDTCWT* pb = static_cast<pyramidStructDTCWT*>(_pb);
    bands.get()->pyramid_dtcwt = pb->pyramid_dtcwt;
  }//end of function

  /*! \brief Get the bands
   *  This function is used to get previously computed wavelet
   *  bands if we want to perform data analysis on the bands.
   */
  void DTCWT2D::GetBands(void* _pb)
  {
    pyramidStructDTCWT* pb = static_cast<pyramidStructDTCWT*>(_pb);
    // Copy the band information
    pb->pyramid_dtcwt = bands.get()->pyramid_dtcwt;
  }//end of function

  /*! \brief ForwardWaveletTransform
   *  This function implements a forward wavelet transform
   *  associated with a single tree of a dual tree complex wavelet transform.
   */
  void DTCWT2D::ForwardWaveletTransform(void* data, int treeIdx, \
                                        void* _filt1, void* _filt2)
  {
    // Downcast variables
    RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(data);

    AbstractLifting2D* filter1 = static_cast<AbstractLifting2D*>(_filt1);
    AbstractLifting2D* filter2 = static_cast<AbstractLifting2D*>(_filt2);

    // Memory for intermediate data
    RMMatrix_Float dummy;
    std::vector<RMMatrix_Float> dTemp(2, dummy);
    // Do the decimation at all levels
    for (int l = 0; l < dtcwtinfo.levels; ++l)
    {
      // Instantiate a WtStruct2D bands object
      WTStruct2D l_bands;

      //Also check if we doing stage 1
      if (l == 0)
      {
        // 1.) Across the rows (decimate the columns)
        filter1->Forward( mat, 1, false, &dTemp[0], &dTemp[1]);
        // 2.) Across the columns (decimate the rows)
        filter1->Forward( &dTemp[0], 0, false, &l_bands.LL, &l_bands.LH);
        filter1->Forward( &dTemp[1], 0, false, &l_bands.HL, &l_bands.HH);
      }
      else
      {
        // 1.) Across the rows (decimate the columns)
        filter2->Forward( mat, 1, false, &dTemp[0], &dTemp[1]);
        // 2.) Across the columns (decimate the rows)
        filter2->Forward( &dTemp[0], 0, false, &l_bands.LL, &l_bands.LH);
        filter2->Forward( &dTemp[1], 0, false, &l_bands.HL, &l_bands.HH);
      }

      // Populate the height and width as this level
      l_bands.height = l_bands.HL.rows();
      l_bands.width  = l_bands.HL.cols();
      // 3.) Place the bands in the map
      bands.get()->pyramid_dtcwt[treeIdx][l]=l_bands;
      // 4.) Clear redundant data
      dTemp[0].resize(0,0);
      dTemp[1].resize(0,0);
      // 5.) Set the image for the next level to the LL image
      mat = &(bands.get()->pyramid_dtcwt[treeIdx][l].LL);
      // 6.) Note: we do not have to preserve the previous LL subbands
      if (l > 0) bands.get()->pyramid_dtcwt[treeIdx][l-1].LL.resize(0,0);
    }
  }//end of function

  /*! \brief InverseWaveletTransform
   *  This function implements an inverse wavelet transform
   *  associated with a single tree of a dual tree complex wavelet transform.
   */
  void DTCWT2D::InverseWaveletTransform(int treeIdx, void* _filt1, void* _filt2,\
                                        void* _output)
  {
    // downcast the lifting filters
    AbstractLifting2D* filter1 = static_cast<AbstractLifting2D*>(_filt1);
    AbstractLifting2D* filter2 = static_cast<AbstractLifting2D*>(_filt2);

    // Memory for intermediate data
    RMMatrix_Float dummy;
    std::vector<RMMatrix_Float> dTemp(2, dummy);
    // Do the decimation at all levels
    for (int l = dtcwtinfo.levels-1; l >=0 ; --l)
    {
      WTStruct2D* l_bands = &(bands.get()->pyramid_dtcwt[treeIdx][l]);
      PermutationMatrix* p_Matrix = &(pMap.get()->pyramid_pMatrix[l]);

      // Check if we are not in the first level
      if (l !=0)
      {
        // 1.) Across the columns (upsample the rows)
        filter2->Inverse(&l_bands->HL, &l_bands->HH, 0, false, p_Matrix, &dTemp[1]);
        filter2->Inverse(&l_bands->LL, &l_bands->LH, 0, false, p_Matrix, &dTemp[0]);
        // 2.) Across the rows (upsample the columns) and place the
        //     reconstructed band in the LL band of the next level
        filter2->Inverse( &dTemp[0], &dTemp[1], 1, false, p_Matrix, \
                          &(bands.get()->pyramid_dtcwt[treeIdx][l-1].LL));
      }
      else
      {
        RMMatrix_Float* outputR = static_cast<RMMatrix_Float*>(_output);
        // 1.) Across the columns (upsample the rows)
        filter1->Inverse(&l_bands->HL, &l_bands->HH, 0, false, p_Matrix, &dTemp[1]);
        filter1->Inverse(&l_bands->LL, &l_bands->LH, 0, false, p_Matrix, &dTemp[0]);
        // 2.) Across the rows (upsample the columns) and place the
        //     reconstructed band in the LL band of the next level
        filter1->Inverse( &dTemp[0], &dTemp[1], 1, false, p_Matrix, outputR);
      }

      // Free the intermdiate output
      dTemp[0].resize(0,0);
      dTemp[1].resize(0,0);
    }
  }//end of function

  /*! \brief Compute
   *  This function is used to compute either a forward or inverse
   *  discrete wavelet transform. Depending on the type of transform
   *  being implemented, the data will either be a set of bands (inverse)
   *  or a 2D image (forward).
   *  It is assumed that all required data would have been computed/enabled
   *  prior to calling this function.
   */
  int DTCWT2D::Compute(void* data, void* _output)
  {
    // Initialize the data structures
    std::shared_ptr<AbstractLifting2D> filter2;
    std::shared_ptr<AbstractLifting2D> filter3;

    switch(dtcwtinfo.filter)
    {
      case DTCWFilters::NK13_19_QS_18:
      {
        filter2 = std::make_shared<LiftingQS_18A_2D>();
        filter3 = std::make_shared<LiftingQS_18B_2D>();
        break;
      }
      default:
      {
        filter2 = std::make_shared<LiftingQS_18A_2D>();
        filter3 = std::make_shared<LiftingQS_18B_2D>();
        break;
      }
    }

    // Now, check if we are implementing a forward or backward transform
    if (forward==1)
    {
      // We are inmplenting a forward wavelet transform
      // We need a valid data to be input
      RMMatrix_Float* mat = static_cast<RMMatrix_Float*>(data);
      if (!mat->data()) return -1;

      // We also check if the dimensions are valid w.r.t # of levels
      // of decomposition
      int height = mat->rows();
      int width  = mat->cols();

      // At this time, we check for the sanity of levels.
      int actLevels = this->CheckLevels(height,width);
      if (dtcwtinfo.levels > actLevels) dtcwtinfo.levels = actLevels;

      // Check if the # of levels of decomposition is valid
      if ( ((height>>dtcwtinfo.levels)<<dtcwtinfo.levels) != height) return -2;
      if ( ((width>>dtcwtinfo.levels)<<dtcwtinfo.levels) != width) return -3;
      dtcwtinfo.height = height;
      dtcwtinfo.width  = width;

      // In the dual-tree complex wavelet transform, two independent transforms
      // are applied to the image (hence the name dual trees). In the first stage
      // a common wavelet-filter decomposition is applied. Subsequently, the
      // same LL band is applied to both trees and a decomposition is computed.
      int NUM_THREADS = 2;
      boost::scoped_array< std::shared_ptr<boost::thread> > tArray(new std::shared_ptr<boost::thread>[NUM_THREADS]);
      // 4.) Create two boost threads on the heap and assign it to thread_group
      //http://stackoverflow.com/questions/1246813/simple-boost-thread-group-question
      std::shared_ptr<boost::thread> pT1(new boost::thread( &DTCWT2D::ForwardWaveletTransform,this,\
                                                            data, 0, \
                                                            filter2.get(), \
                                                            filter2.get()));
      std::shared_ptr<boost::thread> pT2(new boost::thread( &DTCWT2D::ForwardWaveletTransform,this,\
                                                            data, 1, \
                                                            filter3.get(), \
                                                            filter3.get()));
      tArray[0] = pT1;
      tArray[1] = pT2;

      // d.) And execute all threads
      for (int jThread = 0; jThread < NUM_THREADS; ++jThread)
      {
        tArray[jThread].get()->join();
      }
    }
    else
    {
      // do some sanity checks
      if (!bands.get()) return -2;
      if (bands.get()->pyramid_dtcwt.size() <= 0) return -3;
      if (bands.get()->pyramid_dtcwt.size() > 2) return -4;

      if (bands.get()->pyramid_dtcwt[0].size() <=0) return -5;
      if (bands.get()->pyramid_dtcwt[1].size() <=0) return -6;
      if (bands.get()->pyramid_dtcwt[0].size() != bands.get()->pyramid_dtcwt[1].size()) return -7;

      // Also, the permutation maps should have been initialized here
      if(!pMap.get()) return -8;
      if (pMap.get()->pyramid_pMatrix.size() <= 0 || \
          pMap.get()->pyramid_pMatrix.size() != bands.get()->pyramid_dtcwt[1].size()) return -9;

      // Inverse wavelet transforms are effected on both trees in parallel with the last
      // stage having a wavelet transform effected by a different wavelet filter
      RMMatrix_Float tree1_recon, tree2_recon;
      int NUM_THREADS = 2;
      boost::scoped_array< std::shared_ptr<boost::thread> > tArray(new std::shared_ptr<boost::thread>[NUM_THREADS]);
      // 4.) Create two boost threads on the heap and assign it to thread_group
      //http://stackoverflow.com/questions/1246813/simple-boost-thread-group-question
      std::shared_ptr<boost::thread> pT1(new boost::thread( &DTCWT2D::InverseWaveletTransform,this,\
                                                            0, \
                                                            filter2.get(), \
                                                            filter2.get(), &tree1_recon));
      std::shared_ptr<boost::thread> pT2(new boost::thread( &DTCWT2D::InverseWaveletTransform,this,\
                                                            1, \
                                                            filter3.get(), \
                                                            filter3.get(), &tree2_recon));
      tArray[0] = pT1;
      tArray[1] = pT2;

      // d.) And execute all threads
      for (int jThread = 0; jThread < NUM_THREADS; ++jThread)
      {
        tArray[jThread].get()->join();
      }
      // The final result is an average of results from both trees
      RMMatrix_Float* output = static_cast<RMMatrix_Float*>(_output);
      (*output) = (tree1_recon + tree2_recon)*0.5;
    }
    return 1;
  }//end of function

  /*! \brief RecomposeBands
   *  This function is used to recompose bands as they would
   *  appear in a 2D image after wavelet transform. This is primarily
   *  used for display or for running encoders. The output is stored
   *  in output as a floating point matrix data.
   */
  int DTCWT2D::RecomposeBands(void* _output, int treeIdx)
  {
    // sanity checks
    if (!bands.get()) return -1;
    if (bands.get()->pyramid_dtcwt[treeIdx].size() <=0) return -2;

    // typecast the output
    RMMatrix_Float* output = static_cast<RMMatrix_Float*>(_output);

    // Set the dimensions of the image
    int height_half = bands.get()->pyramid_dtcwt[treeIdx][0].HL.rows();
    int width_half  = bands.get()->pyramid_dtcwt[treeIdx][0].HL.cols();

    // Check for discrepancies
    if (height_half <=0 || width_half <=0) return -3;

    int height = height_half<<1;
    int width  = width_half<<1;

    // Set the dimensions of the image
    (*output) = RMMatrix_Float::Zero(height, width);

    // Now, traverse through all levels and place the bands in the
    // respective location. Essentially, the bands are arranged as
    // LL  HL
    // LH  HH
    // at a given level.
    for (int i = 0; i < dtcwtinfo.levels; ++i)
    {
      // Pad the HL band
      output->block(0,width_half,height_half,width_half) = bands.get()->pyramid_dtcwt[treeIdx][i].HL;
      // Pad the HH band
      output->block(height_half,width_half,height_half,width_half) = bands.get()->pyramid_dtcwt[treeIdx][i].HH;
      // Pad the LH band
      output->block(height_half,0,height_half,width_half) = bands.get()->pyramid_dtcwt[treeIdx][i].LH;
      if (i == dtcwtinfo.levels-1)
      {
        // if we are at the last level, pad the LL band
        output->block(0,0,height_half,width_half) = bands.get()->pyramid_dtcwt[treeIdx][i].LL;
      }

      // decrement the dimension sizes
      width_half = width_half >> 1;
      height_half= height_half>> 1;
    }

    return 1;
  }//end of function

}//end of namespace
