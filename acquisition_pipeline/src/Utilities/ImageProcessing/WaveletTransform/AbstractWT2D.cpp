#include "AbstractClasses/AbstractWT2D.h"
#include "Utilities/ImageProcessing/WaveletTransform/WTStruct2D.h"

namespace spin
{
  /*! \brief CheckLevels */
  int AbstractWT2D::CheckLevels(int height, int width)
  {
    //https://stackoverflow.com/questions/10956543/gcd-function-in-c-sans-cmath-library
    //Should change in C++117
    int gcd = std::__gcd(height,width);//hidden function
    int levels = (int)(std::floor(std::log2(gcd)));
    return levels;
  }//end of function

  /*! \brief Initialize
   *  This function is called when performing an inverse wavelet transform
   *  on the data. It stores the permutation matrices that will be associated
   *  with interleaving of data. In addition, we should also tailor the
   *  permutation matrices to account for odd length data.
   */
  void AbstractWT2D::Initialize(int levels, int height, int width, void* _pMap)
  {
    pyramidPermutationMatrix* pMap = static_cast<pyramidPermutationMatrix*>(_pMap);
    for (int i = levels-1; i >=0; --i)
    {
      // Get the dimensions of the matrix at this level
      int height_d = height>>i;
      int width_d  = width>>i;
      PermutationMatrix p;
      // Allocate memory for all matrices
      p.stage0_even.resize(height_d,height>>(i+1));
      p.stage0_odd.resize(height_d ,height>>(i+1));

      p.stage1_even.resize(width>>(i+1), width_d);
      p.stage1_odd.resize(width>>(i+1), width_d);

      // And populate them
      for (int h = 0; h < height>>(i+1); ++h)
      {
        p.stage0_even.insert(2*h,h) = 1;
        p.stage0_odd.insert(2*h+1,h) = 1;
      }

      for (int w = 0; w < width>>(i+1); ++w)
      {
        p.stage1_even.insert(w,2*w) = 1;
        p.stage1_odd.insert(w,2*w+1) = 1;
      }

      // Having populated the matrices, we place it in the
      // respective data structure
      pMap->pyramid_pMatrix[i] = p;
    }
  }//end of function
}//end of namespace
