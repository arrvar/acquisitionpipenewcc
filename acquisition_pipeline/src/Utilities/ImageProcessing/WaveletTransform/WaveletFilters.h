#ifndef WAVELETFILTERS_H_
#define WAVELETFILTERS_H_

enum WFilters
{
  NK13_19,
  NKQS_18A,
  NKQS_18B
};//end of enum

enum DTCWFilters
{
  NK13_19_QS_18
};//end of enum
#endif
