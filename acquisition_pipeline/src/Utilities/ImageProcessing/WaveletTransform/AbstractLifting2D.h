#ifndef ABSTRACTLIFTING2D_H_
#define ABSTRACTLIFTING2D_H_

namespace spin
{
  class AbstractLifting2D
  {
    public:
      // Default destructor
      ~AbstractLifting2D(){}
      // 2D Forward Lifting step
      virtual int Forward(void* _mat, int direction, bool u, void* _outMat1, void* _outMat2)=0;
      // 2D Inverse Lifting step
      virtual int Inverse(void* _mat1, void* _mat2, int direction, bool u, void* p, void* _outMat)=0;
    protected:
      // 2D Lazy transform (foward)
      int Lazy_Forward(void* _mat, int direction, void* _outMat1, void* _outMat2);
      // 2D Lazy transform (inverse)
      int Lazy_Inverse(void* _inMat1, void* _inMat2, int direction, void* p, void* _outMat1);

  };//end of class
}//end of namespace
#endif
