#ifndef IMAGEUTILS_H_
#define IMAGEUTILS_H_

namespace spin
{
  template <class channel>
  int ExtractChannelfromRGBImage(void* InputImage, void* _OutputImage);
  
  template <class T>
  int PasteImageIntoBlankImage( T* InputImage, int newWidth, int newHeight, T* OutputImage);
  
  template <class T>
  int ShrinkImage( T* InputImage, int shrinkWidth, int shrinkHeight, T* OutputImage);
    
  template <class channel, class T>
  int ComputeFFTOfRGBImage( void* InputImage, void* _fftObj, void* _outFFT, int SubSampFact,
                            bool expanded, int width1, int height1, bool incremental);

  template <class T>
  int ComputeFFTOfGrayscaleImage( void* InputImage, void* _fftObj, void* _outFFT, int SubSampFact,
                                  bool expanded, int width1, int height1, bool incremental);

}
#endif
