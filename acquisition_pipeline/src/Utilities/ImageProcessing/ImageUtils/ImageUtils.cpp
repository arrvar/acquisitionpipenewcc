#include "Utilities/ImageProcessing/ImageUtils/ImageUtils.h"
#include "Utilities/ImageProcessing/FFTUtils/FFT2D.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "itkImageAdaptor.h"
#include "itkShrinkImageFilter.h"
#include "itkPasteImageFilter.h"
#include "itkCastImageFilter.h"

/*! \brief This class contains various functions that can be used in conjunction with
 *  other image processing / computer vision pipelines
 */
namespace spin
{
  namespace
  {
    typedef spin::BlueChannelPixelAccessor<unsigned char> blue;
    typedef spin::GreenChannelPixelAccessor<unsigned char> green;
    typedef spin::RedChannelPixelAccessor<unsigned char> red;
  }

  /*! \brief ExtractChannelfromRGBImage
   *  This function is used to extract an RGB channel and return the output as
   *  an unsigned char image
   */
  template <class channel>
  int ExtractChannelfromRGBImage(void* InputImage, void* _OutputImage)
  {
    typedef itk::ImageAdaptor<spin::RGBImageType, channel> ImageAdaptorType;
    typedef itk::Image<typename channel::ExternalType, 2> OutImage;
    typename OutImage::Pointer* OutputImage = \
    static_cast<typename OutImage::Pointer*>(_OutputImage);

    // Typecast the image to an RGB Image pointer
    spin::RGBImageType::Pointer* rgb = static_cast<spin::RGBImageType::Pointer*>(InputImage);
    // Instantiate an operational image
    typename ImageAdaptorType::Pointer tChannel = ImageAdaptorType::New();
    typedef itk::CastImageFilter<ImageAdaptorType,OutImage> CastFilter;
    typename CastFilter::Pointer cast = CastFilter::New();
    tChannel->SetImage(*rgb);
    cast->SetInput(tChannel);
    try
    {
      cast->Update();
      (*OutputImage) = OutImage::New();
      (*OutputImage)->Graft(cast->GetOutput());
    }
    catch(...)
    {
      return -1;
    }
    return 1;
  }//end of function

  /*! \brief PasteImageIntoBlankImage
   *  This function creates a blank image of given dimensions. The input image
   *  is pasted on this image at the 0,0 location and returned. This function
   *  should primarily be used for zero-padding the data.
   */
  template <class T>
  int PasteImageIntoBlankImage( T* InputImage, int newWidth, int newHeight, T* OutputImage)
  {
    typedef itk::PasteImageFilter <T, T > PasteImageFilterType;
    typedef typename T::RegionType Region;
    typedef typename T::IndexType Index;
    typedef typename T::SizeType  Size;
    Index index;
    Size size;
    size[0] = newWidth; size[1] = newHeight;

    index[0]=0; index[1] = 0;

    // Get the size of the input image. It is imperative that the
    // size of the input image is less that newWidth and newHeight
    typename T::SizeType size2 = InputImage->GetLargestPossibleRegion().GetSize();
    if (size2[0] <=0 || size2[0] > newWidth) return -1;
    if (size2[1] <=0 || size2[1] > newHeight) return -2;

    // Allocate memory for the output image
    Region region;
    region.SetSize(size);
    region.SetIndex(index);
    OutputImage = T::New();
    OutputImage->SetRegions(region);
    OutputImage->Allocate();
    OutputImage->FillBuffer(0);

    // And run the paste pipeline
    typename PasteImageFilterType::Pointer pasteFilter = PasteImageFilterType::New ();
    pasteFilter->SetSourceImage(InputImage);
    pasteFilter->SetDestinationImage(OutputImage);
    pasteFilter->SetSourceRegion(InputImage->GetLargestPossibleRegion());
    pasteFilter->SetDestinationIndex(index);
    try
    {
      pasteFilter->Update();
      // Finally, graft the output
      OutputImage->Graft(pasteFilter->GetOutput());
    }
    catch(...)
    {
      return -1;
    }
    return 1;
  }//end of function

  /*! \brief ShrinkImage
   *  This function shrinks an image by an integer factor along either direction
   */
  template <class T>
  int ShrinkImage( T* InputImage, int shrinkWidth, int shrinkHeight, T* OutputImage)
  {
    // sanity check
    if (shrinkWidth <=0 || shrinkWidth > 8) return -1;
    if (shrinkHeight <=0 || shrinkHeight > 8) return -2;
    typedef itk::ShrinkImageFilter <T, T> ShrinkImageFilterType;
    typename ShrinkImageFilterType::Pointer shrinkFilter = ShrinkImageFilterType::New();
    shrinkFilter->SetInput(InputImage);
    shrinkFilter->ReleaseDataFlagOn();
    shrinkFilter->SetShrinkFactor(0, shrinkWidth); // shrink the first dimension by a factor of 2 (i.e. 100 gets changed to 50)
    shrinkFilter->SetShrinkFactor(1, shrinkHeight); // shrink the second dimension by a factor of 3 (i.e. 100 gets changed to 33)
    try
    {
      shrinkFilter->Update();
      // Finally, graft the output
      OutputImage->Graft(shrinkFilter->GetOutput());
    }
    catch(...)
    {
      return -1;
    }
    return 1;
  }//end of function

  /*! \brief ComputeFFTOfRGBImage
   *  This function is a driver to take as input a RGB Image and compute an FFT
   *  of a grayscale and (if requested) resampled version of the image
   *  @InputImage 	: The input buffer that needs to be normalized
   *  @fftObj     	: A pointer to a FFTUtils object
   *  @OutFFT     	: The FFT of the normalized image
   *  @SubSampFact	: Subsampling factor. By default, this is set to 1.
   *  @expanded   	: If we want to compute the fft of a zero-padded image
   *  @width1      	: The width of the dyadic length image
   *  @height1     	: The height of the dyadic length image
   *  @incremental      : If we want to run each step separately (only in debug mode)
   *  This function can be operated in two modes. In the debug mode, we run each step
   *  separately, while in the release mode we run each step in a single process
   */
  template <class channel, class T>
  int ComputeFFTOfRGBImage( void* InputImage, void* _fftObj, void* _outFFT, int SubSampFact,
                            bool expanded, int width1, int height1, bool incremental)
  {
    // 0.) typecast the FFT object
    typedef FFT2D<T> fft2d;
    fft2d* fftObj = static_cast<fft2d*>(_fftObj);
    if (!fftObj) return -1;

    if (!incremental)
    {
      typedef itk::ImageAdaptor<spin::RGBImageType, channel> ImageAdaptorType;
      typedef itk::Image<typename channel::ExternalType, 2> OutImage;
      spin::RGBImageType::Pointer* rgb = static_cast<spin::RGBImageType::Pointer*>(InputImage);

      //1.) Extract the channel
      typename ImageAdaptorType::Pointer tChannel = ImageAdaptorType::New();
      tChannel->SetImage(*rgb);
      try
      {
        tChannel->Update();
      }
      catch(...)
      {
        return -2;
      }

      // We need to cast ImageAdpater as there is no viable API
      // for using the output directly
      typedef itk::CastImageFilter<ImageAdaptorType,OutImage> CastFilter;
      typename CastFilter::Pointer cast = CastFilter::New();
      cast->SetInput(tChannel);
      cast->Update();


      // 2.) Point the data to a pointer
      typename OutImage::Pointer inpData = cast->GetOutput();

      // Get the dimensions of the input image
      typename spin::RGBImageType::SizeType size = (*rgb)->GetLargestPossibleRegion().GetSize();

      // 3.) Now, check if an expanded image is required
      if (expanded)
      {
        if (width1 <=0 || height1 <=0) return -3;
        if (width1<size[0] && height1<size[1]) return -4;
        if (width1<size[0] && height1>=size[1]) return -5;
        if (width1>=size[0] && height1<size[1]) return -6;

        // Allocate memory for the output image
        typename OutImage::RegionType region;
        typename OutImage::SizeType   size;
        typename OutImage::IndexType  index;
        size[0] = width1; size[1] = height1;
        index[0]=0; index[1] = 0;
        region.SetSize(size);
        region.SetIndex(index);
        typename OutImage::Pointer expandedImg = OutImage::New();
        expandedImg->SetRegions(region);
        expandedImg->Allocate();
        expandedImg->FillBuffer(0);

        // And run the paste pipeline
        typedef itk::PasteImageFilter <OutImage, OutImage> PasteImageFilterType;
        typename PasteImageFilterType::Pointer pasteFilter = PasteImageFilterType::New ();
        pasteFilter->SetSourceImage(inpData);
        pasteFilter->SetDestinationImage(expandedImg);
        pasteFilter->SetSourceRegion(inpData->GetLargestPossibleRegion());
        pasteFilter->SetDestinationIndex(index);
        try
        {
          pasteFilter->Update();
          // Finally, point to the new output
          inpData = pasteFilter->GetOutput();
        }
        catch(...)
        {
          return -7;
        }
      }

      // 4.) Now, check if we need to shrink the image
      if (SubSampFact > 1)
      {
        typedef itk::ShrinkImageFilter <OutImage, OutImage> ShrinkImageFilterType;
        typename ShrinkImageFilterType::Pointer shrinkFilter = ShrinkImageFilterType::New();
        shrinkFilter->SetInput(inpData);
        shrinkFilter->ReleaseDataFlagOn();
        shrinkFilter->SetShrinkFactor(0, SubSampFact);
        shrinkFilter->SetShrinkFactor(1, SubSampFact);
        try
        {
          shrinkFilter->Update();
          // and point the data
          inpData = shrinkFilter->GetOutput();
        }
        catch(...)
        {
          return -8;
        }
      }

      // 5.) Finally Compute the FFT
      // First, we get the size of the image
      size = inpData->GetLargestPossibleRegion().GetSize();
      typedef std::complex<T>  Complex_t;
      typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> ComplexMatrix;
      typedef Eigen::Matrix<typename OutImage::PixelType, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;
      // Typecast the output FFT
      ComplexMatrix* outFFT = static_cast<ComplexMatrix*>(_outFFT);
      if (outFFT == NULL) return -9;

      // Allocate memory for computing FFT
      ComplexMatrix image = ComplexMatrix::Zero(size[1],size[0]);
      Matrix tempImg = Matrix::Zero(size[1],size[0]);
      memcpy(tempImg.data(), inpData->GetBufferPointer(), tempImg.rows()*tempImg.cols()*sizeof(typename OutImage::PixelType));
      // And copy the data to the real componentr of image
      image.real() = tempImg.template cast<T>();
      // Finally, implement the FFT pipeline
      int ret = fftObj->ComputePeriodicImageFFT(&image, outFFT);
      // Return a failure if not 1
      if (ret != 1) return -10;
      tempImg.resize(0,0);
      image.resize(0,0);
    }//end of non-incremental mode
    else
    {
    }
    return 1;
  }//end of function

  /*! \brief ComputeFFTOfGrayscaleImage
   *  This function is a driver to take as input a grayscale Image and compute an FFT
   *  and (if requested) resampled version of the image
   *  @InputImage 	: The input buffer that needs to be normalized
   *  @fftObj     	: A pointer to a FFTUtils object
   *  @OutFFT     	: The FFT of the normalized image
   *  @SubSampFact	: Subsampling factor. By default, this is set to 1.
   *  @expanded   	: If we want to compute the fft of a zero-padded image
   *  @width1      	: The width of the dyadic length image
   *  @height1     	: The height of the dyadic length image
   *  @incremental      : If we want to run each step separately (only in debug mode)
   *  This function can be operated in two modes. In the debug mode, we run each step
   *  separately, while in the release mode we run each step in a single process
   */
  template <class T>
  int ComputeFFTOfGrayscaleImage( void* InputImage, void* _fftObj, void* _outFFT, int SubSampFact,
                                  bool expanded, int width1, int height1, bool incremental)
  {
    // 0.) typecast the FFT object
    typedef FFT2D<T> fft2d;
    fft2d* fftObj = static_cast<fft2d*>(_fftObj);
    if (!fftObj) return -1;

    if (!incremental)
    {
      typedef itk::Image<T, 2> OutImage;
      spin::UCharImageType2D::Pointer* gs = static_cast<spin::UCharImageType2D::Pointer*>(InputImage);

      // We need to cast ImageAdpater as there is no viable API
      // for using the output directly
      typedef itk::CastImageFilter<spin::UCharImageType2D,OutImage> CastFilter;
      typename CastFilter::Pointer cast = CastFilter::New();
      cast->SetInput(*gs);
      cast->Update();


      // 2.) Point the data to a pointer
      typename OutImage::Pointer inpData = cast->GetOutput();

      // Get the dimensions of the input image
      typename spin::UCharImageType2D::SizeType size = (*gs)->GetLargestPossibleRegion().GetSize();

      // 3.) Now, check if an expanded image is required
      if (expanded)
      {
        if (width1 <=0 || height1 <=0) return -3;
        if (width1<size[0] && height1<size[1]) return -4;
        if (width1<size[0] && height1>=size[1]) return -5;
        if (width1>=size[0] && height1<size[1]) return -6;

        // Allocate memory for the output image
        typename OutImage::RegionType region;
        typename OutImage::SizeType   size;
        typename OutImage::IndexType  index;
        size[0] = width1; size[1] = height1;
        index[0]=0; index[1] = 0;
        region.SetSize(size);
        region.SetIndex(index);
        typename OutImage::Pointer expandedImg = OutImage::New();
        expandedImg->SetRegions(region);
        expandedImg->Allocate();
        expandedImg->FillBuffer(0);

        // And run the paste pipeline
        typedef itk::PasteImageFilter <OutImage, OutImage> PasteImageFilterType;
        typename PasteImageFilterType::Pointer pasteFilter = PasteImageFilterType::New ();
        pasteFilter->SetSourceImage(inpData);
        pasteFilter->SetDestinationImage(expandedImg);
        pasteFilter->SetSourceRegion(inpData->GetLargestPossibleRegion());
        pasteFilter->SetDestinationIndex(index);
        try
        {
          pasteFilter->Update();
          // Finally, point to the new output
          inpData = pasteFilter->GetOutput();
        }
        catch(...)
        {
          return -7;
        }
      }

      // 4.) Now, check if we need to shrink the image
      if (SubSampFact > 1)
      {
        typedef itk::ShrinkImageFilter <OutImage, OutImage> ShrinkImageFilterType;
        typename ShrinkImageFilterType::Pointer shrinkFilter = ShrinkImageFilterType::New();
        shrinkFilter->SetInput(inpData);
        shrinkFilter->ReleaseDataFlagOn();
        shrinkFilter->SetShrinkFactor(0, SubSampFact);
        shrinkFilter->SetShrinkFactor(1, SubSampFact);
        try
        {
          shrinkFilter->Update();
          // and point the data
          inpData = shrinkFilter->GetOutput();
        }
        catch(...)
        {
          return -8;
        }
      }

      // 5.) Finally Compute the FFT
      // First, we get the size of the image
      size = inpData->GetLargestPossibleRegion().GetSize();
      typedef std::complex<T>  Complex_t;
      typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> ComplexMatrix;
      typedef Eigen::Matrix<typename OutImage::PixelType, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;
      // Typecast the output FFT
      ComplexMatrix* outFFT = static_cast<ComplexMatrix*>(_outFFT);
      if (outFFT == NULL) return -9;

      // Allocate memory for computing FFT
      ComplexMatrix image = ComplexMatrix::Zero(size[1],size[0]);
      Matrix tempImg = Matrix::Zero(size[1],size[0]);
      memcpy(tempImg.data(), inpData->GetBufferPointer(), tempImg.rows()*tempImg.cols()*sizeof(typename OutImage::PixelType));
      // And copy the data to the real componentr of image
      image.real() = tempImg.template cast<T>();
      // Finally, implement the FFT pipeline
      int ret = fftObj->ComputePeriodicImageFFT(&image, outFFT);
      // Return a failure if not 1
      if (ret != 1) return -10;
      tempImg.resize(0,0);
      image.resize(0,0);
    }//end of non-incremental mode
    else
    {
    }
    return 1;
  }//end of function

  // Define explicit templates for all functions
  template int
  ComputeFFTOfRGBImage<blue, float >(void*, void*, void*, int, bool, int, int, bool);
  // Define explicit templates for all functions
  template int
  ComputeFFTOfRGBImage<green, float >(void*, void*, void*, int, bool, int, int, bool);
  // Define explicit templates for all functions
  template int
  ComputeFFTOfRGBImage<red, float >(void*, void*, void*, int, bool, int, int, bool);

  template int
  ComputeFFTOfRGBImage<blue, double >(void*, void*, void*, int, bool, int, int, bool);
  // Define explicit templates for all functions
  template int
  ComputeFFTOfRGBImage<green, double >(void*, void*, void*, int, bool, int, int, bool);
  // Define explicit templates for all functions
  template int
  ComputeFFTOfRGBImage<red, double >(void*, void*, void*, int, bool, int, int, bool);

  // Define explicit templates for all functions
  template int
  ComputeFFTOfGrayscaleImage<float >(void*, void*, void*, int, bool, int, int, bool);
  template int
  ComputeFFTOfGrayscaleImage<double >(void*, void*, void*, int, bool, int, int, bool);

  template int
  ExtractChannelfromRGBImage<blue>(void* , void*);
  template int
  ExtractChannelfromRGBImage<green>(void* , void*);
  template int
  ExtractChannelfromRGBImage<red>(void* , void*);

  template int PasteImageIntoBlankImage<UCharImageType2D>(UCharImageType2D*, int, int, UCharImageType2D*);
  template int PasteImageIntoBlankImage<FloatImageType2D>(FloatImageType2D*, int, int, FloatImageType2D*);

  template int ShrinkImage<UCharImageType2D>( UCharImageType2D*, int, int, UCharImageType2D*);
  template int ShrinkImage<FloatImageType2D>( FloatImageType2D*, int, int, FloatImageType2D*);

}//end of namespace
