#include "Utilities/ImageProcessing/FFTUtils/FFTNormalization1D.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <cmath>

namespace spin
{
  /*! \brief Default constructor */
  template <class T>
  FFTNormalization1D<T>::FFTNormalization1D(int length)
  {
    // Instantiate plans for FFT's
    fftPtrFwd = std::make_shared< fft >(length,false);
    fftPtrInv = std::make_shared< fft >(length,true);
  }//end of function

  /*! \brief Transform
   *  This function implements a Forward/Inverse transform across a
   *  1D complex Eigen matrix subject to a flag being set/unset
   */
  template <class T>
  int FFTNormalization1D<T>::Transform(void* _src, void* _dst, bool forward)
  {
    // get the pixel type
    typedef std::complex<T>  Complex_t;
    typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Complex_Matrix;

    Complex_Matrix* src = static_cast<Complex_Matrix*>(_src);
    Complex_Matrix* dst = static_cast<Complex_Matrix*>(_dst);

    // some sanity checks
    if ( !src->data() || !dst->data() ) return -1;

    if (forward)
    {
      // Run the forward transform
      fftPtrFwd.get()->transform(src->data(), dst->data());
    }
    else
    {
      fftPtrInv.get()->transform(src->data(), dst->data());
    }

    return 1;
  }//end of function

 /*! \brief ComputeNormalization
  *  This function takes a 1D signal (represented as input) and does a FFT
  *  normalization on it. The signal is transformed using a 1D-FFT. A
  *  magnitude of the fft transformed data is computed. Every element of the
  *  data is divided by the first element of the signal. This signal is
  *  now inverse transformed and the real component of the signal is returned
  *  in output
  */
  template <class T>
  int FFTNormalization1D<T>::CommputeNormalization(void* _input, void* _output, int length, int filtLen)
  {
    // get the pixel type
    typedef std::complex<T>  Complex_t;
    typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Complex_Matrix;
    typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RMMatrix;

    // sanity checks
    T* inputData = static_cast<T*>(_input);
    if (!inputData) return -1;
    T* outputData = static_cast<T*>(_output);
    if (!outputData) return -2;

    // Map the data to an Eigen matrices
    RMMatrix src = Eigen::Map<RMMatrix>(inputData, 1, length);
    RMMatrix dst = Eigen::Map<RMMatrix>(outputData, 1, length);

    // 1.) Allocate memory for complex numbers
    Complex_Matrix fftin   = Complex_Matrix::Zero(1, length);
    Complex_Matrix fftout  = Complex_Matrix::Zero(1, length);
    // 2.) Assign the real component to fftin
    fftin.real() = src;
    // 3.) Perform FFT
    Transform(&fftin, &fftout, true);
    // 4.) Magnitude of FFT
    dst = fftout.array().abs();
    // 5.) Normalization of magnitude by DC component
    dst = dst / dst(0, 0);
    // Check if we need to threshold the FFT signal
    // We keep the first filtLen coefficients
    if (filtLen > 0)
    {
      RMMatrix dump = dst;
      dst = RMMatrix::Zero(dst.rows(),dst.cols());
      dst.block(0,0,1,filtLen) = dump.block(0,0,1,filtLen);
    }
    // 6.) Reset fftin to zero
    fftin = Complex_Matrix::Zero(1, length);
    // 7.) Copy dst to fftin
    fftin.real() = dst;
    // 8.) Perform an inverse FFT
    Transform(&fftin, &fftout, false);
    // 9.) Extract the real component of the matrix
    dst = fftout.real();
    // 10.) Scale the data such that it lies between 0 and 1 (and a small offset)
    /*T minVal = dst.minCoeff();
    T maxVal = dst.maxCoeff();
    dst = ((dst.array()-minVal)/(maxVal-minVal)).array()+T{0.1};*/
    // 10.) Finally copy the data from dst to outputData
    memcpy(outputData, dst.data(), length * sizeof(T));
    return 1;
  }//end of function


  // Explicit templates
  template class FFTNormalization1D<float>;
  template class FFTNormalization1D<double>;
}//end of namespace
