#ifndef FFT2D_H_
#define FFT2D_H_
#include <memory>
#include "Utilities/ImageProcessing/FFTUtils/kissfft.hh"

namespace spin
{
  /*! \brief FFTNormalization1D
   *  This class is used to normalize 1D signals using FFT
   */
  template <class T>
  class FFTNormalization1D
  {
    public:
      typedef kissfft<T> fft;
      // Default constructor
      FFTNormalization1D(int length);
      // Default destructor
      ~FFTNormalization1D(){}
      // The main driver function
      int CommputeNormalization(void* input, void* output, int l, int filtLen=10);

    private:
      /*! \brief Transform */
      int Transform(void* src, void* dest, bool f=true);
      std::shared_ptr< fft > fftPtrFwd;
      std::shared_ptr< fft > fftPtrInv;
  };//end of class
}//end of namespace
#endif
