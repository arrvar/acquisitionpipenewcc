#include "Utilities/ImageProcessing/FFTUtils/FFTTranslation.h"
#include "Utilities/ImageProcessing/FFTUtils/FFT2D.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#ifdef DEBUG_FFT
  #include "AbstractClasses/AbstractImageTypes.h"
#endif
#include <utility>
#include <cmath>
#include <boost/timer/timer.hpp>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
namespace spin
{
  /*! \brief CorrectOffset
   *  This function is used to convert displacements into overlaps
   *  by taking into account overlaps between both images. It is
   *  assumed that this function will be called when the overlap
   *  between two AOI's is greater than 50% of either dimension.
   *  Please note that we do consider the case when there are overlaps
   *  in both dimensions.
   *  @gridIdx1     : The current AOI location
   *  @gridIdx2     : A previous AOI location
   *  @tShift       : Raw Displacements from phase correlation
   *  @ModDim       : Size of zero-padded data
   *  @subSampFact  : Amount of subsampling effected
   */
  template <class T>
  int FFTTranslation<T>::CorrectOffset( void* _gridIdx1,
                                        void* _gridIdx2,
                                        void* _tShift,
                                        void* _ModDim,
                                        int   subSampFact)
  {
    typedef std::pair<double,double> pDouble;
    typedef std::pair<int,int> pInt;
    pDouble* gridIdx1 = static_cast<pDouble*>(_gridIdx1);
    pDouble* gridIdx2 = static_cast<pDouble*>(_gridIdx2);
    pDouble* tShift   = static_cast<pDouble*>(_tShift);
    pInt* ModDim      = static_cast<pInt*>(_ModDim);

    // Get the dimensions of the image (with or without zero-padding)
    int mYHeight    = ModDim->first;
    int mXWidth     = ModDim->second;
    // And half-width and height
    double wHalf    = mXWidth / 2.0;
    double hHalf    = mYHeight / 2.0;
    //Scaled up versions of the shifts
    double sXShift  = tShift->first * subSampFact;
    double sYShift  = tShift->second * subSampFact;

    // get the displacements
    double disp_y_height = gridIdx1->first - gridIdx2->first;
    double disp_x_width = gridIdx1->second - gridIdx2->second;

    ////std::cout<<disp_y_height<<" "<<disp_x_width<<std::endl;
    // Do some sanity checks
    if (mYHeight <=0 || mXWidth <=0) return -1;

    if (subSampFact <=0) return -2;

    // We do not consider diagonal cases, i.e., displacements in both
    // along rows and cols
    if ((disp_x_width == 0 && disp_y_height != 0) ||
        (disp_x_width != 0 && disp_y_height == 0))
    {
      // At this time, we need to consider the direction of the
      // recent image w.r.t a previous image. Thus, the current image
      // is the image that will be "morphed" (in this case a translation shift)
      // on to a previous image. In registration terminology, gridIdx1 pertains
      // to a "target" image, while gridIdx2 pertains to a "reference" image.

      // a.) The target image is on the top, while the reference image is in
      //     the bottom; We morph the reference image to the target image. This
      //     pattern arises when the direction of traversal is top to bottom
      //     along the height (i.e., rows) of the grid.
      // b.) The target image is on the bottom, while the reference image is
      //     on the top; We morph the reference image to the target image. This
      //     pattern arises when the direction of traversal is bottom to top
      //     along the height (i.e., rows) of the grid
      // c.) The target image is on the left, while the reference image is on
      //     the right; We morph the reference image to the target image. This
      //     pattern arises when the direction of traversal is left to right
      //     along the width (i.e., cols) of the grid.
      // d.) The target image is on the right, while the reference image is on
      //     the left; We morph the reference image to the target image. This
      //     pattern arises when the direction of traversal is right to left
      //     along the width (i.e., cols) of the grid.
      if ((disp_x_width == 0) && (disp_y_height != 0))
      {
        // First, we estimate displacement along height
        if (disp_y_height < 0)
        {
          tShift->second = -(mYHeight - tShift->second*subSampFact);
        }
        else
        if (disp_y_height > 0)
        {
          tShift->second *= (subSampFact);
        }
        // Now, we estimate displacement along width. The camera would have been
        // tilted and even though the stage shows that no change has happened along
        // the columns, physically, there would be an apparent change in displacement
        if (sXShift > wHalf)
        {
          tShift->first = -(mXWidth - tShift->first*subSampFact);
        }
        else
        {
          tShift->first = tShift->first*subSampFact;
        }
      }
      else
      if ((disp_x_width != 0) && (disp_y_height == 0))
      {
        // First, we estimate displacement along width
        if (disp_x_width < 0)
        {
          tShift->first = -(mXWidth - tShift->first*subSampFact);
        }
        else
        if (disp_x_width > 0)
        {
          tShift->first *= (subSampFact);
        }
        // Now, we estimate displacement along height. The camera would have been
        // tilted and even though the stage shows that no change has happened along
        // the rows, physically, there would be an apparent change in displacement
        if (sYShift > hHalf)
        {
          tShift->second = -(mYHeight - tShift->second*subSampFact);
        }
        else
        {
          tShift->second = tShift->second*subSampFact;
        }
      }
    }
    return 1;
  }//end of function

  /*! \brief RegionalMaxima
   *  This function searches a 3x3 neighborhood around every non-border pixel and
   *  determines if the central pixel is a regional maxima. If it is, then the
   *  pixel location, along with the pixel value is placed in a vector. In addition
   *  the central pixel has to be strictly positive. Finally, to speed up the
   *  process, we can restrict the search area to lie within a bounding box.
   */
  template <class T>
  int RegionalMaxima(void* _mInvProd, int subSampFact, TShift* tShift)
  {
    // 1.) Typecast the data
    typedef std::complex<T>  Complex_t;
    typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Complex_Matrix;
    Complex_Matrix* mInvProd = static_cast<Complex_Matrix*>(_mInvProd);
    // 2.) Sanity checks
    if (!mInvProd->data()) return -1;

    // Initialize some default variables
    bool maximaFound = false;
    tShift->wXShift  = -1;
    tShift->hYShift  = -1;
    tShift->cost     = 1;

    // Extract the real component of the matrix
    typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;

    Matrix pReal = mInvProd->real();

    // Omit subSampFact pixels near the borders. Hence, there should at least
    // be more than subSampFact pixels overlap (after subsampling if used)
    if (subSampFact > 0)
    {
      // We will never encounter boundary pixels
      for (int y = subSampFact; y < pReal.rows()- subSampFact; ++y)
      for (int x = subSampFact; x < pReal.cols()- subSampFact; ++x)
      {
        // We are only interested in positive values
        T g = static_cast<T>(pReal(y, x));
        // Have we obtained a positive value
        if (g > 0)
        {
          // Check if it is a local maxima
          if (g > pReal(y-1,x-1) && g > pReal(y-1,x) && g > pReal(y-1,x+1) &&\
              g > pReal(y,x+1)   && g > pReal(y+1,x+1) && g > pReal(y+1,x) &&\
              g > pReal(y+1,x-1) && g > pReal(y,x-1) )
          {
            // This is a local maxima, but is this a global maxima?
            if (!maximaFound)
            {
              // This is the first instance
              tShift->wXShift = x;
              tShift->hYShift = y;
              tShift->cost    = static_cast<double>(g);
              maximaFound     = true;
            }
            else
            {
              if (g > tShift->cost)
              {
                tShift->wXShift = x;
                tShift->hYShift = y;
                tShift->cost    = static_cast<double>(g);
              }
           }
          }
        }// positive value
      }//finished checking all values
    }
    pReal.resize(0,0);
    return 1;
  }//end of function

  /*! \brief ComputeUpsampledFFT
   *  This function takes a 2D complex Matrix containing the cross-correlation
   *  between two images and applied upsampling in the Fourier domain to return
   *  a modified image. For more details, please refer to:
   *  Manuel Guizar-Sicairos, Samuel T. Thurman, and James R. Fienup,
   *  "Efficient subpixel image registration algorithms," Opt. Lett. 33, 156-158 (2008).
   *  @_Input_Matrix    : The complex input matrix containg the phase information
   *  @fftObj           : An object holding FFT related functionality
   *  @UpSamplingFactor : The factor that will be used to upsample the image in
   *                      the Fourier domain
   *  @nOffsetHeight    : A scaling factor along the rows
   *  @nOffsetWidth     : A scaling factor along the cols
   *  @height_offset    : An initial (pixel-level) shift along the rows
   *  @width_offset     : An initial (pixel-level) shift along the cols
   *  @peak_rloc        : The x-coordinate of the peak in the upsampled grid
   *  @peak_cloc        : The y-coordinate of the peak in the upsampled grid
   */
  template <class T>
  int ComputeUpsampledFFT(  void* _Input_Matrix, FFT2D<T>* fftObj, \
                            T UpSamplingFactor, \
                            T nOffsetHeight, T nOffsetWidth,
                            T height_offset, T width_offset,
                            int* peak_rloc, int* peak_cloc)
  {
    // 1.) Typecast the data
    typedef std::complex<T>  Complex_t;
    typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Complex_Matrix;
    Complex_Matrix* Input_Matrix = static_cast<Complex_Matrix*>(_Input_Matrix);
    if (!Input_Matrix->data()) return -1;

    // Get the dimensions of the matrix
    int height = Input_Matrix->rows();
    int width  = Input_Matrix->cols();
    if (height <=0 || width <=0 ) return -2;

    if (!fftObj) return -3;

    int hHeight = (height >> 1);
    int hWidth  = (width >> 1);

    //////////////////////////////////////////////////////////////////////////////
    std::complex<T> a1(0, -2.0*M_PI / (width * UpSamplingFactor)),
                    a2(0, -2.0*M_PI / (height * UpSamplingFactor));

    //////////////////////////////////////////////////////////////////////////////
    // Compute kernel for columns
    Complex_Matrix b(width, 1);
    for (auto i = 0; i < b.rows(); ++i)
    {
      b(i, 0) = std::complex<T>(i-hWidth, 0);
    }

    // Effect a circular shift
    fftObj->CircularShift(&b);

    Complex_Matrix c(1, static_cast<int>(floor(nOffsetWidth + 0.5)));
    for (auto i = 0; i < c.cols(); ++i)
    {
      c(0, i) = std::complex<T>(i - (width_offset), 0);
    }
    Complex_Matrix colKernel = (a1*b*c).array().exp().conjugate();

    //////////////////////////////////////////////////////////////////////////////
    // Compute kernel for rows
    b.resize(1, height);
    for (auto i = 0; i < b.cols(); ++i)
    {
      b(0, i) = std::complex<T>(i-hHeight, 0);
    }

    // Effect a circular shift
    fftObj->CircularShift(&b);

    c.resize(static_cast<int>(floor(nOffsetHeight + 0.5)), 1);
    for (auto i = 0; i < c.rows(); ++i)
    {
      c(i, 0) = std::complex<T>(i - (height_offset), 0);
    }
    // Compute kernel for rows
    Complex_Matrix rowKernel = (a2*c*b).array().exp().conjugate();

    // and finally get the maximum in this subsampled grid
    (rowKernel * (*Input_Matrix) * colKernel).array().abs().maxCoeff(peak_rloc, peak_cloc);

    rowKernel.resize(0,0);
    colKernel.resize(0,0);
    b.resize(0,0);
    c.resize(0,0);
    return 1;
  }//end of function

  /*! \brief ComputeTranslationShift
  *  This function computes an X-Y translation shift between two images using the
  *  Fast-Fourier transform. In this function, the FFT's of the reference image
  *  is passed by reference, while the fft of the target image is computed and
  *  returned to the calling function.
  *  @matrix1          : FFT of the reference image
  *  @matrix2          : FFT of the target image returned to the calling function
  *  @tShift           : A structure holding displacements as well cost of Norm cross-correlation
  */
  template <class T>
  int FFTTranslation<T>::ComputeTranslationShift(void* _matrix1, void* _matrix2, \
                                                 void* _fftObj, TShift* tShift)
  {
    // 1.) Typecast the data
    typedef std::complex<T>  Complex_t;
    typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Complex_Matrix;
    Complex_Matrix* matrix1 = static_cast<Complex_Matrix*>(_matrix1);
    Complex_Matrix* matrix2 = static_cast<Complex_Matrix*>(_matrix2);
    FFT2D<T>* fftObj = static_cast<FFT2D<T>*>(_fftObj);

    // 2.) Do some sanity checks
    if (!matrix1->data() || !matrix2->data()) return -1;
    // Get the dimensions of the matrix
    int height = matrix1->rows();
    int width  = matrix1->cols();
    if (height <=0 || width <=0 ) return -2;

    // 3.) Compute the product G = (G1 * G2')/(|G1*G2'|);
    Complex_Matrix CCorrelate = Complex_Matrix::Zero(height,width);
#ifdef USE_OPENMP
#pragma omp parallel for num_threads(4)
#endif
    for (long i = 0; i < height*width; ++i)
    {
      {
        Complex_t c = (matrix1->data()[i] * std::conj(matrix2->data()[i]));
        CCorrelate.data()[i] = c/(static_cast<T>(1.0e-6) + std::abs(c));
      }
    }

    // 4.) Obtain the phase map by computing the inverse FFT
    Complex_Matrix PhaseMap  = Complex_Matrix::Zero(height, width);
    fftObj->Inverse(&CCorrelate, &PhaseMap);
    
/*#ifdef DEBUG_FFT
      typedef itk::Image<double, 2> ImageType;
      typedef itk::ImageFileWriter<ImageType> Writer;
      typename ImageType::Pointer image = ImageType::New();
      typename ImageType::SizeType mSize;
      mSize[0] = width;
      mSize[1] = height;
      ImageType::IndexType mIdx;
      mIdx[0] = 0;
      mIdx[1] = 0;
      ImageType::RegionType mRegion;
      mRegion.SetSize(mSize);
      mRegion.SetIndex(mIdx);
      image->SetRegions(mRegion);
      image->Allocate();
      double* buff = image->GetBufferPointer();
      for (long h = 0; h < width*height; ++h)
        buff[h] = (PhaseMap).data()[h].real();
      typename Writer::Pointer writer = Writer::New();
      writer->SetInput(image);
      writer->SetFileName("dummy.nrrd");
      writer->Update();
#endif*/
    // 3.) Compute pixel level shift
    RegionalMaxima<T>(&PhaseMap, subSampFact, tShift);
    return 1;
    // 4.) Apply subpixel level shifts if requested
    if (1)//doSubpixelLevel)
    {
      int peak_rloc, peak_cloc;
      T tShift1 = static_cast<T>(UpSamplingFactor)*static_cast<T>(0.75);

      T p = static_cast<T>(UpSamplingFactor)*static_cast<T>(1.5);
      T q = tShift1 - static_cast<T>(tShift->hYShift) * static_cast<T>(UpSamplingFactor);
      T r = tShift1 - static_cast<T>(tShift->wXShift) * static_cast<T>(UpSamplingFactor);
      // compute the sub-pixel offsets using upsampling
      ComputeUpsampledFFT<T>( &CCorrelate, fftObj, \
                              UpSamplingFactor, \
                              p, p, \
                              q, r, \
                              &peak_rloc, &peak_cloc);
      // and the final sub-pixel shifts
      tShift->hYShift += ((double)peak_rloc - (double)tShift1)/(double)UpSamplingFactor;
      tShift->wXShift += ((double)peak_cloc - (double)tShift1)/(double)UpSamplingFactor;
    }

    // Free memory
    PhaseMap.resize(0,0);
    CCorrelate.resize(0,0);
    return 1;
  }//end of function

  /*! \brief Initialize
  *  This function initializes memory that will be needed for various FFT related
  *  objects.
  *  @width      : The width of images that will be analyzed
  *  @height     : The height of images that will be analyzed
  */
  template <class T>
  void FFTTranslation<T>::Initialize( int sFact )
  {
    // Setup some variables to handle subsampled data
    subSampFact = 4/sFact;
  }//end of function

  // explicit templates
  template class FFTTranslation<float>;
  template class FFTTranslation<double>;
}//end of namespace

