#ifndef _FFTUTILS_H_
#define _FFTUTILS_H_
#include <memory>

namespace spin
{
  /*! \brief Typedef for translation shift parameters*/
  struct TShift
  {
    double wXShift; // shift along the cols
    double hYShift; // shift along the rows
    double cost;    // cost
  };//end of struct
  
  /*! \brief FFTTranslation
   *  This class is a placeholder for all components pertaining to FFT based
   *  displacement estimation between two images. This is a base class that 
   *  can be derived from other classes that are used for other applications
   *  like rotation and scaling.
   */
  template <class T>
  class FFTTranslation
  {
    public: 
      /*! \brief Default constructor */
      FFTTranslation(int sFact=1, bool subPix=true, float upSampFact=17.0)
      {
        // Set various other elements
        doSubpixelLevel = subPix;
        UpSamplingFactor= upSampFact;
        // Initialize some variables
        Initialize(sFact);
      };

      /*! \brief Default destructor */
      ~FFTTranslation(){}

      /*! \brief API to compute translation shift when two FFT's are present*/
      int  ComputeTranslationShift( void* matrix1, void* matrix2, void* fftObj, TShift* disp);      
      /*! \brief API to convert displacements to offsets */
      int  CorrectOffset( void* _gridIdx1, void* _gridIdx2, void* _tShift, void* _ModDim, int subSampFact);

    private:
      bool            doSubpixelLevel;
      int             subSampFact;
      float           UpSamplingFactor;
      /*! API to initialize matrices such that we do not have to recompute them over and over again*/
      void            Initialize(int sFact);  
  };//end of class
}//end of namespace

#endif
