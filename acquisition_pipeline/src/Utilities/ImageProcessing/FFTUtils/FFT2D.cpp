#include "Utilities/ImageProcessing/FFTUtils/FFT2D.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#ifdef USE_OPENMP
#include <omp.h>
#endif
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cmath>
namespace spin
{
  /*! \brief A structure to hold information about Periodic+Smooth
   *  FFT computation
   */
  template <class T>
  struct PerFFT
  {
    typedef std::complex<T>  Complex_t;
    typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> ComplexMatrix;
    ComplexMatrix CosineFactor;

    ~PerFFT()
    {
      CosineFactor.resize(0,0);
    }

    /*! \brief Initialize */
    void Initialize(int height, int width)
    {
      // Allocate memory for the cosine factor matrix
      CosineFactor = ComplexMatrix::Zero(height, width);

      //initialize the smooth image as well as the scaling factor
      T cx = static_cast<T>(2.0)*static_cast<T>(M_PI) / static_cast<T>(width);
      T cy = static_cast<T>(2.0)*static_cast<T>(M_PI) / static_cast<T>(height);

      for (int x = 0; x < width; ++x)
      for (int y = 0; y < height; ++y)
      {
        // If it's not the DC component, modify the pixel.
        if ((x+y)!=0)
        {
          T a = std::cos(cx*static_cast<T>(x));
          T b = static_cast<T>(0.5) / (static_cast<T>(2.0) - a - std::cos(cy*static_cast<T>(y)));
          CosineFactor(y,x) = Complex_t((T)b,(T)0);
        }
      }
    }
  };//end of struct

  /*! \brief Default constructor */
  template <class T>
  FFT2D<T>::FFT2D(int yRows, int xCols)
  {
    // Check for valid numbers
    if (yRows > 0 && xCols > 0)
    {
      // We traversing across columns, the length of the FFT will be equal to
      // the # of rows, and vice-versa
      colfftPtrFwd = std::make_shared< fft >(yRows,false);
      colfftPtrInv = std::make_shared< fft >(yRows,true);
      rowfftPtrFwd = std::make_shared< fft >(xCols,false);
      rowfftPtrInv = std::make_shared< fft >(xCols,true);

      // Instantiate elements for perfft
      perfft = std::make_shared<PerFFT<T> >();
      perfft.get()->Initialize(yRows, xCols);
    }
  }//end of function

  /*! \brief Forward
   *  This function implements a Forward transform across a 2D eigen matrix
   */
  template <class T>
  int FFT2D<T>::Forward(void* _src, void* _dst, bool aRows, bool aCols)
  {
    // get the pixel type
    typedef std::complex<T>  Complex_t;
    typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Complex_Matrix;
    Complex_Matrix* src = static_cast<Complex_Matrix*>(_src);
    Complex_Matrix* dst = static_cast<Complex_Matrix*>(_dst);
    // some sanity checks
    if ( !src->data() || !dst->data() ) return -1;
    if (src->rows() != colfftPtrFwd.get()->GetDim()) return -2;
    if (src->cols() != rowfftPtrFwd.get()->GetDim()) return -3;

    // First, we compute FFT across columns and then across rows.
#ifdef USE_OPENMP
    #pragma omp parallel for num_threads(4)
#endif
    for (int cols = 0; cols < src->cols(); ++cols)
    {
      {
        Complex_Matrix t1 = src->col(cols);
        Complex_Matrix t2 = t1;
        colfftPtrFwd.get()->transform(t1.data(), t2.data());
        dst->col(cols) = t2;
        t1.resize(0,0);
        t2.resize(0,0);
      }
    }

#ifdef USE_OPENMP
    #pragma omp parallel for num_threads(4)
#endif
    for (int rows = 0; rows < dst->rows(); ++rows)
    {
      {
        Complex_Matrix t1 = dst->row(rows);
        Complex_Matrix t2 = t1;
        rowfftPtrFwd.get()->transform(t1.data(), t2.data());
        dst->row(rows) = t2;
        t1.resize(0,0);
        t2.resize(0,0);
      }
    }

    return 1;
  }//end of function

  /*! \brief Inverse
   *  This function implements an Inverse transform across a 2D eigen matrix
   */
  template <class T>
  int FFT2D<T>::Inverse(void* _src, void* _dst, bool aRows, bool aCols)
  {
    // get the pixel type
    typedef std::complex<T>  Complex_t;
    typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Complex_Matrix;
    Complex_Matrix* src = static_cast<Complex_Matrix*>(_src);
    Complex_Matrix* dst = static_cast<Complex_Matrix*>(_dst);
    // some sanity checks
    if ( !src->data() || !dst->data() ) return -1;

    // First, we compute FFT across columns and then across rows.
#ifdef USE_OPENMP
    #pragma omp parallel for num_threads(4)
#endif
    for (int rows = 0; rows < src->rows(); ++rows)
    {
      Complex_Matrix t1 = src->row(rows);
      Complex_Matrix t2 = t1;
      rowfftPtrInv.get()->transform(t1.data(), t2.data());
      dst->row(rows) = t2;
      t1.resize(0,0);
      t2.resize(0,0);
    }

#ifdef USE_OPENMP
    #pragma omp parallel for num_threads(4)
#endif
    for (int cols = 0; cols < dst->cols(); ++cols)
    {
      {
        Complex_Matrix t1 = dst->col(cols);
        Complex_Matrix t2 = t1;
        colfftPtrInv.get()->transform(t1.data(), t2.data());
        dst->col(cols) = t2;
        t1.resize(0,0);
        t2.resize(0,0);
      }
    }

    return 1;
  }//end of function

 /*! \brief ComputePeriodicImageFFT
  *  This function computes an FFT off an image by decomposing it into its
  *  Periodic and smooth versions using and algorithm proposed by Lionel Moisan.
  *  "Periodic plus smooth image decomposition"
  *  @_src     : Input image that is represented as a complex matrix
  *  @_dst     : The output image after a FFT is done
  */
  template <class T>
  int FFT2D<T>::ComputePeriodicImageFFT(void* _src, void* _dst)
  {
    // get the pixel type
    typedef std::complex<T>  Complex_t;
    typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Complex_Matrix;
    Complex_Matrix* src = static_cast<Complex_Matrix*>(_src);
    Complex_Matrix* dst = static_cast<Complex_Matrix*>(_dst);
    // some sanity checks
    if ( !src->data()) return -1;

    // Get the dimensions of the image
    int nx  = src->cols();
    int ny  = src->rows();

    // Allocate memory for dst
    if (dst->rows() == 0 || dst->cols() == 0)
    (*dst) = Complex_Matrix::Zero(ny,nx);
    // build up elements for the smooth image
    Complex_Matrix v      = Complex_Matrix::Zero(ny,nx);
    v.block(0,0,1,nx)     = src->block(0,0,1,nx) - src->block(ny-1,0,1,nx);
    v.block(ny-1,0,1,nx)  = v.block(0,0,1,nx) * (-1.0);
    v.block(0,0,ny,1)     = v.block(0,0,ny,1) + \
                            src->block(0,0,ny,1) - \
                            src->block(0,nx-1,ny,1);
    v.block(0,nx-1,ny,1)  = v.block(0,nx-1,ny,1) - \
                            src->block(0,0,ny,1) + \
                            src->block(0,nx-1,ny,1);

    // Compute the FFT of the original image and the smooth component
    Complex_Matrix fft1  = Complex_Matrix::Zero(ny, nx);
    Complex_Matrix fft2  = Complex_Matrix::Zero(ny, nx);
    Forward(src, &fft1);
    Forward(&v , &fft2);
    //Modify the (0,0) frequency component
    fft2(0,0) = Complex_t(0,0);
    // Compute the periodic FFT
    (*dst) = fft1.array() - fft2.array() * perfft.get()->CosineFactor.array();
    fft1.resize(0,0);
    fft2.resize(0,0);
    v.resize(0,0);
    return 1;
  }//end of function

  /*! \brief CircularShift
   *  This function takes a 2D complex Matrix and circularly shifts the rows/cols
   *  of the matrix depending on the input parameters. The matrix is overwritten
   *  @Input_Matrix : The complex matrix whose columns and rows need to be shifted
   *                  and that will be overwritten at the end of the operation
   */
  template <class T>
  int FFT2D<T>::CircularShift(void* _Input_Matrix)
  {
    // get the pixel type
    typedef std::complex<T>  Complex_t;
    typedef Eigen::Matrix<Complex_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Complex_Matrix;
    Complex_Matrix* Input_Matrix = static_cast<Complex_Matrix*>(_Input_Matrix);

    if (!Input_Matrix->data()) return -1;
    int height = Input_Matrix->rows();
    int width  = Input_Matrix->cols();
    if (height <=0 || width <=0) return -2;

    // Build the co-ordinates that would be used to effect the circular shift
    int hHeight= height>>1;
    int hWidth = width>>1;
    // A scratch buffer
    Complex_Matrix temp(height,width);

    // Circular shift operations.
    temp.block(0,0,height-hHeight,width)                = (*Input_Matrix).block(hHeight,0,height-hHeight,width);
    temp.block(height-hHeight,0,hHeight,width)          = (*Input_Matrix).block(0,0,hHeight,width);
    (*Input_Matrix).block(0,0,height,width-hWidth)      = temp.block(0,hWidth,height,width-hWidth);
    (*Input_Matrix).block(0,width-hWidth,height,hWidth) = temp.block(0,0,height,hWidth);

    temp.resize(0,0);
    return 1;
  }//end of function

  // Explicit templates
  template class FFT2D<float>;
  template class FFT2D<double>;
}
