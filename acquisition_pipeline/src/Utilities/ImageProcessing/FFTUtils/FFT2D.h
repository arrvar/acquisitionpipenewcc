#ifndef FFT2D_H_
#define FFT2D_H_
#include <memory>
#include "Utilities/ImageProcessing/FFTUtils/kissfft.hh"

namespace spin 
{
  template <class T>
  struct PerFFT;
  
  /*! \brief FFT2D 
   *  This class is used to compute FFT's for 2-dimensional data
   *  using kissFFT as the backend
   */
  template <class T>
  class FFT2D
  {
    public:
      typedef kissfft<T> fft;
      FFT2D(int yRows, int xCols);
      ~FFT2D(){}
      /*! \brief Forward */
      int Forward(void* src, void* dest, bool aRows=true, bool aCols=true);
      /*! \brief Inverse */
      int Inverse(void* src, void* dest, bool aRows=true, bool aCols=true);
      /*! \brief P+S FFT */
      int ComputePeriodicImageFFT(void* _src, void* _dst);
      /*! \brief Cicrular shift */
      int CircularShift(void* _src);
    public:
      std::shared_ptr< fft > colfftPtrFwd;
      std::shared_ptr< fft > rowfftPtrFwd;
      std::shared_ptr< fft > colfftPtrInv;
      std::shared_ptr< fft > rowfftPtrInv;
      std::shared_ptr< PerFFT<T> > perfft;
  };//end of class
}//end of namespace
#endif
