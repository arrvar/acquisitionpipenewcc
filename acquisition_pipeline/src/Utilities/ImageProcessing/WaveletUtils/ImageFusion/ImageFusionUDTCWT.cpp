#include "Utilities/ImageProcessing/WaveletUtils/ImageFusion/ImageFusionUDTCWT.h"
#include "Utilities/ImageProcessing/WaveletUtils/ImageFusion/IFStruct.h"
#include "Utilities/ImageProcessing/WaveletTransform/UDTCWT2D.h"
#include "Utilities/MatrixUtils/MatrixUtils.h"
#include "AbstractClasses/AbstractWT2D.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include <vector>
#ifdef USE_OPENMP
#include <omp.h>
#endif

namespace spin
{
  namespace
  {
    typedef boost::container::map<int,WTStruct2D> pyramid;
    typedef boost::container::map<int, pyramid> pyramid2;
  }

  namespace UTransform
  {
    /*! \brief Band2Matrix
     *  This function converts all bands in an image to 1D column vectors.
     */
    int Band2Matrix(int index, pyramid* inpPyr, pyramid* outPyr)
    {
      // sanity checks
      if (!inpPyr) return -1;
      if (inpPyr->size() <=0) return -2;
      if (!outPyr) return -3;
      if (outPyr->size() <=0) return -4;

      // Get the number of levels
      int levels = inpPyr->size();

      //traverse through all bands in the pyramid and resize the matrix
      for (int i = 0; i < levels; ++i)
      {
        // Set the original dimensions at this level
        (*outPyr)[i].height = (*inpPyr)[i].height;
        (*outPyr)[i].width  = (*inpPyr)[i].width;

        //////////////////HL///////////////////////////
        // Check if memory has been allocated
        if ( !(*outPyr)[i].HL.data() ) return -5;
        int size = (*inpPyr)[i].HL.rows() * (*inpPyr)[i].HL.cols();
        // Reshape the matrix and assign it to the correct index
        (*outPyr)[i].HL.col(index) = Eigen::Map<RMMatrix_Float> ((*inpPyr)[i].HL.data(), size,1);

        //////////////////HH///////////////////////////
        // Check if memory has been allocated
        if ( !(*outPyr)[i].HH.data() ) return -6;
        // Reshape the matrix and assign it to the correct index
        (*outPyr)[i].HH.col(index) = Eigen::Map<RMMatrix_Float>((*inpPyr)[i].HH.data(), size,1);

        //////////////////LH///////////////////////////
        // Check if memory has been allocated
        if ( !(*outPyr)[i].LH.data() ) return -7;
        // Reshape the matrix and assign it to the correct index
        (*outPyr)[i].LH.col(index) = Eigen::Map<RMMatrix_Float>((*inpPyr)[i].LH.data(), size,1);

        if (i == (levels-1))
        {
          if ( !(*outPyr)[i].LL.data() ) return -8;
          // Reshape the matrix and assign it to the correct index
          (*outPyr)[i].LL.col(index) = Eigen::Map<RMMatrix_Float>((*inpPyr)[i].LL.data(), size,1);
        }
      }
      return 1;
    }//end of function

    /*! \brief MergeHighFrequency
     *  This pipeline is used to fuse data from high-frequency components.
     */
    void MergeHighFrequency(RMMatrix_Float* input, RMMatrix_Float* output, int height, int width)
    {
      RMMatrix_Float temp1;
      MatrixUtils<RMMatrix_Float> mutils;
      // 1.) Compute the location of the maxima in the matrix (rowwise)
      mutils.RowwiseMaxima(input, &temp1, height, width, 2);
      // 4.) Reshape the data to be that of height,width
      RMMatrix_Float temp2 = temp1.col(0);
      (*output) = Eigen::Map<RMMatrix_Float>(temp2.data(),height,width);
    }//end of function

    /*! \brief MergeHighFrequency
     *  This pipeline is used to fuse data from high-frequency components.
     */
    void JointMergingOfHighFrequency( RMMatrix_Float* HL, RMMatrix_Float* HH, RMMatrix_Float* LH,\
                                      int height, int width,\
                                      RMMatrix_Float* HL1, RMMatrix_Float* HH1, RMMatrix_Float* LH1)
    {
      RMMatrix_Float temp_HL, temp_HH, temp_LH;
      MatrixUtils<RMMatrix_Float> mutils;
      // 1.) Compute the location of the maxima in the matrix (rowwise)
      mutils.RowwiseMaxima(HL, &temp_HL, height, width, 2);
      mutils.RowwiseMaxima(HH, &temp_HH, height, width, 2);
      mutils.RowwiseMaxima(LH, &temp_LH, height, width, 2);

      // 2.) At the end of this operation, we need to check all the bands for consistency.
      //     Suppose if 2 out of 3 bands come from a particular image, then the third band is also
      //     made part of the dominant band.
      RMMatrix_Float check = RMMatrix_Float::Zero(temp_HL.rows(),3);

      RMMatrix_Float check_HL = RMMatrix_Float::Zero(temp_HL.rows(),1);
      RMMatrix_Float check_HH = RMMatrix_Float::Zero(temp_HL.rows(),1);
      RMMatrix_Float check_LH = RMMatrix_Float::Zero(temp_HL.rows(),1);

      for (int i = 0; i < check_HL.rows(); ++i)
      {
        // check if the consistency criterion is satisfied
        if ( (check(i,0) == check(i,1)) && (check(i,0) != check(i,2)))
        {
          check_HL(i,0) = (*HL)(i,(int)check(i,0));
          check_HH(i,0) = (*HH)(i,(int)check(i,0));
          check_LH(i,0) = (*LH)(i,(int)check(i,0));
        }
        else
        if ( (check(i,0) != check(i,1)) && (check(i,0) == check(i,2)))
        {
          check_HL(i,0) = (*HL)(i,(int)check(i,2));
          check_HH(i,0) = (*HH)(i,(int)check(i,2));
          check_LH(i,0) = (*LH)(i,(int)check(i,2));
        }
        else
        if ( (check(i,1) == check(i,2)) && (check(i,1) != check(i,0)))
        {
          check_HL(i,0) = (*HL)(i,(int)check(i,1));
          check_HH(i,0) = (*HH)(i,(int)check(i,1));
          check_LH(i,0) = (*LH)(i,(int)check(i,1));
        }
        else
        {
          check_HL(i,0) = (*HL)(i,(int)check(i,0));
          check_HH(i,0) = (*HH)(i,(int)check(i,1));
          check_LH(i,0) = (*LH)(i,(int)check(i,2));
        }
      }

      (*HL1) = Eigen::Map<RMMatrix_Float>(check_HL.data(),height,width);
      (*HH1) = Eigen::Map<RMMatrix_Float>(check_HH.data(),height,width);
      (*LH1) = Eigen::Map<RMMatrix_Float>(check_LH.data(),height,width);
    }//end of function



    /*! \brief MergeLowFrequency
     *  This pipeline is used to fuse data from low-frequency components
     */
    void MergeLowFrequency(RMMatrix_Float* input, RMMatrix_Float* output, int height, int width)
    {
      // 1.) Compute the location of the maxima in the matrix (rowwise)
      RMMatrix_Float temp1;
      MatrixUtils<RMMatrix_Float> mutils;
      mutils.RowwiseMean(input, &temp1);
      // 4.) Reshape the data to be that of height,width
      (*output) = Eigen::Map<RMMatrix_Float>(temp1.data(),height,width);
    }//end of function

    /*! \brief ExtractBandsFromDTCWT
     *  Given a set of images in a vector, we compute the DTCWT for
     *  each channel of each image. Subsequently, the bands are reshaped
     *  into a map of rectangular matrices for each band in each level
     *  for each channel of every image!
     *  @_input       : A std::vector of RGBTriplets
     *  @levels       : the number of levels of wavelet decomposition required
     *  @filter       : The QMF filters used for wavelet decomposition
     *  @_output      : A std::map of reshaped bands with the # of columns
     *                  indicating the number of data points.
     */
    void ExtractBandsFromDTCWT( std::vector<RMMatrix_Float>* input, \
                                int levels, int filter, \
                                IFStruct* output,\
                                DTCWTInformation* info)
    {
      // Allocate memory for the augmented bands. Data conditioning and sanity checks
      // should have been done prior to calling this function. There will be 6 pyramids in total
      // pertaining to tree 1 and tree 2 from all 3 channels.
      int height = ((*input)[0].rows());
      int width  = ((*input)[0].cols());

      // Allocate memory for the output map
      for (int i = 0; i < levels; ++i)
      {
        for (int j = 0; j < 2; ++j)
        {
          // Allocating memory for HL for both trees
          output->c1[j][i].HL = RMMatrix_Float::Zero(height*width,input->size());
          output->c1[j][i].HH = RMMatrix_Float::Zero(height*width,input->size());
          output->c1[j][i].LH = RMMatrix_Float::Zero(height*width,input->size());

          if (i == (levels-1) )
          {
            // Allocating memory for LH for both trees
            output->c1[j][i].LL = RMMatrix_Float::Zero(height*width,input->size());
          }
        }
      }

      // We run all images in parallel and do thw following:
      // a.) A DTWCWT on each channel,
      // b.) Reshaping of bands from both trees into 1-D vectors
  #ifdef USE_OPENMP
  #pragma omp parallel for
  #endif
      for (int i = 0; i < input->size(); ++i)
      {
        {
          // Instantiate dual tree complex wavelet transform objects for all channels
          std::shared_ptr<AbstractWT2D> wt_c1 = std::make_shared<spin::UDTCWT2D>(levels,filter);

          // implement a DTCWT for the images
          wt_c1.get()->Compute(&(*input)[i], NULL);

          // And extract the left and right tree subbands
          pyramid2 c1;
          wt_c1.get()->GetBands(&c1);

          // Finally, reshape all bands to 1-D column vectors and place them in
          // their respective locations
          Band2Matrix(i, &c1[0], &(output->c1[0]));
          Band2Matrix(i, &c1[1], &(output->c1[1]));
        }
      }

      // populate info
      info->height = (*input)[0].rows();
      info->width  = (*input)[0].cols();
      info->filter = filter;
      info->levels = output->c1[0].size();
    }//end of function

    /*! \brief FusionPipeline
     *  This function does two steps:
     *  a.) At any given level, it finds the absolute max value across all images in
     *      a given band and returns the max value as well as the image location.
     *  b.) Having found the indices, a consistency check is performed in all bands.
     *      In a 5x5 window, a median filter is applied to remove inconsistencies that
     *      may happen wherin the central pixel comes from a particular image, while
     *      all neighboring pixels come from other images.
     *  @_input       : A std::map of levels containing band information from all images
     *  @_output      : A std::map of "fused bands".
     */
    void FusionPipeline( IFStruct* input, IFStruct* output)
    {
      // Get the number of levels from a wavelet decomposition
      int levels = input->c1[0].size();
      // 1.) We Allocate memory for the output data
      for (int i = 0; i < levels; ++i)
      {
        // Allocating memory for HL for both trees
        output->c1[0][i].HL = RMMatrix_Float::Zero(1,1);
        output->c1[1][i].HL = RMMatrix_Float::Zero(1,1);
        // Allocating memory for HH for both trees
        output->c1[0][i].HH = RMMatrix_Float::Zero(1,1);
        output->c1[1][i].HH = RMMatrix_Float::Zero(1,1);
        // Allocating memory for LH for both trees
        output->c1[0][i].LH = RMMatrix_Float::Zero(1,1);
        output->c1[1][i].LH = RMMatrix_Float::Zero(1,1);

        if (i == (levels-1) )
        {
          // Allocating memory for LH for both trees
          output->c1[0][i].LL = RMMatrix_Float::Zero(1,1);
          output->c1[1][i].LL = RMMatrix_Float::Zero(1,1);
        }
      }

      // 2.) We traverse through all levels and for each band in each channel
      //     and fuse high-frequencies. For low-frequencies, we take an average
      //     across all images in all channels
      // TODO: Can we do better load balancing?
  #ifdef USE_OPENMP
  #pragma omp parallel for
  #endif
      for (int i = 0; i < levels; ++i)
      {
        {
          // Compute the maxima of HL bands across all channels
          for (int j = 0; j < 2; ++j)
          {
            // Merge high frequencies
            MergeHighFrequency(&input->c1[j][i].HL, &output->c1[j][i].HL, input->c1[j][i].height, input->c1[j][i].width);
            MergeHighFrequency(&input->c1[j][i].HH, &output->c1[j][i].HH, input->c1[j][i].height, input->c1[j][i].width);
            MergeHighFrequency(&input->c1[j][i].LH, &output->c1[j][i].LH, input->c1[j][i].height, input->c1[j][i].width);
            if (i == (levels-1))
            {
              MergeLowFrequency(&input->c1[j][i].LL, &output->c1[j][i].LL, input->c1[j][i].height, input->c1[j][i].width);
            }
          }//finished for both trees in the DTCWT
        }
      }
    }//end of function

  }//end of namespace UTransform

  /*! \brief Compute
   *  This is the main interface for the DTWCT based image fusion pipeline
   *  @_input   : A vector of RGBTriplets
   *  @_output  : A single fused RGB triplet
   *  @levels   : The # of levels of wavelet decomposition allowed
   *  @filter   : The wavelet filterbank used.
   */
  int ImageFusionUDTCWT::Compute(void* _input, void* _output, int levels, int filter)
  {
    // Sanity checks
    std::vector<RMMatrix_Float>* input = static_cast<std::vector<RMMatrix_Float>*>(_input);
    if (!input) return -1;
    if (input->size() <=0) return -2;
    if (levels <=0) return -3;

    RMMatrix_Float* output = static_cast<RMMatrix_Float*>(_output);
    if (!output) return -4;

    // Allcoate a temporary output to store fused bands
    IFStruct bandsIn, bandsOut;
    DTCWTInformation info;
    // Implement a DTCWT on the input images
    UTransform::ExtractBandsFromDTCWT( input, levels, filter, &bandsIn, &info);
    // Run the fusion pipeline on the decomposed bands
    UTransform::FusionPipeline( &bandsIn, &bandsOut);
    // 3.) Perform an inverse wavelet transform on all the three channels to recover
    //     the three channels (fused).
    // We run all images in parallel and do thw following:
    // a.) A DTWCWT on each channel,
    // b.) Reshaping of bands from both trees into 1-D vectors
    std::shared_ptr<AbstractWT2D> wt_c1 = std::make_shared<spin::UDTCWT2D>(&bandsOut.c1, &info);
    // implement an inverse DTCWT for each channel
    wt_c1->Compute(NULL, output);
    return 1;
  }//end of function
}//end of namespace
