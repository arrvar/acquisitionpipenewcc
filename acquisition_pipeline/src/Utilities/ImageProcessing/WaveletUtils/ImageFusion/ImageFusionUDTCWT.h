#ifndef IMAGEFUSIONUDTCWT_H_
#define IMAGEFUSIONUDTCWT_H_

namespace spin
{
  class ImageFusionUDTCWT
  {
    public:
      ImageFusionUDTCWT(){}

      /*! \brief The main call to implement image fusion */
      int Compute(void* _input, void* _output, int l=3, int f=0);
  };//end of class
}//end of namespace
#endif
