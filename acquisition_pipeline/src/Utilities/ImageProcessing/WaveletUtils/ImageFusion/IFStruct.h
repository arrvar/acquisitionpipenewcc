#ifndef IFSTRUCT_H_
#define IFSTRUCT_H_
#include "Utilities/ImageProcessing/WaveletTransform/WTStruct2D.h"
#include <vector>

namespace spin
{
  struct IFStruct
  {
    ~IFStruct()
    {
      c1.clear();
    }
    // Bands pertaining to channel 1
    boost::container::map<int, boost::container::map<int,WTStruct2D> > c1;
  };//end of struct
}
#endif
