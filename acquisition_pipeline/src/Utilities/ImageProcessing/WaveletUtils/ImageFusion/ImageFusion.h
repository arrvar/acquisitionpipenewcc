#ifndef IMAGEFUSION_H_
#define IMAGEFUSION_H_

namespace spin
{
  class ImageFusion 
  {
    public:
      ImageFusion(){}
      
      /*! \brief The main call to implement image fusion */
      int Compute(void* _input, void* _output, int l=3, int f=0);
  };//end of class
}//end of namespace
#endif
