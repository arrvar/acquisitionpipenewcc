#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include "Utilities/ImageProcessing/ColorSpace/itkRGBToLabColorSpacePixelAccessor.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "itkVectorIndexSelectionCastImageFilter.h"
#include "itkTriangleThresholdImageFilter.h"
#include "itkImageAdaptor.h"
#include <vector>
#include <cstdlib>
#include <algorithm>

namespace spin
{
  namespace
  {
    typedef itk::Vector<float, 3> VectorPixelType;
    typedef itk::Accessor::RGBToLabColorSpacePixelAccessor<unsigned char, float> RGBToLabColorSpaceAccessorType;
    typedef itk::ImageAdaptor<RGBImageType, RGBToLabColorSpaceAccessorType> RGBToLabAdaptorType;
    typedef itk::VectorIndexSelectionCastImageFilter<RGBToLabAdaptorType, FloatImageType2D> VectorCastFilterType;
    typedef itk::TriangleThresholdImageFilter<FloatImageType2D, UCharImageType2D> ThresholdFilterType;
  }

  /*! \brief FuseInformation
    *  In this function we take an RGB image and apply a threshold on the L channel
    *  after transforming it to the LAB space. If the % of FG is > a certain threshold
    *  we do not fuse any information from the second RGB image. If it is lesss than
    *  this %, then the FG values of the image are fused with the other image
    */
  void FuseInformation(void* _aInput, void* _bInput, float thresholdPix)
  {
    RGBImageType::Pointer* aInput   = static_cast<RGBImageType::Pointer*>(_aInput);
    RGBImageType::Pointer* bInput   = static_cast<RGBImageType::Pointer*>(_bInput);
    RGBImageType::SizeType size = (*aInput)->GetLargestPossibleRegion().GetSize();
    int minPixels = std::round(thresholdPix * (size[0]*size[1])+0.5);
    RGBToLabAdaptorType::Pointer rgbToLabAdaptor = RGBToLabAdaptorType::New();
    VectorCastFilterType::Pointer vectorCastFilter = VectorCastFilterType::New();
    ThresholdFilterType::Pointer threshold = ThresholdFilterType::New();
    rgbToLabAdaptor->SetImage(*aInput);
    vectorCastFilter->SetInput(rgbToLabAdaptor);
    vectorCastFilter->SetIndex(0);
    threshold->SetInput(vectorCastFilter->GetOutput());
    threshold->SetInsideValue( 255 );
    threshold->SetOutsideValue( 0 );
    threshold->SetNumberOfHistogramBins( 256 );
    threshold->Update();
    //WriteITKImage<UCharImageType2D>("dump.png",threshold->GetOutput());
    // We iterate through the pixels in threshold and count them. If the
    // percentage is > minPixels, we do not do anything to _aINput.
    unsigned char* mask   = threshold->GetOutput()->GetBufferPointer();
    int count = 0;
    for (long h = 0; h < size[0]*size[1]; ++h)
    {
      if (mask[h] > 0) ++count;
    }
    if (count < minPixels)
    {
      // There is a predominant background in the image
      RGBPixelType* input   = (*aInput)->GetBufferPointer();
      RGBPixelType* output  = (*bInput)->GetBufferPointer();
      for (int h = 0; h < size[0]*size[1]; ++h)
      {
        if (mask[h] <= 0)
        {
          input[h][0] = output[h][0];
          input[h][1] = output[h][1];
          input[h][2] = output[h][2];
        }
      }
    }
  }//end of function

  /*! \brief MinMaxRGBImage
    *  This function computes the minimum and maximum values of each channel of
    *  a RGB image
    */
  void MinMaxRGBImage(void* _aInput, void* _mMin, void* _mMax)
  {
    typedef spin::RGBImageType RGBImageType;
    RGBImageType::Pointer* aInput   = static_cast<RGBImageType::Pointer*>(_aInput);
    RGBPixelType* ref   = (*aInput)->GetBufferPointer();
    RGBImageType::SizeType size = (*aInput)->GetLargestPossibleRegion().GetSize();

    std::vector<float>* mMin = static_cast<std::vector<float>*>(_mMin);
    std::vector<float>* mMax = static_cast<std::vector<float>*>(_mMax);

    mMin->assign(3,255.0);
    mMax->assign(3,0.0);

    // Iterate through all pixels
    for (int j = 0; j < (size[0]*size[1]); ++j)
    {
      if (ref[j][0] < (*mMin)[0]) (*mMin)[0] = ref[j][0];
      if (ref[j][1] < (*mMin)[1]) (*mMin)[1] = ref[j][1];
      if (ref[j][2] < (*mMin)[2]) (*mMin)[2] = ref[j][2];

      if (ref[j][0] > (*mMax)[0]) (*mMax)[0] = ref[j][0];
      if (ref[j][1] > (*mMax)[1]) (*mMax)[1] = ref[j][1];
      if (ref[j][2] > (*mMax)[2]) (*mMax)[2] = ref[j][2];
    }
    (*mMax)[0] = 230;
    (*mMax)[1] = 230;
    (*mMax)[2] = 230;
  }//end of function

  /*! \brief NormalizeRGBWithCalImage
    *  This function normalizes and returns a normalized RGB image by using
    *  a global min/max value
    */
  void NormalizeRGBImageWithCalImage(void* _aInput, void* _bRef, void* _mMax, void* _cOutput)
  {
    typedef spin::RGBImageType RGBImageType;
    RGBImageType::Pointer* aInput   = static_cast<RGBImageType::Pointer*>(_aInput);
    RGBImageType::Pointer* cOutput  = static_cast<RGBImageType::Pointer*>(_cOutput);
    RGBImageType::Pointer* bRef     = static_cast<RGBImageType::Pointer*>(_bRef);
    std::vector<float>* mMax        = static_cast<std::vector<float>*>(_mMax);

    RGBPixelType* input   = (*aInput)->GetBufferPointer();
    RGBPixelType* output  = (*cOutput)->GetBufferPointer();
    RGBPixelType* ref     = (*bRef)->GetBufferPointer();

    RGBImageType::SizeType size = (*aInput)->GetLargestPossibleRegion().GetSize();
#ifdef USE_OPENMP
#pragma omp parallel for num_threads(4)
#endif
    for (int j = 0; j < (size[0]*size[1]); ++j)
    {
      float r = floor((float)input[j][0]/ (1.0+(float)ref[j][0]) * (*mMax)[0] + 0.5);
      float g = floor((float)input[j][1]/ (1.0+(float)ref[j][1]) * (*mMax)[1] + 0.5);
      float b = floor((float)input[j][2]/ (1.0+(float)ref[j][2]) * (*mMax)[2] + 0.5);
      if (r > (*mMax)[0]) r = (*mMax)[0];
      if (g > (*mMax)[1]) g = (*mMax)[1];
      if (b > (*mMax)[2]) b = (*mMax)[2];
      output[j][0] = static_cast<unsigned char>(r);
      output[j][1] = static_cast<unsigned char>(g);
      output[j][2] = static_cast<unsigned char>(b);
    }
  }//end of function

  /*! \brief NormalizeRGBWithCalImage
    *  This function normalizes and returns a normalized RGB image
    *  without using any global min/max value
    */
  void NormalizeRGBImageWithCalImage(void* _aInput, void* _bRef, void* _cOutput)
  {
    typedef spin::RGBImageType RGBImageType;
    RGBImageType::Pointer* aInput   = static_cast<RGBImageType::Pointer*>(_aInput);
    RGBImageType::Pointer* cOutput  = static_cast<RGBImageType::Pointer*>(_cOutput);
    RGBImageType::Pointer* bRef     = static_cast<RGBImageType::Pointer*>(_bRef);

    RGBPixelType* input   = (*aInput)->GetBufferPointer();
    RGBPixelType* output  = (*cOutput)->GetBufferPointer();
    RGBPixelType* ref     = (*bRef)->GetBufferPointer();

    RGBImageType::SizeType size = (*aInput)->GetLargestPossibleRegion().GetSize();
#ifdef USE_OPENMP
#pragma omp parallel for num_threads(4)
#endif
    for (int j = 0; j < (size[0]*size[1]); ++j)
    {
      float r = (float)input[j][0]/ (1.0+(float)ref[j][0]);
      float g = (float)input[j][1]/ (1.0+(float)ref[j][1]);
      float b = (float)input[j][2]/ (1.0+(float)ref[j][2]);
      float v[]={r,g,b};
      float vMax = *(std::max_element(v,v+3));
      float vMin = *(std::min_element(v,v+3));
      vMax = (vMax+vMin)/2.0;
      // scale the values
      r = std::min((float)(r*vMax*255), 255.0f);
      g = std::min((float)(g*vMax*255), 255.0f);
      b = std::min((float)(b*vMax*255), 255.0f);

      output[j][0] = static_cast<unsigned char>(r);
      output[j][1] = static_cast<unsigned char>(g);
      output[j][2] = static_cast<unsigned char>(b);
    }
  }//end of function

  /*! \brief Image2Matrix
   *  This function converts an ITK image to a 2D Eigen matrix having 3 columns
   *  @_aInput	: The rgb image that needs to be made to an eigen matrix
   *  @_Mat	: The 3-column eigen matrix of template T
   */
  template <class T>
  int Image2Matrix(void* _aInput, void* _Mat)
  {
    typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;
    typedef spin::RGBImageType RGBImageType;
    typename RGBImageType::Pointer* aInput   = static_cast<typename RGBImageType::Pointer*>(_aInput);
    if (!aInput) return -1;
    Matrix* Mat = static_cast<Matrix*>(_Mat);
    //if (!Mat->data()) return -2;

    // Get the dimensions of the image
    spin::RGBImageType::SizeType size = (*aInput)->GetLargestPossibleRegion().GetSize();
    // Copy the data to the matrix
    long h = size[0]*size[1];
    *Mat = (Eigen::Map<RMMatrix_UChar>(reinterpret_cast<unsigned char*>((*aInput)->GetBufferPointer()),h,3)).template cast<T>();
    return 1;
  }//end of function

  /*! \brief Matrix2Image
   *  This function converts a 2D Matrix to an ITK RGB Image. It
   *  should be emphasized that the ITK image should be
   *  initialized prior to calling this function.
   */
  template <class T>
  int Matrix2Image(void* _Mat, void* _aImage)
  {
    typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;
    typename RGBImageType::Pointer* aInput   = static_cast<typename RGBImageType::Pointer*>(_aImage);
    if (!aInput) return -1;
    Matrix* Mat = static_cast<Matrix*>(_Mat);
    if (!Mat->data()) return -2;

    //Get the dimensions of the image
    spin::RGBImageType::SizeType size = (*aInput)->GetLargestPossibleRegion().GetSize();
    // Do nothing if there is a mismatch
    if (Mat->rows() != (size[0]*size[1])) return -3;
    // Also do nothing if the number of dimensions of the matrix does not equal 3
    if (Mat->cols() !=3) return -4;

    // Allocate the matrix with a dummy pixel value
    spin::RGBPixelType pix;
    pix.SetRed(0);pix.SetGreen(0);pix.SetBlue(0);
    (*aInput)->FillBuffer(pix);
    // Sadly this step is needed
    RMMatrix_UChar tempx = Mat->template cast<unsigned char>();
    // And copy the data back to an image
    memcpy((*aInput)->GetBufferPointer(), tempx.data(), 3 * size[0] * size[1]);
    tempx.resize(0,0);

    return 1;
  }//end of function

  /*! \brief Image2VectorOfChannels
   *  This function converts an ITK image to a vector of 2D Eigen Matrices
   *  @_aInput	: The rgb image that needs to be made to an eigen matrix
   *  @_Mat	: The 3-column eigen matrix of template T
   */
  template <class T>
  int Image2VectorOfMatrices(void* _aInput, void* _Mat)
  {
    typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;
    typedef std::vector<Matrix> vecMat;
    typedef spin::RGBImageType RGBImageType;
    typename RGBImageType::Pointer* aInput   = static_cast<typename RGBImageType::Pointer*>(_aInput);
    if (!aInput) return -1;
    vecMat* vMat = static_cast<vecMat*>(_Mat);
    // Get the dimensions of the image
    spin::RGBImageType::SizeType size = (*aInput)->GetLargestPossibleRegion().GetSize();
    // Copy the data to the matrix
    long h = size[0]*size[1];
    // Sanity check on matrix data allocation
    if (vMat->size() !=3) return -2;
    if ((*vMat)[0].rows() != size[1] && (*vMat)[0].cols() != size[0]) return -3;
    if ((*vMat)[1].rows() != size[1] && (*vMat)[1].cols() != size[0]) return -4;
    if ((*vMat)[2].rows() != size[1] && (*vMat)[2].cols() != size[0]) return -5;

    Matrix mat = ((Eigen::Map<RMMatrix_UChar>(reinterpret_cast<unsigned char*>((*aInput)->GetBufferPointer()),h,3)).template cast<T>()).transpose();
    // Reshape the data
    (*vMat)[0] = Eigen::Map<Matrix>(mat.row(0).data(),size[1],size[0]);
    (*vMat)[1] = Eigen::Map<Matrix>(mat.row(1).data(),size[1],size[0]);
    (*vMat)[2] = Eigen::Map<Matrix>(mat.row(2).data(),size[1],size[0]);
    return 1;
  }//end of function

  /*! \brief VectorOfMatrices2Image
   *  This function converts a vector of Eigen matrices to an ITK RGB image
   *  @_Mat	: The 3-column eigen matrix of template T
   *  @_aInput	: The rgb image that needs to be made to an eigen matrix
   */
  template <class T>
  int VectorOfMatrices2Image(void* _Mat, void* _aInput)
  {
    typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;
    typedef std::vector<Matrix> vecMat;
    typedef spin::RGBImageType RGBImageType;
    typename RGBImageType::Pointer* aInput   = static_cast<typename RGBImageType::Pointer*>(_aInput);
    if (!aInput) return -1;
    vecMat* vMat = static_cast<vecMat*>(_Mat);
    if (vMat->size() <0) return -2;

    // Get the dimensions of the image
    spin::RGBImageType::SizeType size = (*aInput)->GetLargestPossibleRegion().GetSize();
    Matrix mat  = Matrix::Zero(3,size[0]*size[1]);
    // Reshape the data
    mat.row(0) = Eigen::Map<Matrix>((*vMat)[0].data(),1,size[1]*size[0]);
    mat.row(1) = Eigen::Map<Matrix>((*vMat)[1].data(),1,size[1]*size[0]);
    mat.row(2) = Eigen::Map<Matrix>((*vMat)[2].data(),1,size[1]*size[0]);
    RMMatrix_UChar matUChar = mat.transpose().template cast<unsigned char>();
    mat.resize(0,0);
    // Allocate the matrix with a dummy pixel value
    spin::RGBPixelType pix;
    pix.SetRed(0);pix.SetGreen(0);pix.SetBlue(0);
    (*aInput)->FillBuffer(pix);
    // And copy the data back to an image
    memcpy((*aInput)->GetBufferPointer(), matUChar.data(), 3 * size[0] * size[1]);
    matUChar.resize(0,0);
    return 1;
  }//end of function

  template int Image2Matrix<float>(void*, void*);
  template int Image2Matrix<double>(void*, void*);
  template int Matrix2Image<float>(void*, void*);
  template int Matrix2Image<double>(void*, void*);

  template int Image2VectorOfMatrices<float>(void*, void*);
  template int Image2VectorOfMatrices<double>(void*, void*);
  template int VectorOfMatrices2Image<float>(void*, void*);
  template int VectorOfMatrices2Image<double>(void*, void*);
}//end of namespace
