#ifndef ITKIMAGEUTILS_H_
#define ITKIMAGEUTILS_H_

namespace spin
{
  /*! \brief API to normalize a RGB image with a calibration image*/
  void NormalizeRGBImageWithCalImage(void* _aInput, void* _bRef, void* _mMax, void* _cOutput);

  /*! \brief API to cmpute min/max of a RGB image */
  void MinMaxRGBImage(void* _aInput, void* _mMin, void* _mMax);

  /*! \brief API to fuse information */
  void FuseInformation(void* _aInput, void* _bInput, float threshold);

  /*! \brief API to normalizer a RGB image with a calibration image */
  void NormalizeRGBImageWithCalImage(void* _aInput, void* _bRef, void* _cOutput);

  /*! \brief API to convert RGB image to matrix */
  template <class T>
  int Image2Matrix(void* _aInput, void* _aMat);

  /*! \brief API to convert Matrix to RGB image */
  template <class T>
  int Matrix2Image(void* _aMat, void* _aInput);

  /*! \brief API to convert RGB image to vector of matrices */
  template <class T>
  int Image2VectorOfMatrices(void* _aInput, void* _Mat);

  /*! \brief API to convert vector of matrices to RGB Image */
  template <class T>
  int VectorOfMatrices2Image(void* _aInput, void* _Mat);
}
#endif
