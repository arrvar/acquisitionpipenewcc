#ifndef OPENCVUNSHARPMASK_H_
#define OPENCVUNSHARPMASK_H_

namespace spin
{
  class OpenCVUnsharpMask 
  {
    public:
      // Default constructor
      OpenCVUnsharpMask(){}
      //Default destructor
      ~OpenCVUnsharpMask(){}
      //Compute
      int Compute(void* _imgIn, float sigma, float strength);
      // Compute
      int Compute(void* _imgIn, void* _imgOut, float sigma, float strength);
  };//end of class
}//end of namespace
#endif
