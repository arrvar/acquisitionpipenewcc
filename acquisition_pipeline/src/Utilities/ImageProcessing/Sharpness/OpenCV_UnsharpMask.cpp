#include "Utilities/ImageProcessing/Sharpness/OpenCV_UnsharpMask.h"
#include "AbstractClasses/AbstractImageTypes.h"

namespace spin
{
  // A pipeline to implement Unsharp Masking using in-built functions provided
  // by opencv as explained here
  // https://www.idtools.com.au/unsharp-masking-python-opencv/
  int OpenCVUnsharpMask::Compute(void* _imgIn, float sigma, float strength)
  {
    // typecast the image to an RGB image
    RGBImageType::Pointer* itkImage = static_cast<RGBImageType::Pointer*>(_imgIn);
    if (itkImage == nullptr) return -1;
    
    // Get the dimensions
    RGBImageType::SizeType size = (*itkImage)->GetLargestPossibleRegion().GetSize();
    
    // Allocate memory for an opencv mat object
    cv::Mat inputImage(size[1], size[0], CV_8UC3);
    
    // Copy the data to this buffer
    memcpy(inputImage.data, (*itkImage)->GetBufferPointer(),3*inputImage.rows*inputImage.cols);

    // We need to process each channel separately
    cv::Mat convMat;
    inputImage.convertTo(convMat,CV_32FC3); 
    // Split the channels 
    cv::Mat channels[3];
    cv::split(convMat, channels);
    cv::Mat dummy;
    std::vector<cv::Mat> dump(3,dummy);
    int kernel = static_cast<int>(sigma);

    for (int i = 0; i < 3; ++i)
    {
      cv::Mat temp;
      // Median filtering
      cv::medianBlur(channels[i],temp,kernel);
      // Compute Laplacian
      cv::Laplacian(temp,temp,CV_32FC1);
      // Do the unsharp masking
      temp = channels[i]-strength*temp;
      // Saturate the pixels in either direction 
      //https://stackoverflow.com/questions/33750824/best-way-to-indexing-a-matrix-in-opencv
      for (int r = 0; r < temp.rows; ++r)
      {
        float* pA = temp.ptr<float>(r);
        for (int c =0; c < temp.cols; ++c)
        {
          if (pA[c] > 255.0) pA[c] = 255.0;
          else 
          if (pA[c] < 0) pA[c] = 0;
        }
      }
      dump[i] = temp;
    }//finished enhancing every channel 
    
    // Merge channels
    cv::merge(dump,convMat);
    // convert channels to CV_8UC3
    cv::Mat outImg;
    convMat.convertTo(outImg,CV_8UC3);
    //cv::cvtColor(outImg, inputImage, cv::COLOR_RGB2BGR);
    // and copy it back to the input image
    memcpy((*itkImage)->GetBufferPointer(),outImg.data,3*inputImage.rows*inputImage.cols);
    
    channels[0].release();
    channels[1].release();
    channels[2].release();
    dump[0].release();
    dump[1].release();
    dump[2].release();
    dump.clear();
    inputImage.release();
    convMat.release();
    outImg.release();
    return 1;
  }//end of function

  // A pipeline to implement Unsharp Masking using in-built functions provided
  // by opencv as explained here
  // https://www.idtools.com.au/unsharp-masking-python-opencv/
  // This API is to be used primarily with Pyramid Generation
  int OpenCVUnsharpMask::Compute(void* _imgIn, void* _imgOut, float sigma, float strength)
  {
    cv::Mat* inputImage = static_cast<cv::Mat*>(_imgIn);
    cv::Mat* outputImage = static_cast<cv::Mat*>(_imgOut);

    // We need to process each channel separately
    cv::Mat convMat;
    inputImage->convertTo(convMat,CV_32FC3);
    // Split the channels
    cv::Mat channels[3];
    cv::split(convMat, channels);
    cv::Mat dummy;
    std::vector<cv::Mat> dump(3,dummy);
    int kernel = static_cast<int>(sigma);

    for (int i = 0; i < 3; ++i)
    {
      cv::Mat temp;
      // Median filtering
      cv::medianBlur(channels[i],temp,kernel);
      // Compute Laplacian
      cv::Laplacian(temp,temp,CV_32FC1);
      // Do the unsharp masking
      temp = channels[i]-strength*temp;
      // Saturate the pixels in either direction
      //https://stackoverflow.com/questions/33750824/best-way-to-indexing-a-matrix-in-opencv
      for (int r = 0; r < temp.rows; ++r)
      {
        float* pA = temp.ptr<float>(r);
        for (int c =0; c < temp.cols; ++c)
        {
          if (pA[c] > 255.0) pA[c] = 255.0;
          else
          if (pA[c] < 0) pA[c] = 0;
        }
      }
      dump[i] = temp;
    }//finished enhancing every channel

    // Merge channels
    cv::merge(dump,convMat);
    // convert channels to CV_8UC3
    convMat.convertTo(*outputImage,CV_8UC3);

    channels[0].release();
    channels[1].release();
    channels[2].release();
    dump[0].release();
    dump[1].release();
    dump[2].release();
    dump.clear();
    convMat.release();

    return 1;
  }//end of function
}//end of namespace
