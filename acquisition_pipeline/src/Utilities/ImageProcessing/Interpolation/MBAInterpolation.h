#ifndef MBAINTERPOLATION_H_
#define MBAINTERPOLATION_H_

namespace spin
{
  class MBAInterpolation 
  {
    public:
      MBAInterpolation(){}
      ~MBAInterpolation(){}
      /*! \brief The main driver */
      void Compute(void* data, int numLevels);
      /*! \brief The main driver */
      void Compute(float* data, int height, int width, int numLevels);
  };//end of function
}//end of namespace
#endif
