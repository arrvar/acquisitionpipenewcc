#include "Utilities/ImageProcessing/Interpolation/MBAInterpolation.h"
#include <vector>
#include <memory>
#include <utility>
#include <cfloat>
#include <cmath>
#include <mba/mba.hpp>
#ifdef USE_OPENMP
#include <omp.h>
#endif
namespace spin
{
  /*! \brief A function create a point array */
  template <typename T>
  static inline std::array<T, 2> make_array2(T x,  T y)
  {
    std::array<T, 2> p = {{x, y}};
    return p;
  }//end of function

  /*! \brief Interpolation
  *  This function implements the 2D Multilevel B-spline interpolation
  *  process. The following assumptions are made when building the values:
  *  1.) The data buffer has been allocated
  *  2.) Control points in the buffer are positive values while locations to be
  *      interpolated are marked with a negative value
  *  3.) The max value in the buffer is also known. This value is used to scale
  *      the control points values (and subsequent interpolated values) so that
  *      they lie between 0-1
  *  @pImage      : The buffer that is sparsely populated
  *  @h_Rows      : The height of the buffer
  *  @w_Cols      : The width of the buffer
  *  @mMax        : The maximum value amongst the control points
  *  @nzLev       : The levels of B-spline interpolation needed. More levels
  *                 will lead to sharper images     
  */
  void Interpolation(float* pImage, size_t h_Rows, size_t w_Cols, float mMax)
  {
    // Create two arrays
    std::vector< std::array<double,2> > p; 
    std::vector<double> v;
    std::vector< std::pair<double,double> > iPoints; 
    std::vector<long> iIndex;
    
    // Some constants
    long v1 = w_Cols * h_Rows;
    
    // Fill up the output matrix, as well as the locations of the control points
    for (size_t jCols = 0; jCols < w_Cols; ++jCols)
    for (size_t iRows = 0; iRows < h_Rows; ++iRows)
    {
      // Get the current index
      long idx = iRows + h_Rows * jCols;
      // Get the value at this index
      double pVal = (double)pImage[idx];
      // Check if this value is a valid control point
      if (pVal > 0)
      {
        // This is avalid control point location. We push this value into a vector
        p.push_back(make_array2<double>((double)iRows/(double)h_Rows,(double)jCols/(double)w_Cols));
        // The value at this control point (scaled to lie between 0-1)
        v.push_back(pVal/mMax);
      }
      else
      {
        // Push the y,x coordinates 
        std::pair<double,double> d= std::make_pair((double)iRows/(double)h_Rows,(double)jCols/(double)w_Cols);
        iPoints->push_back(d);
        // And the 1D index
        iIndex.push_back(idx);
      }
    }
    
    // The interpolating functions from the control points are built here
    typedef mba::cloud<2> mba_type; // double by default
    std::shared_ptr< mba_type > iMBA = std::make_shared<mba_type>(make_array2<double>(-0.01, -0.01),
                                                                  make_array2<double>(1.01, 1.01),
                                                                  p, v, make_array2<size_t>(2, 2), 
                                                                  nzLev);
    // clear p and v
    p.clear();
    v.clear();
    
    // Interpolate the missing values here
    #ifdef USE_OPENMP
      #pragma omp parallel for
    #endif
    for (long i = 0; i < iPoints.size(); ++i)
    {
      double g = std::floor((*(iMBA.get())(iPoints[i].first, iPoints[i].second))*mMax+0.5);
      pImage[iIndex[i]] = static_cast<float>(g);
    }   
    
    // clear iPOints and iIndex
    iPoints.clear();
    iIndex.clear();
  }//end of function
  
  /*! \brief Compute
   *  This is the interface to implement a MBA interpolation on a sparsely populated
   *  dataset.
   */
  void MBAInterpolation::Compute(void* data, int numLevels)
  {
    // typecast the data
    RMMatrix_Float* pImg = static_cast<RMMatrix_Float*>(data);
    // Get the dimensions
    size_t hRows = (size_t)pImg->rows();
    size_t wCols = (size_t)pImg->cols();
    // Get the max value
    float pMax = pImg->array().maxCoeff();
    // Implement the interpolation routine
    Interpolation(pImg.data(),hRows,wCols,pMax);
  }//end of function
  
  /*! \brief Compute
   *  This is the interface to implement a MBA interpolation on a sparsely populated
   *  dataset.
   */
  void MBAInterpolation::Compute(float* data, int height, int width, int numLevels)
  {
    // Get the dimensions
    size_t hRows = (size_t)height;
    size_t wCols = (size_t)width;
    // Get the max value
    float pMax = -1;
    for (size_t h = 0; h < hRows*wCols; ++h)
    {
      if (data[h] > pMax) pMax = data[h];
    }
    // Implement the interpolation routine
    Interpolation(data,hRows,wCols,pMax);
  }//end of function
}//end of namespace