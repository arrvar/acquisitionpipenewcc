#include "Utilities/Geometry/PolygonUtils.h"
#include "Utilities/Geometry/PolygonObject.h"
#include "Utilities/Geometry/PointCheck.h"
//#include "Utilities/Geometry/jb/polygon_properties.hpp"
#include <boost/filesystem.hpp>
#include <fstream>

namespace spin
{
  namespace
  {
    typedef PolygonObject::Polygon Polygon;
  }

  //http://alienryderflex.com/polygon/
  bool pointInPolygon(RMMatrix_Float* polygon, std::pair<double,double>* p)
  {
    int polyCorners = polygon->rows()-1;
    int   i, j=polyCorners-1 ;
    bool  oddNodes=false      ;

    float y = (float)(p->second);
    float x = (float)(p->first);
    for (i=0; i<polyCorners; i++)
    {
      if ( ((*polygon)(i,1) < y) && ((*polygon)(j,1) >=y) ||
           ((*polygon)(j,1) < y) && ((*polygon)(i,1) >=y))
      {
        if ( (*polygon)(i,0)+\
             (y-(*polygon)(i,1))/( (*polygon)(j,1) - (*polygon)(i,1)) * ( (*polygon)(j,0) - (*polygon)(i,0) ) < x)
        {
          oddNodes =!oddNodes;
        }
      }
      j=i;
    }

    return oddNodes;
  }

  /*! \brief Initialize
   *  This function is used to initialize Polygon utils
   */
  int PolygonUtils::Initialize()
  {
    pobject = std::make_shared<PolygonObject>();
    return 1;
  }//end of function
  
  /*! \brief AddPolygon
   *  This function is used to add a polygon and an index. The input is a vector
   *  of std::vector< std::pair<float, float> >. Typically, this should be done
   *  only once.
   */
  int PolygonUtils::AddPolygon(int index, void* _poly)
  {
    Polygon* poly = static_cast<Polygon*>(_poly);
    if (poly == nullptr) return -1;
    
    if (pobject == nullptr) return -2;
    
    // Replace the existing index with the desired polygon
    pobject->pmap[index] = *poly;
    pobject->pcentroid[index] = (pobject->pmap[index].colwise().sum()).array()/(pobject->pmap[index].rows());

    return 1;
  }//end of function
  
  
  /*! \brief GetPolygon
   *  This function is used to get vertices of a polygon as an eigen matrix
   */
  int PolygonUtils::GetPolygon(int index, void* _poly)
  {
    Polygon* poly = static_cast<Polygon*>(_poly);
    if (poly == nullptr) return -1;
    
    if (pobject == nullptr) return -2;
    
    // Check if the polygon is present 
    std::map<int, Polygon>::iterator it = pobject->pmap.find(index);
    if (it == pobject->pmap.end()) return -3;
    
    *poly = pobject->pmap[index];
    
    return 1;
  }//end of function 
   
  /*! \brief ResizePolygon
   *  This function is used to resize a polygon by a factor f
   *  //https://gis.stackexchange.com/questions/61786/how-to-scale-reduce-my-polygon-without-changing-the-central-lat-long
   */
  int PolygonUtils::ResizePolygon(int index, float f)
  {   
    if (pobject == nullptr) return -1;
    
    // Check if the polygon is present 
    std::map<int, Polygon>::iterator it = pobject->pmap.find(index);
    if (it == pobject->pmap.end()) return -2;   
    
    // Get the centroid of the polygon. 
    pobject->pcentroid[index] = (pobject->pmap[index].colwise().sum()).array()/(pobject->pmap[index].rows());
    
    // The scale factor is 1+f
    float sF = 1.0+f;
    
    RMMatrix_Float pModified = pobject->pmap[index].array() * sF;
    RMMatrix_Float pOnes = RMMatrix_Float::Ones(pModified.rows(),1);
    // And replace the polygon 
    pobject->pmap[index] = pModified - pOnes*((pobject->pcentroid[index].array()*f).matrix());

    /*int n = pobject->pmap[index].rows();
    double* input = new double[2*n];
    double* output= new double[2*n];

    for (int g = 0; g < n; g++)
    {
      input[2*g] = pobject->pmap[index](g,0);
      input[2*g+1] = pobject->pmap[index](g,1);
    }
    //expand the polygon
    output = spin::jb::polygon_expand ( n, input, (double)f );

    for (int g = 0; g < n; g++)
    {
      pobject->pmap[index](g,0) = output[2*g];
      pobject->pmap[index](g,1) = output[2*g+1];
    }

    delete []input;
    delete []output;
    */
    return 1;
  }//end of function
  
  /*! \brief CheckPointInsidePolygon
   *  This function checks if a given point is within a polygon. It is assumed
   *  that prior to calling this function, the polygon should have been 
   *  populated.
   *  @index              : The polygon that is used for checking the point 
   *  @_point             : The point that needs to be checked.
   */
  int PolygonUtils::CheckPointInsidePolygon(int index, void* _point, bool* h)  
  {
    if (pobject == nullptr) return -1;
    
    // Check if the polygon is present 
    std::map<int, Polygon>::iterator it = pobject->pmap.find(index);
    if (it == pobject->pmap.end()) return -2;   
    
    // by default, point is not within the polygon
    *h = false;
    
    // Now, Check if point is present in the polygon 
    //PointCheck p;
    //*h = p.isInside(&(pobject->pmap[index]),_point);
    std::pair<double, double>* point = static_cast< std::pair<double, double>* > (_point);
    *h = pointInPolygon(&(pobject->pmap[index]), point);
    return 1;
  }//end of function

  /*! \brief SaveModel
   *  This function serializes a model to a file
   */
  int PolygonUtils::SaveModel(std::string& fName)
  {
    std::ofstream ofs(fName.c_str());
    boost::archive::binary_oarchive oa(ofs);
    // write class instance to archive
    oa << (*(pobject.get()));

    return 1;
  }//end of function

  /*! \brief LoadModel
   *  This function serializes a model to a file
   */
  int PolygonUtils::LoadModel(std::string& fName)
  {
    // Run an initialization
    Initialize();

    boost::filesystem::path dir(fName.c_str());
    if (!boost::filesystem::exists(dir))
    {
      // Model file is invalid. Return to the calling function
      return -1;
    }
    // create and open an archive for input
    std::ifstream ifs(fName.c_str());
    boost::archive::binary_iarchive ia(ifs);
    // read class state from archive
    ia >>(*(pobject.get()));

    return 1;
  }//end of function

  /*! \brief GetCentroid
   *  This function gets the centroid of a specified safe operating zone
   */
  int PolygonUtils::GetCentroid(int index, void* _centroid)
  {
    RMMatrix_Float* centroid = static_cast<RMMatrix_Float*>(_centroid);
    if (centroid == nullptr) return -1;

    if (pobject == nullptr) return -2;

    // Check if the polygon is present
    std::map<int, RMMatrix_Float>::iterator it = pobject->pcentroid.find(index);
    if (it == pobject->pcentroid.end()) return -3;

    *centroid = pobject->pcentroid[index];
    return 1;
  }

  /*! \brief GetClosestPoint
   *  This function gets the closest point on the boundary of a polygon, given
   *  a point that lies outside the polygon. We use the squared Euclidean
   *  distance as a measure
   */
  int PolygonUtils::GetClosestPoint(int index, void* _point)
  {
    RMMatrix_Float* point = static_cast<RMMatrix_Float*>(_point);
    if (point == nullptr) return -1;

    if (pobject == nullptr) return -2;

    // Check if the polygon is present
    std::map<int, RMMatrix_Float>::iterator it = pobject->pmap.find(index);
    if (it == pobject->pmap.end()) return -3;

    RMMatrix_Float p = RMMatrix_Float::Ones(it->second.rows(),it->second.cols());
    RMMatrix_Float g = (it->second - p * (*point)).array().square().matrix().rowwise().sum();

    // Get the minimum index location
    int idx = 0;
    for (int h = 1; h < g.rows(); ++h)
    {
      if (g(h,0) < g(idx,0)) idx = h;
    }

    // Now, replace the index
    *point = pobject->pmap[index].row(idx);

    return 1;

  }//end of function
}//end of namespace
