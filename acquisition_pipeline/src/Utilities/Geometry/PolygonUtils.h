#ifndef POLYGONUTILS_H_
#define POLYGONUTILS_H_
#include <memory>

namespace spin
{
  class PolygonObject;
  enum PolygonSOZ
  {
    ACROSS_ROWS=0,
    ACROSS_COLS
  };//end of enum
  
  class PolygonUtils
  {
    public:
      PolygonUtils(){}
      ~PolygonUtils(){}
      
      int Initialize();

      int AddPolygon(int index, void* _poly);
      
      int GetPolygon(int index, void* _poly);
      
      int ResizePolygon(int index, float f);
      
      int CheckPointInsidePolygon(int index, void* _point, bool* h);  

      int SaveModel(std::string& fName);

      int LoadModel(std::string& fName);

      int GetCentroid(int index, void* _centroid);

      int GetClosestPoint(int index, void* _point);
     private:
      std::shared_ptr<PolygonObject> pobject;
  };//end of class 
}//end of namespace
#endif

