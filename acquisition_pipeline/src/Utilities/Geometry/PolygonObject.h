#ifndef POLYGONOBJECT_H_
#define POLYGONOBJECT_H_
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <memory>
#include <map>
#include <vector>
#include <utility>

namespace spin 
{
  class PolygonObject 
  {
    public:
      typedef RMMatrix_Float Polygon;
      typedef std::map<int, Polygon> PolygonMap;
      typedef std::map<int, RMMatrix_Float> CentroidMap;
      PolygonMap pmap;
      CentroidMap pcentroid;

      // Default constructor
      PolygonObject(){}
    
      // Default destructor   
      ~PolygonObject()
      {
        PolygonMap::iterator it = pmap.begin();
        while (it != pmap.end())
        {
          it->second.resize(0,0);
          ++it;
        }
        pmap.clear();

        CentroidMap::iterator it1 = pcentroid.begin();
        while (it1 != pcentroid.end())
        {
          it1->second.resize(0,0);
          ++it1;
        }
        pcentroid.clear();
      }//end of function 
    
    private:
      friend class boost::serialization::access;   
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version)
      {
        ar & pmap;
        ar & pcentroid;
      }//end of class
      

  };//end of class
}//end of namespace
#endif
