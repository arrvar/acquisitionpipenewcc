#include "AbstractClasses/DRObject.h"
#include "AbstractClasses/DRParams.h"
#include "AbstractClasses/AbstractDimensionalityReduction.h"
#include "Utilities/MatrixUtils/EigenUtils.h"
#include "Utilities/MatrixUtils/RandomizedSVD.h"
#include <iostream>

namespace spin
{
  /*! \brief Constructor (during training) */
  template <class T>
  AbstractDimensionalityReduction<T>::AbstractDimensionalityReduction(void* params, bool s)
  {
    // Are we initializing this during training or testing?
    if (s)
    {
      // This is during training
      // typecast the parameter object
      DRParams* dparams = static_cast<DRParams*>(params);
      if (!dparams) return;
      try
      {
        // Initialize drObj
        drObj = std::make_shared< DRObject<T> >(dparams);
      }
      catch(...)
      {
      }

      if (dparams->kernel != "")
      {
        // This method requires a randomized SVD
        rsvd = std::make_shared< RandomizedSVD<T> >(dparams->samples, dparams->rank, dparams->power);
      }
    }
    else
    {
      // This is during testing
      DRObject<T>* dObj = static_cast<DRObject<T>*>(params);
      if (!dObj) return;

      try
      {
        // Initialize drObj
        drObj = std::make_shared< DRObject<T> >(dObj);
      }
      catch(...)
      {
      }
    }
  }//end of function

  /*! \brief ZeroCenterData
   *  This function is used to compute the mean of all features across rows and store it
   *  in drObj. Once the mean is computed, this value is subtracted from every element
   *  in the matrix
   */
  template <class T>
  void AbstractDimensionalityReduction<T>::ZeroCenterData(T* m_InputData)
  {
    // Get the dimensions of the data
    int numFeatures = m_InputData->rows();
    int numDims     = m_InputData->cols();

    typedef typename T::Scalar Scalar;

    // Allocate a matrix of Ones
    T Ones = T::Ones(1,numFeatures);

    // Compute the mean of all features and store it in mean
    drObj.get()->mean = (Ones* (*m_InputData)) * static_cast<Scalar>(1.0)/static_cast<Scalar>(numFeatures);

    // Finally, subtract the mean feature from rows in data. Store the result in zcData
    drObj.get()->zcData = (*m_InputData) - Ones.transpose() * drObj.get()->mean;
  }//end of function

  /*! \brief Normalize Data
   *  This function is used to first make the input data non-negative and then
   *  normalize by the sum of all components in the feature (essentailly a crude approximation to
   *  the pdf)
   */
  template <class T>
  void AbstractDimensionalityReduction<T>::NormalizeData(T* m_InputData, bool previous)
  {
    typedef typename T::Scalar Scalar;

    if (!previous)
    {
      // Compute the max and min values of the data
      drObj.get()->maxVal = m_InputData->maxCoeff();
      drObj.get()->minVal = m_InputData->minCoeff();
    }

    Scalar factor = static_cast<Scalar>(drObj.get()->maxVal - drObj.get()->minVal);
    // Normalize the data to lie between 0-1
    drObj.get()->zcData = ((m_InputData->array() - static_cast<Scalar>(drObj.get()->minVal)).array()/factor).matrix();
  }//end of function

  // Explicit instantiations
  template class AbstractDimensionalityReduction<RMMatrix_Float>;
  template class AbstractDimensionalityReduction<RMMatrix_Double>;
  template class AbstractDimensionalityReduction<CMMatrix_Float>;
  template class AbstractDimensionalityReduction<CMMatrix_Double>;
}//end of namespace
