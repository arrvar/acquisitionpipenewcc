#include "AbstractClasses/DRObject.h"
#include "AbstractClasses/AbstractDimensionalityReduction.h"
#include <iostream>
namespace spin
{
  namespace pca
  {
    template <class T>
    class PCA : public AbstractDimensionalityReduction<T>
    {
      public:
        /*! \brief Default constructor (training)*/
        PCA(void* p, bool f);

        /*! \brief Default destructor */
        ~PCA(){}

        /*! \brief ComputeProjectionMatrix */
        int ComputeProjectionMatrix(T* m, bool c = false);

        /*! \brief InitializeKernel */
        int InitializeKernel(bool t = true)
        {
          // Nothing to do here
          return 1;
        }

        /*! \brief ProjectNewData */
        int ProjectNewData(T* m, T* n);
    };//end of class

    /*! \brief Constructor (during training phase)
    *  @params      : The parameters associated with PCA
    */
    template <class T>
    PCA<T>::PCA(void* params, bool f)
            :spin::AbstractDimensionalityReduction<T>(params, f)
    {
      // populate some meta information
      this->drObj.get()->algorithm="pca";
    }//end of function

    /*! \brief ComputeProjectionMatrix
     *  This function is the driver for implementing standard PCA
     *  @m_InputData      : The input data whose PCA is sought
     *  @computeprojection: If a projection of the input data is sought
     */
    template <class T>
    int PCA<T>::ComputeProjectionMatrix( T* m_InputData, bool computeprojection )
    {
      typedef typename T::Scalar Scalar;
      // Do some sanity checks
      if (m_InputData->data() == NULL) return -1;
      // Check if requested dimension is correct
      if ( (m_InputData->rows() < m_InputData->cols()) && this->drObj.get()->numEigenvals > m_InputData->rows()) return -2;
      if ( (m_InputData->cols() < m_InputData->rows()) && this->drObj.get()->numEigenvals > m_InputData->cols()) return -3;

      // 1.) Zero center the data
      this->ZeroCenterData(m_InputData);

      // 2.) Build the covariance matrix
      T A = (this->drObj.get()->zcData.transpose() * this->drObj.get()->zcData)*(1.0/this->drObj.get()->zcData.rows());

      // 4.) Run a SVD on the data
      Eigen::JacobiSVD<RMMatrix_Double> svd(A.template cast<double>(), Eigen::ComputeThinU|Eigen::ComputeThinV);

      // 5.) Select the first "numEigenVals" eigenvectors
      this->drObj.get()->projectionMatrix = \
      (svd.matrixU().block(0, 0, svd.matrixU().rows(), this->drObj.get()->numEigenvals)).template cast<Scalar>();

      // 6.) Check if the user has requested projecting the original data
      if (computeprojection)
      {
        this->drObj.get()->projection = this->drObj.get()->zcData * this->drObj.get()->projectionMatrix;
      }

      return 1;
    }//end of function

    /*! \brief ProjectNewData
     *  This function is the driver for transforming new data using the projection matrix and mean
     *  obtained during the training phase.
     *  @m_InputData      : The input data that needs to be transformed
     *  @m_ProjectedData  : The output projected data;
     */
    template <class T>
    int PCA<T>::ProjectNewData( T* m_InputData, T* m_ProjectedData )
    {
      // do some sanity check
      if (this->drObj.get()->projectionMatrix.data() == NULL) return -1;

      if (this->drObj.get()->mean.data() == NULL ) return -2;

      typedef typename T::Scalar Scalar;

      // Allocate a matrix of Ones
      T Ones = T::Ones(m_InputData->rows(),1);

      //Subtract the mean from the data and apply the projection matrix
      (*m_ProjectedData)  = ((*m_InputData - Ones * this->drObj.get()->mean) * this->drObj.get()->projectionMatrix).template cast<Scalar>();

      return 1;
    }//end of function

    // Explicit instantiations
    template class PCA<RMMatrix_Float>;
    template class PCA<RMMatrix_Double>;
    template class PCA<CMMatrix_Float>;
    template class PCA<CMMatrix_Double>;
  }//end of namespace pca
}//end of namespace

typedef spin::pca::PCA<spin::RMMatrix_Float>   PCAFloat_Row;
typedef spin::pca::PCA<spin::RMMatrix_Double>  PCADouble_Row;
typedef spin::pca::PCA<spin::CMMatrix_Float>   PCAFloat_Col;
typedef spin::pca::PCA<spin::CMMatrix_Double>  PCADouble_Col;

//Interface for a plugin. Sadly we have to enumerate all the
//types as C does not recognize templates
extern "C" void* DR_Plugin(int* g, void* params, bool* f)
{
  // return the object type
  void* q = NULL;
  if (*g == 1) // RMMatrix_Float
  {
    PCAFloat_Row* x = new PCAFloat_Row(params,*f);
    q = (void*)(x);
  }
  else
  if (*g == 2)
  {
    PCADouble_Row* x = new PCADouble_Row(params,*f);
    q = (void*)(x);
  }
  if (*g == 3) // CMMatrix_Float
  {
    PCAFloat_Col* x = new PCAFloat_Col(params,*f);
    q = (void*)(x);
  }
  else
  if (*g == 4)
  {
    PCADouble_Col* x = new PCADouble_Col(params,*f);
    q = (void*)(x);
  }

  return q;
}//end of function

