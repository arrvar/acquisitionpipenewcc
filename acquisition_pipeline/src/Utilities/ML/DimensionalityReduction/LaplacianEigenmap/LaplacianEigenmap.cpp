#include "AbstractClasses/DRObject.h"
#include "AbstractClasses/AbstractDimensionalityReduction.h"
#include "Utilities/MatrixUtils/RandomizedSVD.h"
#include "Utilities/ML/DimensionalityReduction/LaplacianEigenmap/AbstractKernelLaplacianEigenmap.h"
#include <dlibxx.hxx>

namespace spin
{
  namespace LE
  {
    /*! \brief Laplacian Eigenmaps is a well known technique in non-linear dimensionality
     *  reduction and falls under the general class of spectral dimensionality reduction algorithms.
     *  Given a set of data points X_i in a high dimensional space, the algorithm generates a projection
     *  matrix to map the current (as well as future) data into a lower dimensional space Y_i by
     *  solving the generalized Eigenvalue problem
     *  L.y = \lambda D.y
     *  where,
     *  L is the unnormalized Laplacian matrix (L = D - W),
     *  D is a diagonal computed by summing the rows (or columns) of the affinity matrix W
     *  As can be seen, the naive way of implementing this process will result in computation of a
     *  prohibitive Gram-matrix W and subsequent solving of an eigenvalue decomposition.
     *  Furthermore, the natrual method to extract the eigenvectors would be to use a dedicated
     *  eigensolver (like ARPACK) which again makes the process computatinally expensive as we
     *  would have to extract the k-smallest eigenvectors.
     *  In this implementation, we use a modified version of the equation that makes computation of
     *  eigenvectors a tractable.
     *  An alternate formulation of the Laplacian was given by
     *  Ng. et al., in the Following paper:
     *  Section 2: "On spectral custering: Analysis and an algorithm".
     *  The first change that was effected was to reduce the generalized eigenvalue problem into a standard
     *  eigenvalue problem.
     *  D^-1 * L * y = \lambda y
     *  which can be further reduced to
     *  (I - D^(-1/2)*W*D^(-1/2))
     *  As pointed by the authors in the paper, eigenvectors are computed for
     *  D^(-1/2)*W*D^(-1/2), rather than the above equation as doing so
     *  "...only changes the eigenvalues from \lambda_i to 1-\lambda_i) and not the eigenvectors..."
     *  This change helps us reduce the problem to computing the k-largest eigenvalues rather the smallest.
     *  However, at first sight it still does not allow us to make the process efficient as we have to compute the
     *  affinity matrix W.
     *  In this class we provide an abstraction on a set of kernels that can be used to achieve the purpose
     *  of eigenvector computation without actually building the high dimensional kernel matrix! Details of
     *  each kernel will be described in corresponding files.
     *  We resort to a low-rank decomposition as discussed in the following paper:
     *  N.Halko, P.G.Martinsson and J.A.Tropp, "Finding Structure with Randomness:
     *  Stochastic algorithms for constructing approximate matrix decompositions".
     *  It has been noted by the authors in the aforementioned paper that increasing the power of the matrix
     *  improves the accuracy of eigenvalues. Hence, if
     *  A = D^(-1/2)*W*D^(-1/2), then, we compute the eigenvectors of A^2.
     *  Refer to the following presentation:
     *  https://amath.colorado.edu/faculty/martinss/Talks/2010_mmds_martinsson.pdf
     *  This presentation is hereby refered to as RSVD;
     *  In order to facilitate use of various kernels, we implement the following in a plug and play framework.
     *
     *  For projecting out-of-sample data, we use the algorithm described in
     *  "Incremental laplacian eigenmaps by preserving adjacent information between data points",
     *  More specifically, we use the method described in Method 3.2.1
     */

    /*! \brief LaplacianEigenmap
     *  This class is an abstraction of a specific implementation of LaplacianEigenmap.
     */
    template <class T>
    class LaplacianEigenmap : public spin::AbstractDimensionalityReduction<T>
    {
      public:
        typedef AbstractKernelLaplacianEigenmap<T> KernelType;

        /*! \brief Default constructor (training)*/
        LaplacianEigenmap(void* p, bool f);

        /*! \brief Default destructor */
        ~LaplacianEigenmap()
        {
          lib.get()->close();
        }

        /*! \brief ComputeProjectionMatrix */
        int ComputeProjectionMatrix(T* m, bool c = false);

        /*! \brief InitializeKernel */
        int InitializeKernel(bool t = true);

        /*! \brief ProjectNewData */
        int ProjectNewData(T* m, T* n);
      private:
        std::shared_ptr<dlibxx::handle> lib;
        std::shared_ptr< KernelType > pKernel;
        bool initialized = false;
    };//end of class

    /*! \brief Constructor (during training phase)
     *  @params      : The parameters associated with LaplacianEigenmap
     */
    template <class T>
    LaplacianEigenmap<T>::LaplacianEigenmap(void* params, bool f)
                        : spin::AbstractDimensionalityReduction<T>(params, f)
    {
    }//end of function

    /*! \brief InitializeKernel
     *  This function initializes the kernel that will be used for
     *  @m_InputData      : The input data for which a regression needs to be performed
     *  @m_RegressedData  : The regressed output data
     */
    template <class T>
    int LaplacianEigenmap<T>::InitializeKernel(bool training)
    {
      if (!initialized)
      {
        // Build the name of the Kernel
        std::string libname = "spinDR_LE_"+this->drObj.get()->kernel+"_kernel.so";

        // Instantiate the loader
        lib = std::make_shared<dlibxx::handle>();
        // Resolve symbols when they are referenced instead of on load.
        lib.get()->resolve_policy(dlibxx::resolve::lazy);

        // Load the library specified.
        lib.get()->load(libname);

        // Check if the library could be loaded
        if (!lib.get()->loaded()) return -1;

        // Attempt to create a plugin instance using the "create_kernel"
        // factory method from the dynamic library.
        pKernel = lib.get()->template create< KernelType >("LE_Kernel", this->drObj.get()->type);

        // Check if the kernel module was loaded
        if (!pKernel.get()) return -2;

        int v = 1;
        if (training)
        {
          // Initialize the kernel
          v = pKernel.get()->Initialize(this->drObj.get(), this->rsvd.get());
        }
        else
        {
          v = pKernel.get()->Initialize(this->drObj.get(), NULL);
        }
        if (v !=1) return -3;
        // Kernel will not be initialized again
        initialized = true;
      }

      return 1;
    }//end of function

    /*! \brief ComputeProjectionMatrix
     *  This function is the driver for computing a projection matrix given a set of training data
     *  @m_InputData        : The input data
     *  @computeprojection  : If we need to create a lower-dimensional projection of
     *                        the input data.
     */
    template <class T>
    int LaplacianEigenmap<T>::ComputeProjectionMatrix(T* m_InputData, bool computeprojection)
    {
      // Do some sanity checks
      if (m_InputData->data() == NULL) return -1;
      // Check if requested dimension is correct
      if ( (m_InputData->rows() < m_InputData->cols()) && this->drObj.get()->numEigenvals > m_InputData->rows()) return -2;
      if ( (m_InputData->cols() < m_InputData->rows()) && this->drObj.get()->numEigenvals > m_InputData->cols()) return -3;

      // 1.) Normalize the data to lie between 0-1
      this->NormalizeData(m_InputData);

      // 2.) Load the kernel
      int v = InitializeKernel();

      // 3.) Check if the library could be loaded
      if (v != 1) return -4;

      // 4.) Compute the projection matrix
      v = pKernel.get()->ComputeProjection(computeprojection);

      if (v != 1) return -5;

      return 1;
    }//end of function

    /*! \brief ProjectNewData
     *  This function is the driver for transforming new data using the projection matrix and
     *  normalization factor obtained during the training phase.
     *  @m_InputData   : The input data for which a projection needs to be performed
     *  @m_OutputData  : The projected output data
     */
    template <class T>
    int LaplacianEigenmap<T>::ProjectNewData( T* m_InputData, T* m_OutputData )
    {
      // Do some sanity check on the model
      if (this->drObj.get()->zcData.data() == NULL ) return -1;
      if (this->drObj.get()->projectionMatrix.data() == NULL ) return -2;
      // Do some sanity check on the data
      if (!m_InputData->data()) return -3;
      if (m_InputData->cols() != this->drObj.get()->zcData.cols()) return -4;

      // Now, initialize the kernel
      int v = InitializeKernel();

      // Normalize the data with factors from the training data
      //this->NormalizeData(m_InputData, this->drObj.get()->normData, m_InputData);

      // Compute the out of sample projection
      pKernel.get()->ProjectNewData(m_InputData, m_OutputData);
      return 1;
    }//end of function

    // explicit instantiations
    template class LaplacianEigenmap<RMMatrix_Float>;
    template class LaplacianEigenmap<CMMatrix_Float>;
    template class LaplacianEigenmap<RMMatrix_Double>;
    template class LaplacianEigenmap<CMMatrix_Double>;
  }//end of LE namespace
}//end of namespace

typedef spin::LE::LaplacianEigenmap<spin::RMMatrix_Float>   LEFloat_Row;
typedef spin::LE::LaplacianEigenmap<spin::RMMatrix_Double>  LEDouble_Row;
typedef spin::LE::LaplacianEigenmap<spin::CMMatrix_Float>   LEFloat_Col;
typedef spin::LE::LaplacianEigenmap<spin::CMMatrix_Double>  LEDouble_Col;

//Interface for a plugin. Sadly we have to enumerate all the
//types as C does not recognize templates
extern "C" void* DR_Plugin(int* g, void* params, bool* f)
{
  // return the object type
  void* q = NULL;
  if (*g == 1) // RMMatrix_Float
  {
    LEFloat_Row* x = new LEFloat_Row(params,*f);
    q = (void*)(x);
  }
  else
  if (*g == 2)
  {
    LEDouble_Row* x = new LEDouble_Row(params,*f);
    q = (void*)(x);
  }
  if (*g == 3) // CMMatrix_Float
  {
    LEFloat_Col* x = new LEFloat_Col(params,*f);
    q = (void*)(x);
  }
  else
  if (*g == 4)
  {
    LEDouble_Col* x = new LEDouble_Col(params,*f);
    q = (void*)(x);
  }

  return q;
}//end of function
