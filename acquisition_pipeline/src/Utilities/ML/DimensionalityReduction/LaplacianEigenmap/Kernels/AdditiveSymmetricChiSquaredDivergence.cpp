#include "Utilities/ML/DimensionalityReduction/LaplacianEigenmap/AbstractKernelLaplacianEigenmap.h"
#include "Utilities/MatrixUtils/EigenUtils.h"
#include "Utilities/MatrixUtils/RandomizedSVD.h"
#include "AbstractClasses/DRObject.h"

namespace spin
{ 
  namespace LE
  {    
    /*! \brief This class defines the matrix-vector operation employed
     *  for an additive symmetric chi-squared dissimilarity measure
     */
    template <class Matrix> 
    class AdditiveSymmetricChiSquaredDivergence : public AbstractKernelLaplacianEigenmap<Matrix>
    {
      public:          
        /*! \brief The training phase of the kernel*/
        int ComputeProjection(bool projectTraining=false);
        
        /*! \brief Initialization during projection matrix computation*/
        int Initialize(void* p, void* q);
        
        /*! \brief Regress */
        int ProjectNewData(Matrix* mat, Matrix* output);
      private:
        
        /*! \brief Precompute */
        void Precompute();
        
        // Variables to be used for out-of-sample projection
        Matrix prod1;
        Matrix prod2;
        Matrix prod3;
        Matrix prod4;
        
        Matrix d1;
        Matrix d2;
        Matrix d3;
        Matrix d4;
    };//end of class
    
    /*! \brief The AdditiveSymmetricChiSquaredDivergence between two "vectors" P & Q is given by
     *  AChi(P,Q) = \sum( ((P-Q)^2 (P+Q)) / PQ)
     *            = \sum((P^2 * 1/Q) + ((1/P) * Q^2)) - \sum(P+Q),
     *  During the training phase, P and Q are the same while during the prediction phase they differ. 
     */
    
    /*! \brief The default constructor */
    template <class Matrix>
    int AdditiveSymmetricChiSquaredDivergence<Matrix>::Initialize(void* p, void* q)
    {
      this->drObj = static_cast< spin::DRObject<Matrix>*>(p);
      this->rsvd  = static_cast< spin::RandomizedSVD<Matrix>*>(q);
      if (!this->rsvd)
      {
        // This initialization is happening during Oout-of-sample projection
        if (this->drObj->zcData.data() == NULL) return -1;
        if (this->drObj->projectionMatrix.data() == NULL) return -2;
        
        // Precompute a few terms                  
        Precompute();
      }
      
      return 1;
    }//end of function
  
    /*! \brief ComputePosterior
    *  This is the "training" pipeline wherein the Kernel product with the prior probabilities is computed.
    *  computeRegression  : if we need to compute a regression for the training data
    */  
    template <class Matrix>
    int AdditiveSymmetricChiSquaredDivergence<Matrix>::ComputeProjection(bool projectTraining)
    {
      // typecast the regression object
      spin::DRObject<Matrix>* robject = this->drObj;
      // Get pointers 
      Matrix* P     = &(robject->zcData);
      
      // Do some sanity checks
      if (!P->data()) return -1;
                      
      // Get the scalar type of matrix entries
      typedef typename Matrix::Scalar Scalar;
      
  #ifndef USE_OPENCL
      // The kernel is defined by
      // A = AChi(P,Q) = \sum((P^2 * 1/Q) + ((1/P) * Q^2)) - \sum(P+Q)
      // We need to compute the diagonal sum of this matrix without computing the Gram matrix.
      // Next, we compute the value A^2 such that this can now be used for
      // computing the eigenvectors 
      
      // 1.) Build intermdiate terms
      Matrix P1     = P->unaryExpr(std::ptr_fun(spin::EigenUtils::power2_0<Scalar>));
      Matrix P2     = P->unaryExpr(std::ptr_fun<Scalar>(spin::EigenUtils::inverse<Scalar>));
      Matrix Ones   = Matrix::Ones(P->rows(), P->cols());
      Matrix Ones2  = Matrix::Ones(P->rows(), 1);
      Matrix Y      = this->rsvd->GetRand();
      
      // 2.) We first compute the value D^-0.5
      Matrix DHalf  =  (P1 * P2.transpose() * Ones2 + \
                        P2 * P1.transpose() * Ones2 - \
                       (*P) * Ones.transpose() * Ones2 - \
                       Ones * P->transpose() * Ones2).unaryExpr(std::ptr_fun<Scalar>(spin::EigenUtils::inverse_sqrt<Scalar>));
                       
      // 3.) Compute A^2
      for (int i = 0; i < this->rsvd->GetPower(); ++i)
      {
        Y  =  DHalf.asDiagonal() * P1 * P2.transpose() * Y * DHalf.asDiagonal() + \
              DHalf.asDiagonal() * P2 * P1.transpose() * Y * DHalf.asDiagonal() - \
              DHalf.asDiagonal() * (*P) * Ones.transpose() * Y * DHalf.asDiagonal() - \
              DHalf.asDiagonal() * Ones * P->transpose() * Y * DHalf.asDiagonal();
      }
      
      // 4.) Compute a Gram-Schmidt orthonormalization : Step 3 of RSVD (slide 6)
      //https://forum.kde.org/viewtopic.php?f=74&t=118568
      Y = (Y.householderQr().householderQ()).transpose();
      
      // 5.) Range(B) = Q'*A : Step 4 of RSVD (slide 6). As we did not save A we have to redo the steps
      //     that were performed in Step 2
      for (int i = 0; i < this->rsvd->GetPower(); ++i)
      {
        Y  =  DHalf.asDiagonal() * Y * P1   * P2.transpose()    * DHalf.asDiagonal() + \
              DHalf.asDiagonal() * Y * P2   * P1.transpose()    * DHalf.asDiagonal() - \
              DHalf.asDiagonal() * Y * (*P) * Ones.transpose()  * DHalf.asDiagonal() - \
              DHalf.asDiagonal() * Y * Ones * P->transpose()    * DHalf.asDiagonal();
      }
            
      // 5.) Compute the eigenvectors
      this->rsvd->ComputeSVD(&Y, &robject->projectionMatrix);
      
      // 6.) Compute the out-of-sample projected values
      if (projectTraining)
      {
        ProjectNewData(&this->drObj->zcData, &this->drObj->projection);
      }
  #else
  #endif    
      return 1;
    };//end of function
    
   /*! \brief Precompute
    *  This function precomputes data that will be subsequently used
    *  during data projection. This function should only be 
    *  initialized during projection of out-of-sample data. 
    */
    template <class Matrix>
    void AdditiveSymmetricChiSquaredDivergence<Matrix>::Precompute()
    {
      // Get the scalar type of matrix entries
      typedef typename Matrix::Scalar Scalar;
  #ifndef USE_OPENCL
      Matrix Ones = Matrix::Ones(this->drObj->zcData.rows(), this->drObj->zcData.cols());
      Matrix P    = this->drObj->projectionMatrix.transpose();
      Matrix f1   = this->drObj->zcData.unaryExpr(std::ptr_fun<Scalar>(spin::EigenUtils::power2_0<Scalar>));
      Matrix f2   = this->drObj->zcData.unaryExpr(std::ptr_fun<Scalar>(spin::EigenUtils::inverse<Scalar>));
      prod1       = P * f1;
      prod2       = P * f2;
      prod3       = P * this->drObj->zcData;
      prod4       = P * Ones;
      
      Matrix Ones2= Matrix::Ones(1,this->drObj->zcData.rows());
      d1          = Ones2*f1;
      d2          = Ones2*f2;
      d3          = Ones2*this->drObj->zcData;
      d4          = Ones2*Ones;
  #else
  #endif
    };//end of function
    
    /*! \brief ProjectNewData
    *  This is the "projection" component of the pipeline wherein the projection
    *  matrix obtained during the training phase is used to project new data
    *  Essentially this is tantamount to:
    *  \hat{y} = ProjectionMatrix' * K(P,Q) / sum(K(P,Q))
    *  where,
    *  P is the training data used to build the projection Matrix and,
    *  Q is the new data.
    *  @Q       : The new data that needs to be transformed
    *  @output  : The transformed data
    */  
    template <class Matrix>
    int AdditiveSymmetricChiSquaredDivergence<Matrix>::ProjectNewData(Matrix* Q,  Matrix* output)
    {
      // Get the scalar type of matrix entries
      typedef typename Matrix::Scalar Scalar;
      // Get pointers 
      if (!this->drObj->projectionMatrix.data()) return -1;
      if (Q->cols() != this->drObj->zcData.cols()) return -2;
      
      // Get the scalar type of matrix entries
      typedef typename Matrix::Scalar Scalar;
  
  #ifndef USE_OPENCL
      // We now follow the same logic in building the kernel
      // \hat{y} = ProjectionMatrix' * \sum((P^2 * 1/Q) + ((1/P) * Q^2)) - \sum(P+Q),
      // As we have pre-computed some terms, the outcome is derived by a matrix multiplication of 
      // of the new data and the pre-computed terms
      Matrix Ones   = Matrix::Ones(Q->rows(), Q->cols());
      Matrix P      = this->drObj->zcData;
      Matrix Q1     = Q->unaryExpr(std::ptr_fun<Scalar>(EigenUtils::inverse<Scalar>)).transpose();
      Matrix Q2     = Q->unaryExpr(std::ptr_fun<Scalar>(EigenUtils::power2_0<Scalar>)).transpose();
      // Compute the Numerator
      Matrix BNum   = prod1 * Q1 + prod2 * Q2 - prod3 * Ones.transpose() - prod4 * Q->transpose();
      
      // Compute the denominator
      Matrix BDen   = (d1 * Q1 + d2 * Q2 - d3 * Ones.transpose() - d4 * Q->transpose()).array()+Scalar{1.0e-8};

      //  Generate the values
      (*output) = (BDen.asDiagonal() * BNum).transpose();   
  #else
  #endif
      return 1;
    }//end of function
    
    template class AdditiveSymmetricChiSquaredDivergence<spin::RMMatrix_Float>;
    template class AdditiveSymmetricChiSquaredDivergence<spin::RMMatrix_Double>;
    template class AdditiveSymmetricChiSquaredDivergence<spin::CMMatrix_Float>;
    template class AdditiveSymmetricChiSquaredDivergence<spin::CMMatrix_Double>;
  }//end of namespace GP
}//end of namespace spin


typedef spin::LE::AdditiveSymmetricChiSquaredDivergence<spin::RMMatrix_Float>   ASCDFloat_Row;
typedef spin::LE::AdditiveSymmetricChiSquaredDivergence<spin::RMMatrix_Double>  ASCDDouble_Row;
typedef spin::LE::AdditiveSymmetricChiSquaredDivergence<spin::CMMatrix_Float>   ASCDFloat_Col;
typedef spin::LE::AdditiveSymmetricChiSquaredDivergence<spin::CMMatrix_Double>  ASCDDouble_Col;

//Interface for a plugin. Sadly we have to enumerate all the
//types as C does not recognize templates
extern "C" void* LE_Kernel(int g)
{
  // return the object type
  void* q = NULL;
  if (g == 1) // RMMatrix_Float
  {
    ASCDFloat_Row* x = new ASCDFloat_Row();
    q = (void*)(x);
  }
  else
  if (g == 2)
  {
    ASCDDouble_Row* x = new ASCDDouble_Row();
    q = (void*)(x);
  }
  if (g == 3) // CMMatrix_Float
  {
    ASCDFloat_Col* x = new ASCDFloat_Col();
    q = (void*)(x);
  }
  else
  if (g == 4)
  {
    ASCDDouble_Col* x = new ASCDDouble_Col();
    q = (void*)(x);
  }
  
  return q;
}//end of function

