#ifndef ABSTRACTKERNELLAPLACIANEIGENMAP_H_
#define ABSTRACTKERNELLAPLACIANEIGENMAP_H_

namespace spin
{
  // forward declaration
  template <class T>
  struct DRObject;
  
  // Forward declaration
  template <class T>
  class RandomizedSVD;

  namespace LE
  {
    template <class T>
    class AbstractKernelLaplacianEigenmap
    {
      public:       
        virtual ~AbstractKernelLaplacianEigenmap(){}
        
        /*! \brief Compute Projection Matrix */
        virtual int ComputeProjection(bool projectTraining=false) = 0;
        
        /*! \brief ProjectNewData */
        virtual int ProjectNewData(T* mat, T* output) = 0;
        
        /*! \brief Initialize Object (training) */
        virtual int Initialize(void* p, void* q) = 0;
      protected:
        // a raw pointer to the object
        spin::DRObject<T>* drObj;
        // instance of RandomizedSVD
        spin::RandomizedSVD<T>* rsvd;
    };//end of class
  }//end of namespace
}//end of function
#endif
