#include "AbstractClasses/DRObject.h"
#include "AbstractClasses/AbstractDimensionalityReduction.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <boost/serialization/array_wrapper.hpp>
#include <boost/numeric/ublas/storage.hpp>
#include "Utilities/ML/DimensionalityReduction/GrassmannPCA/grassmann_pca.hpp"
#include "Utilities/ML/DimensionalityReduction/GrassmannPCA/grassmann_pca_with_trimming.hpp"
#include "Utilities/ML/DimensionalityReduction/GrassmannPCA/private/boost_ublas_external_storage.hpp"
#include "Utilities/ML/DimensionalityReduction/GrassmannPCA/private/boost_ublas_row_iterator.hpp"

namespace spin
{
  namespace grassmannpca
  {
    template <class T>
    class GrassmannPCA : public AbstractDimensionalityReduction<T>
    {
      public:
        /*! \brief Default constructor (training)*/
        GrassmannPCA(void* p, bool f);

        /*! \brief Default destructor */
        ~GrassmannPCA(){}

        /*! \brief ComputeProjectionMatrix */
        int ComputeProjectionMatrix(T* m, bool c = false);

        /*! \brief InitializeKernel */
        int InitializeKernel(bool t = true)
        {
          // Nothing to do here
          return 1;
        }

        /*! \brief ProjectNewData */
        int ProjectNewData(T* m, T* n);
    };//end of class

    struct s_algorithm_configuration
    {
      size_t rows;
      size_t columns;
      size_t max_dimension;
      size_t max_iterations;
      size_t max_chunk_size;
      size_t nb_processors;
      size_t nb_pca_steps;
      double trimming_percentage;
    };
    
    /*! \brief grassmann_pca_dispatch
     *  This is the driver function to implement GrassmanPCA
     */
    template <class input_array_type>
    bool grassmann_pca_dispatch(input_array_type* X, \
                                s_algorithm_configuration const& algorithm_configuration, 
                                input_array_type* outputMatrix)
    {
      namespace ub = boost::numeric::ublas;

      using namespace grassmann_averages_pca;
      using namespace grassmann_averages_pca::details::ublas_helpers;


      typedef external_storage_adaptor<input_array_type> input_storage_t;
      typedef ub::matrix<input_array_type, ub::row_major, input_storage_t> input_matrix_t;

      typedef external_storage_adaptor<input_array_type> output_storage_t;
      typedef ub::matrix<input_array_type, ub::row_major, output_storage_t> output_matrix_t; 

      const size_t &dimension = algorithm_configuration.columns;
      const size_t &nb_elements = algorithm_configuration.rows;
      const size_t &max_dimension = algorithm_configuration.max_dimension;
      const size_t &max_iterations = algorithm_configuration.max_iterations;


      // input data matrix, external storage.
      input_storage_t input_storage(nb_elements * dimension, static_cast<input_array_type *>(X));
      input_matrix_t input_data(nb_elements, dimension, input_storage);

      // output data matrix, also external storage for uBlas
      output_storage_t storageOutput(dimension * max_dimension, static_cast<input_array_type *>(outputMatrix));
      output_matrix_t output_basis_vectors(max_dimension, dimension, storageOutput);

      size_t nb_pca_steps = algorithm_configuration.nb_pca_steps;

      // this is the form of the data extracted from the storage
      typedef ub::vector<input_array_type> data_t;
      typedef grassmann_pca< data_t > grassmann_pca_t;

      typedef row_iter<const input_matrix_t> const_input_row_iter_t;
      typedef row_iter<output_matrix_t> output_row_iter_t;

      // main instance
      grassmann_pca_t instance;

      if(algorithm_configuration.nb_processors > 0)
      {
        if(!instance.set_nb_processors(algorithm_configuration.nb_processors))
        {
          return false;
        }
      }

      if(algorithm_configuration.max_chunk_size > 0)
      {
        if(!instance.set_max_chunk_size(algorithm_configuration.max_chunk_size))
        {
          return false;
        }
      }

      if(!instance.set_nb_steps_pca(nb_pca_steps))
      {
        return false;
      }

      return instance.batch_process(  max_iterations, max_dimension, 
                                      const_input_row_iter_t(input_data, 0),
                                      const_input_row_iter_t(input_data, input_data.size1()),
                                      output_row_iter_t(output_basis_vectors, 0), 0);
    }//end of function
  
    /*! \brief Default constructor 
     *  @params : The parameters associated with Grassmann PCA 
     */
    template <class T>
    GrassmannPCA<T>::GrassmannPCA(void* params, bool f):
                    AbstractDimensionalityReduction<T>(params, f)
    {
      // populate some meta information
      this->drObj.get()->algorithm="grassmann_pca";
    }//end of function
    
    /*! \brief ComputeProjectionMatrix
     *  This function is the driver for implementing Grassmannian Averages PCA
     *  @m_InputData      : The input data whose PCA is sought
     *  @computeprojection: If a projection of the input data is sought
     */
    template <class T>
    int GrassmannPCA<T>::ComputeProjectionMatrix( T* m_InputData, bool computeprojection )
    {
      typedef typename T::Scalar Scalar;
      // Do some sanity checks
      if (m_InputData->data() == NULL) return -1;
      // Check if requested dimension is correct
      if ( (m_InputData->rows() < m_InputData->cols()) && this->drObj.get()->numEigenvals > m_InputData->rows()) return -2;
      if ( (m_InputData->cols() < m_InputData->rows()) && this->drObj.get()->numEigenvals > m_InputData->cols()) return -3;
      
      // 1.) Zero center the data
      this->ZeroCenterData(m_InputData);

      // 2.) Build the configuration file (hard-coded for now)
      s_algorithm_configuration config;
      // Check the dimensions of inputs
      config.rows                 = this->drObj.get()->zcData.rows();
      config.columns              = this->drObj.get()->zcData.cols();
      config.trimming_percentage  = -1;
      config.max_iterations       = config.rows; // by default the number of points
      config.max_chunk_size       = std::numeric_limits<size_t>::max();
      config.nb_processors        = 4;
      config.max_dimension        = this->drObj.get()->numEigenvals;
      config.nb_pca_steps         = 10;
      
      // 3.) Allocate memory for the output
      T mOutput = T::Zero(config.max_dimension, config.columns);
      // 4.) Run the pipeline
      bool result = grassmann_pca_dispatch<Scalar>( this->drObj.get()->zcData.data(), config, mOutput.data());
      if (! result) return -4;

      // populate the projection matrix
      this->drObj.get()->projectionMatrix = mOutput.transpose();

      // finally, compute the projection of the input data (if requested)
      if (computeprojection)
      {
        this->drObj.get()->projection = this->drObj.get()->zcData * this->drObj.get()->projectionMatrix;
      }
      
      return 1;
    }//end of function
  
    /*! \brief ProjectNewData
     *  This function is the driver for transforming new data using the projection matrix and mean
     *  obtained during the training phase.
     *  @m_InputData      : The input data that needs to be transformed
     *  @m_ProjectedData  : The output projected data;
     */
    template <class T>
    int GrassmannPCA<T>::ProjectNewData( T* m_InputData, T* m_ProjectedData )
    {
      // do some sanity check
      if (this->drObj.get()->projectionMatrix.data() == NULL) return -1;
      
      if (this->drObj.get()->mean.data() == NULL ) return -2;
      
      typedef typename T::Scalar Scalar;
      
      // Allocate a matrix of Ones
      T Ones = T::Ones(m_InputData->rows(),1);
      
      //Subtract the mean from the data and apply the projection matrix
      (*m_ProjectedData)  = ((*m_InputData - Ones * this->drObj.get()->mean) * this->drObj.get()->projectionMatrix).template cast<Scalar>();
      
      return 1;
    }//end of function
  
    // Explicit instantiations
    template class GrassmannPCA<RMMatrix_Float>;
    template class GrassmannPCA<RMMatrix_Double>;
    template class GrassmannPCA<CMMatrix_Float>;
    template class GrassmannPCA<CMMatrix_Double>;
  }//end of namespace grassmannpca
}//end of namespace

typedef spin::grassmannpca::GrassmannPCA<spin::RMMatrix_Float>   GPCAFloat_Row;
typedef spin::grassmannpca::GrassmannPCA<spin::RMMatrix_Double>  GPCADouble_Row;
typedef spin::grassmannpca::GrassmannPCA<spin::CMMatrix_Float>   GPCAFloat_Col;
typedef spin::grassmannpca::GrassmannPCA<spin::CMMatrix_Double>  GPCADouble_Col;

//Interface for a plugin. Sadly we have to enumerate all the
//types as C does not recognize templates
extern "C" void* DR_Plugin(int* g, void* params, bool* f)
{
  // return the object type
  void* q = NULL;
  if (*g == 1) // RMMatrix_Float
  {
    GPCAFloat_Row* x = new GPCAFloat_Row(params,*f);
    q = (void*)(x);
  }
  else
  if (*g == 2)
  {
    GPCADouble_Row* x = new GPCADouble_Row(params,*f);
    q = (void*)(x);
  }
  if (*g == 3) // CMMatrix_Float
  {
    GPCAFloat_Col* x = new GPCAFloat_Col(params,*f);
    q = (void*)(x);
  }
  else
  if (*g == 4)
  {
    GPCADouble_Col* x = new GPCADouble_Col(params,*f);
    q = (void*)(x);
  }

  return q;
}//end of function
