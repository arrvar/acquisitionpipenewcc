#include "AbstractClasses/DRObject.h"
#include "AbstractClasses/AbstractDimensionalityReduction.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include <unsupported/Eigen/Polynomials>
#include <cmath>

namespace spin
{
  namespace nspca
  {
    /*! \brief NSPCA
     *  This class is an implementation of the non-negative sparse
     *  PCA algorithm of Zass and Sashua
     */
    template <class T>
    class NSPCA : public AbstractDimensionalityReduction<T>
    {
      public:
        /*! \brief Default constructor */
        NSPCA(void* params, bool f);

        ~NSPCA(){}

        /*! \brief ComputeProjectionMatrix */
        int ComputeProjectionMatrix(T* m, bool c = false);

        /*! \brief InitializeKernel */
        int InitializeKernel(bool t = true)
        {
          // Nothing to do here
          return 1;
        }

        /*! \brief ProjectNewData */
        int ProjectNewData(T* m, T* n);
      private:
        double breakPercent = 0.00001; //change between two iterations
        double alpha        = 1.0e+5;  // we want the data to be as orthogonal as possible
        double beta         = 0;       // we dont care about sparsity
        int    iter         = 500;     // The number of iterations that are needed
    };//end of class

    /*! \brief ComputeDifference
     *  This function is used to compute the difference between projection
     *  matrices obtained at two successive iterations and return a scalar
     *  value. This value will subsequently determine if the iteration
     *  needs to be terminated or continued
     */
    double ComputeDifference(RMMatrix_Double* A, RMMatrix_Double* U, double alpha, double beta)
    {
      RMMatrix_Double temp1 = U->transpose();
      RMMatrix_Double temp2 = (RMMatrix_Double::Identity(U->cols(), U->cols()) - (temp1 * (*U)));

      double t1 = (temp1 * (*A) * (*U)).trace();
      double t2 = (temp2.array().square()).sum();
      double t  = t1/2.0 - alpha/4.0 * t2 - beta * (U->sum());
      return t;
    }//end of function

    /*! \brief Default constructor */
    template <class T>
    NSPCA<T>::NSPCA(void* params, bool f):
              AbstractDimensionalityReduction<T>(params, f)
    {
      // populate some meta information
      this->drObj.get()->algorithm="nspca";

      // Populate miscellaneous parameters necessary for this module
      spin::DRParams* pParams = static_cast<spin::DRParams*>(params);

      if (pParams->nspca_breakPercent > 0)
        breakPercent  = pParams->nspca_breakPercent;
      if (pParams->nspca_alpha > 0)
        alpha         = pParams->nspca_alpha;
      if (pParams->nspca_beta >= 0)
        beta          = pParams->nspca_beta;
      if (pParams->nspca_iterations > 0)
        iter          = pParams->nspca_iterations;
    }//end of function

    /*! \brief Compute
    *  This function implements the Non-negative sparse PCA algorithm of
    *  Zass and Sashua as described in this paper:
    *  https://papers.nips.cc/paper/3104-nonnegative-sparse-pca.pdf
    *  @m_inputData       : The input data as a matrix object
    *  @computeprojection : If the input data should be projected
    */
    template <class T>
    int NSPCA<T>::ComputeProjectionMatrix( T* m_InputData, bool computeprojection )
    {
      typedef typename T::Scalar Scalar;

      // Do some sanity checks
      if (m_InputData->data() == NULL) return -1;
      // Check if requested dimension is correct
      if ( (m_InputData->rows() < m_InputData->cols()) && this->drObj.get()->numEigenvals > m_InputData->rows()) return -2;
      if ( (m_InputData->cols() < m_InputData->rows()) && this->drObj.get()->numEigenvals > m_InputData->cols()) return -3;

      // 1.) Zero center the data
      int N = m_InputData->rows();
      this->ZeroCenterData(m_InputData);
      // Make a copy of the data
      RMMatrix_Double D = this->drObj.get()->zcData.template cast<double>();

      // 2.) Compute the covariance matrix
      RMMatrix_Double A = (D.transpose() * D)*(1.0/N);

      int d = A.rows();
      int lowDim = this->drObj.get()->numEigenvals;

      // 3.) Create a default projection matrix
      srand((unsigned int) time(0));
      //http://stackoverflow.com/questions/35827926/eigen-matrix-library-filling-a-matrix-with-random-double-values-in-a-given-range
      RMMatrix_Double U = (RMMatrix_Double::Random(d,lowDim) + RMMatrix_Double::Ones(d,lowDim)).array()*0.5;

      RMMatrix_Double Usquare = U.array().square();

      // 4.) Initialize the error to be a large negative value
      double err = -1.0e10f;

      // 5.) The outer iteration loop
      for (int iterations = 0; iterations < iter; ++iterations)
      {
        for (int i = 0; i < d; ++i)
        {
          for (int j = 0; j < lowDim; ++j)
          {
            //Find roots:
            U(i,j) = 0;
            Usquare(i,j) = 0;

            // Extract the row and column
            Eigen::VectorXd p1 = U.row(i);
            Eigen::VectorXd p2 = U.col(j);

            double a2 = alpha * (Usquare.row(i).sum() + Usquare.col(j).sum() - 1) - A(i,i);
            double a1 = beta + alpha * (p1.dot(U.transpose() * p2)) - (p2.transpose().dot(A.col(i)));

            // Compute roots
            //http://www.ce.unipr.it/~medici/eigen-poly.html
            Eigen::PolynomialSolver<double, Eigen::Dynamic> solver;
            Eigen::VectorXd coeff(4);
            // Note: The matlab version has the coefficients flipped !
            coeff[3] = alpha;
            coeff[2] = 0;
            coeff[1] = a2;
            coeff[0] = a1;
            solver.compute(coeff);

            // From the roots, choose the root that minimizes the objective
            // function. We will need to select only the positive real roots as
            // the roots computed by the solver will be complex in nature.
            Eigen::PolynomialSolver<double, Eigen::Dynamic>::RootsType roots = solver.roots();
            Eigen::NumTraits<double>::Real absImaginaryThreshold = Eigen::NumTraits<double>::dummy_precision();

            double minU = 0;
            double minObj = 0;
            for(int r =0; r < roots.rows(); ++r)
            {
              // first deem a root to be real if the value of the imaginary
              // component is less than the threshold
              if (roots[r].imag() < absImaginaryThreshold)
              {
                double rp  = roots[r].real();
                // This is a real root. Now check if it is positive
                if (rp > 0)
                {
                  double rp2 = rp * rp;
                  double obj = alpha * rp2 * rp2 + 2.0 * a2 * rp2 + 4.0 * a1 * rp;

                  if (obj < minObj)
                  {
                    minObj = obj;
                    minU   = rp;
                  }
                }
              }
            }

            // Update U
            U(i,j) = minU;
            Usquare(i,j) = minU*minU;
          }//finished checking all low-dimension elements
        }//finished checking all rows of the data

        // Check if we need to break from the iterations
        double prev_err = err;
        err = ComputeDifference(&A, &U, alpha, beta);
        // note the change
        double change = (err - prev_err)/std::abs(err);

        // As the percentage change threshold has been
        // satisfied, we break from the loop
        if (change <= breakPercent) break;
      }//end of iterations

      // 6.) Set the projection matrix
      this->drObj.get()->projectionMatrix = U.template cast<Scalar>();

      // 7.) Check if the user has requested projecting the original data
      if (computeprojection)
      {
        this->drObj.get()->projection = this->drObj.get()->zcData * this->drObj.get()->projectionMatrix;
      }

      return 1;
    }//end of function

    /*! \brief ProjectNewData
    *  This function is the driver for transforming new data using the projection matrix and mean
    *  obtained during the training phase.
    *  @m_InputData      : The input data that needs to be transformed
    *  @m_ProjectedData  : The output projected data;
    */
    template <class T>
    int NSPCA<T>::ProjectNewData( T* m_InputData, T* m_ProjectedData )
    {
      // do some sanity check
      if (this->drObj.get()->projectionMatrix.data() == NULL) return -1;

      if (this->drObj.get()->mean.data() == NULL ) return -2;

      typedef typename T::Scalar Scalar;

      // Allocate a matrix of Ones
      T Ones = T::Ones(m_InputData->rows(),1);

      //Subtract the mean from the data and apply the projection matrix
      (*m_ProjectedData)  = ((*m_InputData - Ones * this->drObj.get()->mean) * \
                            this->drObj.get()->projectionMatrix).template cast<Scalar>();

      return 1;
    }//end of function

    // Explicit instantiations
    template class NSPCA<RMMatrix_Float>;
    template class NSPCA<RMMatrix_Double>;
    template class NSPCA<CMMatrix_Float>;
    template class NSPCA<CMMatrix_Double>;
  }//end of namespace nspca
}//end of namespace

typedef spin::nspca::NSPCA<spin::RMMatrix_Float>   NSPCAFloat_Row;
typedef spin::nspca::NSPCA<spin::RMMatrix_Double>  NSPCADouble_Row;
typedef spin::nspca::NSPCA<spin::CMMatrix_Float>   NSPCAFloat_Col;
typedef spin::nspca::NSPCA<spin::CMMatrix_Double>  NSPCADouble_Col;

//Interface for a plugin. Sadly we have to enumerate all the
//types as C does not recognize templates
extern "C" void* DR_Plugin(int* g, void* params, bool* f)
{
  // return the object type
  void* q = NULL;
  if (*g == 1) // RMMatrix_Float
  {
    NSPCAFloat_Row* x = new NSPCAFloat_Row(params,*f);
    q = (void*)(x);
  }
  else
  if (*g == 2)
  {
    NSPCADouble_Row* x = new NSPCADouble_Row(params,*f);
    q = (void*)(x);
  }
  if (*g == 3) // CMMatrix_Float
  {
    NSPCAFloat_Col* x = new NSPCAFloat_Col(params,*f);
    q = (void*)(x);
  }
  else
  if (*g == 4)
  {
    NSPCADouble_Col* x = new NSPCADouble_Col(params,*f);
    q = (void*)(x);
  }

  return q;
}//end of function
