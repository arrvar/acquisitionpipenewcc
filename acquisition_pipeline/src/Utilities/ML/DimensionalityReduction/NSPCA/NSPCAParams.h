#ifndef NSPCAPARAMS_H_
#define NSPCAPARAMS_H_
#include "AbstractClasses/DRParams.h"

namespace spin
{
  namespace nspca
  {
    // Parameters associated with the NSPCA
    struct NSPCAParams : public spin::DRParams
    {
      NSPCAParams()
      {
        // default values
        alpha           = 1.0e+5;
        beta            = 0;
        iterations      = 500;
        breakPercent    = 0.00001;
      }

      // Implementation of virtual method
      void SetAdditionalParams(void* _p)
      {
        NSPCAParams* p = static_cast<NSPCAParams*>(_p);
        alpha       = p->alpha;
        beta        = p->beta;
        iterations  = p->iterations;
        breakPercent= p->breakPercent;
      }
      double alpha ;
      double beta;
      int    iterations;
      double breakPercent;
    };//end of params
  }//end of namespace nspca
}//end of namespace
#endif
