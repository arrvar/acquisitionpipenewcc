#include "Utilities/ML/Regression/GaussianProcess/AbstractGPKernel.h"
#include "AbstractClasses/RegressionObject.h"
#include <dlibxx.h>

namespace spin
{
  namespace
  {
    template <class T>
    struct AKModule
    {
      typedef AbstractKernel<T> KType;
      ~AKModule()
      {
        // Check if the library was loaded successfully.
        lib.get()->close();
      }
      std::shared_ptr<dlibxx::handle> lib;
      std::shared_ptr< KType > pModule;
    };//end of struct
  }//end of namespace

  /*! \brief LoadPlugin
   *  This function is used to load a module which is defined
   *  in the filename libName. This name should have been created prior
   *  to calling this function.
   */
  template <class T>
  int AbstractGPKernel<T>::LoadPlugin(std::string libName)
  {
    // Check if the library exists in the given path
    boost::filesystem::path dfile(libname.c_str());
    if (!boost::filesystem::exists(dfile))
    {
      return -1; // the file does not exis
    }

    // Instantiate an APModule object
    module = std::make_shared<AKModule>();
    // And Instantiate the loader
    module->lib = std::make_shared<dlibxx::handle>();
    // Resolve symbols when they are referenced instead of on load.
    module->lib->resolve_policy(dlibxx::resolve::lazy);
    // Make symbols available for resolution in subsequently loaded libraries.
    module->lib->set_options(dlibxx::options::global | dlibxx::options::no_delete);
    // Load the library specified.
    module->lib->load(libname);
    // Check if the library could be loaded
    if (!module->lib->loaded()) return -2;

    // Now, we try and create the plugin
    module->pModule = module->lib->template create<AKModule::KType>("Regression_Kernel");
    // Check if the module was loaded
    if (!module->pModule) return -3;

    return 1;
  }//end of function

  /*! \brief InitializeTraining
   *  This function is used to iniitialize a kernel during the training phase.
   */
  template <class T>
  int AbstractGPKernel<T>::InitializeTraining(void* params)
  {
    int h = module->pModule->Initialize(params);
    if (h !=1) return -1;

    return 1;
  }//end of function

  /*! \brief InitializePrediction
   *  This function is used to iniitialize a kernel during the prediction phase.
   */
  template <class T>
  int AbstractGPKernel<T>::InitializePrediction(void* model)
  {
    int h = module->pModule->Initialize(model,false);
    if (h !=1) return -1;

    return 1;
  }//end of function

  /*! \brief ComputeProjectionMatrix
   *  This function is used to compute a projection matrix during training. The
   *  computed matrix will be stored in kObject which should have been defined
   *  in the calling function. Optionally, a projection for the training data
   *  is also computed if it is sought.
   *  @M      : Training data
   *  @N      : Response (or observed) data,
   *  @model  : The regression object that will hold the projection matrix
   */
  template <class T>
  int AbstractGPKernel<T>::ComputeProjectionMatrix( void* M, void* N,\
                                                    void* model)
  {
    int h = module->pModule->ComputeProjectionMatrix(M, N, model);
    if (h !=1) return -1;

    return 1;
  }//end of function

  /*! \brief ProjectNewData
   *  This function is used to project a new dataset using a previously
   *  computed prior model. It is assumed that the model has been loaded and
   *  the kernel initialized prior to calling this function.
   *  @M    : The data whose observation values needs to be predicted
   *  @N    : The projected values.
   */
  template <class T>
  int AbstractGPKernel<T>::ProjectNewData( void* M, void* N)
  {
    int h = module->pModule->ProjectNewData(M, N);
    if (h !=1) return -1;

    return 1;
  }//end of function

  // explicut templates
  template class AbstractGPKernel<float>;
  template class AbstractGPKernel<double>;
}//end of namespace
