#ifndef ABSTRACTGPKERNEL_H_
#define ABSTRACTGPKERNEL_H_
#include <memory>

namespace spin
{
  // forward declaration
  template <class T>
  struct AKModule;

  template <class T>
  class AbstractGPKernel
  {
    public:
      // defualt constructor
      AbstractGPKernel();
      // default destructor
      ~AbstractGPKernel();
      // Initialize kernel during training
      int InitializeTraining(void* p);
      // Initialize kernel during prediction
      int InitializePrediction(void* p);
      // Compute projection matrix
      int ComputeProjectionMatrix(void* M, void* N, void* model);
      // Compute regression for new data
      int ProjectNewData(void* M, void* N);
    private:
      // placeholder for kernel module to be loaded
      std::shared_ptr< AKModule<T> > module;
      // load the plugin
      int LoadPlugin(std::string p);
  };//end of class
}//end of namespace
#endif
