#include "AbstractClasses/AbstractRegression.h"
#include "AbstractClasses/RegressionObject.h"
#include "Utilities/ML/Regression/GaussianProcess/GPParams.h"
#include "Utilities/ML/Regression/GaussianProcess/AbstractGPKernel.h"

namespace spin
{
  /*! \brief Gaussian Process is a method for implementing Non-linear kernel based regression
   *  employing the well known Kernel trick. More specifically, we ensure the following:
   *  a.) The input data is present as an M x k matrix where each row of the data corresponds to
   *      a single data sample.
   *  b.) We are also aware of the "kernel" i.e., dot-product based operator that will transform the
   *      low-dimension feature space into a higher dimension space.
   *  c.) We are also aware of a prior associated with the data sample. For example, in case of regression
   *      we know what is the outcome that we typically should expect with each input data. For classification
   *      problems, however, we assign probability of mapping it to 1,2,...p labels. This can be based on some
   *      foreknowledge about the data.
   *  The Gaussian process formulation consists of two distinct phases:
   *  a.) In the first phase, we use the M x k and apply the "Kernel" formulation to obtain a K = M x M matrix. Obviously
   *      as the # of samples increases, building this matrix would become computationally prohibitive.
   *  b.) The following measure is computed:
   *      H = (K + \lambda I)^-1 * y, where
   *      K is the MxM kernel matrix, \lambda is a small regularization constant, I is the identity matrix, and y
   *      is the M x p matrix containing prior information about the data (either regression or classification).
   *  c.) In the second phase, regression or classification need to be applied to a set of unseen data points having
   *      dimension N x k (N data samples). Using the same "kernel-trick", a matrix of dimension M X N is created and
   *      multiplied with H to obtain the posterior information i.e.
   *      \hat{y} = K(M,N) * H
   *  As we can see, building these high dimensional matrices is computationally prohibitive and we would like to avoid
   *  it. However, this is contingent on the fact that the kernel supports it.
   *  This class is an abstraction of a set of kernels that can be used to achieve the purpose of doing Gaussian Process
   *  regression without actually building the high dimensional kernel matrices! Details of each kernel will be
   *  described in corresponding files.
   *  As this process involves a matrix inversion, we resort to a low-rank decomposition as discussed in the following
   *  paper:
   *  N.Halko, P.G.Martinsson and J.A.Tropp, "Finding Structure with Randomness:
   *  Stochastic algorithms for constructing approximate matrix decompositions".
   *  It has been noted by the authors in the aforementioned paper that increasing the power of the matrix
   *  improves the accuracy of eigenvalues. Hence, if
   *  A = (K + \lambda I), then, we compute the inverse of A^2.
   *  Refer to the following presentation:
   *  https://amath.colorado.edu/faculty/martinss/Talks/2010_mmds_martinsson.pdf
   *  This presentation is hereby refered to as RSVD;
   */


  /*! \brief GaussianProcess
   *  This class is an abstraction of a specific implementation of LaplacianEigenmap.
   */
  template <class T>
  class GaussianProcess : public spin::AbstractRegression<T>
  {
    public:
      typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;
      typedef AbstractGPKernel<T> Kernel;

      /*! \brief Default constructor*/
      GaussianProcess();

      /*! \brief Default destructor */
      ~GaussianProcess()
      {
        lib.get()->close();
      }

      /*! \brief ComputeProjectionMatrix */
      int ComputeProjectionMatrix(void* input, void* obs, void* model, bool c = false);

      /*! \brief Initialize */
      int Initialize(void* params, void* _model);

      /*! \brief ProjectNewData */
      int ProjectNewData(T* m, T* n);
    private:
      /*! \brief InitializeKernel */
      int InitializeKernel(bool t = true);

      std::shared_ptr<dlibxx::handle> lib;
      std::shared_ptr< Kernel > pKernel;
      bool initialized = false;
  };//end of class

  /*! \brief Constructor
   */
  template <class T>
  GaussianProcess<T>::GaussianProcess()
  {
    // Initialize the Regression Object model
    this->model = std::make_shared< RegressionObject<Matrix> >();
  }//end of function

  /*! \brief Initialize
   *  This function initializes the GaussianProcess Object. If
   *  params is set to NULL, and obj is not NULL, then it implies that the
   *  prediction phases is being undertaken. If the latter, then it implies a
   *  training phase is being undertaken.
   *  @params   : A parameter object that needs to be generated prior to
   *              calling this function. If this is not initialized or has
   *              problems, and, model is also NULL, then an error is returned.
   *  @model    : A model that is deserialized from a file and passed to this
   *              function.
   */
  template <class T>
  int GaussianProcess::Initialize<T>(void* _params, void* _model)
  {
    // typecast data
    RegressionObject* pModel = static_cast< RegressionObject<Matrix>*>(_model);
    GPParams* params = static_cast< GPParams* >(_params);
    // Check if the data is consistent
    if (!params && !pModel) return -1; // both cannot be nullptr
    if (params && !pModel)
    {
     // This is the training phase.
     pKernel = std::make_shared<Kernel>();
     int h = pKernel->InitializeTraining(_params);
     if (h != 1) return -2;
    }
    else
    if (!params && pModel)
    {
     // This is the testing phase.
     pKernel = std::make_shared<Kernel>();
     int h = pKernel->InitializePrediction(_model);
     if (h !=1) return -3;
    }
    return 1;
  }//end of function

  /*! \brief ComputeProjectionMatrix
   *  This function is used to compute a projection matrix that will be used
   *  to regress new data. Optionally, the current data can also be projected
   *  using the projection matrix.
   *  @_M       : The training data
   *  @_P       : The observation data
   *  @c        : If the training data needs to be projected (default is false)
   */
  template <class T>
  int GaussianProcess::ComputeProjectionMatrix<T>(void* _M, void* _P, bool c)
  {
    // Typecast data
    Matrix* M = static_cast<Matrix*>(_M);
    // sanity checks
    if (!M->data()) return -1;
    if (M->rows() * M->cols() <=0) return -2;

    // Typecast data
    Matrix* P = static_cast<Matrix*>(_P);
    // sanity checks
    if (!P->data()) return -1;
    if (P->rows() * P->cols() <=0) return -3;
    if (P->rows() != M->rows()) return -4;

    // Check if model can be computed
    int h = pKernel->ComputeProjectionMatrix(_M, _P, this->model.get(), c);
    if (h !=1) return -3;

    return 1;
  }//end of function

  /*! \brief ProjectNewData
   *  This function is used to project new data given an existing model. It is
   *  assumed that  the model has been initialized prior to calling this
   *  function. If not, the process will gracefully exit.
   *  @M       : The new data whose projection has to be computed
   *  @N       : The output data after applying the projection matrix.
   */
  template <class T>
  int GaussianProcess::ProjectNewData<T>(void* _M, void* _N)
  {
    // Typecast data
    Matrix* M = static_cast<Matrix*>(_M);
    // sanity checks
    if (!M->data()) return -1;
    if (M->rows() * M->cols() <=0) return -2;

    // Check if projection can be computed
    int h = pKernel->ProjectNewData(_M, _N);
    if (h !=1) return -3;

    return 1;
  }//end of function

  // explicit instantiations
  template class GaussianProcess<float>;
  template class GaussianProcess<double>;
}//end of namespace

// C-style interface. Requires enumeration of all types.
typedef spin::GaussianProcess<float>   GPFloat;
typedef spin::GaussianProcess<double>  GPDouble;

//Interface for a plugin. Sadly we have to enumerate all the
//types as C does not recognize templates
extern "C" void* Regression_Module()
{
  // return the object type
  void* q = NULL;
  if (*g == 0) // RMMatrix_Float
  {
    GPFloat* x = new GPFloat();
    q = (void*)(x);
  }
  if (*g == 1) // RMMatrix_Double
  {
    GPDouble* x = new GPDouble();
    q = (void*)(x);
  }

  return q;
}//end of function
