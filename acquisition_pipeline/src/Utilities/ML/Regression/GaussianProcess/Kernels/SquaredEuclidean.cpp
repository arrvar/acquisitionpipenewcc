#include "Utilities/ML/Regression/GaussianProcess/AbstractKernelGP.h"
#include "Utilities/ML/Regression/GaussianProcess/GPParams.h"
#include "Utilities/MatrixUtils/EigenUtils.h"
#include "Utilities/MatrixUtils/RandomizedSVD.h"
#include "Utilities/MatrixUtils/MatrixProduct.h"
#include "AbstractClasses/RegressionObject.h"
#include <map>
#include <string>

/*! \brief SquaredEuclidean
 *  The functions described in this file pertain to efficient computation of
 *  kernel when using a squared-Euclidean distance metric:
 *  d(X,Y) = X^2 + Y^2 -2*X*Y
 *  The similarity is computed using a negative expotential
 * K(X,Y) = exp(-d^2/(2*sigma)), where \sigma is a regularization factor.
 */
namespace spin
{
  /*! \brief This class defines the matrix-vector operation employed
   *  for an additive symmetric chi-squared dissimilarity measure
   */
  template <class T>
  class SquaredEuclidean : public AbstractKernelGP<T>
  {
    public:
      typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Matrix;
      typedef std::map<std::string, Matrix> MatType;

      /*! \brief Initialize */
      int Initialize(void* p, bool train=true);

      /*! \brief ComputeProjectionMatrix */
      int ComputeProjectionMatrix(void* M, void* N, void* model, bool c=false);

      /*! \brief ProjectNewData */
      int ProjectNewData(void* M, void* N);

    private:
      /*! \brief Precompute */
      void Precompute(void* model);
      // Other variables
      float sigma = 1.0;
      int   q     = 2;
      int rank    = 5;
      float lambda= 1.0e-4;
      MatType mType;
    };//end of class

    /*! \brief Initialize
     *  This function is used to initialize a squared-euclidean kernel.
     *  Depending on which mode of operation we are in, we either initialize
     *  the model (during training), or use existing matrices in the model to
     *  precompute matrices.
     */
    template <class T>
    int SquaredEuclidean::Initialize<T>(void* p, bool train)
    {
      if (train)
      {
        // We are in the training phase. Let us assign some values
        GPParams* params = static_cast<GPParams*>(p);
        if (!params) return -1;
        sigma = params->sigma;
        q     = params->q;
        rank  = params->rank;
        lambda= params->lambda;
      }
      else
      {
        // A model has been previously generated. We will precompute some
        // matrices using the model.
        int h = Precompute(p);
        if (h !=1) return -1;
      }

      return 1;
    }//end of function

    /*! \brief ComputeProjectionMatrix
     *  This is the "training" pipeline wherein the product of the inverse of
     *  the Kernel and the prior matrix is computed.
     *  @_M       : The training data
     *  @_N       : The prior information associated with _M
     *  @_model   : The model that will be generated
     */
    template <class T>
    int SquaredEuclidean<Matrix>::ComputeProjectionMatrix(void* _M, void* _N, \
                                                          void* _model)
    {
      // sanity checks
      Matrix* M = static_cast<Matrix*>(_M);
      if (!M->data()) return -1;
      Matrix* N = static_cast<Matrix*>(_N);
      if (!N->data()) return -2;
      if (M->rows() != N->rows()) return -3;

      int dim = M->rows();

      // List out elements that will be associated with the Squared Euclidean
      // Kernel. As described in Chapter 2, Eq 2.6 and 2.7, and the
      // simplifications suggested in Algorithm 1, we use a random initial
      // guess from the randomizedSVD algorithm to effect the multiplication.
      std::shared_ptr< RandomizedSVD<Matrix> > rsvd =\
      std::make_shared< RandomizedSVD<Matrix> >( static_cast<int>(M->rows()), rank, q);

      // Get the randomized matrix from rsvd
      Matrix Y     = rsvd->GetRand();
      // Get other matrices that would be required
      Matrix M2    = M->unaryExpr(std::ptr_fun(EigenUtils::power2_0<T>));
      Matrix Ones  = Matrix::Ones(M->rows(), M->cols());

      // We populate mType with all matrices that are required in  this kernel
      mType["0"] = *M;
      mType["1"] = M2;
      mType["2"] = Ones;

      // Step 2 in Algorithm 1 from the document. q corresponds to 2q
      // As shown above, none of the matrices are stored barring the essential
      // ones.
      for (int step = 0; step < q; ++step)
      {
        // Make a copy of the regularization
        Matrix Y1 = Y * T{lambda};
        // As per Eq. 2.7, we start the operation from right to left with the
        // dimensions being same.
        // 1.) (I - 1/N * (1 * 1'))
        Y   = Y - Matrix::Ones(dim,1)*(Matrix::Ones(1,dim) * Y) * T{1.0/dim};
        // 2.) Compute all the remaining products (as per Eq. 2.6)
        ComputePermutationProducts(0);
        // 3.) At the end of the product, we implement the left-most centering
        //     product (Eq. 2.7)
        Y   = Y - Matrix::Ones(dim,1)*(Matrix::Ones(1,dim) * Y) * T{1.0/dim};
        // 4.) Finally, we add a small regularization to the kernel
        Y   = Y + Y1;
      }

      // Step 3 in Algorithm 1;
      // We Compute a Gram-Schmidt orthonormalization
      //https://forum.kde.org/viewtopic.php?f=74&t=118568
      Y = (Y.householderQr().householderQ()).transpose();

      // Step 4 in Algorithm 1 from the document. q corresponds to 2q
      // As shown above, none of the matrices are stored barring the essential
      // ones.
      for (int step = 0; step < q; ++step)
      {
        // Make a copy of the regularization
        Matrix Y1 = Y * T{lambda};
        // As per Eq. 2.7, we start the operation from right to left with the
        // dimensions being same.
        // 1.) (I - 1/N * (1 * 1'))
        Y   = Y - (Y * Matrix::Ones(dim,1))*(Matrix::Ones(1,dim)) * T{1.0/dim};
        // 2.) Compute all the remaining products (as per Eq. 2.6)
        ComputePermutationProducts(1);
        // 3.) At the end of the product, we implement the right-most centering
        //     product (Eq. 2.7)
        Y   = Y - (Y * Matrix::Ones(dim,1))*(Matrix::Ones(1,dim)) * T{1.0/dim};
        // 4.) Finally, we add a small regularization to the kernel
        Y   = Y + Y1;
      }

      // 5.) Compute the eigenvectors
      this->rsvd->ComputeSVD(&Y, &robject->projectionMatrix);

      // 6.) Compute the out-of-sample projected values
      if (projectTraining)
      {
        ProjectNewData(&this->drObj->zcData, &this->drObj->projection);
      }

      return 1;
    };//end of function

   /*! \brief Precompute
    *  This function precomputes data that will be subsequently used
    *  during data projection. This function should only be
    *  initialized during projection of out-of-sample data.
    */
    template <class Matrix>
    void AdditiveSymmetricChiSquaredDivergence<Matrix>::Precompute()
    {
      // Get the scalar type of matrix entries
      typedef typename Matrix::Scalar Scalar;
  #ifndef USE_OPENCL
      Matrix Ones = Matrix::Ones(this->drObj->zcData.rows(), this->drObj->zcData.cols());
      Matrix P    = this->drObj->projectionMatrix.transpose();
      Matrix f1   = this->drObj->zcData.unaryExpr(std::ptr_fun<Scalar>(spin::EigenUtils::power2_0<Scalar>));
      Matrix f2   = this->drObj->zcData.unaryExpr(std::ptr_fun<Scalar>(spin::EigenUtils::inverse<Scalar>));
      prod1       = P * f1;
      prod2       = P * f2;
      prod3       = P * this->drObj->zcData;
      prod4       = P * Ones;

      Matrix Ones2= Matrix::Ones(1,this->drObj->zcData.rows());
      d1          = Ones2*f1;
      d2          = Ones2*f2;
      d3          = Ones2*this->drObj->zcData;
      d4          = Ones2*Ones;
  #else
  #endif
    };//end of function

    /*! \brief ProjectNewData
    *  This is the "projection" component of the pipeline wherein the projection
    *  matrix obtained during the training phase is used to project new data
    *  Essentially this is tantamount to:
    *  \hat{y} = ProjectionMatrix' * K(P,Q) / sum(K(P,Q))
    *  where,
    *  P is the training data used to build the projection Matrix and,
    *  Q is the new data.
    *  @Q       : The new data that needs to be transformed
    *  @output  : The transformed data
    */
    template <class Matrix>
    int AdditiveSymmetricChiSquaredDivergence<Matrix>::ProjectNewData(Matrix* Q,  Matrix* output)
    {
      // Get the scalar type of matrix entries
      typedef typename Matrix::Scalar Scalar;
      // Get pointers
      if (!this->drObj->projectionMatrix.data()) return -1;
      if (Q->cols() != this->drObj->zcData.cols()) return -2;

      // Get the scalar type of matrix entries
      typedef typename Matrix::Scalar Scalar;

  #ifndef USE_OPENCL
      // We now follow the same logic in building the kernel
      // \hat{y} = ProjectionMatrix' * \sum((P^2 * 1/Q) + ((1/P) * Q^2)) - \sum(P+Q),
      // As we have pre-computed some terms, the outcome is derived by a matrix multiplication of
      // of the new data and the pre-computed terms
      Matrix Ones   = Matrix::Ones(Q->rows(), Q->cols());
      Matrix P      = this->drObj->zcData;
      Matrix Q1     = Q->unaryExpr(std::ptr_fun<Scalar>(EigenUtils::inverse<Scalar>)).transpose();
      Matrix Q2     = Q->unaryExpr(std::ptr_fun<Scalar>(EigenUtils::power2_0<Scalar>)).transpose();
      // Compute the Numerator
      Matrix BNum   = prod1 * Q1 + prod2 * Q2 - prod3 * Ones.transpose() - prod4 * Q->transpose();

      // Compute the denominator
      Matrix BDen   = (d1 * Q1 + d2 * Q2 - d3 * Ones.transpose() - d4 * Q->transpose()).array()+Scalar{1.0e-8};

      //  Generate the values
      (*output) = (BDen.asDiagonal() * BNum).transpose();
  #else
  #endif
      return 1;
    }//end of function

    template class AdditiveSymmetricChiSquaredDivergence<spin::RMMatrix_Float>;
    template class AdditiveSymmetricChiSquaredDivergence<spin::RMMatrix_Double>;
    template class AdditiveSymmetricChiSquaredDivergence<spin::CMMatrix_Float>;
    template class AdditiveSymmetricChiSquaredDivergence<spin::CMMatrix_Double>;
  }//end of namespace GP
}//end of namespace spin


typedef spin::LE::AdditiveSymmetricChiSquaredDivergence<spin::RMMatrix_Float>   ASCDFloat_Row;
typedef spin::LE::AdditiveSymmetricChiSquaredDivergence<spin::RMMatrix_Double>  ASCDDouble_Row;
typedef spin::LE::AdditiveSymmetricChiSquaredDivergence<spin::CMMatrix_Float>   ASCDFloat_Col;
typedef spin::LE::AdditiveSymmetricChiSquaredDivergence<spin::CMMatrix_Double>  ASCDDouble_Col;

//Interface for a plugin. Sadly we have to enumerate all the
//types as C does not recognize templates
extern "C" void* LE_Kernel(int g)
{
  // return the object type
  void* q = NULL;
  if (g == 1) // RMMatrix_Float
  {
    ASCDFloat_Row* x = new ASCDFloat_Row();
    q = (void*)(x);
  }
  else
  if (g == 2)
  {
    ASCDDouble_Row* x = new ASCDDouble_Row();
    q = (void*)(x);
  }
  if (g == 3) // CMMatrix_Float
  {
    ASCDFloat_Col* x = new ASCDFloat_Col();
    q = (void*)(x);
  }
  else
  if (g == 4)
  {
    ASCDDouble_Col* x = new ASCDDouble_Col();
    q = (void*)(x);
  }

  return q;
}//end of function
