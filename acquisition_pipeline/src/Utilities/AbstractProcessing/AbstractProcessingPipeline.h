#ifndef ABSTRACTPROCESSINGPIPELINE_H_
#define ABSTRACTPROCESSINGPIPELINE_H_

// pipeline includes
#include "AbstractClasses/AbstractCallback.h"
// Boost includes
#include <boost/thread.hpp>
#include <boost/timer/timer.hpp>

// A set of forward declarations for current modules
namespace
{
  struct APModule;
}

namespace spin
{
  /*! \brief AbstractProcessingPipeline
    *  This class will load the modules and will hold it in
    *  scope throughout the scope of the program.
    */
  template <class P>
  class AbstractProcessingPipeline
  {
    public:
      // Default destructor
      ~AbstractProcessingPipeline();

      /*! \brief InstantiatePipeline*/
      int InstantiatePipeline(void* obj, void* m);

      /*! \brief ProcessPipeline*/
      int ProcessPipeline(P* dataPoint, void* obj);

      /*! \brief ProcessData*/
      void ProcessData(P dataPoint, void* obj);

      /*! \brief CompleteProcessing*/
      void CompleteProcessing(void* obj);

      /*! \brief AbortPipeline*/
      void AbortPipeline();

      /*! \brief Reimplemented functions */
      void ConsumeFromQueue(P dataPoint, void* obj);

      void RequestQueue(void* obj);

      // Signals for metrics and logging
      spin::CallbackSignal  sig_Logs;
      spin::CallbackSignal  sig_Metrics;
      spin::ModuleSignal    sig_Finished;
      spin::VoidSignal      sig_GetData;

    private :
      // Function call to load the plugin
      int LoadPlugin(void* _q, void* _r);

      // Threads to process the data
      boost::thread t_Process;
      boost::thread t_Pipelineprocess;
      boost::mutex  abort_mutex;
      bool          abort;
      // Instances of modules that will be loaded in this class
      std::shared_ptr<::APModule> module;

      /*! \brief Special functions*/
      void AbortOperations();
      void BeginProcessingQueue(void* obj);
      void RelayLogs(spin::CallbackStruct p);
      void RelayMetrics(spin::CallbackStruct p);
      void RelaySignal(spin::ModuleStatus p);

      spin::ModuleStatus mStatus;
  };//end of class
}//end of namespace spin

#endif
