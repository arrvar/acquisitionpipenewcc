#include "Utilities/AbstractProcessing/spinAbstractProcessingPipeline.h"
#include "Utilities/AbstractProcessing/AbstractProcessingPipeline.h"
#include "Utilities/AbstractProcessing/AbstractProcessingInterface.h"
#include "AbstractClasses/AbstractProcessingObject.h"
#include "Framework/MPC/MPC.h"
#include "Framework/Logger/LoggerInterface.h"

namespace spin
{
  /*! \brief PauseProducer
    *  enables a boolean to either pause or resume the producer
    */
  template <class P>
  void SpinAbstractProcessingPipeline<P>::PauseProducer(bool state)
  {
    pause = state;
  }//end of function

  /*! \brief ConnectSignals
    *  Connects signals and slots for all the members of this class here
    */
  template<class P>
  int SpinAbstractProcessingPipeline<P>::ConnectSignals(void* obj)
  {
    // Connect the corresponding signals and slots
    mMPC.get()->sig_DataAvailable.connect(boost::bind(&AbstractProcessingPipeline<P>::RequestQueue,mAbstractPipeline,obj));
    mMPC.get()->sig_DataPoint.connect(boost::bind(&AbstractProcessingPipeline<P>::ConsumeFromQueue,mAbstractPipeline, _1, _2));
    mMPC.get()->sig_FinAcq.connect(boost::bind(&AbstractProcessingPipeline<P>::CompleteProcessing,mAbstractPipeline,obj));
    mMPC.get()->sig_Abort.connect(boost::bind(&AbstractProcessingPipeline<P>::AbortPipeline,mAbstractPipeline));
    mMPC.get()->sig_QueueBurdened.connect(boost::bind(&SpinAbstractProcessingPipeline<P>::PauseProducer,this, _1));

    mAbstractInterface.get()->sig_AddQ.connect(boost::bind(&MPC<P>::AddToQueue, mMPC, _1));
    mAbstractInterface.get()->sig_FinAcq.connect(boost::bind(&MPC<P>::Finished, mMPC));
    mAbstractInterface.get()->sig_Abort.connect(boost::bind(&MPC<P>::Abort, mMPC));
    mAbstractPipeline.get()->sig_GetData.connect(boost::bind(&MPC<P>::PopFromQueue, mMPC, obj));
    mAbstractPipeline.get()->sig_Finished.connect(boost::bind(&SpinAbstractProcessingPipeline<P>::RelaySignal, this, _1));

    // Typecast the object
    AbstractProcessingObject* gObject = static_cast<AbstractProcessingObject*>(obj);

    // Connect the logging signal
    if (gObject->loggingInitialized)
    {
      mMPC.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
      mAbstractPipeline.get()->sig_Logs.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
      //mAbstractPipeline.get()->sig_Finished.connect(boost::bind(&spin::LoggerInterface::ReceiveMessage, gObject->logger.get(), _1));
    }

    return 0;
  }//end of function

  /*! \brief Destructor
    *  Clears all objects, joins all threads and exits
    */
  template<class P>
  SpinAbstractProcessingPipeline<P>::~SpinAbstractProcessingPipeline()
  {
    CompleteProcessing();
  }//end of function

  /*! \brief RelayLogs
    *  A slot to relay an incoming signal
    */
  template <class P>
  void SpinAbstractProcessingPipeline<P>::RelaySignal(spin::ModuleStatus p)
  {
    sig_Finished(p);
  }//end of function

  /*! \brief InstantiatePipeline
    *  Instantiates the pipeline. Creates the objects and makes all the
    *  connections if necessary. It does not configure any data structure or
    *  set any constants as they would be dependent on the grid that is being
    *  @obj      : The AbstractProcessingObject that has been initialized prior
    *              to calling this function
    */
  template<class P>
  int SpinAbstractProcessingPipeline<P>::InstantiatePipeline(void* obj, void* m)
  {
    // The producer can be either a camera or a File I/O interface
    mAbstractInterface = std::make_shared<AbstractProcessingInterface<P> >();

    // The consumer can be any process that can receive this data structure
    mAbstractPipeline = std::make_shared<AbstractProcessingPipeline<P> >();

    // Both the producer and consumer communicate with each other via a Broker
    mMPC = std::make_shared<MPC<P> >();

    // Typecast the object
    AbstractProcessingObject* gObject = static_cast<AbstractProcessingObject*>(obj);

    // Connect the logging signal
    if (gObject->loggingInitialized)
    {
      mMPC->EnableLogging();
    }

    // Instantiate the pipelines (lightweight in nature)
    mAbstractPipeline->InstantiatePipeline(obj, m);

    // Connect all the signals and slots
    ConnectSignals(obj);

    return 0;
  }//end of function

  /*! \brief CompleteProcessing
    *  Completes the pipeline till the end and cleanly exits
    */
  template<class P>
  int SpinAbstractProcessingPipeline<P>::CompleteProcessing()
  {
    // Send a signal to indicate end of acquisition
    mAbstractInterface.get()->sig_FinAcq();

    return 0;
  }//end of function

  /*! \brief ProcessPipeline
    *  Processes the data that is input from the source. Internally it pushes
    *  the data into an appropriate data structure and then to a queue for
    *  any consumer to pick it up.
    */
  template<class P>
  int SpinAbstractProcessingPipeline<P>::ProcessPipeline(P* producerDataPoint, void* obj)
  {
    // Push this data point into a queue, but check if this is in a paused state
    while(pause)
    {
      boost::this_thread::sleep(boost::posix_time::milliseconds(5));
    }

    // Push this data point into a queue
    mAbstractInterface.get()->AddToQueue(producerDataPoint);

    return 1;
  }//end of function

  /*! \brief AbortPipeline
    *  Aborts all operations and cleanly exits
    */
  template<class P>
  int SpinAbstractProcessingPipeline<P>::AbortPipeline()
  {
    // Send a signal to indicate end of acquisition
    mAbstractInterface.get()->sig_Abort();

    return 0;
  }//end of function

  // explicit instantiation of template class
  template class SpinAbstractProcessingPipeline<vista::ProducerDataType>;
}//end of spin namespace
