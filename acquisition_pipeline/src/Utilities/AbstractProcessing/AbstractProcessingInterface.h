#ifndef ABSTRACTINTERFACE_H_
#define ABSTRACTINTERFACE_H_
// Pipeline includes
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  /*! \brief AbstractInterface
   *  This class is the interface between the image acquisition source and the
   *  preprocessing pipeline framework
   */
  template <class P>
  class AbstractProcessingInterface
  {
    public:
      // Signals
      // Add data point to queue in broker
      boost::signals2::signal<void(P)> sig_AddQ;
      // Finished acquisition
      spin::VoidSignal sig_FinAcq;
      // Save image
      spin::CharSignal sig_SaveImageToDisk;
      // Abort Operations
      spin::VoidSignal sig_Abort;

      // Reimplemented function
      void AddToQueue(void* dataPoint);
  };//end of class
}//end of namespace spin
#endif
