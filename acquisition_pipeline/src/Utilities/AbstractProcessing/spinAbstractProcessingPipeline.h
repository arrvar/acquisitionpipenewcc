#ifndef SPINABSTRACTPROCESSINGPIPELINE_H_
#define SPINABSTRACTPROCESSINGPIPELINE_H_

// Pipeline includes
#include <memory>
#include "AbstractClasses/AbstractCallback.h"

namespace spin
{
  // Forward declaration of Producer Consumer class
  template <class P>
  class MPC;
  // Forward declaration
  template <class P> class AbstractProcessingInterface;
  template <class P> class AbstractProcessingPipeline;

  /*! \brief
    * This is a base class to load all modules as plugins
    * within a producer consumer framework
    */
  template <class P>
  class SpinAbstractProcessingPipeline
  {
    public:
      /*! \brief Destructor*/
      ~SpinAbstractProcessingPipeline();

      /*! \brief InstantiatePipeline*/
      int InstantiatePipeline(void* obj, void* m);

      /*! \brief ProcessPipeline*/
      int ProcessPipeline(P* producerDataPoint, void* obj);

      /*! \brief CompleteProcessing*/
      int CompleteProcessing();

      /*! \brief AbortPipeline*/
      int AbortPipeline();

      // signal to indicate finish of process
      spin::ModuleSignal     sig_Finished;
    protected:
      bool pause;
      // Create objects for the producer level, broker and consumer level
      std::shared_ptr< AbstractProcessingInterface<P> > mAbstractInterface;
      std::shared_ptr< AbstractProcessingPipeline<P> >  mAbstractPipeline;
      std::shared_ptr< spin::MPC<P> > mMPC;

      /*! \brief ConnectSignals*/
      int ConnectSignals(void* obj);
      /*! \brief Pause producer */
      void PauseProducer(bool state);
      /*! \brief RelaySignal*/
      void RelaySignal(spin::ModuleStatus p);
    };
}//end of namespace spin
#endif
