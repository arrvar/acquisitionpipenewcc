// Pipeline includes
#include "Utilities/AbstractProcessing/AbstractProcessingPipeline.h"
#include "AbstractClasses/AbstractProcessingObject.h"
#include "AbstractClasses/AbstractPlugin.h"
#include "AbstractClasses/AbstractModuleObject.h"
#include <dlibxx.hxx>

namespace
{
  struct APModule
  {
    typedef spin::AbstractPlugin APType;
    ~APModule()
    {
      // Check if the library was loaded successfully.
      lib.get()->close();
    }
    std::shared_ptr<dlibxx::handle> lib;
    std::shared_ptr< APType > pModule;
  };//end of struct
}//end of namespace

namespace spin
{
  /*! \brief Destructor */
  template<class P>
  AbstractProcessingPipeline<P>::~AbstractProcessingPipeline()
  {
    // Call the destructor in the sub routines as well
    AbortPipeline();
  }//end of function

  /*! \brief RelayLogs
   *  A slot to relay an incoming signal
   */
  template <class P>
  void AbstractProcessingPipeline<P>::RelayLogs(spin::CallbackStruct p)
  {
    // Push the log
    sig_Logs(p);
  }//end of function

  /*! \brief RelaySignalFinished
   *  A slot to relay an incoming signal
   */
  template <class P>
  void AbstractProcessingPipeline<P>::RelaySignal(spin::ModuleStatus p)
  {
    sig_Finished(p);
  }//end of function

  /*! \brief RelayLogs
   *  A slot to relay an incoming signal
   */
  template <class P>
  void AbstractProcessingPipeline<P>::RelayMetrics(spin::CallbackStruct p)
  {
    // Push the log
    sig_Metrics(p);
  }//end of function

  /*! \brief LoadPlugin
   *  This function is used to load a module which is defined
   *  in the struct, q
   */
  template<class P>
  int AbstractProcessingPipeline<P>::LoadPlugin(void* _q, void* obj)
  {
    spin::ModuleObject* q = static_cast<spin::ModuleObject*>(_q);
    // sanity check
    if (!q) return -1;

    // Frame the module being loaded
#ifndef _MSC_VER
    std::string libname = q->pPath+"/spin_plugin_"+q->pModuleName+".so";
#else
    std::string libname = q->pPath+"/spin_plugin_"+q->pModuleName+".dll";
#endif
    // Check if the library exists in the given path
    boost::filesystem::path dfile(libname.c_str());
    if (!boost::filesystem::exists(dfile))
    {
      return -2; // the file does not exis
    }

    // Instantiate an APModule object
    module = std::make_shared<APModule>();
    // And Instantiate the loader
    module->lib = std::make_shared<dlibxx::handle>();
    // Resolve symbols when they are referenced instead of on load.
    module->lib->resolve_policy(dlibxx::resolve::lazy);
    // Make symbols available for resolution in subsequently loaded libraries.
    module->lib->set_options(dlibxx::options::global | dlibxx::options::no_delete);
    // Load the library specified.
    module->lib->load(libname);
    // Check if the library could be loaded
    if (!module->lib->loaded()) return -3;

    // Now, we try and create the plugin
    module->pModule = module->lib->template create<APModule::APType>("Processing_Plugin");
    // Check if the module was loaded
    if (!module->pModule.get()) return -4;

    // Every module will have a parameter file. We try to parse it to check the efficacy
    // and health of the module.
    int v = module->pModule->InstantiatePipeline(q->pParamFile.c_str(), obj);
    if (v !=1) return -5;

    return 1;
  }//end of function

  /*! \brief InstantiatePipeline
    *  Instantiates the pipeline by loading modules
    */
  template<class P>
  int AbstractProcessingPipeline<P>::InstantiatePipeline(void* obj, void* _m)
  {
    //Check if the module can be loaded
    int v = LoadPlugin(_m, obj);
    if (v !=1) return -1;

    return 1;
  }//end of function

  /*! \brief ProcessPipeline
   * Processes the data that is input from the source. Internally it pushes
   * the data into an appropriate data structure and then to a queue for
   * any consumer to pick it up.
   * parameters :
   * @P: The pointer to a data point from the producer. It contains
   *     an image that will need to be processed
   */
  template<class P>
  int AbstractProcessingPipeline<P>::ProcessPipeline(P* DataPoint, void* obj)
  {
    // Run the module
    int h = module->pModule->ProcessPipeline(DataPoint, NULL, NULL);
    if (h !=1) return -1;

    return 1;
  }//end of function

  /*! \brief ProcessData
   * Chains up the process pipelines that need to be sequenced for the data
   * point
   */
  template <class P>
  void AbstractProcessingPipeline<P>::ProcessData(P dataPoint, void* obj)
  {
    // Process the data point using the pipeline described in
    // ProcessPipeline only if it is a foreground
    if (dataPoint.isForeground)
    {
      ProcessPipeline(&dataPoint, obj);
    }
  }//end of function

  /*! \brief CompleteProcessing [SLOT]
   * This function ensures that processing is finished
   */
  template <class P>
  void AbstractProcessingPipeline<P>::CompleteProcessing(void* obj)
  {
    spin::AbstractProcessingObject* aObj = static_cast<spin::AbstractProcessingObject*>(obj);
    // If the pipeline has to abort then there's no point in forwarding a
    // signal that requests the other consumers to proceed
    abort_mutex.lock();
    if (abort)
    {
      abort_mutex.unlock();
      return;
    }
    abort_mutex.unlock();

    // Complete Processing stuff associated with the plugin
    module->pModule->CompleteProcessing();

    if(t_Process.joinable())
    {
      // Join the thread now
      t_Process.join();

      // Indicate that this module has completed by marking the
      // approrpiate location in the module registry
      aObj->mModuleStatus[mStatus.Module] = true;
      // And Send a completion signal to the calling thread
      sig_Finished(mStatus);
    }
    return;
  }//end of function

  /*! \brief BeginProcessingQueue [SLOT]
   * This function Sends a request to the queue in the broker for data
   */
  template <class P>
  void AbstractProcessingPipeline<P>::BeginProcessingQueue(void* obj)
  {
    // downcast the object
    AbstractProcessingObject* gObject = static_cast<AbstractProcessingObject*>(obj);

    // This function is moved into another thread
    int n_images = 0;
    // Get the number of images that need to be processed
    int max_images = gObject->max_images;

    while( (n_images < max_images) )
    {
      // If the abort signal was called, exit the loop
      abort_mutex.lock();
      if (abort)
      {
        abort_mutex.unlock();
        break;
      }
      abort_mutex.unlock();

      if(t_Pipelineprocess.joinable())
      {
        t_Pipelineprocess.join();
        ++n_images;
      }
      else
      {
        sig_GetData();
        boost::this_thread::sleep(boost::posix_time::milliseconds(5));
      }
    }
  }//end of function

  /*! \brief ConsumeFromQueue [SLOT]
   *  This function receives a signal from the MPC with a new data point
   */
  template <class P>
  void AbstractProcessingPipeline<P>::ConsumeFromQueue(P dataPoint, void* obj)
  {
    // Process the data point in a separate thread
    t_Pipelineprocess = boost::thread(&AbstractProcessingPipeline<P>::ProcessData,\
                                      this,dataPoint,obj);
    dataPoint.Clear();
  }//end of function

  /*! \brief RequestQueue [SLOT]
   *  This function receives a signal from the MPC that a new datapoint is
   *  available for consumption
   */
  template <class P>
  void AbstractProcessingPipeline<P>::RequestQueue(void* obj)
  {
    if(!t_Process.joinable())
    {
      t_Process = boost::thread(&AbstractProcessingPipeline<P>::BeginProcessingQueue,this,obj);
    }
  }//end of function

  /*! \brief AbortPipeline
    *  Aborts the pipelines and cleanly exits
    */
  template <class P>
  void AbstractProcessingPipeline<P>::AbortPipeline()
  {
    abort_mutex.lock();
    abort  = true;
    abort_mutex.unlock();
    AbortOperations();
  }//end of function

  /*! \brief AbortOperations
    *	This function forwards the abort signal to other consumers and
    *	deallocates any memory objects that are part of this class alone.
    *	This however does not destroy the configuration for which this
    *  pipeline was primed with. Thereby enabling reacquisition instantly.
    */
  template <class P>
  void AbstractProcessingPipeline<P>::AbortOperations()
  {

  }//end of function

  // explicit instantiation of template class
  template class AbstractProcessingPipeline<vista::ProducerDataType>;
}//end of namespace spin
