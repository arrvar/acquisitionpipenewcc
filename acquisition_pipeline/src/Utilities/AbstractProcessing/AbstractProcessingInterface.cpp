#include "Utilities/AbstractProcessing/AbstractProcessingInterface.h"
#include "AbstractClasses/AbstractDataStructures.h"

namespace spin
{
  /*! \brief AddToQueue
    *  This function adds an object to the Pre-processing queue
    */
  template<class P>
  void AbstractProcessingInterface<P>::AddToQueue(void* dataPoint)
  {
    // typecasts
    vista::ProducerDataType* dp = static_cast<vista::ProducerDataType*>(dataPoint);
    //emit the signal
    sig_AddQ(*dp);
    dp->Clear();
  }//end of function

  // explicit instantiation of template class
  template class AbstractProcessingInterface<vista::ProducerDataType>;
}//end of spin namespace
