#ifndef RBCMASKGENERATION_H
#define RBCMASKGENERATION_H

#include <memory>
#include "AbstractClasses/AbstractMaskGeneration.h"

namespace spin
{ 
  /*! \brief RBCMaskGeneration 
   *  This class encapsulates a pipeline to generate a mask 
   *  from RGB images
   */
  class RBCMaskGeneration : public AbstractMaskGeneration
  {
    public:
      /*! \brief Default constructor */
      RBCMaskGeneration(){}
      
      /*! \brief Default destructor */
      ~RBCMaskGeneration(){}
      
      /*! \brief Run Pipeline */
      int RunPipeline(void* _imageLeft, \
                      void* _imageRight, \
                      void* _params, \
                      void* _out, \
                      std::string _imgName, \
                      std::string outFldr, \
                      std::string outExtn, \
                      bool saveMask);
  };//end of class
}
#endif
