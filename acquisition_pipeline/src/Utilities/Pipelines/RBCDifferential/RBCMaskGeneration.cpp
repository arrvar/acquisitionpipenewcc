#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include "Utilities/ImageProcessing/ColorSpace/itkRGBToLabColorSpacePixelAccessor.h"
#include "Utilities/ImageProcessing/ColorSpace/itkRGBToRGBColorSpacePixelAccessor.h"
#include "Utilities/Pipelines/RBCDifferential/RBCMaskGeneration.h"

// ITK image utils
#include "itkImageAdaptor.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkMaximumEntropyThresholdImageFilter.h"
#include "itkTriangleThresholdImageFilter.h"
#include "itkLiThresholdImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"
#include "itkMedianImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkVectorIndexSelectionCastImageFilter.h"
#include "itkBinaryMorphologicalOpeningImageFilter.h"
#include "itkBinaryMorphologicalClosingImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryImageToShapeLabelMapFilter.h"
#include "itkShapeOpeningLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

namespace spin
{
  namespace
  {
    // A list of all typedefs
    typedef spin::UCharImageType2D UCharImageType2D;
    typedef spin::FloatImageType2D FloatImageType;
    typedef spin::RGBPixelType RGBPixelType;
    typedef spin::RGBImageType RGBImageType;
    typedef itk::Vector<float, 3> VectorPixelType;

    typedef itk::Accessor::RGBToLabColorSpacePixelAccessor<unsigned char, float> RGBToLabColorSpaceAccessorType;
    typedef itk::ImageAdaptor<RGBImageType, RGBToLabColorSpaceAccessorType> RGBToLabAdaptorType;
    typedef itk::Accessor::RGBToRGBColorSpacePixelAccessor<unsigned char, float> RGBToRGBColorSpaceAccessorType;
    typedef itk::ImageAdaptor<RGBImageType, RGBToRGBColorSpaceAccessorType> RGBToRGBAdaptorType;

    typedef itk::RescaleIntensityImageFilter<FloatImageType, UCharImageType2D> FloatRescalerType;
    typedef itk::BinaryBallStructuringElement<float, 2> StructuringElementType;
    typedef itk::BinaryImageToShapeLabelMapFilter<UCharImageType2D> BinaryImageToShapeLabelMapFilterType;
    typedef itk::ShapeOpeningLabelMapFilter< BinaryImageToShapeLabelMapFilterType::OutputImageType > ShapeOpeningLabelMapFilterType;
    typedef itk::LabelMapToLabelImageFilter<BinaryImageToShapeLabelMapFilterType::OutputImageType, UCharImageType2D> LabelMapToLabelImageFilterType;

    typedef itk::MinimumMaximumImageCalculator <UCharImageType2D> ImageCalculatorFilterType;
    typedef itk::BinaryThresholdImageFilter <UCharImageType2D,UCharImageType2D> BinaryThresholdImageFilterType;

    typedef itk::BinaryBallStructuringElement<unsigned char, 2> StructuringElementTypeB;
    typedef itk::BinaryMorphologicalOpeningImageFilter<UCharImageType2D, UCharImageType2D, StructuringElementTypeB >  OpeningFilterTypeB;
    typedef itk::BinaryMorphologicalClosingImageFilter<UCharImageType2D, UCharImageType2D, StructuringElementTypeB >  ClosingFilterTypeB;
    typedef itk::MedianImageFilter<UCharImageType2D, UCharImageType2D > MedianFilterType;

    typedef itk::MaximumEntropyThresholdImageFilter<FloatImageType, UCharImageType2D> MaxEntThresholdFilterType;
    typedef itk::TriangleThresholdImageFilter<FloatImageType, UCharImageType2D> TriThresholdFilterType;
    typedef itk::OtsuThresholdImageFilter<FloatImageType, UCharImageType2D> OtsuThresholdFilterType;
    typedef itk::LiThresholdImageFilter<FloatImageType, UCharImageType2D> LiThresholdFilterType;
  }

  /*! \brief RBCMaskPostProcessing
   *  This function is used to post-process a RBC mask
   */
  void RBCMaskPostProcessing( UCharImageType2D::Pointer img, \
                              spin::hemocalc::RBCDifferentialParams* params)
  {
    BinaryImageToShapeLabelMapFilterType::Pointer binaryImageToShapeLabelMapFilter  = BinaryImageToShapeLabelMapFilterType::New();
    ShapeOpeningLabelMapFilterType::Pointer shapeOpeningLabelMapFilter              = ShapeOpeningLabelMapFilterType::New();
    LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter              = LabelMapToLabelImageFilterType::New();
    BinaryThresholdImageFilterType::Pointer thresholdRBC                            = BinaryThresholdImageFilterType::New();
    MedianFilterType::Pointer medianFilter                                          = MedianFilterType::New();

    // Use a size threshold to remove blobs that are not within
    // certain size limits
    binaryImageToShapeLabelMapFilter->SetInput(img);
    shapeOpeningLabelMapFilter->SetInput( binaryImageToShapeLabelMapFilter->GetOutput() );
    shapeOpeningLabelMapFilter->SetLambda( params->rbc_min_size );
    shapeOpeningLabelMapFilter->SetAttribute( ShapeOpeningLabelMapFilterType::LabelObjectType::NUMBER_OF_PIXELS);
    labelMapToLabelImageFilter->SetInput(shapeOpeningLabelMapFilter->GetOutput());
    thresholdRBC->SetInput(labelMapToLabelImageFilter->GetOutput());
    thresholdRBC->SetLowerThreshold(1);
    thresholdRBC->SetInsideValue(255);
    thresholdRBC->SetOutsideValue(0);
    thresholdRBC->Update();

    binaryImageToShapeLabelMapFilter->SetInput(thresholdRBC->GetOutput());
    shapeOpeningLabelMapFilter->SetInput( binaryImageToShapeLabelMapFilter->GetOutput() );
    shapeOpeningLabelMapFilter->SetLambda( params->rbc_max_size );
    shapeOpeningLabelMapFilter->SetAttribute( ShapeOpeningLabelMapFilterType::LabelObjectType::NUMBER_OF_PIXELS);
    shapeOpeningLabelMapFilter->ReverseOrderingOn();
    labelMapToLabelImageFilter->SetInput(shapeOpeningLabelMapFilter->GetOutput());
    thresholdRBC->SetInput(labelMapToLabelImageFilter->GetOutput());
    thresholdRBC->SetLowerThreshold(1);
    thresholdRBC->SetInsideValue(255);
    thresholdRBC->SetOutsideValue(0);
    thresholdRBC->Update();

    // Apply median filtering on the image
    MedianFilterType::InputSizeType radius;
    radius.Fill(params->rbc_postprocess_medianFilter);
    medianFilter->SetRadius(radius);
    medianFilter->SetInput( thresholdRBC->GetOutput() );
    medianFilter->Update();

    // and copy the image
    img->Graft(thresholdRBC->GetOutput());
  }//end of function

  /*! \brief PrimaryThreshold
   *  This function is used to split the RGB image into a different color space and extract
   *  a specific channel
   */
  template <class Adapter, class ThreshType>
  int PrimaryThreshold(void* img, UCharImageType2D::Pointer mask, spin::hemocalc::RBCDifferentialParams* params)
  {
    typename RGBImageType::Pointer* mInpImage = static_cast<RGBImageType::Pointer*>(img);
    if (!mInpImage) return -1;

    typedef itk::VectorIndexSelectionCastImageFilter<Adapter, FloatImageType> VectorCastFilterType;
    typename Adapter::Pointer adapter                         = Adapter::New();
    typename VectorCastFilterType::Pointer vectorCastFilter   = VectorCastFilterType::New();
    typename ThreshType::Pointer threshold                    = ThreshType::New();

    /////////////////////////////////////////////////////////////////////////////
    // Convert RGB image to Lab color space and work on the a-channel
    adapter->SetImage(*mInpImage);
    vectorCastFilter->SetInput(adapter);
    vectorCastFilter->SetIndex(params->channel);

    /////////////////////////////////////////////////////////////////////////////
    // Apply thresholding on the morphologically enhanced image
    threshold->SetInput(vectorCastFilter->GetOutput());
    threshold->SetInsideValue( 0 );
    threshold->SetOutsideValue( 255 );
    threshold->SetNumberOfHistogramBins( 256 );
    threshold->Update();

    //Copy the output to mask
    mask->Graft(threshold->GetOutput());

    return 1;
  }//end of function

  /*! \brief RBCMaskGeneration
   *  This is the pipeline used for generating a rbc mask.
   *  @_imageLeft       : The undistorted Left image
   *  @_imageRight      : We do not use this image (in this pipeline)
   *  @_params          : The parameters associated with this pipeline
   *  @_out             : A std::map containing two binary images (WBC and platelet mask)
   *  @outFldr          : The output folder that will containg results
   *  @saveMask         : Do we want to save intermediate masks?
   */
  int RBCMaskGeneration::RunPipeline( void* _imageLeft, \
                                      void* _imageRight, \
                                      void* _params, \
                                      void* _out, \
                                      std::string _imgName, \
                                      std::string outFldr, \
                                      std::string outExtn, \
                                      bool saveMask)
  {
    RGBImageType::Pointer* mInpImage = static_cast<RGBImageType::Pointer*>(_imageLeft);
    if (!mInpImage) return -1;

    spin::hemocalc::RBCDifferentialParams* params = \
    static_cast<spin::hemocalc::RBCDifferentialParams*>(_params);
    if (!params) return -2;

    std::map<int, UCharImageType2D::Pointer>* masks =
    static_cast< std::map<int, UCharImageType2D::Pointer>* >(_out);
    if (!masks) return -3;

    // Create the output folder depending on the image being processed
    std::vector<std::string> temp;
    boost::split(temp, _imgName, boost::is_any_of("/"));
    // From the image name we will derive the folder name
    std::string imgName = temp[temp.size()-1];
    temp.clear();
    boost::split(temp, imgName, boost::is_any_of("."));
    // It is assumed that the folder should have been created prior to calling
    // this function. If not, we exit from this function
    std::string outFolder = outFldr+"/"+temp[0];
    if (!boost::filesystem::exists(outFolder))
    {
      return -4;
    }
    /////////////////////////////////////////////////////////////////////////////
    // Instantiate all smart pointers
    FloatRescalerType::Pointer floatRescaler        = FloatRescalerType::New();
    BinaryImageToShapeLabelMapFilterType::Pointer binaryImageToShapeLabelMapFilter \
                                                    = BinaryImageToShapeLabelMapFilterType::New();
    ShapeOpeningLabelMapFilterType::Pointer shapeOpeningLabelMapFilter \
                                                    = ShapeOpeningLabelMapFilterType::New();
    LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter \
                                                    = LabelMapToLabelImageFilterType::New();
    BinaryThresholdImageFilterType::Pointer thresholdWBC \
                                                    = BinaryThresholdImageFilterType::New();
    ImageCalculatorFilterType::Pointer minmax = ImageCalculatorFilterType::New();

    ///////////////////////////////////////////////////////////////////////////
    // Apply thresholding on the RGB Image and extract a preliminary mask
    UCharImageType2D::Pointer gMask = UCharImageType2D::New();
    if (params->colorSpace == 0) // RGB color space
    {
      if (params->thresholdAlgorithm == 0)//Otsu
      {
        PrimaryThreshold<RGBToRGBAdaptorType,OtsuThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 1)//Max-entropy
      {
        PrimaryThreshold<RGBToRGBAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 2)//Triangle
      {
        PrimaryThreshold<RGBToRGBAdaptorType,TriThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 3)//Li
      {
        PrimaryThreshold<RGBToRGBAdaptorType,LiThresholdFilterType>(mInpImage, gMask, params);
      }
      else // Default is max-entropy
      {
        PrimaryThreshold<RGBToRGBAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
    }
    else
    if (params->colorSpace == 1) // Lab color space
    {
      if (params->thresholdAlgorithm == 0)//Otsu
      {
        PrimaryThreshold<RGBToLabAdaptorType,OtsuThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 1)//Max-entropy
      {
        PrimaryThreshold<RGBToLabAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 2)//Triangle
      {
        PrimaryThreshold<RGBToLabAdaptorType,TriThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 3)//Li
      {
        PrimaryThreshold<RGBToLabAdaptorType,LiThresholdFilterType>(mInpImage, gMask, params);
      }
      else // Default is max-entropy
      {
        PrimaryThreshold<RGBToLabAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
    }
    else // default is Lab color space
    {
      if (params->thresholdAlgorithm == 0)//Otsu
      {
        PrimaryThreshold<RGBToLabAdaptorType,OtsuThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 1)//Max-entropy
      {
        PrimaryThreshold<RGBToLabAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 2)//Triangle
      {
        PrimaryThreshold<RGBToLabAdaptorType,TriThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 3)//Li
      {
        PrimaryThreshold<RGBToLabAdaptorType,LiThresholdFilterType>(mInpImage, gMask, params);
      }
      else // Default is max-entropy
      {
        PrimaryThreshold<RGBToLabAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
    }
    //TODO: add other color spaces

    // Check if there are any connected components in the image.
    minmax->SetImage(gMask);
    minmax->Compute();

    // If no RBC's are found we do not have to do any recognition (or if requested
    // save the mask images)
    if (minmax->GetMaximum() > 0)
    {
      // WBC's are present in the image. We do some post-processing
      // and clean up spurious blobs
      (*masks)[1] = UCharImageType2D::New();
      (*masks)[1]->SetRegions((*mInpImage)->GetLargestPossibleRegion());
      (*masks)[1]->Allocate();
      (*masks)[1]->Graft(gMask);
      RBCMaskPostProcessing((*masks)[1], params);
      // Now, check if we need to save the WBC mask image
      if (saveMask)
      {
        std::string rbcMask = outFolder+"/RBC_"+temp[0]+outExtn;
        spin::WriteITKImage<spin::UCharImageType2D>(rbcMask, (*masks)[1]);
      }
    }

    temp.clear();
    return 1;
  }//end of function
}//end of namespace
