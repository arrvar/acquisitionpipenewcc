#ifndef WBCPLATELETMASKGENERATION_H
#define WBCPLATELETMASKGENERATION_H

#include <memory>
#include "AbstractClasses/AbstractMaskGeneration.h"

namespace spin
{ 
  /*! \brief WBCPlateletMaskGeneration 
   *  This class encapsulates a pipeline to generate a mask 
   *  from RGB images
   */
  class WBCPlateletMaskGeneration : public AbstractMaskGeneration
  {
    public:
      /*! \brief Default constructor */
      WBCPlateletMaskGeneration(){}
      
      /*! \brief Default destructor */
      ~WBCPlateletMaskGeneration(){}
      
      /*! \brief Run Pipeline */
      int RunPipeline(void* _imageLeft, \
                      void* _imageRight, \
                      void* _params, \
                      void* _out, \
                      std::string _imgName, \
                      std::string outFldr, \
                      std::string outExtn, \
                      bool saveMask);
  };//end of class
}
#endif
