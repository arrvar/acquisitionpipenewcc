#include "Libraries/hemocalc/Pipeline/Initialize/spinHemocalcObject.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include "Utilities/ImageProcessing/ColorSpace/itkRGBToLabColorSpacePixelAccessor.h"
#include "Utilities/ImageProcessing/ColorSpace/itkRGBToRGBColorSpacePixelAccessor.h"
#include "Utilities/Pipelines/WBCDifferential/WBCPlateletMaskGeneration.h"

// ITK image utils
#include "itkImageAdaptor.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkMaximumEntropyThresholdImageFilter.h"
#include "itkTriangleThresholdImageFilter.h"
#include "itkLiThresholdImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"
#include "itkMedianImageFilter.h"
#include "itkSubtractImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkVectorIndexSelectionCastImageFilter.h"
#include "itkGrayscaleMorphologicalOpeningImageFilter.h"
#include "itkGrayscaleMorphologicalClosingImageFilter.h"
#include "itkGrayscaleDilateImageFilter.h"
#include "itkGrayscaleErodeImageFilter.h"
#include "itkBinaryMorphologicalOpeningImageFilter.h"
#include "itkBinaryMorphologicalClosingImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryImageToShapeLabelMapFilter.h"
#include "itkShapeOpeningLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkBinaryFillholeImageFilter.h"
#include "itkBinaryDilateImageFilter.h"
#include "itkGrayscaleDilateImageFilter.h"
#include "itkBinaryContourImageFilter.h"
#include "itkDiscreteGaussianImageFilter.h"
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

namespace spin
{
  namespace
  {
    // A list of all typedefs
    typedef spin::UCharImageType2D UCharImageType2D;
    typedef spin::FloatImageType2D FloatImageType;
    typedef spin::RGBPixelType RGBPixelType;
    typedef spin::RGBImageType RGBImageType;
    typedef itk::Vector<float, 3> VectorPixelType;

    typedef itk::Accessor::RGBToLabColorSpacePixelAccessor<unsigned char, float> RGBToLabColorSpaceAccessorType;
    typedef itk::ImageAdaptor<RGBImageType, RGBToLabColorSpaceAccessorType> RGBToLabAdaptorType;
    typedef itk::Accessor::RGBToRGBColorSpacePixelAccessor<unsigned char, float> RGBToRGBColorSpaceAccessorType;
    typedef itk::ImageAdaptor<RGBImageType, RGBToRGBColorSpaceAccessorType> RGBToRGBAdaptorType;

    typedef itk::DiscreteGaussianImageFilter<FloatImageType, FloatImageType> GaussianFilterType;
    typedef itk::RescaleIntensityImageFilter<FloatImageType, UCharImageType2D> FloatRescalerType;
    typedef itk::BinaryBallStructuringElement<float, 2> StructuringElementType;
    typedef itk::GrayscaleMorphologicalOpeningImageFilter<FloatImageType, FloatImageType, StructuringElementType >  OpeningFilterType;
    typedef itk::GrayscaleMorphologicalClosingImageFilter<FloatImageType, FloatImageType, StructuringElementType >  ClosingFilterType;
    typedef itk::GrayscaleDilateImageFilter<FloatImageType, FloatImageType, StructuringElementType >  DilateImageFilterType;
    typedef itk::GrayscaleErodeImageFilter<FloatImageType, FloatImageType, StructuringElementType >  ErodeImageFilterType;
    typedef itk::BinaryImageToShapeLabelMapFilter<UCharImageType2D> BinaryImageToShapeLabelMapFilterType;
    typedef itk::ShapeOpeningLabelMapFilter< BinaryImageToShapeLabelMapFilterType::OutputImageType > ShapeOpeningLabelMapFilterType;
    typedef itk::LabelMapToLabelImageFilter<BinaryImageToShapeLabelMapFilterType::OutputImageType, UCharImageType2D> LabelMapToLabelImageFilterType;

    typedef itk::MinimumMaximumImageCalculator <UCharImageType2D> ImageCalculatorFilterType;
    typedef itk::BinaryThresholdImageFilter <UCharImageType2D,UCharImageType2D> BinaryThresholdImageFilterType;
    typedef itk::BinaryFillholeImageFilter< UCharImageType2D > HoleFilterType;

    typedef itk::BinaryBallStructuringElement<unsigned char, 2> StructuringElementTypeB;
    typedef itk::BinaryMorphologicalOpeningImageFilter<UCharImageType2D, UCharImageType2D, StructuringElementTypeB >  OpeningFilterTypeB;
    typedef itk::BinaryMorphologicalClosingImageFilter<UCharImageType2D, UCharImageType2D, StructuringElementTypeB >  ClosingFilterTypeB;
    typedef itk::SubtractImageFilter <UCharImageType2D, UCharImageType2D > SubtractImageFilterType;
    typedef itk::MedianImageFilter<UCharImageType2D, UCharImageType2D > MedianFilterType;
    typedef itk::BinaryContourImageFilter <UCharImageType2D, UCharImageType2D > BinaryContourImageFilterType;
    typedef itk::BinaryDilateImageFilter <UCharImageType2D, UCharImageType2D, StructuringElementTypeB> BinaryDilateImageFilterType;

    typedef itk::MaximumEntropyThresholdImageFilter<FloatImageType, UCharImageType2D> MaxEntThresholdFilterType;
    typedef itk::TriangleThresholdImageFilter<FloatImageType, UCharImageType2D> TriThresholdFilterType;
    typedef itk::OtsuThresholdImageFilter<FloatImageType, UCharImageType2D> OtsuThresholdFilterType;
    typedef itk::LiThresholdImageFilter<FloatImageType, UCharImageType2D> LiThresholdFilterType;
  }

  /*! \brief PlateletMaskPostProcessing
   *  This function is used to post-process a WBC mask
   */
  void PlateletMaskPostProcessing(UCharImageType2D::Pointer img, \
                                  spin::hemocalc::WBCDifferentialParams* params)
  {
    BinaryImageToShapeLabelMapFilterType::Pointer binaryImageToShapeLabelMapFilter = BinaryImageToShapeLabelMapFilterType::New();
    ShapeOpeningLabelMapFilterType::Pointer shapeOpeningLabelMapFilter = ShapeOpeningLabelMapFilterType::New();
    LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
    BinaryThresholdImageFilterType::Pointer thresholdPlatelet = BinaryThresholdImageFilterType::New();
    MedianFilterType::Pointer medianFilter = MedianFilterType::New();

    // Remove objects that are larger than a certain size
    binaryImageToShapeLabelMapFilter->SetInput(img);
    shapeOpeningLabelMapFilter->SetInput( binaryImageToShapeLabelMapFilter->GetOutput() );
    shapeOpeningLabelMapFilter->SetLambda( params->platelet_max_size );
    shapeOpeningLabelMapFilter->SetAttribute( ShapeOpeningLabelMapFilterType::LabelObjectType::NUMBER_OF_PIXELS);
    //shapeOpeningLabelMapFilter->ReverseOrderingOn();
    labelMapToLabelImageFilter->SetInput(shapeOpeningLabelMapFilter->GetOutput());
    labelMapToLabelImageFilter->Update();
    thresholdPlatelet->SetInput(labelMapToLabelImageFilter->GetOutput());
    thresholdPlatelet->SetLowerThreshold(1);
    thresholdPlatelet->SetInsideValue(255);
    thresholdPlatelet->SetOutsideValue(0);
    thresholdPlatelet->Update();

    // Remove objects that do not satisfy a circularity constraint
    binaryImageToShapeLabelMapFilter->SetInput(thresholdPlatelet->GetOutput());
    shapeOpeningLabelMapFilter->SetInput( binaryImageToShapeLabelMapFilter->GetOutput() );
    shapeOpeningLabelMapFilter->SetLambda( params->platelet_eccentricity );
    shapeOpeningLabelMapFilter->SetAttribute( ShapeOpeningLabelMapFilterType::LabelObjectType::ELONGATION);
    shapeOpeningLabelMapFilter->ReverseOrderingOn();
    labelMapToLabelImageFilter->SetInput(shapeOpeningLabelMapFilter->GetOutput());
    thresholdPlatelet->SetInput(labelMapToLabelImageFilter->GetOutput());
    thresholdPlatelet->SetLowerThreshold(1);
    thresholdPlatelet->SetInsideValue(255);
    thresholdPlatelet->SetOutsideValue(0);
    thresholdPlatelet->Update();

    // Apply median filtering on the image
    MedianFilterType::InputSizeType radius;
    radius.Fill(params->platelet_postprocess_medianFilter); // change this value if used besides 100x magnification
    medianFilter->SetRadius(radius);
    medianFilter->SetInput( thresholdPlatelet->GetOutput() );
    medianFilter->Update();
    img->Graft(medianFilter->GetOutput());
  }//end of function

  /*! \brief WBCMaskPostProcessing
   *  This function is used to post-process a WBC mask
   */
  void WBCMaskPostProcessing( UCharImageType2D::Pointer img, \
                              spin::hemocalc::WBCDifferentialParams* params)
  {
    BinaryImageToShapeLabelMapFilterType::Pointer binaryImageToShapeLabelMapFilter  = BinaryImageToShapeLabelMapFilterType::New();
    ShapeOpeningLabelMapFilterType::Pointer shapeOpeningLabelMapFilter              = ShapeOpeningLabelMapFilterType::New();
    LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter              = LabelMapToLabelImageFilterType::New();
    BinaryThresholdImageFilterType::Pointer thresholdWBC                            = BinaryThresholdImageFilterType::New();
    MedianFilterType::Pointer medianFilter                                          = MedianFilterType::New();
    OpeningFilterTypeB::Pointer  opening                                            = OpeningFilterTypeB::New();
    ClosingFilterTypeB::Pointer  closing                                            = ClosingFilterTypeB::New();
    HoleFilterType::Pointer hfilter                                                 = HoleFilterType::New();

    /////////////////////////////////////////////////////////////////////////////
    // Create a structuring element
    StructuringElementTypeB  structuringElement;
    structuringElement.SetRadius(params->wbc_postprocess_morphology);
    structuringElement.CreateStructuringElement();

    ////////////////////////////////////////////////////////////////////////////
    //Fill up holes
    hfilter->SetInput( img );
    hfilter->SetForegroundValue( 255);
    //Run a morphological enhancement of the image
    opening->SetKernel(  structuringElement );
    closing->SetKernel(  structuringElement );
    closing->SetInput(hfilter->GetOutput());
    opening->SetInput(closing->GetOutput());
    // Finally we use a size threshold to remove blobs that are not within
    // a certain size
    binaryImageToShapeLabelMapFilter->SetInput(opening->GetOutput());
    shapeOpeningLabelMapFilter->SetInput( binaryImageToShapeLabelMapFilter->GetOutput() );
    shapeOpeningLabelMapFilter->SetLambda( params->wbc_min_size );
    shapeOpeningLabelMapFilter->SetAttribute( ShapeOpeningLabelMapFilterType::LabelObjectType::NUMBER_OF_PIXELS);
    labelMapToLabelImageFilter->SetInput(shapeOpeningLabelMapFilter->GetOutput());
    thresholdWBC->SetInput(labelMapToLabelImageFilter->GetOutput());
    thresholdWBC->SetLowerThreshold(1);
    thresholdWBC->SetInsideValue(255);
    thresholdWBC->SetOutsideValue(0);
    thresholdWBC->Update();

    binaryImageToShapeLabelMapFilter->SetInput(thresholdWBC->GetOutput());
    shapeOpeningLabelMapFilter->SetInput( binaryImageToShapeLabelMapFilter->GetOutput() );
    shapeOpeningLabelMapFilter->SetLambda( params->wbc_max_size );
    shapeOpeningLabelMapFilter->SetAttribute( ShapeOpeningLabelMapFilterType::LabelObjectType::NUMBER_OF_PIXELS);
    shapeOpeningLabelMapFilter->ReverseOrderingOn();
    labelMapToLabelImageFilter->SetInput(shapeOpeningLabelMapFilter->GetOutput());
    thresholdWBC->SetInput(labelMapToLabelImageFilter->GetOutput());
    thresholdWBC->SetLowerThreshold(1);
    thresholdWBC->SetInsideValue(255);
    thresholdWBC->SetOutsideValue(0);
    thresholdWBC->Update();

    // Apply median filtering on the image
    MedianFilterType::InputSizeType radius;
    radius.Fill(params->wbc_postprocess_medianFilter);
    medianFilter->SetRadius(radius);
    medianFilter->SetInput( thresholdWBC->GetOutput() );
    medianFilter->Update();

    // and copy the image
    img->Graft(medianFilter->GetOutput());
  }//end of function

  /*! \brief PrimaryThreshold
   *  This function is used to split the RGB image into a different color space and extract
   *  a specific channel
   */
  template <class Adapter, class ThreshType>
  int PrimaryThreshold(void* img, UCharImageType2D::Pointer mask, spin::hemocalc::WBCDifferentialParams* params)
  {
    typename RGBImageType::Pointer* mInpImage = static_cast<RGBImageType::Pointer*>(img);
    if (!mInpImage) return -1;

    typedef itk::VectorIndexSelectionCastImageFilter<Adapter, FloatImageType> VectorCastFilterType;
    typename Adapter::Pointer adapter                         = Adapter::New();
    typename VectorCastFilterType::Pointer vectorCastFilter   = VectorCastFilterType::New();
    typename ThreshType::Pointer threshold                    = ThreshType::New();
    typename OpeningFilterType::Pointer  opening              = OpeningFilterType::New();
    typename ClosingFilterType::Pointer  closing              = ClosingFilterType::New();
    typename GaussianFilterType::Pointer gaussianPre          = GaussianFilterType::New();
    typename GaussianFilterType::Pointer gaussianPost         = GaussianFilterType::New();
    typename DilateImageFilterType::Pointer dilation          = DilateImageFilterType::New();

    /////////////////////////////////////////////////////////////////////////////
    // Create a structuring element. The larger the radius of the structuring
    // element, the more chances are that platelets will be missed. At 100x oil,
    // we keep the default size to 5;
    StructuringElementType  structuringElement;
    structuringElement.SetRadius( params->strel_preprocess );
    structuringElement.CreateStructuringElement();

    // Setup the opening and closing methods
    opening->SetKernel(  structuringElement );
    closing->SetKernel(  structuringElement );
    dilation->SetKernel( structuringElement);

    /////////////////////////////////////////////////////////////////////////////
    // Convert RGB image to Lab color space and work on the a-channel
    adapter->SetImage(*mInpImage);
    vectorCastFilter->SetInput(adapter);
    vectorCastFilter->SetIndex(params->channel);

    /////////////////////////////////////////////////////////////////////////////
    // Apply Gaussian smoothing on the image
    gaussianPre->SetVariance( params->sigma );
    gaussianPre->SetInput(vectorCastFilter->GetOutput());
    dilation->SetInput(gaussianPre->GetOutput());
    gaussianPost->SetVariance( params->sigma );
    gaussianPost->SetInput(dilation->GetOutput());

    /////////////////////////////////////////////////////////////////////////////
    // Apply morphological enhancement
    //closing->SetInput(vectorCastFilter->GetOutput());
    //opening->SetInput(closing->GetOutput());

    /////////////////////////////////////////////////////////////////////////////
    // Apply thresholding on the morphologically enhanced image
    threshold->SetInput(gaussianPost->GetOutput());
    threshold->SetInsideValue( 0 );
    threshold->SetOutsideValue( 255 );
    threshold->SetNumberOfHistogramBins( 256 );
    threshold->Update();

    //Copy the output to mask
    mask->Graft(threshold->GetOutput());

    return 1;
  }//end of function

  /*! \brief WBCPlateletMaskGeneration
   *  This is the pipeline used for generating a wbc mask, along with any platelets
   *  that may be present in the image. The platelets are processed in a different
   *  pipeline. At the end of the pipline the wbc (nuclei, cytoplasm also if
   *  eosinophils are present) along with platelets are masked out and returned to
   *  the calling pipeline.
   *  @_imageLeft       : The undistorted Left image
   *  @_imageRight      : We do not use this image (in this pipeline)
   *  @_params          : The parameters associated with this pipeline
   *  @_out             : A std::map containing two binary images (WBC and platelet mask)
   *  @outFldr          : The output folder that will containg results
   *  @saveMask         : Do we want to save intermediate masks?
   */
  int WBCPlateletMaskGeneration::RunPipeline( void* _imageLeft, \
                                              void* _imageRight, \
                                              void* _params, \
                                              void* _out, \
                                              std::string _imgName, \
                                              std::string outFldr, \
                                              std::string outExtn, \
                                              bool saveMask)
  {
    RGBImageType::Pointer* mInpImage = static_cast<RGBImageType::Pointer*>(_imageLeft);
    if (!mInpImage) return -1;

    spin::hemocalc::WBCDifferentialParams* params = \
    static_cast<spin::hemocalc::WBCDifferentialParams*>(_params);
    if (!params) return -2;

    std::map<int, UCharImageType2D::Pointer>* masks =
    static_cast< std::map<int, UCharImageType2D::Pointer>* >(_out);
    if (!masks) return -3;

    // Create the output folder depending on the image being processed
    std::vector<std::string> temp;
    boost::split(temp, _imgName, boost::is_any_of("/"));
    // From the image name we will derive the folder name
    std::string imgName = temp[temp.size()-1];
    temp.clear();
    boost::split(temp, imgName, boost::is_any_of("."));
    // It is assumed that the folder should have been created prior to calling
    // this function. If not, we exit from this function
    std::string outFolder = outFldr+"/"+temp[0];
    if (!boost::filesystem::exists(outFolder))
    {
      return -4;
    }
    /////////////////////////////////////////////////////////////////////////////
    // Instantiate all smart pointers
    FloatRescalerType::Pointer floatRescaler        = FloatRescalerType::New();
    SubtractImageFilterType::Pointer subtractFilter = SubtractImageFilterType::New ();
    BinaryImageToShapeLabelMapFilterType::Pointer binaryImageToShapeLabelMapFilter \
                                                    = BinaryImageToShapeLabelMapFilterType::New();
    ShapeOpeningLabelMapFilterType::Pointer shapeOpeningLabelMapFilter \
                                                    = ShapeOpeningLabelMapFilterType::New();
    LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter \
                                                    = LabelMapToLabelImageFilterType::New();
    BinaryThresholdImageFilterType::Pointer thresholdWBC \
                                                    = BinaryThresholdImageFilterType::New();
    ImageCalculatorFilterType::Pointer minmax = ImageCalculatorFilterType::New();
    ///////////////////////////////////////////////////////////////////////////
    // Apply thresholding on the RGB Image and extract a preliminary mask
    UCharImageType2D::Pointer gMask = UCharImageType2D::New();
    if (params->colorSpace == 0) // RGB color space
    {
      if (params->thresholdAlgorithm == 0)//Otsu
      {
        PrimaryThreshold<RGBToRGBAdaptorType,OtsuThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 1)//Max-entropy
      {
        PrimaryThreshold<RGBToRGBAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 2)//Triangle
      {
        PrimaryThreshold<RGBToRGBAdaptorType,TriThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 3)//Li
      {
        PrimaryThreshold<RGBToRGBAdaptorType,LiThresholdFilterType>(mInpImage, gMask, params);
      }
      else // Default is max-entropy
      {
        PrimaryThreshold<RGBToRGBAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
    }
    else
    if (params->colorSpace == 1) // Lab color space
    {
      if (params->thresholdAlgorithm == 0)//Otsu
      {
        PrimaryThreshold<RGBToLabAdaptorType,OtsuThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 1)//Max-entropy
      {
        PrimaryThreshold<RGBToLabAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 2)//Triangle
      {
        PrimaryThreshold<RGBToLabAdaptorType,TriThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 3)//Li
      {
        PrimaryThreshold<RGBToLabAdaptorType,LiThresholdFilterType>(mInpImage, gMask, params);
      }
      else // Default is max-entropy
      {
        PrimaryThreshold<RGBToLabAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
    }
    else // default is Lab color space
    {
      if (params->thresholdAlgorithm == 0)//Otsu
      {
        PrimaryThreshold<RGBToLabAdaptorType,OtsuThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 1)//Max-entropy
      {
        PrimaryThreshold<RGBToLabAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 2)//Triangle
      {
        PrimaryThreshold<RGBToLabAdaptorType,TriThresholdFilterType>(mInpImage, gMask, params);
      }
      else
      if (params->thresholdAlgorithm == 3)//Li
      {
        PrimaryThreshold<RGBToLabAdaptorType,LiThresholdFilterType>(mInpImage, gMask, params);
      }
      else // Default is max-entropy
      {
        PrimaryThreshold<RGBToLabAdaptorType,MaxEntThresholdFilterType>(mInpImage, gMask, params);
      }
    }
    //TODO: add other color spaces

    /////////////////////////////////////////////////////////////////////////////
    // The mask will contain platelets and debris. We need to remove them. For that
    // we apply a small. On an average platelet size will be approximately 500 pixels or less
    // at a 100x magnification. We use this as a threshold. However, clumped up platelets
    // will be missed out here. If thats the case then the mask will pass onto the WBC mask.
    // It should not be a matter of concern as the second part of the WBC pipeline will detect
    // and eliminate it given that platelets will have a very small cytoplasm count
    binaryImageToShapeLabelMapFilter->SetInput(gMask);
    shapeOpeningLabelMapFilter->SetInput( binaryImageToShapeLabelMapFilter->GetOutput() );
    shapeOpeningLabelMapFilter->SetLambda( params->platelet_min_size );
    shapeOpeningLabelMapFilter->SetAttribute( ShapeOpeningLabelMapFilterType::LabelObjectType::NUMBER_OF_PIXELS);
    labelMapToLabelImageFilter->SetInput(shapeOpeningLabelMapFilter->GetOutput());
    thresholdWBC->SetInput(labelMapToLabelImageFilter->GetOutput());
    thresholdWBC->SetLowerThreshold(1);
    thresholdWBC->SetInsideValue(255);
    thresholdWBC->SetOutsideValue(0);
    thresholdWBC->Update();

    // Check if there are any connected components in the image.
    minmax->SetImage(thresholdWBC->GetOutput());
    minmax->Compute();

    // If no WBC's are found we do not have to do any recognition (or if requested
    // save the mask images)
    if (minmax->GetMaximum() > 0)
    {
      // WBC's are present in the image. We do some post-processing
      // and clean up spurious blobs
      (*masks)[0] = UCharImageType2D::New();
      (*masks)[0]->SetRegions((*mInpImage)->GetLargestPossibleRegion());
      (*masks)[0]->Allocate();
      (*masks)[0]->Graft(thresholdWBC->GetOutput());
      WBCMaskPostProcessing((*masks)[0], params);
      // Now, check if we need to save the WBC mask image
      if (saveMask)
      {
        std::string wbcMask = outFolder+"/WBC_"+temp[0]+outExtn;
        spin::WriteITKImage<spin::UCharImageType2D>(wbcMask, (*masks)[0]);
      }

      // Subtract the remaining blobs to obtain the platelet mask
      subtractFilter->SetInput1(gMask);
      subtractFilter->SetInput2((*masks)[0]);
      subtractFilter->Update();
      (*masks)[2] = UCharImageType2D::New();
      (*masks)[2]->SetRegions((*mInpImage)->GetLargestPossibleRegion());
      (*masks)[2]->Allocate();
      (*masks)[2]->Graft(subtractFilter->GetOutput());
      PlateletMaskPostProcessing((*masks)[2], params);

      // Now, check if we need to save the Platelet mask image
      if (saveMask)
      {
        std::string pltMask = outFolder+"/Platelet_"+temp[0]+outExtn;
        spin::WriteITKImage<spin::UCharImageType2D>(pltMask, (*masks)[2]);
      }
    }
    else
    {
      // We try and see if we can find any platelets
      (*masks)[2] = UCharImageType2D::New();
      (*masks)[2]->SetRegions((*mInpImage)->GetLargestPossibleRegion());
      (*masks)[2]->Allocate();
      (*masks)[2]->Graft(gMask);
      PlateletMaskPostProcessing((*masks)[2], params);

      // Now, check if we need to save the Platelet mask image
      if (saveMask)
      {
        std::string pltMask = outFolder+"/Platelet_"+temp[0]+outExtn;
        spin::WriteITKImage<spin::UCharImageType2D>(pltMask, (*masks)[2]);
      }
    }

    temp.clear();
    return 1;
  }//end of function
}//end of namespace
