#ifdef SPIN_PLUGIN_FRAMEWORK
#include "Framework/Exports/spinAPI.h"
#include "AbstractClasses/AbstractPlugin.h"
#else
#include "Utilities/Pipelines/Fusion/FusionInterface.h"
#endif
#include "AbstractClasses/AbstractImageTypes.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "Utilities/ImageProcessing/WaveletUtils/ImageFusion/ImageFusionUWT.h"
#include "Utilities/ImageProcessing/WaveletUtils/ImageFusion/ImageFusionUDTCWT.h"
#include "Utilities/ImageProcessing/WaveletUtils/ImageFusion/ImageFusion.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include <memory>
#include <boost/thread.hpp>
#include <boost/scoped_array.hpp>

namespace spin
{
#ifdef SPIN_PLUGIN_FRAMEWORK
  class SPIN_API FusionInterface : public AbstractPlugin
  {
    public:
      // Dummy constructor
      FusionInterface(){}

      /*! \brief InstantiatePipeline */
      int InstantiatePipeline(const char* inp);

      /*! \brief Process Pipeline */
      int ProcessPipeline(void* input, void* _aoiInfo, void* output);

      /*! \brief Complete Processing */
      int CompleteProcessing();
    private:
      // Type of wavelet transform being used
      int type = 1; //undecimated wavelet transform
      // Number of levels of decomposition
      int levels = 1; // 2 levels of wavelet decomposition
  };//end of class
#endif

  /*! \brief FusionThread
   *  This function is the main driver to run the image fusion thread
   */
  int FusionThread( std::vector<RMMatrix_Float>* img, \
                    int levels, int type, RMMatrix_Float* outImg)
  {
    int y = 0;
    if (type == 0)
    {
      std::shared_ptr<ImageFusion> fusion = std::make_shared<ImageFusion>();
      y = fusion->Compute(img, outImg, levels);
    }
    else
    {
      std::shared_ptr<ImageFusionUWT> fusion = std::make_shared<ImageFusionUWT>();
      y = fusion->Compute(img, outImg, levels);
    }
    return y;
  }//end of function

#ifdef SPIN_PLUGIN_FRAMEWORK
  /*! \brief Instantiate
   *  This function is used to parse a XML file and populate parameters, if
   *  required.
   *  TODO: we keep this empty for now
   */
  int FusionInterface::InstantiatePipeline(const char* inpFile)
  {
    return 1;
  }//end of function

  /*! \brief CompleteProcessing
   *  This function is not used for now.
   */
  int FusionInterface::CompleteProcessing()
  {
    return 1;
  }//end of function
#endif

  /*! \brief ProcessPipeline
   *  This is the interface function to fuse a set of images using
   *  an undecimated dual tree complex wavelet transform. It is assumed that the
   *  data contains RGB channels separated as Eigen Matrices in a vector.
   *  The number of levels of decomposition are also specified. As the transform
   *  is an undecimated wavelet transform. A flag indicates if the channels need
   *  to be extracted and grouped together.
   *  @_cImg    : The images that need to be fused.
   *  @info     : The info variable is not initialized
   *  @_img     : The fused RGB image
   */
#ifdef SPIN_PLUGIN_FRAMEWORK
  int FusionInterface::ProcessPipeline(void* _cImg, void* info, void* _img)
#else
  int FusionInterface::ProcessPipeline(void* _cImg, int levels, int type, void* _img)
#endif
  {
    //sanity checks
    typedef std::vector<RMMatrix_Float> vChannel;
    typedef std::vector<vChannel> vImage;
    vImage* cImg = static_cast<vImage*>(_cImg);
    if (!cImg) return -1;
    if (cImg->size() <= 0) return -2;
    if (cImg->size() == 1) return -3;
    RGBImageType::Pointer* Img = static_cast<RGBImageType::Pointer*>(_img);
    if (!Img) return -4;
    if (!(*Img)->GetBufferPointer()) return -5;

    // Allocate memory for all channels
    vChannel r(cImg->size(),RMMatrix_Float::Zero((*cImg)[0][0].rows(),(*cImg)[0][0].cols()));
    vChannel g(cImg->size(),RMMatrix_Float::Zero((*cImg)[0][0].rows(),(*cImg)[0][0].cols()));
    vChannel b(cImg->size(),RMMatrix_Float::Zero((*cImg)[0][0].rows(),(*cImg)[0][0].cols()));
    auto count=0;
    for (auto i: (*cImg))
    {
      r[count] = i[0];
      g[count] = i[1];
      b[count] = i[2];
      ++count;
    }

    // Instantiate a placeholder for holding the fused matrices
    vChannel fused(3,RMMatrix_Float::Zero((*cImg)[0][0].rows(),(*cImg)[0][0].cols()));

    //Create three boost threads on the heap and assign it to thread_group
    //http://stackoverflow.com/questions/1246813/simple-boost-thread-group-question
    int NUM_THREADS = 3; // corresponding to three channels
    typedef std::shared_ptr<boost::thread> bThread;
    boost::scoped_array< bThread > tArray(new bThread[NUM_THREADS]);
    // Place them in the group
    tArray[0] = bThread(new boost::thread(boost::bind(&FusionThread, &r, levels, type, &fused[0])));
    tArray[1] = bThread(new boost::thread(boost::bind(&FusionThread, &g, levels, type, &fused[1])));
    tArray[2] = bThread(new boost::thread(boost::bind(&FusionThread, &b, levels, type, &fused[2])));

    //Execute all threads
    for (int jThread = 0; jThread < NUM_THREADS; ++jThread)
    {
      tArray[jThread].get()->join();
    }
    r.clear();g.clear();b.clear();

    //Fuse the channels to a RGB Image
    int h = VectorOfMatrices2Image<float>(&fused, _img);
    if (h !=1) return -4;
    return 1;
  }//end of function
}//end of namespace

#ifdef SPIN_PLUGIN_FRAMEWORK
typedef spin::FusionInterface   FusionInterface;
extern "C" void* Processing_Plugin()
{
  // return the object type
  FusionInterface* obj = new FusionInterface();
  void* q = (void*)(obj);
  return q;
}//end of function
#endif
