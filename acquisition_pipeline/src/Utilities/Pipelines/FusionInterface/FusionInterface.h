#ifndef SPIN_PLUGIN_FRAMEWORK
  #ifndef FUSIONINTERFACE_H_
  #define FUSIONINTERFACE_H_
  #include "Framework/Exports/spinAPI.h"
  #include "AbstractClasses/AbstractPlugin.h"
  #include <string>

  namespace spin
  {
    class SPIN_API FocusFusionInterface
    {
      public:
        // Dummy constructor
        FocusFusionInterface(){}

        /*! \brief Compute */
        int ProcessPipeline(void* _cImg, int levels, int type, void* _img);
    };//end of class
  }
  #endif
#endif
