#ifdef SPIN_PLUGIN_FRAMEWORK
#include "Framework/Exports/spinAPI.h"
#include "AbstractClasses/AbstractPlugin.h"
#include "Utilities/MatrixUtils/EigenUtils.h"
#include <utility>
#include <map>
#else
#endif
#include "AbstractClasses/AbstractImageTypes.h"
namespace spin
{
#ifdef SPIN_PLUGIN_FRAMEWORK
  class SPIN_API AlphaBlending : public AbstractPlugin
  {
    public:
      typedef std::map<int, std::pair<float,float> > ramp;
      typedef std::map<int, ramp> rampMap;
      // Dummy constructor
      AlphaBlending(){}

      // Default destructor
      ~AlphaBlending()
      {
        rampMap::iterator it = rMap.begin();
        while (it != rMap.end())
        {
          it->second.clear();
          ++it;
        }
      }
      /*! \brief InstantiatePipeline */
      int InstantiatePipeline(const char* inp, void* meta=nullptr);

      /*! \brief Process Pipeline */
      int ProcessPipeline(void* input, void* _aoiInfo, void* output);

      /*! \brief Complete Processing */
      int CompleteProcessing();
    private:
      rampMap rMap;
      // Check Metric
      void ComputeMetric(void* _cImg1, void* _cImg2, void* _regions);
      
  };//end of class
#endif

#ifdef SPIN_PLUGIN_FRAMEWORK
  /*! \brief Instantiate
   *  This function is used to parse a XML file and populate parameters, if
   *  required.
   *  TODO: we keep this empty for now
   */
  int AlphaBlending::InstantiatePipeline(const char* inpFile, void* meta)
  {
    return 1;
  }//end of function

  /*! \brief CompleteProcessing
   *  This function is not used for now.
   */
  int AlphaBlending::CompleteProcessing()
  {
    return 1;
  }//end of function
#endif

  /*! \brief ComputeMetric
   *  This function is used to compute a metric between two overlapping regions
   */
  void AlphaBlending::ComputeMetric(void* _cImg1, void* _cImg2, void* _regions)
  {
    RGBImageType::Pointer* pRefImg = static_cast<RGBImageType::Pointer*>(_cImg1);
    if (!pRefImg) return -1;
    RGBImageType::Pointer* pTgtImg = static_cast<RGBImageType::Pointer*>(_cImg2);
    if (!pTgtImg) return -2;
    RegionStruct* regions = static_cast<RegionStruct*>(_regions);
    if (!regions) return -3;

    // Get the regions
    RGBImageType::RegionType refOverlapRegion = regions->refOverlapRegion;
    RGBImageType::RegionType tgtOverlapRegion = regions->tgtOverlapRegion;
    RGBImageType::SizeType   refSizeType      = refOverlapRegion.GetSize();
    RGBImageType::SizeType   tgtSizeType      = tgtOverlapRegion.GetSize();
   
    // Traverse through both images
    typedef itk::ImageRegionIteratorWithIndex<RGBImageType> Iterator;
    Iterator ref((*pRefImg),refOverlapRegion);
    Iterator tgt((*pTgtImg),tgtOverlapRegion);
    // Determine which one has the smaller of the two indices. This will determine
    // the mapping to the LUT
    ref.GoToBegin();
    tgt.GoToBegin();
    
    // Initialize 3 matrices
    RMMatrix_Float ent1 = RMMatrix_Float::Zero(1,256);
    RMMatrix_Float ent2 = RMMatrix_Float::Zero(1,256);
    RMMatrix_Float jent = RMMatrix_Float::Zero(256,256);
    // Begin the metric computation process
    while (!ref.IsAtEnd())
    {
      // Get the Pixel values from both locations
      RGBPixelType pixRef = ref.Get();
      RGBPixelType pixTgt = tgt.Get();
      // Update the matrices
      int g1 = pixRef.GetGreen();
      int g2 = pixTgt.GetGreen();
      // Increment marginals
      ++ent1(0,g1);
      ++ent2(0,g2);
      // and the joint
      ++jent(g1,g2);
      // Finally, increment the iterators
      ++ref;
      ++tgt;
    }
    
    // Build the approximate pdf's
    ent1 = ent1.array()/(ent1.sum());
    RMMatrix_Float ent1logent1 = ent1.array().unaryExpr(std::ref(EigenUtils::xlogx<float>));
    ent2 = ent2.array()/(ent2.sum());
    RMMatrix_Float ent2logent2 = ent2.array().unaryExpr(std::ref(EigenUtils::xlogx<float>));
    jent = jent.array()/(jent.sum());
    RMMatrix_Float jentlogjent = jent.array().unaryExpr(std::ref(EigenUtils::xlogx<float>));
    
    // and the entropies
    float entropy1 = ent1logent1.sum(); 
    float entropy2 = ent2logent2.sum();
    float entropy3 = jentlogjent.sum();
    
    //Normalized mutual informationa as defined by
    // Studholme, C., Hill, D., L.G., Hawkes, D.J.: An Overlap Invariant Entropy Measure
    // of 3D Medical Image Alignment. Patt. Rec. 32, 71–86 (1999)
    // Also: Eq 3.30 of 
    // "Medical Image Registration", Hajnal, Hill and Hawkes, CRC Press, 2001, p.61-62
    regions->metricVal = (entropy1+entropy2)/(entropy3);  
  }//end of function
  
  /*! \brief ProcessPipeline
   *  This is the interface function to fuse a set of images using
   *  an undecimated dual tree complex wavelet transform. It is assumed that the
   *  data contains RGB channels separated as Eigen Matrices in a vector.
   *  The number of levels of decomposition are also specified. As the transform
   *  is an undecimated wavelet transform. A flag indicates if the channels need
   *  to be extracted and grouped together.
   *  @_cImg    : The images that need to be fused.
   *  @info     : The info variable is not initialized
   *  @_img     : The fused RGB image
   */
#ifdef SPIN_PLUGIN_FRAMEWORK
  int AlphaBlending::ProcessPipeline(void* _cImg1, void* _cImg2, void* _regions)
#else
#endif
  {
    RGBImageType::Pointer* pRefImg = static_cast<RGBImageType::Pointer*>(_cImg1);
    if (!pRefImg) return -1;
    RGBImageType::Pointer* pTgtImg = static_cast<RGBImageType::Pointer*>(_cImg2);
    if (!pTgtImg) return -2;
    RegionStruct* regions = static_cast<RegionStruct*>(_regions);
    if (!regions) return -3;

    // Get the regions
    RGBImageType::RegionType refOverlapRegion = regions->refOverlapRegion;
    RGBImageType::RegionType tgtOverlapRegion = regions->tgtOverlapRegion;
    RGBImageType::SizeType   refSizeType      = refOverlapRegion.GetSize();
    RGBImageType::SizeType   tgtSizeType      = tgtOverlapRegion.GetSize();

    // First we will check a metric
    ComputeMetric(_cImg1, _cImg2, _regions);
    
    // Make a check if the threshold criterion is met
    if (regions->metricVal < regions->metricThresh)
    {
      // Create/Use a LUT of Ramp values that will be used to proportion the
      // two images during blending. This LUT is determined by the smaller of the
      // two dimensions of the overlapping region
      int rampLen = (std::min)((int)refSizeType[1],(int)refSizeType[0]);
      ramp pLUT;
      // Now, check if this ramp was created somewhere earlier
      rampMap::iterator it = rMap.find(rampLen);
      if (it != rMap.end())
      {
        pLUT = it->second;
      }
      else
      {
        // This ramplength was not created. So we create one
        for (int i = 0; i < rampLen; ++i)
        {
          float v1 = (i);
          float v2 = (float)(rampLen - i - 1);
          pLUT[i] = std::make_pair(v1, v2);
        }
        rMap[rampLen] = pLUT;
      }

      // Blend the two images
      typedef itk::ImageRegionIteratorWithIndex<RGBImageType> Iterator;
      Iterator ref((*pRefImg),refOverlapRegion);
      Iterator tgt((*pTgtImg),tgtOverlapRegion);
      // Determine which one has the smaller of the two indices. This will determine
      // the mapping to the LUT
      ref.GoToBegin();
      tgt.GoToBegin();
      RGBImageType::IndexType idxRef = ref.GetIndex();
      RGBImageType::IndexType idxTgt = tgt.GetIndex();
      std::pair<int,int> tMap;
      if (rampLen == refSizeType[1])
      {
        if (idxRef[1] < idxTgt[1])
        {
          //use the Indices of the reference image for mapping to the LUT
          tMap = std::make_pair(0,1);
        }
        else
        {
          //use the Indices of the target image for mapping to the LUT
          tMap = std::make_pair(1,1);
        }
      }
      else
      {
        if (idxRef[0] < idxTgt[0])
        {
          //use the Indices of the reference image for mapping to the LUT
          tMap = std::make_pair(0,0);
        }
        else
        {
          //use the Indices of the target image for mapping to the LUT
          tMap = std::make_pair(1,0);
        }
      }

      // Begin the blending process
      while (!ref.IsAtEnd())
      {
        // Get the Pixel values from both locations
        RGBPixelType pixRef = ref.Get();
        RGBPixelType pixTgt = tgt.Get();

        // Get the blended pixel values
        unsigned char Red=0;
        unsigned char Green=0;
        unsigned char Blue=0;
        float alpha1=1.0;
        float alpha2=0.0;
        if (tMap.first == 0)
        {
          // Get the index of the reference image
          RGBImageType::IndexType idx = ref.GetIndex();
          if (tMap.second == 0)
          {
            alpha1 = pLUT[idx[0]].first / (pLUT[idx[0]].second+pLUT[idx[0]].first);
            alpha2 = 1.0-alpha1;
          }
          else
          {
            alpha1 = pLUT[idx[1]].first / (pLUT[idx[1]].second+pLUT[idx[1]].first);
            alpha2 = 1.0-alpha1;
          }
        }
        else
        {
          // Get the index of the target image
          RGBImageType::IndexType idx = tgt.GetIndex();
          if (tMap.second == 0)
          {
            alpha2 = pLUT[idx[0]].first/(pLUT[idx[0]].second + pLUT[idx[0]].first);
            alpha1 = 1.0-alpha2;
          }
          else
          {
            alpha2 = pLUT[idx[1]].first/(pLUT[idx[1]].second + pLUT[idx[1]].first);
            alpha1 = 1.0-alpha2;
          }
        }

        // Set the red, green and blue values
        Red   = static_cast<unsigned char>(round( (float)pixRef.GetRed()*alpha1+(float)pixTgt.GetRed()*alpha2));
        Green = static_cast<unsigned char>(round( (float)pixRef.GetGreen()*alpha1+(float)pixTgt.GetGreen()*alpha2));
        Blue  = static_cast<unsigned char>(round( (float)pixRef.GetBlue()*alpha1+(float)pixTgt.GetBlue()*alpha2));

        // Update the pixel values here
        pixRef.SetRed(Red);pixRef.SetGreen(Green);pixRef.SetBlue(Blue);
        pixTgt.SetRed(Red);pixTgt.SetGreen(Green);pixTgt.SetBlue(Blue);

        // And place them in the image
        ref.Set(pixRef);
        tgt.Set(pixTgt);

        // Finally, increment the iterators
        ++ref;
        ++tgt;
      }
     
      return 1;
    }
    // Metric is not satisfied
    return 1;
  }//end of function
}//end of namespace

#ifdef SPIN_PLUGIN_FRAMEWORK
typedef spin::AlphaBlending   AlphaBlending;
extern "C" void* Processing_Plugin()
{
  // return the object type
  AlphaBlending* obj = new AlphaBlending();
  void* q = (void*)(obj);
  return q;
}//end of function
#endif
