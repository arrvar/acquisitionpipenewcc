#include <iostream>
#include <cstdlib>
#include <chrono>
#include <dlibxx.hxx>
#include "boost/filesystem.hpp"
#include "AbstractClasses/AbstractPlugin.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"
#include <memory>

/*! \brief
 * This is the main driver to generate a pyramid
 */
int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    //std::cout<< argv[0] << std::endl;
    //std::cout << "<path to module> <path_to_xml_file> <input image path>  <output image path> " << std::endl;
    exit(-1);
  }
  std::string libname = std::string(argv[1]);
  boost::filesystem::path dfile(libname.c_str());
  if (!boost::filesystem::exists(dfile))
  {
    //std::cout<<"Library does not exist"<<std::endl;
    return -2;
  }
  std::shared_ptr<dlibxx::handle>lib = std::make_shared<dlibxx::handle>();
  lib->resolve_policy(dlibxx::resolve::lazy);
  lib->set_options(dlibxx::options::global);
  lib->load(libname);
  if(!lib->loaded())
  {
    //std::cout<<"Library could not be loaded: "<<lib->error()<<std::endl;
    return -3;
  }
  std::shared_ptr<spin::AbstractPlugin> cModule = lib->template create<spin::AbstractPlugin>("Processing_Plugin");
  if (!cModule.get())
  {
    //std::cout<<"Module could not be initialized: "<<std::endl;
    return -4;
  }

  int v = cModule->InstantiatePipeline(argv[2], NULL);
  if (v != 1)
  {
    //std::cout<<"Parameter file could not be parsed"<<std::endl;
    return -5;
  }

  std::string inputImagePath = std::string(argv[3]);
  spin::RGBImageType::Pointer in_img = spin::RGBImageType::New();
  spin::ReadITKImage<spin::RGBImageType>(inputImagePath, in_img);
  spin::RGBImageType::SizeType size = in_img->GetLargestPossibleRegion().GetSize();
  spin::RMMatrix_Float resMat = spin::RMMatrix_Float::Zero(size[1],size[0]);
  // Convert the image to a matrix
  int h = spin::Image2Matrix<float>(&in_img, &resMat);//converting itk image to RMMatrix_float
  if (h !=1)
  return -2;

  // Process the image
  spin::UCharImageType2D::Pointer out_img = spin::UCharImageType2D::New();
  out_img->SetRegions(in_img->GetLargestPossibleRegion());
  out_img->Allocate();
  cModule->ProcessPipeline(&resMat, NULL, &out_img);

  // Save mask to disk
  std::string outImgName = std::string(argv[4]);
  spin::WriteITKImage<spin::UCharImageType2D>(outImgName, out_img);
}//end of function
