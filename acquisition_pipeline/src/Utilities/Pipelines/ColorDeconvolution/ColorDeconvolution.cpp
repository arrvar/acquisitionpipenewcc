#include "AbstractClasses/AbstractPlugin.h"
//Common headers for plugin and unit test
#include <stdio.h>
#include <math.h>
#include "AbstractClasses/AbstractImageTypes.h"
#include "AbstractClasses/AbstractMatrixTypes.h"
#include "Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.h"

#include <unsupported/Eigen/MatrixFunctions>
#include "itkImage.h"
#include "itkImageFileReader.h"
#include <itkOpenCVImageBridge.h>
#include "boost/filesystem.hpp"
#include "boost/property_tree/xml_parser.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/foreach.hpp"
#include "itkBinaryThresholdImageFilter.h"

//opencv headers
#include "opencv2/opencv.hpp"


namespace spin
{
  class ColorDeconvolution : public AbstractPlugin
  {
    public:
	  ColorDeconvolution(){}
	  ~ColorDeconvolution(){}

	  /*! \brief InstantiatePipeline */
	  int InstantiatePipeline(const char* inp, void* meta);

    /*! \brief Process Pipeline */
    int ProcessPipeline(void* input, void* input2, void* output);

    /*! \brief Complete Processing */
    int CompleteProcessing();
	private:
	  bool writeImage = false;

	  struct DeconvolutionParams
    {
	    RMMatrix_Float MOD_mat;
	    RMMatrix_Float QMat;
    };
	  float q[9] = {0};
	  float log255 = log(255);
	  int thresholdValue = 200;
	  
	  DeconvolutionParams deParams;
	  
	  int ParseParams(const char* inp);

	  void matrixInversion(float cosx[3], float cosy[3], float cosz[3]);

	  void postprocessing(cv::Mat resImage, cv::Mat& mask);

	  void translationalMatrix(float MODx[3], float MODy[3], float MODz[3], float cosx[3], float cosy[3], float cosz[3]);
  }; //end of class

  int ColorDeconvolution::InstantiatePipeline(const char* inp, void* meta)
  {
	  //if only there is a change in the parameters
	  int ret = ParseParams(inp);
	  if (ret != 1) return -1;
	  float MODx[3] = {};
    float MODy[3] = {};
    float MODz[3] = {};
    if (ret == 1)
    {
	    float *ptrMODx;
	    ptrMODx = deParams.MOD_mat.data();
	    for (int i = 0; i<3; ++i)
	    {
	      MODx[i] = *(ptrMODx+i);
	      MODy[i] = *(ptrMODx+i+3);
	      MODz[i] = *(ptrMODx+i+6);
	    }
	  }
    else
    {
      float MODx[3] = {0.21393921,0.74890292, 0.268 };
	    float MODy[3] = {0.85112669, 0.60624161, 0.570};
	    float MODz[3] = {0.47794022, 0.26731082, 0.776};
    }
	  float cosx[3] = {0};
	  float cosy[3] = {0};
	  float cosz[3] = {0};

	  float (*ptrcosx)[3] = &cosx;
	  float (*ptrcosy)[3] = &cosy;
	  float (*ptrcosz)[3] = &cosz;
	  translationalMatrix(MODx, MODy, MODz, (*ptrcosx), (*ptrcosy), (*ptrcosz));
	  matrixInversion((*ptrcosx), (*ptrcosy), (*ptrcosz));
	  return 1;
  }

  int ColorDeconvolution::ProcessPipeline(void* input,  void* input2, void* _output)
  {
    UCharImageType2D::Pointer* output = static_cast<UCharImageType2D::Pointer*>(_output);
	  //typename inImgType::Pointer* inImgTypeInput   = static_cast<typename inImgType::Pointer*>(input);//casting to itk image
	  RMMatrix_Float* image_mat = static_cast<RMMatrix_Float*>(input);
    if (!image_mat->data()) return -1;

	  RMMatrix_Float resMat = (-((255*((*image_mat/255.0).array().log()))/log255)) ;
	  resMat *= deParams.QMat.transpose();
	  resMat = (((-(resMat.array()-255)) * log255) /255.0).array().exp();
    // Take the first channel
    RMMatrix_Float finMat = resMat.col(0);
    // Copy the image to an ITK Image for thresholding
    FloatImageType2D::Pointer fltImg = FloatImageType2D::New();
    fltImg->SetRegions((*output)->GetLargestPossibleRegion());
    fltImg->Allocate();
    fltImg->FillBuffer(0);
    memcpy(fltImg->GetBufferPointer(), finMat.data(), sizeof(float) * finMat.rows());
    typedef itk::BinaryThresholdImageFilter<FloatImageType2D, UCharImageType2D> FilterType;
    FilterType::Pointer filter = FilterType::New();
    filter->SetInput(fltImg);
    filter->SetLowerThreshold(0);
    filter->SetUpperThreshold(thresholdValue);
    filter->Update();
    memcpy((*output)->GetBufferPointer(), filter->GetOutput()->GetBufferPointer(), sizeof(unsigned char) * finMat.rows());
    resMat.resize(0,0);
    finMat.resize(0,0);
    return 1;
  }//end of function

  int ColorDeconvolution::ParseParams(const char* inputFile)
  {
  	//reading the from the xml file if it is passed
  	deParams.MOD_mat.resize(3,3);
  	boost::filesystem::path basePath(inputFile);
  	std::ifstream input(inputFile);
  	using boost::property_tree::ptree;
  	ptree pt;
  	try
  	{
  	  read_xml(input, pt);
  	}
  	catch (const boost::property_tree::ptree_error &e)
  	{
  	  return -2;//cant load xml
  	}
    BOOST_FOREACH(ptree::value_type& v_dec, pt.get_child("DeconvolutionParameters"))
    { // read values for each user input values
	  if (v_dec.first == "MODx")
	  {
		BOOST_FOREACH(ptree::value_type& v1, v_dec.second)
		{
			if (v1.first == "MODx1")
			    deParams.MOD_mat(0,0) = v1.second.get<float>("");
			else if (v1.first == "MODx2")
				deParams.MOD_mat(0,1) = v1.second.get<float>("");
			else if (v1.first == "MODx3")
				deParams.MOD_mat(0,2) = v1.second.get<float>("");
		}

	  }
	  else if (v_dec.first == "MODy")
	  {
		BOOST_FOREACH(ptree::value_type& v1, v_dec.second)
		{
		  if (v1.first == "MODy1")
		   	deParams.MOD_mat(1,0) = v1.second.get<float>("");
		  else if (v1.first == "MODy2")
			deParams.MOD_mat(1,1) = v1.second.get<float>("");
		  else if (v1.first == "MODy3")
			deParams.MOD_mat(1,2) = v1.second.get<float>("");
		}
	  }
	  else if (v_dec.first == "MODz")
	  {
		BOOST_FOREACH(ptree::value_type& v1, v_dec.second)
		{
		     if (v1.first == "MODz1")
			  	deParams.MOD_mat(2,0) = v1.second.get<float>("");
			 else if (v1.first == "MODz2")
				deParams.MOD_mat(2,1) = v1.second.get<float>("");
			 else if (v1.first == "MODz3")
				deParams.MOD_mat(2,2) = v1.second.get<float>("");
		}
	  }
	  else if (v_dec.first == "WriteMask")
	  {
		if (v_dec.second.get<int>("") == 1)
			writeImage = true;
		else writeImage = false;
	  }
	  else if (v_dec.first == "Threshold")
		  thresholdValue = v_dec.second.get<int>("");
	}
	return 1;
  }

  int ColorDeconvolution::CompleteProcessing()
  {
	  return 1;//not implemented as of now
  }

  void ColorDeconvolution::matrixInversion(float cosx[3], float cosy[3], float cosz[3])
  {
    //matrix inversion to get the end-members
    float A =   cosy[1] -   cosx[1] *   cosy[0] /   cosx[0];
    float V =   cosz[1] -  cosx[1] *   cosz[0] /   cosx[0];
    float C =   cosz[2] -   cosy[2] * V/A +   cosx[2]*(V/A *   cosy[0]/  cosx[0] -   cosz[0]/  cosx[0]);
    deParams.QMat.resize(3,3);
    deParams.QMat(0,2) = (-  cosx[2] /   cosx[0] -   cosx[2] / A *   cosx[1] /   cosx[0] *   cosy[0] /   cosx[0] +   cosy[2] / A *   cosx[1] /   cosx[0]) / C;
    deParams.QMat(0,1) = (-  deParams.QMat(0,2) * V / A -   cosx[1] / (  cosx[0] * A));
    deParams.QMat(0,0) = (1.0 /   cosx[0] -   deParams.QMat(0,1) *   cosy[0] /   cosx[0] -   deParams.QMat(0,2) *   cosz[0] /   cosx[0]);
    deParams.QMat(1,2) = (-  cosy[2] / A +   cosx[2] / A *   cosy[0] /   cosx[0]) / C;
    deParams.QMat(1,1) = (-  deParams.QMat(1,2) * V / A + 1.0 / A);
    deParams.QMat(1,0) = (-  deParams.QMat(1,1) *   cosy[0] /   cosx[0] -   deParams.QMat(1,2) *   cosz[0] /   cosx[0]);
    deParams.QMat(2,2) = (1.0 / C );
    deParams.QMat(2,1) = -  deParams.QMat(2,2) * V / A ;
    deParams.QMat(2,0) = -  deParams.QMat(2,1) *   cosy[0] /   cosx[0] -   deParams.QMat(2,2) *   cosz[0] /   cosx[0];
  }

  /**
   * @brief to obtain the mask from the deconvolved image
   * @param resImage:  Image obtained after deconvolution
   * @param mask: Thresholded binary mask
   */
  void ColorDeconvolution::postprocessing(cv::Mat resImage, cv::Mat& mask)
  {
    std::vector<cv::Mat> imageChannels(3),channels(3);
    cv::split(resImage, channels);
    cv::threshold(channels[0], mask, 200, 255, cv::THRESH_BINARY_INV);
  }

  void ColorDeconvolution::translationalMatrix(float MODx[3], float MODy[3], float MODz[3], float cosx[3], float cosy[3], float cosz[3])
  {
    float len[3] = {0};
    for (int i=0; i<3; ++i)
    {
      len[i] = sqrt((MODx[i] * MODx[i]) + (MODy[i] * MODy[i]) + (MODz[i] * MODz[i]));
  	  if (len[i] != 0.0)
  	  {
  		cosx[i] = MODx[i] / (len[i]);
  		cosy[i] = MODy[i] / (len[i]);
  		cosz[i] = MODz[i] / (len[i]);
  	  }
  	  if (cosx[1] == 0.0)
  	  {
  	    if (cosy[1] == 0.0)
  	    {
  	      if (cosz[1] == 0.0)
    		  {
    		    cosx[1] = cosx[0];
    		    cosy[1] = cosy[0];
    		    cosz[0] = cosz[0];
    		  }
  	    }
  	  }
  	  if (cosx[2] == 0.0)//checking for worst case senarios
  	  {
  	    if (cosy[2] == 0.0)
  	    {
  		    if (cosz[2] == 0.0)
  		    {
  		  	  if ((cosx[0]*cosx[0] + cosx[1]*cosx[1]) > 1)
  			    {
  			      cosx[2] = 0.0;
  			    }
      			else
      			{
      			  cosx[2] = sqrt(1.0 - (cosx[0]*cosx[0]) - (cosx[1]*cosx[1]));
      			}
      			if ((cosy[0]*cosy[0] + cosy[1]*cosy[1]) > 1)
      			{
      			  cosy[2] = 0.0;
      			}
      			else
      			{
      			  cosy[2] = sqrt(1.0 - (cosy[0]*cosy[0]) - (cosy[1]*cosy[1]));
      			}
      			if ((cosz[0]*cosz[0] + cosz[1]*cosz[1]) > 1)
      			{
      			  cosz[2] = 0.0;
      			}
      			else
      			{
      			  cosz[2] = sqrt(1.0 - (cosz[0]*cosz[0]) - (cosx[1]*cosx[1]));
      			}
  		    }
  	    }
  	  }
    }
  }//end of function
}//end of namespace

#ifdef SPIN_PLUGIN_FRAMEWORK
typedef spin::ColorDeconvolution   spColorDeconvolution;
extern "C" void* Processing_Plugin()
{
  // return the object type
  spColorDeconvolution* obj = new spColorDeconvolution();
  void* q = (void*)(obj);
  return q;
}//end of function
#endif
