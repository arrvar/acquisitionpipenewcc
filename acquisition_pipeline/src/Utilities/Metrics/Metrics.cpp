#include "Utilities/Metrics/Metrics.h"
#include "AbstractClasses/AbstractImageTypes.h"
#include "itkImageRegionConstIterator.h"

namespace spin
{
  namespace
  {
    typedef FloatImageType2D GrayImageType;
  }

  /*! \brief ImagePairSadandCorrleation
   *  This functions takes a pair of images and calculates the sum of absoulte 
   *  error and Normalized cross correlation
   *  @inputimage1        : The first image
   *  @inputimage2        : The second image
   *  @sadiff_metric      : The sum of absolute difference metric
   *  @correlation_metric : The normalized correlation metric
   */
  void Metrics::ImagePairSadandCorrleation( void* inputimage1, 
                                            void* inputimage2, 
                                            float* sadiff_metric, 
                                            float* correlation_metric)
  {
    // ITK version of the Entropy calculations
    //typecasts
    GrayImageType::Pointer* image_1 = static_cast<GrayImageType::Pointer*>(inputimage1);
    GrayImageType::Pointer* image_2 = static_cast<GrayImageType::Pointer*>(inputimage2);

    itk::ImageRegionConstIterator<GrayImageType> Iterator1(*image_1, (*image_1)->GetLargestPossibleRegion());
    itk::ImageRegionConstIterator<GrayImageType> Iterator2(*image_2, (*image_2)->GetLargestPossibleRegion());

    //SAD implementation
    float sad_metric = 0, fixed_value, moving_value;
    float sum_ff = 0, sum_mm = 0, sum_fm = 0, sum_f = 0, sum_m = 0;

    double m_NumberOfPixelsCounted = 0, norm_corrmeasure = 0;

    while (!Iterator1.IsAtEnd())
    {
      fixed_value = Iterator1.Get();
      moving_value = Iterator2.Get();

      sad_metric += std::abs(fixed_value - moving_value);
      sum_ff += fixed_value  * fixed_value;
      sum_mm += moving_value * moving_value;
      sum_fm += fixed_value  * moving_value;
      sum_f += fixed_value;
      sum_m += moving_value;
      m_NumberOfPixelsCounted++;
      ++Iterator2;
      ++Iterator1;
    }

    if (m_NumberOfPixelsCounted > 0)
    {
      sum_ff           -= (sum_f * sum_f / m_NumberOfPixelsCounted);
      sum_mm           -= (sum_m * sum_m / m_NumberOfPixelsCounted);
      sum_fm           -= (sum_f * sum_m / m_NumberOfPixelsCounted);
    }

    float denom         = -1.0 * std::sqrt(sum_ff * sum_mm);
    sad_metric          = sad_metric / m_NumberOfPixelsCounted;
    norm_corrmeasure    = sum_fm / denom;
    *sadiff_metric      = sad_metric;
    *correlation_metric = std::abs(norm_corrmeasure);
  }//end of function
}//end of namespace



