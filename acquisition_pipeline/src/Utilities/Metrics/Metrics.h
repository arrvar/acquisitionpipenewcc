/*
@author : Srihari Seshadri
@description :
This class contains all the fidelity measurements that are to be 
used in metric calculations and other corrections also
*/
namespace spin
{
  class Metrics 
  {
    public : 
      void ImagePairSadandCorrleation(void* i1, void* i2, float* sad, float* c);
  };//end of class
}//end of namespace
