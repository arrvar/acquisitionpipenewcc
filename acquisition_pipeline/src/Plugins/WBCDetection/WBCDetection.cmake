#------------------------------------------------------------------------------
#Plugin for WBC Detection
#------------------------------------------------------------------------------
SET(PPROJ WBCDetection)
INCLUDE(${PROJECT_SOURCE_DIR}/Plugins/${PPROJ}/TPL_Linux.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

#------------------------------------------------------------------------------
#Set the source files necessary to build this plugin
#------------------------------------------------------------------------------
SET(${PPROJ}_DIRS
    Utilities/Pipelines/WBCDetection
    Common/AbstractClasses
)

add_definitions(-DWBC_DETECTION)
add_definitions(-DWBC_CELL_DETECTION)
SET(${PPROJ}_SRCS) 
FOREACH(CONTENT ${${PPROJ}_DIRS})
  FILE(GLOB_RECURSE U_SRCS
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.cpp
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.hh
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.h
                    ${PROJECT_SOURCE_DIR}/${CONTENT}/*.txx)
  SET(${PPROJ}_SRCS ${${PPROJ}_SRCS} ${U_SRCS})
ENDFOREACH()

#------------------------------------------------------------------------------
# Build the plugin as a shared library
#------------------------------------------------------------------------------
ADD_LIBRARY(spin_plugin_${PPROJ} SHARED ${${PPROJ}_SRCS})
install(TARGETS spin_plugin_${PPROJ}   DESTINATION	 ${CMAKE_INSTALL_PREFIX})  
#https://stackoverflow.com/questions/5096881/does-set-target-properties-in-cmake-override-cmake-cxx-flags
TARGET_COMPILE_OPTIONS(spin_plugin_${PPROJ} PRIVATE "-DSPIN_PLUGIN_FRAMEWORK")
TARGET_LINK_LIBRARIES(spin_plugin_${PPROJ} ${${PPROJ}_EXT_LIBS})
#This step ensures that the plugin has similar naming conventions across platforms
SET_TARGET_PROPERTIES(spin_plugin_${PPROJ} PROPERTIES  PREFIX "")

