#-----------------------------------------------------------------------------------#
#Boost
#-----------------------------------------------------------------------------------#
SET(Boost_MANDATORY_PACKAGES
    filesystem
    thread
    date_time
    chrono
    system
)

FIND_PACKAGE(Boost REQUIRED ${Boost_MANDATORY_PACKAGES})
IF(Boost_FOUND)
  ADD_DEFINITIONS(-DBOOST_ALL_DYN_LINK)
  INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
  SET(${PPROJC}_EXT_LIBS ${${PPROJC}_EXT_LIBS} ${Boost_LIBRARIES})
ENDIF()

#-----------------------------------------------------------------------------------#
#OPENCV
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(OpenCV REQUIRED)
INCLUDE_DIRECTORIES(${OpenCV_INCLUDE_DIRS})
SET(${PPROJC}_EXT_LIBS ${${PPROJC}_EXT_LIBS} ${OpenCV_LIBS})

#-----------------------------------------------------------------------------------#
#ITK
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(ITK REQUIRED)
INCLUDE(${ITK_USE_FILE})
LINK_DIRECTORIES(${ITK_LIBRARY_DIR})
SET(${PPROJC}_EXT_LIBS ${${PPROJC}_EXT_LIBS} ${ITK_LIBRARIES})


#-----------------------------------------------------------------------------------#
#Eigen
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(Eigen3 REQUIRED)
INCLUDE_DIRECTORIES(${EIGEN3_INCLUDE_DIR})

#-----------------------------------------------------------------------------------#
#DLIBXX
#-----------------------------------------------------------------------------------#
FIND_PACKAGE(DLIBXX REQUIRED)
SET(${PPROJC}_EXT_LIBS ${${PPROJC}_EXT_LIBS} ${DLIBXX_LIBRARIES})
	

	
	
