#------------------------------------------------------------------------------
#Plugin for ColorDeconvolution
#------------------------------------------------------------------------------
SET (PPROJC ColorDeconvolution)
INCLUDE (${PROJECT_SOURCE_DIR}/Plugins/${PPROJC}/TPL_Linux.cmake)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_SOURCE_DIR}/Common)

#------------------------------------------------------------------------------
#Set the source files necessary to build this plugin
#------------------------------------------------------------------------------

SET(${PPROJC}_DIRS
   Common/AbstractClasses
   Utilities/Pipelines/ColorDeconvolution
)

SET(${PPROJC}_SRCS)
FOREACH(CONTENT ${${PPROJC}_DIRS})
	FILE(GLOB_RECURSE U_SRCS
		 ${PROJECT_SOURCE_DIR}/${CONTENT}/*.cpp
                 ${PROJECT_SOURCE_DIR}/${CONTENT}/*.hh
                 ${PROJECT_SOURCE_DIR}/${CONTENT}/*.h
                 ${PROJECT_SOURCE_DIR}/${CONTENT}/*.txx)
    SET(${PPROJC}_SRCS ${${PPROJC}_SRCS} ${U_SRCS})
ENDFOREACH()

#------------------------------------------------------------------------------
# Build the plugin as a shared library
#------------------------------------------------------------------------------
ADD_LIBRARY(spin_plugin_${PPROJC} SHARED ${${PPROJC}_SRCS})
#https://stackoverflow.com/questions/5096881/does-set-target-properties-in-cmake-override-cmake-cxx-flags
TARGET_COMPILE_OPTIONS(spin_plugin_${PPROJC} PRIVATE "-DSPIN_PLUGIN_FRAMEWORK")
TARGET_LINK_LIBRARIES(spin_plugin_${PPROJC} ${${PPROJC}_EXT_LIBS})
#This step ensures that the plugin has similar naming conventions across platforms
SET_TARGET_PROPERTIES(spin_plugin_${PPROJC} PROPERTIES  PREFIX "")

#------------------------------------------------------------------------------
#Check if a C++ Unit Test is requested
#------------------------------------------------------------------------------
SET(ut_spin${PPROJC} OFF CACHE BOOL "Build C++ Unit test for ${PPROJC} Module")
IF(ut_spin${PPROJC}) 
    ADD_EXECUTABLE(ut_spin${PPROJC} ${PROJECT_SOURCE_DIR}/Utilities/Pipelines/ColorDeconvolution/UnitTest/ColorDeconvolution_UnitTest.cpp
				    ${PROJECT_SOURCE_DIR}/Utilities/ImageProcessing/ITKImageUtils/ITKImageUtils.cpp)
    TARGET_LINK_LIBRARIES(ut_spin${PPROJC} ${${PPROJC}_EXT_LIBS} dl)
ENDIF()

