#Set all Cmake related flags in this file
#----------------------------------------
IF(CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -fPIC -Wno-macro-redefined -Wno-comment -Wno-return-type -Wno-deprecated-declarations -DEIGEN_DONT_PARALLELIZE")
  SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC -DEIGEN_DONT_PARALLELIZE")
ENDIF()

IF (CMAKE_CXX_COMPILER_ID STREQUAL "GNU" )
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++14 -fPIC -DEIGEN_DONT_PARALLELIZE")
  SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC -DEIGEN_DONT_PARALLELIZE")
endif ()
