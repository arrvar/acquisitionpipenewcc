
# Set a default build type if none was specified
IF(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  MESSAGE(STATUS "Setting build type to 'Release' as none was specified.")
  SET(CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  SET_PROPERTY(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Release" "Debug" "MinSizeRel" "RelWithDebInfo")
ENDIF()

#===============================================================================
#check if OpenMP needs to be enabled (for G++ compilers)
#===============================================================================
IF(NOT MSVC)
  CHECK_CXX_COMPILER_FLAG("-fopenmp" COMPILER_SUPPORT_OPENMP)
  IF(COMPILER_SUPPORT_OPENMP)
    ADD_DEFINITIONS(-DUSE_OPENMP)
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")
    SET(CMAKE_C_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")
    MESSAGE(STATUS "OpenMP Available")
    #Now, the compiler may support OpenMP, but the user may have disabled it
    #In such a scenario, we remove it from the defines
    IF(NOT ENABLE_OPENMP)
      FOREACH(lang C CXX)
        IF("${CMAKE_${lang}_FLAGS}" MATCHES "-fopenmp")
          STRING(REGEX REPLACE "-fopenmp" "" CMAKE_${lang}_FLAGS "${CMAKE_${lang}_FLAGS}")
        ENDIF()
        
        IF("${CMAKE_${lang}_FLAGS}" MATCHES "-DUSE_OPENMP")
          STRING(REGEX REPLACE "-DUSE_OPENMP" "" CMAKE_${lang}_FLAGS "${CMAKE_${lang}_FLAGS}")
        ENDIF()
      ENDFOREACH()
      MESSAGE("..but not enabled by user")
    ENDIF()
  ENDIF()
ENDIF()

#===============================================================================
#check if OpenMP needs to be enabled (MSVC)
#===============================================================================
IF(MSVC)
  CHECK_CXX_COMPILER_FLAG("/openmp" COMPILER_SUPPORT_OPENMP)
  IF(COMPILER_SUPPORT_OPENMP)
    ADD_DEFINITIONS(-DUSE_OPENMP)
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /openmp")
    SET(CMAKE_C_FLAGS "${CMAKE_CXX_FLAGS} /openmp")
    MESSAGE(STATUS "OpenMP Available")
    #Now, the compiler may support OpenMP, but the user may have disabled it
    #In such a scenario, we remove it from the defines
    IF(NOT ENABLE_OPENMP)
      FOREACH(lang C CXX)
        IF("${CMAKE_${lang}_FLAGS}" MATCHES "/openmp")
          STRING(REGEX REPLACE "/openmp" "" CMAKE_${lang}_FLAGS "${CMAKE_${lang}_FLAGS}")
        ENDIF()
        
        IF("${CMAKE_${lang}_FLAGS}" MATCHES "-DUSE_OPENMP")
          STRING(REGEX REPLACE "-DUSE_OPENMP" "" CMAKE_${lang}_FLAGS "${CMAKE_${lang}_FLAGS}")
        ENDIF()
      ENDFOREACH()
      MESSAGE("..but not enabled by user")
    ENDIF()
  ENDIF()
ENDIF(MSVC)

IF(MSVC)
  IF (NOT ONCE_SET_CMAKE_INSTALL_PREFIX)
    SET(ONCE_SET_CMAKE_INSTALL_PREFIX true CACHE BOOL
        "Have we set the install prefix yet?" FORCE)
    SET(CMAKE_INSTALL_PREFIX /usr/local CACHE PATH
        "Install path prefix, prepended onto install directories" FORCE)
  ENDIF()
  ADD_DEFINITIONS("/wd4996 /wd4267 /wd4291 /wd4244 /wd4800 /wd4251 /wd4101 /wd4661 /wd4172")
ENDIF()

#------------------------------------------------------------------------------
# A macro to parse through all sub folders within a main folder
#------------------------------------------------------------------------------
MACRO(SUBDIRLIST result curdir)
  FILE(GLOB children RELATIVE ${curdir} ${curdir}/*)
  SET(dirlist "")
  FOREACH(child ${children})
    IF(IS_DIRECTORY ${curdir}/${child})
      LIST(APPEND dirlist ${child})
    ENDIF()
  ENDFOREACH()
  SET(${result} ${dirlist})
ENDMACRO()
