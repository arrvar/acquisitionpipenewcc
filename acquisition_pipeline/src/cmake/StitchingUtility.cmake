INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR} ${PROJECT_BINARY_DIR})
#extract all the required source files to be compiled
FILE(GLOB_RECURSE SR_SRCS
   ${PROJECT_SOURCE_DIR}/UnitTest/StitchingUtility/*.cpp
   ${PROJECT_SOURCE_DIR}/UnitTest/StitchingUtility/*.h
)
#============================================================================#
#Now, there may be other files that would need to be MOC'ed
#============================================================================#
FILE(GLOB_RECURSE SR_MOC_HDRS
   ${PROJECT_SOURCE_DIR}/UnitTest/StitchingUtility/*.h
)

#============================================================================#
#Make a copy of this list
#============================================================================#
SET(DUP_MOC_HDRS ${SR_MOC_HDRS})

#============================================================================#
#Not all files will contain MOC-able code. remove such files
#============================================================================#
FOREACH(MY_SUBFILE ${SR_MOC_HDRS})
  #==========================================================================#
  #Read the contents of the file. If it does not contain Q_OBJECT remove it
  #from the list
  #==========================================================================#
  FILE(STRINGS ${MY_SUBFILE} MY_CONTENT)
  #==========================================================================#
  #By default, we assume that the file does not contain Q_OBJECT$
  #==========================================================================#
  SET(POS -1)
  #==========================================================================#
  #Now, check if the file contains Q_OBJECT. If not, remove it from the list
  #==========================================================================#
  FOREACH(CONTENT ${MY_CONTENT})
    STRING(REGEX MATCH "Q_OBJECT$" A_STRING "${CONTENT}")
    IF("${A_STRING}" STREQUAL "Q_OBJECT")
      SET(POS 1)
      STRING(REGEX MATCH "^//" B_STRING "${CONTENT}")
      IF("${B_STRING}" STREQUAL "//")
        SET(POS -1)
      ENDIF()
    ENDIF()
  ENDFOREACH(CONTENT)
  #==========================================================================#
  #Now, decide if we need to remove this file from the list based on POS
  #==========================================================================#
  IF(POS EQUAL -1)
    LIST( REMOVE_ITEM DUP_MOC_HDRS ${MY_SUBFILE} )
  ENDIF()
ENDFOREACH(MY_SUBFILE)


#==============================================================================#
# this command will generate rules that will run MOC on all files from 
# DUP_MOC_HDRS.
#==============================================================================#
SET(SR_MOC_SRCS)
QT5_WRAP_CPP(SR_MOC_SRCS ${DUP_MOC_HDRS} )

#==============================================================================#
#Build the libraries here
#==============================================================================#
ADD_EXECUTABLE(StitchingUtility ${SR_SRCS} ${SR_MOC_SRCS})
TARGET_LINK_LIBRARIES(StitchingUtility spin${PROJ}Acquisition ${${PROJ}_EXT_LIBS}) 

IF(MSVC)
    ADD_CUSTOM_COMMAND(
		    TARGET StitchingUtility  
		    POST_BUILD 
		    COMMAND ${CMAKE_COMMAND} 
             -DTARGETDIR=$<TARGET_FILE_DIR:StitchingUtility> 
             -DRELEASE_LIBS=${RELEASE_LIBS_TO_BE_COPIED}
             -DDEBUG_LIBS=${DEBUG_LIBS_TO_BE_COPIED}
             -DCFG=${CMAKE_CFG_INTDIR} -P "${LIBRARY_ROOT}/PostBuild.cmake"
    )
ENDIF()
