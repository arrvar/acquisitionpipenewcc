#-------------------------------------------------------------------------------
#Generic CMakeLists.txt for all Spectral Insights projects
#-------------------------------------------------------------------------------
PROJECT( spin C CXX)
CMAKE_MINIMUM_REQUIRED(VERSION 3.4)

#------------------------------------------------------------------------------#
# Enable OpenMP if available. In addition, provide a manual override which is
# ON unless the user disables it, in which case OpenMP will not be enabled
#------------------------------------------------------------------------------#
SET(ENABLE_OPENMP OFF CACHE BOOL "OpenMP disabled by default")
INCLUDE(CheckCXXCompilerFlag)
INCLUDE(${PROJECT_SOURCE_DIR}/cmake/CommonUtils.cmake)
INCLUDE(${PROJECT_SOURCE_DIR}/cmake/CMake_Flags.cmake)

#===============================================================================
# Check if we are building OpenCL enabled applications
#===============================================================================
SET(ENABLE_OPENCL OFF CACHE BOOL "Build OpenCL Applications")
IF(ENABLE_OPENCL)
  ADD_DEFINITIONS(-DCL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY)
  ADD_DEFINITIONS(-DUSE_OPENCL)
  SET(ENABLE_AMD_SI_NI_BUG OFF CACHE BOOL "AMD Hainan Bug?")
  IF(ENABLE_AMD_SI_NI_BUG)
      ADD_DEFINITIONS(-DAMD_HAINAN_BUG)
  ELSE()
      REMOVE_DEFINITIONS(-DAMD_HAINAN_BUG)
  ENDIF()
ELSE()
  REMOVE_DEFINITIONS(-DUSE_OPENCL)
ENDIF()

#===============================================================================
# List all the modules that can be build and by default do not build anything
#===============================================================================
SUBDIRLIST(MODULES ${PROJECT_SOURCE_DIR}/Libraries)
FOREACH(module ${MODULES})
  SET(Module_${module} OFF CACHE BOOL "Build ${module} Module")
  IF(Module_${module})
    INCLUDE(${PROJECT_SOURCE_DIR}/Libraries/${module}/cmake/${module}.cmake)
  ENDIF()
ENDFOREACH()

#===============================================================================
# List all plugins that can be build and by default do not build anything
#===============================================================================
SUBDIRLIST(PLUGINS ${PROJECT_SOURCE_DIR}/Plugins)
FOREACH(plugin ${PLUGINS})
  SET(Plugin_${plugin} OFF CACHE BOOL "Build ${plugin} PLUGIN")
  IF(Plugin_${plugin})
    INCLUDE(${PROJECT_SOURCE_DIR}/Plugins/${plugin}/${plugin}.cmake)
  ENDIF()
ENDFOREACH()
